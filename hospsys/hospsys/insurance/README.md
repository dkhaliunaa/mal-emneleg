Бүртгэлийн Програм
---------------------------------

# Хавтасын зохион байгуулалт
-----------------------------------------
pubspec.yaml      Dart тохиргооны файл
jars              Java сангууд  
lib               Вэб клиентийн дундын код, сан

WorkingDirectory  Програм ажиллах хавтас
  ├── data/       Өгөгдөл байрлах хавтас
  ├── tools/      Туслах болон ар талд ажиллах програмууд
  ├── log/        Log файлууд
  └── tmp/        Програм ажиллах явцад үүссэн файлууд


# Суулгах
----------------------------

## Jetty 9.3 хувилбар суулгах

http://download.eclipse.org/jetty/ хаягаас татаад задлана

jetty-н start.ini файлд дараах нэмэлт модулиуд тохируулна

--module=servlets
--module=servlet
--module=webapp
--module=jsp
--module=jstl
--module=websocket
--module=gzip

## MongoDB суулгах
 
2.2 Polymer.js програмын хувьд

gulp програмаар хөрвүүлнэ
 
3. Хөрвүүлэлтээс үүссэн файлуудыг хуулах

Үүнд:
- dart програмаас үүссэн build хавтас дотор байгаа web хавтасыг release байрлах хавтас рүү web нэртэй болгон хуулна.
- polymer.js програм хөрвүүлээд үүссэн dist хавтасын нэрийг сольж WebContent руу хуулна
- jars хавтас
- WorkingDirectory хавтасыг бүтнээр нь хуулна (сервер дээр өмнө нь байгаа бол шаардлагагүй)
   
   
# Тохиргоо
-----------------------------------------

## Модулиудын тохиргоо 

Модулиудын тохиргоог WorkingDirectory/*.properties файлуудад хийнэ. 
Жнь: TUS-н тохиргоо tus.properties файлд байна.

## JVM орчны хувьсагч тохируулах

jetty дээр асаах бол start.ini файлд дараах тохируулгууд хийнэ.

-Derp.home=/home/erp/WorkingDirectory
-Derp.debug=true

erp.home орчны хувьсагчид WorkingDirectory замыг заана. 
Жишээ нь: -Derp.home= ~/erp/WorkingDirectory

Тестийн горимд програмыг асаах бол erp.debug=true хувьсагчийг тохируулна
Жишээ нь: -Derp.debug=true

## Зураг болон файлын замыг заах

/jetty/etc/jetty.xml - Энэ файлд дараах замыг тохируулна
<Item>
    <New class="org.eclipse.jetty.server.handler.ContextHandler">
        <Set name="contextPath">/res</Set>
        <Set name="handler">
            <New class="org.eclipse.jetty.server.handler.ResourceHandler">
            <Set name="directoriesListed">true</Set>
            <Set name="resourceBase">/home/erp/tmp</Set>
            </New>
        </Set>
    </New>
</Item>
<Item>
    <New class="org.eclipse.jetty.server.handler.ContextHandler">
        <Set name="contextPath">/book</Set>
        <Set name="handler">
            <New class="org.eclipse.jetty.server.handler.ResourceHandler">
            <Set name="directoriesListed">true</Set>
            <Set name="resourceBase">/home/erp/ebook</Set>
            </New>
        </Set>
    </New>
</Item>


## Хавтасууд тохируулах

FILE_PATH - Програм ажиллах явцад үүсэж байгаа файлууд байрших хавтас
BOOK_PATH - eBook програмд ашиглагдах электрон номууд байрлах хавтас
ANKET_PATH - Ажил горилогчийн мэдээлэл файлууд байрлах хавтас


## Вэб клиент програмаас хандах URL хаягуудыг тохируулна

RPC_SERVER - RPC серверийн хаяг
FILE_SERVER - Файл серверийн хаяг

## Tools ажиллуулах

WorkingDirectory/tools хавтас дотор ар талд эсвэл командын мөрнөөс шууд ажиллах програмуудыг байршуулсан байгаа. 
Эдгээрийг *_runner.sh  төгсгөлтэй програмуудыг crontab ашиглан цагийн тохируулга хийх хэрэгтэй.


# Cервисүүд

WebContent/WEB-INF/web.xml файлд сервисүүдийг тохируулсан байгаа.
Сервисүүд нь дараах хаягууд дээр ажиллана:

Үндсэн сервисүүд
---------------------------
/rpc/auth   - AuthService
/rpc/db     - DataService

Файл сервисүүд
--------------------------
/file       - FileService
/picture    - PictureService

# Ерөнхий алдааны мессеж харуулах
this.fire('show-toast-message', {success:false,message:"Та энэ үйлдлийг хийх боломжгүй байна!!!"});


# Jetty сан

D:\Tiku\erp\Jetty\lib\jetty-server-9.3.3.v20150827.jar

D:\Tiku\erp\Jetty\lib\jetty-servlet-9.3.3.v20150827.jar

D:\Tiku\erp\Jetty\lib\jetty-servlets-9.3.3.v20150827.jar

D:\Tiku\erp\Jetty\lib\websocket\javax.websocket-api-1.0.jar

D:\Tiku\erp\Jetty\lib\websocket\websocket-api-9.3.3.v20150827.jar

D:\Tiku\erp\Jetty\lib\websocket\websocket-server-9.3.3.v20150827.jar

D:\Tiku\erp\Jetty\lib\websocket\websocket-servlet-9.3.3.v20150827.jar

AIzaSyAYv6UB3NxC2XzwcdQeKqf5aiWw0tk2wDo