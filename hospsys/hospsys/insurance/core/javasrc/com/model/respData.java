package com.model;

import java.util.List;
/**
 * Created by 22cc3355 on 3/23/2017.
 */
public class respData {
    private boolean success;
    private String message;
    private List<ErrorObject> errors;

    public void setSuccess(boolean success) { this.success = success; }
    public boolean getSuccess() { return this.success; }
    public void setMessage(String message) { this.message = message; }
    public void setError(List<ErrorObject> errors) { this.errors = errors; }
}
