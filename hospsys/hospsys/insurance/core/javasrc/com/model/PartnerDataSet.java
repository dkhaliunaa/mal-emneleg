package com.model;

/**
 * Created by 24be10264 on 2/8/2017.
 */
public class PartnerDataSet {
    private int index;
    private String lastName = "";
    private String firstName = "";
    private String regId = "";
    private String mobile = "";
    private String address = "";
    private String familyProcent = "";

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getRegId() {
        return regId;
    }

    public void setRegId(String regId) {
        this.regId = regId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFamilyProcent() {
        return familyProcent;
    }

    public void setFamilyProcent(String familyProcent) {
        this.familyProcent = familyProcent;
    }
}
