package com.model;

import com.mongodb.BasicDBObject;

import java.sql.Date;

/**
 * Created by 24be10264 on 8/19/2016.
 */
public class FavoritePages extends BasicDBObject {
    /**
     * Хамгийн их ашигласан хуудас
     *
     * id           -- Хүснэгтийн  дугаар
     * page         -- Ашигласан хуудас
     * create_at    -- Сүүлд ашигласан огноо
     * create_by    -- Ашиглсан ажилтан
     * count        -- Хэр олон удаа ашигласан тоо
     * */
    
    public static final String ID = "id";
    public static final String MENUID = "menu_id";
    public static final String CREATE_AT = "cre_at";
    public static final String CREATE_BY = "cre_by";
    public static final String COUNT = "usecount";

    public int getId() {
        return this.getInt(ID);
    }

    public void setId(int id) {
        this.put(ID, id);
    }

    public String getMenuid() {
        return this.getString(MENUID, "");
    }

    public void setMenuid(String page) {
        this.put(MENUID, page);
    }

    public Date getCreate_at() {
        return (Date) this.getDate(CREATE_AT);
    }

    public void setCreate_at(Date create_at) {
        this.put(CREATE_AT, create_at);
    }

    public String getCreate_by() {
        return this.getString(CREATE_BY, "");
    }

    public void setCreate_by(String create_by) {
        this.put(CREATE_BY, create_by);
    }

    public int getCount(){
        return  this.getInt(COUNT, 0);
    }

    public void setCount(int count){
        this.put(COUNT, count);
    }
}
