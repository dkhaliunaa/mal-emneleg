package com.model;

import com.mongodb.BasicDBObject;

import java.sql.Date;

/**
 * Created by 24be10264 on 12/22/2016.
 */
public class UserAndRole extends BasicDBObject{
    /**
     ID	        1	1	N	INTEGER		            None
     DOMAIN	    2		N	VARCHAR2 (20 Byte)		Frequency
     ROLE_CODE	3		N	INTEGER		            Frequency
     PERMS	    4		N	CHAR (1 Byte)		    None
     CREATED_AT	5		N	DATE		            None
     CREATED_BY	6		N	VARCHAR2 (20 Byte)		None
     MODIFY_AT	7		Y	DATE		            None
     MODIFY_BY	8		Y	VARCHAR2 (20 Byte)		None
     *
     *
     */

    private final String USERNAME = "username";
    private final String ROLECODE = "rolecode";
    private final String CREAT = "cre_at";
    private final String CREBY = "cre_by";
    private final String MODAT = "mod_at";
    private final String MODBY = "mod_by";

    public String getUsername() {
        return this.getString(USERNAME, "");
    }

    public void setUsername(String username) {
        this.put(USERNAME, username);
    }

    public String getRoleCode() {
        return this.getString(ROLECODE, "");
    }

    public void setRoleCode(String roleCode) {
        this.put(ROLECODE, roleCode);
    }

    public Date getCreAt() {
        return (Date) this.getDate(CREAT, null);
    }

    public void setCreAt(Date creAt) {
        this.put(CREAT, creAt);
    }

    public String getCreBy() {
        return this.getString(CREBY, "");
    }

    public void setCreBy(String creBy) {
        this.put(CREBY, creBy);
    }

    public Date getModAt() {
        return (Date) this.getDate(MODAT, null);
    }

    public void setModAt(Date modAt) {
        this.put(MODAT, modAt);
    }

    public String getModBy() {
        return this.getString(MODBY, "");
    }

    public void setModBy(String modBy) {
        this.put(MODBY, modBy);
    }
}
