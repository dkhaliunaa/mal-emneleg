package com.model;

import com.mongodb.BasicDBObject;

/**
 * Created by 22cc3355 on 6/30/2016.
 */
public class UserroleList extends BasicDBObject {

    /**
     * домайн
     */
    public int getId() {
        return this.getInt("id");
    }

    public void setId(int id) { this.put("id", id); }

    /**
     * Өөрийн нэр
     */
    public String getDomain() { return this.getString("domain"); }

    public void setDomain(String domain) {
        this.put("domain", domain);
    }

    /**
     * Өөрийн нэр
     */
    public String getFirstname() { return this.getString("firstname"); }

    public void setFirstname(String name) {
        this.put("firstname", name);
    }

    /**
     * Өөрийн нэр
     */
    public String getLastname() {
        return this.getString("lastname");
    }

    public void setLastname(String name) {
        this.put("lastname", name);
    }

    /**
     * Э-мэйл
     */
    public String getEmail() {
        return this.getString("email");
    }

    public void setEmail(String email) {
        this.put("email", email);
    }

    /**
     * Эрх
     */
    public String getRole() {
        return this.getString("role");
    }

    public void setRole(String role) {
        this.put("role", role);
    }

    /**
     * Эрх
     */
    public String getPerms() {
        return this.getString("perms");
    }

    public void setPerms(String perms) {
        this.put("perms", perms);
    }

    /*
    Menu Id
     */
    public String getMenuId() {
        return this.getString("menuid");
    }

    public void setMenuId(String menuId) {
        this.put("menuid", menuId);
    }

    /*
    Menu Id
     */
    public String getDescription() {
        return this.getString("description");
    }

    public void setDescription(String description) {
        this.put("description", description);
    }
}
