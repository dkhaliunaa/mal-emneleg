package com.model;

import com.mongodb.BasicDBObject;

/**
 * Created by 24be10264 on 12/17/2016.
 */
public class ProjectErrorCode extends BasicDBObject {

    /**
     * Прожектэд холбоотой алдааны мэдээллийг хадгалсан
     *
     ERRCODE	1	1	N	INTEGER		        None
     ERRNAME	2		N	NVARCHAR2 (500)		None
     ERRDESC	3		N	NVARCHAR2 (1200)	None

     * @param errCode           - Алдааны код
     * @param errName           - Алдааны мэдээлэл
     * @param errDesc           - Алдааны тайлбар (Ямар тохиолдолд гардаг талаар дэлгэрэнгүй бичсэн байна.)
     *
     */

    public static final String ERRCODE = "errCode";
    public static final String ERRNAME = "errName";
    public static final String ERRDESC = "errDesc";

    public int getErrCode() {
        return (this.get(ERRCODE) != null) ? getInt (ERRCODE) : 0;
    }

    public void setErrCode(int errCode) {
        this.put(ERRCODE, errCode);
    }

    public String getErrName() {
        return getString (ERRNAME);
    }

    public void setErrName(String errName) {
        this.put(ERRNAME, errName);
    }

    public String getErrDesc() {
        return getString (ERRDESC);
    }

    public void setErrDesc(String errDesc) {
        this.put(ERRDESC, errDesc);
    }
}
