package com.model.hos;

/**
 * Created by 24be10264 on 9/5/2017.
 */

import com.mongodb.BasicDBObject;

import java.sql.Date;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public class Form52Entity extends BasicDBObject {
    public final String ID = "id";
    public final String UVCHINNER = "uvchinNer";
    public final String MALNER = "malNer";
    public final String BUGDUVCHILSEN = "bugdUvchilsen";
    public final String BUGDUHSEN = "bugdUhsen";
    public final String BUGDEDGERSEN = "bugdEdgersen";
    public final String TEMEEUVCHILSEN = "temeeUvchilsen";
    public final String TEMEEUHSEN = "temeeUhsen";
    public final String TEMEEEDGERSEN = "temeeEdgersen";
    public final String ADUUUVCHILSEN = "aduuUvchilsen";
    public final String ADUUUHSEN = "aduuUhsen";
    public final String ADUUEDGERSEN = "aduuEdgersen";
    public final String UHERUVCHILSEN = "uherUvchilsen";
    public final String UHERUHSEN = "uherUhsen";
    public final String UHEREDGERSEN = "uherEdgersen";
    public final String HONIUVCHILSEN = "honiUvchilsen";
    public final String HONIUHSEN = "honiUhsen";
    public final String HONIEDGERSEN = "honiEdgersen";
    public final String YAMAAUVCHILSEN = "yamaaUvchilsen";
    public final String YAMAAUHSEN = "yamaaUhsen";
    public final String YAMAAEDGERSEN = "yamaaEdgersen";
    public final String BUSADUVCHILSEN = "busadUvchilsen";
    public final String BUSADUHSEN = "busadUhsen";
    public final String BUSADEDGERSEN = "busadEdgersen";
    public final String AIMAGNER = "aimagNer";
    public final String SUMNER = "sumNer";

    public final String UVCHINNER_EN = "uvchinNer_En";
    public final String AIMAG = "aimag";
    public final String SUM = "sum";
    public final String AIMAG_EN = "aimag_en";
    public final String SUM_EN = "sum_en";

    public final String DELFLG = "delFlg";
    public final String ACTFLG = "actFlg";
    public final String MODBY = "modBy";
    public final String MODAT = "modAt";
    public final String CREBY = "creBy";
    public final String CREAT = "creAt";
    public final String RECORD_DATE = "recordDate";
    public final String SEARCH_RECORD_DATE = "searchRecordDate";


    public Form52Entity() {

    }

    public Long getId() {
        return this.getLong(ID, 0);
    }

    public void setId(Long id) {
        this.put(ID, id);
    }

    public String getAimagNer() {
        return this.getString(AIMAGNER, "");
    }

    public void setAimagNer(String aimagNer) {
        this.put(AIMAGNER, aimagNer);
    }

    public String getMalNer() {
        return this.getString(MALNER, "");
    }

    public void setMalNer(String malNer) {
        this.put(MALNER, malNer);
    }

    public String getSumNer() {
        return this.getString(SUMNER, "");
    }

    public void setSumNer(String sumNer) {
        this.put(SUMNER, sumNer);
    }

    public String getAimag() {
        return this.getString(AIMAG);
    }

    public void setAimag(String aimag) {
        this.put(AIMAG, aimag);
    }

    public String getSum() {
        return this.getString(SUM, "");
    }

    public void setSum(String sum) {
        this.put(SUM, sum);
    }

    public String getAimag_en() {
        return this.getString(AIMAG_EN, "");
    }

    public void setAimag_en(String aimag_en) {
        this.put(AIMAG_EN, aimag_en);
    }

    public String getSum_en() {
        return this.getString(SUM_EN, "");
    }

    public void setSum_en(String sum_en) {
        this.put(SUM_EN, sum_en);
    }

    public String getUvchinNer() {
        return this.getString(UVCHINNER, "");
    }

    public void setUvchinNer(String uvchinner) {
        this.put(UVCHINNER, uvchinner);
    }

    public String getUvchinNer_En() {
        return this.getString(UVCHINNER_EN, "");
    }

    public void setUvchinNer_En(String uvchinNer_en) {
        this.put(UVCHINNER_EN, uvchinNer_en);
    }

    public Long getBugdUvchilsen() {
        return this.getLong(BUGDUVCHILSEN, 0);
    }

    public void setBugdUvchilsen(Long bugdUvchilsen) {
        this.put(BUGDUVCHILSEN, bugdUvchilsen);
    }

    public Long getBugdUhsen() {
        return this.getLong(BUGDUHSEN, 0);
    }

    public void setBugdUhsen(Long bugdUhsen) {
        this.put(BUGDUHSEN, bugdUhsen);
    }

    public Long getBugdEdgersen() {
        return this.getLong(BUGDEDGERSEN, 0);
    }

    public void setBugdEdgersen(Long bugdEdgersen) {
        this.put(BUGDEDGERSEN, bugdEdgersen);
    }

    public Long getTemeeUvchilsen() {
        return this.getLong(TEMEEUVCHILSEN, 0);
    }

    public void setTemeeUvchilsen(Long temeeUvchilsen) {
        this.put(TEMEEUVCHILSEN, temeeUvchilsen);
    }

    public Long getTemeeUhsen() {
        return this.getLong(TEMEEUHSEN, 0);
    }

    public void setTemeeUhsen(Long temeeUhsen) {
        this.put(TEMEEUHSEN, temeeUhsen);
    }

    public Long getTemeeEdgersen() {
        return this.getLong(TEMEEEDGERSEN, 0);
    }

    public void setTemeeEdgersen(Long temeeEdgersen) {
        this.put(TEMEEEDGERSEN, temeeEdgersen);
    }

    public Long getAduuUvchilsen() {
        return this.getLong(ADUUUVCHILSEN, 0);
    }

    public void setAduuUvchilsen(Long aduuUvchilsen) {
        this.put(ADUUUVCHILSEN, aduuUvchilsen);
    }

    public Long getAduuUhsen() {
        return this.getLong(ADUUUHSEN, 0);
    }

    public void setAduuUhsen(Long aduuUhsen) {
        this.put(ADUUUHSEN, aduuUhsen);
    }

    public Long getAduuEdgersen() {
        return this.getLong(ADUUEDGERSEN, 0);
    }

    public void setAduuEdgersen(Long aduuEdgersen) {
        this.put(ADUUEDGERSEN, aduuEdgersen);
    }

    public Long getUherUvchilsen() {
        return this.getLong(UHERUVCHILSEN, 0);
    }

    public void setUherUvchilsen(Long uherUvchilsen) {
        this.put(UHERUVCHILSEN, uherUvchilsen);
    }

    public Long getUherUhsen() {
        return this.getLong(UHERUHSEN, 0);
    }

    public void setUherUhsen(Long uherUhsen) {
        this.put(UHERUHSEN, uherUhsen);
    }

    public Long getUherEdgersen() {
        return this.getLong(UHEREDGERSEN, 0);
    }

    public void setUherEdgersen(Long uherEdgersen) {
        this.put(UHEREDGERSEN, uherEdgersen);
    }

    public Long getHoniUvchilsen() {
        return this.getLong(HONIUVCHILSEN, 0);
    }

    public void setHoniUvchilsen(Long honiUvchilsen) {
        this.put(HONIUVCHILSEN, honiUvchilsen);
    }

    public Long getHoniUhsen() {
        return this.getLong(HONIUHSEN, 0);
    }

    public void setHoniUhsen(Long honiUhsen) {
        this.put(HONIUHSEN, honiUhsen);
    }

    public Long getHoniEdgersen() {
        return this.getLong(HONIEDGERSEN, 0);
    }

    public void setHoniEdgersen(Long honiEdgersen) {
        this.put(HONIEDGERSEN, honiEdgersen);
    }

    public Long getYamaaUvchilsen() {
        return this.getLong(YAMAAUVCHILSEN, 0);
    }

    public void setYamaaUvchilsen(Long yamaaUvchilsen) {
        this.put(YAMAAUVCHILSEN, yamaaUvchilsen);
    }

    public Long getYamaaUhsen() {
        return this.getLong(YAMAAUHSEN, 0);
    }

    public void setYamaaUhsen(Long yamaaUhsen) {
        this.put(YAMAAUHSEN, yamaaUhsen);
    }

    public Long getYamaaEdgersen() {
        return this.getLong(YAMAAEDGERSEN, 0);
    }

    public void setYamaaEdgersen(Long yamaaEdgersen) {
        this.put(YAMAAEDGERSEN, yamaaEdgersen);
    }

    public Long getBusadUvchilsen() {
        return this.getLong(BUSADUVCHILSEN, 0);
    }

    public void setBusadUvchilsen(Long busadUvchilsen) {
        this.put(BUSADUVCHILSEN, busadUvchilsen);
    }

    public Long getBusadUhsen() {
        return this.getLong(BUSADUHSEN, 0);
    }

    public void setBusadUhsen(Long busaduhsnUhsen) {
        this.put(BUSADUHSEN, busaduhsnUhsen);
    }


    public Long getBusadEdgersen() {
        return this.getLong(BUSADEDGERSEN, 0);
    }

    public void setBusadEdgersen(Long busadEdgersen) {
        this.put(BUSADEDGERSEN, busadEdgersen);
    }
    public String getDelflg() {
        return this.getString(DELFLG, "");
    }

    public void setDelflg(String delflg) {
        this.put(DELFLG, delflg);
    }

    public String getActflg() {
        return this.getString(ACTFLG, "");
    }

    public void setActflg(String actflg) {
        this.put(ACTFLG, actflg);
    }

    public String getModby() {
        return getString(MODBY, "");
    }

    public void setModby(String modby) {
        this.put(MODBY, modby);
    }

    public Date getModAt() {
        return (Date) getDate(MODAT);
    }

    public void setModAt(Date modAt) {
        this.put(MODAT, modAt);
    }

    public String getCreby() {
        return getString(CREBY, "");
    }

    public void setCreby(String creby) {
        this.put(CREBY, creby);
    }

    public Date getCreat() {
        return (Date) getDate(CREAT, null);
    }

    public void setCreat(Date creat) {
        this.put(CREAT, creat);
    }

    public Date getRecordDate() {
        return (Date) this.getDate(RECORD_DATE, null);
    }

    public void setRecordDate(Date recordDate) {
        this.put(RECORD_DATE, recordDate);
    }

    public Date getSearchRecordDate() {
        return (Date) this.getDate(SEARCH_RECORD_DATE, null);
    }

    public void setSearchRecordDate(Date recordDate) {
        this.put(SEARCH_RECORD_DATE, recordDate);
    }
}
