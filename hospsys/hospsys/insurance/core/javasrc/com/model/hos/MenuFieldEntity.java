package com.model.hos;

import java.util.Date;

/**
 * Created by 252510615 on 17.08.22.
 */
public class MenuFieldEntity {
    private String fiedlId;
    private String fieldName;
    private String fieldTitleMn;
    private String fieldTitleEn;
    private String fieldReq;
    private String fieldMask;
    private String fieldDefVal;
    private Integer fieldSortId;
    private String fieldType;
    private String comboId;
    private String comboIsChild;
    private String comboIsChildFieldId;
    private String radioButtonId;
    private String fieldDateFormat;
    private String delFlag;
    private String actFlag;
    private Date createAt;
    private String createBy;
    private Date modifyAt;
    private String modifyBy;

    public String getFieldId() {
        return fiedlId;
    }

    public void setFieldId(String fiedlId) {
        this.fiedlId = fiedlId;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldTitleMn() {
        return fieldTitleMn;
    }

    public void setFieldTitleMn(String fieldTitleMn) {
        this.fieldTitleMn = fieldTitleMn;
    }

    public String getFieldTitleEn() {
        return fieldTitleEn;
    }

    public void setFieldTitleEn(String fieldTitleEn) {
        this.fieldTitleEn = fieldTitleEn;
    }

    public String getFieldReq() {
        return fieldReq;
    }

    public void setFieldReq(String fieldReq) {
        this.fieldReq = fieldReq;
    }

    public String getFieldMask() {
        return fieldMask;
    }

    public void setFieldMask(String fieldMask) {
        this.fieldMask = fieldMask;
    }

    public String getFieldDefVal() {
        return fieldDefVal;
    }

    public void setFieldDefVal(String fieldDefVal) {
        this.fieldDefVal = fieldDefVal;
    }

    public Integer getFieldSortId() {
        return fieldSortId;
    }

    public void setFieldSortId(Integer fieldSortId) {
        this.fieldSortId = fieldSortId;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getComboId() {
        return comboId;
    }

    public void setComboId(String comboId) {
        this.comboId = comboId;
    }

    public String getComboIsChild() {
        return comboIsChild;
    }

    public void setComboIsChild(String comboIsChild) {
        this.comboIsChild = comboIsChild;
    }

    public String getComboIsChildFieldId() {
        return comboIsChildFieldId;
    }

    public void setComboIsChildFieldId(String comboIsChildFieldId) {
        this.comboIsChildFieldId = comboIsChildFieldId;
    }

    public String getRadioButtonId() {
        return radioButtonId;
    }

    public void setRadioButtonId(String radioButtonId) {
        this.radioButtonId = radioButtonId;
    }

    public String getFieldDateFormat() {
        return fieldDateFormat;
    }

    public void setFieldDateFormat(String fieldDateFormat) {
        this.fieldDateFormat = fieldDateFormat;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getActFlag() {
        return actFlag;
    }

    public void setActFlag(String actFlag) {
        this.actFlag = actFlag;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getModifyAt() {
        return modifyAt;
    }

    public void setModifyAt(Date modifyAt) {
        this.modifyAt = modifyAt;
    }

    public String getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(String modifyBy) {
        this.modifyBy = modifyBy;
    }
}