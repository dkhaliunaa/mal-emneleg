package com.model.hos;

import com.mongodb.BasicDBObject;

import java.sql.Date;

/**
 * Created by Tiku on 8/19/2017.
 * <p>
 * Меню удирдах класс юм. Өгөгдлийн сан дээр h_menu нэртэй хүснэгтийн
 * объект класс нь юм. Класс нь зөвхөн объект дамжуулах үүрэгтэй.
 * Олон төрлийн арга бичихийг хориглоно. Класс нь дараах гишүүн өгөгдөлтэй. ҮҮНД:
 * 1	id
 * 2	menu_id
 * 3	menu_name_mn
 * 4	menu_desc
 * 5	menu_name_en
 * 6	delflg
 * 7	cre_at
 * 8	cre_by
 * 9	mod_at
 * 10	mod_by
 * 11	formid
 */
public class MenuEntity extends BasicDBObject {
    /**
     * Гишүүн өгөгдлүүд болон тэдгээрийн getter, setter аргууд нь байна.
     */
    public static final String ID = "id";
    public static final String MENUID = "menuId";
    public static final String MENUNAMEMN = "menuNameMN";
    public static final String MENUNAMEEN = "menuNameEN";
    public static final String MENUDESC = "menuDesc";
    public static final String FORMID = "formId";
    public static final String DELFLG = "delFlg";
    public static final String ACTFLG = "actFlg";
    public static final String MODBY = "modBy";
    public static final String MODAT = "modAt";
    public static final String CREBY = "creBy";
    public static final String CREAT = "creAt";

    public Long getID() {
        return this.getLong(ID, 0);
    }

    public void setId(Long id) {
        this.put(ID, id);
    }

    public String getMenuId() {
        return this.getString(MENUID, "");
    }

    public void setMenuid(String menuid) {
        this.put(MENUID, menuid);
    }

    public String getMenuNameMN() {
        return this.getString(MENUNAMEMN, "");
    }

    public void setMenunamemn(String menunamemn) {
        this.put(MENUNAMEMN, menunamemn);
    }

    public String getMenuNameEN() {
        return this.getString(MENUNAMEEN, "");
    }

    public void setMenunameen(String menunameen) {
        this.put(MENUNAMEEN, menunameen);
    }

    public String getMenuDesc() {
        return this.getString(MENUDESC, "");
    }

    public void setMenudesc(String menudesc) {
        this.put(MENUDESC, menudesc);
    }

    public String getFormid() {
        return this.getString(FORMID, "");
    }

    public void setFormid(String formid) {
        this.put(FORMID, formid);
    }

    public String getDelflg() {
        return this.getString(DELFLG, "");
    }

    public void setDelflg(String delflg) {
        this.put(DELFLG, delflg);
    }

    public String getActflg() {
        return this.getString(ACTFLG, "");
    }

    public void setActflg(String actflg) {
        this.put(ACTFLG, actflg);
    }

    public String getModby() {
        return getString(MODBY, "");
    }

    public void setModby(String modby) {
        this.put(MODBY, modby);
    }

    public  String getModat() {
        return MODAT;
    }

    public void setModat(Date modat){
        put(MODAT, modat);
    }

    public String getCreby() {
        return getString(CREBY, "");
    }

    public void setCreby(String creby) {
        this.put(CREBY, creby);
    }

    public String getCreat() {
        return getString(CREAT, "");
    }

    public void setCreat(Date creat) {
        this.put(CREAT, creat);
    }
}
