package com.model.hos;

import com.mongodb.BasicDBObject;
import java.sql.Date;

public class NewsEntity extends BasicDBObject {

    public final String ID="id";

    public final String TITLE ="title";
    public final String CONTENT ="content";

    public final String TITLE_EN ="title_en";
    public final String CONTENT_EN ="content_en";

    public final String DELFLG = "delFlg";
    public final String ACTFLG = "actFlg";
    public final String MODBY = "modBy";
    public final String MODAT = "modAt";
    public final String CREBY = "creBy";
    public final String CREAT = "creAt";

    public Long getId() {
        return this.getLong(ID,0);
    }

    public void setId(Long id) {
        this.put(ID,id);
    }

    public String getTitle() {
        return this.getString(TITLE,"");
    }

    public void setTitle(String title) {
        this.put(TITLE,title);
    }

    public String getContent() {
        return this.getString(CONTENT,"");
    }

    public void setContent(String content) {
        this.put(CONTENT,content);
    }

    public String getTitle_en() {
        return this.getString(TITLE_EN,"");
    }

    public void setTitle_en(String title_en) {
        this.put(TITLE_EN,title_en);
    }

    public String getContent_en() {
        return this.getString(CONTENT_EN,"");
    }

    public void setContent_en(String content_en) {
        this.put(CONTENT_EN,content_en);
    }

    public String getDelflg() {
        return this.getString(DELFLG, "");
    }

    public void setDelflg(String delflg) {
        this.put(DELFLG, delflg);
    }

    public String getActflg() {
        return this.getString(ACTFLG, "");
    }

    public void setActflg(String actflg) {
        this.put(ACTFLG, actflg);
    }

    public String getModby() {
        return getString(MODBY, "");
    }

    public void setModby(String modby) {
        this.put(MODBY, modby);
    }

    public Date getModAt() {
        return (Date) getDate(MODAT);
    }

    public void setModAt(Date modAt){
        this.put(MODAT, modAt);
    }

    public String getCreby() {
        return getString(CREBY, "");
    }

    public void setCreby(String creby) {
        this.put(CREBY, creby);
    }

    public Date getCreat() {
        return (Date) getDate(CREAT, null);
    }

    public void setCreat(Date creat) {
        this.put(CREAT, creat);
    }

    public NewsEntity(){

    }

}