package com.model.hos;

import com.mongodb.BasicDBObject;

import java.math.BigDecimal;
import java.sql.Date;

public class Form13aEntity extends BasicDBObject {
    public final String ID="id";

    public final String UZLEG_HIIH = "uzleg_hiih";
    public final String HEMJIH_NEGJ = "hemjih_negj";
    public final String TOO_HEMJEE = "too_hemjee";
    public final String DEEJIIN_TOO = "deejiin_too";
    public final String SHINJILGEE_HIISEN = "shinjilgee_hiisen";
    public final String SHINJILSEN_ARGA = "shinjilsen_arga";
    public final String HERGTSEEND_TOHIRSON = "hergtseend_tohirson";
    public final String USTGASAN = "ustgasan";
    public final String HALDBARGUIJUULELT = "haldbarguijuulelt";
    public final String BRUTSELLYOZ = "brutsellyoz";
    public final String MASTIT = "mastit";
    public final String TRIXINELL = "trixinell";
    public final String FINNOZ = "finnoz";
    public final String SURIYEE = "suriyee";
    public final String BOOM = "boom";
    public final String BUSAD = "busad";
    public final String BUGD = "bugd";
    public final String DEL_FLG = "del_flg";
    public final String CRE_BY = "cre_by";
    public final String CRE_AT = "cre_at";
    public final String MOD_BY = "mod_by";
    public final String MOD_AT = "mod_at";
    public final String NO_DATA = "no_data";
    public final String RECORD_DATE = "record_date";
    public final String SEARCH_RECORD_DATE = "searchRecordDate";
    public final String CITYCODE = "citycode";
    public final String SUMCODE = "sumcode";
    public final String BAG_CODE = "bag_code";
    public final String CITYNAME = "cityname";
    public final String SUMNAME = "sumname";

    public Long getId() {
        return this.getLong(ID,0);
    }

    public void setId(Long id) {
        this.put(ID,id);
    }

    public String getUzleg_hiih() {
        return this.getString(UZLEG_HIIH, "");
    }

    public void setUzleg_hiih(String uzleg_hiih) {
        this.put(UZLEG_HIIH, uzleg_hiih);
    }

    public String getHemjih_negj() {
        return this.getString(HEMJIH_NEGJ, "");
    }

    public void setHemjih_negj(String hemjih_negj) {
        this.put(HEMJIH_NEGJ, hemjih_negj);
    }

    public String getToo_hemjee() {
        return this.getString(TOO_HEMJEE, "");
    }

    public void setToo_hemjee(String too_hemjee) {
        this.put(TOO_HEMJEE, too_hemjee);
    }

    public String getDeejiin_too() {
        return this.getString(DEEJIIN_TOO, "");
    }

    public void setDeejiin_too(String deejiin_too) {
        this.put(DEEJIIN_TOO, deejiin_too);
    }

    public String getShinjilgee_hiisen() {
        return this.getString(SHINJILGEE_HIISEN, "");
    }

    public void setShinjilgee_hiisen(String shinjilgee_hiisen) {
        this.put(SHINJILGEE_HIISEN, shinjilgee_hiisen);
    }

    public String getShinjilsen_arga() {
        return this.getString(SHINJILSEN_ARGA, "");
    }

    public void setShinjilsen_arga(String shinjilsen_arga) {
        this.put(SHINJILSEN_ARGA, shinjilsen_arga);
    }

    public Double getHergtseend_tohirson() {
        return this.getDouble(HERGTSEEND_TOHIRSON, 0);
    }

    public void setHergtseend_tohirson(Double hergtseend_tohirson) {
        this.put(HERGTSEEND_TOHIRSON, hergtseend_tohirson);
    }

    public Double getUstgasan() {
        return this.getDouble(USTGASAN, 0);
    }

    public void setUstgasan(Double ustgasan) {
        this.put(USTGASAN, ustgasan);
    }

    public Double getHaldbarguijuulelt() {
        return this.getDouble(HALDBARGUIJUULELT, 0);
    }

    public void setHaldbarguijuulelt(Double haldbarguijuulelt) {
        this.put(HALDBARGUIJUULELT, haldbarguijuulelt);
    }

    public String getBrutsellyoz() {
        return this.getString(BRUTSELLYOZ, "");
    }

    public void setBrutsellyoz(String brutsellyoz) {
        this.put(BRUTSELLYOZ, brutsellyoz);
    }

    public String getMastit() {
        return this.getString(MASTIT, "");
    }

    public void setMastit(String mastit) {
        this.put(MASTIT, mastit);
    }

    public String getTrixinell() {
        return this.getString(TRIXINELL, "");
    }

    public void setTrixinell(String trixinell) {
        this.put(TRIXINELL, trixinell);
    }

    public String getFinnoz() {
        return this.getString(FINNOZ, "");
    }

    public void setFinnoz(String finnoz) {
        this.put(FINNOZ, finnoz);
    }

    public String getSuriyee() {
        return this.getString(SURIYEE, "");
    }

    public void setSuriyee(String suriyee) {
        this.put(SURIYEE, suriyee);
    }

    public String getBoom() {
        return this.getString(BOOM, "");
    }

    public void setBoom(String boom) {
        this.put(BOOM, boom);
    }

    public String getBusad() {
        return this.getString(BUSAD, "");
    }

    public void setBusad(String busad) {
        this.put(BUSAD, busad);
    }

    public String getBugd() {
        return this.getString(BUGD, "");
    }

    public void setBugd(String bugd) {
        this.put(BUGD, bugd);
    }

    public String getDel_flg() {
        return this.getString(DEL_FLG, "");
    }

    public void setDel_flg(String del_flg) {
        this.put(DEL_FLG, del_flg);
    }

    public String getCre_by() {
        return this.getString(CRE_BY, "");
    }

    public void setCre_by(String cre_by) {
        this.put(CRE_BY, cre_by);
    }

    public Date getCre_at() {
        return (Date) this.getDate(CRE_AT);
    }

    public void setCre_at(Date cre_at) {
        this.put(CRE_AT, cre_at);
    }

    public String getMod_by() {
        return this.getString(MOD_BY, "");
    }

    public void setMod_by(String mod_by) {
        this.put(MOD_BY, mod_by);
    }

    public Date getMod_at() {
        return (Date) this.getDate(MOD_AT);
    }

    public void setMod_at(Date mod_at) {
        this.put(MOD_AT, mod_at);
    }

    public String getNo_data() {
        return this.getString(NO_DATA, "");
    }

    public void setNo_data(String no_data) {
        this.put(NO_DATA, no_data);
    }

    public String getCitycode() {
        return this.getString(CITYCODE, "");
    }

    public void setCitycode(String citycode) {
        this.put(CITYCODE, citycode);
    }

    public String getSumcode() {
        return this.getString(SUMCODE, "");
    }

    public void setSumcode(String sumcode) {
        this.put(SUMCODE, sumcode);
    }

    public String getBag_code() {
        return this.getString(BAG_CODE, "");
    }

    public void setBag_code(String bag_code) {
        this.put(BAG_CODE, bag_code);
    }

    public Date getRecordDate() {
        return (Date) this.getDate(RECORD_DATE, null);
    }

    public void setRecordDate(Date recordDate) {
        this.put(RECORD_DATE, recordDate);
    }

    public Date getSearchRecordDate() {
        return (Date) this.getDate(SEARCH_RECORD_DATE, null);
    }

    public void setSearchRecordDate(Date recordDate) {
        this.put(SEARCH_RECORD_DATE, recordDate);
    }

    public Form13aEntity(){

    }

    public void setCityName(String cityName) {
        put(CITYNAME, cityName);
    }

    public String getCityName(){
        return this.getString(CITYNAME, "");
    }

    public void setSumName(String sumName) {
        put(SUMNAME, sumName);
    }

    public String getSumName(){
        return this.getString(SUMNAME, "");
    }
}
