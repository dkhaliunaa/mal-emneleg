package com.model.hos;

import com.mongodb.BasicDBObject;

import java.sql.Date;

public class TemtsehArgaHemEntity extends BasicDBObject {

    public final String ID = "id";
	public final String FORM01KEY = "form01key";
	public final String CITYCODE = "citycode";
	public final String SUMCODE = "sumcode";
	public final String BAGCODE = "bagcode";
	public final String MALCHINNER = "malchinner";
	public final String BAKCINOGNOO = "bakcinOgnoo";
	public final String BAKCIN_UUSEL = "bakcinUusel";
    public final String HEDEN_MAL_UVCHILSON = "hedenMalUvchilson";
	public final String HAMRAGDSAN_MAL_TOO = "hamragdsanMalToo";
	public final String UVCH_ILERSEN_ESEH = "uvchinIlersenEseh";
	public final String HED_HO_DARAA = "hedHoDaraaUvch";
	public final String BAKCINJUULALT_OGNOO = "bakciinjuulaltiinOgnoo";
	public final String MAL_EMCH_NEGJ_NER = "malEmchNegjNer";
	public final String HALD_HIISEN_ESEH = "haldHiisenEseh";
	public final String HEDEN_UDAA = "hedenUdaa";
	public final String YMAR_BODIS = "ymarBodis";
	public final String YMAR_ARGA = "ymarArgaar";
	public final String TALBAI = "talbai";
	public final String ZAR_BODIS = "zarBodis";
	public final String ECSN_HALD_OGNOO = "ecsnHaldOgnoo";
	public final String MMNDEMUZTANHESEH = "mmndemuztanheseh";
	public final String MUVCHTMHESEH = "muvchtmheseh";
	public final String GAM_TARAASA_ESEH = "garAvlagaMatTaraasanEseh";
	public final String ZUV_SERMA_UGSUN_ESEH = "zuvSerMaUgsunEseh";
	public final String ON_MASH_HUD_ON_GAR_ESEH = "onMaShHudONgargasanEseh";
	public final String BUSAD_SUM_MED_ESEH = "busdSumMedEseh";
	public final String UVCH_BURT_TAL_MED_ESEH = "uvchinBurtTalMedEseh";
	public final String RECORD_DATE = "recordDate";
	public final String DELFLG = "delflg";
	public final String ACTFLG = "actflg";
	public final String CRE_AT = "cre_at";
	public final String CRE_BY = "cre_by";
	public final String MOD_AT = "mod_at";
	public final String MOD_BY = "mod_by";

    public Long getId() {
        return this.getLong(ID, 0);
    }

    public void setId(Long id) {
        this.put(ID, id);
    }

    public String getForm01key() {
        return this.getString(FORM01KEY, "");
    }

    public void setForm01key(String form01key) {
        this.put(FORM01KEY, form01key);
    }

    public String getCitycode() {
        return this.getString(CITYCODE, "");
    }

    public void setCitycode(String citycode) {
        this.put(CITYCODE, citycode);
    }

    public String getSumcode() {
        return this.getString(SUMCODE, "");
    }

    public void setSumcode(String sumcode) {
        this.put(SUMCODE, sumcode);
    }

    public String getBagcode() {
        return this.getString(BAGCODE, "");
    }

    public void setBagcode(String bagcode) {
        this.put(BAGCODE, bagcode);
    }

    public String getMalchinner() {
        return this.getString(MALCHINNER, "");
    }

    public void setMalchinner(String malchinner) {
        this.put(MALCHINNER, malchinner);
    }

    public Date getBakcinOgnoo() {
        return (Date) this.getDate(BAKCINOGNOO);
    }

    public void setBakcinOgnoo(Date bakcinOgnoo) {
        this.put(BAKCINOGNOO, bakcinOgnoo);
    }

    public String getBakcin_uusel() {
        return this.getString(BAKCIN_UUSEL, "");
    }

    public void setBakcin_uusel(String bakcin_uusel) {
        this.put(BAKCIN_UUSEL, bakcin_uusel);
    }

    public String getHedenMalUvchilson() {
        return this.getString(HEDEN_MAL_UVCHILSON, "");
    }

    public void setHedenMalUvchilson(String bakcin_uusel) {
        this.put(HEDEN_MAL_UVCHILSON, bakcin_uusel);
    }

    public String getHamragdsan_mal_too() {
        return this.getString(HAMRAGDSAN_MAL_TOO, "");
    }

    public void setHamragdsan_mal_too(String hamragdsan_mal_too) {
        this.put(HAMRAGDSAN_MAL_TOO, hamragdsan_mal_too);
    }

    public String getUvch_ilersen_eseh() {
        return this.getString(UVCH_ILERSEN_ESEH, "");
    }

    public void setUvch_ilersen_eseh(String uvch_ilersen_eseh) {
        this.put(UVCH_ILERSEN_ESEH, uvch_ilersen_eseh);
    }

    public String getHed_ho_daraa() {
        return this.getString(HED_HO_DARAA, "");
    }

    public void setHed_ho_daraa(String hed_ho_daraa) {
        this.put(HED_HO_DARAA, hed_ho_daraa);
    }

    public String getBakcinjuulalt_ognoo() {
        return this.getString(BAKCINJUULALT_OGNOO, "");
    }

    public void setBakcinjuulalt_ognoo(String bakcinjuulalt_ognoo) {
        this.put(BAKCINJUULALT_OGNOO, bakcinjuulalt_ognoo);
    }

    public String getMal_emch_negj_ner() {
        return this.getString(MAL_EMCH_NEGJ_NER, "");
    }

    public void setMal_emch_negj_ner(String mal_emch_negj_ner) {
        this.put(MAL_EMCH_NEGJ_NER, mal_emch_negj_ner);
    }

    public String getHald_hiisen_eseh() {
        return this.getString(HALD_HIISEN_ESEH, "");
    }

    public void setHald_hiisen_eseh(String hald_hiisen_eseh) {
        this.put(HALD_HIISEN_ESEH, hald_hiisen_eseh);
    }

    public int getHeden_udaa() {
        return this.getInt(HEDEN_UDAA, 0);
    }

    public void setHeden_udaa(int heden_udaa) {
        this.put(HEDEN_UDAA, heden_udaa);
    }

    public String getYmar_bodis() {
        return this.getString(YMAR_BODIS, "");
    }

    public void setYmar_bodis(String ymar_bodis) {
        this.put(YMAR_BODIS, ymar_bodis);
    }

    public String getYmar_arga() {
        return this.getString(YMAR_ARGA, "");
    }

    public void setYmar_arga(String ymar_arga) {
        this.put(YMAR_ARGA, ymar_arga);
    }

    public String getTalbai() {
        return this.getString(TALBAI, "");
    }

    public void setTalbai(String talbai) {
        this.put(TALBAI, talbai);
    }

    public String getZar_bodis() {
        return this.getString(ZAR_BODIS, "");
    }

    public void setZar_bodis(String zar_bodis) {
        this.put(ZAR_BODIS, zar_bodis);
    }

    public Date getEcsn_hald_ognoo() {
        return (Date) this.getDate(ECSN_HALD_OGNOO);
    }

    public void setEcsn_hald_ognoo(Date ecsn_hald_ognoo) {
        this.put(ECSN_HALD_OGNOO, ecsn_hald_ognoo);
    }

    public String getMmndemuztanheseh() {
        return this.getString(MMNDEMUZTANHESEH, "");
    }

    public void setMmndemuztanheseh(String mmndemuztanheseh) {
        this.put(MMNDEMUZTANHESEH, mmndemuztanheseh);
    }

    public String getMuvchtmheseh() {
        return this.getString(MUVCHTMHESEH, "");
    }

    public void setMuvchtmheseh(String muvchtmheseh) {
        this.put(MUVCHTMHESEH, muvchtmheseh);
    }

    public String getGam_taraasa_eseh() {
        return this.getString(GAM_TARAASA_ESEH, "");
    }

    public void setGam_taraasa_eseh(String gam_taraasa_eseh) {
        this.put(GAM_TARAASA_ESEH, gam_taraasa_eseh);
    }

    public String getZuv_serma_ugsun_eseh() {
        return this.getString(ZUV_SERMA_UGSUN_ESEH, "");
    }

    public void setZuv_serma_ugsun_eseh(String zuv_serma_ugsun_eseh) {
        this.put(ZUV_SERMA_UGSUN_ESEH, zuv_serma_ugsun_eseh);
    }

    public String getOn_mash_hud_on_gar_eseh() {
        return this.getString(ON_MASH_HUD_ON_GAR_ESEH, "");
    }

    public void setOn_mash_hud_on_gar_eseh(String on_mash_hud_on_gar_eseh) {
        this.put(ON_MASH_HUD_ON_GAR_ESEH, on_mash_hud_on_gar_eseh);
    }

    public String getBusad_sum_med_eseh() {
        return this.getString(BUSAD_SUM_MED_ESEH, "");
    }

    public void setBusad_sum_med_eseh(String busad_sum_med_eseh) {
        this.put(BUSAD_SUM_MED_ESEH, busad_sum_med_eseh);
    }

    public String getUvch_burt_tal_med_eseh() {
        return this.getString(UVCH_BURT_TAL_MED_ESEH, "");
    }

    public void setUvch_burt_tal_med_eseh(String uvch_burt_tal_med_eseh) {
        this.put(UVCH_BURT_TAL_MED_ESEH, uvch_burt_tal_med_eseh);
    }

    public Date getRecord_date() {
        return (Date) this.getDate(RECORD_DATE);
    }

    public void setRecord_date(Date record_date) {
        this.put(RECORD_DATE, record_date);
    }

    public String getDelflg() {
        return this.getString(DELFLG, "");
    }

    public void setDelflg(String delflg) {
        this.put(DELFLG, delflg);
    }

    public String getActflg() {
        return this.getString(ACTFLG, "");
    }

    public void setActflg(String actflg) {
        this.put(ACTFLG, actflg);
    }

    public Date getCre_at() {
        return (Date) this.getDate(CRE_AT);
    }

    public void setCre_at(Date cre_at) {
        this.put(CRE_AT, cre_at);
    }

    public String getCre_by() {
        return this.getString(CRE_BY, "");
    }

    public void setCre_by(String cre_by) {
        this.put(CRE_BY, cre_by);
    }

    public Date getMod_at() {
        return (Date) this.getDate(MOD_AT);
    }

    public void setMod_at(Date mod_at) {
        this.put(MOD_AT, mod_at);
    }

    public String getMod_by() {
        return this.getString(MOD_BY, "");
    }

    public void setMod_by(String mod_by) {
        this.put(MOD_BY, mod_by);
    }
}
