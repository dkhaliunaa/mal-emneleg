package com.model.hos;

import com.mongodb.BasicDBObject;

import java.sql.Date;
import java.util.List;

public class SuregMedeeFormEntity extends BasicDBObject {

    public final String ID = "id";
    public final String FORM01KEY = "frmkey";
    public final String CITYCODE = "citycode";
    public final String SUMCODE = "sumcode";
    public final String BAGCODE = "bagcode";
    public final String MALCHINNER = "malchinner";
    public final String DELFLG = "delflg";
    public final String ACTFLG = "actflg";
    public final String CRE_AT = "cre_at";
    public final String CRE_BY = "cre_by";
    public final String MOD_AT = "mod_at";
    public final String MOD_BY = "mod_by";
    public final String RECORD_DATE = "recordDate";
    public final String SUREG_KEY = "sureg_key";
    public final String STR1 = "str1";
    public final String STR2 = "str2";

    public final String MEDREMTGII_DATA_LIST = "medremtgiidata";
    public final String UVCHILSON_MAL_LIST = "uvchilsondata";
    public final String USTGASAN_LIST = "ustgasanmedeelel";
    public final String UHSEN_LIST = "uhsenmedeelel";

    public Long getId() {
        return this.getLong(ID, 0);
    }

    public void setId(Long id) {
        this.put(ID, id);
    }

    public String getForm01key() {
        return this.getString(FORM01KEY, "");
    }

    public void setForm01key(String form01key) {
        this.put(FORM01KEY, form01key);
    }

    public String getCitycode() {
        return this.getString(CITYCODE, "");
    }

    public void setCitycode(String citycode) {
        this.put(CITYCODE, citycode);
    }

    public String getSumcode() {
        return this.getString(SUMCODE, "");
    }

    public void setSumcode(String sumcode) {
        this.put(SUMCODE, sumcode);
    }

    public String getBagcode() {
        return this.getString(BAGCODE, "");
    }

    public void setBagcode(String bagcode) {
        this.put(BAGCODE, bagcode);
    }

    public String getMalchinner() {
        return this.getString(MALCHINNER, "");
    }

    public void setMalchinner(String malchinner) {
        this.put(MALCHINNER, malchinner);
    }

    public String getDelflg() {
        return this.getString(DELFLG, "");
    }

    public void setDelflg(String delflg) {
        this.put(DELFLG, delflg);
    }

    public String getActflg() {
        return this.getString(ACTFLG, "");
    }

    public void setActflg(String actflg) {
        this.put(ACTFLG, actflg);
    }

    public Date getCre_at() {
        return (Date) this.getDate(CRE_AT);
    }

    public void setCre_at(Date cre_at) {
        this.put(CRE_AT, cre_at);
    }

    public String getCre_by() {
        return this.getString(CRE_BY, "");
    }

    public void setCre_by(String cre_by) {
        this.put(CRE_BY, cre_by);
    }

    public Date getMod_at() {
        return (Date) this.getDate(MOD_AT);
    }

    public void setMod_at(Date mod_at) {
        this.put(MOD_AT, mod_at);
    }

    public String getMod_by() {
        return this.getString(MOD_BY, "");
    }

    public void setMod_by(String mod_by) {
        this.put(MOD_BY, mod_by);
    }

    public Date getRecord_date() {
        return (Date) this.getDate(RECORD_DATE);
    }

    public void setRecord_date(Date record_date) {
        this.put(RECORD_DATE, record_date);
    }

    public String getSureg_key() {
        return this.getString(SUREG_KEY, "");
    }

    public void setSureg_key(String sureg_key) {
        this.put(SUREG_KEY, sureg_key);
    }

    public String getStr1() {
        return this.getString(STR1, "");
    }

    public void setStr1(String str1) {
        this.put(STR1, str1);
    }

    public String getStr2() {
        return this.getString(STR2, "");
    }

    public void setStr2(String str2) {
        this.put(STR2, str2);
    }

    public List<MedremtgiiDataEntity> getMedremtgiiDataList() {
        return (List<MedremtgiiDataEntity>) this.get(MEDREMTGII_DATA_LIST);
    }

    public void setMedremtgiiDataList(List<MedremtgiiDataEntity> medremtgiiDataList) {
        this.put(MEDREMTGII_DATA_LIST, medremtgiiDataList);
    }

    public List<MedremtgiiDataEntity> getUvchilsonMalList() {
        return (List<MedremtgiiDataEntity>) this.get(UVCHILSON_MAL_LIST);
    }

    public void setUvchilsonMalList(List<MedremtgiiDataEntity> uvchilsonMalList) {
        this.put(UVCHILSON_MAL_LIST, uvchilsonMalList);
    }

    public List<MedremtgiiDataEntity> getUstgasanList() {
        return (List<MedremtgiiDataEntity>) this.get(USTGASAN_LIST);
    }

    public void setUstgasanList(List<MedremtgiiDataEntity> ustgasanList) {
        this.put(USTGASAN_LIST, ustgasanList);
    }

    public List<MedremtgiiDataEntity> getUhsenList() {
        return (List<MedremtgiiDataEntity>) this.get(UHSEN_LIST);
    }

    public void setUhsenList(List<MedremtgiiDataEntity> uhsenList) {
        this.put(UHSEN_LIST, uhsenList);
    }
}
