package com.model.hos;

/**
 * Created by 24be10264 on 9/5/2017.
 */

import com.mongodb.BasicDBObject;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public class Form51Entity extends BasicDBObject {
    public static final String ID = "id";
    public static final String SUMBAGNER = "sumBagNer";
    public final String AIMAGNER="aimagNer";
    public final String SUMNER="sumNer";
    public static final String MURIINDUGAAR = "muriinDugaar";
    public static final String NIITMALTOO = "niitMalToo";
    public static final String BOGTOO = "bogToo";
    public static final String BODTOO = "bodToo";
    public static final String TOLTOO = "tolToo";
    public static final String EHNIIULDEGDEL = "ehniiUldegdel";
    public static final String ORLOGO = "orlogo";
    public static final String ZARLAGA = "zarlaga";
    public static final String JILEULDEGDEL = "jilEUldegdel";
    public final String AIMAG = "aimag";
    public final String SUM = "sum";

    public static final String SUMBAGNER_EN = "sumBagNer_en";
    public final String AIMAG_EN = "aimag_en";
    public final String SUM_EN = "sum_en";

    public final String DELFLG = "delFlg";
    public final String ACTFLG = "actFlg";
    public final String MODBY = "modBy";
    public final String MODAT = "modAt";
    public final String CREBY = "creBy";
    public final String CREAT = "creAt";
    public final String RECORD_DATE = "recordDate";
    public final String SEARCH_RECORD_DATE = "searchRecordDate";



    public Form51Entity() {

    }

    public Long getId() {
        return this.getLong(ID, 0);
    }

    public void setId(Long id) {
        this.put(ID, id);
    }

    public String getAimagNer() {
        return this.getString(AIMAGNER,"");
    }

    public void setAimagNer(String aimagNer) {
        this.put(AIMAGNER,aimagNer);
    }

    public String getSumNer() {
        return this.getString(SUMNER,"");
    }

    public void setSumNer(String sumNer) {
        this.put(SUMNER,sumNer);
    }

    public String getAimag() {
        return this.getString(AIMAG);
    }

    public void setAimag(String aimag) {
        this.put(AIMAG, aimag);
    }

    public String getSum() {
        return this.getString(SUM, "");
    }

    public void setSum(String sum) {
        this.put(SUM, sum);
    }

    public String getAimag_en() {
        return this.getString(AIMAG_EN, "");
    }

    public void setAimag_en(String aimag_en) {
        this.put(AIMAG_EN, aimag_en);
    }

    public String getSum_en() {
        return this.getString(SUM_EN, "");
    }

    public void setSum_en(String sum_en) {
        this.put(SUM_EN, sum_en);
    }

    public String getSumBagNer() {
        return this.getString(SUMBAGNER, "");
    }

    public void setSumBagNer(String sumBagNer) {
        this.put(SUMBAGNER, sumBagNer);
    }

    public Long getMuriinDugaar() {
        return this.getLong(MURIINDUGAAR, 0);
    }

    public void setMuriinDugaar(Long muriinDugaar) {
        this.put(MURIINDUGAAR, muriinDugaar);
    }

    public Long getNiitMalToo() {
        return this.getLong(NIITMALTOO, 0);
    }

    public void setNiitMalToo(Long niitMalToo) {
        this.put(NIITMALTOO, niitMalToo);
    }

    public Long getBogToo() {
        return this.getLong(BOGTOO, 0);
    }

    public void setBogToo(Long bogToo) {
        this.put(BOGTOO, bogToo);
    }

    public Long getBodToo() {
        return this.getLong(BODTOO, 0);
    }

    public void setBodToo(Long bodToo) {
        this.put(BODTOO, bodToo);
    }

    public Long getTolToo() {
        return this.getLong(TOLTOO, 0);
    }

    public void setTolToo(Long tolToo) {
        this.put(TOLTOO, tolToo);
    }

    public BigDecimal getEhniiUldegdel() {
        return BigDecimal.valueOf(this.getDouble(EHNIIULDEGDEL, 0));
    }

    public void setEhniiUldegdel(BigDecimal ehniiUldegdel) {
        this.put(EHNIIULDEGDEL, ehniiUldegdel);
    }

    public BigDecimal getOrlogo() {
        return BigDecimal.valueOf(this.getDouble(ORLOGO, 0));
    }

    public void setOrlogo(BigDecimal orlogo) {
        this.put(ORLOGO, orlogo);
    }

    public BigDecimal getJilEUldegdel() {
        return BigDecimal.valueOf(this.getDouble(JILEULDEGDEL, 0));
    }

    public void setJilEUldegdel(BigDecimal jilEUldegdel) {
        this.put(JILEULDEGDEL, jilEUldegdel);
    }

    public String getSumBagNer_en() {
        return this.getString(SUMBAGNER_EN, "");
    }

    public void setSumBagNer_en(String sumBagNer_en) {
        this.put(SUMBAGNER_EN, sumBagNer_en);
    }

    public BigDecimal getZarlaga() {
        return BigDecimal.valueOf(this.getDouble(ZARLAGA, 0));
    }

    public void setZarlaga(BigDecimal zarlaga) {
        this.put(ZARLAGA, zarlaga);
    }

    public String getDelflg() {
        return this.getString(DELFLG, "");
    }

    public void setDelflg(String delflg) {
        this.put(DELFLG, delflg);
    }

    public String getActflg() {
        return this.getString(ACTFLG, "");
    }

    public void setActflg(String actflg) {
        this.put(ACTFLG, actflg);
    }

    public String getModby() {
        return getString(MODBY, "");
    }

    public void setModby(String modby) {
        this.put(MODBY, modby);
    }

    public Date getModAt() {
        return (Date) getDate(MODAT);
    }

    public void setModAt(Date modAt){
        this.put(MODAT, modAt);
    }

    public String getCreby() {
        return getString(CREBY, "");
    }

    public void setCreby(String creby) {
        this.put(CREBY, creby);
    }

    public Date getCreat() {
        return (Date) getDate(CREAT, null);
    }

    public void setCreat(Date creat) {
        this.put(CREAT, creat);
    }

    public Date getRecordDate() {
        return (Date) this.getDate(RECORD_DATE, null);
    }

    public void setRecordDate(Date recordDate) {
        this.put(RECORD_DATE, recordDate);
    }

    public Date getSearchRecordDate() {
        return (Date) this.getDate(SEARCH_RECORD_DATE, null);
    }

    public void setSearchRecordDate(Date recordDate) {
        this.put(SEARCH_RECORD_DATE, recordDate);
    }
}
