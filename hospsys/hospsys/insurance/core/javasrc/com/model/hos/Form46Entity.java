package com.model.hos;

import com.mongodb.BasicDBObject;

import java.math.BigDecimal;
import java.sql.Date;

public class Form46Entity extends BasicDBObject {
    public final String ID = "id";
    public final String PIE = "pie";
    public final String AIMAG = "aimag";
    public final String SUM = "sum";
    public final String BAGHOROONER = "bagHorooNer_mn";
    public final String MURDUGAAR = "murDugaar_mn";
    public final String UHERNIITTOO = "uherNiitToo_mn";
    public final String TUGALTOO = "tugalToo_mn";
    public final String BUSADTOO = "busadToo_mn";
    public final String GUURTAINIITTOO = "guurtaiNiitToo_mn";
    public final String GUURTAITUGALTOO = "guurtaiTugalToo_mn";
    public final String GUURTAIBUSADTOO = "guurtaiBusadToo_mn";
    public final String DUNDAJHUVI = "dundajHuvi_mn";
    public final String TUGALHUVI = "tugalHuvi_mn";
    public final String BUSADHUVI = "busadHuvi_mn";
    public final String AIMAGNER="aimagNer";
    public final String SUMNER="sumNer";

    public final String AIMAG_EN = "aimag_en";
    public final String SUM_EN = "sum_en";
    public final String BAGHOROONER_EN = "bagHorooNer_en";
    public final String MURDUGAAR_EN = "murDugaar_en";
    public final String UHERNIITTOO_EN = "uherNiitToo_en";
    public final String TUGALTOO_EN = "tugalToo_en";
    public final String BUSADTOO_EN = "busadToo_en";
    public final String GUURTAINIITTOO_EN = "guurtaiNiitToo_en";
    public final String GUURTAITUGALTOO_EN = "guurtaiTugalToo_en";
    public final String GUURTAIBUSADTOO_EN = "guurtaiBusadToo_en";
    public final String DUNDAJHUVI_EN = "dundajHuvi_en";
    public final String TUGALHUVI_EN = "tugalHuvi_en";
    public final String BUSADHUVI_EN = "busadHuvi_en";

    public final String DELFLG = "delFlg";
    public final String ACTFLG = "actFlg";
    public final String MODBY = "modBy";
    public final String MODAT = "modAt";
    public final String CREBY = "creBy";
    public final String CREAT = "creAt";
    public final String RECORD_DATE = "recordDate";
    public final String SEARCH_RECORD_DATE = "searchRecordDate";

    public Form46Entity() {

    }

    public Long getId() {
        return this.getLong(ID, 0);
    }

    public void setId(Long id) {
        this.put(ID, id);
    }

    public String getBagHorooNer() {
        return this.getString(BAGHOROONER, "");
    }

    public void setBagHorooNer(String bagHorooNer) {
        this.put(BAGHOROONER, bagHorooNer);
    }

    public String getMurDugaar() {
        return this.getString(MURDUGAAR, "");
    }

    public void setMurDugaar(String murDugaar) {
        this.put(MURDUGAAR, murDugaar);
    }

    public String getUherNiitToo() {
        return this.getString(UHERNIITTOO,"");
    }

    public void setUherNiitToo(String uherNiitToo) {
        this.put(UHERNIITTOO, uherNiitToo);
    }

    public Long getTugalToo() {
        return this.getLong(TUGALTOO, 0);
    }

    public void setTugalToo(Long tugalToo_mn) {
        this.put(TUGALTOO, tugalToo_mn);
    }

    public Long getBusadToo() {
        return this.getLong(BUSADTOO, 0);
    }

    public void setBusadToo(Long busadToo) {
        this.put(BUSADTOO, busadToo);
    }

    public String getGuurtaiNiitToo() {
        return this.getString(GUURTAINIITTOO, "");
    }

    public void setGuurtaiNiitToo(String guurtaiNiitToo) {
        this.put(GUURTAINIITTOO, guurtaiNiitToo);
    }

    public Long getGuurtaiTugalToo() {
        return this.getLong(GUURTAITUGALTOO, 0);
    }

    public void setGuurtaiTugalToo(Long guurtaiTugalToo) {
        this.put(GUURTAITUGALTOO, guurtaiTugalToo);
    }

    public Long getGuurtaiBusadToo() {
        return this.getLong(GUURTAIBUSADTOO, 0);
    }

    public void setGuurtaiBusadToo(Long guurtaiBusadToo) {
        this.put(GUURTAIBUSADTOO, guurtaiBusadToo);
    }

    public String getDundajHuvi() {
        return this.getString(DUNDAJHUVI, "");
    }

    public void setDundajHuvi(String dundajHuvi) {
        this.put(DUNDAJHUVI, dundajHuvi);
    }

    public String getTugalHuvi() {
        return this.getString(TUGALHUVI, "");
    }

    public void setTugalHuvi(String tugalHuvi) {
        this.put(TUGALHUVI, tugalHuvi);
    }

    public String getBusadHuvi() {
        return this.getString(BUSADHUVI, "");
    }

    public void setBusadHuvi(String busadHuvi) {
        this.put(BUSADHUVI, busadHuvi);
    }

    public String getBagHorooNer_en() {
        return this.getString(BAGHOROONER_EN, "");
    }

    public void setBagHorooNer_en(String bagHorooNer_en) {
        this.put(BAGHOROONER_EN, bagHorooNer_en);
    }

    public String getMurDugaar_en() {
        return this.getString(MURDUGAAR_EN, "");
    }

    public void setMurDugaar_en(String murDugaar_en) {
        this.put(MURDUGAAR_EN, murDugaar_en);
    }

    public Long getUherNiitToo_en() {
        return this.getLong(UHERNIITTOO_EN, 0);
    }

    public void setUherNiitToo_en(Long uherNiitToo_en) {
        this.put(UHERNIITTOO_EN, uherNiitToo_en);
    }

    public Long getTugalToo_en() {
        return this.getLong(TUGALTOO_EN, 0);
    }

    public void setTugalToo_en(Long tugalToo_en) {
        this.put(TUGALTOO_EN, tugalToo_en);
    }

    public Long getBusadToo_en() {
        return this.getLong(BUSADTOO_EN, 0);
    }

    public void setBusadToo_en(Long busadToo_en) {
        this.put(BUSADTOO_EN, busadToo_en);
    }

    public Long getGuurtaiNiitToo_en() {
        return this.getLong(GUURTAINIITTOO_EN, 0);
    }

    public void setGuurtaiNiitToo_en(Long guurtaiNiitToo_en) {
        this.put(GUURTAINIITTOO_EN, guurtaiNiitToo_en);
    }

    public Long getGuurtaiTugalToo_en() {
        return this.getLong(GUURTAITUGALTOO_EN, 0);
    }

    public void setGuurtaiTugalToo_en(Long guurtaiTugalToo_en) {
        this.put(GUURTAITUGALTOO_EN, guurtaiTugalToo_en);
    }

    public Long getGuurtaiBusadToo_en() {
        return this.getLong(GUURTAIBUSADTOO_EN, 0);
    }

    public void setGuurtaiBusadToo_en(Long guurtaiBusadToo_en) {
        this.put(GUURTAIBUSADTOO_EN, guurtaiBusadToo_en);
    }

    public BigDecimal getDundajHuvi_en() {
        return BigDecimal.valueOf(this.getDouble(DUNDAJHUVI_EN, 0));
    }

    public void setDundajHuvi_en(BigDecimal dundajHuvi_en) {
        this.put(DUNDAJHUVI_EN, dundajHuvi_en);
    }

    public String getTugalHuvi_en() {
        return this.getString(TUGALHUVI_EN, "0");
    }

    public void setTugalHuvi_en(String tugalHuvi_en) {
        this.put(TUGALHUVI_EN, tugalHuvi_en);
    }

    public String getBusadHuvi_en() {
        return this.getString(BUSADHUVI_EN, "0");
    }

    public void setBusadHuvi_en(String busadHuvi_en) {
        this.put(BUSADHUVI_EN, busadHuvi_en);
    }

    public String getAimag() {
        return this.getString(AIMAG);
    }

    public void setAimag(String aimag) {
        this.put(AIMAG, aimag);
    }

    public String getSum() {
        return this.getString(SUM, "");
    }

    public void setSum(String sum) {
        this.put(SUM, sum);
    }

    public String getAimag_en() {
        return this.getString(AIMAG_EN, "");
    }

    public void setAimag_en(String aimag_en) {
        this.put(AIMAG_EN, aimag_en);
    }

    public String getSum_en() {
        return this.getString(SUM_EN, "");
    }

    public void setSum_en(String sum_en) {
        this.put(SUM_EN, sum_en);
    }

    public String getAimagNer() {
        return this.getString(AIMAGNER, "");
    }

    public void setAimagNer(String aimagNer) {
        this.put(AIMAGNER, aimagNer);
    }

    public String getSumNer() {
        return this.getString(SUMNER, "");
    }

    public void setSumNer(String sumNer) {
        this.put(SUMNER, sumNer);
    }

    public String getDelflg() {
        return this.getString(DELFLG, "");
    }

    public void setDelflg(String delflg) {
        this.put(DELFLG, delflg);
    }

    public String getActflg() {
        return this.getString(ACTFLG, "");
    }

    public void setActflg(String actflg) {
        this.put(ACTFLG, actflg);
    }

    public String getModby() {
        return getString(MODBY, "");
    }

    public void setModby(String modby) {
        this.put(MODBY, modby);
    }

    public Date getModAt() {
        return (Date) getDate(MODAT);
    }

    public void setModAt(Date modAt) {
        this.put(MODAT, modAt);
    }

    public String getCreby() {
        return getString(CREBY, "");
    }

    public void setCreby(String creby) {
        this.put(CREBY, creby);
    }

    public Date getCreat() {
        return (Date) getDate(CREAT, null);
    }

    public void setCreat(Date creat) {
        this.put(CREAT, creat);
    }

    public Date getRecordDate() {
        return (Date) this.getDate(RECORD_DATE, null);
    }

    public void setRecordDate(Date recordDate) {
        this.put(RECORD_DATE, recordDate);
    }

    public Date getSearchRecordDate() {
        return (Date) this.getDate(SEARCH_RECORD_DATE, null);
    }

    public void setSearchRecordDate(Date recordDate) {
        this.put(SEARCH_RECORD_DATE, recordDate);
    }
    public String getPie() {
        return getString(PIE, "");
    }

    public void setPie(String creby) {
        this.put(PIE, creby);
    }
}