package com.model.hos;

import com.mongodb.BasicDBObject;

import java.sql.Date;

public class Form09HavsraltEntity extends BasicDBObject {
    public final String ID="id";
    public final String CITYCODE = "citycode";
    public final String SUMCODE = "sumcode";
    public final String BAGCODE = "bagcode";
    public final String GAZARNER = "gazarner";
    public final String URTRAG = "urtrag";
    public final String ORGOROG = "orgorog";
    public final String GOLOMTTOO = "golomttoo";
    public final String OVCHIN_NER = "ovchin_ner";
    public final String MAL_TURUL = "mal_turul";
    public final String UVCHILSON = "uvchilson";
    public final String UHSEN = "uhsen";
    public final String EDGERSEN = "edgersen";

    public final String RECORD_DATE = "recordDate";
    public final String SEARCH_RECORD_DATE = "searchRecordDate";

    public final String DELFLG = "delFlg";
    public final String MODBY = "modBy";
    public final String MODAT = "modAt";
    public final String CREBY = "creBy";
    public final String CREAT = "creAt";

    public final String CITYNAME = "cityname";
    public final String SUM_NAME = "sumname";
    public final String BAG_NAME = "bagname";

    public Long getId() {
        return this.getLong(ID,0);
    }

    public void setId(Long id) {
        this.put(ID,id);
    }


    public String getCitycode() {
        return this.getString(CITYCODE, "");
    }

    public void setCitycode(String citycode) {
        this.put(CITYCODE, citycode);
    }

    public String getSumcode() {
        return this.getString(SUMCODE, "");
    }

    public void setSumcode(String sumcode) {
        this.put(SUMCODE, sumcode);
    }

    public String getBagcode() {
        return this.getString(BAGCODE, "");
    }

    public void setBagcode(String bagcode) {
        this.put(BAGCODE, bagcode);
    }

    public String getGazarner() {
        return this.getString(GAZARNER, "");
    }

    public void setGazarner(String gazarner) {
        this.put(GAZARNER, gazarner);
    }

    public String getUrtrag() {
        return this.getString(URTRAG, "");
    }

    public void setUrtrag(String urtrag) {
        this.put(URTRAG, urtrag);
    }

    public String getOrgorog() {
        return this.getString(ORGOROG, "");
    }

    public void setOrgorog(String orgorog) {
        this.put(ORGOROG, orgorog);
    }

    public int getGolomttoo() {
        return this.getInt(GOLOMTTOO, 0);
    }

    public void setGolomttoo(int golomttoo) {
        this.put(GOLOMTTOO, golomttoo);
    }

    public String getOvchin_ner() {
        return this.getString(OVCHIN_NER, "");
    }

    public void setOvchin_ner(String ovchin_ner) {
        this.put(OVCHIN_NER, ovchin_ner);
    }

    public String getMal_turul() {
        return this.getString(MAL_TURUL, "");
    }

    public void setMal_turul(String mal_turul) {
        this.put(MAL_TURUL, mal_turul);
    }

    public int getUvchilson() {
        return this.getInt(UVCHILSON, 0);
    }

    public void setUvchilson(int uvchilson) {
        this.put(UVCHILSON, uvchilson);
    }

    public int getUhsen() {
        return this.getInt(UHSEN, 0);
    }

    public void setUhsen(int uhsen) {
        this.put(UHSEN, uhsen);
    }

    public int getEdgersen() {
        return this.getInt(EDGERSEN, 0);
    }

    public void setEdgersen(int edgersen) {
        this.put(EDGERSEN, edgersen);
    }

    public String getCityName() {
        return this.getString(CITYNAME, "");
    }

    public void setCityName(String shinjilgee_arga) {
        this.put(CITYNAME, shinjilgee_arga);
    }

    public String getSumName() {
        return this.getString(SUM_NAME, "");
    }

    public void setSumName(String shinjilgee_arga) {
        this.put(SUM_NAME, shinjilgee_arga);
    }

    public String getBagName() {
        return this.getString(BAG_NAME, "");
    }

    public void setBagName(String shinjilgee_arga) {
        this.put(BAG_NAME, shinjilgee_arga);
    }

    public String getDelflg() {
        return this.getString(DELFLG, "");
    }

    public void setDelflg(String delflg) {
        this.put(DELFLG, delflg);
    }

    public String getModby() {
        return getString(MODBY, "");
    }

    public void setModby(String modby) {
        this.put(MODBY, modby);
    }

    public Date getModAt() {
        return (Date) getDate(MODAT);
    }

    public void setModAt(Date modAt){
        this.put(MODAT, modAt);
    }

    public String getCreby() {
        return getString(CREBY, "");
    }

    public void setCreby(String creby) {
        this.put(CREBY, creby);
    }

    public Date getCreat() {
        return (Date) getDate(CREAT, null);
    }

    public void setCreat(Date creat) {
        this.put(CREAT, creat);
    }

    public Date getRecordDate() {
        return (Date) this.getDate(RECORD_DATE, null);
    }

    public void setRecordDate(Date recordDate) {
        this.put(RECORD_DATE, recordDate);
    }

    public Date getSearchRecordDate() {
        return (Date) this.getDate(SEARCH_RECORD_DATE, null);
    }

    public void setSearchRecordDate(Date recordDate) {
        this.put(SEARCH_RECORD_DATE, recordDate);
    }

    public Form09HavsraltEntity(){

    }
}
