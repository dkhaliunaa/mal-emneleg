package com.model.hos;

import com.mongodb.BasicDBObject;

import java.sql.Date;

public class TarkhvarDataEntity extends BasicDBObject {
    public final String ID = "id";
	public final String HFORMKEY = "hformkey";
	public final String CITYCODE = "citycode";
	public final String SUMCODE = "sumcode";
	public final String BAGCODE = "bagcode";
	public final String MALCHINNER = "malchinner";
	public final String BAIGAL_HALDVARIIN = "baigal_haldvariin";
	public final String BAKTSINII = "baktsinii";
	public final String DS_HIISENESEH = "hiiseneseh";
	public final String DS_BAG_GISHUUN_NERS = "bagiinGishuudNers";
	public final String DS_UR_DUN = "garsanUrdun";
	public final String ZUVLOMJ = "idZuvlomj";
	public final String HUCHIN_ZUIL_OGNOO = "huchinzuilortsonhugatsaa";
	public final String URJLIIN_MAL_HUD = "urjliinMalHud";
	public final String BUSAD_MAL_HUD = "busadMalHud";
	public final String BAKCIN_TARGU = "bakcinTariagu";
	public final String OTOR_NUUDEL = "otorNuudel";
	public final String HUN_SH_HOD = "huniiShiljiltHud";
	public final String TEEVER_SH_HOD = "teevriinShiljHud";
	public final String TABIUL_MAL_AVSAN = "tabiulMalAvsan";
	public final String BELCHEER_NEG = "belcheerNeg";
	public final String HUDAG_US = "hudagUs";
	public final String BUSAD_HUCH_ZUIL = "busadHuchinZuil";
	public final String SHALTGAAN_TODGU = "shaltgaanTodgu";
	public final String MALCHIN_UURUU = "malchinUuruu";
	public final String SAHALT_AIL = "sahaltAil";
	public final String MAL_EMNELEG = "malEmneleg";
	public final String UZLEG_TANDALT = "uzlegTandalt";
	public final String TANDALT_SHINJILGEE = "tandaltShinjilgee";
	public final String BUSAD = "busad";
	public final String TUSGAARLASAN_ESEH = "tusgaarlasaneseh";
	public final String TUSGAARLASAN_OGNOO = "tusgaarlasanognoo";
	public final String TUS_HOROO_BAIRLAL = "tushoroobairlal";
	public final String DELFLG = "delflg";
	public final String ACTFLG = "actflg";
	public final String MOD_AT = "mod_at";
	public final String MOD_BY = "mod_by";
	public final String CRE_AT = "cre_at";
	public final String CRE_BY = "cre_by";
	public final String RECORD_DATE = "recordDate";


    public Long getId() {
        return this.getLong(ID, 0);
    }

    public void setId(Long id) {
        this.put(ID, id);
    }

    public String getHformkey() {
        return this.getString(HFORMKEY, "");
    }

    public void setHformkey(String hformkey) {
        this.put(HFORMKEY, hformkey);
    }

    public String getCitycode() {
        return this.getString(CITYCODE, "");
    }

    public void setCitycode(String citycode) {
        this.put(CITYCODE, citycode);
    }

    public String getSumcode() {
        return this.getString(SUMCODE, "");
    }

    public void setSumcode(String sumcode) {
        this.put(SUMCODE, sumcode);
    }

    public String getBagcode() {
        return this.getString(BAGCODE, "");
    }

    public void setBagcode(String bagcode) {
        this.put(BAGCODE, bagcode);
    }

    public String getMalchinner() {
        return this.getString(MALCHINNER, "");
    }

    public void setMalchinner(String malchinner) {
        this.put(MALCHINNER, malchinner);
    }

    public String getBaigal_haldvariin() {
        return this.getString(BAIGAL_HALDVARIIN, "");
    }

    public void setBaigal_haldvariin(String baigal_haldvariin) {
        this.put(BAIGAL_HALDVARIIN, baigal_haldvariin);
    }

    public String getBaktsinii() {
        return this.getString(BAKTSINII, "");
    }

    public void setBaktsinii(String baktsinii) {
        this.put(BAKTSINII, baktsinii);
    }

    public String getDs_hiiseneseh() {
        return this.getString(DS_HIISENESEH, "");
    }

    public void setDs_hiiseneseh(String ds_hiiseneseh) {
        this.put(DS_HIISENESEH, ds_hiiseneseh);
    }

    public String getDs_bag_gishuun_ners() {
        return this.getString(DS_BAG_GISHUUN_NERS, "");
    }

    public void setDs_bag_gishuun_ners(String ds_bag_gishuun_ners) {
        this.put(DS_BAG_GISHUUN_NERS, ds_bag_gishuun_ners);
    }

    public String getDs_ur_dun() {
        return this.getString(DS_UR_DUN, "");
    }

    public void setDs_ur_dun(String ds_ur_dun) {
        this.put(DS_UR_DUN, ds_ur_dun);
    }

    public String getZuvlomj() {
        return this.getString(ZUVLOMJ, "");
    }

    public void setZuvlomj(String zuvlomj) {
        this.put(ZUVLOMJ, zuvlomj);
    }

    public String getHuchin_zuil_ognoo() {
        return this.getString(HUCHIN_ZUIL_OGNOO, "");
    }

    public void setHuchin_zuil_ognoo(String huchin_zuil_ognoo) {
        this.put(HUCHIN_ZUIL_OGNOO, huchin_zuil_ognoo);
    }

    public String getUrjliin_mal_hud() {
        return this.getString(URJLIIN_MAL_HUD, "");
    }

    public void setUrjliin_mal_hud(String urjliin_mal_hud) {
        this.put(URJLIIN_MAL_HUD, urjliin_mal_hud);
    }

    public String getBusad_mal_hud() {
        return this.getString(BUSAD_MAL_HUD, "");
    }

    public void setBusad_mal_hud(String busad_mal_hud) {
        this.put(BUSAD_MAL_HUD, busad_mal_hud);
    }

    public String getBakcin_targu() {
        return this.getString(BAKCIN_TARGU, "");
    }

    public void setBakcin_targu(String bakcin_targu) {
        this.put(BAKCIN_TARGU, bakcin_targu);
    }

    public String getOtor_nuudel() {
        return this.getString(OTOR_NUUDEL, "");
    }

    public void setOtor_nuudel(String otor_nuudel) {
        this.put(OTOR_NUUDEL, otor_nuudel);
    }

    public String getHun_sh_hod() {
        return this.getString(HUN_SH_HOD, "");
    }

    public void setHun_sh_hod(String hun_sh_hod) {
        this.put(HUN_SH_HOD, hun_sh_hod);
    }

    public String getTeever_sh_hod() {
        return this.getString(TEEVER_SH_HOD, "");
    }

    public void setTeever_sh_hod(String teever_sh_hod) {
        this.put(TEEVER_SH_HOD, teever_sh_hod);
    }

    public String getTabiul_mal_avsan() {
        return this.getString(TABIUL_MAL_AVSAN, "");
    }

    public void setTabiul_mal_avsan(String tabiul_mal_avsan) {
        this.put(TABIUL_MAL_AVSAN, tabiul_mal_avsan);
    }

    public String getBelcheer_neg() {
        return this.getString(BELCHEER_NEG, "");
    }

    public void setBelcheer_neg(String belcheer_neg) {
        this.put(BELCHEER_NEG, belcheer_neg);
    }

    public String getHudag_us() {
        return this.getString(HUDAG_US, "");
    }

    public void setHudag_us(String hudag_us) {
        this.put(HUDAG_US, hudag_us);
    }

    public String getBusad_huch_zuil() {
        return this.getString(BUSAD_HUCH_ZUIL, "");
    }

    public void setBusad_huch_zuil(String busad_huch_zuil) {
        this.put(BUSAD_HUCH_ZUIL, busad_huch_zuil);
    }

    public String getShaltgaan_todgu() {
        return this.getString(SHALTGAAN_TODGU, "");
    }

    public void setShaltgaan_todgu(String shaltgaan_todgu) {
        this.put(SHALTGAAN_TODGU, shaltgaan_todgu);
    }

    public String getMalchin_uuruu() {
        return this.getString(MALCHIN_UURUU, "");
    }

    public void setMalchin_uuruu(String malchin_uuruu) {
        this.put(MALCHIN_UURUU, malchin_uuruu);
    }

    public String getSahalt_ail() {
        return this.getString(SAHALT_AIL, "");
    }

    public void setSahalt_ail(String sahalt_ail) {
        this.put(SAHALT_AIL, sahalt_ail);
    }

    public String getMal_emneleg() {
        return this.getString(MAL_EMNELEG, "");
    }

    public void setMal_emneleg(String mal_emneleg) {
        this.put(MAL_EMNELEG, mal_emneleg);
    }

    public String getUzleg_tandalt() {
        return this.getString(UZLEG_TANDALT, "");
    }

    public void setUzleg_tandalt(String uzleg_tandalt) {
        this.put(UZLEG_TANDALT, uzleg_tandalt);
    }

    public String getTandalt_shinjilgee() {
        return this.getString(TANDALT_SHINJILGEE, "");
    }

    public void setTandalt_shinjilgee(String tandalt_shinjilgee) {
        this.put(TANDALT_SHINJILGEE, tandalt_shinjilgee);
    }

    public String getBusad() {
        return this.getString(BUSAD, "");
    }

    public void setBusad(String busad) {
        this.put(BUSAD, busad);
    }

    public String getTusgaarlasan_eseh() {
        return this.getString(TUSGAARLASAN_ESEH, "");
    }

    public void setTusgaarlasan_eseh(String tusgaarlasan_eseh) {
        this.put(TUSGAARLASAN_ESEH, tusgaarlasan_eseh);
    }

    public Date getTusgaarlasan_ognoo() {
        return (Date) this.getDate(TUSGAARLASAN_OGNOO);
    }

    public void setTusgaarlasan_ognoo(Date tusgaarlasan_ognoo) {
        this.put(TUSGAARLASAN_OGNOO, tusgaarlasan_ognoo);
    }

    public String getTus_horoo_bairlal() {
        return this.getString(TUS_HOROO_BAIRLAL, "");
    }

    public void setTus_horoo_bairlal(String tus_horoo_bairlal) {
        this.put(TUS_HOROO_BAIRLAL, tus_horoo_bairlal);
    }

    public String getDelflg() {
        return this.getString(DELFLG, "");
    }

    public void setDelflg(String delflg) {
        this.put(DELFLG, delflg);
    }

    public String getActflg() {
        return this.getString(ACTFLG, "");
    }

    public void setActflg(String actflg) {
        this.put(ACTFLG, actflg);
    }

    public Date getMod_at() {
        return (Date) this.getDate(MOD_AT);
    }

    public void setMod_at(Date mod_at) {
        this.put(MOD_AT, mod_at);
    }

    public String getMod_by() {
        return this.getString(MOD_BY, "");
    }

    public void setMod_by(String mod_by) {
        this.put(MOD_BY, mod_by);
    }

    public Date getCre_at() {
        return (Date) this.getDate(CRE_AT);
    }

    public void setCre_at(Date cre_at) {
        this.put(CRE_AT, cre_at);
    }

    public String getCre_by() {
        return this.getString(CRE_BY, "");
    }

    public void setCre_by(String cre_by) {
        this.put(CRE_BY, cre_by);
    }

    public Date getRecord_date() {
        return (Date) this.getDate(RECORD_DATE);
    }

    public void setRecord_date(Date record_date) {
        this.put(RECORD_DATE, record_date);
    }
}
