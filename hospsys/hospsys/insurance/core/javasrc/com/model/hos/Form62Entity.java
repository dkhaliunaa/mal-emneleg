package com.model.hos;

/**
 * Created by 24be10264 on 9/5/2017.
 */

import com.mongodb.BasicDBObject;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public class Form62Entity extends BasicDBObject {
    public static final String ID = "id";
    public final String AIMAG = "aimag";
    public final String SUM = "sum";
    public static final String MURIINDUGAAR = "muriinDugaar";
    public static final String AJAHUINNER = "ajahuinner";
    public static final String EZENNER = "ezenner";
    public static final String MERGEJIL = "mergejil";
    public static final String IHEMCH = "ihEmch";
    public static final String BAGAEMCH = "bagaEmch";
    public static final String MALZUICH = "malZuich";
    public static final String ORHTOO = "orhToo";
    public static final String MALTOO = "malToo";
    public static final String NEGJMALTOO = "negjMalToo";
    public static final String MERGEJILTENMALTOO = "mergejiltenMalToo";
    public static final String BAIR = "bair";
    public static final String UNAA = "unaa";
    public static final String ONTSSAIN = "ontsSain";
    public static final String SAIN = "sain";
    public static final String DUND = "dund";
    public static final String HANGALTGUI = "hangaltgui";
    public final String AIMAGNER="aimagNer";
    public final String SUMNER="sumNer";

    public final String AIMAG_EN = "aimag_en";
    public final String SUM_EN = "sum_en";
    public static final String AJAHUINNER_EN = "ajAhuinNer_en";
    public static final String EZENNER_EN = "ezenNer_en";
    public static final String MERGEJIL_EN = "mergejil_en";
    public static final String IHEMCH_EN = "ihEmch_en";
    public static final String BAGAEMCH_EN = "bagaEmch_en";
    public static final String MALZUICH_EN = "malZuich_en";
    public static final String BAIR_EN = "bair_en";
    public static final String UNAA_EN = "unaa_en";
    public static final String ONTSSAIN_EN = "ontsSain_en";
    public static final String SAIN_EN = "sain_en";
    public static final String DUND_EN = "dund_en";
    public static final String HANGALTGUI_EN = "hangaltgui_en";

    public final String DELFLG = "delFlg";
    public final String ACTFLG = "actFlg";
    public final String MODBY = "modBy";
    public final String MODAT = "modAt";
    public final String CREBY = "creBy";
    public final String CREAT = "creAt";
    public final String RECORD_DATE = "recordDate";
    public final String SEARCH_RECORD_DATE = "searchRecordDate";

//    form62Entity.setBagaEmch(String.valueOf(pairs.getValue()));


    public Form62Entity() {

    }

    public Long getId() {
        return this.getLong(ID, 0);
    }

    public void setId(Long id) {
        this.put(ID, id);
    }

    public String getAimag() {
        return this.getString(AIMAG);
    }

    public void setAimag(String aimag) {
        this.put(AIMAG, aimag);
    }

    public String getSum() {
        return this.getString(SUM, "");
    }

    public void setSum(String sum) {
        this.put(SUM, sum);
    }

    public String getAimag_en() {
        return this.getString(AIMAG_EN, "");
    }

    public void setAimag_en(String aimag_en) {
        this.put(AIMAG_EN, aimag_en);
    }

    public String getSum_en() {
        return this.getString(SUM_EN, "");
    }
    public void setSum_en(String sum_en) {
        this.put(SUM_EN, sum_en);
    }

    public Long getMuriinDugaar() {
        return this.getLong(MURIINDUGAAR, 0);
    }

    public void setMuriinDugaar(Long muriinDugaar) {
        this.put(MURIINDUGAAR, muriinDugaar);
    }

    public String getAjAhuinNer() {
        return this.getString(AJAHUINNER, "");
    }

    public void setAjAhuinNer(String ajAhuinNer) {
        this.put(AJAHUINNER, ajAhuinNer);
    }

    public String getEzenNer() {
        return this.getString(EZENNER, "");
    }

    public void setEzenNer(String ezenNer) {
        this.put(EZENNER, ezenNer);
    }

    public String getMergejil() {
        return this.getString(MERGEJIL, "");
    }

    public void setMergejil(String mergejil) {
        this.put(MERGEJIL, mergejil);
    }

    public String getIhEmch() {
        return this.getString(IHEMCH, "");
    }

    public void setIhEmch(String ihEmch) {
        this.put(IHEMCH, ihEmch);
    }

    public String getBagaEmch() {
        return this.getString(BAGAEMCH, "");
    }

    public void setBagaEmch(String bagaEmch) {
        this.put(BAGAEMCH, bagaEmch);
    }

    public String getMalZuich() {
        return this.getString(MALZUICH, "");
    }

    public void setMalZuich(String malZuich) {
        this.put(MALZUICH, malZuich);
    }

    public Long getOrhToo() {
        return this.getLong(ORHTOO, 0);
    }

    public void setOrhToo(Long orhToo) {
        this.put(ORHTOO, orhToo);
    }

    public Long getMalToo() {
        return this.getLong(MALTOO, 0);
    }

    public void setMalToo(Long malToo) {
        this.put(MALTOO, malToo);
    }

    public Long getNegjMalToo() {
        return this.getLong(NEGJMALTOO, 0);
    }

    public void setNegjMalToo(Long negjMalToo) {
        this.put(NEGJMALTOO, negjMalToo);
    }

    public BigDecimal getMergejiltenMalToo() {
        return BigDecimal.valueOf(this.getDouble(MERGEJILTENMALTOO,0));
    }

    public void setMergejiltenMalToo(BigDecimal mergejiltenMalToo) {
        this.put(MERGEJILTENMALTOO, mergejiltenMalToo);
    }

    public String getBair() {
        return this.getString(BAIR, "");
    }

    public void setBair(String bair) {
        this.put(BAIR, bair);
    }

    public String getUnaa() {
        return this.getString(UNAA, "");
    }

    public void setUnaa(String unaa) {
        this.put(UNAA, unaa);
    }

    public String getOntsSain() {
        return this.getString(ONTSSAIN, "");
    }

    public void setOntsSain(String ontsSain) {
        this.put(ONTSSAIN, ontsSain);
    }

    public String getSain() {
        return this.getString(SAIN, "");
    }

    public void setSain(String sain) {
        this.put(SAIN, sain);
    }

    public String getDund() {
        return this.getString(DUND, "");
    }

    public void setDund(String dund) {
        this.put(DUND, dund);
    }

    public String getHangaltgui() {
        return this.getString(HANGALTGUI, "");
    }

    public void setHangaltgui(String hangaltgui) {
        this.put(HANGALTGUI, hangaltgui);
    }

    public String getAjAhuinNer_en() {
        return this.getString(AJAHUINNER_EN, "");
    }

    public void setAjAhuinNer_en(String ajAhuinNer_en) {
        this.put(AJAHUINNER_EN, ajAhuinNer_en);
    }

    public String getEzenNer_en() {
        return this.getString(EZENNER_EN, "");
    }

    public void setEzenNer_en(String ezenNer_en) {
        this.put(EZENNER_EN, ezenNer_en);
    }

    public String getMergejil_en() {
        return this.getString(MERGEJIL_EN, "");
    }

    public void setMergejil_en(String mergejil_en) {
        this.put(MERGEJIL_EN, mergejil_en);
    }

    public String getIhEmch_en() {
        return this.getString(IHEMCH_EN, "");
    }

    public void setIhEmch_en(String ihEmch_en) {
        this.put(IHEMCH_EN, ihEmch_en);
    }

    public String getBagaEmch_en() {
        return this.getString(BAGAEMCH_EN, "");
    }

    public void setBagaEmch_en(String bagaEmch_en) {
        this.put(BAGAEMCH_EN, bagaEmch_en);
    }

    public String getMalZuich_en() {
        return this.getString(MALZUICH_EN, "");
    }

    public void setMalZuich_en(String malZuich_en) {
        this.put(MALZUICH_EN, malZuich_en);
    }

    public String getBair_en() {
        return this.getString(BAIR_EN, "");
    }

    public void setBair_en(String bair_en) {
        this.put(BAIR_EN, bair_en);
    }

    public String getUnaa_en() {
        return this.getString(UNAA_EN, "");
    }

    public void setUnaa_en(String unaa_en) {
        this.put(UNAA_EN, unaa_en);
    }

    public String getOntsSain_en() {
        return this.getString(ONTSSAIN_EN, "");
    }

    public void setOntsSain_en(String ontsSain_en) {
        this.put(ONTSSAIN_EN, ontsSain_en);
    }

    public String getSain_en() {
        return this.getString(SAIN_EN, "");
    }

    public void setSain_en(String sain_en) {
        this.put(SAIN_EN, sain_en);
    }

    public String getDund_en() {
        return this.getString(DUND_EN, "");
    }

    public void setDund_en(String dund_en) {
        this.put(DUND_EN, dund_en);
    }

    public String getHangaltgui_en() {
        return this.getString(HANGALTGUI_EN, "");
    }

    public void setHangaltgui_en(String hangaltgui_en) {
        this.put(HANGALTGUI_EN, hangaltgui_en);
    }

    public String getDelflg() {
        return this.getString(DELFLG, "");
    }

    public void setDelflg(String delflg) {
        this.put(DELFLG, delflg);
    }

    public String getActflg() {
        return this.getString(ACTFLG, "");
    }

    public void setActflg(String actflg) {
        this.put(ACTFLG, actflg);
    }

    public String getModby() {
        return getString(MODBY, "");
    }

    public void setModby(String modby) {
        this.put(MODBY, modby);
    }

    public java.sql.Date getModAt() {
        return (java.sql.Date) getDate(MODAT);
    }

    public void setModAt(java.sql.Date modAt) {
        this.put(MODAT, modAt);
    }

    public String getCreby() {
        return getString(CREBY, "");
    }

    public void setCreby(String creby) {
        this.put(CREBY, creby);
    }

    public java.sql.Date getCreat() {
        return (java.sql.Date) getDate(CREAT, null);
    }

    public void setCreat(java.sql.Date creat) {
        this.put(CREAT, creat);
    }

    public Date getRecordDate() {
        return (Date) this.getDate(RECORD_DATE, null);
    }

    public void setRecordDate(Date recordDate) {
        this.put(RECORD_DATE, recordDate);
    }

    public Date getSearchRecordDate() {
        return (Date) this.getDate(SEARCH_RECORD_DATE, null);
    }

    public void setSearchRecordDate(Date recordDate) {
        this.put(SEARCH_RECORD_DATE, recordDate);
    }

    public String getAimagNer() {
        return this.getString(AIMAGNER,"");
    }

    public void setAimagNer(String aimagNer) {
        this.put(AIMAGNER,aimagNer);
    }

    public String getSumNer() {
        return this.getString(SUMNER,"");
    }

    public void setSumNer(String sumNer) {
        this.put(SUMNER,sumNer);
    }
}
