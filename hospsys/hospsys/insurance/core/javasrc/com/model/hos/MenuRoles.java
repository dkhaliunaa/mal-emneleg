package com.model.hos;

import com.mongodb.BasicDBObject;

import java.sql.Date;

public class MenuRoles extends BasicDBObject{
    public final String ID = "id";
    public final String ROLECODE = "rolecode";
    public final String MENUID = "menuid";
    public final String DELFLG = "delflg";
    public final String CREAT = "creat";
    public final String CREBY = "creby";
    
    public final String ROLENAME = "rolename";
    public final String MENUNAME = "menuname";
    public final String FORMID = "formid";

    public Long getId() {
        return this.getLong(ID, 0);
    }

    public void setId(Long id) {
        this.put(ID,id);
    }

    public String getRolecode() {
        return this.getString(ROLECODE, "");
    }

    public void setRolecode(String rolecode) {
        this.put(ROLECODE, rolecode);
    }

    public String getMenuId() {
        return this.getString(MENUID, "");
    }

    public void setMenuId(String menuId) {
        this.put(MENUID, menuId);
    }

    public String getDelflg() {
        return this.getString(DELFLG,"");
    }

    public void setDelflg(String delflg) {
        this.put(DELFLG, delflg);
    }

    public Date getCreAt() {
        return (Date) this.getDate(CREAT, null);
    }

    public void setCreAt(Date creAt) {
        this.put(CREAT, creAt);
    }

    public String getCreBy() {
        return this.getString(CREBY);
    }

    public void setCreBy(String creBy) {
        this.put(CREBY, creBy);
    }

    public String getRoleName() {
        return this.getString(ROLENAME);
    }

    public void setRoleName(String roleName) {
        this.put(ROLENAME, roleName);
    }

    public String getMenuName() {
        return this.getString(MENUNAME);
    }

    public void setMenuName(String menuName) {
        this.put(MENUNAME, menuName);
    }

    public String getFormId() {
        return this.getString(FORMID);
    }

    public void setFormId(String menuName) {
        this.put(FORMID, menuName);
    }
}
