package com.model.hos;

/**
 * Created by Tiku on 8/20/2017.
 *
 * Объект дамжуулах класс бөгөөд htable хүснэгт уруу set хийх
 * entity класс юм. Тухайн хүснэгт дээр хийгдэх өөрчлөлт шууд
 * нөлөөлөх юм.
 *
 */
public class HTableEntity extends EntityClass {
    /**
     *
     * 1	id
     * 2	tblname
     * 3	colname
     * 4	coltype
     * 5	col_pk
     * 6	col_nn
     * 7	col_ai
     * 8	col_uq
     * 9	col_fk
     * 10	ref_tbl
     * 11	ref_col
     * */

    private Long id;
    private String tblName;
    private String colName;
    private String colType;
    private String colPK;
    private String colNN;
    private String colAI;
    private String colUQ;
    private String colFK;
    private String refTbl;
    private String refCol;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTblName() {
        return tblName;
    }

    public void setTblName(String tblName) {
        this.tblName = tblName;
    }

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public String getColType() {
        return colType;
    }

    public void setColType(String colType) {
        this.colType = colType;
    }

    public String getColPK() {
        return colPK;
    }

    public void setColPK(String colPK) {
        this.colPK = colPK;
    }

    public String getColNN() {
        return colNN;
    }

    public void setColNN(String colNN) {
        this.colNN = colNN;
    }

    public String getColAI() {
        return colAI;
    }

    public void setColAI(String colAI) {
        this.colAI = colAI;
    }

    public String getColUQ() {
        return colUQ;
    }

    public void setColUQ(String colUQ) {
        this.colUQ = colUQ;
    }

    public String getColFK() {
        return colFK;
    }

    public void setColFK(String colFK) {
        this.colFK = colFK;
    }

    public String getRefTbl() {
        return refTbl;
    }

    public void setRefTbl(String refTbl) {
        this.refTbl = refTbl;
    }

    public String getRefCol() {
        return refCol;
    }

    public void setRefCol(String refCol) {
        this.refCol = refCol;
    }
}
