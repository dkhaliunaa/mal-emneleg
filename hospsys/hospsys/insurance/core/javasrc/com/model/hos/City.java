package com.model.hos;

import com.mongodb.BasicDBObject;

import java.sql.Date;

/**
 * Created by 24c410353 on 9/20/2017.
 */
public class City extends BasicDBObject {
    public final  String ID = "id";
    public final String CODE = "code";
    public final String COUNTRYCODE = "countrycode";
    public final String CITYNAME = "cityname";
    public final String CITYDESC = "citydesc";
    public final String CITYNAME_EN = "cityname_en";
    public final String SEARCHDATE = "searchDate";
    public final String DELFLG = "delFlg";
    public final String ACTFLG = "actFlg";
    public final String MODBY = "modBy";
    public final String MODAT = "modAt";
    public final String CREBY = "creBy";
    public final String CREAT = "creAt";

    public final String LATITUDE = "latitude";
    public final String LONGITUDE = "longitude";

    public Long getId (){
        return this.getLong(ID, 0);
    }

    public void setId(long id){
        this.put(ID, id);
    }

    public String getCode() {
        return this.getString(CODE, "");
    }

    public void setCode(String code) {
        this.put(CODE, code);
    }

    public String getCountrycode() {
        return this.getString(COUNTRYCODE);
    }

    public void setCountrycode(String countrycode) {
        this.put(COUNTRYCODE, countrycode);
    }

    public String getCityname() {
        return this.getString(CITYNAME, "");
    }

    public void setCityname(String cityname) {
        this.put(CITYNAME, cityname);
    }

    public String getCitydesc() {
        return this.getString(CITYDESC, "");
    }

    public void setCitydesc(String citydesc) {
        this.put(CITYDESC, citydesc);
    }

    public String getCitynameEn() {
        return this.getString(CITYNAME_EN, "");
    }

    public void setCitynameEn(String citynameEn) {
        this.put(CITYNAME_EN, citynameEn);
    }

    public Date getSearchDate() {
        return (Date) this.getDate(SEARCHDATE,null);
    }

    public void setSearchdate(String searchdate) {
        this.put(SEARCHDATE, searchdate);
    }

    public String getDelflg() {
        return this.getString(DELFLG, "");
    }

    public void setDelflg(String delflg) {
        this.put(DELFLG, delflg);
    }

    public String getActflg() {
        return this.getString(ACTFLG,"");
    }

    public void setActflg(String actflg) {
        this.put(ACTFLG, actflg);
    }

    public String getModBy() {
        return getString(MODBY,"");
    }

    public void setModby(String modby){
        this.put(MODBY,modby);
    }

    public Date getModAt() {
        return (Date) this.getDate(MODAT, null);
    }

    public void setModAt(Date modAt) {
        this.put(MODAT, modAt);
    }

    public String getCreBy() {
        return getString(CREBY,"");
    }

    public void setCreby(String creby){
        this.put(CREBY,creby);
    }

    public java.sql.Date getCreAt() {
        return (java.sql.Date) getDate(CREAT, null);
    }

    public void setCreat(Date creat){
        this.put(CREAT,creat);
    }

    public String getLongitude() {
        return getString(LONGITUDE,"");
    }

    public void setLongitude(String modby){
        this.put(LONGITUDE,modby);
    }

    public String getLatitude() {
        return getString(LATITUDE,"");
    }

    public void setLatitude(String modby){
        this.put(LATITUDE,modby);
    }
}
