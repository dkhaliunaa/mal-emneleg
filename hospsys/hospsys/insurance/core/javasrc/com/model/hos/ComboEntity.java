package com.model.hos;

import com.mongodb.BasicDBObject;

import java.sql.Date;

/**
 * Created by 24be10264 on 8/18/2017.
 */
public class ComboEntity extends BasicDBObject{
    /**
     *
     * `id` INT NOT NULL AUTO_INCREMENT,
     * `cmbtype` VARCHAR(45) NOT NULL,
     * `cmbval` VARCHAR(45) NOT NULL,
     * `cmbname` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NOT NULL,
     * `cmbdesc` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NULL,
     * `langid` VARCHAR(45) NOT NULL,
     * `delflg` CHAR(1) NOT NULL,
     * `actflg` CHAR(1) NOT NULL,
     * `cre_at` DATE NOT NULL,
     * `cre_by` VARCHAR(45) NOT NULL,
     * `mod_at` DATE NULL,
     * `mod_by` VARCHAR(45) NULL
     */

    public final String ID = "id";
    public final String CMBTYPE = "cmbtype";
    public final String CMBVAL = "cmbval";
    public final String CMBNAME = "cmbname";
    public final String CMBDESC = "cmbdesc";
    public final String LANGID = "langid";
    public final String DELFLG = "delflg";
    public final String ACTFLG = "actflg";
    public final String CREAT = "creAt";
    public final String CREBY = "creBy";
    public final String MODAT = "modAt";
    public final String MODBY = "modBy";


    public Long getId() {
        return this.getLong(ID, 0);
    }

    public void setId(Long id) {
        this.put(ID, id);
    }

    public String getCmbtype() {
        return this.getString(CMBTYPE, "");
    }

    public void setCmbtype(String cmbtype) {
        this.put(CMBTYPE,cmbtype);
    }

    public String getCmbval() {
        return this.getString(CMBVAL, "");
    }

    public void setCmbval(String cmbval) {
        this.put(CMBVAL, cmbval);
    }

    public String getCmbname() {
        return this.getString(CMBNAME, "");
    }

    public void setCmbname(String cmbname) {
        this.put(CMBNAME, cmbname);
    }

    public String getCmbdesc() {
        return this.getString(CMBDESC, "");
    }

    public void setCmbdesc(String cmbdesc) {
        this.put(CMBDESC, cmbdesc);
    }

    public String getLangid() {
        return this.getString(LANGID, "mn");
    }

    public void setLangid(String langid) {
        this.put(LANGID, langid);
    }

    public String getDelflg() {
        return this.getString(DELFLG, "");
    }

    public void setDelflg(String delflg) {
        this.put(DELFLG, delflg);
    }

    public String getActflg() {
        return this.getString(ACTFLG, "");
    }

    public void setActflg(String actflg) {
        this.put(ACTFLG, actflg);
    }

    public Date getCreAt() {
        return (Date) this.getDate(CREAT);
    }

    public void setCreAt(Date creAt) {
        this.put(CREAT, creAt);
    }

    public String getCreBy() {
        return this.getString(CREBY, "");
    }

    public void setCreBy(String creBy) {
        this.put(CREBY, creBy);
    }

    public Date getModAt() {
        return (Date) this.getDate(MODAT);
    }

    public void setModAt(Date modAt) {
        this.put(MODAT, modAt);
    }

    public String getModBy() {
        return this.getString(MODBY);
    }

    public void setModBy(String modBy) {
        this.put(MODBY, modBy);
    }
}
