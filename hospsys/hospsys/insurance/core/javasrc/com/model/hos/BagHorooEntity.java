package com.model.hos;

import com.mongodb.BasicDBObject;

import java.sql.Date;

/**
 * Created by 24be10264 on 9/27/2017.
 */
public class BagHorooEntity extends BasicDBObject{
    public final String ID = "id";
    public final String CITY_CODE = "citycode";
    public final String HOROO_CODE = "horoocode";
    public final String SUM_CODE = "sumcode";
    public final String HOROO_NAME = "horooname";
    public final String HOROO_DESC = "horoodesc";
    public final String HOROO_NAME_EN = "horooname_en";
    public final String DELFLG = "delflg";
    public final String ACTFLG = "actflg";
    public final String CRE_AT = "creat";
    public final String CRE_BY = "creby";
    public final String MOD_AT = "modat";
    public final String MOD_BY = "modby";
    public final String SEARCH_CDATE = "searchcdate";
    public final String SEARCH_MDATE = "searchmdate";
    public final String CITY_NAME = "cityname";
    public final String SUM_NAME = "sumname";

    public final String LATITUDE = "latitude";
    public final String LONGITUDE = "longitude";

    public Long getId() {
        return this.getLong(ID, 0);
    }

    public void setId(Long id) {
        this.put(ID, id);
    }

    public String getCityCode() {
        return this.getString(CITY_CODE, "");
    }

    public void setCityCode(String horoocode) {
        this.put(CITY_CODE, horoocode);
    }

    public String getHoroocode() {
        return this.getString(HOROO_CODE, "");
    }

    public void setHoroocode(String horoocode) {
        this.put(HOROO_CODE, horoocode);
    }

    public String getSumcode() {
        return getString(SUM_CODE, "");
    }

    public void setSumcode(String sumcode) {
        this.put(SUM_CODE, sumcode);
    }

    public String getHorooname() {
        return this.getString(HOROO_NAME, "");
    }

    public void setHorooname(String horooname) {
        this.put(HOROO_NAME, horooname);
    }

    public String getHoroodesc() {
        return this.getString(HOROO_DESC, "");
    }

    public void setHoroodesc(String horoodesc) {
        this.put(HOROO_DESC, horoodesc);
    }

    public String getHorooname_en() {
        return this.getString(HOROO_NAME_EN, "");
    }

    public void setHorooname_en(String horooname_en) {
        this.put(HOROO_NAME_EN, horooname_en);
    }

    public String getDelflg() {
        return this.getString(DELFLG, "");
    }

    public void setDelflg(String delflg) {
        this.put(DELFLG, delflg);
    }

    public String getActflg() {
        return this.getString(ACTFLG);
    }

    public void setActflg(String actflg) {
        this.put(ACTFLG, actflg);
    }

    public Date getCre_at() {
        return (Date) this.getDate(CRE_AT, null);
    }

    public void setCre_at(Date cre_at) {
        this.put(CRE_AT, cre_at);
    }

    public String getCre_by() {
        return this.getString(CRE_BY, "");
    }

    public void setCre_by(String cre_by) {
        this.put(CRE_BY, cre_by);
    }

    public Date getMod_at() {
        return (Date) this.getDate(MOD_AT);
    }

    public void setMod_at(Date mod_at) {
        this.put(MOD_AT, mod_at);
    }

    public String getMod_by() {
        return this.getString(MOD_BY);
    }

    public void setMod_by(String mod_by) {
        this.put(MOD_BY, mod_by);
    }


    public Date getSearchCdate() {
        return (Date) this.getDate(SEARCH_CDATE);
    }

    public void setSearchCdate(Date searchCdate) {
        put(SEARCH_CDATE, searchCdate);
    }

    public Date getSearchMdate() {
        return (Date) this.getDate(SEARCH_MDATE);
    }

    public void setSearchMdate(Date searchCdate) {
        put(SEARCH_MDATE, searchCdate);
    }

    public String getCityName() {
        return this.getString(CITY_NAME, "");
    }

    public void setCityName(String horoocode) {
        this.put(CITY_NAME, horoocode);
    }

    public String getSumName() {
        return this.getString(SUM_NAME, "");
    }

    public void setSumName(String horoocode) {
        this.put(SUM_NAME, horoocode);
    }

    public String getLongitude() {
        return getString(LONGITUDE,"");
    }

    public void setLongitude(String modby){
        this.put(LONGITUDE,modby);
    }

    public String getLatitude() {
        return getString(LATITUDE,"");
    }

    public void setLatitude(String modby){
        this.put(LATITUDE,modby);
    }
}
