package com.model.hos;

import com.mongodb.BasicDBObject;

import java.sql.Date;

public class GalzuuFormEntity extends BasicDBObject {
    public static String ID = "id";
    public static String FRMKEY = "frmkey";
    public static String CITYCODE = "citycode";
    public static String SUMCODE = "sumcode";
    public static String BAGNAME = "bagcode";
    public static String GAZARNER = "gazarner";
    public static String GOLOMTTOO = "golomttoo";
    public static String MALCHINNER = "malchinner";
    public static String UVCHLOL_BURT_URH_HARI = "uvchlol_burt_urh_hari";
    public static String COORDINATE_N = "coordinate_n";
    public static String COORDINATE_E = "coordinate_e";
    public static String DUUDLAGAOGNOO = "duudlagaognoo";
    public static String UVCHLOLEHELSEN_OGNOO = "uvchlolehelsen_ognoo";
    public static String UVCHLOLBURT_OGNOO = "uvchlolburt_ognoo";
    public static String LAB_BAT_OGNOO = "lab_bat_ognoo";
    public static String LAB_BAT_HEVSHIL = "lab_bat_hevshil";
    public static String SHARGA = "sharga";
    public static String SHURDUN = "shurdun";

    public static String UVCH_UHER = "uvch_uher";
    public static String UVCH_HONI = "uvch_honi";
    public static String UVCH_YMAA = "uvch_ymaa";
    public static String UVCH_BUSAD = "uvch_busad";

    public static String UHSEN_UHER = "uhsen_uher";
    public static String UHSEN_HONI = "uhsen_honi";
    public static String UHSEN_YMAA = "uhsen_ymaa";
    public static String UHSEN_BUSAD = "uhsen_busad";

    public static String USTGASAN_UHER = "ustgasan_uher";
    public static String USTGASAN_HONI = "ustgasan_honi";
    public static String USTGASAN_YMAA = "ustgasan_ymaa";
    public static String USTGASAN_BUSAD = "ustgasan_busad";

    public static String HORIO_CEER_ESEH = "horio_ceer_eseh";
    public static String HORIO_CEER_OGNOO = "horio_ceer_ognoo";
    public static String HORIO_CEER_CUCALSAN_OGNOO = "horio_ceer_cucalsan_ognoo";
    public static String USTGASAN_ESEH = "ustgasan_eseh";
    public static String USTGAL_HIISEN_OGNOO = "ustgal_hiisen_ognoo";

    public static String DELFLG = "delflg";
    public static String ACTFLG = "actflg";
    public static String CRE_AT = "cre_at";
    public static String CRE_BY = "cre_by";
    public static String MOD_AT = "mod_at";
    public static String MOD_BY = "mod_by";
    public static String RECORD_DATE = "record_date";
    public static String SEARCH_DATE = "search_date";
    public final String AIMAGNER="aimagNer";
    public final String SUMNER="sumNer";
    public String getAimagNer() {
        return this.getString(AIMAGNER,"");
    }

    public void setAimagNer(String aimagNer) {
        this.put(AIMAGNER,aimagNer);
    }

    public String getSumNer() {
        return this.getString(SUMNER,"");
    }

    public void setSumNer(String sumNer) {
        this.put(SUMNER,sumNer);
    }

    public Long getId() {
        return this.getLong(ID, -1);
    }

    public void setId(Long id) {
        this.put(ID, id);
    }

    public String getFrmkey() {
        return this.getString(FRMKEY, "");
    }

    public void setFrmkey(String frmkey) {
        this.put(FRMKEY, frmkey);
    }

    public String getCitycode() {
        return this.getString(CITYCODE, "");
    }

    public void setCitycode(String citycode) {
        this.put(CITYCODE, citycode);
    }

    public String getSumcode() {
        return this.getString(SUMCODE, "");
    }

    public void setSumcode(String sumcode) {
        this.put(SUMCODE, sumcode);
    }

    public String getBagname() {
        return this.getString(BAGNAME, "");
    }

    public void setBagname(String bagname) {
        this.put(BAGNAME, bagname);
    }

    public String getGazarner() {
        return this.getString(GAZARNER, "");
    }

    public void setGazarner(String gazarner) {
        this.put(GAZARNER, gazarner);
    }

    public double getGolomttoo() {
        return this.getDouble(GOLOMTTOO, 0);
    }

    public void setGolomttoo(double golomttoo) {
        this.put(GOLOMTTOO,golomttoo);
    }

    public String getMalchinner() {
        return this.getString(MALCHINNER, "");
    }

    public void setMalchinner(String malchinner) {
        this.put(MALCHINNER, malchinner);
    }

    public String getUvchlol_burt_urh_hari() {
        return this.getString(UVCHLOL_BURT_URH_HARI, "");
    }

    public void setUvchlol_burt_urh_hari(String uvchlol_burt_urh_hari) {
        this.put(UVCHLOL_BURT_URH_HARI, uvchlol_burt_urh_hari);
    }

    public String getCoordinate_n() {
        return this.getString(COORDINATE_N, "");
    }

    public void setCoordinate_n(String coordinate_n) {
        this.put(COORDINATE_N, coordinate_n);
    }

    public String getCoordinate_e() {
        return this.getString(COORDINATE_E, "");
    }

    public void setCoordinate_e(String coordinate_e) {
        this.put(COORDINATE_E, coordinate_e);
    }

    public Date getDuudlagaognoo() {
        return (Date) this.getDate(DUUDLAGAOGNOO);
    }

    public void setDuudlagaognoo(Date duudlagaognoo) {
        this.put(DUUDLAGAOGNOO, duudlagaognoo);
    }

    public Date getUvchlolehelsen_ognoo() {
        return (Date) this.getDate(UVCHLOLEHELSEN_OGNOO);
    }

    public void setUvchlolehelsen_ognoo(Date uvchlolehelsen_ognoo) {
        this.put(UVCHLOLEHELSEN_OGNOO, uvchlolehelsen_ognoo);
    }

    public Date getUvchlolburt_ognoo() {
        return (Date) this.getDate(UVCHLOLBURT_OGNOO);
    }

    public void setUvchlolburt_ognoo(Date uvchlolburt_ognoo) {
        this.put(UVCHLOLBURT_OGNOO, uvchlolburt_ognoo);
    }

    public Date getLab_bat_ognoo() {
        return (Date) this.getDate(LAB_BAT_OGNOO);
    }

    public void setLab_bat_ognoo(Date lab_bat_ognoo) {
        this.put(LAB_BAT_OGNOO, lab_bat_ognoo);
    }

    public String getLab_bat_hevshil() {
        return this.getString(LAB_BAT_HEVSHIL, "");
    }

    public void setLab_bat_hevshil(String lab_bat_hevshil) {
        this.put(LAB_BAT_HEVSHIL, lab_bat_hevshil);
    }

    public String getSharga() {
        return this.getString(SHARGA, "");
    }

    public void setSharga(String sharga) {
        this.put(SHARGA, sharga);
    }

    public String getShurdun() {
        return this.getString(SHURDUN, "");
    }

    public void setShurdun(String shurdun) {
        this.put(SHURDUN, shurdun);
    }

    public double getUvch_uher() {
        return this.getDouble(UVCH_UHER, 0);
    }

    public void setUvch_uher(double uvch_uher) {
        this.put(UVCH_UHER, uvch_uher);
    }

    public double getUvch_honi() {
        return this.getDouble(UVCH_HONI, 0);
    }

    public void setUvch_honi(double uvch_honi) {
        this.put(UVCH_HONI, uvch_honi);
    }

    public double getUvch_ymaa() {
        return this.getDouble(UVCH_YMAA, 0);
    }

    public void setUvch_ymaa(double uvch_ymaa) {
        this.put(UVCH_YMAA, uvch_ymaa);
    }

    public double getUvch_busad() {
        return this.getDouble(UVCH_BUSAD, 0);
    }

    public void setUvch_busad(double uvch_busad) {
        this.put(UVCH_BUSAD, uvch_busad);
    }

    public double getUhsen_uher() {
        return this.getDouble(UHSEN_UHER, 0);
    }

    public void setUhsen_uher(double uhsen_uher) {
        this.put(UHSEN_UHER, uhsen_uher);
    }

    public double getUhsen_honi() {
        return this.getDouble(UHSEN_HONI, 0);
    }

    public void setUhsen_honi(double uhsen_honi) {
        this.put(UHSEN_HONI, uhsen_honi);
    }

    public double getUhsen_ymaa() {
        return this.getDouble(UHSEN_YMAA, 0);
    }

    public void setUhsen_ymaa(double uhsen_ymaa) {
        this.put(UHSEN_YMAA, uhsen_ymaa);
    }

    public double getUhsen_busad() {
        return this.getDouble(UHSEN_BUSAD, 0);
    }

    public void setUhsen_busad(double uhsen_busad) {
        this.put(UHSEN_BUSAD, uhsen_busad);
    }

    public double getUstgasan_uher() {
        return this.getDouble(USTGASAN_UHER, 0);
    }

    public void setUstgasan_uher(double ustgasan_uher) {
        this.put(USTGASAN_UHER, ustgasan_uher);
    }

    public double getUstgasan_honi() {
        return this.getDouble(USTGASAN_HONI, 0);
    }

    public void setUstgasan_honi(double ustgasan_honi) {
        this.put(USTGASAN_HONI, ustgasan_honi);
    }

    public double getUstgasan_ymaa() {
        return this.getDouble(USTGASAN_YMAA, 0);
    }

    public void setUstgasan_ymaa(double ustgasan_ymaa) {
        this.put(USTGASAN_YMAA, ustgasan_ymaa);
    }

    public double getUstgasan_busad() {
        return this.getDouble(USTGASAN_BUSAD, 0);
    }

    public void setUstgasan_busad(double ustgasan_busad) {
        this.put(USTGASAN_BUSAD, ustgasan_busad);
    }

    public String getHorio_ceer_eseh() {
        return this.getString(HORIO_CEER_ESEH, "");
    }

    public void setHorio_ceer_eseh(String horio_ceer_eseh) {
        this.put(HORIO_CEER_ESEH, horio_ceer_eseh);
    }

    public Date getHorio_ceer_ognoo() {
        return (Date) this.getDate(HORIO_CEER_OGNOO);
    }

    public void setHorio_ceer_ognoo(Date horio_ceer_ognoo) {
        this.put(HORIO_CEER_OGNOO, horio_ceer_ognoo);
    }

    public Date getHorio_ceer_cucalsan_ognoo() {
        return (Date) getDate(HORIO_CEER_CUCALSAN_OGNOO);
    }

    public void setHorio_ceer_cucalsan_ognoo(Date horio_ceer_cucalsan_ognoo) {
        this.put(HORIO_CEER_CUCALSAN_OGNOO, horio_ceer_cucalsan_ognoo);
    }

    public String getUstgasan_eseh() {
        return this.getString(USTGASAN_ESEH, "");
    }

    public void setUstgasan_eseh(String ustgasan_eseh) {
        this.put(USTGASAN_ESEH, ustgasan_eseh);
    }

    public Date getUstgal_hiisen_ognoo() {
        return (Date) this.getDate(USTGAL_HIISEN_OGNOO);
    }

    public void setUstgal_hiisen_ognoo(Date ustgal_hiisen_ognoo) {
        this.put(USTGAL_HIISEN_OGNOO, ustgal_hiisen_ognoo);
    }

    public String getDelflg() {
        return this.getString(DELFLG, "");
    }

    public void setDelflg(String delflg) {
        this.put(DELFLG, delflg);
    }

    public String getActflg() {
        return this.getString(ACTFLG, "");
    }

    public void setActflg(String actflg) {
        this.put(ACTFLG, actflg);
    }

    public Date getCre_at() {
        return (Date) this.getDate(CRE_AT);
    }

    public void setCre_at(Date cre_at) {
        this.put(CRE_AT, cre_at);
    }

    public String getCre_by() {
        return this.getString(CRE_BY, "");
    }

    public void setCre_by(String cre_by) {
        this.put(CRE_BY, cre_by);
    }

    public Date getMod_at() {
        return (Date) this.getDate(MOD_AT);
    }

    public void setMod_at(Date mod_at) {
        this.put(MOD_AT, mod_at);
    }

    public String getMod_by() {
        return this.getString(MOD_BY, "");
    }

    public void setMod_by(String mod_by) {
        this.put(MOD_BY, mod_by);
    }

    public Date getRecord_date() {
        return (Date) this.getDate(RECORD_DATE);
    }

    public void setRecord_date(Date record_date) {
        this.put(RECORD_DATE, record_date);
    }

    public Date getSearchDate() {
        return (Date) this.getDate(SEARCH_DATE);
    }

    public void setSearchDate(Date record_date) {
        this.put(SEARCH_DATE, record_date);
    }
}
