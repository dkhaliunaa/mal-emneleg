package com.model.hos;

import com.mongodb.BasicDBObject;

/**
 * Created by 24be10264 on 9/2/2017.
 *
 * Form 09 Entity class
 */
public class FormSGPEntity extends BasicDBObject {
    public static final String ID="id";
    public final String AIMAG="aimag";
    public final String AIMAG_NER="aimag_Ner";
    public final String SUM="sum";
    public final String SUM_NER="sum_Ner";
    public final String BAGNER="bagner";
    public final String GAZAR_NER="gazar_Ner";
    public final String MALCHIN_NER="malchin_Ner";
    public final String COOR_N_HOUR="coor_N_Hour";
    public final String COOR_N_MINUT="coor_N_Minut";
    public final String COOR_N_SECUND="coor_N_Secund";
    public final String COOR_E_HOUR="coor_E_Hour";
    public final String COOR_E_MINUT="coor_E_Minut";
    public final String COOR_E_SECUND="coorESecund";
    public final String DUUDLAGA_IR_OGNOO="duudlaga_Ir_Ognoo";
    public final String UVCHLUL_BURT_OGNOO="uvchlul_Burt_Ognoo";
    public final String LAB_BAT_OGNOO="lab_Bat_Ognoo";
    public final String LAB_BAT_HEVSHIL="lab_Bat_Hevshil";
    public final String USTGAL_HIISEN_OGNOO="ustgal_Hiisen_Ognoo";
    public final String HORIO_TSEER_TOGTOOSON="horio_tseer_togtooson";
    public final String HORIO_TSEER_TOGTOOSON_OGNOO="horio_Tseer_Togtooson_Ognoo";
    public final String HORIO_TSEER_TSUTSALSAN_OGNOO="horio_Tseer_Tsutsalsan_Ognoo";


    public static final String DELFLG = "delFlg";
    public static final String ACTFLG = "actFlg";
    public static final String MODBY = "modBy";
    public static final String MODAT = "modAt";
    public static final String CREBY = "creBy";
    public static final String CREAT = "creAt";

    public static String getID() {
        return ID;
    }

    public String getAIMAG() {
        return AIMAG;
    }

    public String getAIMAG_NER() {
        return AIMAG_NER;
    }

    public String getSUM() {
        return SUM;
    }

    public String getSUM_NER() {
        return SUM_NER;
    }

    public String getBAGNER() {
        return BAGNER;
    }

    public String getGAZAR_NER() {
        return GAZAR_NER;
    }

    public String getMALCHIN_NER() {
        return MALCHIN_NER;
    }

    public String getCOOR_N_HOUR() {
        return COOR_N_HOUR;
    }

    public String getCOOR_N_MINUT() {
        return COOR_N_MINUT;
    }

    public String getCOOR_N_SECUND() {
        return COOR_N_SECUND;
    }

    public String getCOOR_E_HOUR() {
        return COOR_E_HOUR;
    }

    public String getCOOR_E_MINUT() {
        return COOR_E_MINUT;
    }

    public String getCOOR_E_SECUND() {
        return COOR_E_SECUND;
    }

    public String getDUUDLAGA_IR_OGNOO() {
        return DUUDLAGA_IR_OGNOO;
    }

    public String getUVCHLUL_BURT_OGNOO() {
        return UVCHLUL_BURT_OGNOO;
    }

    public String getLAB_BAT_OGNOO() {
        return LAB_BAT_OGNOO;
    }

    public String getLAB_BAT_HEVSHIL() {
        return LAB_BAT_HEVSHIL;
    }

    public String getUSTGAL_HIISEN_OGNOO() {
        return USTGAL_HIISEN_OGNOO;
    }

    public String getHORIO_TSEER_TOGTOOSON() {
        return HORIO_TSEER_TOGTOOSON;
    }

    public String getHORIO_TSEER_TOGTOOSON_OGNOO() {
        return HORIO_TSEER_TOGTOOSON_OGNOO;
    }

    public String getHORIO_TSEER_TSUTSALSAN_OGNOO() {
        return HORIO_TSEER_TSUTSALSAN_OGNOO;
    }
}
