package com.model.hos;

import com.mongodb.BasicDBObject;

import java.sql.Date;

public class MedremtgiiDataEntity extends BasicDBObject {
    public final String ID= "id";
	public final String SUREG_KEY= "sureg_key";
	public final String MTYPE= "mtype";
	public final String M_AGE= "m_age";
	public final String AGE_SEX= "age_sex";
	public final String UHER= "_uher";
	public final String HONI= "_honi";
	public final String YMAA= "_ymaa";
	public final String TEMEE= "_temee";
	public final String GAHAI= "_gahai";
	public final String BUSAD= "_busad";
	public final String ZERLEG_AMITAD= "_zerlegAmitad";
	public final String DELFLG= "delflg";
	public final String ACTFLG= "actflg";
	public final String CRE_AT= "cre_at";
	public final String CRE_BY= "cre_by";
	public final String MOD_AT= "mod_at";
	public final String MOD_BY= "mod_by";
	public final String RECORD_DATE= "record_date";
	public final String REC_TYPE= "rec_type";


    public Long getId() {
        return this.getLong(ID, 0);
    }

    public void setId(Long id) {
        this.put(ID, id);
    }

    public String getSureg_key() {
        return this.getString(SUREG_KEY, "");
    }

    public void setSureg_key(String sureg_key) {
        this.put(SUREG_KEY, sureg_key);
    }

    public String getMtype() {
        return this.getString(MTYPE, "");
    }

    public void setMtype(String mtype) {
        this.put(MTYPE, mtype);
    }

    public String getM_age() {
        return this.getString(M_AGE, "");
    }

    public void setM_age(String m_age) {
        this.put(M_AGE, m_age);
    }

    public String getAge_sex() {
        return this.getString(AGE_SEX, "");
    }

    public void setAge_sex(String age_sex) {
        this.put(AGE_SEX, age_sex);
    }

    public String getUher() {
        return this.getString(UHER, "");
    }

    public void setUher(String uher) {
        this.put(UHER, uher);
    }

    public String getHoni() {
        return this.getString(HONI, "");
    }

    public void setHoni(String honi) {
        this.put(HONI, honi);
    }

    public String getYmaa() {
        return this.getString(YMAA, "");
    }

    public void setYmaa(String ymaa) {
        this.put(YMAA, ymaa);
    }

    public String getTemee() {
        return this.getString(TEMEE, "");
    }

    public void setTemee(String temee) {
        this.put(TEMEE, temee);
    }

    public String getGahai() {
        return this.getString(GAHAI, "");
    }

    public void setGahai(String gahai) {
        this.put(GAHAI, gahai);
    }

    public String getBusad() {
        return this.getString(BUSAD, "");
    }

    public void setBusad(String busad) {
        this.put(BUSAD, busad);
    }

    public String getZerleg_amitad() {
        return this.getString(ZERLEG_AMITAD, "");
    }

    public void setZerleg_amitad(String zerleg_amitad) {
        this.put(ZERLEG_AMITAD, zerleg_amitad);
    }

    public String getDelflg() {
        return this.getString(DELFLG, "");
    }

    public void setDelflg(String delflg) {
        this.put(DELFLG, delflg);
    }

    public String getActflg() {
        return this.getString(ACTFLG, "");
    }

    public void setActflg(String actflg) {
        this.put(ACTFLG, actflg);
    }

    public Date getCre_at() {
        return (Date) this.getDate(CRE_AT);
    }

    public void setCre_at(Date cre_at) {
        this.put(CRE_AT, cre_at);
    }

    public String getCre_by() {
        return this.getString(CRE_BY, "");
    }

    public void setCre_by(String cre_by) {
        this.put(CRE_BY, cre_by);
    }

    public Date getMod_at() {
        return (Date) this.getDate(MOD_AT);
    }

    public void setMod_at(Date mod_at) {
        this.put(MOD_AT, mod_at);
    }

    public String getMod_by() {
        return this.getString(MOD_BY, "");
    }

    public void setMod_by(String mod_by) {
        this.put(MOD_BY, mod_by);
    }

    public Date getRecord_date() {
        return (Date) this.getDate(RECORD_DATE);
    }

    public void setRecord_date(Date record_date) {
        this.put(RECORD_DATE, record_date);
    }

    public String getRec_type() {
        return this.getString(REC_TYPE, "");
    }

    public void setRec_type(String rec_type) {
        this.put(REC_TYPE, rec_type);
    }
}
