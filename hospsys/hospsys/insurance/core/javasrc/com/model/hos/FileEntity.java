package com.model.hos;

import com.mongodb.BasicDBObject;

import java.sql.Date;

/**
 * Created by 24be10264 on 9/27/2017.
 */
public class FileEntity extends BasicDBObject{
    public final String ID = "id";
    public final String FYEAR = "fyear";
    public final String FMONTH = "fmonth";
    public final String FDESC = "fdesc";
    public final String FNAME = "fname";
    public final String FSIZE = "fsize";
    public final String DELFLG = "delflg";
    public final String ACTFLG = "actflg";
    public final String CRE_AT = "creat";
    public final String CRE_BY = "creby";
    public final String MOD_AT = "modat";
    public final String MOD_BY = "modby";



    public Long getId() {
        return this.getLong(ID, 0);
    }

    public void setId(Long id) {
        this.put(ID, id);
    }

    public String getYear() {
        return this.getString(FYEAR, "");
    }

    public void setYear(String year) {
        this.put(FYEAR, year);
    }

    public String getMonth() {
        return this.getString(FMONTH, "");
    }

    public void setMonth(String month) {
        this.put(FMONTH, month);
    }

    public String getFdesc() {
        return this.getString(FDESC, "");
    }

    public void setFdesc(String fdesc) {
        this.put(FDESC, fdesc);
    }

    public String getFname() {
        return this.getString(FNAME, "");
    }

    public void setFname(String fname) {
        this.put(FNAME, fname);
    }

    public String getSize() {
        return this.getString(FSIZE, "");
    }

    public void setSize(String size) {
        this.put(FSIZE, size);
    }

    public String getDelflg() {
        return this.getString(DELFLG, "");
    }

    public void setDelflg(String delflg) {
        this.put(DELFLG, delflg);
    }

    public String getActflg() {
        return this.getString(ACTFLG);
    }

    public void setActflg(String actflg) {
        this.put(ACTFLG, actflg);
    }

    public Date getCre_at() {
        return (Date) this.getDate(CRE_AT, null);
    }

    public void setCre_at(Date cre_at) {
        this.put(CRE_AT, cre_at);
    }

    public String getCre_by() {
        return this.getString(CRE_BY, "");
    }

    public void setCre_by(String cre_by) {
        this.put(CRE_BY, cre_by);
    }

    public Date getMod_at() {
        return (Date) this.getDate(MOD_AT);
    }

    public void setMod_at(Date mod_at) {
        this.put(MOD_AT, mod_at);
    }

    public String getMod_by() {
        return this.getString(MOD_BY);
    }

    public void setMod_by(String mod_by) {
        this.put(MOD_BY, mod_by);
    }
}
