package com.model.hos;


import com.enumclass.ErrorType;

/**
 * Created by 24be10264 on 8/22/2017.
 *
 *
 * Энэхүү класс нь дэлгэц нь дээр алдааны мессежийг дэлгэрэнгүй харуулахын тулд ашиглах класс юм.
 * TODO::Алдааны мессеж формат: [Алдааны төрөл]([Алдааны код])[Алдааны мессеж байна]
 *
 * Алдааны төрлүүдэд:
 *  1. FATAL        -- заавал засах шаардлагатай алдаануудаа    (хар)
 *  2. EXCEPTION    -- засах шаардлагатай алдаа                 (бор)
 *  3. ERROR        -- өгөгдлөөс хамаарч гарч болох алдаа       (улаан)
 *  4. WARNING      -- анхааруулга өгөх                         (шар)
 *  4. INFO         -- амжилттай & амжилтгүй мэдээлэл           (ногоон)
 *
 * Алдааны код (Заавал байна):
 *  1000 буюу 4 оронтой тоогоох эхэлсэн тоон утга байна.
 *
 * Алдааны мессеж (Заавал байна):
 *  Тухайн алдааны дэлгэрэнгүй тайлбар текст байх ба хэлний сонголтоос хамаарч тус хэл нь дээрх
 *  алдааны текстийг харуулах юм.
 *
 *
 * Алдааны мессеж нь хэлний сонголтоос хамаарах тул өгөгдлийн сангаас авахаар зохион байгуулагдах юм.
 * Энэхүү классын объектийг зөвхөн control класс дээр үүсгэж өгвөл тохиромжтой байх.
 */
public class ErrorEntity extends EntityClass{
    private Long id;
    private ErrorType errType;     // -- VALUE: FAT, ERR, EXC, WAR, INFO
    private Long errCode;       // -- VALUE: 1000 < ...
    private String errText;     // -- VALUE: blablabla
    private String errDesc;     // -- VALUE: desc //uursdoo ymar ued aldaa garsan baih talaar delgerengu bichne
    private String langCode;    // -- VALUE: MN || EN

    public ErrorEntity(){

    }

    public ErrorEntity(ErrorType errType, Long errCode) {
        this.errType = errType;
        this.errCode = errCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ErrorType getErrType() {
        return errType;
    }

    public void setErrType(ErrorType errType) {
        this.errType = errType;
    }

    public Long getErrCode() {
        return errCode;
    }

    public void setErrCode(Long errCode) {
        this.errCode = errCode;
    }

    public String getErrText() {
        return errText;
    }

    public void setErrText(String errText) {
        this.errText = errText;
    }

    public String getErrDesc() {
        return errDesc;
    }

    public void setErrDesc(String errDesc) {
        this.errDesc = errDesc;
    }

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }
}
