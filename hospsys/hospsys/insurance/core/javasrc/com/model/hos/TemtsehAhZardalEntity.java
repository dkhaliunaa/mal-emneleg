package com.model.hos;

import com.mongodb.BasicDBObject;

import java.sql.Date;

public class TemtsehAhZardalEntity extends BasicDBObject {

    public final String ID = "id";
    public final String FORM01KEY = "form01key";
    public final String CITYCODE = "citycode";
    public final String SUMCODE = "sumcode";
    public final String BAGCODE = "bagcode";
    public final String MALCHINNER = "malchinner";
    public final String AJIL_HESEG = "ajilHeseg";
	public final String MAL_EMCH = "malEmch";
	public final String BUSAD_HUMUUS = "busadHumuus";
	public final String TECHNIC_HEREGSEL = "tehnicHeregsel";
	public final String MEUN = "meun";
	public final String MEUT = "meut";
	public final String MEA = "mea";
	public final String TUR_ZB = "turZB";
	public final String NUHUN_OLG_ZARDAL = "nuhunOlgZardal";
    public final String RECORD_DATE = "recordDate";
    public final String DELFLG = "delflg";
    public final String ACTFLG = "actflg";
    public final String CRE_AT = "cre_at";
    public final String CRE_BY = "cre_by";
    public final String MOD_AT = "mod_at";
    public final String MOD_BY = "mod_by";


    public Long getId() {
        return this.getLong(ID, 0);
    }

    public void setId(Long id) {
        this.put(ID, id);
    }

    public String getForm01key() {
        return this.getString(FORM01KEY, "");
    }

    public void setForm01key(String form01key) {
        this.put(FORM01KEY, form01key);
    }

    public String getCitycode() {
        return this.getString(CITYCODE, "");
    }

    public void setCitycode(String citycode) {
        this.put(CITYCODE, citycode);
    }

    public String getSumcode() {
        return this.getString(SUMCODE, "");
    }

    public void setSumcode(String sumcode) {
        this.put(SUMCODE, sumcode);
    }

    public String getBagcode() {
        return this.getString(BAGCODE, "");
    }

    public void setBagcode(String bagcode) {
        this.put(BAGCODE, bagcode);
    }

    public String getMalchinner() {
        return this.getString(MALCHINNER, "");
    }

    public void setMalchinner(String malchinner) {
        this.put(MALCHINNER, malchinner);
    }


    public String getAjil_heseg() {
        return this.getString(AJIL_HESEG, "");
    }

    public void setAjil_heseg(String ajil_heseg) {
        this.put(AJIL_HESEG, ajil_heseg);
    }

    public String getMal_emch() {
        return this.getString(MAL_EMCH, "");
    }

    public void setMal_emch(String mal_emch) {
        this.put(MAL_EMCH, mal_emch);
    }

    public String getBusad_humuus() {
        return this.getString(BUSAD_HUMUUS, "");
    }

    public void setBusad_humuus(String busad_humuus) {
        this.put(BUSAD_HUMUUS, busad_humuus);
    }

    public String getTechnic_heregsel() {
        return this.getString(TECHNIC_HEREGSEL, "");
    }

    public void setTechnic_heregsel(String technic_heregsel) {
        this.put(TECHNIC_HEREGSEL, technic_heregsel);
    }

    public String getMeun() {
        return this.getString(MEUN, "");
    }

    public void setMeun(String meun) {
        this.put(MEUN, meun);
    }

    public String getMeut() {
        return this.getString(MEUT, "");
    }

    public void setMeut(String meut) {
        this.put(MEUT, meut);
    }

    public String getMea() {
        return this.getString(MEA, "");
    }

    public void setMea(String mea) {
        this.put(MEA, mea);
    }

    public String getTur_zb() {
        return this.getString(TUR_ZB, "");
    }

    public void setTur_zb(String tur_zb) {
        this.put(TUR_ZB, tur_zb);
    }

    public String getNuhun_olg_zardal() {
        return this.getString(NUHUN_OLG_ZARDAL, "");
    }

    public void setNuhun_olg_zardal(String nuhun_olg_zardal) {
        this.put(NUHUN_OLG_ZARDAL, nuhun_olg_zardal);
    }

    public Date getRecord_date() {
        return (Date) this.getDate(RECORD_DATE);
    }

    public void setRecord_date(Date record_date) {
        this.put(RECORD_DATE, record_date);
    }

    public String getDelflg() {
        return this.getString(DELFLG, "");
    }

    public void setDelflg(String delflg) {
        this.put(DELFLG, delflg);
    }

    public String getActflg() {
        return this.getString(ACTFLG, "");
    }

    public void setActflg(String actflg) {
        this.put(ACTFLG, actflg);
    }

    public Date getCre_at() {
        return (Date) this.getDate(CRE_AT);
    }

    public void setCre_at(Date cre_at) {
        this.put(CRE_AT, cre_at);
    }

    public String getCre_by() {
        return this.getString(CRE_BY, "");
    }

    public void setCre_by(String cre_by) {
        this.put(CRE_BY, cre_by);
    }

    public Date getMod_at() {
        return (Date) this.getDate(MOD_AT);
    }

    public void setMod_at(Date mod_at) {
        this.put(MOD_AT, mod_at);
    }

    public String getMod_by() {
        return this.getString(MOD_BY, "");
    }

    public void setMod_by(String mod_by) {
        this.put(MOD_BY, mod_by);
    }
}
