package com.model.hos;

import com.mongodb.BasicDBObject;

import java.sql.Date;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public class Form43Entity extends BasicDBObject {

    public static final String ID="id";
    public final String AIMAG="aimag";
    public final String SUM="sum";
    public static final String AJAHUINNEGJ="ajahuinNegj";
    public static final String EMBELDMELNER="emBeldmelNer";
    public static final String HEMJIHNEGJ="hemjihNegj";
    public static final String UILDVERLESENTOO="uildverlesenToo";
    public static final String NEGJUNE="negjUne";
    public static final String NIITUNE="niitUne";

    public final String AIMAG_EN="aimag_en";
    public final String SUM_EN="sum_en";
    public static final String AJAHUINNEGJ_EN="ajahuinNegj_en";
    public static final String EMBELDMELNER_EN="emBeldmelNer_en";
    public static final String HEMJIHNEGJ_EN="hemjihNegj_en";
    public final String AIMAGNER="aimagNer";
    public final String SUMNER="sumNer";

    public final String DELFLG = "delFlg";
    public final String ACTFLG = "actFlg";
    public final String MODBY = "modBy";
    public final String MODAT = "modAt";
    public final String CREBY = "creBy";
    public final String CREAT = "creAt";
    public final String RECORD_DATE = "recordDate";
    public final String SEARCH_RECORD_DATE = "searchRecordDate";
    public final String TUSGAIZOVSHOOROL = "tusgaiZovshoorol";
    public final String PARMAKOP = "parmakop";

    public Form43Entity() {

    }

    public Long getId() {
        return this.getLong(ID,0);
    }

    public void setId(Long id) {
        this.put(ID,id);
    }

    public String getAimag() {
        return this.getString(AIMAG);
    }

    public void setAimag(String aimag) {
        this.put(AIMAG,aimag);
    }

    public String getSum() {
        return this.getString(SUM,"");
    }

    public void setSum(String sum) {
        this.put(SUM,sum);
    }

    public String getAimag_en() {
        return this.getString(AIMAG_EN,"");
    }

    public void setAimag_en(String aimag_en) {
        this.put(AIMAG_EN,aimag_en);
    }

    public String getSum_en() {
        return this.getString(SUM_EN,"");
    }

    public void setSum_en(String sum_en) {
        this.put(SUM_EN,sum_en);
    }

    public String getAjahuinNegj() {
        return this.getString(AJAHUINNEGJ,"");
    }

    public void setAjahuinNegj(String ajahuinNegj) {
        this.put(AJAHUINNEGJ,ajahuinNegj);
    }

    public String getEmBeldmelNer() {
        return this.getString(EMBELDMELNER,"");
    }

    public void setEmBeldmelNer(String emBeldmelNer) {
        this.put(EMBELDMELNER,emBeldmelNer);
    }

    public String getHemjihNegj() {
        return this.getString(HEMJIHNEGJ,"");
    }

    public void setHemjihNegj(String hemjihNegj) {
        this.put(HEMJIHNEGJ,hemjihNegj);
    }

    public Long getUildverlesenToo() {
        return this.getLong(UILDVERLESENTOO,0);
    }

    public void setUildverlesenToo(Long uildverlesenToo) {
        this.put(UILDVERLESENTOO,uildverlesenToo);
    }

    public String getNegjUne() {
        return this.getString(NEGJUNE,"");
    }

    public void setNegjUne(String negjUne) {
        this.put(NEGJUNE,negjUne);
    }

    public String getNiitUne() {
        return this.getString(NIITUNE,"");
    }

    public void setNiitUne(String niitUne) {
        this.put(NIITUNE,niitUne);
    }

    public String getAjahuinNegj_en() {
        return this.getString(AJAHUINNEGJ_EN,"");
    }

    public void setAjahuinNegj_en(String ajahuinNegj_en) {
        this.put(AJAHUINNEGJ_EN,ajahuinNegj_en);
    }

    public String getEmBeldmelNer_en() {
        return this.getString(EMBELDMELNER_EN,"");
    }

    public void setEmBeldmelNer_en(String emBeldmelNer_en) {
        this.put(EMBELDMELNER_EN,emBeldmelNer_en);
    }

    public String getHemjihNegj_en() {
        return this.getString(HEMJIHNEGJ_EN,"");
    }

    public void setHemjihNegj_en(String hemjihNegj_en) {
        this.put(HEMJIHNEGJ_EN,hemjihNegj_en);
    }

    public String getDelflg() {
        return this.getString(DELFLG, "");
    }

    public void setDelflg(String delflg) {
        this.put(DELFLG, delflg);
    }

    public String getActflg() {
        return this.getString(ACTFLG, "");
    }

    public void setActflg(String actflg) {
        this.put(ACTFLG, actflg);
    }

    public String getModby() {
        return getString(MODBY, "");
    }

    public void setModby(String modby) {
        this.put(MODBY, modby);
    }

    public java.sql.Date getModAt() {
        return (java.sql.Date) getDate(MODAT);
    }

    public void setModAt(java.sql.Date modAt){
        this.put(MODAT, modAt);
    }

    public String getCreby() {
        return getString(CREBY, "");
    }

    public void setCreby(String creby) {
        this.put(CREBY, creby);
    }

    public java.sql.Date getCreat() {
        return (java.sql.Date) getDate(CREAT, null);
    }

    public void setCreat(java.sql.Date creat) {
        this.put(CREAT, creat);
    }

    public Date getTusgaiZovshoorol() {
        return (Date) this.getDate(TUSGAIZOVSHOOROL, null);
    }

    public void setTusgaiZovshoorol(Date tusgaiZovshoorol) {
        this.put(TUSGAIZOVSHOOROL, tusgaiZovshoorol);
    }

    public Long getParmakop() {
        return this.getLong(PARMAKOP, 0);
    }

    public void setParmakop(Long parmakop) {
        this.put(PARMAKOP,parmakop);
    }



    public Date getRecordDate() {
        return (Date) this.getDate(RECORD_DATE, null);
    }

    public void setRecordDate(Date recordDate) {
        this.put(RECORD_DATE, recordDate);
    }

    public Date getSearchRecordDate() {
        return (Date) this.getDate(SEARCH_RECORD_DATE, null);
    }

    public void setSearchRecordDate(Date recordDate) {
        this.put(SEARCH_RECORD_DATE, recordDate);
    }

    public String getAimagNer() {
        return this.getString(AIMAGNER,"");
    }

    public void setAimagNer(String aimagNer) {
        this.put(AIMAGNER,aimagNer);
    }

    public String getSumNer() {
        return this.getString(SUMNER,"");
    }

    public void setSumNer(String sumNer) {
        this.put(SUMNER,sumNer);
    }

}
