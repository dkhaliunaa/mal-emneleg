package com.model.hos;

/**
 * Created by Tiku on 8/20/2017.
 */
public class HFormEntity extends EntityClass {
    /**
     * 1	id
     * 2	formid
     * 3	field_id
     * 4	fld_name
     * 5	fld_title_mn
     * 6	fld_title_en
     * 7	fld_req
     * 8	fld_mask
     * 9	fld_defval
     * 10	fld_sort_id
     * 11	fld_type
     * 12	cmb_id
     * 13	cmb_ischild
     * 14	cmb_childid
     * 15	cmb_childfldid
     * 16	fld_date_format
     **/

    private Long id;
    private String formId;
    private String frmFldId;
    private String fieldId;
    private String fldName;
    private String fldTitleMN;
    private String fldTitleEN;
    private String fldReq;
    private String fldMask;
    private String fldDefVal;
    private String fldSortId;
    private String fldType;
    private String cmbId;
    private String cmbIsChild;
    private String cmbChildId;
    private String cmbChildfldId;
    private String fldDateFormat;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFrmFldId() {
        return frmFldId;
    }

    public void setFrmFldId(String frmFldId) {
        this.frmFldId = frmFldId;
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getFieldId() {
        return fieldId;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }

    public String getFldName() {
        return fldName;
    }

    public void setFldName(String fldName) {
        this.fldName = fldName;
    }

    public String getFldTitleMN() {
        return fldTitleMN;
    }

    public void setFldTitleMN(String fldTitleMN) {
        this.fldTitleMN = fldTitleMN;
    }

    public String getFldTitleEN() {
        return fldTitleEN;
    }

    public void setFldTitleEN(String fldTitleEN) {
        this.fldTitleEN = fldTitleEN;
    }

    public String getFldReq() {
        return fldReq;
    }

    public void setFldReq(String fldReq) {
        this.fldReq = fldReq;
    }

    public String getFldMask() {
        return fldMask;
    }

    public void setFldMask(String flgMask) {
        this.fldMask = flgMask;
    }

    public String getFldDefVal() {
        return fldDefVal;
    }

    public void setFldDefVal(String fldDefVal) {
        this.fldDefVal = fldDefVal;
    }

    public String getFldSortId() {
        return fldSortId;
    }

    public void setFldSortId(String fldSortId) {
        this.fldSortId = fldSortId;
    }

    public String getFldType() {
        return fldType;
    }

    public void setFldType(String fldType) {
        this.fldType = fldType;
    }

    public String getCmbId() {
        return cmbId;
    }

    public void setCmbId(String cmbId) {
        this.cmbId = cmbId;
    }

    public String getCmbIsChild() {
        return cmbIsChild;
    }

    public void setCmbIsChild(String cmbIsChild) {
        this.cmbIsChild = cmbIsChild;
    }

    public String getCmbChildId() {
        return cmbChildId;
    }

    public void setCmbChildId(String cmbChildId) {
        this.cmbChildId = cmbChildId;
    }

    public String getCmbChildfldId() {
        return cmbChildfldId;
    }

    public void setCmbChildfldId(String cmbChildfldId) {
        this.cmbChildfldId = cmbChildfldId;
    }

    public String getFldDateFormat() {
        return fldDateFormat;
    }

    public void setFldDateFormat(String fldDateFormat) {
        this.fldDateFormat = fldDateFormat;
    }
}
