package com.model.hos;

/**
 * Created by 24be10264 on 9/5/2017.
 */

import com.mongodb.BasicDBObject;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public class Form14Entity extends BasicDBObject {
    public static final String ID = "id";
    public final String AIMAG="aimag";
    public final String AIMAGNER="aimagNer";
    public final String SUM="sum";
    public final String SUMNER="sumNer";
    public final String SHALGAHGAZAR = "shalgahGazar";
    public static final String UZLEGTOO = "uzlegToo";
    public static final String BAIGUULLAGATOO = "baiguullagaToo";
    public static final String AJAHUINNEGJTOO = "ajAhuinNegjToo";
    public static final String IRGEDTOO = "irgedToo";
    public static final String HUULIINBAIGSHILJSEN = "huuliinBaigShiljsen";
    public static final String ZAHIRGAAHARITOO = "zahirgaaHariToo";
    public static final String TORGUULIHEMJEE = "torguuliHemjee";
    public static final String TOLBOR = "tolbor";
    public static final String TORGUULIAR = "torguuliar";
    public static final String TOLBOROOR = "tolboroor";

    public static final String SHALGAHGAZAR_EN = "shalgahGazar_en";
    public final String AIMAG_EN="aimag_en";
    public final String SUM_EN="sum_en";
    public static final String UZLEGTOO_EN = "uzlegToo_en";
    public static final String BAIGUULLAGATOO_EN = "baiguullagaToo_en";
    public static final String AJAHUINNEGJTOO_EN = "ajAhuinNegjToo_en";
    public static final String IRGEDTOO_EN = "irgedToo_en";
    public static final String HUULIINBAIGSHILJSEN_EN = "huuliinBaigShiljsen_en";
    public static final String ZAHIRGAAHARITOO_EN = "zahirgaaHariToo_en";
    public static final String TORGUULIHEMJEE_EN = "torguuliHemjee_en";
    public static final String TOLBOR_EN = "tolbor_en";
    public static final String TORGUULIAR_EN = "torguuliar_en";
    public static final String TOLBOROOR_EN = "tolboroor_en";

    public static final String DELFLG = "delFlg";
    public static final String ACTFLG = "actFlg";
    public static final String MODBY = "modBy";
    public static final String MODAT = "modAt";
    public static final String CREBY = "creBy";
    public static final String CREAT = "creAt";

    public final String RECORD_DATE = "recordDate";
    public final String SEARCH_RECORD_DATE = "searchRecordDate";


    public Long getUzlegToo_en() {
        return this.getLong(UZLEGTOO_EN, 0);
    }

    public void setUzlegToo_en(Long uzlegToo_en) {
        this.put(UZLEGTOO_EN, uzlegToo_en);
    }

    public String getAimag() {
        return this.getString(AIMAG);
    }

    public void setAimag(String aimag) {
        this.put(AIMAG,aimag);
    }

    public String getSum() {
        return this.getString(SUM,"");
    }

    public void setSum(String sum) {
        this.put(SUM,sum);
    }

    public String getAimag_en() {
        return this.getString(AIMAG_EN,"");
    }

    public void setAimag_en(String aimag_en) {
        this.put(AIMAG_EN,aimag_en);
    }

    public String getSum_en() {
        return this.getString(SUM_EN,"");
    }

    public void setSum_en(String sum_en) {
        this.put(SUM_EN,sum_en);
    }

    public Long getBaiguullagaToo_en() {
        return this.getLong(BAIGUULLAGATOO_EN, 0);
    }

    public void setBaiguullagaToo_en(Long baiguullagaToo_en) {
        this.put(BAIGUULLAGATOO_EN, baiguullagaToo_en);
    }

    public Long getAjAhuinNegjToo_en() {
        return this.getLong(AJAHUINNEGJTOO_EN, 0);
    }

    public void setAjAhuinNegjToo_en(Long ajAhuinNegjToo_en) {
        this.put(AJAHUINNEGJTOO_EN, ajAhuinNegjToo_en);
    }

    public Long getIrgedToo_en() {
        return this.getLong(IRGEDTOO_EN, 0);
    }

    public void setIrgedToo_en(Long irgedToo_en) {
        this.put(IRGEDTOO_EN, irgedToo_en);
    }

    public Long getHuuliinBaigShiljsen_en() {
        return this.getLong(HUULIINBAIGSHILJSEN_EN, 0);
    }

    public void setHuuliinBaigShiljsen_en(Long huuliinBaigShiljsen_en) {
        this.put(HUULIINBAIGSHILJSEN_EN, huuliinBaigShiljsen_en);
    }

    public Long getZahirgaaHariToo_en() {
        return this.getLong(ZAHIRGAAHARITOO_EN, 0);
    }

    public void setZahirgaaHariToo_en(Long zahirgaaHariToo_en) {
        this.put(ZAHIRGAAHARITOO_EN, zahirgaaHariToo_en);
    }

    public BigDecimal getTorguuliHemjee_en() {
        return BigDecimal.valueOf(this.getDouble(TORGUULIHEMJEE_EN, 0));
    }

    public void setTorguuliHemjee_en(BigDecimal torguuliHemjee_en) {
        this.put(TORGUULIHEMJEE_EN, torguuliHemjee_en);
    }

    public BigDecimal getTolbor_en() {
        return BigDecimal.valueOf(this.getDouble(TOLBOR_EN, 0));
    }

    public void setTolbor_en(BigDecimal tolbor_en) {
        this.put(TOLBOR_EN, tolbor_en);
    }

    public BigDecimal getTorguuliar_en() {
        return BigDecimal.valueOf(getDouble(TORGUULIAR_EN, 0));
    }

    public void setTorguuliar_en(BigDecimal torguuliar_en) {
        this.put(TORGUULIAR_EN, torguuliar_en);
    }

    public BigDecimal getTolboroor_en() {
        return BigDecimal.valueOf(this.getDouble(TOLBOR_EN, 0));
    }

    public void setTolboroor_en(BigDecimal tolboroor_en) {
        this.put(TOLBOROOR_EN, tolboroor_en);
    }

    public Form14Entity() {

    }

    public Long getId() {
        return this.getLong(ID, 0);
    }

    public void setId(Long id) {
        this.put(ID, id);
    }

    public String getShalgahGazar() {
        return getString(SHALGAHGAZAR, "");
    }

    public void setShalgahGazar(String shalgahGazar) {
        this.put(SHALGAHGAZAR, shalgahGazar);
    }


    public String getAimagNer() {
        return this.getString(AIMAGNER,"");
    }

    public void setAimagNer(String aimagNer) {
        this.put(AIMAGNER,aimagNer);
    }

    public String getSumNer() {
        return this.getString(SUMNER,"");
    }

    public void setSumNer(String sumNer) {
        this.put(SUMNER,sumNer);
    }

    public Long getUzlegToo() {
        return this.getLong(UZLEGTOO, 0);
    }

    public void setUzlegToo(Long uzlegToo) {
        this.put(UZLEGTOO, uzlegToo);
    }

    public Long getBaiguullagaToo() {
        return this.getLong(BAIGUULLAGATOO, 0);
    }

    public void setBaiguullagaToo(Long baiguullagaToo) {
        this.put(BAIGUULLAGATOO, baiguullagaToo);
    }

    public Long getAjAhuinNegjToo() {
        return this.getLong(AJAHUINNEGJTOO, 0);
    }

    public void setAjAhuinNegjToo(Long ajAhuinNegjToo) {
        this.put(AJAHUINNEGJTOO, ajAhuinNegjToo);
    }

    public Long getIrgedToo() {
        return this.getLong(IRGEDTOO, 0);
    }

    public void setIrgedToo(Long irgedToo) {
        this.put(IRGEDTOO, irgedToo);
    }

    public Long getHuuliinBaigShiljsen() {
        return this.getLong(HUULIINBAIGSHILJSEN, 0);
    }

    public void setHuuliinBaigShiljsen(Long huuliinBaigShiljsen) {
        this.put(HUULIINBAIGSHILJSEN, huuliinBaigShiljsen);
    }

    public Long getZahirgaaHariToo() {
        return this.getLong(ZAHIRGAAHARITOO, 0);
    }

    public void setZahirgaaHariToo(Long zahirgaaHariToo) {
        this.put(ZAHIRGAAHARITOO, zahirgaaHariToo);
    }

    public BigDecimal getTorguuliHemjee() {
        return BigDecimal.valueOf(getDouble(TORGUULIHEMJEE, 0));
    }

    public void setTorguuliHemjee(BigDecimal torguuliHemjee) {
        this.put(TORGUULIHEMJEE, torguuliHemjee);
    }

    public BigDecimal getTolbor() {
        return BigDecimal.valueOf(getDouble(TOLBOR, 0));
    }

    public void setTolbor(BigDecimal tolbor) {
        this.put(TOLBOR, tolbor);
    }

    public BigDecimal getTorguuliar() {
        return BigDecimal.valueOf(this.getDouble(TORGUULIAR, 0));
    }

    public void setTorguuliar(BigDecimal torguuliar) {
        this.put(TORGUULIAR, torguuliar);
    }

    public BigDecimal getTolboroor() {
        return BigDecimal.valueOf(this.getDouble(TOLBOROOR, 0));
    }

    public void setTolboroor(BigDecimal tolboroor) {
        this.put(TOLBOROOR, tolboroor);
    }

    public String getShalgahGazar_en() {
        return this.getString(SHALGAHGAZAR_EN, "");
    }

    public void setShalgahGazar_en(String shalgahGazar_en) {
        this.put(SHALGAHGAZAR_EN, shalgahGazar_en);
    }


    public String getDELFLG() {
        return this.getString(DELFLG, "");
    }

    public void setDelflg(String delflg) {
        this.put(DELFLG, delflg);
    }

    public String getACTFLG() {
        return this.getString(ACTFLG, "");
    }

    public void setActflg(String actflg) {
        this.put(ACTFLG, actflg);
    }

    public String getMODBY() {
        return getString(MODBY, "");
    }

    public void setModby(String modby) {
        this.put(MODBY, modby);
    }

    public java.sql.Date getMODAT() {
        return (java.sql.Date) getDate(MODAT);
    }

    public void setModat(java.sql.Date modat){this.put(MODAT,modat);}

    public String getCREBY() {
        return getString(CREBY, "");
    }

    public void setCreby(String creby) {
        this.put(CREBY, creby);
    }

    public java.sql.Date getCREAT() {
        return (java.sql.Date) getDate(CREAT);
    }

    public void setCreat(java.sql.Date creat) {
        this.put(CREAT, creat);
    }

    public Date getRecordDate() {
        return (Date) this.getDate(RECORD_DATE, null);
    }

    public void setRecordDate(Date recordDate) {
        this.put(RECORD_DATE, recordDate);
    }

    public Date getSearchRecordDate() {
        return (Date) this.getDate(SEARCH_RECORD_DATE, null);
    }

    public void setSearchRecordDate(Date recordDate) {
        this.put(SEARCH_RECORD_DATE, recordDate);
    }
}
