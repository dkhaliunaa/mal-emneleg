package com.model.hos;

import com.mongodb.BasicDBObject;
import java.sql.Date;

public class Form11Entity extends BasicDBObject {
    public final String ID="id";

    public final String AIMAG="aimag";
    public final String SUM="sum";
    public final String BAGCODE="bagcode";
    public final String AIMAGNER="aimagNer";
    public final String DEEJIINTURUL="deejiinTurul_mn";
    public final String SUMBAGNER="sumBagNer_mn";
    public final String SUMNER="sumNer";
    public final String MALTURUL="malTurul_mn";
    public final String TOOTOLGOI="tooTolgoi_mn";
    public final String HAANASHINJILSEN="haanaShinjilsen_mn";
    public final String SHINJILSENARGA="shinjilsenArga_mn";
    public final String ERUUL="eruul_mn";
    public final String UVCHTEI="uvchtei_mn";
    public final String ONOSH="onosh_mn";

    public final String AIMAG_EN="aimag_en";
    public final String SUM_EN="sum_en";
    public final String DEEJIINTURUL_EN="deejiinTurul_en";
    public final String MURIINDUGAAR_EN="muriinDugaar_en";
    public final String SUMBAGNER_EN="sumBagNer_en";
    public final String MALTURUL_EN="malTurul_en";
    public final String TOOTOLGOI_EN="tooTolgoi_en";
    public final String HAANASHINJILSEN_EN="haanaShinjilsen_en";
    public final String SHINJILSENARGA_EN="shinjilsenArga_en";
    public final String ERUUL_EN="eruul_en";
    public final String UVCHTEI_EN="uvchtei_en";
    public final String ONOSH_EN="onosh_en";

    public final String RECORD_DATE = "recordDate";
    public final String SEARCH_RECORD_DATE = "searchRecordDate";

    public final String DELFLG = "delflg";
    public final String ACTFLG = "actflg";
    public final String MODBY = "mod_by";
    public final String MODAT = "mod_at";
    public final String CREBY = "cre_by";
    public final String CREAT = "cre_at";

    public Form11Entity(){

    }

    public Long getId() {
        return this.getLong(ID,0);
    }

    public void setId(Long id) {
        this.put(ID,id);
    }

    public String getDeejiinTurul() {
        return this.getString(DEEJIINTURUL,"");
    }

    public void setDeejiinTurul(String deejiinTurul) {
        this.put(DEEJIINTURUL,deejiinTurul);
    }

    public String getSumBagNer() {
        return this.getString(SUMBAGNER,"");
    }

    public void setSumBagNer(String sumBagNer) {
        this.put(SUMBAGNER,sumBagNer);
    }

    public String getMalTurul() {
        return this.getString(MALTURUL,"");
    }

    public void setMalTurul(String malTurul) {
        this.put(MALTURUL,malTurul);
    }

    public Long getTooTolgoi() {
        return this.getLong(TOOTOLGOI,0);
    }

    public void setTooTolgoi(Long tooTolgoi) {
        this.put(TOOTOLGOI,tooTolgoi);
    }

    public String getHaanaShinjilsen() {
        return this.getString(HAANASHINJILSEN,"");
    }

    public void setHaanaShinjilsen(String haanaShinjilsen) {
        this.put(HAANASHINJILSEN,haanaShinjilsen);
    }

    public String getShinjilsenArga() {
        return this.getString(SHINJILSENARGA,"");
    }

    public void setShinjilsenArga(String shinjilsenArga) {
        this.put(SHINJILSENARGA,shinjilsenArga);
    }

    public Long getEruul() {
        return this.getLong(ERUUL,0);
    }

    public void setEruul(Long eruul) {
        this.getLong(ERUUL,eruul);
    }

    public Long getUvchtei() {
        return this.getLong(UVCHTEI,0);
    }

    public void setUvchtei(Long uvchtei) {
        this.put(UVCHTEI,uvchtei);
    }

    public Long getOnosh() {
        return this.getLong(ONOSH,0);
    }

    public void setOnosh(Long onosh) {
        this.put(ONOSH,onosh);
    }

    public String getDeejiinTurul_en() {
        return this.getString(DEEJIINTURUL_EN,"");
    }

    public void setDeejiinTurul_en(String deejiinTurul_en) {
        this.put(DEEJIINTURUL_EN,deejiinTurul_en);
    }

    public String getSumBagNer_en() {
        return this.getString(SUMBAGNER,"");
    }

    public void setSumBagNer_en(String sumBagNer_en) {
        this.put(SUMBAGNER_EN,sumBagNer_en);
    }

    public String getMalTurul_en() {
        return this.getString(MALTURUL_EN,"");
    }

    public void setMalTurul_en(String malTurul_en) {
        this.put(MALTURUL_EN,malTurul_en);
    }

    public Long getTooTolgoi_en() {
        return getLong(TOOTOLGOI_EN,0);
    }

    public void setTooTolgoi_en(Long tooTolgoi_en) {
        this.put(TOOTOLGOI_EN,0);
    }

    public String getHaanaShinjilsen_en() {
        return this.getString(HAANASHINJILSEN_EN,"");
    }

    public void setHaanaShinjilsen_en(String haanaShinjilsen_en) {
        this.put(HAANASHINJILSEN_EN,haanaShinjilsen_en);
    }

    public String getShinjilsenArga_en() {
        return this.getString(SHINJILSENARGA_EN,"");
    }

    public void setShinjilsenArga_en(String shinjilsenArga_en) {
        this.put(SHINJILSENARGA_EN,shinjilsenArga_en);
    }

    public Long getEruul_en() {
        return this.getLong(ERUUL_EN,0);
    }

    public void setEruul_en(Long eruul_en) {
        this.put(ERUUL_EN,eruul_en);
    }

    public Long getUvchtei_en() {
        return this.getLong(UVCHTEI_EN,0);
    }

    public void setUvchtei_en(Long uvchtei_en) {
        this.put(UVCHTEI_EN,uvchtei_en);
    }

    public Long getOnosh_en() {
        return getLong(ONOSH_EN,0);
    }

    public void setOnosh_en(Long onosh_en) {
        this.put(ONOSH_EN,onosh_en);
    }

    public String getAimag() {
        return getString(AIMAG,"");
    }

    public void setAimag(String aimag) {
        this.put(AIMAG,aimag);
    }

    public String getAimag_en() {
        return this.getString(AIMAG_EN,"");
    }

    public void setAimag_en(String aimag_en) {
        this.put(AIMAG_EN,aimag_en);
    }

    public String getDelflg() {
        return this.getString(DELFLG, "");
    }

    public void setDelflg(String delflg) {
        this.put(DELFLG, delflg);
    }

    public String getActflg() {
        return this.getString(ACTFLG, "");
    }

    public void setActflg(String actflg) {
        this.put(ACTFLG, actflg);
    }

    public String getModby() {
        return getString(MODBY, "");
    }

    public void setModby(String modby) {
        this.put(MODBY, modby);
    }

    public Date getModAt() {
        return (Date) getDate(MODAT);
    }

    public void setModAt(Date modAt){
        this.put(MODAT, modAt);
    }

    public String getCreby() {
        return getString(CREBY, "");
    }

    public void setCreby(String creby) {
        this.put(CREBY, creby);
    }

    public Date getCreat() {
        return (Date) getDate(CREAT, null);
    }

    public void setCreat(Date creat) {
        this.put(CREAT, creat);
    }

    public String getSum() {
        return this.getString(SUM,"");
    }

    public void setSum(String sum) {
        this.put(SUM,sum);
    }

    public String getSum_en() {
        return this.getString(SUM_EN,"");
    }

    public void setSum_en(String sum) {
        this.put(SUM_EN, sum);
    }

    public String getAimagNer() {
        return this.getString(AIMAGNER,"");
    }

    public void setAimagNer(String aimagNer) {
        this.put(AIMAGNER,aimagNer);
    }

    public String getSumNer() {
        return this.getString(SUMNER,"");
    }

    public void setSumNer(String sumNer) {
        this.put(SUMNER,sumNer);
    }

    public String getBagCode() {
        return this.getString(BAGCODE,"");
    }

    public void setBagCode(String bagCode) {
        this.put(BAGCODE,bagCode);
    }

    public Date getRecordDate() {
        return (Date) this.getDate(RECORD_DATE, null);
    }

    public void setRecordDate(Date recordDate) {
        this.put(RECORD_DATE, recordDate);
    }

    public Date getSearchRecordDate() {
        return (Date) this.getDate(SEARCH_RECORD_DATE, null);
    }

    public void setSearchRecordDate(Date recordDate) {
        this.put(SEARCH_RECORD_DATE, recordDate);
    }
}