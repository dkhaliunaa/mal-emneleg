package com.model.hos;

/**
 * Created by Tiku on 8/20/2017.
 */
public class RbtnEntity extends EntityClass{
    /**
     * 1	id
     * 2	rbtnid
     * 3	val
     * 4	desc_mn
     * 5	desc_en
     * 6	strfield1
     * 7	strfield2
     **/

    private Long id;
    private String frmFldId;
    private String rbtnId;
    private String val;
    private String descMn;
    private String descEn;
    private String strFld1;
    private String strFld2;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFrmFldId() {
        return frmFldId;
    }

    public void setFrmFldId(String frmFldId) {
        this.frmFldId = frmFldId;
    }

    public String getRbtnId() {
        return rbtnId;
    }

    public void setRbtnId(String rbtnId) {
        this.rbtnId = rbtnId;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public String getDescMn() {
        return descMn;
    }

    public void setDescMn(String descMn) {
        this.descMn = descMn;
    }

    public String getDescEn() {
        return descEn;
    }

    public void setDescEn(String descEn) {
        this.descEn = descEn;
    }

    public String getStrFld1() {
        return strFld1;
    }

    public void setStrFld1(String strFld1) {
        this.strFld1 = strFld1;
    }

    public String getStrFld2() {
        return strFld2;
    }

    public void setStrFld2(String strFld2) {
        this.strFld2 = strFld2;
    }
}
