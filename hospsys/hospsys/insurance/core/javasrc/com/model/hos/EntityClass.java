package com.model.hos;

import java.sql.Date;

/**
 * Created by 24be10264 on 8/18/2017.
 *
 * Хүснэгтүүдэд байж болох ижил төстэй чанаруудыг агуулсан entity класс юм.
 * Шинээр үүссэн entity класс болгон энэхүү классаас удамших юм.
 *
 * Цаашдаа лог бичих гэх мэт ашиглах юм.
 */
public class EntityClass {
    protected String delFlg;
    protected String actFlg;
    protected String modBy;
    protected Date modAt;
    protected String creBy;
    protected Date creAt;

    public String getDelFlg() {
        return delFlg;
    }

    public void setDelFlg(String delFlg) {
        this.delFlg = delFlg;
    }

    public String getActFlg() {
        return actFlg;
    }

    public void setActFlg(String actFlg) {
        this.actFlg = actFlg;
    }

    public String getModBy() {
        return modBy;
    }

    public void setModBy(String modBy) {
        this.modBy = modBy;
    }

    public Date getModAt() {
        return modAt;
    }

    public void setModAt(Date modAt) {
        this.modAt = modAt;
    }

    public String getCreBy() {
        return creBy;
    }

    public void setCreBy(String creBy) {
        this.creBy = creBy;
    }

    public Date getCreAt() {
        return creAt;
    }

    public void setCreAt(Date creAt) {
        this.creAt = creAt;
    }
}
