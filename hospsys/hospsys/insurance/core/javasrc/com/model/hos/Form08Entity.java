package com.model.hos;

import com.mongodb.BasicDBObject;

import java.math.BigDecimal;
import java.sql.Date;

public class Form08Entity extends BasicDBObject {
    public final String ID="id";
    public final String AIMAG="aimag";
    public final String AIMAGNER="aimagNer";
    public final String SUM="sum";
    public final String SUMNER="sumNer";
    public final String ARGA_HEMJEE_NER="argaHemjeeNer";
    public final String MURDUGAAR="murDugaar";
    public final String NER="ner";
    public final String ZARTSUULSAN_HEMJEE="zartsuulsanHemjee";
    public final String MALTURUL="malturul";
    public final String MALTURULNER="malTurulNer";
    public final String TOLOVLOGOO="tolovlogoo";
    public final String GUITSETGEL="guitsetgel";
    public final String HUVI="huvi";

    public final String AIMAG_EN="aimag_en";
    public final String SUM_EN="sum_en";
    public final String ARGA_HEMJEE_NER_EN="ARGA_HEMJEE_NER_en";
    public final String MURDUGAAR_EN="murDugaar_en";
    public final String NER_EN="ner_en";
    public final String ZARTSUULSAN_HEMJEE_EN="zartsuulsanHemjee_en";
    public final String MALTURUL_EN="malTurul_en";
    public final String TOLOVLOGOO_EN="tolovlogoo_en";
    public final String GUITSETGEL_EN="guitsetgel_en";
    public final String HUVI_EN="huvi_en";

    public final String RECORD_DATE = "recordDate";
    public final String SEARCH_RECORD_DATE = "searchRecordDate";

    public final String DELFLG = "delFlg";
    public final String ACTFLG = "actFlg";
    public final String MODBY = "modBy";
    public final String MODAT = "modAt";
    public final String CREBY = "creBy";
    public final String CREAT = "creAt";

    public final String OLGOSON_TUN="olgosonTun";
    public final String HEREGLESEN_TUN="hereglesenTun";
    public final String USTGASAN_TUN="ustgasanTun";
    public final String NUUTSULSEN_TUN="nuutsulsenTun";


    public Long getNUUTSULSEN_TUN() {
        return this.getLong(NUUTSULSEN_TUN,0);
    }

    public void setNUUTSULSEN_TUN(Long nuutsulsenTun) {
        this.put(NUUTSULSEN_TUN,nuutsulsenTun);
    }

    public Long getUSTGASAN_TUN() {
        return this.getLong(USTGASAN_TUN,0);
    }

    public void setUSTGASAN_TUN(Long ustgasanTun) {
        this.put(USTGASAN_TUN,ustgasanTun);
    }

    public Long getHEREGLESEN_TUN() {
        return this.getLong(HEREGLESEN_TUN,0);
    }

    public void setHEREGLESEN_TUN(Long hereglesenTun) {
        this.put(HEREGLESEN_TUN,hereglesenTun);
    }

    public Long getOLGOSON_TUN() {
        return this.getLong(OLGOSON_TUN,0);
    }

    public void setOLGOSON_TUN(Long olgosonTun) {
        this.put(OLGOSON_TUN,olgosonTun);
    }

    public Long getId() {
        return this.getLong(ID,0);
    }

    public void setId(Long id) {
        this.put(ID,id);
    }

    public String getArgaHemjeeNer() {
        return this.getString(ARGA_HEMJEE_NER,"");
    }

    public void setArgaHemjeeNer(String argaHemjeeNer) {
        this.put(ARGA_HEMJEE_NER,argaHemjeeNer);
    }

    public BigDecimal getMurDugaar() {
        return BigDecimal.valueOf(this.getDouble(MURDUGAAR,0));
    }

    public void setMurDugaar(BigDecimal murDugaar) {
        this.put(MURDUGAAR,murDugaar);
    }

    public String getNer() {
        return this.getString(NER,"");
    }

    public void setNer(String ner) {
        this.put(NER,ner);
    }

    public String getZartsuulsanHemjee() {
        return this.getString(ZARTSUULSAN_HEMJEE,"");
    }

    public void setZartsuulsanHemjee(String zartsuulsanHemjee) {
        this.put(ZARTSUULSAN_HEMJEE,zartsuulsanHemjee);
    }

    public String getMalTurul() {
        return this.getString(MALTURUL,"");
    }

    public void setMalTurul(String malTurul) {
        this.put(MALTURUL,malTurul);
    }

    public String getTolovlogoo() {
        return this.getString(TOLOVLOGOO,"");
    }

    public void setTolovlogoo(String tolovlogoo) {
        this.put(TOLOVLOGOO,tolovlogoo);
    }

    public String getGuitsetgel() {
        return this.getString(GUITSETGEL,"");
    }

    public void setGuitsetgel(String guitsetgel) {
        this.put(GUITSETGEL,guitsetgel);
    }

    public String getHuvi() {
        return this.getString(HUVI,"");
    }

    public void setHuvi(String huvi) {
        this.put(HUVI,huvi);
    }

    public String getArgaHemjeeNer_en() {
        return this.getString(ARGA_HEMJEE_NER_EN,"");
    }

    public void setArgaHemjeeNer_en(String argaHemjeeNer_en) {
        this.put(ARGA_HEMJEE_NER_EN,argaHemjeeNer_en);
    }

    public BigDecimal getMurDugaar_en() {
        return BigDecimal.valueOf(this.getDouble(MURDUGAAR_EN,0));
    }

    public void setMurDugaar_en(BigDecimal murDugaar_en) {
        this.put(MURDUGAAR_EN,murDugaar_en);
    }

    public String getNer_en() {
        return this.getString(NER_EN,"");
    }

    public void setNer_en(String ner_en) {
        this.put(NER_EN,ner_en);
    }

    public String getZartsuulsanHemjee_en() {
        return this.getString(ZARTSUULSAN_HEMJEE_EN,"");
    }

    public void setZartsuulsanHemjee_en(String zartsuulsanHemjee_en) {
        this.put(ZARTSUULSAN_HEMJEE_EN,zartsuulsanHemjee_en);
    }

    public String getMalTurul_en() {
        return this.getString(MALTURUL_EN,"");
    }

    public void setMalTurul_en(String malTurul_en) {
        this.put(MALTURUL_EN,malTurul_en);
    }

    public String getTolovlogoo_en() {
        return this.getString(TOLOVLOGOO_EN,"");
    }

    public void setTolovlogoo_en(String tolovlogoo_en) {
        this.put(TOLOVLOGOO_EN,tolovlogoo_en);
    }

    public String getGuitsetgel_en() {
        return this.getString(GUITSETGEL_EN,"");
    }

    public void setGuitsetgel_en(String guitsetgel_en) {
        this.put(GUITSETGEL_EN,guitsetgel_en);
    }

    public String getHuvi_en() {
        return this.getString(HUVI,"");
    }

    public void setHuvi_en(String huvi_en) {
        this.put(HUVI_EN,huvi_en);
    }

    public String getAimag() {
        return this.getString(AIMAG);
    }

    public void setAimag(String aimag) {
        this.put(AIMAG,aimag);
    }

    public String getSum() {
        return this.getString(SUM,"");
    }

    public void setSum(String sum) {
        this.put(SUM,sum);
    }

    public String getAimag_en() {
        return this.getString(AIMAG_EN,"");
    }

    public void setAimag_en(String aimag_en) {
        this.put(AIMAG_EN,aimag_en);
    }

    public String getSum_en() {
        return this.getString(SUM_EN,"");
    }

    public void setSum_en(String sum_en) {
        this.put(SUM_EN,sum_en);
    }

    public String getAimagNer() {
        return this.getString(AIMAGNER,"");
    }

    public void setAimagNer(String aimagNer) {
        this.put(AIMAGNER,aimagNer);
    }

    public String getSumNer() {
        return this.getString(SUMNER,"");
    }

    public void setSumNer(String sumNer) {
        this.put(SUMNER,sumNer);
    }

    public String getMalTurulNer() {
        return this.getString(MALTURULNER,"");
    }

    public void setMalTurulNer(String malTurulNer) {
        this.put(MALTURULNER,malTurulNer);
    }

    public String getDelflg() {
        return this.getString(DELFLG, "");
    }

    public void setDelflg(String delflg) {
        this.put(DELFLG, delflg);
    }

    public String getActflg() {
        return this.getString(ACTFLG, "");
    }

    public void setActflg(String actflg) {
        this.put(ACTFLG, actflg);
    }

    public String getModby() {
        return getString(MODBY, "");
    }

    public void setModby(String modby) {
        this.put(MODBY, modby);
    }

    public Date getModAt() {
        return (Date) getDate(MODAT);
    }

    public void setModAt(Date modAt){
        this.put(MODAT, modAt);
    }

    public String getCreby() {
        return getString(CREBY, "");
    }

    public void setCreby(String creby) {
        this.put(CREBY, creby);
    }

    public Date getCreat() {
        return (Date) getDate(CREAT, null);
    }

    public void setCreat(Date creat) {
        this.put(CREAT, creat);
    }

    public Date getRecordDate() {
        return (Date) this.getDate(RECORD_DATE, null);
    }

    public void setRecordDate(Date recordDate) {
        this.put(RECORD_DATE, recordDate);
    }

    public Date getSearchRecordDate() {
        return (Date) this.getDate(SEARCH_RECORD_DATE, null);
    }

    public void setSearchRecordDate(Date recordDate) {
        this.put(SEARCH_RECORD_DATE, recordDate);
    }

    public Form08Entity(){

    }
}
