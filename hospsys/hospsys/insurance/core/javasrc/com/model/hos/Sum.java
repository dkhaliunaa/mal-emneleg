package com.model.hos;

import com.mongodb.BasicDBObject;

import java.sql.Date;

/**
 * Created by 24be10264 on 9/13/2017.
 */
public class Sum extends BasicDBObject{
    public final String ID = "id";
    public final String SUMCODE = "sumcode";
    public final String CITYCODE = "citycode";
    public final String SUMNAME = "sumname";
    public final String SUMNAME_EN = "sumname_en";
    public final String SUMDESC = "sumdesc";
    public final String SEARCH_CDATE = "searchCDate";
    public final String SEARCH_MDATE = "searchMDate";
    public final String DELFLG = "delflg";
    public final String ACTFLG = "actflg";
    public final String CREBY = "cre_at";
    public final String CREAT = "cre_by";
    public final String MODBY = "mod_at";
    public final String MODAT = "mod_by";
    public final String CITYNAME = "cityname";
    public final String LATITUDE = "latitude";
    public final String LONGITUDE = "longitude";

    public Long getId() {
        return this.getLong(ID, 0);
    }

    public void setId(Long id) {
        put(ID, id);
    }

    public String getSumcode() {
        return getString(SUMCODE, "");
    }

    public void setSumcode(String sumcode) {
        this.put(SUMCODE, sumcode);
    }

    public String getCitycode() {
        return this.getString(CITYCODE, "");
    }

    public void setCitycode(String citycode) {
        this.put(CITYCODE, citycode);
    }

    public String getSumname() {
        return this.getString(SUMNAME, "");
    }

    public void setSumname(String sumname) {
        this.put(SUMNAME, sumname);
    }

    public String getSumname_en() {
        return getString(SUMNAME_EN, "");
    }

    public void setSumname_en(String sumname_en) {
        this.put(SUMNAME_EN, sumname_en);
    }

    public String getSumdesc() {
        return this.getString(SUMDESC, "");
    }

    public void setSumdesc(String sumdesc) {
        this.put(SUMDESC, sumdesc);
    }

    public Date getSearchCDate() {
        return (Date) this.getDate(SEARCH_CDATE);
    }

    public void setSearchCDate(Date searchCDate) {
        this.put(SEARCH_CDATE, searchCDate);
    }

    public Date getSearchMDate() {
        return (Date) this.getDate(SEARCH_CDATE);
    }

    public void setSearchMDate(Date searchMDate) {
        this.put(SEARCH_MDATE, searchMDate);
    }

    public String getDelFlg() {
        return this.getString(DELFLG, "");
    }

    public void setDelFlg(String delFlg) {
        this.put(DELFLG, delFlg);
    }

    public String getActFlg() {
        return this.getString(ACTFLG, "");
    }

    public void setActFlg(String actFlg) {
        this.put(ACTFLG, actFlg);
    }

    public String getCreBy() {
        return this.getString(CREBY, "");
    }

    public void setCreBy(String creBy) {
        this.put(CREBY, creBy);
    }

    public Date getCreAt() {
        return (Date) getDate(CREAT, null);
    }

    public void setCreAt(Date creAt) {
        this.put(CREAT, creAt);
    }

    public String getModBy() {
        return this.getString(MODBY);
    }

    public void setModBy(String modBy) {
        this.put(MODBY, modBy);
    }

    public Date getModAt() {
        return (Date) this.getDate(MODAT);
    }

    public void setModAt(Date modAt) {
        this.put(MODAT, modAt);
    }
    public String getCityName() {
        return this.getString(CITYNAME, "");
    }

    public void setCityName(String cityname) {
        this.put(CITYNAME, cityname);
    }

    public String getLongitude() {
        return getString(LONGITUDE,"");
    }

    public void setLongitude(String modby){
        this.put(LONGITUDE,modby);
    }

    public String getLatitude() {
        return getString(LATITUDE,"");
    }

    public void setLatitude(String modby){
        this.put(LATITUDE,modby);
    }
}
