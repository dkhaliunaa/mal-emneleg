package com.model.hos;

/**
 * Created by 24be10264 on 9/5/2017.
 */

import com.mongodb.BasicDBObject;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public class Form61Entity extends BasicDBObject {
    public static final String ID="id";
    public final String AIMAGNER="aimagNer";
    public static final String MURIINDUGAAR="muriinDugaar";
    public static final String SUMNER="sumNer";
    public static final String ZOORITOO="zooriToo";
    public static final String ZOORITALBAI="zooriTalbai";
    public static final String MALHAASHAATOO="malHaashaaToo";
    public static final String MALHAASHAATALBAI="malHaashaaTalbai";
    public static final String HUDAGTOO="hudagToo";
    public static final String HUDAGHEMJEE="hudagHemjee";
    public static final String TEJAGUUTOO="tejAguuToo";
    public static final String TEJAGUUTALBAI="tejAguuTalbai";
    public static final String TUUHIIEDHADTOO="tuuhiiEdHadToo";
    public static final String TUUHIIEDHADHEMJEE="tuuhiiEdHadHemjee";
    public static final String MALTONOGHERGSEL="malTonogHergsel";
    public static final String BELCHEERTALBAI="belcheerTalbai";
    public static final String NIITTALBAI="niitTalbai";

    public static final String TEEVERTOO="teeverToo";
    public static final String ZORCHIGCHTOO="zorchigchToo";

    public final String AIMAG = "aimag";
    public final String SUM = "sum";

    public static final String SUMNER_EN="sumNer_en";
    public final String AIMAG_EN = "aimag_en";
    public final String SUM_EN = "sum_en";

    public final String DELFLG = "delFlg";
    public final String ACTFLG = "actFlg";
    public final String MODBY = "modBy";
    public final String MODAT = "modAt";
    public final String CREBY = "creBy";
    public final String CREAT = "creAt";
    public final String RECORD_DATE = "recordDate";
    public final String SEARCH_RECORD_DATE = "searchRecordDate";

    public Form61Entity(){

    }

    public Long getId() {
        return this.getLong(ID,0);
    }

    public void setId(Long id) {
        this.put(ID,id);
    }

    public String getAimag() {
        return this.getString(AIMAG);
    }

    public void setAimag(String aimag) {
        this.put(AIMAG, aimag);
    }


    public Long getZorchigchtoo() {
        return this.getLong(ZORCHIGCHTOO, 0);
    }

    public void setZorchigchtoo(Long zorchigchtoo) {
        this.put(ZORCHIGCHTOO, zorchigchtoo);
    }


    public Long getTeevertoo() {
        return this.getLong(TEEVERTOO, 0);
    }

    public void setTeevertoo(Long teevertoo) {
        this.put(TEEVERTOO, teevertoo);
    }


    public String getSum() {
        return this.getString(SUM, "");
    }

    public void setSum(String sum) {
        this.put(SUM, sum);
    }

    public String getAimag_en() {
        return this.getString(AIMAG_EN, "");
    }

    public void setAimag_en(String aimag_en) {
        this.put(AIMAG_EN, aimag_en);
    }

    public String getSum_en() {
        return this.getString(SUM_EN, "");
    }

    public void setSum_en(String sum_en) {
        this.put(SUM_EN, sum_en);
    }

    public Long getMuriinDugaar() {
        return this.getLong(MURIINDUGAAR,0);
    }

    public void setMuriinDugaar(Long muriinDugaar) {
        this.put(MURIINDUGAAR,muriinDugaar);
    }

    public String getAimagNer() {
        return this.getString(AIMAGNER,"");
    }

    public void setAimagNer(String aimagNer) {
        this.put(AIMAGNER,aimagNer);
    }

    public String getSumNer() {
        return this.getString(SUMNER,"");
    }

    public void setSumNer(String sumNer) {
        this.put(SUMNER,sumNer);
    }

    public Long getZooriToo() {
        return this.getLong(ZOORITOO,0);
    }

    public void setZooriToo(Long zooriToo) {
        this.put(ZOORITOO,zooriToo);
    }

    public BigDecimal getZooriTalbai() {
        return BigDecimal.valueOf(this.getDouble(ZOORITALBAI,0));
    }

    public void setZooriTalbai(BigDecimal zooriTalbai) {
        this.put(ZOORITALBAI,zooriTalbai);
    }

    public Long getMalHaashaaToo() {
        return this.getLong(MALHAASHAATOO,0);
    }

    public void setMalHaashaaToo(Long malHaashaaToo) {
        this.put(MALHAASHAATOO,malHaashaaToo);
    }

    public BigDecimal getMalHaashaaTalbai() {
        return BigDecimal.valueOf(this.getDouble(MALHAASHAATALBAI,0));
    }

    public void setMalHaashaaTalbai(BigDecimal malHaashaaTalbai) {
        this.put(MALHAASHAATALBAI,malHaashaaTalbai);
    }

    public Long getHudagToo() {
        return this.getLong(HUDAGTOO,0);
    }

    public void setHudagToo(Long hudagToo) {
        this.put(HUDAGTOO,hudagToo);
    }

    public BigDecimal getHudagHemjee() {
        return BigDecimal.valueOf(this.getDouble(HUDAGHEMJEE,0));
    }

    public void setHudagHemjee(BigDecimal hudagHemjee) {
        this.put(HUDAGHEMJEE,hudagHemjee);
    }

    public Long getTejAguuToo() {
        return this.getLong(TEJAGUUTOO,0);
    }

    public void setTejAguuToo(Long tejAguuToo) {
        this.put(TEJAGUUTOO,tejAguuToo);
    }

    public BigDecimal getTejAguuTalbai() {
        return BigDecimal.valueOf(this.getDouble(TEJAGUUTALBAI,0));
    }

    public void setTejAguuTalbai(BigDecimal tejAguuTalbai) {
        this.put(TEJAGUUTALBAI,tejAguuTalbai);
    }

    public Long getTuuhiiEdHadToo() {
        return this.getLong(TUUHIIEDHADTOO,0);
    }

    public void setTuuhiiEdHadToo(Long tuuhiiEdHadToo) {
        this.put(TUUHIIEDHADTOO,tuuhiiEdHadToo);
    }

    public BigDecimal getTuuhiiEdHadHemjee() {
        return BigDecimal.valueOf(this.getDouble(TUUHIIEDHADHEMJEE,0));
    }

    public void setTuuhiiEdHadHemjee(BigDecimal tuuhiiEdHadHemjee) {
        this.put(TUUHIIEDHADHEMJEE,tuuhiiEdHadHemjee);
    }

    public Long getMalTonogHergsel() {
        return this.getLong(MALTONOGHERGSEL,0);
    }

    public void setMalTonogHergsel(Long malTonogHergsel) {
        this.put(MALTONOGHERGSEL,malTonogHergsel);
    }

    public BigDecimal getBelcheerTalbai() {
        return BigDecimal.valueOf(this.getDouble(BELCHEERTALBAI,0));
    }

    public void setBelcheerTalbai(BigDecimal belcheerTalbai) {
        this.put(BELCHEERTALBAI,belcheerTalbai);
    }

    public BigDecimal getNiitTalbai() {
        return BigDecimal.valueOf(this.getDouble(NIITTALBAI,0));
    }

    public void setNiitTalbai(BigDecimal niitTalbai) {
        this.put(NIITTALBAI,niitTalbai);
    }

    public String getSumNer_en() {
        return this.getString(SUMNER_EN,"");
    }

    public void setSumNer_en(String sumNer_en) {
        this.put(SUMNER_EN,sumNer_en);
    }


    public String getDelflg() {
        return this.getString(DELFLG, "");
    }

    public void setDelflg(String delflg) {
        this.put(DELFLG, delflg);
    }

    public String getActflg() {
        return this.getString(ACTFLG, "");
    }

    public void setActflg(String actflg) {
        this.put(ACTFLG, actflg);
    }

    public String getModby() {
        return getString(MODBY, "");
    }

    public void setModby(String modby) {
        this.put(MODBY, modby);
    }

    public java.sql.Date getModAt() {
        return (java.sql.Date) getDate(MODAT);
    }

    public void setModAt(java.sql.Date modAt) {
        this.put(MODAT, modAt);
    }

    public String getCreby() {
        return getString(CREBY, "");
    }

    public void setCreby(String creby) {
        this.put(CREBY, creby);
    }

    public java.sql.Date getCreat() {
        return (java.sql.Date) getDate(CREAT, null);
    }

    public void setCreat(java.sql.Date creat) {
        this.put(CREAT, creat);
    }

    public Date getRecordDate() {
        return (Date) this.getDate(RECORD_DATE, null);
    }

    public void setRecordDate(Date recordDate) {
        this.put(RECORD_DATE, recordDate);
    }

    public Date getSearchRecordDate() {
        return (Date) this.getDate(SEARCH_RECORD_DATE, null);
    }

    public void setSearchRecordDate(Date recordDate) {
        this.put(SEARCH_RECORD_DATE, recordDate);
    }
}
