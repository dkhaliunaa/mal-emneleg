package com.model.hos;

import com.mongodb.BasicDBObject;

import java.sql.Date;

/**
 * Created by 24be10264 on 9/2/2017.
 *
 * Form 09 Entity class
 */
public class Form09Entity extends BasicDBObject {
    public static final String ID="id";
    public final String AIMAG="aimag";
    public final String AIMAGNER="aimagNer";
    public final String SUM="sum";
    public final String SUMNER="sumNer";
    public final String TARILGANER="tarilgaNer";
    public final String HALDVART_UVCHIN_NER="hald_uvch_ner";
    public static final String NER="ner";
    public static final String ZARTSUULSANHEMJEE="zartsuulsanHemjee";
    public final String MALTURUL="malTurul";
    public final String MALTURULNER="malTurulNer";
    public static final String TOLOVLOGOO="tolovlogoo";
//    idTarilgaNer
    public static final String GUITSETGEL="guitsetgel";
    public static final String HUVI="huvi";

    public final String AIMAG_EN="aimag_en";
    public final String SUM_EN="sum_en";
    public static final String TARILGANER_EN="tarilgaNer_en";
    public static final String NER_EN="ner_en";
    public static final String ZARTSUULSANHEMJEE_EN="zartsuulsanHemjee_en";
    public static final String MALTURUL_EN="malTurul_en";
    public static final String TOLOVLOGOO_EN="tolovlogoo_en";
    public static final String GUITSETGEL_EN="guitsetgel_en";
    public static final String HUVI_EN="huvi_en";

    public static final String DELFLG = "delFlg";
    public static final String ACTFLG = "actFlg";
    public static final String MODBY = "modBy";
    public static final String MODAT = "modAt";
    public static final String CREBY = "creBy";
    public static final String CREAT = "creAt";
    public final String RECORD_DATE = "recordDate";
    public final String SEARCH_RECORD_DATE = "searchRecordDate";

    public final String OLGOSON_TUN="olgosonTun";
    public final String HEREGLESEN_TUN="hereglesenTun";
    public final String USTGASAN_TUN="ustgasanTun";
    public final String NUUTSULSEN_TUN="nuutsulsenTun";

    public Form09Entity(){

    }

    public Long getNUUTSULSEN_TUN() {
        return this.getLong(NUUTSULSEN_TUN,0);
    }

    public void setNUUTSULSEN_TUN(Long nuutsulsenTun) {
        this.put(NUUTSULSEN_TUN,nuutsulsenTun);
    }

    public Long getUSTGASAN_TUN() {
        return this.getLong(USTGASAN_TUN,0);
    }

    public void setUSTGASAN_TUN(Long ustgasanTun) {
        this.put(USTGASAN_TUN,ustgasanTun);
    }

    public Long getHEREGLESEN_TUN() {
        return this.getLong(HEREGLESEN_TUN,0);
    }

    public void setHEREGLESEN_TUN(Long hereglesenTun) {
        this.put(HEREGLESEN_TUN,hereglesenTun);
    }

    public Long getOLGOSON_TUN() {
        return this.getLong(OLGOSON_TUN,0);
    }

    public void setOLGOSON_TUN(Long olgosonTun) {
        this.put(OLGOSON_TUN,olgosonTun);
    }
    public String getMalTurulNer() {
        return this.getString(MALTURULNER,"");
    }

    public void setMalTurulNer(String malTurulNer) {
        this.put(MALTURULNER,malTurulNer);
    }
    public Long getId() {
        return this.getLong(ID,0);
    }

    public void setId(Long id) {
        this.put(ID,id);
    }

    public String getAimag() {
        return this.getString(AIMAG, "");
    }

    public void setAimag(String aimag) {
        this.put(AIMAG,aimag);
    }

    public String getSum() {
        return this.getString(SUM,"");
    }

    public void setSum(String sum) {
        this.put(SUM,sum);
    }

    public String getAimagNer() {
        return this.getString(AIMAGNER,"");
    }

    public void setAimagNer(String aimagNer) {
        this.put(AIMAGNER,aimagNer);
    }

    public String getSumNer() {
        return this.getString(SUMNER,"");
    }

    public void setSumNer(String sumNer) {
        this.put(SUMNER,sumNer);
    }

    public String getAimag_en() {
        return this.getString(AIMAG_EN,"");
    }

    public void setAimag_en(String aimag_en) {
        this.put(AIMAG_EN,aimag_en);
    }

    public String getSum_en() {
        return this.getString(SUM_EN,"");
    }

    public void setSum_en(String sum_en) {
        this.put(SUM_EN,sum_en);
    }

    public String getTarilgaNer() {
        return this.getString(TARILGANER,"");
    }

    public void setTarilgaNer(String tarilgaNer) {
        this.put(TARILGANER,tarilgaNer);
    }

    public String getNer() {
        return this.getString(NER,"");
    }

    public void setNer(String ner) {
        this.put(NER,ner);
    }

    public String getZartsuulsanHemjee() {
        return this.getString(ZARTSUULSANHEMJEE,"");
    }

    public void setZartsuulsanHemjee(String zartsuulsanHemjee) {
        this.put(ZARTSUULSANHEMJEE,zartsuulsanHemjee);
    }

    public String getMalTurul() {
        return this.getString(MALTURUL,"");
    }

    public void setMalTurul(String malTurul) {
        this.put(MALTURUL,malTurul);
    }

    public String getTolovlogoo() {
        return this.getString(TOLOVLOGOO,"");
    }

    public void setTolovlogoo(String tolovlogoo) {
        this.put(TOLOVLOGOO,tolovlogoo);
    }

    public String getGuitsetgel() {
        return this.getString(GUITSETGEL,"");
    }

    public void setGuitsetgel(String guitsetgel) {
        this.put(GUITSETGEL,guitsetgel);
    }

    public String getHuvi() {
        return this.getString(HUVI,"");
    }

    public void setHuvi(String huvi) {
        this.put(HUVI,huvi);
    }

    public String getTarilgaNer_en() {
        return this.getString(TARILGANER_EN,"");
    }

    public void setTarilgaNer_en(String tarilgaNer_en) {
        this.put(TARILGANER_EN,tarilgaNer_en);
    }

    public String getNer_en() {
        return this.getString(NER_EN,"");
    }

    public void setNer_en(String ner_en) {
        this.put(NER_EN,ner_en);
    }

    public String getZartsuulsanHemjee_en() {
        return this.getString(ZARTSUULSANHEMJEE_EN,"");
    }

    public void setZartsuulsanHemjee_en(String zartsuulsanHemjee_en) {
        this.put(ZARTSUULSANHEMJEE_EN,zartsuulsanHemjee_en);
    }

    public String getMalTurul_en() {
        return this.getString(MALTURUL_EN,"");
    }

    public void setMalTurul_en(String malTurul_en) {
        this.put(MALTURUL_EN,malTurul_en);
    }

    public String getTolovlogoo_en() {
        return this.getString(TOLOVLOGOO_EN,"");
    }

    public void setTolovlogoo_en(String tolovlogoo_en) {
        this.put(TOLOVLOGOO_EN,tolovlogoo_en);
    }

    public String getGuitsetgel_en() {
        return this.getString(GUITSETGEL_EN,"");
    }

    public void setGuitsetgel_en(String guitsetgel_en) {
        this.put(GUITSETGEL_EN,guitsetgel_en);
    }

    public String getHuvi_en() {
        return this.getString(HUVI_EN,"");
    }

    public void setHuvi_en(String huvi_en) {
        this.put(HUVI_EN,huvi_en);
    }


    public String getDELFLG() {
        return this.getString(DELFLG, "");
    }

    public void setDelflg(String delflg) {
        this.put(DELFLG, delflg);
    }

    public String getACTFLG() {
        return this.getString(ACTFLG, "");
    }

    public void setActflg(String actflg) {
        this.put(ACTFLG, actflg);
    }

    public String getMODBY() {
        return getString(MODBY, "");
    }

    public void setModby(String modby) {
        this.put(MODBY, modby);
    }

    public Date getMODAT() {
         return (java.sql.Date) getDate(MODAT);
    }

    public void setModat(Date modat){this.put(MODAT,modat);}

    public String getCREBY() {
        return getString(CREBY, "");
    }

    public void setCreby(String creby) {
        this.put(CREBY, creby);
    }

    public Date getCreat() {
        return (Date) getDate(CREAT, null);
    }

    public void setCreat(Date creat) {
        this.put(CREAT, creat);
    }

    public Date getRecordDate() {
        return (Date) this.getDate(RECORD_DATE, null);
    }

    public void setRecordDate(Date recordDate) {
        this.put(RECORD_DATE, recordDate);
    }

    public Date getSearchRecordDate() {
        return (Date) this.getDate(SEARCH_RECORD_DATE, null);
    }

    public void setSearchRecordDate(Date recordDate) {
        this.put(SEARCH_RECORD_DATE, recordDate);
    }

    public String getHaldvartUvchinNer()  {
        return this.getString(HALDVART_UVCHIN_NER,"");
    }

    public void setHaldvartUvchinNer(String uvchinNer) {
        this.put(HALDVART_UVCHIN_NER,uvchinNer);
    }

}
