package com.model.hos;

import java.sql.Date;

/**
 * Created by Tiku on 8/19/2017.
 *
 * Энэхүү класс нь h_xml хүснэгтийн entity класс нь бөгөөд
 * зөвхөн объект дамжуулах үүрэгтэй.
 *
 * Класс дотор нь арга бичихийг хориглоно. Энэхүү класс нь
 * тухайн хүснэгтийн бүх багануудыг агуулж байдаг тул
 * хүснэгт дээр хийгдсэн өөрчлөлтийг болж өгвөл давхар
 * өөрчлөх шаардлагатай байдаг юм.
 *
 * Хэрвээ өөрчлөлт орсон бол дэлгэрэнгүй лог байна.
 */
public class XMLEntity extends EntityClass {
    /**
     * Класс нь дараах гишүүн өгөгдлүүдийг агуулна
     *
     * 1    id
     * 2	tbl_name
     * 3	form_id
     * 4	xml_file_name
     * 5	file_cre_at
     */

    private Long id;
    private String tblName;
    private String formId;
    private String xmlFName;
    private Date fileCreAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTblName() {
        return tblName;
    }

    public void setTblName(String tblName) {
        this.tblName = tblName;
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getXmlFName() {
        return xmlFName;
    }

    public void setXmlFName(String xmlFName) {
        this.xmlFName = xmlFName;
    }

    public Date getFileCreAt() {
        return fileCreAt;
    }

    public void setFileCreAt(Date fileCreAt) {
        this.fileCreAt = fileCreAt;
    }
}
