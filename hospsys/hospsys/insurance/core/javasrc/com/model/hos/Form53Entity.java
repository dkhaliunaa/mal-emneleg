package com.model.hos;

/**
 * Created by 24be10264 on 9/5/2017.
 */

import com.mongodb.BasicDBObject;

import java.sql.Date;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public class Form53Entity extends BasicDBObject {
    public final String ID = "id";
    public final String UVCHINNER = "uvchinNer";
    public final String MALNER = "malNer";
    public final String BUGDUVCHILSEN = "bugdUvchilsen";
    public final String BUGDUHSEN = "bugdUhsen";
    public final String BUGDEDGERSEN = "bugdEdgersen";
    public final String BOTGOUVCHILSEN = "botgoUvchilsen";
    public final String BOTGOUHSEN = "botgoUhsen";
    public final String BOTGOEDGERSEN = "botgoEdgersen";
    public final String UNAGAUVCHILSEN = "unagaUvchilsen";
    public final String UNAGAUHSEN = "unagaUhsen";
    public final String UNAGAEDGERSEN = "unagaEdgersen";
    public final String TUGALUVCHILSEN = "tugalUvchilsen";
    public final String TUGALUHSEN = "tugalUhsen";
    public final String TUGALEDGERSEN = "tugalEdgersen";
    public final String HURGAUVCHILSEN = "hurgaUvchilsen";
    public final String HURGAUHSEN = "hurgaUhsen";
    public final String HURGAEDGERSEN = "hurgaEdgersen";
    public final String ISHIGUVCHILSEN = "ishigUvchilsen";
    public final String ISHIGUHSEN = "ishigUhsen";
    public final String ISHIGEDGERSEN = "ishigEdgersen";
    public final String BUSADUVCHILSEN = "busadUvchilsen";
    public final String BUSADUHSEN = "busadUhsen";
    public final String BUSADEDGERSEN = "busadEdgersen";
    public final String AIMAGNER = "aimagNer";
    public final String SUMNER = "sumNer";

    public final String UVCHINNER_EN = "uvchinNer_En";
    public final String AIMAG = "aimag";
    public final String SUM = "sum";
    public final String AIMAG_EN = "aimag_en";
    public final String SUM_EN = "sum_en";

    public final String DELFLG = "delFlg";
    public final String ACTFLG = "actFlg";
    public final String MODBY = "modBy";
    public final String MODAT = "modAt";
    public final String CREBY = "creBy";
    public final String CREAT = "creAt";
    public final String RECORD_DATE = "recordDate";
    public final String SEARCH_RECORD_DATE = "searchRecordDate";


    public Form53Entity() {

    }

    public Long getId() {
        return this.getLong(ID, 0);
    }

    public void setId(Long id) {
        this.put(ID, id);
    }

    public String getAimagNer() {
        return this.getString(AIMAGNER, "");
    }

    public void setAimagNer(String aimagNer) {
        this.put(AIMAGNER, aimagNer);
    }

    public String getSumNer() {
        return this.getString(SUMNER, "");
    }

    public void setSumNer(String sumNer) {
        this.put(SUMNER, sumNer);
    }

    public String getAimag() {
        return this.getString(AIMAG);
    }

    public void setAimag(String aimag) {
        this.put(AIMAG, aimag);
    }

    public String getSum() {
        return this.getString(SUM, "");
    }

    public void setSum(String sum) {
        this.put(SUM, sum);
    }

    public String getAimag_en() {
        return this.getString(AIMAG_EN, "");
    }

    public void setAimag_en(String aimag_en) {
        this.put(AIMAG_EN, aimag_en);
    }

    public String getMalNer() {
        return this.getString(MALNER, "");
    }

    public void setMalNer(String malNer) {
        this.put(MALNER, malNer);
    }

    public String getSum_en() {
        return this.getString(SUM_EN, "");
    }

    public void setSum_en(String sum_en) {
        this.put(SUM_EN, sum_en);
    }

    public String getUvchinNer() {
        return this.getString(UVCHINNER, "");
    }

    public void setUvchinNer(String uvchinner) {
        this.put(UVCHINNER, uvchinner);
    }

    public String getUvchinNer_En() {
        return this.getString(UVCHINNER_EN, "");
    }

    public void setUvchinNer_En(String uvchinNer_en) {
        this.put(UVCHINNER_EN, uvchinNer_en);
    }

    public Long getBugdUvchilsen() {
        return this.getLong(BUGDUVCHILSEN, 0);
    }

    public void setBugdUvchilsen(Long bugdUvchilsen) {
        this.put(BUGDUVCHILSEN, bugdUvchilsen);
    }

    public Long getBugdUhsen() {
        return this.getLong(BUGDUHSEN, 0);
    }

    public void setBugdUhsen(Long bugdUhsen) {
        this.put(BUGDUHSEN, bugdUhsen);
    }

    public Long getBugdEdgersen() {
        return this.getLong(BUGDEDGERSEN, 0);
    }

    public void setBugdEdgersen(Long bugdEdgersen) {
        this.put(BUGDEDGERSEN, bugdEdgersen);
    }

    public Long getBotgoUvchilsen() {
        return this.getLong(BOTGOUVCHILSEN, 0);
    }

    public void setBotgoUvchilsen(Long botgoUvchilsen) {
        this.put(BOTGOUVCHILSEN, botgoUvchilsen);
    }

    public Long getBotgoUhsen() {
        return this.getLong(BOTGOUHSEN, 0);
    }

    public void setBotgoUhsen(Long botgoUhsen) {
        this.put(BOTGOUHSEN, botgoUhsen);
    }

    public Long getBotgoEdgersen() {
        return this.getLong(BOTGOEDGERSEN, 0);
    }

    public void setBotgoEdgersen(Long botgoEdgersen) {
        this.put(BOTGOEDGERSEN, botgoEdgersen);
    }

    public Long getUnagaUvchilsen() {
        return this.getLong(UNAGAUVCHILSEN, 0);
    }

    public void setUnagaUvchilsen(Long unagaUvchilsen) {
        this.put(UNAGAUVCHILSEN, unagaUvchilsen);
    }

    public Long getUnagaUhsen() {
        return this.getLong(UNAGAUHSEN, 0);
    }

    public void setUnagaUhsen(Long unagaUhsen) {
        this.put(UNAGAUHSEN, unagaUhsen);
    }

    public Long getUnagaEdgersen() {
        return this.getLong(UNAGAEDGERSEN, 0);
    }

    public void setUnagaEdgersen(Long unagaEdgersen) {
        this.put(UNAGAEDGERSEN, unagaEdgersen);
    }

    public Long getTugalUvchilsen() {
        return this.getLong(TUGALUVCHILSEN, 0);
    }

    public void setTugalUvchilsen(Long tugalUvchilsen) {
        this.put(TUGALUVCHILSEN, tugalUvchilsen);
    }

    public Long getTugalUhsen() {
        return this.getLong(TUGALUHSEN, 0);
    }

    public void setTugalUhsen(Long tugalUhsen) {
        this.put(TUGALUHSEN, tugalUhsen);
    }

    public Long getTugalEdgersen() {
        return this.getLong(TUGALEDGERSEN, 0);
    }

    public void setTugalEdgersen(Long tugalEdgersen) {
        this.put(TUGALEDGERSEN, tugalEdgersen);
    }

    public Long getHurgaUvchilsen() {
        return this.getLong(HURGAUVCHILSEN, 0);
    }

    public void setHurgaUvchilsen(Long hurgaUvchilsen) {
        this.put(HURGAUVCHILSEN, hurgaUvchilsen);
    }

    public Long getHurgaUhsen() {
        return this.getLong(HURGAUHSEN, 0);
    }

    public void setHurgaUhsen(Long hurgaUhsen) {
        this.put(HURGAUHSEN, hurgaUhsen);
    }

    public Long getHurgaEdgersen() {
        return this.getLong(HURGAEDGERSEN, 0);
    }

    public void setHurgaEdgersen(Long hurgaEdgersen) {
        this.put(HURGAEDGERSEN, hurgaEdgersen);
    }

    public Long getIshigUvchilsen() {
        return this.getLong(ISHIGUVCHILSEN, 0);
    }

    public void setIshigUvchilsen(Long ishigUvchilsen) {
        this.put(ISHIGUVCHILSEN, ishigUvchilsen);
    }

    public Long getIshigUhsen() {
        return this.getLong(ISHIGUHSEN, 0);
    }

    public void setIshigUhsen(Long ishigUhsen) {
        this.put(ISHIGUHSEN, ishigUhsen);
    }

    public Long getIshigEdgersen() {
        return this.getLong(ISHIGEDGERSEN, 0);
    }

    public void setIshigEdgersen(Long ishigEdgersen) {
        this.put(ISHIGEDGERSEN, ishigEdgersen);
    }

    public Long getBusadUvchilsen() {
        return this.getLong(BUSADUVCHILSEN, 0);
    }

    public void setBusadUvchilsen(Long busadUvchilsen) {
        this.put(BUSADUVCHILSEN, busadUvchilsen);
    }

    public Long getBusadUhsen() {
        return this.getLong(BUSADUHSEN, 0);
    }

    public void setBusadUhsen(Long busaduhsnUhsen) {
        this.put(BUSADUHSEN, busaduhsnUhsen);
    }


    public Long getBusadEdgersen() {
        return this.getLong(BUSADEDGERSEN, 0);
    }

    public void setBusadEdgersen(Long busadEdgersen) {
        this.put(BUSADEDGERSEN, busadEdgersen);
    }
    public String getDelflg() {
        return this.getString(DELFLG, "");
    }

    public void setDelflg(String delflg) {
        this.put(DELFLG, delflg);
    }

    public String getActflg() {
        return this.getString(ACTFLG, "");
    }

    public void setActflg(String actflg) {
        this.put(ACTFLG, actflg);
    }

    public String getModby() {
        return getString(MODBY, "");
    }

    public void setModby(String modby) {
        this.put(MODBY, modby);
    }

    public Date getModAt() {
        return (Date) getDate(MODAT);
    }

    public void setModAt(Date modAt) {
        this.put(MODAT, modAt);
    }

    public String getCreby() {
        return getString(CREBY, "");
    }

    public void setCreby(String creby) {
        this.put(CREBY, creby);
    }

    public Date getCreat() {
        return (Date) getDate(CREAT, null);
    }

    public void setCreat(Date creat) {
        this.put(CREAT, creat);
    }

    public Date getRecordDate() {
        return (Date) this.getDate(RECORD_DATE, null);
    }

    public void setRecordDate(Date recordDate) {
        this.put(RECORD_DATE, recordDate);
    }

    public Date getSearchRecordDate() {
        return (Date) this.getDate(SEARCH_RECORD_DATE, null);
    }

    public void setSearchRecordDate(Date recordDate) {
        this.put(SEARCH_RECORD_DATE, recordDate);
    }
}
