package com.model.hos;

public class MapEntity extends EntityClass {
    private Long id;
    private String aimagner;
    private String aimagzip;
    private String sumner;
    private String sumzip;
    private String urtarag;
    private String orgorog;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAimagner() {
        return aimagner;
    }

    public void setAimagner(String aimagner) {
        this.aimagner = aimagner;
    }

    public String getAimagzip() {
        return aimagzip;
    }

    public void setAimagzip(String aimagzip) {
        this.aimagzip = aimagzip;
    }

    public String getSumner() {
        return sumner;
    }

    public void setSumner(String sumner) {
        this.sumner = sumner;
    }

    public String getSumzip() {
        return sumzip;
    }

    public void setSumzip(String sumzip) {
        this.sumzip = sumzip;
    }

    public String getUrtarag() {
        return urtarag;
    }

    public void setUrtarag(String urtarag) {
        this.urtarag = urtarag;
    }

    public String getOrgorog() {
        return orgorog;
    }

    public void setOrgorog(String orgorog) {
        this.orgorog = orgorog;
    }
}
