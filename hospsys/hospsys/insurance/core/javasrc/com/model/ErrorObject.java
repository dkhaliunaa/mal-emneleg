package com.model;

/**
 * Created by 22cc3355 on 3/23/2017.
 */
public class ErrorObject {
    private String message;
    private String field;

    public void setMessage(String message) { this.message = message; }
    public void setField(String field) { this.field = field; }

}