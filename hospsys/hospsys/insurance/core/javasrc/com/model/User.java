package com.model;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Системийн хэрэглэгч
 * 
 * @author ub
 * 
 */
public class User extends BasicDBObject {
	/**
	* `id` INT NOT NULL AUTO_INCREMENT,
	* `username` VARCHAR(45) NOT NULL,
	* `password` VARCHAR(255) NOT NULL,
	* `userrolecode` VARCHAR(45) NOT NULL,
	* `first_name` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NOT NULL,
	* `last_name` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NOT NULL,
	* `full_name` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NOT NULL,
	* `middle_name` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NULL,
	* `nick_name` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NULL,
	* `gender` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NULL,
	* `birth_day` DATE NULL,
	* `cre_time` DATETIME NOT NULL,
	* `cre_by` VARCHAR(45) NOT NULL,
	* `mod_time` DATETIME NULL,
	* `mod_by` VARCHAR(45) NULL,
	* `del_flg` CHAR(1) NOT NULL,
	* `registration` VARCHAR(45)

	* */
	public User() {
		setAuthType(AuthType.LDAP);
	}

	public User(String username) {
		this();
		setUsername(username);
	}


	public final String ID = "id";
	public final String USERNAME = "username";
	public final String PASSWORD = "password";
	public final String ROLECODE = "roleCode";
	public final String FIRSTNAME = "firstName";
	public final String LASTNAME = "lastName";
	public final String MIDDLENAME = "middleName";
	public final String NICKNAME = "nickName";
	public final String GENDER = "gender";
	public final String BIRTHDATE = "birthDate";
	public final String REGID = "regId";
	public final String CITY = "city";
	public final String SUM = "sum";

	public final String CITY_NAME = "cityname";
	public final String CITY_NAME_EN = "cityname_en";
	public final String SUM_NAME = "sumname";
	public final String SUM_NAME_EN = "sumname_en";

	public final String DELFLG = "delFlg";
	public final String ACTFLG = "actFlg";
	public final String MODBY = "modBy";
	public final String MODAT = "modAt";
	public final String CREBY = "creBy";
	public final String CREAT = "creAt";
	public final String LOGIN_DATE = "logindate";
	public final String CHAT_ID = "chatid";

	public Long getId() {
		return this.getLong(ID, 0);
	}

	public void setId(Long id) {
		this.put(ID, id);
	}

	public String getUsername() {
		return this.getString(USERNAME, "");
	}

	public void setUsername(String username) {
		this.put(USERNAME, username);
	}

	public String getPassword() {
		return this.getString(PASSWORD, "");
	}

	public void setPassword(String password) {
		this.put(PASSWORD, password);
	}

	public String getRoleCode() {
		return this.getString(ROLECODE, "");
	}

	public void setRoleCode(String roleCode) {
		this.put(ROLECODE, roleCode);
	}

	public String getFirstName() {
		return this.getString(FIRSTNAME, "");
	}

	public void setFirstName(String firstName) {
		this.put(FIRSTNAME, firstName);
	}

	public String getLastName() {
		return this.getString(LASTNAME, "");
	}

	public void setLastName(String lastName) {
		this.put(LASTNAME, lastName);
	}

	public String getMiddleName() {
		return getString(MIDDLENAME, "");
	}

	public void setMiddleName(String middleName) {
		this.put(MIDDLENAME,middleName);
	}

	public String getNickName() {
		return this.getString(NICKNAME, "");
	}

	public void setNickName(String nickName) {
		this.put(NICKNAME, nickName);
	}

	public String getGender() {
		return getString(GENDER, "");
	}

	public void setGender(String gender) {
		this.put(GENDER, gender);
	}

	public java.sql.Date getBirthDate() {
		return (java.sql.Date) this.getDate(BIRTHDATE);
	}

	public void setBirthDate(java.sql.Date birthDate) {
		this.put(BIRTHDATE, birthDate);
	}

	public String getRegId() {
		return getString(REGID, "");
	}

	public void setRegId(String regId) {
		this.put(REGID, regId);
	}

	public String getCity() {
		return getString(CITY, "");
	}

	public void setCity(String city) {
		this.put(CITY, city);
	}

	public String getSum() {
		return this.getString(SUM, "");
	}

	public void setSum(String sum) {
		this.put(SUM, sum);
	}

	public String getDelFlg() {
		return getString(DELFLG, "N");
	}

	public void setDelFlg(String delFlg) {
		this.put(DELFLG, delFlg);
	}

	public String getActFlg() {
		return this.getString(ACTFLG, "Y");
	}

	public void setActFlg(String actFlg) {
		this.put(ACTFLG, actFlg);
	}

	public String getModBy() {
		return this.getString(MODBY, "");
	}

	public void setModBy(String modBy) {
		this.put(MODBY, modBy);
	}

	public java.sql.Date getModAt() {
		return (java.sql.Date) this.getDate(MODAT);
	}

	public void setModAt(java.sql.Date modAt) {
		this.put(MODAT, modAt);
	}

	public String getCreBy() {
		return getString(CREBY,"");
	}

	public void setCreBy(String creBy) {
		this.put(CREBY, creBy);
	}

	public java.sql.Date getCreAt() {
		return (java.sql.Date) getDate(CREAT);
	}

	public void setCreAt(java.sql.Date creAt) {
		this.put(CREAT, creAt);
	}


	public String getCityName() {
		return getString(CITY_NAME,"");
	}

	public void setCityName(String creBy) {
		this.put(CITY_NAME, creBy);
	}

	public String getCityNameEn() {
		return getString(CITY_NAME_EN,"");
	}

	public void setCityNameEn(String creBy) {
		this.put(CITY_NAME_EN, creBy);
	}

	public String getSumName() {
		return getString(SUM_NAME,"");
	}

	public void setSumName(String creBy) {
		this.put(SUM_NAME, creBy);
	}

	public String getSumNameEn() {
		return getString(SUM_NAME_EN,"");
	}

	public void setSumNameEn(String creBy) {
		this.put(SUM_NAME_EN, creBy);
	}

	public Date getLoginDate() {
		return getDate(LOGIN_DATE);
	}

	public void setLoginDate(Date creBy) {
		this.put(LOGIN_DATE, creBy);
	}

	public String getChatId() {
		return getString(CHAT_ID,"");
	}

	public void setChatId(String chatId) {
		this.put(CHAT_ID, chatId);
	}

	/**
	 * Нэвтрэх горим
	 */
	public AuthType getAuthType() {
		return AuthType.valueOf(this.getString("authType"));
	}

	public void setAuthType(AuthType authType) {
		this.put("authType", authType.toString());
	}


	/**
	 * Дүрүүд
	 */
	@SuppressWarnings("unchecked")
	public Set<String> getRoles() {
		Object roles = this.get("roles");
		if (roles instanceof BasicDBList) {
			return new HashSet<>((List<String>) roles);
		}
		return (Set<String>) roles;
	}

	public void setRoles(Set<String> roles) {
		this.put("roles", roles);
	}

	public enum AuthType {
		/**
		 * LDAP хэрэглэгч
		 */
		LDAP,
		/**
		 * Application
		 */
		APP,
		/**
		 * Дотоод
		 */
		INTERNAL
	}
}