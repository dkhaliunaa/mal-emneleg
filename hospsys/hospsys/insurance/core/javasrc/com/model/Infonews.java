package com.model;

import com.mongodb.BasicDBObject;

import java.util.Date;

/**
 * Created by 22cc3355 on 6/20/2016.
 */
public class Infonews extends BasicDBObject {

    /**
     *  Түлхүүр дугаар
     */
    public int id;

    /**
     *  Гарчиг
     */
    public String name;

    /**
     *  Файлын нэр
     */
    public String filename;

    /**
     *  Холбоос ашиглах эсэх
     */
    public String useurl;

    /**
     *  Холбоос
     */
    public String url;

    /**
     *  Идвэхтэй эсэх
     */
    public String isActive;

    /**
     *  Устгасан эсэх
     */
    public String delFlg;

    /**
     *  Үүгэсэн огноо
     */
    public Date createdAt;

    /**
     *  Үүсгэсэн хэрэглэгч
     */
    public String createdBy;

    /**
     *  Хамгийн сүүлийн засвар орсон огноо
     */
    public Date modifyBt;

    /**
     *  Хамгийн сүүлийн засвар хийсэн хэрэглэгч
     */
    public String modifyBy;

    public Infonews(){
    }

    /**
     * Түлхүүр дугаар
     */
    public int getId() {
        return this.getInt("id");
    }

    public void setId(int id) { this.put("id", id); }

    /**
     * Гарчиг
     */
    public String getName() {
        return this.getString("name");
    }

    public void setName(String name) {
        this.put("name", name);
    }

    /**
     * Файлын нэр
     */
    public String getFilename() {
        return this.getString("filename");
    }

    public void setFilename(String filename) {
        this.put("filename", filename);
    }

    /**
     * Холбоос ашиглах эсэх
     */
    public boolean getUseurl() {
        return this.getBoolean("useurl");
    }

    public void setUseurl(boolean useurl) { this.put("useurl", useurl); }

    /**
     * Холбоос
     */
    public String getUrl() {
        return this.getString("url");
    }

    public void setUrl(String url) {
        this.put("url", url);
    }

    /**
     * Идвэхтэй эсэх
     */
    public boolean getIsActive() {
        return this.getBoolean("isActive");
    }

    public void setIsActive(boolean isActive) { this.put("isActive", isActive); }

    /**
     * Устгасан эсэх
     */
    public String getDelFlg() {
        return this.getString("delFlg");
    }

    public void setDelFlg(String delFlg) { this.put("delFlg", delFlg); }

    /**
     * Үүгэсэн огноо
     */
    public String getCreatedAt() {
        return this.getString("createdAt");
    }

    public void setCreatedAt(String createdAt) {
        this.put("createdAt", createdAt);
    }

    /**
     * Үүсгэсэн хэрэглэгч
     */
    public String getCreatedBy() {
        return this.getString("createdBy");
    }

    public void setCreatedBy(String createdBy) {
        this.put("createdBy", createdBy);
    }

    /**
     * Хамгийн сүүлийн засвар орсон огноо
     */
    public String getModifyAt() {
        return this.getString("modifyAt");
    }

    public void setModifyAt(String modifyAt) {
        this.put("modifyAt", modifyAt);
    }

    /**
     * Хамгийн сүүлийн засвар хийсэн хэрэглэгч
     */
    public String getModifyBy() {
        return this.getString("modifyBy");
    }

    public void setModifyBy(String modifyBy) {
        this.put("modifyBy", modifyBy);
    }


}
