package com.model;

import java.util.List;

/**
 * Created by 22cc3355 on 4/3/2017.
 */
public class RespList {
    private Metadata metadata;
    private ListInfo listinfo;
    private List<?> lists;

    public void setMetadata(Metadata metadata) { this.metadata = metadata; }
    public void setListinfo(ListInfo listinfo) { this.listinfo = listinfo; }
    public void setReports(List<?> lists) { this.lists = lists; }

}
