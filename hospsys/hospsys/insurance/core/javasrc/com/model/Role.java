package com.model;

import com.mongodb.BasicDBObject;

/**
 * Created by tiku on 7/1/2016.
 */
public class Role extends BasicDBObject {
    public final String ID = "id";
    public final String CODE = "code";
    public final String NAME = "name";
    public final String DESC = "rdesc";
    public final String PARENT = "parent";
    public final String QUALITY = "quality";

    public Long getId() {
        return this.getLong(ID, 0);
    }

    public void setId(Long id) {
        this.put(ID, id);
    }

    public String getCode() {
        return this.getString(CODE, "");
    }

    public void setCode(String code) {
        this.put(CODE, code);
    }

    public String getName() {
        return this.getString(NAME, "");
    }

    public void setName(String name) {
        this.put(NAME, name);
    }

    public String getDesc() {
        return this.getString(DESC);
    }

    public void setDesc(String desc) {
        this.put(DESC, desc);
    }

    public String getParent() {
        return this.getString(PARENT);
    }

    public void setParent(String parent) {
        this.put(PARENT, parent);
    }

    public int getQuality() {
        return this.getInt(QUALITY, 0);
    }

    public void setQuality(int quality) {
        this.put(QUALITY, quality);
    }
}
