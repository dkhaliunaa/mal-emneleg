package com.model;

import com.mongodb.BasicDBObject;

/**
 *
 * Энэ Class нь pagelog table-тэй хамааралтай
 *
 * Created by 22cc3355 on 06/16/2016.
 */
public class Page extends BasicDBObject {

    /**
     *  Хуудасны нэр
     */
    public String page;

    /**
     *  Хуудас дуудсан тоо
     */
    public int hits;

    public Page(){
    }

    /**
     * Хуудасны нэр өгөх
     */
    public String getPage() {
        return this.getString("page");
    }

    /**
     * Хуудасны нэр авах
     */
    public void setPage(String page) {
        this.put("page", page);
    }

    /**
     * Хуудас дуудсан тоо өгөх
     */
    public String getHits() {
        return this.getString("hits");
    }

    /**
     * Хуудас дуудсан тоо авах
     */
    public void setHits(int hits) {
        this.put("hits", hits);
    }
}
