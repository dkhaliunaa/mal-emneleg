package com.model;

import com.mongodb.BasicDBObject;

import java.util.Date;
import java.util.Set;

/**
 * Клиентийн холболтын мэдээлэл
 * 
 * @author ub
 * 
 */
@SuppressWarnings("serial")
public class Session extends BasicDBObject {

	@SuppressWarnings("unchecked")
	public Session(String user, String token, String remoteAddr) {
		this.setToken(token);
		this.setLogin(user);
		this.setRemoteAddr(remoteAddr);
	}


	public void setLogin(String user) {
		this.put("user", user);
	}

	public String getLogin() {
		return this.getString("user");
	}

	public String getToken() {
		return this.getString("_id");
	}

	public void setToken(String token) {
		this.put("_id", token);
	}

	public String getRemoteAddr() {
		return this.getString("remoteAddr");
	}

	public void setRemoteAddr(String remoteAddr) {
		this.put("remoteAddr", remoteAddr);
	}

	public Date getUpdated() {
		return this.getDate("updated");
	}

	public void setUpdated(Date updated) {
		this.put("updated", updated);
	}

	public String getUsername() {
		return this.getString("username");
	}

	public void setUsername(String username) {
		this.put("username", username);
	}

	public Set<String> getRoles() {
		return (Set<String>) this.get("roles");
	}

	public void setRoles(Set<String> p) {
		this.put("roles", p);
	}

	public Set<String> getPerms() {
		return (Set<String>) this.get("perms");
	}

	public void setPerms(Set<String> p) {
		this.put("perms", p);
	}

    public String getEmail() {
        return this.getString("email");
    }

    public void setEmail(String email) {
        this.put("email", email);
    }

    public String getRoleCode(){
		return this.getString("rcode", "");
	}

	public void setRoleCode(String roleCode){
    	this.put("rcode", roleCode);
	}
}
