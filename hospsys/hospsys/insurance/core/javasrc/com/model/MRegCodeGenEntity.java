package com.model;

import com.mongodb.BasicDBObject;

/**
 * Created by 24be10264 on 2/22/2017.
 */
public class MRegCodeGenEntity extends BasicDBObject {

    /**
     * CURRYEAR	    1		N	INTEGER		            None
     * ORGCODE	2		Y	VARCHAR2 (10 Byte)		None
     * PRODCODE	3		Y	VARCHAR2 (10 Byte)		None
     * SEQCODE	4		N	INTEGER		            None
     */


    public static final String CURRYEAR = "year";
    public static final String ORGCODE = "orgCode";
    public static final String PRODCODE = "prodCode";
    public static final String SEQCODE = "seqCode";

    public int getYear() {
        return (this.get(CURRYEAR) != null) ? this.getInt(CURRYEAR) : 0;
    }

    public void setYear(int year) {
        this.put(CURRYEAR, year);
    }

    public String getOrgCode() {
        return (this.get(ORGCODE) != null) ? this.getString(ORGCODE) : "";
    }

    public void setOrgCode(String orgCode) {
        this.put(ORGCODE, orgCode);
    }

    public String getProdCode() {
        return (this.get(PRODCODE) != null) ? this.getString(PRODCODE) : "";
    }

    public void setProdCode(String prodCode) {
        this.put(PRODCODE, prodCode);
    }

    public int getSeqCode() {
        return (this.get(SEQCODE) != null) ? this.getInt(SEQCODE) : 0;
    }

    public void setSeqCode(int seqCode) {
        this.put(SEQCODE, seqCode);
    }
}
