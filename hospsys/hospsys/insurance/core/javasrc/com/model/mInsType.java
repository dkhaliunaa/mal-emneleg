package com.model;

import com.mongodb.BasicDBObject;

/**
 * Бүртгэлийн төрөл
 *
 * Created by 24be10264 on 8/17/2016.
 */
public class mInsType extends BasicDBObject{

    public static final String ID = "id";

    public static final String TNAME = "itname";

    public static final String TDESC = "itdesc";

    public static final String ISDELETE = "isdelete";

    public static final String ISACTIVE = "isactive";


    public int getId() {
        return this.getInt(ID);
    }

    public void setId(int id) {
        this.put(ID, id);
    }

    public String getTname() {
        return this.getString(TNAME);
    }

    public void setTname(String tname) {
        this.put(TNAME, tname);
    }

    public String getTdesc() {
        return this.getString(TDESC);
    }

    public void setTdesc(String tdesc) {
        this.put(TDESC, tdesc);
    }

    public int getIsdelete() {
        return this.getInt(ISDELETE);
    }

    public void setIsdelete(int isdelete) {
        this.put(ISDELETE, isdelete);
    }

    public int getIsactive() {
        return this.getInt(ISACTIVE);
    }

    public void setIsactive(int isactive) {
        this.put(ISACTIVE, isactive);
    }
}
