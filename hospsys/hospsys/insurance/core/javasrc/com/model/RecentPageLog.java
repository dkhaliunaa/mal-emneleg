package com.model;

import com.mongodb.BasicDBObject;

import java.sql.Date;

/**
 * Хамгийн сүүлд ашигласан менюүүдийг логийг хадгалах
 *
 * Created by 24be10264 on 8/24/2016.
 */
public class RecentPageLog extends BasicDBObject {
    /**
     * Дараах шинж чанаруудыг агуулна
     *
     * ID	                -- Хүснэгтийн дугаар (давтагдахгүй)
     * PAGE	                -- Ашигласан меню-н нэр
     * CODE1                -- Хийсэн үйлдлийн дахин давтагдашгүй дугаар. Жишээ нь: Шинэээр гэрээ байгуулсан бол ГЭРЭЭНИЙ ДУГААР хадгална
     * CODE2                -- Нөөц дугаар.
     * CODE3                -- Нөөц дугаар.
     * NAME1                -- Хийсэн үйлдлийн нэр. Жишээ нь: Шинээр бүтээгдэхүүн бүртгэсэн бол БҮТЭЭГДЭХҮҮНИЙ НЭР-ийг хадгална
     * NAME2                -- Нэгээс олон нэр тодорхойлох үед ашиглана
     * NAME3                -- Нэгээс олон нэр тодорхойлох үед ашиглана
     * REF_ID               -- Холбогдох хүснэгтийн ДУГААР. Жишээ нь: Гэрээ бүртгэсэн бол тус гэрээний REGCONTRACT хүснэгтийн дугаар
     * RECORDTYPE           -- Ямар төрлийн үйлдэл хийсэнийг хадгална. Жишээ нь: Зассан, устгах, харах, бүртгэх
     * MODIFY_AT            -- Зассан огноо
     * MODIFY_BY            -- Зассан ажилтан
     * CREATE_AT            -- Бүртгэсэн огноо
     * CREATE_BY            -- Бүртгэсэн ажилтан
     *
     * */


    public static final String ID = "id";
    public static final String PAGE = "page";
    public static final String CODE1 = "cd1";
    public static final String CODE2 = "cd2";
    public static final String CODE3 = "cd3";
    public static final String NAME1 = "nm1";
    public static final String NAME2 = "nm2";
    public static final String NAME3 = "nm3";
    public static final String REFID = "refid";
    public static final String RECORDTYPE = "reccount";
    public static final String MODIFYAT = "mat";
    public static final String MODIFYBY = "mby";
    public static final String CREATEAT = "cat";
    public static final String CREATEBY = "cby";



    public int getId() {
        return this.getInt(ID);
    }

    public void setId(int id) {
        this.put(ID, id);
    }

    public String getPage() {
        return this.getString(PAGE);
    }

    public void setPage(String page) {
        this.put(PAGE, page);
    }

    public String getCode1() {
        return this.getString(CODE1);
    }

    public void setCode1(String code1) {
        this.put(CODE1, code1);
    }

    public String getCode2() {
        return this.getString(CODE2);
    }

    public void setCode2(String code2) {
        this.put(CODE2, code2);
    }

    public String getCode3() {
        return this.getString(CODE3);
    }

    public void setCode3(String code3) {
        this.put(CODE3, code3);
    }

    public String getName1() {
        return this.getString(NAME1);
    }

    public void setName1(String name1) {
        this.put(NAME1, name1);
    }

    public String getName2() {
        return this.getString(NAME2);
    }

    public void setName2(String name2) {
        this.put(NAME2, name2);
    }

    public String getName3() {
        return this.getString(NAME3);
    }

    public void setName3(String name3) {
        this.put(NAME3, name3);
    }

    public int getRefId() {
        return this.getInt(REFID);
    }

    public void setRefId(int refId) {
        this.put(REFID, refId);
    }

    public String getRecordType() {
        return this.getString(RECORDTYPE);
    }

    public void setRecordType(String recordType) {
        this.put(RECORDTYPE, recordType);
    }

    public Date getModifyAt() {
        return (Date) this.getDate(MODIFYAT);
    }

    public void setModifyAt(Date modifyAt) {
        this.put(MODIFYAT, modifyAt);
    }

    public String getModifyBy() {
        return this.getString(MODIFYBY);
    }

    public void setModifyBy(String modifyBy) {
        this.put(MODIFYBY, modifyBy);
    }

    public Date getCreateAt() {
        return (Date) this.getDate(CREATEAT);
    }

    public void setCreateAt(Date createAt) {
        this.put(CREATEAT, createAt);
    }

    public String getCreateBy() {
        return this.getString(CREATEBY);
    }

    public void setCreateBy(String createBy) {
        this.put(CREATEBY, createBy);
    }
}
