package com.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *  Төрөл бүрийн утга хадгалахад зориулсан динамик map төрөл
 *
 * Created by ub on 3/12/15.
 */
public class XMap extends LinkedHashMap<String, Object> {

    public XMap() {

    }

    public XMap(String key, Object val) {
        this.put(key, val);
     }

    public XMap append(String key, Object val) {
        this.put(key, val);
        return this;
    }

    @Override
    public String toString() {
        return toXML();
    }

    // XML текст үүсгэнэ
    public String toXML() {
        final SimpleDateFormat date14 = new SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss.SSS");

        StringBuilder sb = new StringBuilder();
        for (Map.Entry e : this.entrySet()) {
            sb.append("<").append(e.getKey()).append(">");

            Object val = e.getValue();

            if (val != null) {
                if (val instanceof XMap) {
                    sb.append(((XMap) val).toXML());
                } else if (val instanceof Date) {
                    sb.append(date14.format(val));
                } else if (val instanceof List) {
                    List lst = (List)val;
                    for (Object o : lst) {
                        String clz = o.getClass().getSimpleName();
                        sb.append("<").append(clz).append(">");
                        sb.append(o.toString());
                        sb.append("</").append(clz).append(">");
                    }
                } else {
                    sb.append(val);
                }
            }

            sb.append("</").append(e.getKey()).append(">");
        }

        return sb.toString();
    }

    public Object query(String path) {
        if (this.containsKey(path)) {
            return this.get(path);
        }

        for (Map.Entry e : this.entrySet()) {
            Object val = e.getValue();

            if (val != null && val instanceof XMap) {
                return ((XMap) val).query(path);
            }
        }

        return null;
    }


    
    public XMap getXMap(String key) {
        return (XMap)get(key);
    }

    public Date getDate(String key) {
        return (Date)get(key);
    }

    public Object get(String key) {
        return super.get(key);
    }

    public int getInt(String key) {
        Object o = this.get(key);
        if (o == null) {
            throw new NullPointerException("no value for: " + key);
        } else {
            return ((Number)o).intValue();
        }
    }

    public int getInt(String key, int def) {
        Object foo = this.get(key);
        return foo == null?def: ((Number)foo).intValue();
    }

    public long getLong(String key) {
        Object foo = this.get(key);
        return ((Number)foo).longValue();
    }

    public long getLong(String key, long def) {
        Object foo = this.get(key);
        return foo == null?def:((Number)foo).longValue();
    }

    public double getDouble(String key) {
        Object foo = this.get(key);
        return ((Number)foo).doubleValue();
    }

    public double getDouble(String key, double def) {
        Object foo = this.get(key);
        return foo == null?def:((Number)foo).doubleValue();
    }

    public String getString(String key) {
        Object foo = this.get(key);
        return foo == null?null:foo.toString();
    }

    public String getString(String key, String def) {
        Object foo = this.get(key);
        return foo == null?def:foo.toString();
    }

    public boolean getBoolean(String key) {
        return this.getBoolean(key, false);
    }

    public boolean getBoolean(String key, boolean def) {
        Object foo = this.get(key);
        if(foo == null) {
            return def;
        } else if(foo instanceof Number) {
            return ((Number)foo).intValue() > 0;
        } else if(foo instanceof Boolean) {
            return ((Boolean)foo).booleanValue();
        } else {
            throw new IllegalArgumentException("can\'t coerce to bool:" + foo.getClass());
        }
    }
}
