package com.utils;

import com.mongodb.BasicDBObject;

/**
 * Id, Name хослол утгуудыг төлөөлөх класс
 * 
 * @author ub
 * 
 */
public class IdName extends BasicDBObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public IdName() {

	}
	
	public IdName(String id, String name) {
		setId(id);
		setName(name);
	}

	public IdName(String id, String name, int read, String dept) {
		setId(id);
		setName(name);
		setRead(read);
		setDept(dept);
	}

	public String getId() {
		return this.getString("id");
	}

	public void setId(String id) {
		this.put("id", id);
	}

	public String getName() {
		return this.getString("name");
	}

	public void setName(String name) {
		this.put("name", name);
	}

	public int getRead() {
		return this.getInt("read");
	}

	public void setRead(int read) {
		this.put("read", read);
	}
	
	public String getDept() {
		return this.getString("dept");
	}

	public void setDept(String dept) {
		this.put("dept", dept);
	}

	@Override
	public String toString() {
		return getName();
	}

}
