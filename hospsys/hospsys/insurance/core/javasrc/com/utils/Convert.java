package com.utils;

import java.text.NumberFormat;

public class Convert {

    static final NumberFormat f = NumberFormat.getInstance();

    static {
        f.setGroupingUsed(false);
        f.setMaximumFractionDigits(2);
        f.setMinimumFractionDigits(2);
    }

    // converts integer n into a base b string

    public static String toString(int n, int base) {
        // special case
        if (n == 0) {
            return "0";
        }

        String digits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String s = "";
        while (n > 0) {
            int d = n % base;
            s = digits.charAt(d) + s;
            n = n / base;
        }
        return s;
    }

    public static String toBinaryString(int n) {
        return toString(n, 2);
    }

    public static String toHexString(int n) {
        return toString(n, 16);
    }

    public static void inputError(String s) {
        throw new RuntimeException("Input error with" + s);
    }

    public static boolean tryParseDouble(String value, Double out) {
        try {
            out = Double.parseDouble(value);
            return true;
        } catch (Exception ex) {
            return false;
        }

    }

    // convert a String representing a base b integer into an int
    public static int fromString(String s, int b) {
        int result = 0;
        int digit = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c >= '0' && c <= '9') {
                digit = c - '0';
            } else if (c >= 'A' && c <= 'Z') {
                digit = 10 + c - 'A';
            } else {
                inputError(s);
            }

            if (digit < b) {
                result = b * result + digit;
            } else {
                inputError(s);
            }
        }
        return result;
    }

    public static int fromBinaryString(String s) {
        return fromString(s, 2);
    }

    public static int fromHexString(String s) {
        return fromString(s, 16);
    }

    public static synchronized String toString(double value) {
        return f.format(value);
    }

    // fixml test client
    public static void main(String[] args) {
        double d = 7888888812345.567;
        System.out.println("Number:" + toString(d));

    }
}