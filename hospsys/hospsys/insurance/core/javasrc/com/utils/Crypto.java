package com.utils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;

/**
 * Created by ub on 1/30/15.
 */
public class Crypto {

    public static String hashPassword(String pwd) throws Exception {
        MessageDigest digest = MessageDigest.getInstance("SHA-1");
        digest.reset();
        digest.update(pwd.getBytes("utf8"));

        byte[] digestBytes = digest.digest();
        return javax.xml.bind.DatatypeConverter.printHexBinary(digestBytes);
    }

    // энэ түлхүүрийг нууцлах хэрэгтэй
    private static final char[] PASSWORD = "GolomtBankPassKey1#`dasd!@"
            .toCharArray();
    private static final byte[] SALT = {(byte) 0xde, (byte) 0x33, (byte) 0x10,
            (byte) 0x12, (byte) 0xde, (byte) 0x33, (byte) 0x10, (byte) 0x12,};

    private static String convertToHex(byte[] data) {
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9)) {
                    buf.append((char) ('0' + halfbyte));
                } else {
                    buf.append((char) ('a' + (halfbyte - 10)));
                }
                halfbyte = data[i] & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static String SHA1(String text) {
        byte[] sha1hash = new byte[40];
        try {
            MessageDigest md;
            md = MessageDigest.getInstance("SHA-1");
            md.update(text.getBytes("UTF-8"), 0, text.length());
            sha1hash = md.digest();
        } catch (Exception ex) {
            System.out.println("Crypto.SHA1: " + ex.getMessage());
        }
        return convertToHex(sha1hash);
    }

    public static String encrypt(String data) throws GeneralSecurityException,
            UnsupportedEncodingException {
        SecretKeyFactory keyFactory = SecretKeyFactory
                .getInstance("PBEWithMD5AndDES");
        SecretKey key = keyFactory.generateSecret(new PBEKeySpec(PASSWORD));
        Cipher pbeCipher = Cipher.getInstance("PBEWithMD5AndDES");
        pbeCipher
                .init(Cipher.ENCRYPT_MODE, key, new PBEParameterSpec(SALT, 20));

        return Base64.encode(pbeCipher.doFinal(data.getBytes()));
    }

    public static String decrypt(String data) throws GeneralSecurityException,
            IOException {
        SecretKeyFactory keyFactory = SecretKeyFactory
                .getInstance("PBEWithMD5AndDES");
        SecretKey key = keyFactory.generateSecret(new PBEKeySpec(PASSWORD));
        Cipher pbeCipher = Cipher.getInstance("PBEWithMD5AndDES");
        pbeCipher
                .init(Cipher.DECRYPT_MODE, key, new PBEParameterSpec(SALT, 20));
        return new String(pbeCipher.doFinal(Base64.decode(data)), "UTF-8");
    }

    public static String toHex(byte[] bytes) {
        BigInteger bi = new BigInteger(1, bytes);
        return String.format("%0" + (bytes.length << 1) + "X", bi);
    }

    public static String md5(String text) throws Exception {
        final MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        messageDigest.reset();
        messageDigest.update(text.getBytes("UTF-8"));
        final byte[] resultByte = messageDigest.digest();
        final String result = toHex(resultByte);
        return result;
    }

    public static void main(String[] args) throws Exception {
        System.out.println(encrypt("7797"));

        if (args.length > 1) {
            if ("e".equals(args[0])) {
                String encryptedPassword = encrypt(args[1]);
                System.out.println("Encrypted password: " + encryptedPassword);
            } else {
                String decryptedPassword = decrypt(args[1]);
                System.out.println("Decrypted password: " + decryptedPassword);
            }
        } else {
            System.out.println("Usage:  java Crypto e|d password");
        }
    }
}
