package com.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * 
 * @author 232c1998
 */
public class CreateZipFile {

    public static void createZipFile(List<File> files, String zipName)
            throws Exception {
        byte[] buf = new byte[1024];

        try (ZipOutputStream out = new ZipOutputStream(new FileOutputStream(
                zipName))) {
            File f = new File(zipName);

            if (f.exists()) {
                f.delete();
            }

            for (File f1 : files) {
                if (f1.isFile()) {
                    try (FileInputStream in = new FileInputStream(f1)) {
                        out.putNextEntry(new ZipEntry(f1.getName()));
                        int len = 0;
                        while ((len = in.read(buf)) > 0) {
                            out.write(buf, 0, len);
                        }
                    }
                }
            }
        }
    }
}
