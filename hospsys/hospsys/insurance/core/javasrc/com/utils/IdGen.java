package com.utils;

import org.bson.types.ObjectId;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class IdGen {

    static final SimpleDateFormat FULL_TIME_FORMAT = new SimpleDateFormat(
            "yyMMddHHmmssSSSS");
    static final Random rand = new Random();

    public static String getFormatString(String pattern) {
        final SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        return formatter.format(new java.util.Date());
    }

    public synchronized static String genTrxId(String prefix) {
        return prefix + FULL_TIME_FORMAT.format(new Date());
    }

    public synchronized static String makeUnikey() {
        return FULL_TIME_FORMAT.format(new Date());
    }

    public synchronized static String makeRefNo() {
        final String str = String.valueOf(System.nanoTime());
        if (str.length() > 12) {
            return str.substring(str.length() - 12, str.length());
        }
        return str;
    }

    public synchronized static String makeDbId() {
        return new ObjectId().toString();
    }

    public synchronized static String makeSessionId() {
        return new ObjectId().toString() + String.valueOf(rand.nextInt());
    }
}