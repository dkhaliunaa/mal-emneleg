package com.utils;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Тэмдэгтийн энкодлол хувиргана
 * 
 * @author ub
 */
public class CharsetConverter {

    public static String utf8_cp1251(String utf8_str)
            throws UnsupportedEncodingException {
        StringBuilder sb = new StringBuilder(utf8_str.length());
        int c;
        for (int i = 0; i < utf8_str.length(); i++) {
            c = utf8_str.charAt(i);
            if (1040 < c && c <= 1103) {
                sb.append((char) (c - 848));
            } else if (c == 1025) {
                sb.append((char) 168);
            } else if (c == 1256) {
                sb.append((char) 170);
            } else if (c == 1198) {
                sb.append((char) 175);
            } else if (c == 1105) {
                sb.append((char) 184);
            } else if (c == 1257) {
                sb.append((char) 186);
            } else if (c == 1199) {
                sb.append((char) 191);
            } else {
                sb.append((char) c);
            }
        }

        return sb.toString();
    }

    public static String cp1251_utf8(String cp1251_str)
            throws UnsupportedEncodingException {
        StringBuilder sb = new StringBuilder(cp1251_str.length());
        int c;
        for (int i = 0; i < cp1251_str.length(); i++) {
            c = cp1251_str.charAt(i);
            if (191 < c && c <= 255) {
                sb.append((char) (c + 848));
            } else if (c == 168) {
                sb.append((char) 1025);
            } else if (c == 170) {
                sb.append((char) 1256);
            } else if (c == 175) {
                sb.append((char) 1198);
            } else if (c == 184) {
                sb.append((char) 1105);
            } else if (c == 186) {
                sb.append((char) 1257);
            } else if (c == 191) {
                sb.append((char) 1199);
            } else {
                sb.append((char) c);
            }
        }

        return sb.toString();
    }

    static final Map<Character, String> utf8_latin_map = new HashMap<Character, String>() {

        /**
         *
         */
        private static final long serialVersionUID = -7507998017317650511L;

        {
            put('А', "A");
            put('Б', "B");
            put('В', "V");
            put('Г', "G");
            put('Д', "D");
            put('Е', "YE");
            put('Ё', "YO");
            put('Ж', "J");
            put('З', "Z");
            put('И', "I");
            put('Й', "I");
            put('К', "K");
            put('Л', "L");
            put('М', "M");
            put('Н', "N");
            put('О', "O");
            put('Ө', "U");
            put('П', "P");
            put('Р', "R");
            put('С', "S");
            put('Т', "T");
            put('У', "U");
            put('Ү', "U");
            put('Ф', "F");
            put('Х', "KH");
            put('Ц', "TS");
            put('Ч', "CH");
            put('Ш', "SH");
            put('Щ', "SHCH");
            put('Ъ', "I");
            put('Ы', "II");
            put('Ь', "I");
            put('Э', "E");
            put('Ю', "YU");
            put('Я', "YA");
        }
    };

    public static String utf8_latin(String str) {
        StringBuilder sb = new StringBuilder();
        str = str.toUpperCase();

        char c;
        for (int i = 0; i < str.length(); i++) {
            c = str.charAt(i);
            if ('А' <= c && c <= 'Я') {
                sb.append(utf8_latin_map.get(c));
            } else if (c == 'Ө' || c == 'Ү' || c == 'Ё') {
                sb.append(utf8_latin_map.get(c));
            } else {
                sb.append(c);
            }
        }

        return sb.toString();

    }

    public static void main(String[] args) {
        System.out.println(utf8_latin("Хүлээ"));
    }
}