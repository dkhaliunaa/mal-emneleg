package com.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Цифрийг бичиг рүү хөрвүүлэх
 * 
 * @author ub
 */
public class NumberSpeller {

    public static String read(long n) {
        return read(n, false);
    }

    public static String read(long n, boolean withSuffix) {
        if (n == 0) {
            return "тэг";
        }

        if (n == 1) {
            return "нэг";
        }

        List<String> words = new ArrayList<>();

        int p = 0;
        int d1, d2, d3;
        int a = 0;

        while (n > 0) {
            // a = (d3 d2 d1)
            a = (int) (n % 1000);

            if (a > 0) {
                if (sep_num.containsKey(p)) {
                    words.add(sep_num.get(p));
                } else if (p > 0) {
                    words.add("{нэргүй}");
                }
            }

            d1 = a % 10;
            d2 = (a / 10) % 10;
            d3 = a / 100;

            if (d1 > 0) {
                words.add(num.get(d1));
            }
            words.add(num.get(d2 * 10));
            words.add(num.get(d3 * 100));

            n = n / 1000;
            p += 3;
        }

        // холбогч нэрсээр солих
        return connectWords(words, withSuffix);
    }

    private static String connectWords(List<String> words, boolean withSuffix) {
        StringBuilder result = new StringBuilder();

        for (int i = words.size() - 1; i > 0; i--) {
            if (words.get(i).length() > 0) {
                result.append(makeBander(words.get(i)));
                result.append(' ');
            }
        }

        if (withSuffix) {
            if (words.get(0).equals("мянга")) {
                result.append(words.get(0));
                result.append('н');
            } else {
                result.append(makeBander(words.get(0)));
            }
        } else {
            result.append(words.get(0));
        }

        String s = result.toString().trim();

        if (s.startsWith("нэгэн") && s.length() > "нэгэн".length()) {
            s = "нэг" + s.substring("нэгэн".length());
        }

        return s.trim();
    }

    private static String makeBander(String w) {
        if (bander.containsKey(w)) {
            return bander.get(w);
        } else if (w.endsWith("зуу")) {
            return w + "н";
        }
        return w;
    }

    /**
     * Constants
     */
    static final Map<Integer, String> num = new HashMap<Integer, String>() {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        {
            put(0, "");
            put(1, "нэг");
            put(2, "хоёр");
            put(3, "гурав");
            put(4, "дөрөв");
            put(5, "тав");
            put(6, "зургаа");
            put(7, "долоо");
            put(8, "найм");
            put(9, "ес");
            put(10, "арав");
            put(20, "хорь");
            put(30, "гуч");
            put(40, "дөч");
            put(50, "тавь");
            put(60, "жар");
            put(70, "дал");
            put(80, "ная");
            put(90, "ер");
            put(100, "нэг зуу");
            put(200, "хоёр зуу");
            put(300, "гурван зуу");
            put(400, "дөрвөн зуу");
            put(500, "таван зуу");
            put(600, "зургаан зуу");
            put(700, "долоон зуу");
            put(800, "найман зуу");
            put(900, "есөн зуу");
        }
    };
    static final Map<Integer, String> sep_num = new HashMap<Integer, String>() {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        {
            put(3, "мянга");
            put(6, "сая");
            put(9, "тэрбум");
            put(12, "наяд");
        }
    };
    static final Map<String, String> bander = new HashMap<String, String>() {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        {
            put("нэг", "нэгэн");
            put("гурав", "гурван");
            put("дөрөв", "дөрвөн");
            put("тав", "таван");
            put("зургаа", "зургаан");
            put("долоо", "долоон");
            put("найм", "найман");
            put("ес", "есөн");
            put("арав", "арван");
            put("хорь", "хорин");
            put("гуч", "гучин");
            put("дөч", "дөчин");
            put("тавь", "тавин");
            put("жар", "жаран");
            put("дал", "далан");
            put("ная", "наян");
            put("ер", "ерэн");
            put("зуу", "зуун");
        }
    };
}
