package com.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 
 * @author ubs121
 */
public class DateUtil {

    public static final SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd");
    public static final SimpleDateFormat yyyyMMdd_HHMMSS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final SimpleDateFormat yyyy_MM_dd = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat dd_MM_yyyy = new SimpleDateFormat("dd-MM-yyyy");
    public static final SimpleDateFormat yyyy_MM = new SimpleDateFormat("yyyy-MM");
    public static final SimpleDateFormat yyyyMM = new SimpleDateFormat("yyyyMM");
    public static final SimpleDateFormat MM = new SimpleDateFormat("MM");
    public static final SimpleDateFormat MMddHHmmss = new SimpleDateFormat("MMddHHmmss");
    public static final SimpleDateFormat date14 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    public static final SimpleDateFormat date15 = new SimpleDateFormat("yyyyMMdd hhmmss");
    public static Calendar cal = Calendar.getInstance();
    public static final SimpleDateFormat date16 = new SimpleDateFormat("yyyy.MM.dd");
    public static final SimpleDateFormat date17 = new SimpleDateFormat("yyyy.MM.dd hh:mm:ss");
    public static final SimpleDateFormat salDate = new SimpleDateFormat("yyyyMMdd_hh_mm");
    public static final SimpleDateFormat HHMMSS = new SimpleDateFormat("HH:mm:ss");

    public static final SimpleDateFormat DATETIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    public static Date getPrevMonth(Date current, int subMonth)
			throws Exception {
		cal.setTime(current);
		cal.add(Calendar.MONTH, -subMonth);

		return cal.getTime();
	}

	public static Date getPrevDate(Date dt) throws Exception {
		cal.setTime(dt);
		cal.add(Calendar.DATE, -1);
		return cal.getTime();
	}

	public static Date getPrevDate(Date dt, int subDate) throws Exception {
		cal.setTime(dt);
		cal.add(Calendar.DATE, -1 * subDate);
		return cal.getTime();
	}

	public static Date getNextDate(Date dt) throws Exception {
		cal.setTime(dt);
		cal.add(Calendar.DATE, 1);
		return cal.getTime();
	}

	public static Double getDayIndex(Date d) {
		cal.setTime(d);

		return cal.get(Calendar.YEAR) * 365 + cal.get(Calendar.MONTH) * 30.42
				+ cal.get(Calendar.DATE);
	}

	
    public static Date addDays(Date date, int days){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }

    public static String hhss(long duration){

        int mm = 0;
        int ss = 0;

        if (duration > 60) {
            mm = (int) Math.floor(duration / 60);
            ss = (int) Math.floor(duration % 60);
        } else {
            ss = (int) duration;
        }

        return (mm > 10 ? "" + mm: "0" + mm) + ":" + (ss > 10 ? "" + ss: "0" + ss);

    }


}
