package com.utils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.util.JSON;

/**
 * 
 * @author 232c1998
 */
public class JSONUtil {

	public static String getSendStr(String str) {
		String fixStr = "";
		String str1 = "";

		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) == "$".charAt(0)) {
				fixStr = fixStr.substring(0, fixStr.length() - 3);
				i += 8;
				while (true) {
					if (str.charAt(i) == "}".charAt(0)) {
						break;
					} else {
						fixStr += str.charAt(i);
						i++;
					}
				}
			} else
				fixStr += str.charAt(i);

			if (fixStr.length() >= 12) {
				str1 = fixStr.substring(fixStr.length() - 9, fixStr.length());

				if (str1.equals("className")) {
					fixStr = fixStr.substring(0, fixStr.length() - 12);

					while (true) {
						if (str.charAt(i) == ",".charAt(0)) {
							fixStr += str.charAt(i);
							break;
						} else {
							i++;
						}
					}
				}
			}
		}
		fixStr = fixStr.replaceAll(": ", ":").replaceAll(" :", ":")
				.replaceAll(" ,", ",").replaceAll(", ", ",");
		return fixStr.replaceAll(" '", "'").replaceAll("' ", "'");
	}

	public static String replaceIdesc(String desc) {
		return desc.replaceAll("\\$", "").replaceAll("\\#", "")
				.replaceAll("%", "").replaceAll("\\^", "")
				.replaceAll("\\*", "").replaceAll("\\!", "")
				.replaceAll("\\?", "").replaceAll("\\]", "")
				.replaceAll("\\[", "").replaceAll("'", "")
				.replaceAll("\\&", " ").replaceAll("\r\n", " ")
				.replaceAll("\n", " ").replaceAll("\r", " ")
				.replaceAll("\t", " ").replaceAll("\\/", " ")
				.replaceAll(";", "-").replaceAll("\\\\", "");
	}

	public static String getSendStr(DBCursor cur, String callBack) {
		BasicDBObject obj = null;
		Map<String, String> sendMap = new HashMap<>();
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		while (cur.hasNext()) {
			sendMap.clear();
			obj = (BasicDBObject) cur.next();
			for (String str : obj.keySet()) {
				if (str.equals("RDATE") || str.equals("FDATE")
						|| str.equals("CDATE") || str.equals("DDATE")
						|| str.equals("reg_date")) {
					sendMap.put(str,
							DateUtil.date14.format((Date) obj.get(str)));
				} else
					sendMap.put(
							str,
							obj.getString(str) != null ? replaceIdesc(obj
									.getString(str)) : "");
			}
			sb.append(
					JSON.serialize(sendMap).replaceAll("\"", "'")
							.replaceAll("\r\n", "").replaceAll("\r", "")
							.replaceAll("\n", "")).append(",");
		}

		String temp = sb.toString();
		if (!temp.equals("[")) {
			sb = new StringBuilder();
			sb.append(temp.substring(0, temp.length() - 1));
		}
		sb.append("]");
		return (callBack + "(" + sb.toString() + ");");
	}

	public static String getSendStr(int type, String text, String callback) {
		Map<String, String> msgMap = new HashMap<>();
		msgMap.put("type", "" + type);
		msgMap.put("message", text);
		return callback + "([" + JSON.serialize(msgMap).replaceAll("\"", "'")
				+ "])";
	}

	public static String getSendStr(List<BasicDBObject> dbList, String callBack) {
		Map<String, String> sendMap = new HashMap<>();
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (BasicDBObject obj : dbList) {
			sendMap.clear();
			for (String str : obj.keySet()) {
				if (str.equals("RDATE") || str.equals("FDATE")
						|| str.equals("CDATE") || str.equals("DDATE")) {
					try {
						sendMap.put(str,
								DateUtil.date14.format((Date) obj.get(str)));
					} catch (Exception e) {
						sendMap.put(str, str);
					}
				} else
					sendMap.put(
							str,
							obj.getString(str) != null ? replaceIdesc(obj
									.getString(str)) : "");
			}
			sb.append(
					JSON.serialize(sendMap).replaceAll("\"", "'")
							.replaceAll("\r\n", "").replaceAll("\r", "")
							.replaceAll("\n", "")).append(",");
		}
		String temp = sb.toString();
		if (!temp.equals("[")) {
			sb = new StringBuilder();
			sb.append(temp.substring(0, temp.length() - 1));
		}
		sb.append("]");
		return (callBack + "(" + sb.toString() + ");");
	}

	public static void main(String[] args) {
		System.out.println("sdfds$#a".replaceAll("\\$", "")
				.replaceAll("\\#", "").replaceAll("%", "")
				.replaceAll("\\^", "").replaceAll("\\*", "")
				.replaceAll("\\!", "").replaceAll("\\?", "")
				.replaceAll("\\]", "").replaceAll("\\[", "")
				.replaceAll("'", "").replaceAll("\\&", " ")
				.replaceAll("\r\n", " ").replaceAll("\n", " ")
				.replaceAll("\r", " ").replaceAll("\t", " ")
				.replaceAll("\\/", " ").replaceAll(";", "-")
				.replaceAll("\\\\", ""));
	}
}
