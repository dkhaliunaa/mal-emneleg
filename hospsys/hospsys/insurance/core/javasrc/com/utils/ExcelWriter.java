package com.utils;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.net.URLDecoder;
import java.util.List;

/**
 * Json өгөгдлийг Xlsx файл руу хөрвүүлэх
 *
 * @author Turbold 20db3806
 *
 */
public class ExcelWriter {


    // xlsx
    public void writeXLSXFile(List<DBObject> flatJson, List<String> headers, BasicDBObject labels, String fileName, String sheetName) throws IOException {

        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet sheet = wb.createSheet(sheetName) ;

        // Толгойн хэсгийг хөрвүүлэх
        headerRow(sheet, headers, labels);

        // Их биеийн хэсгийг хөрвүүлэх
        bodyRow(sheet, headers, flatJson);

        //Файл үүсгэж бичих
        writeToFile(wb, fileName);
    }

    // Толгойн хэсгийг эхний мөрний нүднүүдэд оруулж байна
    private void headerRow(XSSFSheet sheet, List<String> headers, BasicDBObject labels) {
        XSSFRow header = sheet.createRow(0);
        for(int i = 0; i < headers.size(); i++){
            String head = headers.get(i);
            XSSFCell cell = header.createCell(i);
            if(labels.getString(head) != null){
                cell.setCellValue(labels.getString(head).toUpperCase().replace(",", ""));
            } else {
                cell.setCellValue(head.toUpperCase());
            }
        }
    }

    // Их биеийн мэдээллийг мөрнүүд болон мөрнүүдийн нүднүүдийн оруулж байна
    private void bodyRow(XSSFSheet sheet, List<String> headers, List<DBObject> jsonData) {

        // Json жагсаалтыг давтана
        for(int i = 0; i < jsonData.size(); i++){

            DBObject rowData = (DBObject) jsonData.get(i);
            XSSFRow row = sheet.createRow(i+1);

            // Толгойн түлхүүр үгнүүдийг давтана
            for(int j = 0; j < headers.size(); j++){

                String value = "";
                String header = headers.get(j);
                XSSFCell cell = row.createCell(j);

                if(header.contains(".")){
                    header = header.replace(".", ";");
                    header = header + ";";
                    String[] h = header.split(";");

                    if(rowData.get(h[0]) instanceof List){
                        for(DBObject r : (List<DBObject>) rowData.get(h[0])){
                            value = value + r.get(h[1]) + " | ";
                        }
                    } else {
                        DBObject r = (DBObject) rowData.get(h[0]);
                        value = r != null ? r.get(h[1]) == null ? "" : r.get(h[1]).toString().replace(",", "") : "";
                    }
                } else {
                    value = rowData.get(header) == null ? "" : rowData.get(header).toString().replace(",", "");

                }

                cell.setCellValue(value);
            }
        }
    }


    private void writeToFile(XSSFWorkbook wb, String fileName) throws FileNotFoundException {
        try {
            File fileRenamed = new File(URLDecoder.decode(
                    fileName, "UTF-8"));

            FileOutputStream fos = new FileOutputStream(fileRenamed);

            wb.write(fos);
            fos.flush();
            fos.close();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {

        }
    }


}

