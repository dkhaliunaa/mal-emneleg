package com.utils;

import java.io.*;

public class IOUtils {

    public static String readFully(InputStream stream) throws IOException {
        StringBuilder sb = new StringBuilder();

        try (BufferedReader rdr = new BufferedReader(new InputStreamReader(
                stream))) {
            String line = null;
            while ((line = rdr.readLine()) != null) {
                sb.append(line);
            }
        }
        return sb.toString();
    }

    public static void readFully(InputStream in, byte[] buf, int length)
            throws IOException {
        int writeIndex = 0;
        int remaining = length;

        int nbread;

        while (remaining > 0) {
            nbread = in.read(buf, writeIndex, remaining);
            if (nbread == -1) {
                throw new EOFException();
            }

            remaining -= nbread;
            writeIndex += nbread;
        }
    }

    public static byte[] readLV(InputStream in, int vlen) throws IOException {
        byte[] lenBuf = new byte[vlen];
        in.read(lenBuf);
        int len = 0;
        for (int i = 0; i < lenBuf.length; i++) {
            len = len * 10 + (lenBuf[i] - '0');
        }
        if (len > 0) {
            byte[] buf = new byte[len];
            in.read(buf);
            return buf;
        }

        return new byte[]{};
    }

    public static void writeLV(OutputStream out, int vlen, byte[] buf)
            throws IOException {
        byte[] bytesHdr = String.format("%06d", buf.length).getBytes();
        byte[] bytesSent = new byte[buf.length + 6];

        System.arraycopy(bytesHdr, 0, bytesSent, 0, 6);
        System.arraycopy(buf, 0, bytesSent, 6, buf.length);

        out.write(bytesSent);
        out.flush();
    }
}
