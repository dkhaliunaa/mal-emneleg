package com.comm;

import com.Config;
import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.api.ContentResponse;

import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * SMS Gateway
 *
 * @author ub
 *
 */
public class SendSMS {

    static Properties props;

    public static final int MSG_LEN = 140;

    public static final HttpClient http = new HttpClient();

    public static final List<String> MOBICOM_PREFIX = Arrays
            .asList(new String[]{"99", "75", "94", "95"});

    public static final List<String> UNITEL_PREFIX = Arrays
            .asList(new String[] { "88", "89", "86" });

    public static final List<String> SKYTEL_PREFIX = Arrays
            .asList(new String[] { "91", "92", "50", "58", "96", "90" });

    public static final List<String> GMOBILE_PREFIX = Arrays
            .asList(new String[] { "98", "55", "93", "97", "53" });

    static void init() {

        try {
            // SMS тохиргоо унших
            props = Config.get("comm.properties");

            http.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * SMS илгээх
     *
     * @param to
     * @param msg
     */
    public static void send(String to, String msg) throws Exception {
        if (!http.isStarted()) {
            init();
        }

        final String pre = to.substring(0, 2);

        // Мессежийг таслаж багтаах
        if (msg.length() > MSG_LEN) {
            msg = msg.substring(0, MSG_LEN - 3) + "...";
        }
        if (UNITEL_PREFIX.contains(pre)) {
            _sendUnitel(to, msg);
        } else if (MOBICOM_PREFIX.contains(pre)) {
            _sendMobicom(to, msg);
        } else if (GMOBILE_PREFIX.contains(pre)) {
            _sendGMobile(to, msg);
        } else if (SKYTEL_PREFIX.contains(pre)) {
            _sendSkytel(to, msg);
        } else {
            throw new Exception(to + " - Unknown numbering prefix !");
        }

        System.out.println("sms.Send(" + to + "): " + msg);
    }

    static synchronized void _sendUnitel(String to, String msg)
            throws Exception {
        ContentResponse response = http.POST(props.getProperty("sms.unitel"))
                .param("uname", "golomt").param("from", "132525")
                .param("upass", "nL3cIkwwGr").param("mobile", to)
                .param("sms", msg).send();
        String str = response.getContentAsString();
        if (str.indexOf("SUCCESS") < 0) {
            throw new Exception(str);
        }
    }

    static synchronized void _sendMobicom(String to, String msg)
            throws Exception {
        ContentResponse response = http.POST(props.getProperty("sms.mobicom"))
                .param("user", "golomt").param("pwd", "golomt")
                .param("mobile", to).param("msg", msg).send();
        String str = response.getContentAsString();

        if (str.indexOf("Ok") < 0) {
            throw new Exception(str);
        }
    }

    static synchronized void _sendSkytel(String to, String msg)
            throws Exception {
        msg = URLEncoder.encode(msg, "utf-8");
        String url = props.getProperty("sms.skytel") + "?id=3&src=132525&dest=" + to
                + "&text=" + msg;

        ContentResponse response = http.GET(url);

        String str = response.getContentAsString();

        if (str.indexOf("OK") < 0) {
            throw new Exception(str);
        }
    }

    static synchronized void _sendGMobile(String to, String msg)
            throws Exception {
        msg = URLEncoder.encode(msg, "utf-8");
        ContentResponse response = http.GET(props.getProperty("sms.gmobile")
                + "?username=sms_golomt&password=smssms&to=" + to + "&text="
                + msg);
        String str = response.getContentAsString();

        // 0: Accepted for delivery
        if (str.indexOf("0:") < 0) {
            throw new Exception(str);
        }
    }

    public static void main(String[] args) throws Exception {
        send("99180558", "Easy\nInfo\nTest");
    }
}

