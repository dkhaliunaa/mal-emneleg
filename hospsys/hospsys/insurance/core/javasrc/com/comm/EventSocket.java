package com.comm;

import com.model.XMap;
import com.rpc.DateAdapter;
import com.rpc.DoubleSerializer;
import com.utils.ObjectIdSerializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.bson.types.ObjectId;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WebSocketAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by ub on 5/4/15.
 */
public class EventSocket extends WebSocketAdapter {
    public static final ConcurrentHashMap<String, Session> allSessionCache = new ConcurrentHashMap<>();
    public static final ConcurrentHashMap<String, Session> operatorSessionCache = new ConcurrentHashMap<>();
    public static final ConcurrentHashMap<String, Session> customerSessionCache = new ConcurrentHashMap<>();
    public static final ConcurrentHashMap<String, XMap>  operatorList = new ConcurrentHashMap<>();
    protected final GsonBuilder gsonBuilder = new GsonBuilder();

    public EventSocket() {
        gsonBuilder.registerTypeAdapter(Date.class, new DateAdapter());
        gsonBuilder.registerTypeAdapter(ObjectId.class,
                new ObjectIdSerializer());
        gsonBuilder.registerTypeAdapter(Date.class, new DateAdapter());
        gsonBuilder.registerTypeAdapter(Double.class, new DoubleSerializer());
    }


    @Override
    public void onWebSocketConnect(Session sess) {
        super.onWebSocketConnect(sess);
        sess.setIdleTimeout(7 *24 *60 *60 *1000);
        // TODO: видео session үүсгэх
//        System.out.println("SessionID "+getSession().suspend());
        String ipAddr = sess.getRemoteAddress().getHostName();
        allSessionCache.put(ipAddr,sess);

        System.out.println("Socket Connected: " + sess);
    }

    @Override
    public void onWebSocketText(String message) {
        super.onWebSocketText(message);
        // Parse message
        Gson gson = gsonBuilder.create();
        Event msg = gson.fromJson(message, Event.class);
        String key = getSession().getRemoteAddress().getHostName();
        Session tempSession;
        Boolean messageResp = true;
        checkDeadSessions();
        if(!msg.to.equals("NoDest") && !msg.to.equals("")){
            try{
                if(!allSessionCache.get(msg.to).isOpen()) {
                    System.out.println(msg.to + " is Closed ");
                    checkDeadSessions();
                    messageResp = false;
                }
            } catch(Exception e){
                e.getMessage();
            }
        }
        if(messageResp) {
            switch (msg.type) {
                case "webrtc.HELLO":
//                key = getSession().getRemoteAddress().getHostName();
                    if(msg.role.equals("Operator")){
                        operatorList.put(key, new XMap("name",msg.data.toString()).append("domain",msg.candidate.toString()).append("status","available"));
                        operatorSessionCache.put(key,getSession());
//                        sendOperatorsList(true);
                    } else {
                        customerSessionCache.put(key,getSession());
//                        sendOperatorsList(false);
                    }

                    System.out.println("Name: " + msg.data.toString());
                    break;

                case "webrtc.HANGUP":
                    System.out.println("Hangup or Decline");

                    msg.type = "webrtc.HANGUP";

                    //Хүсэлтийг хэн илгээснийг шалгаж байна
                    if(msg.role.equals("Operator")){
                        tempSession = customerSessionCache.get(msg.to);
                        System.out.println("Operator decline");
                        //Ямар хүсэлт болохийг шалгаж байна
                        if(operatorList.get(key).get("status").equals("available")){
                            msg.data = "Decline";
                        } else {
                            msg.data = "Hangup";
                        }
                    } else {
                        tempSession = operatorSessionCache.get(msg.to);
                        System.out.println("Customer decline");
                        //Ямар хүсэлт болохийг шалгаж байна
                        if(operatorList.get(msg.to).get("status").equals("available")){
                            msg.data = "Decline";
                        } else {
                            msg.data = "Hangup";
                        }
                    }

                    msg.to = key;

                    try {
                        tempSession.getRemote().sendString(gson.toJson(msg));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    break;

                case "webrtc.OFFER":
                    System.out.println("Offer");
                    msg.to = "not_available";
                    for ( ConcurrentHashMap.Entry<String, Session> entry : operatorSessionCache.entrySet()) {
                        String key_temp = entry.getValue().getRemoteAddress().getHostName();
                        if(operatorList.get(key_temp).get("status").equals("available")){
                            msg.to = key_temp;
                            break;
                        }
                    }


                    if(!msg.to.equals("not_available")){
                        operatorList.get(msg.to).put("status","busy");
//                        sendOperatorsList(true);
                        tempSession = operatorSessionCache.get(msg.to);
                        msg.to = key;
                        try {
                            tempSession.getRemote().sendString(gson.toJson(msg));
//                        System.out.println("Offer sent to operator");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        msg.type = "webrtc.BUSY";
                        msg.data = "Busy";
                        System.out.println("Operator busy");
                        try {
                            getSession().getRemote().sendString(gson.toJson(msg));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    break;

                case "webrtc.ANSWER":
                    operatorList.get(key).put("status","busy");
                    tempSession = allSessionCache.get(msg.to);
                    msg.to = key;
                    try {
                        tempSession.getRemote().sendString(gson.toJson(msg));
                        System.out.println("Answer sent to customer");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;

                case "webrtc.CANDIDATE":
                    if(!msg.to.equals("NoDest")){
                        tempSession = allSessionCache.get(msg.to);
                        msg.to = key;
                        try {
                            tempSession.getRemote().sendString(gson.toJson(msg));
                            System.out.println("Candidate sent to operator or customer");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    break;

                case "webrtc.ALIVE":
//                System.out.println("alive"+msg.role);
                    break;

                case "webrtc.BYE":
                    System.out.println(msg.role + " : "+ msg.to);
//                key =  getSession().getRemoteAddress().getHostName();
                    allSessionCache.remove(key);
                    if(msg.role.equals("Operator")) {
                        operatorSessionCache.remove(key);
                        operatorList.remove(key);
//                        sendOperatorsList(true);
                        if(!msg.to.equals("NoDest")){
                            tempSession = customerSessionCache.get(msg.to);
                            msg.to = key;
                            try {
                                tempSession.getRemote().sendString(gson.toJson(msg));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        customerSessionCache.remove(key);
                        if(!msg.to.equals("NoDest")) {
                            tempSession = operatorSessionCache.get(msg.to);
                            msg.to = key;
                            try {
                                tempSession.getRemote().sendString(gson.toJson(msg));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                    }

                    break;
            }
        }
        System.out.println("Received TEXT message: " + message);
    }

    @Override
    public void onWebSocketClose(int statusCode, String reason) {
        Gson gson = gsonBuilder.create();
        for ( ConcurrentHashMap.Entry<String, Session> entry : allSessionCache.entrySet()) {
            Session session = entry.getValue();
            if(session.isOpen()) {
                //Dead
                Event msg = new Event();
                msg.type = "webrtc.BYE";
                try {
                    session.getRemote().sendString(gson.toJson(msg));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        super.onWebSocketClose(statusCode, reason);
        System.out.println("Socket Closed: [" + statusCode + "] " + reason);
    }

    @Override
    public void onWebSocketError(Throwable cause) {
        super.onWebSocketError(cause);
        cause.printStackTrace(System.err);
    }


    public void checkDeadSessions(){
        System.out.println("total Sessions : "+ allSessionCache.size());
        Gson gson = gsonBuilder.create();
        for ( ConcurrentHashMap.Entry<String, Session> entry : allSessionCache.entrySet()) {
            Session session = entry.getValue();
            if(!session.isOpen()){
                //Connected but idle
                try{
                    String key = session.getRemoteAddress().getHostName();
                    allSessionCache.remove(key);
                    operatorSessionCache.remove(key);
                    customerSessionCache.remove(key);
                    operatorList.remove(key);
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }

//    public class checkIdleSessions implements Runnable{
//        public checkIdleSessions(){
//
//        }
//        @Override
//        public void run() {
//            do{
//                try {
//                    Thread.sleep(3000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                Gson gson = gsonBuilder.create();
//                for ( ConcurrentHashMap.Entry<String, Session> entry : allSessionCache.entrySet()) {
//                    Session session = entry.getValue();
//                    System.out.println("idle : "+session.getIdleTimeout());
//                    if(session.getIdleTimeout() >= 4000){
//                        if(session.isOpen()){
//                            //Dead
//                            Event msg = new Event();
//                            msg.type = "webrtc.BYE";
//                            try {
//                                session.getRemote().sendString(gson.toJson(msg));
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
//                        } else {
//                            //Connected but idle
//                            try{String key = session.getRemoteAddress().getHostName();
//                                allSessionCache.remove(key);
//                                operatorSessionCache.remove(key);
//                                customerSessionCache.remove(key);
//                                operatorList.remove(key);
//                            } catch (Exception e){
//                                e.printStackTrace();
//                            }
//                        }
//
//                    }
//                }
//            }while(allSessionCache.size() > 0);
//        }
//    }


    //Бүх хэрэглэгчидрүү болон тухайн нэг хэрэглэгчрүү Операторуудын list явуулна
    public void sendOperatorsList(Boolean toAll){
        Gson gson = gsonBuilder.create();

        List<XMap> operatorIps = new ArrayList<>();

//                    System.out.println("Size "+operatorSessionCache.size());

        for ( ConcurrentHashMap.Entry<String, Session> entry : operatorSessionCache.entrySet()) {
            String key = entry.getValue().getRemoteAddress().getHostName();
            XMap operator = operatorList.get(key).append("ip",key);
//            System.out.println("operator XMap : "+operator);
            operatorIps.add(operator);
        }

        Event msgEvent = new Event();
        msgEvent.type = "webrtc.getContacsResult";
        msgEvent.data = operatorIps;
//        System.out.println("Operators: "+operatorIps);

        try {
            if(toAll){
                for ( ConcurrentHashMap.Entry<String, Session> entry : customerSessionCache.entrySet()) {
                    entry.getValue().getRemote().sendString(gson.toJson(msgEvent));
                }
            } else {
                getRemote().sendString(gson.toJson(msgEvent));
            }


        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }
}
