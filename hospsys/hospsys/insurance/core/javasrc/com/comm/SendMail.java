package com.comm;

import com.Config;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;

public class SendMail {

	static {

	}


	public static void send(String to, String subject, String body) {
		try {
			// э-мэйлийн тохиргоо унших
			final Properties props = Config.get("comm.properties");

			Session session = Session.getInstance(props,
					new Authenticator() {
						@Override
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(props.getProperty("mail.smtp.user"),
									props.getProperty("mail.smtp.password"));
						}
					});

			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(props.getProperty("mail.smtp.from")));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(to.trim().replaceAll(" ", "")));
			message.setSubject(subject);
			message.setHeader("Content-Type", "text/html; charset=UTF-8");
			message.setContent(body, "text/html;charset=UTF-8");

            Transport.send(message);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

    public static void sendAttachment(String to, String subject, String body, String attachFile) {
        try {
            // э-мэйлийн тохиргоо унших
            final Properties props = Config.get("comm.properties");
            //mail@golomtbank.com -ийн тохиргоо байгаа
            Session session = Session.getInstance(props,
                    new Authenticator() {
                        @Override
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(props.getProperty("mail2.smtp.user"),
                                    props.getProperty("mail2.smtp.password"));
                        }
                    });

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(props.getProperty("mail2.smtp.from")));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to.trim().replaceAll(" ", "")));
            message.setSubject(subject);
            message.setHeader("Content-Type", "text/html; charset=UTF-8");

            // creates message part
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(body, "text/html;charset=UTF-8");

            // creates multi-part
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);

            // adds attachment
            if (attachFile != null) {
//                attachFile = "D:/Work/erp/selflab/web/contents/emailContents/loan8.pdf";
                MimeBodyPart attachPart = new MimeBodyPart();
                DataSource source = new FileDataSource(attachFile);
                attachPart.setDataHandler(new DataHandler(source));
                attachPart.setFileName("attachment.pdf");

//                try {
//                    DataHandler dataHandler = new DataHandler(new URL(attachFile));
//                    attachPart.setDataHandler(dataHandler);
//                    attachPart.setFileName("attachment.pdf");
//                    dataHandler.getOutputStream().close();
//                } catch (IOException ex) {
//                    ex.printStackTrace();
//                }
                multipart.addBodyPart(attachPart);
            }

            // sets the multi-part as e-mail's content
            message.setContent(multipart);
            Transport.send(message);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
