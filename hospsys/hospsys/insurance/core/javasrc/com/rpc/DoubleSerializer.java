package com.rpc;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.math.BigDecimal;

public class DoubleSerializer implements JsonSerializer<Double> {

    @Override
    public JsonElement serialize(Double value, Type arg1,
                                 JsonSerializationContext arg2) {
        return new JsonPrimitive((new BigDecimal(value)).setScale(3,
                BigDecimal.ROUND_HALF_UP));
    }
}
