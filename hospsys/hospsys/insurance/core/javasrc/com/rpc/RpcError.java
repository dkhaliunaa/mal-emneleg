package com.rpc;

@SuppressWarnings("serial")
public final class RpcError extends RuntimeException {

    private final Integer code;
    private final String msg;
    private final String data;

    public RpcError(Integer code, String msg, String data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return msg;
    }

    public String getData() {
        return data;
    }

    public static final RpcError PARSE_ERROR = new RpcError(2700,
            "PARSE_ERROR", "JSON parseRequest error");
    public static final RpcError INVALID_REQUEST = new RpcError(2600,
            "INVALID_REQUEST", "Invalid request");
    public static final RpcError INVALID_PARAMS = new RpcError(2602,
            "INVALID_PARAMS", "Invalid params");
    public static final RpcError INTERNAL_ERROR = new RpcError(666,
            "INTERNAL_ERROR", "Internal Error");
    public static final RpcError ACCESS_DENIED = new RpcError(2604,
            "ACCESS_DENIED", "Access denied");
    public static final RpcError PERMISSION_DENIED = new RpcError(2605,
            "PERMISSION_DENIED", "Permission denied");

    // 2700, JSON parseRequest error
    // 2600, Invalid request
    // 2601, Method not found
    // 2602, Invalid params
    // 666, Internal error
    // 2604, Access denied
    // 2605, Permission denied
    // 5001, "Grape системийн алдаа"
    // 5002, "Үлдэгдэл хүрэлцэхгүй байна"
    // 5004, "Дансны үлдэгдэл авахад алдаа гарлаа"
    // 5005, "Банк хоорондын гүйлгээний данс олдсонгүй"
    // 5006, "Картын данс олдсонгүй"
    // 5007, "Зарлага гаргах данс заах хэрэгтэй!"
    // 5008, "Орлогын данс заах хэрэгтэй!"
    // 5009, "Paymex алдаа"
    // 5010, "Дансны үлдэгдэл шалгахад алдаа гарлаа"
    // 5011, "Paymex хэрэглэгч олдсонгүй"
    // 5012, "Картын орлогын алдаа"
    // 5013, "Гүйлгээ давхардаж байна."
    // 5014, "Гүйлгээний төрөл буруу"
    // 5015, "Гүйлгээ давхардаж байна"
    // 5016, "Гүйлгээ олдсонгүй
    // 5031, "Дансны дугаар буруу"
    // 5041, "Карт бүртгэлгүй байна!"
    // 5051, "Дэд гишүүн банкны карт байна!"
    // 5052, "Картын хугацаа дууссан тул гүйлгээ хийх боломжгүй!"
    // 5053, "Картын төлөв гүйлгээ хийж болохооргүй байна!"
    // 5054, "Гүйлгээ хийхэд хэрэглэгчийн эрх хүрэхгүй байна!"
    // 5055, "USD local card-руу онлайн орлого хийх боломжгүй!"
    // 5056, "Картын мэдээлэл байхгүй!"
    // 5057, "Дансны мэдээлэл байхгүй!"
    // 5060, "Хүлээн авагчийн мэдээлэл байхгүй!"
    // 5061, "Валютын ханш 0 байж болохгүй!"
    // 5062, "Гүйлгээний clientId талбар хоосон байна!"
    // 5063, "Хуудаслалтын хязгаар хэтэрсэн!"
    // 5064, "Гүйлгээ буцаахад алдаа гарлаа!"
    // 5066, "Grape системээс хоосон хариу ирлээ!"
    // 5077, "Картын дуусах хугацаа тодорхойгүй байна!"
    // 5078, "Харилцагчийн регистрийн дугаар олдсонгүй!"
    // 5080, "Валютын ханш авахад алдаа гарлаа!"
    // 5081, "Хуулганы интервал буруу!"

    private static final int SERVER_ERROR_START = -32000;
    public static final int METHOD_NOT_FOUND = 2601;

    /**
     * Server error range : (-32099..-32000)
     */

    public static int getServerError(int n) {
        return SERVER_ERROR_START - n;
    }

    @Override
    public String toString() {
        return "code:" + code + ", msg:" + msg + ", data:" + data;
    }
}
