package com.rpc;

import java.lang.reflect.Method;
import java.util.*;

public class RpcHandler<T> {

    private final T obj;
    private final Map<String, String[]> signatures;
    private final Set<Method> methods;
    private final TypeChecker typeChecker = new GsonTypeChecker();

    public RpcHandler(T handler, Class<?> cls) {
        if (handler == null) {
            throw new NullPointerException("obj");
        }

        this.obj = handler;

        Map<String, List<String>> map = new HashMap<String, List<String>>();
        Set<Method> set = new HashSet<Method>();

        if (!cls.isInterface()) {
            throw new IllegalArgumentException(
                    "class should be an interface : " + cls);
        }

        for (Method m : cls.getMethods()) {
            set.add(m);
            Class<?>[] params = m.getParameterTypes();

            List<String> list = map.get(m.getName());
            if (list == null) {
                list = new ArrayList<String>();
            }
            StringBuilder buff = new StringBuilder(typeChecker.getTypeName(m
                    .getReturnType()));
            for (int i = 0; i < params.length; i++) {
                buff.append(",").append(typeChecker.getTypeName(params[i]));
            }
            list.add(buff.toString());
            map.put(m.getName(), list);
        }

        Map<String, String[]> signs = new TreeMap<String, String[]>();
        for (Map.Entry<String, List<String>> e : map.entrySet()) {
            String[] arr = new String[e.getValue().size()];
            signs.put(e.getKey(), e.getValue().toArray(arr));
        }

        this.methods = Collections.unmodifiableSet(set);
        this.signatures = Collections.unmodifiableMap(signs);
    }

    public T getObj() {
        return obj;
    }

    public java.util.Map<String, String[]> getSignatures() {
        return signatures;
    }

    public java.util.Set<java.lang.reflect.Method> getMethods() {
        return methods;
    }
}
