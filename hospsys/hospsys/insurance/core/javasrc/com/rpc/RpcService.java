package com.rpc;

import com.model.User;
import com.utils.ObjectIdSerializer;
import com.google.gson.*;
import org.bson.types.ObjectId;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by ub on 4/2/15.
 */
public abstract class RpcService {
    protected RpcHandler<?> handler;
    protected String requestId;
    protected String method;
    protected JsonArray params;
    protected String requestIPAddress;

    // нууцлалын нэмэлт параметрүүд
    protected String token;
    protected String remoteAddr;
    protected User.AuthType authType;

    // serializer
    protected final GsonBuilder gsonBuilder = new GsonBuilder();


    public RpcService() {
        gsonBuilder.registerTypeAdapter(Date.class, new DateAdapter());
        gsonBuilder.registerTypeAdapter(ObjectId.class,
                new ObjectIdSerializer());
        gsonBuilder.registerTypeAdapter(Date.class, new DateAdapter());
        gsonBuilder.registerTypeAdapter(Double.class, new DoubleSerializer());
    }

    public String getServiceName() {
        return this.getClass().getSimpleName();
    }

    public String welcome() {
        StringBuilder sb = new StringBuilder();

        sb.append(this.getServiceName() +  " is serving...\n</br>");
        sb.append("\n</br>");
        sb.append("Methods: \n</br>");
        sb.append("-------------------------\n</br>");

        if (handler != null) {
            Map<String, String[]> sings = handler.getSignatures();
            for (Map.Entry e:  sings.entrySet()) {
                sb.append(e.getKey()).append(": ").append(e.getValue()).append("\n</br>");
            }
        }

        return sb.toString();
    }

    public static String getClientIpAddr(HttpServletRequest request) {

        String newIPAddress = null;
        try {
            String ipAddress = "";
            InetAddress inetAddress = InetAddress.getLocalHost();
            ipAddress = inetAddress.getHostAddress();
            newIPAddress = ipAddress;

            return newIPAddress;
        } catch (Exception e) {
            System.err.println("Хүсэлтийн IP хаягийг авахад алдаа гарлаа : " + e);
            return newIPAddress;
        }
    }

    /**
     *  Тухайлсан нэг хүсэлтийг боловсруулна
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    public void dispatch(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        // эхэлсэн цаг
        long startTime = System.currentTimeMillis();
        JsonObject jsonResp = new JsonObject();

        try {
            // хүсэлтийг задлах
            parseRequest(req, resp);

            // гүйцэтгэх
            execute(jsonResp);

        } catch (Throwable t) {

            if (t instanceof RpcError) {
                RpcError e = (RpcError) t;
                addError(jsonResp, e.getCode(), e.getMessage(), e.getData());
            } else {
                addError(jsonResp, RpcError.getServerError(1),
                        t.getCause().toString(), "Invalid request!");
            }

        } finally {
            // Хариуг клиент руу илгээх
            try {
                long duration = System.currentTimeMillis() - startTime;
                String responseData = jsonResp.toString();
                System.out.println("Server (Duration=" + duration + ") >> "
                        + responseData);
                writeResponse(resp, responseData);
            } catch (Exception e) {
                System.err.println("Хариу илгээхэд алдаа гарлаа : " + e);
            }
        }
    }

    protected void parseRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {

        // хүсэлтийн бие унших
        String cmd = readBody(req);

        // хүсэлтийг задлах
        final JsonParser parser = new JsonParser();
        JsonObject jsonReq = (JsonObject) parser.parse(new StringReader(cmd));

        if (!jsonReq.has("id")) {
            throw RpcError.INVALID_REQUEST;
        }


        if (!jsonReq.has("method")) {
            throw RpcError.INVALID_REQUEST;
        }

        requestId = jsonReq.get("id").getAsString();

        authType = User.AuthType.LDAP; // default утга
        if (jsonReq.has("authType")) {
            if ("APP".equals( jsonReq.get("authType").getAsString() )) {
                authType = User.AuthType.APP;
            } else {
                // бусад горимыг зөвшөөрөхгүй, LDAP гэж тооцно
            }
        }

        // методын нэр, параметрүүдийг унших
        String methodFull = jsonReq.getAsJsonPrimitive("method").getAsString();
        int i = methodFull.lastIndexOf('.');
        if (i > 0) {
            method = methodFull.substring(i);
        } else {
            method = methodFull;
        }

        if(method.equals("login")){
            // log бичихгүй console руу хэвлэхгүй
        } else {
            System.out.println("Server <<  " + cmd);
        }

        requestIPAddress = getClientIpAddr(req);

        params = (JsonArray) jsonReq.get("params");
        if (params == null) {
            params = new JsonArray();
        }

        remoteAddr = req.getRemoteAddr();

        if ("login".equals(method)) {
            // login-г шалгахгүй өнгөрөөх
        } else {
            if (!jsonReq.has("token")) {
                throw RpcError.ACCESS_DENIED;
            }

            token = jsonReq.get("token").getAsString();

            // хэрэглэгчийг шалгах (шалгах эсэхийг тохиргоогоор шийдэх)
            // Энэ ямар учиртай код юм болоо сонин юм аа
            /*if (!authenticate()) {
                throw new AccessControlException("Access Denied!");
            }*/
        }
    }

    protected boolean authenticate() {
        return false;
    }

    /**
     * RPC функцийг ажиллуулна
     *
     * @param resp
     * @throws Exception
     */
    public void execute(JsonObject resp) throws Exception {
        resp.addProperty("jsonrpc", "2.0");
        resp.addProperty("id", requestId);

        Method executableMethod = null;

        for (Method m : handler.getMethods()) {
            if (!m.getName().equals(method)) {
                continue;
            }

            if (canExecute(m, params)) {
                executableMethod = m;
                break;
            }
        }

        if (executableMethod == null) {
            throw new RpcError(RpcError.METHOD_NOT_FOUND, "Method not found", method);
        }

        Object o = executableMethod.invoke(handler.getObj(), getParameters(executableMethod, params));

        JsonElement result = gsonBuilder.serializeNulls().create().toJsonTree(o);
        resp.add("result", result);
    }

    public boolean canExecute(Method method, JsonArray params) {
        if (method.getParameterTypes().length != params.size()) {
            return false;
        }

        // TODO: тухайн методыг дуудах эрхтэй эсэхийг шалгах

        return true;
    }

    public Object[] getParameters(Method method, JsonArray params) {
        List<Object> list = new ArrayList<>();
        final Gson gson = gsonBuilder.create();
        Class<?>[] types = method.getParameterTypes();

        for (int i = 0; i < types.length; i++) {
            JsonElement p = params.get(i);
            Object o = gson.fromJson(p.toString(), types[i]);
            list.add(o);
        }

        return list.toArray();
    }

    private static final int BUFF_LENGTH = 1024;

    public String readBody(HttpServletRequest req) throws IOException {
        try (InputStream in = req.getInputStream()) {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            byte[] buff = new byte[BUFF_LENGTH];
            int n;
            while ((n = in.read(buff)) > 0) {
                bos.write(buff, 0, n);
            }

            return bos.toString();
        }
    }

    private void addError(JsonObject resp, Integer code, String message,
                          String data) {
        // алдаатай тохиолдолд 'result' талбар байхгүй
        resp.remove("result");

        JsonObject error = new JsonObject();
        if (code != null) {
            error.addProperty("code", code);
        }

        if (message != null) {
            error.addProperty("message", message);
        }

        if (data != null) {
            error.addProperty("data", data);
        }

        resp.add("error", error);

    }

    public void writeResponse(HttpServletResponse resp, String responseData)
            throws Exception {
        byte[] data = responseData.getBytes("utf-8");
        resp.setHeader("Access-Control-Allow-Origin", "*");
        resp.setHeader("Access-Control-Allow-Methods", "POST");
        resp.setHeader("Access-Control-Allow-Headers", "*");
        resp.setHeader("Content-Type", "application/json;charset=utf-8");
        resp.setHeader("Content-Length", Integer.toString(data.length));
        resp.setContentType("application/json;charset=utf-8");
        resp.setStatus(HttpServletResponse.SC_OK);

        OutputStream out = resp.getOutputStream();
        out.write(data, 0, data.length);
        // out.flush();
    }

    public static int getLineNumber() {
        return Thread.currentThread().getStackTrace()[2].getLineNumber();
    }
}
