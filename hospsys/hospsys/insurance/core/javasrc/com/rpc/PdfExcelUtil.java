package com.rpc;

import net.sf.jasperreports.engine.JRStyle;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.design.JRDesignStyle;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.Map;

public class PdfExcelUtil {

    public static void generateReport(String file, String JRXML_PATH, String REPORT_PATH, String fileName, Map parameters, Connection connection) throws Exception{
        System.out.println(parameters);
        JasperPrint jasperPrint = JasperFillManager.fillReport(JRXML_PATH + "/" + fileName + ".jasper", parameters, connection);
        JRStyle jrStyle = new JRDesignStyle();
        jrStyle.setFontName("DejaVu Serif");
        jrStyle.setPdfEncoding("Identity-H");
        jrStyle.setPdfEmbedded(true);
        jasperPrint.setDefaultStyle(jrStyle);
        if (file.equals("pdf")) {
            OutputStream outputStream = new FileOutputStream(new File(REPORT_PATH + "/" +fileName+ ".pdf"));
            JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
        } else if (file.equals("xls")) {
            ByteArrayOutputStream outputByteArray = new ByteArrayOutputStream();
            OutputStream outputfile = new FileOutputStream(new File(REPORT_PATH + "/" + fileName +".xls"));

            JRXlsExporter xlsExporter = new JRXlsExporter();
            xlsExporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            xlsExporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outputByteArray);
            xlsExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
            xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            xlsExporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.FALSE);
            xlsExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);

            xlsExporter.exportReport();
            outputfile.write(outputByteArray.toByteArray());
        }
    }
}
