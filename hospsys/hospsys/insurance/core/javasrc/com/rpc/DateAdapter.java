package com.rpc;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateAdapter implements JsonSerializer<Date>,
        JsonDeserializer<Date> {
    static final SimpleDateFormat longFormat = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss");
    static final SimpleDateFormat shortFormat = new SimpleDateFormat(
            "yyyy-MM-dd");

    @Override
    public JsonElement serialize(Date d, Type arg1,
                                 JsonSerializationContext arg2) {
        Calendar cal = Calendar.getInstance();
        cal.clear(); // as per BalusC comment.
        cal.setTime(d);
        if (cal.get(Calendar.HOUR) == 0 && cal.get(Calendar.MINUTE) == 0
                && cal.get(Calendar.SECOND) == 0) {
            return new JsonPrimitive(shortFormat.format(d));
        }
        return new JsonPrimitive(longFormat.format(d));
    }

    @Override
    public Date deserialize(JsonElement json, Type arg1,
                            JsonDeserializationContext arg2) throws JsonParseException {
        String dateStr = json.getAsJsonPrimitive().getAsString();
        Date ret = null;
        if (dateStr.length() == 10) {
            try {
                ret = shortFormat.parse(dateStr);
            } catch (Exception ex) {
                throw new JsonParseException(ex.getMessage());
            }
        } else {
            try {
                ret = longFormat.parse(dateStr);
            } catch (Exception ex) {
                throw new JsonParseException(ex.getMessage());
            }
        }

        return ret;
    }
}
