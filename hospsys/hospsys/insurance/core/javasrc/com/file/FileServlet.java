package com.file;

import com.Config;
import com.google.gson.JsonObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;


public class FileServlet extends HttpServlet {

	void allowXORS(HttpServletRequest req, HttpServletResponse resp) {
		resp.setHeader("Access-Control-Allow-Origin", "*");
		resp.setHeader("Access-Control-Allow-Methods", "*");
		resp.setHeader("Access-Control-Allow-Headers", req.getHeader("Access-Control-Request-Headers"));
	}

	@Override
	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		allowXORS(req, resp);
	}

	// Файл унших
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		allowXORS(req, resp);

		try {
            System.out.println(" 123 " + req);


			Path filePath = Paths.get(Config.getFilePath(), req.getPathInfo());

			if (Files.exists(filePath)) {
				// mime type
				String content = Files.probeContentType(filePath);
				resp.setContentType(content);
				Files.copy(filePath, resp.getOutputStream());
			} else {
				resp.sendError(HttpServletResponse.SC_NOT_FOUND);
			}

		} catch (Exception ex) {
			resp.sendError(HttpServletResponse.SC_NOT_FOUND);
		}

	}

	// Файл бичих
	// file/filename.pdf
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		allowXORS(req, resp);

		String requestedPath = req.getPathInfo();

		if (requestedPath.startsWith("/")) {
			requestedPath = requestedPath.substring(1);
		}

		if (requestedPath.equalsIgnoreCase("")) {
			resp.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
			return;
		}

		JsonObject jsonResp = new JsonObject();
		jsonResp.addProperty("jsonrpc", "2.0");

		try {
			Path storePath = Paths.get(Config.getFilePath(), requestedPath);

			String fileName = storePath.getFileName().toString();

			if (fileName.contains(".")) {
				if (Files.exists(storePath)) {
					// ийм нэртэй файл байгаа учраас, файлын нэрийг солих
					String renamedPath = renameFile(requestedPath);
					storePath = Paths.get(Config.getFilePath(), renamedPath);

					Files.copy(req.getInputStream(), storePath);
					jsonResp.addProperty("result", renamedPath);
				} else {
					Files.copy(req.getInputStream(), storePath);
					jsonResp.addProperty("result", requestedPath);
				}
			} else {
				// өртгөтгөл байхгүй файлыг заасан нэрээр нь дарж хадгална
				Files.copy(req.getInputStream(), storePath, StandardCopyOption.REPLACE_EXISTING);
				jsonResp.addProperty("result", requestedPath);
			}
		} catch (Exception ex) {
			addError(jsonResp, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Файл хадгалахад алдаа гарлаа!", req
					.getPathInfo().toString());
		}

		jsonResponse(resp, jsonResp);
	}

	/**
	 * Файлын нэрийн ард дугаарлалтыг өсгөж нэрийг солино
	 * 
	 * @param filePath
	 * @return
	 */
	String renameFile(String filePath) {
		int i = filePath.length() - 1;

		// өргөтгөлийг алгасах
		String ext = "";
		while (i > 0 && filePath.charAt(i) != '.') {
			ext = filePath.charAt(i) + ext;
			i--;
		}

		// давхардсан файлуудыг dup гэж нэрлэх
		return "dup_" + String.valueOf(System.nanoTime()) + "." + ext;

	}

	// Файл устгах
	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Path filePath = Paths.get(Config.getFilePath(), req.getPathInfo());

		try {
			Files.delete(filePath);
		} catch (Exception ex) {

		}

		allowXORS(req, resp);
	}

	public void jsonResponse(HttpServletResponse resp, JsonObject json) throws IOException {
		String responseData = json.toString();
		byte[] data = responseData.getBytes("utf-8");
		resp.setHeader("Content-Type", "application/json;charset=utf-8");
		resp.setHeader("Content-Length", Integer.toString(data.length));

		OutputStream out = resp.getOutputStream();
		out.write(data);
	}

	private void addError(JsonObject resp, Integer code, String message, String data) {
		JsonObject error = new JsonObject();
		if (code != null) {
			error.addProperty("code", code);
		}

		if (message != null) {
			error.addProperty("message", message);
		}

		if (data != null) {
			error.addProperty("data", data);
		}

		resp.add("error", error);
		resp.remove("result");
	}

}
