package com.file;

import com.Config;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLDecoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("serial")
public class PictureService extends HttpServlet {

	private static final int BUF_SIZE = 100 * 1024 * 1024; // 100MB
	
    void allowXORS(HttpServletRequest req, HttpServletResponse resp) {
        resp.setHeader("Access-Control-Allow-Origin", "*");
        resp.setHeader("Access-Control-Allow-Methods", "*");
        resp.setHeader("Access-Control-Allow-Headers", req.getHeader("Access-Control-Request-Headers"));
    }

    @Override
    protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        allowXORS(req, resp);
    }

	// picture/?filename=filename.jpg
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        allowXORS(req, resp);
		String path = Config.getTmpPath();

		String fileName = req.getParameter("filename");

		if (fileName == null) {
			resp.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
			return;
		}

		File file = new File(path, URLDecoder.decode(fileName, "UTF-8"));

		try (InputStream in = req.getInputStream();
				BufferedOutputStream bos = new BufferedOutputStream(
						new FileOutputStream(file))) {
			byte[] buff = new byte[BUF_SIZE];
			int n;
			while ((n = in.read(buff)) > 0) {
				bos.write(buff, 0, n);
			}
		} catch (IOException e) {
			System.out.println("Exception IMG >> " + e.toString());
		}
	}

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        allowXORS(req, resp);

		String path = Config.getTmpPath();

		String fileName = req.getParameter("filename");
		if (fileName == null || fileName.trim().equals("")) {
			((HttpServletResponse) resp)
					.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
			return;
		}
		
		String ext = "";
		Pattern p = Pattern.compile("(.*?)(\\..*)?");
		Matcher m = p.matcher(fileName);
		
		if (m.matches()) {
			
			// group 2 is the suffix
			ext = m.group(2).toLowerCase();
		}

		String content = "";
		String htype = "";
		switch (ext) {
		
		case ".jpeg":
		case ".jpg":
			content = "image/jpg";
			htype = "attachment";
			break;
		case ".gif":
			content = "image/gif";
			htype = "attachment";
			break;
		case ".png":
			content = "image/png";
			htype = "attachment";
			break;
		case ".tif":
			content = "image/tif";
			htype = "attachment";
			break;
			
			
		}
		
		resp.setContentType(content);
		resp.setHeader("Content-Disposition", "" + htype + "; filename="
                + fileName + "");

		File file = new File(path, URLDecoder.decode(fileName,
				"UTF-8"));

		try (FileInputStream fileIn = new FileInputStream(file);
				ServletOutputStream out = resp.getOutputStream()) {
			byte[] outputByte = new byte[(int) file.length()];
			// copy binary contect to output stream
			while (fileIn.read(outputByte, 0, (int) file.length()) != -1) {
				out.write(outputByte, 0, (int) file.length());
			}

			out.flush();
		} 
	}
}
