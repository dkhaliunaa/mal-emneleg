package com.enumclass;

/**
 * Created by Tiku on 8/23/2017.
 *
 * Тогтмол байдаг алдааны төрлүүд
 */
public enum ErrorType {
    FATAL,
    EXCEPTION,
    ERROR,
    WARNING,
    INFO
}