package com.hospital;

import com.model.MRegCodeGenEntity;
import com.model.ProjectErrorCode;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by 24be10264 on 2/22/2017.
 */
public class MRegCodeGenControl {

    private Connection connection = null;

    public MRegCodeGenControl(Connection connection){
        this.connection = connection;
    }

    /**
     *
     * @param genEntity
     * @param errorCode
     * @return
     * @throws SQLException
     * @throws Exception
     */
    public int getGenerateCode(MRegCodeGenEntity genEntity, ProjectErrorCode errorCode) throws SQLException, Exception{
        int genCode = 0;

        try{
            boolean isUpdate = false;
            int selIndex = 1;
            String query = null;
            PreparedStatement statement = null;
            ResultSet resultset = null;

            if (genEntity != null){
                query = "SELECT " +
                        "   M.CURRYEAR, M.ORGCODE, M.PRODCODE, " +
                        "   M.SEQCODE  " +
                        "FROM INSUDB.M_REGCODEGEN M WHERE ";

                query += " ORGCODE = ? AND ";

                query += " PRODCODE = ? AND ";

                query += " CURRYEAR = ? ";

                statement = connection.prepareStatement(query);

                statement.setString(selIndex++, genEntity.getOrgCode());

                statement.setString(selIndex++, genEntity.getProdCode());

                if (currentYear() == 0){
                    errorCode.setErrCode(1195);
                    return 0;
                }
                statement.setInt(selIndex++, currentYear());

                resultset = statement.executeQuery();

                while(resultset.next()){
                    genCode = resultset.getInt("SEQCODE");
                    isUpdate = true;
                }

                if (statement != null) statement.close();
                if (resultset != null) resultset.close();

                genCode ++;

                genEntity.setSeqCode(genCode);
                genEntity.setYear(currentYear());

                if (isUpdate){
                    if (!update(genEntity, errorCode)) {
                        genCode = 0;
                    }
                }else {
                    if (!insert(genEntity, errorCode)) {
                        genCode = 0;
                    }
                }
            }else {
                errorCode.setErrCode(1194);
            }
        }catch (Exception ex){
            genCode = 0;
            ex.printStackTrace();
            errorCode.setErrCode(1193);
        }

        return genCode;
    }//end for


    /**
     *
     * @param genEntity
     * @param errorCode
     * @return
     */
    public boolean insert(MRegCodeGenEntity genEntity, ProjectErrorCode errorCode){
        try{
            String query = "";
            PreparedStatement statement = null;


            query = "INSERT INTO INSUDB.M_REGCODEGEN (" +
                    "   CURRYEAR, ORGCODE, PRODCODE, " +
                    "   SEQCODE) " +
                    "VALUES ( ?, ?, ?, ?)";

            statement = connection.prepareStatement(query);

            statement.setInt(1, genEntity.getYear());
            statement.setString(2, genEntity.getOrgCode());
            statement.setString(3, genEntity.getProdCode());
            statement.setInt(4, genEntity.getSeqCode());

            int insRows = statement.executeUpdate();

            if (insRows == 1){
                return true;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return false;
    }

    public boolean update(MRegCodeGenEntity genEntity, ProjectErrorCode errorCode){
        try{
            String query = "";
            PreparedStatement statement = null;


            query = "UPDATE INSUDB.M_REGCODEGEN " +
                    "SET SEQCODE  = ? " +
                    "WHERE CURRYEAR = ? AND ORGCODE = ? AND PRODCODE = ?";

            statement = connection.prepareStatement(query);

            statement.setInt(1, genEntity.getSeqCode());
            statement.setInt(2, genEntity.getYear());
            statement.setString(3, genEntity.getOrgCode());
            statement.setString(4, genEntity.getProdCode());

            int insRows = statement.executeUpdate();

            if (insRows == 1){
                return true;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return false;

    }

    private int currentYear(){
        try{
            DateFormat dateFormat = new SimpleDateFormat("YYYY");
            Date date = new Date();
            return Integer.parseInt(dateFormat.format(date));
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return 0;
    }

    /*public static void main(String args[]){
        DateFormat dateFormat = new SimpleDateFormat("YY");
        Date date = new Date();
        System.out.println(dateFormat.format(date));
    }*/
}
