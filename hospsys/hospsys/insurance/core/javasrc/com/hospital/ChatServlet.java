package com.hospital;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.model.User;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;

public class ChatServlet extends WebSocketServlet {
    public static HashMap<Session, User> chatSessionList = new HashMap<>();

    @Override
    public void init() throws ServletException {
        super.init();

        System.out.println("Init");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
        System.out.println("Do get");
    }


    @Override
    public void configure(WebSocketServletFactory webSocketServletFactory) {
        System.out.println("configure");

        // set a 10 second idle timeout
        webSocketServletFactory.getPolicy().setIdleTimeout(60*60*1000);
        // register my socket
        webSocketServletFactory.register(ChatSocket.class);
    }
}
