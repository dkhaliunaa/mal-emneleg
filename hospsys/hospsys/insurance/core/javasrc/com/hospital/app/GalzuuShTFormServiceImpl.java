package com.hospital.app;


import com.enumclass.ErrorType;
import com.jolbox.bonecp.BoneCP;
import com.model.hos.ErrorEntity;
import com.model.hos.GalzuuShTFormEntity;
import com.mongodb.BasicDBObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tilyeujan on 9/2/2017.
 */
public class GalzuuShTFormServiceImpl implements IGalzuuShTFormService {
    /**
     * Objects
     */
    private BoneCP boneCP;
    private ErrorEntity errorEntity;


    public GalzuuShTFormServiceImpl(BoneCP boneCP) {
        this.boneCP = boneCP;
    }

    @Override
    public ErrorEntity insertNewData(GalzuuShTFormEntity entity) throws SQLException {
        PreparedStatement statement = null;

        if (entity == null) {
            errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));

            errorEntity.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errorEntity.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            
            return errorEntity;
        }

        String query_login = "INSERT INTO `hform01_shtemdeg`(`form01key`, `delflg`, `actflg`, " +
                "`cre_at`, `cre_by`, `mod_at`, `mod_by`, `record_date`, `citycode`, `sumcode`, `bagcode`, " +
                "`malchinner`, `haluursan`, `helen_d_ulhii`, `zavj_d_ulhii`, `huliin_sal_ulhii`, `unjiih`, " +
                "`tejeel_idehgu`, `str1`, `str2`) " +
                "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        try (Connection connection = boneCP.getConnection()) {
            try{
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, (entity.get(entity.FORM01KEY) != null) ? entity.getForm01key().trim() : "");
                statement.setString(2, (entity.get(entity.DELFLG) != null) ? entity.getDelflg().trim() : "");
                statement.setString(3, (entity.get(entity.ACTFLG) != null) ? entity.getActflg().trim() : "");
                statement.setDate(4, entity.getCre_at());
                statement.setString(5, (entity.get(entity.CRE_BY) != null) ? entity.getCre_by().trim() : "");
                statement.setDate(6, entity.getMod_at());
                statement.setString(7, (entity.get(entity.MOD_BY) != null) ? entity.getMod_by().trim() : "");
                statement.setDate(8, entity.getRecord_date());
                statement.setString(9, (entity.get(entity.CITYCODE) != null) ? entity.getCitycode().trim() : "");
                statement.setString(10, (entity.get(entity.SUMCODE) != null) ? entity.getSumcode().trim() : "");
                statement.setString(11, (entity.get(entity.BAGCODE) != null) ? entity.getBagcode().trim() : "");
                statement.setString(12, (entity.get(entity.MALCHINNER) != null) ? entity.getMalchinner().trim() : "");
                statement.setString(13, (entity.get(entity.HALUURSAN) != null) ? entity.getHaluursan().trim() : "");
                statement.setString(14, (entity.get(entity.HELEN_D_ULHII) != null) ? entity.getHelen_d_ulhii().trim() : "");
                statement.setString(15, (entity.get(entity.ZAVJ_D_ULHII) != null) ? entity.getZavj_d_ulhii().trim() : "");
                statement.setString(16, (entity.get(entity.HULIIN_SAL_ULHII) != null) ? entity.getHuliin_sal_ulhii().trim() : "");
                statement.setString(17, (entity.get(entity.UNJIIH) != null) ? entity.getUnjiih().trim() : "");
                statement.setString(18, (entity.get(entity.TEJEEL_IDEHGU) != null) ? entity.getTejeel_idehgu().trim() : "");
                statement.setString(19, (entity.get(entity.STR1) != null) ? entity.getStr1().trim() : "");
                statement.setString(20, (entity.get(entity.STR2) != null) ? entity.getStr2().trim() : "");

                int insertCount = statement.executeUpdate();

                if (insertCount == 1){
                    connection.commit();

                    errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));

                    errorEntity.setErrText("Бичлэг амжилттай хадгалагдлаа.");
                    errorEntity.setErrDesc("Бичлэг амжилттай хадгалагдлаа.");

                    return errorEntity;
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }finally {
                if (statement != null)statement.close();
                connection.close();
            }

        }catch (Exception ex){

        }

        errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1057));
        errorEntity.setErrText("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");
        errorEntity.setErrDesc("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");

        return errorEntity;
    }

    @Override
    public List<GalzuuShTFormEntity> getListData(GalzuuShTFormEntity entity) throws SQLException {
        int stIndex = 1;
        int index = 1;
        List<GalzuuShTFormEntity> entityList = null;

        String query = "SELECT `hform01_shtemdeg`.`id`, `hform01_shtemdeg`.`form01key`, `hform01_shtemdeg`.`delflg`, " +
                "`hform01_shtemdeg`.`actflg`, `hform01_shtemdeg`.`cre_at`, `hform01_shtemdeg`.`cre_by`, `hform01_shtemdeg`.`mod_at`, " +
                "`hform01_shtemdeg`.`mod_by`, " +
                "`hform01_shtemdeg`.`record_date`, `hform01_shtemdeg`.`citycode`, `hform01_shtemdeg`.`sumcode`, " +
                "`hform01_shtemdeg`.`bagcode`, `hform01_shtemdeg`.`malchinner`, `hform01_shtemdeg`.`haluursan`, `hform01_shtemdeg`.`helen_d_ulhii`, " +
                "`hform01_shtemdeg`.`zavj_d_ulhii`, `hform01_shtemdeg`.`huliin_sal_ulhii`, `hform01_shtemdeg`.`unjiih`, " +
                "`hform01_shtemdeg`.`tejeel_idehgu`, `hform01_shtemdeg`.`str1`, `hform01_shtemdeg`.`str2`," +
                "`h_sum`.sumname, `h_sum`.sumname_en, `h_baghoroo`.horooname, `h_baghoroo`.horooname_en, " +
                "`h_city`.cityname, `h_city`.cityname_en " +
                "FROM `hform01_shtemdeg` INNER JOIN `h_city` " +
                "ON `h_city`.code = `hform01_shtemdeg`.citycode " +
                "INNER JOIN `h_sum` " +
                "ON `h_sum`.sumcode = `hform01_shtemdeg`.sumcode " +
                "INNER JOIN `h_baghoroo` ON `hform01_shtemdeg`.bagcode = `h_baghoroo`.horoocode AND `h_baghoroo`.sumcode = `hform01_shtemdeg`.`sumcode` " +
                "WHERE ";

        if (entity.getForm01key() != null && !entity.getForm01key().isEmpty()){
            query += " `hform01_shtemdeg`.`form01key` = ? AND ";
        }
        if (entity.getCitycode() != null && !entity.getCitycode().isEmpty()){
            query += " `hform01_shtemdeg`.`citycode` = ? AND ";
        }
        if (entity.getSumcode() != null && !entity.getSumcode().isEmpty()){
            query += " `hform01_shtemdeg`.`sumcode` = ? AND ";
        }
        if (entity.getBagcode() != null && !entity.getBagcode().isEmpty()){
            query += " `hform01_shtemdeg`.`bagcode` = ? AND ";
        }
        if (entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
            query += " `hform01_shtemdeg`.delflg = ? AND ";
        }
        if (entity.getActflg() != null && !entity.getActflg().isEmpty()){
            query += " `hform01_shtemdeg`.actflg = ? AND ";
        }
        if (entity != null && entity.getCre_by() != null && !entity.getCre_by().isEmpty()){
            query += " `hform01_shtemdeg`.cre_by = ? AND ";
        }

        query += " `hform01_shtemdeg`.`id` > 0 ";
        query += " ORDER BY `hform01_shtemdeg`.`cre_at` DESC ";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query);

                if (entity.getForm01key() != null && !entity.getForm01key().isEmpty()){
                    statement.setString(stIndex++, entity.getForm01key());
                }
                if (entity.getCitycode() != null && !entity.getCitycode().isEmpty()){
                    statement.setString(stIndex++, entity.getCitycode());
                }
                if (entity.getSumcode() != null && !entity.getSumcode().isEmpty()){
                    statement.setString(stIndex++, entity.getSumcode());
                }
                if (entity.getBagcode() != null && !entity.getBagcode().isEmpty()){
                    statement.setString(stIndex++, entity.getBagcode());
                }
                if (entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
                    statement.setString(stIndex++, entity.getDelflg());
                }
                if (entity.getActflg() != null && !entity.getActflg().isEmpty()){
                    statement.setString(stIndex++, entity.getActflg());
                }
                if (entity != null && entity.getCre_by() != null && !entity.getCre_by().isEmpty()){
                    statement.setString(stIndex++, entity.getCre_by());
                }


                ResultSet resultSet = statement.executeQuery();

                entityList = new ArrayList<>();

                while (resultSet.next()) {
                    GalzuuShTFormEntity objEntity = new GalzuuShTFormEntity();

                    objEntity.setId(resultSet.getLong("id"));
                    objEntity.setForm01key(resultSet.getString("form01key"));
                    objEntity.setCitycode(resultSet.getString("citycode"));
                    objEntity.setSumcode(resultSet.getString("sumcode"));
                    objEntity.setBagcode(resultSet.getString("bagcode"));
                    objEntity.setMalchinner(resultSet.getString("malchinner"));
                    objEntity.setHaluursan(resultSet.getString("haluursan"));
                    objEntity.setHelen_d_ulhii(resultSet.getString("helen_d_ulhii"));
                    objEntity.setZavj_d_ulhii(resultSet.getString("zavj_d_ulhii"));
                    objEntity.setHuliin_sal_ulhii(resultSet.getString("huliin_sal_ulhii"));
                    objEntity.setUnjiih(resultSet.getString("unjiih"));
                    objEntity.setTejeel_idehgu(resultSet.getString("tejeel_idehgu"));
                    objEntity.setStr1(resultSet.getString("str1"));
                    objEntity.setStr2(resultSet.getString("str2"));
                    objEntity.setDelflg(resultSet.getString("delflg"));
                    objEntity.setCre_at(resultSet.getDate("cre_at"));
                    objEntity.setCre_by(resultSet.getString("cre_by"));
                    objEntity.setMod_at(resultSet.getDate("mod_at"));
                    objEntity.setMod_by(resultSet.getString("mod_by"));
                    objEntity.setRecord_date(resultSet.getDate("record_date"));
                    objEntity.setActflg(resultSet.getString("actflg"));

                    objEntity.put("cityname", resultSet.getString("cityname"));
                    objEntity.put("sumname", resultSet.getString("sumname"));
                    objEntity.put("horooname", resultSet.getString("horooname"));
                    objEntity.put("index", index++);
                    objEntity.put("strRecordOgnoo", dateFormat.format(objEntity.getRecord_date()));

                    entityList.add(objEntity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return entityList;
    }

    /**
     * Get name
     *
     * @param cmbType
     * @param mal_turul_mn
     * @return
     */
    private String getTypeNer(List<BasicDBObject> cmbType, String mal_turul_mn) {
        for (BasicDBObject obj :
                cmbType) {
            if (obj.get("id") != null && mal_turul_mn != null && obj.getString("id").equalsIgnoreCase(mal_turul_mn)) {
                return obj.getString("name");
            }
        }

        return "";
    }

    @Override
    public ErrorEntity updateData(GalzuuShTFormEntity entity) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (entity == null) new ErrorEntity(ErrorType.INFO, Long.valueOf(1059));
        if (entity == null) {
            errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errorEntity.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errorEntity.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errorEntity;
        }

        query = "UPDATE `hospital`.`hform01_shtemdeg` " +
                "SET     ";

        if (entity.getForm01key() != null && !entity.getForm01key().isEmpty()){
            query += " `hform01_shtemdeg`.`form01key` = ?, ";
        }
        if (entity.getCitycode() != null && !entity.getCitycode().isEmpty()){
            query += " `hform01_shtemdeg`.`citycode` = ?, ";
        }
        if (entity.getSumcode() != null && !entity.getSumcode().isEmpty()){
            query += " `hform01_shtemdeg`.`sumcode` = ?, ";
        }
        if (entity.getBagcode() != null && !entity.getBagcode().isEmpty()){
            query += " `hform01_shtemdeg`.`bagcode` = ?, ";
        }
        if (entity.getMalchinner() != null && !entity.getMalchinner().isEmpty()){
            query += " `hform01_shtemdeg`.`malchinner` = ?, ";
        }
        if (entity.getHaluursan() != null && !entity.getHaluursan().isEmpty()){
            query += " `hform01_shtemdeg`.`haluursan` = ?, ";
        }

        if (entity.getHelen_d_ulhii() != null && !entity.getHelen_d_ulhii().isEmpty()){
            query += " `hform01_shtemdeg`.`helen_d_ulhii` = ?, ";
        }
        if (entity.getZavj_d_ulhii() != null && !entity.getZavj_d_ulhii().isEmpty()){
            query += " `hform01_shtemdeg`.`zavj_d_ulhii` = ?, ";
        }
        if (entity.getHuliin_sal_ulhii() != null && !entity.getHuliin_sal_ulhii().isEmpty()){
            query += " `hform01_shtemdeg`.`huliin_sal_ulhii` = ?, ";
        }
        if (entity.getUnjiih() != null && !entity.getUnjiih().isEmpty()){
            query += " `hform01_shtemdeg`.`unjiih` = ?, ";
        }
        if (entity.getTejeel_idehgu() != null && !entity.getTejeel_idehgu().isEmpty()){
            query += " `hform01_shtemdeg`.`tejeel_idehgu` = ?, ";
        }
        if (entity.getStr1() != null && !entity.getStr1().isEmpty()){
            query += " `hform01_shtemdeg`.`str1` = ?, ";
        }
        if (entity.getStr2() != null && !entity.getStr2().isEmpty()){
            query += " `hform01_shtemdeg`.`str2` = ?, ";
        }

        if (entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
            query += " `hform01_shtemdeg`.delflg = ?, ";
        }
        if (entity.getActflg() != null && !entity.getActflg().isEmpty()){
            query += " `hform01_shtemdeg`.actflg = ?, ";
        }
        if (entity.getRecord_date() != null){
            query += " `hform01_shtemdeg`.`record_date` = ?,";
        }

        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";
        query += " WHERE  ID   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (entity.getForm01key() != null && !entity.getForm01key().isEmpty()){
                    statement.setString(stIndex++, entity.getForm01key());
                }
                if (entity.getCitycode() != null && !entity.getCitycode().isEmpty()){
                    statement.setString(stIndex++, entity.getCitycode());
                }
                if (entity.getSumcode() != null && !entity.getSumcode().isEmpty()){
                    statement.setString(stIndex++, entity.getSumcode());
                }
                if (entity.getBagcode() != null && !entity.getBagcode().isEmpty()){
                    statement.setString(stIndex++, entity.getBagcode());
                }
                if (entity.getMalchinner() != null && !entity.getMalchinner().isEmpty()){
                    statement.setString(stIndex++, entity.getMalchinner());
                }
                if (entity.getHaluursan() != null && !entity.getHaluursan().isEmpty()){
                    statement.setString(stIndex++, entity.getHaluursan());
                }

                if (entity.getHelen_d_ulhii() != null && !entity.getHelen_d_ulhii().isEmpty()){
                    statement.setString(stIndex++, entity.getHelen_d_ulhii());
                }
                if (entity.getZavj_d_ulhii() != null && !entity.getZavj_d_ulhii().isEmpty()){
                    statement.setString(stIndex++, entity.getZavj_d_ulhii());
                }
                if (entity.getHuliin_sal_ulhii() != null && !entity.getHuliin_sal_ulhii().isEmpty()){
                    statement.setString(stIndex++, entity.getHuliin_sal_ulhii());
                }
                if (entity.getUnjiih() != null && !entity.getUnjiih().isEmpty()){
                    statement.setString(stIndex++, entity.getUnjiih());
                }
                if (entity.getTejeel_idehgu() != null && !entity.getTejeel_idehgu().isEmpty()){
                    statement.setString(stIndex++, entity.getTejeel_idehgu());
                }
                if (entity.getStr1() != null && !entity.getStr1().isEmpty()){
                    statement.setString(stIndex++, entity.getStr1());
                }
                if (entity.getStr2() != null && !entity.getStr2().isEmpty()){
                    statement.setString(stIndex++, entity.getStr2());
                }

                if (entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
                    statement.setString(stIndex++, entity.getDelflg());
                }
                if (entity.getActflg() != null && !entity.getActflg().isEmpty()){
                    statement.setString(stIndex++, entity.getActflg());
                }
                if (entity.getRecord_date() != null){
                    statement.setDate(stIndex++, entity.getRecord_date());
                }

                statement.setDate(stIndex++, entity.getMod_at());
                statement.setString(stIndex++, entity.getMod_by());
                statement.setLong(stIndex++, entity.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                    errorEntity.setErrText("Бичлэг амжилттай засагдлаа.");
                    errorEntity.setErrDesc("Бичлэг амжилттай засагдлаа.");
                    return errorEntity;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }


        errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1061));
        errorEntity.setErrText("Амжилтгүй боллоо. Дахин оролдоно уу.");
        errorEntity.setErrDesc("Амжилтгүй боллоо. Дахин оролдоно уу.");

        return errorEntity;
    }
}
