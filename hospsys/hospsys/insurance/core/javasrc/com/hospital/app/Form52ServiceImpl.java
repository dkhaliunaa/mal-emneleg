package com.hospital.app;


import com.Config;
import com.enumclass.ErrorType;
import com.jolbox.bonecp.BoneCP;
import com.model.hos.City;
import com.model.hos.ErrorEntity;
import com.model.hos.Form52Entity;
import com.model.hos.Sum;
import com.mongodb.BasicDBObject;
import com.rpc.PdfExcelUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public class Form52ServiceImpl implements IForm52Service {
    /**
     * Objects
     */

    private BoneCP boneCP;
    private ErrorEntity errObj;


    public Form52ServiceImpl(BoneCP boneCP) {
        this.boneCP = boneCP;
    }

    @Override
    public ErrorEntity insertNewData(Form52Entity form52) throws SQLException {
        PreparedStatement statement = null;

        if (form52 == null) {
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        String query_login = "INSERT INTO `hospital`.`hform52` " +
                "(`uvchinNer`, " +
                "`uvchinNer_En`, " +
                "`bugdUvchilsen`, " +
                "`bugdUhsen`, " +
                "`bugdEdgersen`, " +
                "`temeeUvchilsen`, " +
                "`temeeUhsen`, " +
                "`temeeEdgersen`, " +
                "`aduuUvchilsen`, " +
                "`aduuUhsen`, " +
                "`aduuEdgersen`, " +
                "`uherUvchilsen`, " +
                "`uherUhsen`, " +
                "`uherEdgersen`, " +
                "`honiUvchilsen`, " +
                "`honiUhsen`, " +
                "`honiEdgersen`, " +
                "`yamaaUvchilsen`, " +
                "`yamaaUhsen`, " +
                "`yamaaEdgersen`, " +
                "`busadUvchilsen`, " +
                "`busadUhsen`, " +
                "`busadEdgersen`, " +
                "`del_flg`, " +
                "`actflg`, " +
                "`mod_at`, " +
                "`mod_by`, " +
                "`cre_at`, " +
                "`recorddate`," +
                "`cre_by`," +
                "`aimag`," +
                "`aimag_en`," +
                "`sum`," +
                "`sum_en`)" +
                "VALUES " +
                "(? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? ,? , ? ,? ,? , ? , ? , ? , ? , ? , ? , ? , ? ,? , ? , ? , ? )";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, form52.getUvchinNer());
                statement.setString(2, form52.getUvchinNer_En());
                statement.setLong(3, form52.getBugdUvchilsen());
                statement.setLong(4, form52.getBugdUhsen());
                statement.setLong(5, form52.getBugdEdgersen());
                statement.setLong(6, form52.getTemeeUvchilsen());
                statement.setLong(7, form52.getTemeeUhsen());
                statement.setLong(8, form52.getTemeeEdgersen());
                statement.setLong(9, form52.getAduuUvchilsen());
                statement.setLong(10, form52.getAduuUhsen());
                statement.setLong(11, form52.getAduuEdgersen());
                statement.setLong(12, form52.getUherUvchilsen());
                statement.setLong(13, form52.getUherUhsen());
                statement.setLong(14, form52.getUherEdgersen());
                statement.setLong(15, form52.getHoniUvchilsen());
                statement.setLong(16, form52.getHoniUhsen());
                statement.setLong(17, form52.getHoniEdgersen());
                statement.setLong(18, form52.getYamaaUvchilsen());
                statement.setLong(19, form52.getYamaaUhsen());
                statement.setLong(20, form52.getYamaaEdgersen());
                statement.setLong(21, form52.getBusadUvchilsen());
                statement.setLong(22, form52.getBusadUhsen());
                statement.setLong(23, form52.getBusadEdgersen());
                statement.setString(24, form52.getDelflg());
                statement.setString(25, form52.getActflg());
                statement.setDate(26, form52.getModAt());
                statement.setString(27, form52.getModby());
                statement.setDate(28, form52.getCreat());
                statement.setDate(29, form52.getRecordDate());
                statement.setString(30, form52.getCreby());
                statement.setString(31, form52.getAimag());
                statement.setString(32, form52.getAimag_en());
                statement.setString(33, form52.getSum());
                statement.setString(34, form52.getSum_en());


                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));
                    errObj.setErrText("Бичлэг амжилттай хадгалагдлаа.");
                    errObj.setErrDesc("Бичлэг амжилттай хадгалагдлаа.");
                    return errObj;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }

        } catch (Exception ex) {

        }

        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1057));
        errObj.setErrText("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");
        errObj.setErrDesc("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");

        return errObj;
    }

    @Override
    public List<Form52Entity> getListData(Form52Entity form52) throws SQLException {
        int stIndex = 1;
        List<Form52Entity> form52EntitieList = null;

        String query = "SELECT `hform52`.`id`, " +
                "    `hform52`.`uvchinNer`, " +
                "    `hform52`.`uvchinNer_En`, " +
                "    `hform52`.`bugdUvchilsen`, " +
                "    `hform52`.`bugdUhsen`, " +
                "    `hform52`.`bugdEdgersen`, " +
                "    `hform52`.`temeeUvchilsen`, " +
                "    `hform52`.`temeeUhsen`, " +
                "    `hform52`.`temeeEdgersen`, " +
                "    `hform52`.`aduuUvchilsen`, " +
                "    `hform52`.`aduuUhsen`, " +
                "    `hform52`.`aduuEdgersen`, " +
                "    `hform52`.`uherUvchilsen`, " +
                "    `hform52`.`uherUhsen`, " +
                "    `hform52`.`uherEdgersen`, " +
                "    `hform52`.`honiUvchilsen`, " +
                "    `hform52`.`honiUhsen`, " +
                "    `hform52`.`honiEdgersen`, " +
                "    `hform52`.`yamaaUvchilsen`, " +
                "    `hform52`.`yamaaUhsen`, " +
                "    `hform52`.`yamaaEdgersen`, " +
                "    `hform52`.`busadUvchilsen`, " +
                "    `hform52`.`busadUhsen`, " +
                "    `hform52`.`busadEdgersen`, " +
                "    `hform52`.`del_flg`, " +
                "    `hform52`.`actflg`, " +
                "    `hform52`.`mod_at`, " +
                "    `hform52`.`mod_by`, " +
                "    `hform52`.`cre_at`, " +
                "    `hform52`.`recorddate`, " +
                "    `hform52`.`cre_by`, " +
                "    `hform52`.`sum`, " +
                "    `hform52`.`sum_en`, " +
                "    `hform52`.`aimag`, " +
                "    `hform52`.`aimag_en`, " +
                "    c.cityname, c.cityname_en, " +
                "    `h_sum`.sumname, `h_sum`.sumname_en," +
                "    `h_sum`.latitude as sum_latitude, `h_sum`.longitude as sum_longitude  " +
                "FROM `hospital`.`hform52` INNER JOIN `h_city` AS c " +
                "ON c.code = `hform52`.aimag " +
                "INNER JOIN `h_sum` " +
                "ON `h_sum`.sumcode = `hform52`.sum " +
                "WHERE ";

//        /* NER */
        if (form52.getUvchinNer() != null && !form52.getUvchinNer().toString().isEmpty()) {
            query += " uvchinNer = ? AND ";
        }
        if (form52.getUvchinNer_En() != null && !form52.getUvchinNer_En().toString().isEmpty()) {
            query += " uvchinNer_En = ? AND ";
        }
        /* Zartsuulsan hemjee */
        if (form52.getBugdUvchilsen() != null && !form52.getBugdUvchilsen().toString().isEmpty() && form52.getBugdUvchilsen() != 0) {
            query += " bugdUvchilsen = ? AND ";
        }
        if (form52.getBugdUhsen() != null && !form52.getBugdUhsen().toString().isEmpty() && form52.getBugdUhsen() != 0) {
            query += " bugdUhsen = ? AND ";
        }

        /* MAL TURUL */
        if (form52.getBugdEdgersen() != null && !form52.getBugdEdgersen().toString().isEmpty() && form52.getBugdEdgersen() != 0) {
            query += " bugdEdgersen = ? AND ";
        }
        if (form52.getTemeeUvchilsen() != null && !form52.getTemeeUvchilsen().toString().isEmpty() && form52.getTemeeUvchilsen() != 0) {
            query += " temeeUvchilsen = ? AND ";
        }

        /* Tuluvlugu */
        if (form52.getTemeeUhsen() != null && !form52.getTemeeUhsen().toString().isEmpty() && form52.getTemeeUhsen() != 0) {
            query += " temeeUhsen = ? AND ";
        }
        if (form52.getTemeeEdgersen() != null && !form52.getTemeeEdgersen().toString().isEmpty() && form52.getTemeeEdgersen() != 0) {
            query += " temeeEdgersen = ? AND ";
        }

        /* Guitsetgel */
        if (form52.getAduuUvchilsen() != null && !form52.getAduuUvchilsen().toString().isEmpty() && form52.getAduuUvchilsen() != 0) {
            query += " aduuUvchilsen = ? AND ";
        }
        if (form52.getAduuUhsen() != null && !form52.getAduuUhsen().toString().isEmpty() && form52.getAduuUhsen() != 0) {
            query += " aduuUhsen = ? AND ";
        }

        //ASD
        /* Zartsuulsan hemjee */
        if (form52.getAduuEdgersen() != null && !form52.getAduuEdgersen().toString().isEmpty() && form52.getAduuEdgersen() != 0) {
            query += " aduuEdgersen = ? AND ";
        }
        if (form52.getUherUvchilsen() != null && !form52.getUherUvchilsen().toString().isEmpty() && form52.getUherUvchilsen() != 0) {
            query += " uherUvchilsen = ? AND ";
        }

        /* MAL TURUL */
        if (form52.getUherUhsen() != null && !form52.getUherUhsen().toString().isEmpty() && form52.getUherUhsen() != 0) {
            query += " uherUhsen = ? AND ";
        }
        if (form52.getUherEdgersen() != null && !form52.getUherEdgersen().toString().isEmpty() && form52.getUherEdgersen() != 0) {
            query += " uherEdgersen = ? AND ";
        }

        /* Tuluvlugu */
        if (form52.getHoniUvchilsen() != null && !form52.getHoniUvchilsen().toString().isEmpty() && form52.getHoniUvchilsen() != 0) {
            query += " honiUvchilsen = ? AND ";
        }
        if (form52.getHoniUhsen() != null && !form52.getHoniUhsen().toString().isEmpty() && form52.getHoniUhsen() != 0) {
            query += " honiUhsen = ? AND ";
        }

        /* Guitsetgel */
        if (form52.getHoniEdgersen() != null && !form52.getHoniEdgersen().toString().isEmpty() && form52.getHoniEdgersen() != 0) {
            query += " honiEdgersen = ? AND ";
        }
        if (form52.getYamaaUvchilsen() != null && !form52.getYamaaUvchilsen().toString().isEmpty() && form52.getYamaaUvchilsen() != 0) {
            query += " yamaaUvchilsen = ? AND ";
        }

        if (form52.getYamaaUhsen() != null && !form52.getYamaaUhsen().toString().isEmpty() && form52.getYamaaUhsen() != 0) {
            query += " yamaaUhsen = ? AND ";
        }
        if (form52.getYamaaEdgersen() != null && !form52.getYamaaEdgersen().toString().isEmpty() && form52.getYamaaEdgersen() != 0) {
            query += " yamaaEdgersen = ? AND ";
        }



        if (form52.getBusadUvchilsen() != null && !form52.getBusadUvchilsen().toString().isEmpty() && form52.getBusadUvchilsen() != 0) {
            query += " busadUvchilsen = ? AND ";
        }

        if (form52.getBusadUhsen() != null && !form52.getBusadUhsen().toString().isEmpty() && form52.getBusadUhsen() != 0) {
            query += " busadUhsen = ? AND ";
        }
        if (form52.getBusadEdgersen() != null && !form52.getBusadEdgersen().toString().isEmpty() && form52.getBusadEdgersen() != 0) {
            query += " busadEdgersen = ? AND ";
        }
        //ASD

        if (form52 != null && form52.getAimag() != null && !form52.getAimag().isEmpty()) {
            query += " `aimag` = ? AND ";
        }
        if (form52 != null && form52.getAimag_en() != null && !form52.getAimag_en().isEmpty()) {
            query += " `aimag_en` = ? AND ";
        }
        if (form52.getSum() != null && !form52.getSum().isEmpty()) {
            query += " sum = ? AND ";
        }

        if (form52.getSum_en() != null && !form52.getSum_en().isEmpty()) {
            query += " sum_en = ? AND ";
        }

        if (form52 != null && form52.getRecordDate() != null && form52.getSearchRecordDate() != null) {
            query += " (`recorddate` BETWEEN ? AND ?) AND ";
        }

        if (form52 != null && form52.getCreby() != null && !form52.getCreby().isEmpty()) {
            query += " `hform52`.`cre_by` = ? AND ";
        }

        query += " `hform52`.`del_flg` = 'N' ORDER BY `hform52`.recorddate DESC";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query);

//            /* NER */
                if (form52.getUvchinNer() != null && !form52.getUvchinNer().toString().isEmpty()) {
                    statement.setString(stIndex++, form52.getUvchinNer());
                }
                if (form52.getUvchinNer_En() != null && !form52.getUvchinNer_En().toString().isEmpty()) {
                    statement.setString(stIndex++, form52.getUvchinNer_En());
                }

            /* Zartsuulsan hemjee */
                if (form52.getBugdUvchilsen() != null && !form52.getBugdUvchilsen().toString().isEmpty() && form52.getBugdUvchilsen() != 0) {
                    statement.setLong(stIndex++, form52.getBugdUvchilsen());
                }
                if (form52.getBugdUhsen() != null && !form52.getBugdUhsen().toString().isEmpty() && form52.getBugdUhsen() != 0) {
                    statement.setLong(stIndex++, form52.getBugdUhsen());
                }

            /* MAL TURUL */
                if (form52.getBugdEdgersen() != null && !form52.getBugdEdgersen().toString().isEmpty() && form52.getBugdEdgersen() != 0) {
                    statement.setLong(stIndex++, form52.getBugdEdgersen());
                }
                if (form52.getTemeeUvchilsen() != null && !form52.getTemeeUvchilsen().toString().isEmpty() && form52.getTemeeUvchilsen() != 0) {
                    statement.setLong(stIndex++, form52.getTemeeUvchilsen());
                }

            /* Tuluvlugu */
                if (form52.getTemeeUhsen() != null && !form52.getTemeeUhsen().toString().isEmpty() && form52.getTemeeUhsen().longValue() != 0) {
                    statement.setLong(stIndex++, form52.getTemeeUhsen());
                }
                if (form52.getTemeeEdgersen() != null && !form52.getTemeeEdgersen().toString().isEmpty() && form52.getTemeeEdgersen().longValue() != 0) {
                    statement.setLong(stIndex++, form52.getTemeeEdgersen());
                }

            /* Guitsetgel */
                if (form52.getAduuUvchilsen() != null && !form52.getAduuUvchilsen().toString().isEmpty() && form52.getAduuUvchilsen().longValue() != 0) {
                    statement.setLong(stIndex++, form52.getAduuUvchilsen());
                }
                if (form52.getAduuUhsen() != null && !form52.getAduuUhsen().toString().isEmpty() && form52.getAduuUhsen().longValue() != 0) {
                    statement.setLong(stIndex++, form52.getAduuUhsen());
                }

                if (form52.getAduuEdgersen() != null && !form52.getAduuEdgersen().toString().isEmpty() && form52.getAduuEdgersen().longValue() != 0) {
                    statement.setLong(stIndex++, form52.getAduuEdgersen());
                }
                //////////////////////////////////////////////////
                if (form52.getUherUvchilsen() != null && !form52.getUherUvchilsen().toString().isEmpty() && form52.getUherUvchilsen().longValue() != 0) {
                    statement.setLong(stIndex++, form52.getUherUvchilsen());
                }
                if (form52.getUherUhsen() != null && !form52.getUherUhsen().toString().isEmpty() && form52.getUherUhsen().longValue() != 0) {
                    statement.setLong(stIndex++, form52.getUherUhsen());
                }

                if (form52.getUherEdgersen() != null && !form52.getUherEdgersen().toString().isEmpty() && form52.getUherEdgersen().longValue() != 0) {
                    statement.setLong(stIndex++, form52.getUherEdgersen());
                }
                if (form52.getHoniUvchilsen() != null && !form52.getHoniUvchilsen().toString().isEmpty() && form52.getHoniUvchilsen().longValue() != 0) {
                    statement.setLong(stIndex++, form52.getHoniUvchilsen());
                }
                if (form52.getHoniUhsen() != null && !form52.getHoniUhsen().toString().isEmpty() && form52.getHoniUhsen().longValue() != 0) {
                    statement.setLong(stIndex++, form52.getHoniUhsen());
                }

                if (form52.getHoniEdgersen() != null && !form52.getHoniEdgersen().toString().isEmpty() && form52.getHoniEdgersen().longValue() != 0) {
                    statement.setLong(stIndex++, form52.getHoniEdgersen());
                }
                if (form52.getYamaaUvchilsen() != null && !form52.getYamaaUvchilsen().toString().isEmpty() && form52.getYamaaUvchilsen().longValue() != 0) {
                    statement.setLong(stIndex++, form52.getYamaaUvchilsen());
                }
                if (form52.getYamaaUhsen() != null && !form52.getYamaaUhsen().toString().isEmpty() && form52.getYamaaUhsen().longValue() != 0) {
                    statement.setLong(stIndex++, form52.getYamaaUhsen());
                }

                if (form52.getYamaaEdgersen() != null && !form52.getYamaaEdgersen().toString().isEmpty() && form52.getYamaaEdgersen().longValue() != 0) {
                    statement.setLong(stIndex++, form52.getYamaaEdgersen());
                }
                if (form52.getBusadUvchilsen() != null && !form52.getBusadUvchilsen().toString().isEmpty() && form52.getBusadUvchilsen().longValue() != 0) {
                    statement.setLong(stIndex++, form52.getBusadUvchilsen());
                }
                if (form52.getBusadUhsen() != null && !form52.getBusadUhsen().toString().isEmpty() && form52.getBusadUhsen().longValue() != 0) {
                    statement.setLong(stIndex++, form52.getBusadUhsen());
                }

                if (form52.getBusadEdgersen() != null && !form52.getBusadEdgersen().toString().isEmpty() && form52.getBusadEdgersen().longValue() != 0) {
                    statement.setLong(stIndex++, form52.getBusadEdgersen());
                }
                ////////////////////////////////////////////////////

                if (form52 != null && form52.getAimag() != null && !form52.getAimag().isEmpty()) {
                    statement.setString(stIndex++, form52.getAimag());
                }

                if (form52 != null && form52.getAimag_en() != null && !form52.getAimag_en().isEmpty()) {
                    statement.setString(stIndex++, form52.getAimag_en());
                }

                if (form52 != null && form52.getSum() != null && !form52.getSum().isEmpty()) {
                    statement.setString(stIndex++, form52.getSum());
                }

                if (form52 != null && form52.getSum_en() != null && !form52.getSum_en().isEmpty()) {
                    statement.setString(stIndex++, form52.getSum_en());
                }

                if (form52 != null && form52.getRecordDate() != null && form52.getSearchRecordDate() != null) {
                    statement.setDate(stIndex++, form52.getRecordDate());
                    statement.setDate(stIndex++, form52.getSearchRecordDate());
                }
                if (form52 != null && form52.getCreby() != null && !form52.getCreby().isEmpty()) {
                    statement.setString(stIndex++, form52.getCreby());
                }

                ResultSet resultSet = statement.executeQuery();

                form52EntitieList = new ArrayList<>();

                IComboService comboService = new ComboServiceImpl(boneCP);
                List<BasicDBObject> uvchinNer = comboService.getComboVals("HGUVCH", "MN");

                int index = 1;

                while (resultSet.next()) {
                    Form52Entity entity = new Form52Entity();

                    entity.setId(resultSet.getLong("id"));
                    entity.setUvchinNer(resultSet.getString("uvchinNer"));
                    entity.setUvchinNer_En(resultSet.getString("uvchinNer_En"));
                    entity.setBugdUvchilsen(resultSet.getLong("bugdUvchilsen"));
                    entity.setBugdUhsen(resultSet.getLong("bugdUhsen"));
                    entity.setBugdEdgersen(resultSet.getLong("bugdEdgersen"));
                    entity.setTemeeUvchilsen(resultSet.getLong("temeeUvchilsen"));
                    entity.setTemeeUhsen(resultSet.getLong("temeeUhsen"));
                    entity.setTemeeEdgersen(resultSet.getLong("temeeEdgersen"));
                    entity.setAduuUvchilsen(resultSet.getLong("aduuUvchilsen"));
                    entity.setAduuUhsen(resultSet.getLong("aduuUhsen"));
                    entity.setAduuEdgersen(resultSet.getLong("aduuEdgersen"));
                    entity.setUherUvchilsen(resultSet.getLong("uherUvchilsen"));
                    entity.setUherUhsen(resultSet.getLong("uherUhsen"));
                    entity.setUherEdgersen(resultSet.getLong("uherEdgersen"));
                    entity.setHoniUvchilsen(resultSet.getLong("honiUvchilsen"));
                    entity.setHoniUhsen(resultSet.getLong("honiUhsen"));
                    entity.setHoniEdgersen(resultSet.getLong("honiEdgersen"));
                    entity.setYamaaUvchilsen(resultSet.getLong("yamaaUvchilsen"));
                    entity.setYamaaUhsen(resultSet.getLong("yamaaUhsen"));
                    entity.setYamaaEdgersen(resultSet.getLong("yamaaEdgersen"));
                    entity.setBusadUvchilsen(resultSet.getLong("busadUvchilsen"));
                    entity.setBusadUhsen(resultSet.getLong("busadUhsen"));
                    entity.setBusadEdgersen(resultSet.getLong("busadEdgersen"));
                    entity.setDelflg(resultSet.getString("del_flg"));
                    entity.setActflg(resultSet.getString("actflg"));
                    entity.setModAt(resultSet.getDate("mod_at"));
                    entity.setModby(resultSet.getString("mod_by"));
                    entity.setCreat(resultSet.getDate("cre_at"));
                    entity.setRecordDate(resultSet.getDate("recorddate"));
                    entity.setCreby(resultSet.getString("cre_by"));
                    entity.setSum(resultSet.getString("sum"));
                    entity.setSum_en(resultSet.getString("sum_en"));
                    entity.setAimag(resultSet.getString("aimag"));
                    entity.setAimag_en(resultSet.getString("aimag_en"));
                    entity.put("uvchinner", getTypeNer(uvchinNer, resultSet.getString("uvchinNer")));
                    entity.put("sum_latitude", resultSet.getString("sum_latitude"));
                    entity.put("sum_longitude", resultSet.getString("sum_longitude"));
                    entity.put("index", index++);

                    form52EntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return form52EntitieList;
    }

    /**
     * Get name
     *
     * @param cmbType
     * @param mal_turul_mn
     * @return
     */
    private String getTypeNer(List<BasicDBObject> cmbType, String mal_turul_mn) {
        for (BasicDBObject obj :
                cmbType) {
            if (obj.get("id") != null && mal_turul_mn != null && obj.getString("id").equalsIgnoreCase(mal_turul_mn)) {
                return obj.getString("name");
            }
        }

        return "";
    }

    @Override
    public ErrorEntity updateData(Form52Entity form52) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        query = "UPDATE `hospital`.`hform52` " +
                "SET     ";

        if (form52.getSum() != null && !form52.getSum().isEmpty()) {
            query += " sum = ?, ";
        }

        if (form52.getSum_en() != null && !form52.getSum_en().isEmpty()) {
            query += " sum_en = ? , ";
        }

        if (form52.getUvchinNer() != null && !form52.getUvchinNer().toString().isEmpty()) {
            query += " uvchinNer = ? , ";
        }
        if (form52.getUvchinNer_En() != null && !form52.getUvchinNer_En().toString().isEmpty()) {
            query += " uvchinNer_En = ? , ";
        }
        /* Zartsuulsan hemjee */
        if (form52.getBugdUvchilsen() != null && !form52.getBugdUvchilsen().toString().isEmpty() && form52.getBugdUvchilsen() != 0) {
            query += " bugdUvchilsen = ? , ";
        }
        if (form52.getBugdUhsen() != null && !form52.getBugdUhsen().toString().isEmpty() && form52.getBugdUhsen() != 0) {
            query += " bugdUhsen = ? , ";
        }

        /* MAL TURUL */
        if (form52.getBugdEdgersen() != null && !form52.getBugdEdgersen().toString().isEmpty() && form52.getBugdEdgersen() != 0) {
            query += " bugdEdgersen = ? , ";
        }
        if (form52.getTemeeUvchilsen() != null && !form52.getTemeeUvchilsen().toString().isEmpty() && form52.getTemeeUvchilsen() != 0) {
            query += " temeeUvchilsen = ? , ";
        }

        /* Tuluvlugu */
        if (form52.getTemeeUhsen() != null && !form52.getTemeeUhsen().toString().isEmpty() && form52.getTemeeUhsen() != 0) {
            query += " temeeUhsen = ? , ";
        }
        if (form52.getTemeeEdgersen() != null && !form52.getTemeeEdgersen().toString().isEmpty() && form52.getTemeeEdgersen() != 0) {
            query += " temeeEdgersen = ? , ";
        }

        /* Guitsetgel */
        if (form52.getAduuUvchilsen() != null && !form52.getAduuUvchilsen().toString().isEmpty() && form52.getAduuUvchilsen() != 0) {
            query += " aduuUvchilsen = ? , ";
        }
        if (form52.getAduuUhsen() != null && !form52.getAduuUhsen().toString().isEmpty() && form52.getAduuUhsen() != 0) {
            query += " aduuUhsen = ? , ";
        }

        //ASD
        /* Zartsuulsan hemjee */
        if (form52.getAduuEdgersen() != null && !form52.getAduuEdgersen().toString().isEmpty() && form52.getAduuEdgersen() != 0) {
            query += " aduuEdgersen = ? , ";
        }
        if (form52.getUherUvchilsen() != null && !form52.getUherUvchilsen().toString().isEmpty() && form52.getUherUvchilsen() != 0) {
            query += " uherUvchilsen = ? , ";
        }

        /* MAL TURUL */
        if (form52.getUherUhsen() != null && !form52.getUherUhsen().toString().isEmpty() && form52.getUherUhsen() != 0) {
            query += " uherUhsen = ? , ";
        }
        if (form52.getUherEdgersen() != null && !form52.getUherEdgersen().toString().isEmpty() && form52.getUherEdgersen() != 0) {
            query += " uherEdgersen = ? , ";
        }

        /* Tuluvlugu */
        if (form52.getHoniUvchilsen() != null && !form52.getHoniUvchilsen().toString().isEmpty() && form52.getHoniUvchilsen() != 0) {
            query += " honiUvchilsen = ? , ";
        }
        if (form52.getHoniUhsen() != null && !form52.getHoniUhsen().toString().isEmpty() && form52.getHoniUhsen() != 0) {
            query += " honiUhsen = ? , ";
        }

        /* Guitsetgel */
        if (form52.getHoniEdgersen() != null && !form52.getHoniEdgersen().toString().isEmpty() && form52.getHoniEdgersen() != 0) {
            query += " honiEdgersen = ? , ";
        }
        if (form52.getYamaaUvchilsen() != null && !form52.getYamaaUvchilsen().toString().isEmpty() && form52.getYamaaUvchilsen() != 0) {
            query += " yamaaUvchilsen = ? , ";
        }

        if (form52.getYamaaUhsen() != null && !form52.getYamaaUhsen().toString().isEmpty() && form52.getYamaaUhsen() != 0) {
            query += " yamaaUhsen = ? , ";
        }
        if (form52.getYamaaEdgersen() != null && !form52.getYamaaEdgersen().toString().isEmpty() && form52.getYamaaEdgersen() != 0) {
            query += " yamaaEdgersen = ? , ";
        }



        if (form52.getBusadUvchilsen() != null && !form52.getBusadUvchilsen().toString().isEmpty() && form52.getBusadUvchilsen() != 0) {
            query += " busadUvchilsen = ? , ";
        }

        if (form52.getBusadUhsen() != null && !form52.getBusadUhsen().toString().isEmpty() && form52.getBusadUhsen() != 0) {
            query += " busadUhsen = ? , ";
        }
        if (form52.getBusadEdgersen() != null && !form52.getBusadEdgersen().toString().isEmpty() && form52.getBusadEdgersen() != 0) {
            query += " busadEdgersen = ? , ";
        }

        /* Delete flag */
        if (form52.getDelflg() != null && !form52.getDelflg().toString().isEmpty()) {
            query += " del_flg = ?, ";
        }

        /* Active flag */
        if (form52.getActflg() != null && !form52.getActflg().toString().isEmpty()) {
            query += " actflg = ?, ";
        }

        if (form52 != null && form52.getRecordDate() != null) {
            query += " `recorddate` = ?, ";
        }
        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";
        query += " WHERE  ID   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (form52.getSum() != null && !form52.getSum().isEmpty()) {
                    statement.setString(stIndex++, form52.getSum());
                }

                if (form52.getSum_en() != null && !form52.getSum_en().isEmpty()) {
                    statement.setString(stIndex++, form52.getSum_en());
                }

            if (form52.getUvchinNer() != null && !form52.getUvchinNer().toString().isEmpty()) {
                statement.setString(stIndex++, form52.getUvchinNer());
            }
            if (form52.getUvchinNer_En() != null && !form52.getUvchinNer_En().toString().isEmpty()) {
                statement.setString(stIndex++, form52.getUvchinNer_En());
            }

            /* Zartsuulsan hemjee */
            if (form52.getBugdUvchilsen() != null && !form52.getBugdUvchilsen().toString().isEmpty() && form52.getBugdUvchilsen() != 0) {
                statement.setLong(stIndex++, form52.getBugdUvchilsen());
            }
            if (form52.getBugdUhsen() != null && !form52.getBugdUhsen().toString().isEmpty() && form52.getBugdUhsen() != 0) {
                statement.setLong(stIndex++, form52.getBugdUhsen());
            }

            /* MAL TURUL */
            if (form52.getBugdEdgersen() != null && !form52.getBugdEdgersen().toString().isEmpty() && form52.getBugdEdgersen() != 0) {
                statement.setLong(stIndex++, form52.getBugdEdgersen());
            }
            if (form52.getTemeeUvchilsen() != null && !form52.getTemeeUvchilsen().toString().isEmpty() && form52.getTemeeUvchilsen() != 0) {
                statement.setLong(stIndex++, form52.getTemeeUvchilsen());
            }

            /* Tuluvlugu */
            if (form52.getTemeeUhsen() != null && !form52.getTemeeUhsen().toString().isEmpty() && form52.getTemeeUhsen().longValue() != 0) {
                statement.setLong(stIndex++, form52.getTemeeUhsen());
            }
            if (form52.getTemeeEdgersen() != null && !form52.getTemeeEdgersen().toString().isEmpty() && form52.getTemeeEdgersen().longValue() != 0) {
                statement.setLong(stIndex++, form52.getTemeeEdgersen());
            }

            /* Guitsetgel */
            if (form52.getAduuUvchilsen() != null && !form52.getAduuUvchilsen().toString().isEmpty() && form52.getAduuUvchilsen().longValue() != 0) {
                statement.setLong(stIndex++, form52.getAduuUvchilsen());
            }
            if (form52.getAduuUhsen() != null && !form52.getAduuUhsen().toString().isEmpty() && form52.getAduuUhsen().longValue() != 0) {
                statement.setLong(stIndex++, form52.getAduuUhsen());
            }

            if (form52.getAduuEdgersen() != null && !form52.getAduuEdgersen().toString().isEmpty() && form52.getAduuEdgersen().longValue() != 0) {
                statement.setLong(stIndex++, form52.getAduuEdgersen());
            }
            //////////////////////////////////////////////////
            if (form52.getUherUvchilsen() != null && !form52.getUherUvchilsen().toString().isEmpty() && form52.getUherUvchilsen().longValue() != 0) {
                statement.setLong(stIndex++, form52.getUherUvchilsen());
            }
            if (form52.getUherUhsen() != null && !form52.getUherUhsen().toString().isEmpty() && form52.getUherUhsen().longValue() != 0) {
                statement.setLong(stIndex++, form52.getUherUhsen());
            }

            if (form52.getUherEdgersen() != null && !form52.getUherEdgersen().toString().isEmpty() && form52.getUherEdgersen().longValue() != 0) {
                statement.setLong(stIndex++, form52.getUherEdgersen());
            }
            if (form52.getHoniUvchilsen() != null && !form52.getHoniUvchilsen().toString().isEmpty() && form52.getHoniUvchilsen().longValue() != 0) {
                statement.setLong(stIndex++, form52.getHoniUvchilsen());
            }
            if (form52.getHoniUhsen() != null && !form52.getHoniUhsen().toString().isEmpty() && form52.getHoniUhsen().longValue() != 0) {
                statement.setLong(stIndex++, form52.getHoniUhsen());
            }

            if (form52.getHoniEdgersen() != null && !form52.getHoniEdgersen().toString().isEmpty() && form52.getHoniEdgersen().longValue() != 0) {
                statement.setLong(stIndex++, form52.getHoniEdgersen());
            }
            if (form52.getYamaaUvchilsen() != null && !form52.getYamaaUvchilsen().toString().isEmpty() && form52.getYamaaUvchilsen().longValue() != 0) {
                statement.setLong(stIndex++, form52.getYamaaUvchilsen());
            }
            if (form52.getYamaaUhsen() != null && !form52.getYamaaUhsen().toString().isEmpty() && form52.getYamaaUhsen().longValue() != 0) {
                statement.setLong(stIndex++, form52.getYamaaUhsen());
            }

            if (form52.getYamaaEdgersen() != null && !form52.getYamaaEdgersen().toString().isEmpty() && form52.getYamaaEdgersen().longValue() != 0) {
                statement.setLong(stIndex++, form52.getYamaaEdgersen());
            }
            if (form52.getBusadUvchilsen() != null && !form52.getBusadUvchilsen().toString().isEmpty() && form52.getBusadUvchilsen().longValue() != 0) {
                statement.setLong(stIndex++, form52.getBusadUvchilsen());
            }
            if (form52.getBusadUhsen() != null && !form52.getBusadUhsen().toString().isEmpty() && form52.getBusadUhsen().longValue() != 0) {
                statement.setLong(stIndex++, form52.getBusadUhsen());
            }

            if (form52.getBusadEdgersen() != null && !form52.getBusadEdgersen().toString().isEmpty() && form52.getBusadEdgersen().longValue() != 0) {
                statement.setLong(stIndex++, form52.getBusadEdgersen());
            }
            ////////////////////////////////////////////////////

            /* Delete flag */
                if (form52.getDelflg() != null && !form52.getDelflg().toString().isEmpty()) {
                    statement.setString(stIndex++, form52.getDelflg());
                }

            /* Active flag */
                if (form52.getActflg() != null && !form52.getActflg().toString().isEmpty()) {
                    statement.setString(stIndex++, form52.getActflg());
                }

                if (form52 != null && form52.getRecordDate() != null) {
                    statement.setDate(stIndex++, form52.getRecordDate());
                }

                statement.setDate(stIndex++, form52.getModAt());
                statement.setString(stIndex++, form52.getModby());
                statement.setLong(stIndex++, form52.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                    errObj.setErrText("Бичлэг амжилттай засагдлаа.");
                    errObj.setErrDesc("Бичлэг амжилттай засагдлаа.");
                    return errObj;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1061));
        errObj.setErrText("Амжилтгүй боллоо. Дахин оролдоно уу.");
        errObj.setErrDesc("Амжилтгүй боллоо. Дахин оролдоно уу.");

        return errObj;
    }

    @Override
    public List<Form52Entity> getPieChartData(Form52Entity form52Entity) throws SQLException {
        int stIndex = 1;
        List<Form52Entity> form52EntitieList = null;

        String query_sel = "SELECT `hform52`.`id`,  " +
                "    `hform52`.`aimag`,  " +
                "    `hform52`.`sum`,  " +
                "    SUM(`hform52`.`zartsuulsan_hemjee_mn`) as zartsuulsan_hemjee,  " +
                "    c.cityname, c.cityname_en,  " +
                "    `h_sum`.sumname, `h_sum`.sumname_en  " +
                "FROM `hospital`.`hform52`  " +
                "INNER JOIN `h_city` AS c  " +
                "ON c.code = `hform52`.aimag  " +
                "INNER JOIN `h_sum`  " +
                "ON `h_sum`.sumcode = `hform52`.sum  " +
                "WHERE `hform52`.`del_flg` = 'N' and `hform52`.`actflg` = 'Y' and `hform52`.`aimag` = ?  " +
                "GROUP BY `hform52`.`sum` ";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                if (form52Entity.getAimag() != null && !form52Entity.getAimag().isEmpty()) {
                    statement.setString(stIndex++, form52Entity.getAimag());
                }

                ResultSet resultSet = statement.executeQuery();

                form52EntitieList = new ArrayList<>();

                while (resultSet.next()) {
//                    Form52Entity entity = new Form52Entity();
//
//                    entity.setId(resultSet.getLong("id"));
//                    entity.setZartsuulsanHemjee(resultSet.getString("zartsuulsan_hemjee"));
//                    entity.setZartsuulsanHemjee_en(resultSet.getString("zartsuulsan_hemjee"));
//                    entity.setAimag(resultSet.getString("aimag"));
//                    entity.setSum(resultSet.getString("sum"));
//                    entity.setAimagNer(resultSet.getString("cityname"));
//                    entity.setSumNer(resultSet.getString("sumname"));
//
//                    form52EntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return form52EntitieList;
    }//


    @Override
    public ErrorEntity exportReport(String file, Form52Entity form52) throws SQLException {
        ErrorEntity errorEntity = null;
        String fileName = "hform52";
        try (Connection connection = boneCP.getConnection()) {
            String JRXML_PATH = Config.getJrxmlPath();
            String REPORT_PATH = Config.getReportPath();

            try {
                Map parameters = new HashMap();

                String reportTitle = "МАЛ АМЬТДЫН ХАЛДВАРГҮЙ ӨВЧНИЙ $$YEAR$$ ОНЫ ХАГАС ЖИЛИЙН, ЖИЛИЙН ЭЦСИЙН МЭДЭЭ",
                        currentYear = "$$YEAR$$ он №....",
                        printDate = "$$YEAR$$ оны $$MONTH$$ сарын $$DAY$$";

                Calendar now = Calendar.getInstance();

                int year = now.get(Calendar.YEAR);
                int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
                int day = now.get(Calendar.DAY_OF_MONTH);

                printDate = printDate.replace("$$YEAR$$", String.valueOf(year));
                printDate = printDate.replace("$$MONTH$$", String.valueOf(month));
                printDate = printDate.replace("$$DAY$$", String.valueOf(day));

                currentYear = currentYear.replace("$$YEAR$$", String.valueOf(year));

                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(new Date(form52.getRecordDate().getTime()));

                reportTitle = reportTitle.replace("$$YEAR$$", String.valueOf(calendar1.get(Calendar.YEAR)));

                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(new Date(form52.getSearchRecordDate().getTime()));

                if (calendar2.get(Calendar.MONTH) < 4) {
                    reportTitle = reportTitle.replace("$$SEASON$$", "1");
                } else if (calendar2.get(Calendar.MONTH) < 7) {
                    reportTitle = reportTitle.replace("$$SEASON$$", "2");
                } else if (calendar2.get(Calendar.MONTH) < 10) {
                    reportTitle = reportTitle.replace("$$SEASON$$", "3");
                } else if (calendar2.get(Calendar.MONTH) <= 12) {
                    reportTitle = reportTitle.replace("$$SEASON$$", "4");
                }

                parameters.put("start_date", form52.getRecordDate());
                parameters.put("end_date", form52.getSearchRecordDate());
                parameters.put("citycode", (form52.getAimag() != null && !form52.getAimag().isEmpty()) ? form52.getAimag() : null);
                parameters.put("sumcode", (form52.getSum() != null && !form52.getSum().isEmpty()) ? form52.getSum() : null);
                parameters.put("currentyear", currentYear);
                CityServiceImpl cityService = new CityServiceImpl(boneCP);
                City tmpCity = new City();
                tmpCity.setCode(form52.getAimag());
                List<City> clist = cityService.selectAll(tmpCity);

                if (clist != null && clist.size() == 1) {
                    parameters.put("aimagner", clist.get(0).getCityname());
                } else {
                    parameters.put("aimagner", (form52.getAimagNer() == null) ? "" : form52.getAimagNer());
                }
                ISumService sumService = new SumServiceImpl(boneCP);
                Sum tmpSum = new Sum();
                tmpSum.setCitycode(form52.getAimag());
                tmpSum.setSumcode(form52.getSum());
                List<Sum> sumList = sumService.selectAll(tmpSum);

                if (sumList != null && sumList.size() == 1){
                    parameters.put("sumner", sumList.get(0).getSumname());
                }
                else{
                    parameters.put("sumner", (form52.getSumNer() == null) ? "" : form52.getSumNer());
                }

                parameters.put("printdate", printDate);
                parameters.put("reporttitle", reportTitle);
                parameters.put("creby", (form52.getCreby() != null && !form52.getCreby().isEmpty()) ? form52.getCreby() : null);

                PdfExcelUtil.generateReport(file, JRXML_PATH, REPORT_PATH, fileName, parameters, connection);


                System.out.println("File Generated");
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (connection != null) connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1001));
            if(file.equals("pdf")){
                errorEntity.setErrText(fileName + ".pdf");
            }else if(file.equals("xls")){
                errorEntity.setErrText(fileName + ".xls");
            }
        }
        return errorEntity;
    }

    /**
     * Get name
     *
     * @param cmbType
     * @param mal_turul_mn
     * @return
     */
    private String getMalTurulNer(List<BasicDBObject> cmbType, String mal_turul_mn) {
        for (BasicDBObject obj :
                cmbType) {
            if (obj.get("id") != null && mal_turul_mn != null && obj.getString("id").equalsIgnoreCase(mal_turul_mn)) {
                return obj.getString("name");
            }
        }

        return "";
    }
    @Override
    public BasicDBObject getColHighChartData(Form52Entity entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();
        List<String> categories = new ArrayList<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        //dateFormat.parse(dv).getTime()
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }

        try {
            IComboService comboService = new ComboServiceImpl(boneCP);

            List<BasicDBObject> cmbType = new ArrayList<>();
            if (entity != null && !entity.getMalNer().isEmpty()) {
                cmbType = comboService.getComboNames("TYPEFORM52MAL", "MN");
            } else {
                cmbType = comboService.getComboNames("VALFORM52MAL", "MN");
            }

            //Saraar n haruulah
            int month = calendar1.get(Calendar.MONTH) + 1;
            int year1 = calendar1.get(Calendar.YEAR);
            int year2 = calendar2.get(Calendar.YEAR);

            try (Connection connection = boneCP.getConnection()) {
                String query_sel = "";
                if (entity != null && !entity.getMalNer().isEmpty()) {
                    if (entity.getMalNer().equals("aduu")) {
                        query_sel = "SELECT uvchinNer,  sum(`aduuUvchilsen`) AS uvchilsen,  sum(`aduuUhsen`) AS uhsen," +
                                "sum(`aduuEdgersen`) AS edgersen, aimag, sum FROM hospital.hform52  WHERE";
                    } else if (entity.getMalNer().equals("temee")) {
                        query_sel = "SELECT uvchinNer,  sum(`temeeUvchilsen`) AS uvchilsen,  sum(`temeeUhsen`) AS uhsen," +
                                "sum(`temeeEdgersen`) AS edgersen, aimag, sum FROM hospital.hform52  WHERE";
                    } else if (entity.getMalNer().equals("uher")) {
                        query_sel = "SELECT uvchinNer,  sum(`uherUvchilsen`) AS uvchilsen,  sum(`uherUhsen`) AS uhsen," +
                                "sum(`uherEdgersen`) AS edgersen, aimag, sum FROM hospital.hform52  WHERE";
                    } else if (entity.getMalNer().equals("honi")) {
                        query_sel = "SELECT uvchinNer,  sum(`honiUvchilsen`) AS uvchilsen,  sum(`honiUhsen`) AS uhsen," +
                                "sum(`honiEdgersen`) AS edgersen, aimag, sum FROM hospital.hform52  WHERE";
                    } else if (entity.getMalNer().equals("yamaa")) {
                        query_sel = "SELECT uvchinNer,  sum(`yamaaUvchilsen`) AS uvchilsen,  sum(`yamaaUhsen`) AS uhsen," +
                                "sum(`yamaaEdgersen`) AS edgersen, aimag, sum FROM hospital.hform52  WHERE";
                    } else if (entity.getMalNer().equals("busad")) {
                        query_sel = "SELECT uvchinNer,  sum(`busadUvchilsen`) AS uvchilsen,  sum(`busadUhsen`) AS uhsen," +
                                "sum(`busadEdgersen`) AS edgersen, aimag, sum FROM hospital.hform52  WHERE";
                    }
                }else{
                    query_sel = "SELECT `uvchinNer`, sum(`temeeUvchilsen`) AS temeeUvchilsen,  sum(`temeeUhsen`) AS temeeUhsen," +
                            "sum(`temeeEdgersen`) AS temeeEdgersen, " +
                            "sum(`aduuUvchilsen`) AS aduuUvchilsen,  sum(`aduuUhsen`) AS aduuUhsen," +
                            "sum(`aduuEdgersen`) AS aduuEdgersen," +
                            "sum(`uherUvchilsen`) AS uherUvchilsen,  sum(`uherUhsen`) AS uherUhsen," +
                            "sum(`uherEdgersen`) AS uherEdgersen," +
                            "sum(`honiUvchilsen`) AS honiUvchilsen,  sum(`honiEdgersen`) AS honiEdgersen," +
                            "sum(`honiUhsen`) AS honiUhsen," +
                            "sum(`yamaaUvchilsen`) AS yamaaUvchilsen,  sum(`yamaaUhsen`) AS yamaaUhsen," +
                            "sum(`yamaaEdgersen`) AS yamaaEdgersen," +
                            "sum(`busadUvchilsen`) AS busadUvchilsen,  sum(`busadUhsen`) AS busadUhsen," +
                            "sum(`busadEdgersen`) AS busadEdgersen,aimag, sum FROM hospital.hform52  WHERE";
                }

                if (entity != null && entity.getAimag()!= null && !entity.getAimag().isEmpty()) {
                    query_sel += " `hform52`.`aimag` = ? AND ";
                }
                if (entity != null && !entity.getSum().isEmpty() && entity.getSum() != null ) {
                    query_sel += " `hform52`.`sum` = ? AND ";
                }
                if (entity != null && !entity.getDelflg().isEmpty()) {
                    query_sel += " `hform52`.`del_flg` = ? AND ";
                }
                if (entity != null && !entity.getUvchinNer().isEmpty() && entity.getUvchinNer() != null ) {
                    query_sel += " `hform52`.`uvchinNer` = ? AND ";
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                    query_sel += " (`recorddate` BETWEEN ? AND ?)";
                }

                do {
                    //do process
                    categories.add(year1 + "-" + month);

                    try {
                        PreparedStatement statement = connection.prepareStatement(query_sel);

                        java.sql.Date stdate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-01 00:00:00").getTime());

                        java.sql.Date enddate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-31 00:00:00").getTime());

                        if (month == 12 && year1 < year2) {
                            month = 1;
                            year1++;
                        } else {
                            month++;
                        }

                        int stindex = 1;

                        if (entity != null && entity.getAimag()!= null && !entity.getAimag().isEmpty()) {
                            statement.setString(stindex++, entity.getAimag());
                        }
                        if (entity != null && !entity.getDelflg().isEmpty()) {
                            statement.setString(stindex++, entity.getDelflg());
                        }
                        if (entity != null && !entity.getUvchinNer().isEmpty() && entity.getUvchinNer() != null ) {
                            statement.setString(stindex++, entity.getUvchinNer());
                        }
                        if (entity != null && !entity.getSum().isEmpty() && entity.getSum() != null ) {
                            statement.setString(stindex++, entity.getSum());
                        }
                        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                            statement.setDate(stindex++, stdate);
                            statement.setDate(stindex++, enddate);
                        }

                        ResultSet resultSet = statement.executeQuery();

                        HashMap<String, String> hashMap = new HashMap<>();

                        if (entity != null && !entity.getMalNer().isEmpty()) {
                            while (resultSet.next()) {
                                String uhsen = resultSet.getString("uhsen");
                                String uvchilsen = resultSet.getString("uvchilsen");
                                String edgersen = resultSet.getString("edgersen");
                                hashMap.put("uhsen", uhsen);
                                hashMap.put("uvchilsen", uvchilsen);
                                hashMap.put("edgersen", edgersen);

                            }//end while
                        } else {
                            while (resultSet.next()) {
                                String temeeUvchilsen = resultSet.getString("temeeUvchilsen");
                                String temeeUhsen = resultSet.getString("temeeUhsen");
                                String temeeEdgersen = resultSet.getString("temeeEdgersen");
                                String aduuUvchilsen = resultSet.getString("aduuUvchilsen");
                                String aduuUhsen = resultSet.getString("aduuUhsen");
                                String aduuEdgersen = resultSet.getString("aduuEdgersen");
                                String uherUvchilsen = resultSet.getString("uherUvchilsen");
                                String uherUhsen = resultSet.getString("uherUhsen");
                                String uherEdgersen = resultSet.getString("uherEdgersen");
                                String honiUvchilsen = resultSet.getString("honiUvchilsen");
                                String honiUhsen = resultSet.getString("honiUhsen");
                                String honiEdgersen = resultSet.getString("honiEdgersen");
                                String yamaaUvchilsen = resultSet.getString("yamaaUvchilsen");
                                String yamaaUhsen = resultSet.getString("yamaaUhsen");
                                String yamaaEdgersen = resultSet.getString("yamaaEdgersen");
                                String busadUvchilsen = resultSet.getString("busadUvchilsen");
                                String busadUhsen = resultSet.getString("busadUhsen");
                                String busadEdgersen = resultSet.getString("busadEdgersen");

                                hashMap.put("temeeUvchilsen", temeeUvchilsen);
                                hashMap.put("temeeUhsen", temeeUhsen);
                                hashMap.put("temeeEdgersen", temeeEdgersen);
                                hashMap.put("aduuUvchilsen", aduuUvchilsen);
                                hashMap.put("aduuUhsen", aduuUhsen);
                                hashMap.put("aduuEdgersen", aduuEdgersen);
                                hashMap.put("uherUvchilsen", uherUvchilsen);
                                hashMap.put("uherUhsen", uherUhsen);
                                hashMap.put("uherEdgersen", uherEdgersen);
                                hashMap.put("honiUvchilsen", honiUvchilsen);
                                hashMap.put("honiUhsen", honiUhsen);
                                hashMap.put("honiEdgersen", honiEdgersen);
                                hashMap.put("yamaaUvchilsen", yamaaUvchilsen);
                                hashMap.put("yamaaUhsen", yamaaUhsen);
                                hashMap.put("yamaaEdgersen", yamaaEdgersen);
                                hashMap.put("busadUvchilsen", busadUvchilsen);
                                hashMap.put("busadUhsen", busadUhsen);
                                hashMap.put("busadEdgersen", busadEdgersen);

                            }//end while
                        }
                        List<Double> list = null;

                        for (int i = 0; i < cmbType.size(); i++) {
                            BasicDBObject obj = cmbType.get(i);

                            if (obj.get("data") == null){
                                //new record
                                list = new ArrayList<>();
                                String val = hashMap.get(obj.getString("id"));
                                if (val == null) list.add(0.0);
                                else if (val.isEmpty()) list.add(0.0);
                                else list.add(Double.parseDouble(val));
                                obj.put("data", list);
                            }else{
                                //already exist
                                list = (List<Double>) obj.get("data");
                                String val = hashMap.get(obj.getString("id"));
                                if (val == null) list.add(0.0);
                                else if (val.isEmpty()) list.add(0.0);
                                else list.add(Double.parseDouble(val));
                            }

                        }

                        if (statement != null) statement.close();
                        if (resultSet != null) resultSet.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();

                        break;
                    }

                    ///
                } while ((month <= calendar2.get(Calendar.MONTH) + 1 && year1 <= year2) || year1 < year2);

                basicDBObject.put("categories", categories);
                basicDBObject.put("series", cmbType);

                connection.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }

    @Override
    public BasicDBObject getPieHighChartData(Form52Entity entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();

        String query_sel = "";

        if (entity != null && !entity.getMalNer().isEmpty()) {
            if (entity.getMalNer().equals("aduu")) {
                query_sel = "SELECT uvchinNer,  sum(`aduuUvchilsen`) AS uvchilsen,  sum(`aduuUhsen`) AS uhsen," +
                        "sum(`aduuEdgersen`) AS edgersen, aimag, sum FROM hospital.hform52  WHERE";
            } else if (entity.getMalNer().equals("temee")) {
                query_sel = "SELECT uvchinNer,  sum(`temeeUvchilsen`) AS uvchilsen,  sum(`temeeUhsen`) AS uhsen," +
                        "sum(`temeeEdgersen`) AS edgersen, aimag, sum FROM hospital.hform52  WHERE";
            } else if (entity.getMalNer().equals("uher")) {
                query_sel = "SELECT uvchinNer,  sum(`uherUvchilsen`) AS uvchilsen,  sum(`uherUhsen`) AS uhsen," +
                        "sum(`uherEdgersen`) AS edgersen, aimag, sum FROM hospital.hform52  WHERE";
            } else if (entity.getMalNer().equals("honi")) {
                query_sel = "SELECT uvchinNer,  sum(`honiUvchilsen`) AS uvchilsen,  sum(`honiUhsen`) AS uhsen," +
                        "sum(`honiEdgersen`) AS edgersen, aimag, sum FROM hospital.hform52  WHERE";
            } else if (entity.getMalNer().equals("yamaa")) {
                query_sel = "SELECT uvchinNer,  sum(`yamaaUvchilsen`) AS uvchilsen,  sum(`yamaaUhsen`) AS uhsen," +
                        "sum(`yamaaEdgersen`) AS edgersen, aimag, sum FROM hospital.hform52  WHERE";
            } else if (entity.getMalNer().equals("busad")) {
                query_sel = "SELECT uvchinNer,  sum(`busadUvchilsen`) AS uvchilsen,  sum(`busadUhsen`) AS uhsen," +
                        "sum(`busadEdgersen`) AS edgersen, aimag, sum FROM hospital.hform52  WHERE";
            }
        }else{
            query_sel = "SELECT `uvchinNer`, sum(`temeeUvchilsen`) AS temeeUvchilsen,  sum(`temeeUhsen`) AS temeeUhsen," +
                    "sum(`temeeEdgersen`) AS temeeEdgersen, " +
                    "sum(`aduuUvchilsen`) AS aduuUvchilsen,  sum(`aduuUhsen`) AS aduuUhsen," +
                    "sum(`aduuEdgersen`) AS aduuEdgersen," +
                    "sum(`uherUvchilsen`) AS uherUvchilsen,  sum(`uherUhsen`) AS uherUhsen," +
                    "sum(`uherEdgersen`) AS uherEdgersen," +
                    "sum(`honiUvchilsen`) AS honiUvchilsen,  sum(`honiEdgersen`) AS honiEdgersen," +
                    "sum(`honiUhsen`) AS honiUhsen," +
                    "sum(`yamaaUvchilsen`) AS yamaaUvchilsen,  sum(`yamaaUhsen`) AS yamaaUhsen," +
                    "sum(`yamaaEdgersen`) AS yamaaEdgersen," +
                    "sum(`busadUvchilsen`) AS busadUvchilsen,  sum(`busadUhsen`) AS busadUhsen," +
                    "sum(`busadEdgersen`) AS busadEdgersen,aimag, sum FROM hospital.hform52  WHERE";
        }

        if (entity != null && entity.getAimag()!= null && !entity.getAimag().isEmpty()) {
            query_sel += " `hform52`.`aimag` = ? AND ";
        }
        if (entity != null && !entity.getSum().isEmpty() && entity.getSum() != null ) {
            query_sel += " `hform52`.`sum` = ? AND ";
        }
        if (entity != null && !entity.getDelflg().isEmpty()) {
            query_sel += " `hform52`.`del_flg` = ? AND ";
        }
        if (entity != null && !entity.getUvchinNer().isEmpty() && entity.getUvchinNer() != null ) {
            query_sel += " `hform52`.`uvchinNer` = ? AND ";
        }
        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
            query_sel += " (`recorddate` BETWEEN ? AND ?)";
        }

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                int stindex = 1;

                if (entity != null && entity.getAimag()!= null && !entity.getAimag().isEmpty()) {
                    statement.setString(stindex++, entity.getAimag());
                }
                if (entity != null && !entity.getDelflg().isEmpty()) {
                    statement.setString(stindex++, entity.getDelflg());
                }
                if (entity != null && !entity.getUvchinNer().isEmpty() && entity.getUvchinNer() != null ) {
                    statement.setString(stindex++, entity.getUvchinNer());
                }
                if (entity != null && !entity.getSum().isEmpty() && entity.getSum() != null ) {
                    statement.setString(stindex++, entity.getSum());
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                    statement.setDate(stindex++, entity.getRecordDate());
                    statement.setDate(stindex++, entity.getSearchRecordDate());
                }

                System.out.println(statement);
                ResultSet resultSet = statement.executeQuery();

                IComboService comboService = new ComboServiceImpl(boneCP);
                List<BasicDBObject> list = new ArrayList<>();

                while (resultSet.next()) {

                    if (entity != null && !entity.getMalNer().isEmpty()) {
                        BasicDBObject entity1 = new BasicDBObject();
                        BasicDBObject entity2 = new BasicDBObject();
                        BasicDBObject entity3 = new BasicDBObject();

                        entity1.put("name", "Өвчилсэн");
                        entity1.put("y", resultSet.getDouble("uvchilsen"));
                        entity2.put("name", "Үхсэн");
                        entity2.put("y", resultSet.getDouble("uhsen"));
                        entity3.put("name", "Эдгэрсэн");
                        entity3.put("y", resultSet.getDouble("edgersen"));
                        list.add(entity1);
                        list.add(entity2);
                        list.add(entity3);
                    }else {
                        BasicDBObject entity1 = new BasicDBObject();
                        BasicDBObject entity2 = new BasicDBObject();
                        BasicDBObject entity3 = new BasicDBObject();
                        BasicDBObject entity4 = new BasicDBObject();
                        BasicDBObject entity5 = new BasicDBObject();
                        BasicDBObject entity6 = new BasicDBObject();
                        BasicDBObject entity7 = new BasicDBObject();
                        BasicDBObject entity8 = new BasicDBObject();
                        BasicDBObject entity9 = new BasicDBObject();
                        BasicDBObject entity10 = new BasicDBObject();
                        BasicDBObject entity11 = new BasicDBObject();
                        BasicDBObject entity12 = new BasicDBObject();
                        BasicDBObject entity13 = new BasicDBObject();
                        BasicDBObject entity14 = new BasicDBObject();
                        BasicDBObject entity15 = new BasicDBObject();
                        BasicDBObject entity16 = new BasicDBObject();
                        BasicDBObject entity17 = new BasicDBObject();
                        BasicDBObject entity18 = new BasicDBObject();

                        entity1.put("name", "Өвчилсэн тэмээ");
                        entity1.put("y", resultSet.getDouble("temeeUvchilsen"));
                        entity2.put("name", "Үхсэн тэмээ");
                        entity2.put("y", resultSet.getDouble("temeeUhsen"));
                        entity3.put("name", "Эдгэрсэн тэмээ");
                        entity3.put("y", resultSet.getDouble("temeeEdgersen"));
                        entity4.put("name", "Өвчилсөн адуу");
                        entity4.put("y", resultSet.getDouble("aduuUvchilsen"));
                        entity5.put("name", "Үхсэн адуу");
                        entity5.put("y", resultSet.getDouble("aduuUhsen"));
                        entity6.put("name", "Эдгэрсэн адуу");
                        entity6.put("y", resultSet.getDouble("aduuEdgersen"));
                        entity7.put("name", "Өвчилсэн үхэр");
                        entity7.put("y", resultSet.getDouble("uherUvchilsen"));
                        entity8.put("name", "Үхсэн үхэр");
                        entity8.put("y", resultSet.getDouble("uherUhsen"));
                        entity9.put("name", "Эдгэрсэн үхэр");
                        entity9.put("y", resultSet.getDouble("uherEdgersen"));
                        entity10.put("name", "Өвчилсөн хонь");
                        entity10.put("y", resultSet.getDouble("honiUvchilsen"));
                        entity11.put("name", "Үхсэн хонь");
                        entity11.put("y", resultSet.getDouble("honiUhsen"));
                        entity12.put("name", "Эдгэрсэн хонь");
                        entity12.put("y", resultSet.getDouble("honiEdgersen"));
                        entity13.put("name", "Өвчилсөн ямаа");
                        entity13.put("y", resultSet.getDouble("yamaaUvchilsen"));
                        entity14.put("name", "Үхсэн ямаа");
                        entity14.put("y", resultSet.getDouble("yamaaUhsen"));
                        entity15.put("name", "Эдгэрсэн ямаа");
                        entity15.put("y", resultSet.getDouble("yamaaEdgersen"));
                        entity16.put("name", "Бусад өвчилсөн");
                        entity16.put("y", resultSet.getDouble("busadUvchilsen"));
                        entity17.put("name", "Бусад үхсэн");
                        entity17.put("y", resultSet.getDouble("busadUhsen"));
                        entity18.put("name", "Бусад эдгэрсэн");
                        entity18.put("y", resultSet.getDouble("busadEdgersen"));
                        list.add(entity1);
                        list.add(entity2);
                        list.add(entity3);
                        list.add(entity4);
                        list.add(entity5);
                        list.add(entity6);
                        list.add(entity7);
                        list.add(entity8);
                        list.add(entity9);
                        list.add(entity10);
                        list.add(entity11);
                        list.add(entity12);
                        list.add(entity13);
                        list.add(entity14);
                        list.add(entity15);
                        list.add(entity16);
                        list.add(entity17);
                        list.add(entity18);
                    }

                }

                basicDBObject.put("data", list);

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }

}
