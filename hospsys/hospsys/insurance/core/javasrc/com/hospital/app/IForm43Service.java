package com.hospital.app;

import com.model.hos.ErrorEntity;
import com.model.hos.Form43Entity;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public interface IForm43Service {

    /**
     * Insert new data
     *
     *
     * @param form43
     * @return
     * @throws Exception
     */
    ErrorEntity insertNewData(Form43Entity form43) throws SQLException;

    /**
     * Select all
     *
     * @param form43
     * @return
     * @throws SQLException
     */
    List<Form43Entity> getListData(Form43Entity form43) throws SQLException;


    /**
     * update form data
     *
     * @param form43
     * @return
     * @throws SQLException
     */
    ErrorEntity updateData(Form43Entity form43) throws SQLException;


    List<Form43Entity> getPieChartData(Form43Entity form43Entity) throws SQLException;


    ErrorEntity exportReport(Form43Entity form43) throws SQLException;
}
