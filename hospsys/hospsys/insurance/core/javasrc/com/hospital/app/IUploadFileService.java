package com.hospital.app;

import com.model.hos.ErrorEntity;
import com.model.hos.FileEntity;
import com.mongodb.BasicDBObject;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public interface IUploadFileService {

    /**
     * Insert new data
     *
     *
     * @param entity
     * @return
     * @throws Exception
     */
    ErrorEntity insertNewData(FileEntity entity) throws SQLException;

    /**
     * Select all
     *
     * @param entity
     * @return
     * @throws SQLException
     */
    List<FileEntity> getListData(FileEntity entity) throws SQLException;


    /**
     * update form data
     *
     * @param entity
     * @return
     * @throws SQLException
     */
    ErrorEntity updateData(FileEntity entity) throws SQLException;

    Integer getFileCount()throws SQLException;
}
