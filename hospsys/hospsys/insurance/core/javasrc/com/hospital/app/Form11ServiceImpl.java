package com.hospital.app;

import com.Config;
import com.enumclass.ErrorType;
import com.jolbox.bonecp.BoneCP;
import com.model.hos.City;
import com.model.hos.ErrorEntity;
import com.model.hos.Form11Entity;
import com.mongodb.BasicDBObject;
import com.rpc.PdfExcelUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Form11ServiceImpl implements IForm11Service{
    /**
     * Objects
     */

    private BoneCP boneCP;


    public Form11ServiceImpl (BoneCP boneCP) {
        this.boneCP = boneCP;
    }

    @Override
    public ErrorEntity insertNewData(Form11Entity form11) throws SQLException {
        PreparedStatement statement = null;

        if (form11 == null) new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));

        String query_login = "INSERT INTO `hospital`.`hform11` " +
                "(`deejiinTurul_mn`, " +
                "`deejiinTurul_en`, " +
                "`malTurul_mn`, " +
                "`malTurul_en`, " +
                "`tooTolgoi_mn`, " +
                "`tooTolgoi_en`, " +
                "`haanaShinjilsen_mn`, " +
                "`haanaShinjilsen_en`, " +
                "`shinjilsenArga_mn`, " +
                "`shinjilsenArga_en`, " +
                "`eruul_mn`, " +
                "`eruul_en`, " +
                "`uvchtei_mn`, " +
                "`uvchtei_en`, " +
                "`onosh_mn`, " +
                "`onosh_en`, " +
                "`delflg`, " +
                "`actflg`, " +
                "`mod_at`, " +
                "`mod_by`, " +
                "`cre_at`, " +
                "`cre_by`," +
                "`aimag`," +
                "`sumBagNer_mn`," +
                "`aimag_en`," +
                "`sumBagNer_en`," +
                "`sum`," +
                "`sum_en`, " +
                "`recorddate` )" +
                "VALUES " +
                "(? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?,? )";

        try (Connection connection = boneCP.getConnection()) {
            try{
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, form11.getDeejiinTurul());
                statement.setString(2, form11.getDeejiinTurul_en());
                statement.setString(3, form11.getMalTurul());
                statement.setString(4, form11.getMalTurul_en());
                statement.setString(5, String.valueOf(form11.getTooTolgoi()));
                statement.setString(6, String.valueOf(form11.getTooTolgoi_en()));
                statement.setString(7, form11.getHaanaShinjilsen());
                statement.setString(8, form11.getHaanaShinjilsen_en());
                statement.setString(9, form11.getShinjilsenArga());
                statement.setString(10, form11.getShinjilsenArga_en());
                System.out.println(form11.getEruul());
                statement.setString(11, String.valueOf(form11.getEruul()));
                statement.setString(12, String.valueOf(form11.getEruul_en()));
                statement.setString(13, String.valueOf(form11.getUvchtei()));
                statement.setString(14, String.valueOf(form11.getUvchtei_en()));
                statement.setString(15, String.valueOf(form11.getOnosh()));
                statement.setString(16, String.valueOf(form11.getOnosh_en()));
                statement.setString(17, form11.getDelflg());
                statement.setString(18, form11.getActflg());
                statement.setDate(19, form11.getModAt());
                statement.setString(20, form11.getModby());
                statement.setDate(21, form11.getCreat());
                statement.setString(22, form11.getCreby());
                statement.setString(23, form11.getAimag());
                statement.setString(24, form11.getBagCode());
                statement.setString(25, form11.getAimag_en());
                statement.setString(26, form11.getBagCode());
                statement.setString(27, form11.getSum());
                statement.setString(28, form11.getSum_en());
                statement.setDate(29, form11.getRecordDate());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1){
                    connection.commit();
                    return new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }finally {
                if (statement != null)statement.close();
                connection.close();
            }

        }catch (Exception ex){

        }

        return new ErrorEntity(ErrorType.ERROR, Long.valueOf(1057));
    }

    @Override
    public List<Form11Entity> getListData(Form11Entity form11) throws SQLException {
        int stIndex = 1;
        List<Form11Entity> form11EntitieList = null;

        String query = "SELECT `hform11`.`id`, " +
                "    `hform11`.`deejiinTurul_mn`, " +
                "    `hform11`.`deejiinTurul_en`, " +
                "    `hform11`.`sumBagNer_mn`, " +
                "    `hform11`.`sumBagNer_en`, " +
                "    `hform11`.`malTurul_mn`, " +
                "    `hform11`.`malTurul_en`, " +
                "    `hform11`.`tooTolgoi_mn`, " +
                "    `hform11`.`tooTolgoi_en`, " +
                "    `hform11`.`haanaShinjilsen_mn`, " +
                "    `hform11`.`haanaShinjilsen_en`, " +
                "    `hform11`.`shinjilsenArga_mn`, " +
                "    `hform11`.`shinjilsenArga_en`, " +
                "    `hform11`.`eruul_mn`, " +
                "    `hform11`.`eruul_en`, " +
                "    `hform11`.`uvchtei_mn`, " +
                "    `hform11`.`uvchtei_en`, " +
                "    `hform11`.`onosh_mn`, " +
                "    `hform11`.`onosh_en`, " +
                "    `hform11`.`delflg`, " +
                "    `hform11`.`actflg`, " +
                "    `hform11`.`mod_at`, " +
                "    `hform11`.`mod_by`, " +
                "    `hform11`.`cre_at`, " +
                "    `hform11`.`cre_by`, " +
                "    `hform11`.`aimag`, " +
                "    `hform11`.`aimag_en`, " +
                "    `hform11`.`sum`, " +
                "    `hform11`.`sum_en`, " +
                "    `hform11`.`recorddate`, " +
                "    c.cityname, c.cityname_en, " +
                "    `h_sum`.sumname, `h_sum`.sumname_en, " +
                "    `h_baghoroo`.horooname, " +
                "    `h_sum`.latitude as sum_latitude, `h_sum`.longitude as sum_longitude  " +
                "FROM `hospital`.`hform11` INNER JOIN `h_city` AS c " +
                "ON c.code = `hform11`.aimag " +
                "INNER JOIN `h_sum` " +
                "ON `h_sum`.sumcode = `hform11`.sum " +
                "INNER JOIN `h_baghoroo` " +
                "ON `h_baghoroo`.horoocode = `hform11`.`sumBagNer_mn` AND `h_baghoroo`.sumcode = `hform11`.sum " +
                "WHERE ";

        if (form11.getDeejiinTurul() != null && !form11.getDeejiinTurul().isEmpty()){
            query += " deejiinTurul_mn = ? AND ";
        }
        if (form11.getDeejiinTurul_en() != null && !form11.getDeejiinTurul_en().isEmpty()){
            query += " deejiinTurul_en = ? AND ";
        }

        if (form11.getMalTurul() != null && !form11.getMalTurul().isEmpty()){
            query += " malTurul_mn = ? AND ";
        }
        if (form11.getMalTurul_en() != null && !form11.getMalTurul_en().toString().isEmpty()){
            query += " malTurul_en = ? AND ";
        }

        if (form11.getTooTolgoi() != null && !form11.getTooTolgoi().toString().isEmpty() && form11.getTooTolgoi() !=0){
            query += " tooTolgoi_mn = ? AND ";
        }

        if (form11.getHaanaShinjilsen() != null && !form11.getHaanaShinjilsen().toString().isEmpty()){
            query += " haanaShinjilsen_mn = ? AND ";
        }
        if (form11.getHaanaShinjilsen_en() != null && !form11.getHaanaShinjilsen_en().toString().isEmpty()){
            query += " haanaShinjilsen_en = ? AND ";
        }

        if (form11.getShinjilsenArga() != null && !form11.getShinjilsenArga().isEmpty()){
            query += " shinjilsenArga_mn = ? AND ";
        }
        if (form11.getShinjilsenArga_en() != null && !form11.getShinjilsenArga_en().toString().isEmpty()){
            query += " shinjilsenArga_en = ? AND ";
        }

        if (form11.getEruul() != null && !form11.getEruul().toString().isEmpty() && form11.getEruul() !=0){
            query += " eruul_mn = ? AND ";
        }

        if (form11.getUvchtei() != null && !form11.getUvchtei().toString().isEmpty() && form11.getUvchtei() != 0){
            query += " uvchtei_mn = ? AND  ";
        }

        if (form11.getOnosh() != null && !form11.getOnosh().toString().isEmpty() && form11.getOnosh() !=0){
            query += " onosh_mn = ? AND  ";
        }

        if (form11 != null && form11.getAimag() != null && !form11.getAimag().isEmpty()){
            query += " aimag = ? AND  ";
        }
        if (form11 != null && form11.getSum() != null && !form11.getSum().isEmpty()){
            query += " sum = ? AND  ";
        }
        if (form11 != null && form11.getBagCode() != null && !form11.getBagCode().isEmpty()){
            query += " sumBagNer_mn = ? AND  ";
        }

        if (form11 != null && form11.getRecordDate() != null && form11.getSearchRecordDate() != null){
            query += " (`recorddate` BETWEEN ? AND ?) AND ";
        }

        if (form11 != null && form11.getCreby() != null && !form11.getCreby().isEmpty()){
            query += " `hform11`.`cre_by` = ? AND ";
        }
        query += " `hform11`.`delflg` = 'N' ORDER BY cre_at DESC";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query);

                if (form11.getDeejiinTurul() != null && !form11.getDeejiinTurul().isEmpty()){
                    statement.setString(stIndex++, form11.getDeejiinTurul());
                }
                if (form11.getDeejiinTurul_en() != null && !form11.getDeejiinTurul_en().isEmpty()){
                    statement.setString(stIndex++, form11.getDeejiinTurul_en());
                }

                if (form11.getSumBagNer() != null && !form11.getSumBagNer().isEmpty()){
                    statement.setString(stIndex++, form11.getSumBagNer());
                }
                if (form11.getSumBagNer_en() != null && !form11.getSumBagNer_en().isEmpty()){
                    statement.setString(stIndex++, form11.getSumBagNer_en());
                }

                if (form11.getMalTurul() != null && !form11.getMalTurul().isEmpty()) {
                    statement.setString(stIndex++, form11.getMalTurul());
                }
                if (form11.getMalTurul_en() != null && !form11.getMalTurul_en().isEmpty()) {
                    statement.setString(stIndex++, form11.getMalTurul_en());
                }

                if (form11.getTooTolgoi() != null && !form11.getTooTolgoi().toString().isEmpty() && form11.getTooTolgoi() != 0){
                    statement.setString(stIndex++, form11.getTooTolgoi().toString());
                }

                if (form11.getHaanaShinjilsen() != null && !form11.getHaanaShinjilsen().isEmpty()){
                    statement.setString(stIndex++, form11.getHaanaShinjilsen());
                }
                if (form11.getHaanaShinjilsen_en() != null && !form11.getHaanaShinjilsen_en().isEmpty()){
                    statement.setString(stIndex++, form11.getHaanaShinjilsen_en());
                }

                if (form11.getShinjilsenArga() != null && !form11.getShinjilsenArga().isEmpty()){
                    statement.setString(stIndex++, form11.getShinjilsenArga());
                }
                if (form11.getShinjilsenArga_en() != null && !form11.getShinjilsenArga_en().isEmpty()){
                    statement.setString(stIndex++, form11.getShinjilsenArga_en());
                }

                if (form11.getEruul() != null && !form11.getEruul().toString().isEmpty() && form11.getEruul() != 0){
                    statement.setString(stIndex++, form11.getEruul().toString());
                }

                if (form11.getUvchtei() != null && !form11.getUvchtei().toString().isEmpty() && form11.getUvchtei() != 0){
                    statement.setString(stIndex++, form11.getUvchtei().toString());
                }

                if (form11.getOnosh() != null && !form11.getOnosh().toString().isEmpty() && form11.getOnosh() != 0){
                    statement.setString(stIndex++, form11.getOnosh().toString());
                }

                if (form11 != null && form11.getAimag() != null && !form11.getAimag().isEmpty()){
                    statement.setString(stIndex++, form11.getAimag());
                }
                if (form11 != null && form11.getSum() != null && !form11.getSum().isEmpty()){
                    statement.setString(stIndex++, form11.getSum());
                }

                if (form11 != null && form11.getBagCode() != null && !form11.getBagCode().isEmpty()){
                    statement.setString(stIndex++, form11.getBagCode());
                }

                if (form11 != null && form11.getRecordDate() != null && form11.getSearchRecordDate() != null){
                    statement.setDate(stIndex++, form11.getRecordDate());
                    statement.setDate(stIndex++, form11.getSearchRecordDate());
                }

                if (form11 != null && form11.getCreby() != null && !form11.getCreby().isEmpty()){
                    statement.setString(stIndex++, form11.getCreby());
                }

                ResultSet resultSet = statement.executeQuery();

                form11EntitieList = new ArrayList<>();

                IComboService comboService = new ComboServiceImpl(boneCP);
                List<BasicDBObject> cmbType = comboService.getComboVals("MAL", "MN");
                List<BasicDBObject> deejType = comboService.getComboVals("DEEJ", "MN");
                List<BasicDBObject> shgazarType = comboService.getComboVals("SHGAZ", "MN");
                List<BasicDBObject> shargaType = comboService.getComboVals("SHARGA", "MN");

                int index=1;

                while (resultSet.next()) {
                    Form11Entity entity = new Form11Entity();

                    entity.setId(resultSet.getLong("id"));
                    entity.setDeejiinTurul(resultSet.getString("deejiinTurul_mn"));
                    entity.setDeejiinTurul_en(resultSet.getString("deejiinTurul_en"));
                    entity.setMalTurul(resultSet.getString("malTurul_mn"));
                    entity.setMalTurul_en(resultSet.getString("malTurul_en"));
                    entity.setTooTolgoi(Long.parseLong(resultSet.getString("tooTolgoi_mn")));
                    entity.setTooTolgoi_en(Long.parseLong(resultSet.getString("tooTolgoi_en")));
                    entity.setHaanaShinjilsen(resultSet.getString("haanaShinjilsen_mn"));
                    entity.setHaanaShinjilsen_en(resultSet.getString("haanaShinjilsen_en"));
                    entity.setShinjilsenArga(resultSet.getString("shinjilsenArga_mn"));
                    entity.setShinjilsenArga_en(resultSet.getString("shinjilsenArga_en"));
                    entity.setEruul(Long.parseLong(resultSet.getString("eruul_mn")));
                    entity.setEruul_en(Long.parseLong(resultSet.getString("eruul_en")));
                    entity.setUvchtei(Long.parseLong(resultSet.getString("uvchtei_mn")));
                    entity.setUvchtei_en(Long.parseLong(resultSet.getString("uvchtei_en")));
                    entity.setOnosh(Long.parseLong(resultSet.getString("onosh_mn")));
                    entity.setOnosh_en(Long.parseLong(resultSet.getString("onosh_en")));
                    entity.setDelflg(resultSet.getString("delflg"));
                    entity.setActflg(resultSet.getString("actflg"));
                    entity.setModAt(resultSet.getDate("mod_at"));
                    entity.setModby(resultSet.getString("mod_by"));
                    entity.setCreat(resultSet.getDate("cre_at"));
                    entity.setCreby(resultSet.getString("cre_by"));
                    entity.setAimag(resultSet.getString("aimag"));
                    entity.setAimag_en(resultSet.getString("aimag_en"));
                    entity.setSumBagNer(resultSet.getString("sumBagNer_mn"));
                    entity.setSumBagNer_en(resultSet.getString("sumBagNer_en"));
                    entity.setSum(resultSet.getString("sum"));
                    entity.setSum_en(resultSet.getString("sum_en"));
                    entity.setAimagNer(resultSet.getString("cityname"));
                    entity.setSumNer(resultSet.getString("sumname"));
                    entity.setRecordDate(resultSet.getDate("recorddate"));
                    entity.put("sum_latitude", resultSet.getString("sum_latitude"));
                    entity.put("sum_longitude", resultSet.getString("sum_longitude"));

                    entity.put("bagname", resultSet.getString("horooname"));
                    entity.put("malturlner", getTypeNer(cmbType, resultSet.getString("malTurul_mn")));
                    entity.put("deejner", getTypeNer(deejType, resultSet.getString("deejiinTurul_mn")));
                    entity.put("shgazarner", getTypeNer(shgazarType, resultSet.getString("haanaShinjilsen_mn")));
                    entity.put("sharganer", getTypeNer(shargaType, resultSet.getString("shinjilsenArga_mn")));
                    entity.put("index",index++);

                    form11EntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return form11EntitieList;
    }// end of function

    private String getTypeNer(List<BasicDBObject> cmbType, String malTurul_mn) {
        for (BasicDBObject obj :
                cmbType) {
            if (obj.get("id") != null && malTurul_mn != null && obj.getString("id").equalsIgnoreCase(malTurul_mn)) {
                return obj.getString("name");
            }
        }

        return "";
    }

    @Override
    public ErrorEntity updateData(Form11Entity form11) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (form11 == null) new ErrorEntity(ErrorType.INFO, Long.valueOf(1059));

        query = "UPDATE `hospital`.`hform11` " +
                "SET     ";

        if (form11.getDeejiinTurul() != null && !form11.getDeejiinTurul().isEmpty()){
            query += " deejiinTurul_mn = ?, ";
        }

        if (form11.getDeejiinTurul_en() != null && !form11.getDeejiinTurul_en().isEmpty()){
            query += " deejiinTurul_en = ?, ";
        }

        if (form11.getMalTurul() != null && !form11.getMalTurul().isEmpty()) {
            query += " malTurul_mn = ?, ";
        }
        if (form11.getMalTurul_en() != null && !form11.getMalTurul_en().isEmpty()) {
            query += " malTurul_en = ?, ";
        }

        if (form11.getTooTolgoi() != null && !form11.getTooTolgoi().toString().isEmpty()){
            query += " tooTolgoi_mn = ?, ";
        }
        if (form11.getTooTolgoi_en() != null && !form11.getTooTolgoi_en().toString().isEmpty()){
            query += " tooTolgoi_en = ?, ";
        }

        if (form11.getHaanaShinjilsen() != null && !form11.getHaanaShinjilsen().isEmpty()){
            query += " haanaShinjilsen_mn = ?, ";
        }
        if (form11.getHaanaShinjilsen_en() != null && !form11.getHaanaShinjilsen_en().isEmpty()){
            query += " haanaShinjilsen_en = ?, ";
        }

        if (form11.getShinjilsenArga() != null && !form11.getShinjilsenArga().isEmpty()){
            query += " shinjilsenArga_mn = ?, ";
        }
        if (form11.getShinjilsenArga_en() != null && !form11.getShinjilsenArga_en().isEmpty()){
            query += " shinjilsenArga_en = ?, ";
        }

        if (form11.getEruul() != null && !form11.getEruul().toString().isEmpty()){
            query += " eruul_mn = ?, ";
        }
        if (form11.getEruul_en() != null && !form11.getEruul_en().toString().isEmpty()){
            query += " eruul_en = ?, ";
        }

        if (form11.getUvchtei() != null && !form11.getUvchtei().toString().isEmpty()){
            query += " uvchtei_en = ?, ";
        }
        if (form11.getUvchtei_en() != null && !form11.getUvchtei_en().toString().isEmpty()){
            query += " uvchtei_en = ?, ";
        }

        if (form11.getOnosh() != null && !form11.getOnosh().toString().isEmpty()){
            query += " onosh_mn = ?, ";
        }
        if (form11.getOnosh_en() != null && !form11.getOnosh_en().toString().isEmpty()){
            query += " onosh_en = ?, ";
        }

        if (form11.getDelflg() != null && !form11.getDelflg().isEmpty()){
            query += " delflg = ?, ";
        }
        if (form11.getActflg() != null && !form11.getActflg().isEmpty()){
            query += " actflg = ?, ";
        }
        if (form11 != null && form11.getRecordDate() != null){
            query += " `recorddate` = ?, ";
        }

        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";
        query += " WHERE  id   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (form11.getDeejiinTurul() != null && !form11.getDeejiinTurul().isEmpty()) {
                    statement.setString(stIndex++, form11.getDeejiinTurul());
                }
                if (form11.getDeejiinTurul_en() != null && !form11.getDeejiinTurul_en().isEmpty()) {
                    statement.setString(stIndex++, form11.getDeejiinTurul_en());
                }

                if (form11.getMalTurul() != null && !form11.getMalTurul().isEmpty()) {
                    statement.setString(stIndex++, form11.getMalTurul());
                }
                if (form11.getMalTurul_en() != null && !form11.getMalTurul_en().isEmpty()) {
                    statement.setString(stIndex++, form11.getMalTurul_en());
                }

                if (form11.getTooTolgoi() != null && !form11.getTooTolgoi().toString().isEmpty()){
                    statement.setString(stIndex++, form11.getTooTolgoi().toString());
                }
                if (form11.getTooTolgoi_en() != null && !form11.getTooTolgoi_en().toString().isEmpty()){
                    statement.setString(stIndex++, form11.getTooTolgoi_en().toString());
                }

                if (form11.getHaanaShinjilsen() != null && !form11.getHaanaShinjilsen().isEmpty()){
                    statement.setString(stIndex++, form11.getHaanaShinjilsen());
                }
                if (form11.getHaanaShinjilsen_en() != null && !form11.getHaanaShinjilsen_en().isEmpty()){
                    statement.setString(stIndex++, form11.getHaanaShinjilsen_en());
                }

                if (form11.getShinjilsenArga() != null && !form11.getShinjilsenArga().isEmpty()){
                    statement.setString(stIndex++, form11.getShinjilsenArga());
                }
                if (form11.getShinjilsenArga_en() != null && !form11.getShinjilsenArga_en().isEmpty()){
                    statement.setString(stIndex++, form11.getShinjilsenArga_en());
                }

                if (form11.getEruul() != null && !form11.getEruul().toString().isEmpty()){
                    statement.setString(stIndex++, form11.getEruul().toString());
                }
                if (form11.getEruul_en() != null && !form11.getEruul_en().toString().isEmpty()){
                    statement.setString(stIndex++, form11.getEruul_en().toString());
                }

                if (form11.getUvchtei() != null && !form11.getUvchtei().toString().isEmpty()){
                    statement.setString(stIndex++, form11.getUvchtei().toString());
                }
                if (form11.getUvchtei_en() != null && !form11.getUvchtei_en().toString().isEmpty()){
                    statement.setString(stIndex++, form11.getUvchtei_en().toString());
                }

                if (form11.getOnosh() != null && !form11.getOnosh().toString().isEmpty()){
                    statement.setString(stIndex++, form11.getOnosh().toString());
                }
                if (form11.getOnosh_en() != null && !form11.getOnosh_en().toString().isEmpty()){
                    statement.setString(stIndex++, form11.getOnosh_en().toString());
                }

                if (form11.getDelflg() != null && !form11.getDelflg().isEmpty()) {
                    statement.setString(stIndex++, form11.getDelflg());
                }

                if (form11.getActflg() != null && !form11.getActflg().isEmpty()) {
                    statement.setString(stIndex++, form11.getActflg());
                }

                if (form11 != null && form11.getRecordDate() != null){
                    statement.setDate(stIndex++, form11.getRecordDate());
                }

                statement.setDate(stIndex++, form11.getModAt());
                statement.setString(stIndex++, form11.getModby());
                statement.setLong(stIndex++, form11.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    return new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }


        return new ErrorEntity(ErrorType.ERROR, Long.valueOf(1061));
    }

    @Override
    public List<BasicDBObject> getPieChartData(Form11Entity entity) throws SQLException {
        int stIndex = 1;
        List<BasicDBObject> formEntitieList = null;

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }


        String query_sel = "SELECT `malTurul_mn`, COUNT(`id`) AS cnt, `hform11`.`aimag`, `hform11`.`sum`  " +
                "FROM `hospital`.`hform11`  " +
                "WHERE   ";

        if (entity != null && !entity.getAimag().isEmpty()){
            query_sel += " `hform11`.`aimag` = ? AND ";
        }
        if (entity != null && !entity.getSum().isEmpty()){
            query_sel += " `hform11`.`sum` = ? AND ";
        }
        if (entity != null && !entity.getDelflg().isEmpty()){
            query_sel += " `hform11`.`delflg` = ? AND ";
        }
        if (entity != null && !entity.getActflg().isEmpty()){
            query_sel += " `hform11`.`actflg` = ? AND ";
        }
        if (entity != null && !entity.getMalTurul().isEmpty()){
            query_sel += " `hform11`.`malTurul_mn` = ? AND ";
        }
        if (entity != null && !entity.getCreby().isEmpty()){
            query_sel += " `hform11`.`cre_by` = ? AND ";
        }
        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null){
            query_sel += " (`recorddate` BETWEEN ? AND ?) AND ";
        }

        query_sel += " `id` > 0  GROUP BY `hform11`.`malTurul_mn`";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                int stindex = 1;

                if (entity != null && !entity.getAimag().isEmpty()){
                    statement.setString(stindex++, entity.getAimag());
                }
                if (entity != null && !entity.getSum().isEmpty()){
                    statement.setString(stindex++, entity.getSum());
                }
                if (entity != null && !entity.getDelflg().isEmpty()){
                    statement.setString(stindex++, entity.getDelflg());
                }
                if (entity != null && !entity.getActflg().isEmpty()){
                    statement.setString(stindex++, entity.getActflg());
                }
                if (entity != null && !entity.getMalTurul().isEmpty()){
                    statement.setString(stindex++, entity.getMalTurul());
                }
                if (entity != null && !entity.getCreby().isEmpty()){
                    statement.setString(stindex++, entity.getCreby());
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null){
                    statement.setDate(stindex++, entity.getRecordDate());
                    statement.setDate(stindex++, entity.getSearchRecordDate());
                }

                ResultSet resultSet = statement.executeQuery();

                formEntitieList = new ArrayList<>();

                IComboService comboService = new ComboServiceImpl(boneCP);
                List<BasicDBObject> cmbType = comboService.getComboVals("MAL", "MN");

                while (resultSet.next()) {
                    BasicDBObject obj = new BasicDBObject();

                    obj.put("malturulner", getMalTurulNer(cmbType, resultSet.getString("malTurul_mn")));
                    obj.put("malturul", resultSet.getString("malTurul_mn"));
                    obj.put("count", resultSet.getString("cnt"));
                    obj.put("aimag", resultSet.getString("aimag"));
                    obj.put("sum", resultSet.getString("sum"));

                    formEntitieList.add(obj);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return formEntitieList;
    }

    private String getMalTurulNer(List<BasicDBObject> cmbType, String malTurul_mn) {
        for (BasicDBObject obj :
                cmbType) {
            if (obj.get("id") != null && malTurul_mn != null && obj.getString("id").equalsIgnoreCase(malTurul_mn)) {
                return obj.getString("name");
            }
        }

        return "";
    }

    @Override
    public BasicDBObject getColumnChartData(Form11Entity entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        //dateFormat.parse(dv).getTime()
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }

        try {
            IComboService comboService = new ComboServiceImpl(boneCP);
            List<Integer[]> rowdataList = new ArrayList<>();
            List<BasicDBObject> cmbType = comboService.getComboVals("MAL", "MN");

            basicDBObject.put("coldata", cmbType);

            //Saraar n haruulah
            int month = calendar1.get(Calendar.MONTH) + 1;
            int year1 = calendar1.get(Calendar.YEAR);
            int year2 = calendar2.get(Calendar.YEAR);

            try (Connection connection = boneCP.getConnection()) {
                String query_sel = "SELECT `malTurul_mn`, COUNT(`id`) AS cnt, `hform11`.`aimag`, `hform11`.`sum`  " +
                        "FROM `hospital`.`hform11`  " +
                        "WHERE  ";

                if (entity != null && !entity.getAimag().isEmpty()){
                    query_sel += " `hform11`.`aimag` = ? AND ";
                }
                if (entity != null && !entity.getSum().isEmpty()){
                    query_sel += " `hform11`.`sum` = ? AND ";
                }
                if (entity != null && !entity.getDelflg().isEmpty()){
                    query_sel += " `hform11`.`delflg` = ? AND ";
                }
                if (entity != null && !entity.getActflg().isEmpty()){
                    query_sel += " `hform11`.`actflg` = ? AND ";
                }
                if (entity != null && !entity.getMalTurul().isEmpty()){
                    query_sel += " `hform11`.`malTurul_mn` = ? AND ";
                }
                if (entity != null && !entity.getCreby().isEmpty()){
                    query_sel += " `hform11`.`cre_by` = ? AND ";
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null){
                    query_sel += " (`recorddate` BETWEEN ? AND ?) AND ";
                }

                query_sel += " `id` > 0  GROUP BY `hform11`.`malTurul_mn` ORDER BY `hform11`.`malTurul_mn`";

                do {
                    //do process
                    Integer[] rowdata = new Integer[cmbType.size() + 1];
                    //
                    //rowdata[0] = month + " -р сар";
                    rowdata[0] = month;

                    try {
                        PreparedStatement statement = connection.prepareStatement(query_sel);

                        java.sql.Date stdate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-01 00:00:00").getTime());

                        java.sql.Date enddate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-31 00:00:00").getTime());

                        if (month == 12 && year1 < year2) {
                            month = 1;
                            year1++;
                        } else {
                            month++;
                        }

                        int stindex = 1;

                        if (entity != null && !entity.getAimag().isEmpty()){
                            statement.setString(stindex++, entity.getAimag());
                        }
                        if (entity != null && !entity.getSum().isEmpty()){
                            statement.setString(stindex++, entity.getSum());
                        }
                        if (entity != null && !entity.getDelflg().isEmpty()){
                            statement.setString(stindex++, entity.getDelflg());
                        }
                        if (entity != null && !entity.getActflg().isEmpty()){
                            statement.setString(stindex++, entity.getActflg());
                        }
                        if (entity != null && !entity.getMalTurul().isEmpty()){
                            statement.setString(stindex++, entity.getMalTurul());
                        }
                        if (entity != null && !entity.getCreby().isEmpty()){
                            statement.setString(stindex++, entity.getCreby());
                        }
                        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null){
                            statement.setDate(stindex++, stdate);
                            statement.setDate(stindex++, enddate);
                        }


                        ResultSet resultSet = statement.executeQuery();

                        HashMap<String, String> hashMap = new HashMap<>();

                        while (resultSet.next()) {
                            String malturul = resultSet.getString("malTurul_mn");
                            String count = resultSet.getString("cnt");

                            hashMap.put(malturul, count);
                        }//end while

                        for (int i = 1; i <= cmbType.size(); i++) {
                            rowdata[i] = (hashMap.get(cmbType.get(i - 1).getString("id")) == null) ? 0 :
                                    Integer.parseInt(hashMap.get(cmbType.get(i - 1).getString("id")));
                        }

                        rowdataList.add(rowdata);

                        if (statement != null) statement.close();
                        if (resultSet != null) resultSet.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();

                        break;
                    }

                    ///
                } while (month <= calendar2.get(Calendar.MONTH) + 1);

                basicDBObject.put("rowdata", rowdataList);

                connection.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }


    @Override
    public ErrorEntity exportReport(String file, Form11Entity form11) throws SQLException {
        ErrorEntity errorEntity = null;

        String fileName="hform11";

        try (Connection connection = boneCP.getConnection()){
            String JRXML_PATH = Config.getJrxmlPath();
            String REPORT_PATH = Config.getReportPath();

            try {
                Map parameters = new HashMap();

                String reportTitle = "АЙМАГ, СУМЫН МАЛ ЭМНЭЛГИЙН ЛАБОРАТОРИУДЫН ШИЖИЛГЭЭНИЙ $$SEASON$$ -Р УЛИРАЛЫН ТАЙЛАН",
                        currentYear = "$$YEAR$$ он №....",
                        printDate = "$$YEAR$$ оны $$MONTH$$ сарын $$DAY$$";

                Calendar now = Calendar.getInstance();

                int year = now.get(Calendar.YEAR);
                int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
                int day = now.get(Calendar.DAY_OF_MONTH);

                printDate = printDate.replace("$$YEAR$$", String.valueOf(year));
                printDate = printDate.replace("$$MONTH$$", String.valueOf(month));
                printDate = printDate.replace("$$DAY$$", String.valueOf(day));

                currentYear = currentYear.replace("$$YEAR$$", String.valueOf(year));

                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(new Date(form11.getRecordDate().getTime()));

                reportTitle = reportTitle.replace("$$YEAR$$", String.valueOf(calendar1.get(Calendar.YEAR)));

                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(new Date(form11.getSearchRecordDate().getTime()));

                if (calendar2.get(Calendar.MONTH) < 4){
                    reportTitle = reportTitle.replace("$$SEASON$$", "1");
                }
                else if (calendar2.get(Calendar.MONTH) < 7){
                    reportTitle = reportTitle.replace("$$SEASON$$", "2");
                }
                else if (calendar2.get(Calendar.MONTH) < 10){
                    reportTitle = reportTitle.replace("$$SEASON$$", "3");
                }
                else if (calendar2.get(Calendar.MONTH) <= 12){
                    reportTitle = reportTitle.replace("$$SEASON$$", "4");
                }

                parameters.put("start_date", form11.getRecordDate());
                parameters.put("end_date", form11.getSearchRecordDate());
                parameters.put("citycode", (form11.getAimag() != null && !form11.getAimag().isEmpty()) ? form11.getAimag() : null);
                parameters.put("sumcode", (form11.getSum() != null && !form11.getSum().isEmpty()) ? form11.getSum() : null);
                parameters.put("currentyear", currentYear);

                ICityService cityService = new CityServiceImpl(boneCP);
                City tmpCity = new City();
                tmpCity.setCode(form11.getAimag());
                List<City> clist = cityService.selectAll(tmpCity);

                if (clist != null && clist.size() == 1) {
                    parameters.put("aimagner", clist.get(0).getCityname());
                } else {
                    parameters.put("aimagner", form11.getAimagNer());
                }

                parameters.put("printdate", printDate);
                parameters.put("reporttitle", reportTitle);
                parameters.put("creby", (form11.getCreby() != null && !form11.getCreby().isEmpty()) ? form11.getCreby() : null);

                PdfExcelUtil.generateReport(file, JRXML_PATH, REPORT_PATH, fileName, parameters, connection);

                System.out.println("File Generated");
            }catch (Exception ex){
                ex.printStackTrace();
            }
            finally {
                if (connection != null)connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1001));
            if(file.equals("pdf")){
                errorEntity.setErrText(fileName + ".pdf");
            }else if(file.equals("xls")){
                errorEntity.setErrText(fileName + ".xls");
            }
        }
        return errorEntity;
    }

    @Override
    public BasicDBObject getColHighChartData(Form11Entity form11Entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();
        List<String> categories = new ArrayList<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(form11Entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(form11Entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }

        try {
            IComboService comboService = new ComboServiceImpl(boneCP);
            List<BasicDBObject> cmbType = comboService.getComboNames("MAL", "MN");
            if (form11Entity != null && !form11Entity.getMalTurul().isEmpty()) {
                cmbType = comboService.getComboNames("DEEJ", "MN");
            }
            //Saraar n haruulah
            int month = calendar1.get(Calendar.MONTH) + 1;
            int year1 = calendar1.get(Calendar.YEAR);
            int year2 = calendar2.get(Calendar.YEAR);

            try (Connection connection = boneCP.getConnection()) {
                String query_sel = "SELECT `malTurul_mn`, COUNT(`id`) AS cnt, `hform11`.`aimag`, `hform11`.`sum`  " +
                        "FROM `hospital`.`hform11`  " +
                        "WHERE   ";
                if (form11Entity != null && !form11Entity.getMalTurul().isEmpty()) {
                    query_sel = "SELECT `deejiinTurul_mn`, SUM(`tooTolgoi_mn`) AS cnt, `hform11`.`aimag`, `hform11`.`sum`" +
                            "FROM `hospital`.`hform11`  " +
                            "WHERE  ";
                }
                if (form11Entity != null && !form11Entity.getAimag().isEmpty()) {
                    query_sel += " `hform11`.`aimag` = ? AND ";
                }
                if (form11Entity != null && !form11Entity.getSum().isEmpty()) {
                    query_sel += " `hform11`.`sum` = ? AND ";
                }
                if (form11Entity != null && !form11Entity.getDelflg().isEmpty()) {
                    query_sel += " `hform11`.`delflg` = ? AND ";
                }
                if (form11Entity != null && !form11Entity.getActflg().isEmpty()) {
                    query_sel += " `hform11`.`actflg` = ? AND ";
                }
                if (form11Entity != null && !form11Entity.getMalTurul().isEmpty()) {
                    query_sel += " `hform11`.`malTurul_mn` = ? AND ";
                }
                if (form11Entity != null && !form11Entity.getDeejiinTurul().isEmpty()) {
                    query_sel += " `hform11`.`deejiinTurul_mn` = ? AND ";
                }
                if (form11Entity != null && !form11Entity.getCreby().isEmpty()) {
                    query_sel += " `hform11`.`cre_by` = ? AND ";
                }
                if (form11Entity != null && form11Entity.getRecordDate() != null && form11Entity.getSearchRecordDate() != null) {
                    query_sel += " (`recorddate` BETWEEN ? AND ?) AND ";
                }

                if (form11Entity != null && !form11Entity.getMalTurul().isEmpty()) {
                    query_sel += " `id` > 0  GROUP BY `hform11`.`deejiinTurul_mn` ORDER BY `hform11`.`deejiinTurul_mn`";
                }
                else{
                    query_sel += " `id` > 0  GROUP BY `hform11`.`malTurul_mn` ORDER BY `hform11`.`malTurul_mn`";
                }
                do {
                    //do process
                    categories.add(year1 + "-" + month);

                    try {
                        PreparedStatement statement = connection.prepareStatement(query_sel);

                        java.sql.Date stdate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-01 00:00:00").getTime());

                        java.sql.Date enddate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-31 00:00:00").getTime());

                        if (month == 12 && year1 < year2) {
                            month = 1;
                            year1++;
                        } else {
                            month++;
                        }

                        int stindex = 1;

                        if (form11Entity != null && !form11Entity.getAimag().isEmpty()) {
                            statement.setString(stindex++, form11Entity.getAimag());
                        }
                        if (form11Entity != null && !form11Entity.getSum().isEmpty()) {
                            statement.setString(stindex++, form11Entity.getSum());
                        }
                        if (form11Entity != null && !form11Entity.getDelflg().isEmpty()) {
                            statement.setString(stindex++, form11Entity.getDelflg());
                        }
                        if (form11Entity != null && !form11Entity.getActflg().isEmpty()) {
                            statement.setString(stindex++, form11Entity.getActflg());
                        }
                        if (form11Entity != null && !form11Entity.getMalTurul().isEmpty()) {
                            statement.setString(stindex++, form11Entity.getMalTurul());
                        }
                        if (form11Entity != null && !form11Entity.getCreby().isEmpty()) {
                            statement.setString(stindex++, form11Entity.getCreby());
                        }
                        if (form11Entity != null && !form11Entity.getDeejiinTurul().isEmpty()) {
                            statement.setString(stindex++, form11Entity.getDeejiinTurul());
                        }
                        if (form11Entity != null && form11Entity.getRecordDate() != null && form11Entity.getSearchRecordDate() != null) {
                            statement.setDate(stindex++, stdate);
                            statement.setDate(stindex++, enddate);
                        }
                        ResultSet resultSet = statement.executeQuery();

                        HashMap<String, String> hashMap = new HashMap<>();

                        while (resultSet.next()) {
                            String uvchinner = "";
                            if (form11Entity != null && !form11Entity.getMalTurul().isEmpty()) {
                                uvchinner = resultSet.getString("deejiinTurul_mn");
                            }else{
                                uvchinner = resultSet.getString("malTurul_mn");
                            }
                            String count = resultSet.getString("cnt");
                            hashMap.put(uvchinner, count);
                        }//end while

                        List<Double> list = null;
                         for (int i = 0; i < cmbType.size(); i++) {
                            BasicDBObject obj = cmbType.get(i);

                            if (obj.get("data") == null){
                                //new record
                                list = new ArrayList<>();
                                String val = hashMap.get(obj.getString("id"));
                                if (val == null) list.add(0.0);
                                else if (val.isEmpty()) list.add(0.0);
                                else list.add(Double.parseDouble(val));
                                obj.put("data", list);
                            }else{
                                //already exist
                                list = (List<Double>) obj.get("data");
                                String val = hashMap.get(obj.getString("id"));
                                if (val == null) list.add(0.0);
                                else if (val.isEmpty()) list.add(0.0);
                                else list.add(Double.parseDouble(val));
                            }
                        }

                        if (statement != null) statement.close();
                        if (resultSet != null) resultSet.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();

                        break;
                    }

                    ///
                } while ((month <= calendar2.get(Calendar.MONTH) + 1 && year1 <= year2) || year1 < year2);

                basicDBObject.put("categories", categories);
                basicDBObject.put("series", cmbType);

                connection.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }

    @Override
    public BasicDBObject getPieHighChartData(Form11Entity form11Entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(form11Entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(form11Entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }


        String query_sel = "SELECT `malTurul_mn`, COUNT(`id`) AS cnt, `hform11`.`aimag`, `hform11`.`sum`  " +
                "FROM `hospital`.`hform11`  " +
                "WHERE   ";
        if (form11Entity != null && !form11Entity.getMalTurul().isEmpty()) {
            query_sel = "SELECT `deejiinTurul_mn`, SUM(`tooTolgoi_mn`) AS cnt, `hform11`.`aimag`, `hform11`.`sum`" +
                    "FROM `hospital`.`hform11`  " +
                    "WHERE  ";
        }
        if (form11Entity != null && !form11Entity.getAimag().isEmpty()) {
            query_sel += " `hform11`.`aimag` = ? AND ";
        }
        if (form11Entity != null && !form11Entity.getSum().isEmpty()) {
            query_sel += " `hform11`.`sum` = ? AND ";
        }
        if (form11Entity != null && !form11Entity.getDelflg().isEmpty()) {
            query_sel += " `hform11`.`delflg` = ? AND ";
        }
        if (form11Entity != null && !form11Entity.getActflg().isEmpty()) {
            query_sel += " `hform11`.`actflg` = ? AND ";
        }
        if (form11Entity != null && !form11Entity.getMalTurul().isEmpty()) {
            query_sel += " `hform11`.`malTurul_mn` = ? AND ";
        }
        if (form11Entity != null && !form11Entity.getDeejiinTurul().isEmpty()) {
            System.out.println(form11Entity.getDeejiinTurul());
            query_sel += " `hform11`.`deejiinTurul_mn` = ? AND ";
        }
        if (form11Entity != null && !form11Entity.getCreby().isEmpty()) {
            query_sel += " `hform11`.`cre_by` = ? AND ";
        }
        if (form11Entity != null && form11Entity.getRecordDate() != null && form11Entity.getSearchRecordDate() != null) {
            query_sel += " (`recorddate` BETWEEN ? AND ?) AND ";
        }

        if (form11Entity != null && !form11Entity.getMalTurul().isEmpty()) {
            query_sel += " `id` > 0  GROUP BY `hform11`.`deejiinTurul_mn` ORDER BY `hform11`.`deejiinTurul_mn`";
        }
        else{
            query_sel += " `id` > 0  GROUP BY `hform11`.`malTurul_mn` ORDER BY `hform11`.`malTurul_mn`";
        }

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                int stindex = 1;

                if (form11Entity != null && !form11Entity.getAimag().isEmpty()) {
                    statement.setString(stindex++, form11Entity.getAimag());
                }
                if (form11Entity != null && !form11Entity.getSum().isEmpty()) {
                    statement.setString(stindex++, form11Entity.getSum());
                }
                if (form11Entity != null && !form11Entity.getDelflg().isEmpty()) {
                    statement.setString(stindex++, form11Entity.getDelflg());
                }
                if (form11Entity != null && !form11Entity.getActflg().isEmpty()) {
                    statement.setString(stindex++, form11Entity.getActflg());
                }
                if (form11Entity != null && !form11Entity.getMalTurul().isEmpty()) {
                    statement.setString(stindex++, form11Entity.getMalTurul());
                }
                if (form11Entity != null && !form11Entity.getCreby().isEmpty()) {
                    statement.setString(stindex++, form11Entity.getCreby());
                }
                if (form11Entity != null && !form11Entity.getDeejiinTurul().isEmpty()) {
                    statement.setString(stindex++, form11Entity.getDeejiinTurul());
                }
                if (form11Entity != null && form11Entity.getRecordDate() != null && form11Entity.getSearchRecordDate() != null) {
                    statement.setDate(stindex++, form11Entity.getRecordDate());
                    statement.setDate(stindex++, form11Entity.getSearchRecordDate());
                }

                ResultSet resultSet = statement.executeQuery();

                IComboService comboService = new ComboServiceImpl(boneCP);
                List<BasicDBObject> cmbType = comboService.getComboNames("MAL", "MN");
                List<BasicDBObject> list = new ArrayList<>();
                if (form11Entity != null && !form11Entity.getMalTurul().isEmpty()) {
                    cmbType = comboService.getComboNames("DEEJ", "MN");
                }
                while (resultSet.next()) {
                    BasicDBObject entity = new BasicDBObject();

                    if (form11Entity != null && !form11Entity.getMalTurul().isEmpty()) {
                        entity.put("name", getMalTurulNer(cmbType, resultSet.getString("deejiinTurul_mn")));
                    } else {
                        entity.put("name", getMalTurulNer(cmbType, resultSet.getString("malTurul_mn")));
                    }
                    entity.put("y", resultSet.getDouble("cnt"));

                    list.add(entity);
                }

                basicDBObject.put("data", list);

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }

}
