package com.hospital.app;

import com.model.hos.ErrorEntity;
import com.model.hos.Form52Entity;
import com.model.hos.Form61Entity;
import com.mongodb.BasicDBObject;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public interface IForm61Service {

    /**
     * Insert new data
     *
     *
     * @param form61
     * @return
     * @throws Exception
     */
    ErrorEntity insertNewData(Form61Entity form61) throws SQLException;

    /**
     * Select all
     *
     * @param form61
     * @return
     * @throws SQLException
     */
    List<Form61Entity> getListData(Form61Entity form61) throws SQLException;


    /**
     * update form data
     *
     * @param form61
     * @return
     * @throws SQLException
     */
    ErrorEntity updateData(Form61Entity form61) throws SQLException;


    List<Form61Entity> getPieChartData(Form61Entity form61Entity) throws SQLException;

    ErrorEntity exportReport(String file, Form61Entity form61) throws SQLException;

    BasicDBObject getColHighChartData(Form61Entity Form61Entity) throws SQLException;

    BasicDBObject getPieHighChartData(Form61Entity Form61Entity) throws SQLException;
}
