package com.hospital.app;


import com.Config;
import com.enumclass.ErrorType;
import com.jolbox.bonecp.BoneCP;
import com.model.hos.*;
import com.mongodb.BasicDBObject;
import com.rpc.PdfExcelUtil;
import net.sf.jasperreports.engine.JRStyle;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.design.JRDesignStyle;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public class Form13aServiceImpl implements IForm13aService{
    /**
     * Objects
     */

    private BoneCP boneCP;
    private ErrorEntity errObj;

    public Form13aServiceImpl(BoneCP boneCP) {
        this.boneCP = boneCP;
    }

    @Override
    public ErrorEntity insertNewData(Form13aEntity form08) throws SQLException {
        PreparedStatement statement = null;

        if (form08 == null) {
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        String query_login = "INSERT INTO `hospital`.`hform13a`" +
                "(`uzleg_hiih`," +
                "`hemjih_negj`," +
                "`too_hemjee`," +
                "`deejiin_too`," +
                "`shinjilgee_hiisen`," +
                "`shinjilsen_arga`," +
                "`hergtseend_tohirson`," +
                "`ustgasan`," +
                "`haldbarguijuulelt`," +
                "`brutsellyoz`," +
                "`mastit`," +
                "`trixinell`," +
                "`finnoz`," +
                "`suriyee`," +
                "`boom`," +
                "`busad`," +
                "`bugd`," +
                "`del_flg`," +
                "`cre_by`," +
                "`cre_at`," +
                "`mod_by`," +
                "`mod_at`," +
                "`no_data`," +
                "`record_date`," +
                "`citycode`," +
                "`sumcode`," +
                "`bag_code`)" +
                "VALUES" +
                "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try (Connection connection = boneCP.getConnection()) {
            try{
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);
                statement.setString(1, form08.getUzleg_hiih());
                statement.setString(2, form08.getHemjih_negj());
                statement.setString(3, form08.getToo_hemjee());
                statement.setString(4, form08.getDeejiin_too());
                statement.setString(5, form08.getShinjilgee_hiisen());
                statement.setString(6, form08.getShinjilsen_arga());
                statement.setDouble(7, form08.getHergtseend_tohirson());
                statement.setDouble(8, form08.getUstgasan());
                statement.setDouble(9, form08.getHaldbarguijuulelt());
                statement.setString(10, form08.getBrutsellyoz() == null || form08.getBrutsellyoz().equals("") || form08.getBrutsellyoz().isEmpty() ? "0" : form08.getBrutsellyoz());
                statement.setString(11, form08.getMastit() == null || form08.getMastit().equals("") || form08.getMastit().isEmpty() ? "0" : form08.getMastit());
                statement.setString(12, form08.getTrixinell() == null || form08.getTrixinell().equals("") || form08.getTrixinell().isEmpty() ? "0" : form08.getTrixinell());
                statement.setString(13, form08.getFinnoz() == null || form08.getFinnoz().equals("") || form08.getFinnoz().isEmpty() ? "0" : form08.getFinnoz());
                statement.setString(14, form08.getSuriyee() == null || form08.getSuriyee().equals("") || form08.getSuriyee().isEmpty() ? "0" : form08.getSuriyee());
                statement.setString(15, form08.getBoom() == null || form08.getBoom().equals("") || form08.getBoom().isEmpty() ? "0" : form08.getBoom());
                statement.setString(16, form08.getBusad() == null || form08.getBusad().equals("") || form08.getBusad().isEmpty() ? "0" : form08.getBusad());
                statement.setString(17, form08.getBugd() == null || form08.getBugd().equals("") || form08.getBugd().isEmpty() ? "0" : form08.getBugd());
                statement.setString(18, form08.getDel_flg());
                statement.setString(19, form08.getCre_by());
                statement.setDate(20, form08.getCre_at());
                statement.setString(21, form08.getMod_by());
                statement.setDate(22, form08.getMod_at());
                statement.setString(23, form08.getNo_data());
                statement.setDate(24, form08.getRecordDate());
                statement.setString(25, form08.getCitycode());
                statement.setString(26, form08.getSumcode());
                statement.setString(27, form08.getBag_code());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1){
                    connection.commit();
                    errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));
                    errObj.setErrText("Бичлэг амжилттай хадгалагдлаа.");
                    errObj.setErrDesc("Бичлэг амжилттай хадгалагдлаа.");
                    return errObj;
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }finally {
                if (statement != null)statement.close();
                connection.close();
            }

        }catch (Exception ex){

        }

        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1057));
        errObj.setErrText("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");
        errObj.setErrDesc("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");

        return errObj;
    }

    @Override
    public List<Form13aEntity> getListData(Form13aEntity form13) throws SQLException {
        int stIndex = 1;
        List<Form13aEntity> form08EntitieList = null;

        String query = "SELECT `hform13a`.`id`, " +
                "    `hform13a`.`uzleg_hiih`, " +
                "    `hform13a`.`hemjih_negj`, " +
                "    `hform13a`.`too_hemjee`, " +
                "    `hform13a`.`deejiin_too`, " +
                "    `hform13a`.`shinjilgee_hiisen`, " +
                "    `hform13a`.`shinjilsen_arga`, " +
                "    `hform13a`.`hergtseend_tohirson`, " +
                "    `hform13a`.`ustgasan`, " +
                "    `hform13a`.`haldbarguijuulelt`, " +
                "    `hform13a`.`brutsellyoz`, " +
                "    `hform13a`.`mastit`, " +
                "    `hform13a`.`trixinell`, " +
                "    `hform13a`.`finnoz`, " +
                "    `hform13a`.`suriyee`, " +
                "    `hform13a`.`boom`, " +
                "    `hform13a`.`busad`, " +
                "    `hform13a`.`bugd`, " +
                "    `hform13a`.`del_flg`, " +
                "    `hform13a`.`cre_by`, " +
                "    `hform13a`.`cre_at`, " +
                "    `hform13a`.`mod_by`, " +
                "    `hform13a`.`mod_at`, " +
                "    `hform13a`.`no_data`, " +
                "    `hform13a`.`record_date`, " +
                "    `hform13a`.`citycode`, " +
                "    `hform13a`.`sumcode`, " +
                "    `hform13a`.`bag_code` " +
                "FROM `hospital`.`hform13a` INNER JOIN `h_city` AS c " +
                "ON c.code = `hform13a`.citycode " +
                "INNER JOIN `h_sum` " +
                "ON `h_sum`.sumcode = `hform13a`.sumcode " +
                "WHERE ";

        if (form13.getUzleg_hiih() != null && !form13.getUzleg_hiih().isEmpty()){
            query += " `hform13a`.uzleg_hiih = ? AND ";
        }

        if (form13.getHemjih_negj() != null && !form13.getHemjih_negj().isEmpty()){
            query += " `hform13a`.hemjih_negj = ? AND ";
        }

        if (form13.getToo_hemjee() != null && !form13.getToo_hemjee().isEmpty()){
            query += " `hform13a`.too_hemjee = ? AND ";
        }
        if (form13.getDeejiin_too() != null && !form13.getDeejiin_too().isEmpty()){
            query += " `hform13a`.deejiin_too = ? AND ";
        }
        if (form13.getShinjilgee_hiisen() != null && !form13.getShinjilgee_hiisen().isEmpty()){
            query += " `hform13a`.shinjilgee_hiisen = ? AND ";
        }
        if (form13.getShinjilsen_arga() != null && !form13.getShinjilsen_arga().isEmpty()){
            query += " `hform13a`.shinjilsen_arga = ? AND ";
        }
        if (form13.getHergtseend_tohirson() != null && !form13.getHergtseend_tohirson().toString().isEmpty()  && form13.getHergtseend_tohirson() != 0){
            query += " `hform13a`.hergtseend_tohirson = ? AND ";
        }
        if (form13.getUstgasan() != null && !form13.getUstgasan().toString().isEmpty() && form13.getUstgasan() != 0){
            query += " `hform13a`.ustgasan = ? AND ";
        }
        if (form13.getHaldbarguijuulelt() != null && !form13.getHaldbarguijuulelt().toString().isEmpty()  && form13.getHaldbarguijuulelt() != 0){
            query += " `hform13a`.haldbarguijuulelt = ? AND ";
        }
        if (form13.getBrutsellyoz() != null && !form13.getBrutsellyoz().isEmpty()){
            query += " `hform13a`.brutsellyoz = ? AND ";
        }
        if (form13.getMastit() != null && !form13.getMastit().isEmpty()){
            query += " `hform13a`.mastit = ? AND ";
        }
        if (form13.getTrixinell() != null && !form13.getTrixinell().isEmpty()){
            query += " `hform13a`.trixinell = ? AND ";
        }
        if (form13.getFinnoz() != null && !form13.getFinnoz().isEmpty()){
            query += " `hform13a`.finnoz = ? AND ";
        }
        if (form13.getSuriyee() != null && !form13.getSuriyee().isEmpty()){
            query += " `hform13a`.suriyee = ? AND ";
        }
        if (form13.getBoom() != null && !form13.getBoom().isEmpty()){
            query += " `hform13a`.boom = ? AND ";
        }
        if (form13.getBusad() != null && !form13.getBusad().isEmpty()){
            query += " `hform13a`.busad = ? AND ";
        }
        if (form13.getBugd() != null && !form13.getBugd().isEmpty()){
            query += " `hform13a`.bugd = ? AND ";
        }
        if (form13.getDel_flg() != null && !form13.getDel_flg().isEmpty()){
            query += " `hform13a`.del_flg = ? AND ";
        }
        if (form13.getCre_by() != null && !form13.getCre_by().isEmpty()){
            query += " `hform13a`.cre_by = ? AND ";
        }
        if (form13.getCre_at() != null && !form13.getCre_at().toString().isEmpty()){
            query += " `hform13a`.cre_at = ? AND ";
        }
        if (form13.getMod_by() != null && !form13.getMod_by().isEmpty()){
            query += " `hform13a`.mod_by = ? AND ";
        }
        if (form13.getMod_at() != null && !form13.getMod_at().toString().isEmpty()){
            query += " `hform13a`.mod_at = ? AND ";
        }
        if (form13.getNo_data() != null && !form13.getNo_data().isEmpty()){
            query += " `hform13a`.no_data = ? AND ";
        }
        if (form13.getRecordDate() != null && !form13.getRecordDate().toString().isEmpty()){
            query += " (`hform13a`.`record_date` BETWEEN ? AND ?) AND ";
        }
        if (form13.getCitycode() != null && !form13.getCitycode().isEmpty()){
            query += " `hform13a`.citycode = ? AND ";
        }
        if (form13.getSumcode() != null && !form13.getSumcode().isEmpty()){
            query += " `hform13a`.sumcode = ? AND ";
        }
        if (form13.getBag_code() != null && !form13.getBag_code().isEmpty()){
            query += " `hform13a`.bag_code = ? AND ";
        }
        query += " `hform13a`.`del_flg` = 'N'";
        query += " ORDER BY `hform13a`.`cre_at` DESC ";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query);

                if (form13.getUzleg_hiih() != null && !form13.getUzleg_hiih().isEmpty()){
                    statement.setString(stIndex++, form13.getUzleg_hiih());
                }

                if (form13.getHemjih_negj() != null && !form13.getHemjih_negj().isEmpty()){
                    statement.setString(stIndex++, form13.getHemjih_negj());
                }

                if (form13.getToo_hemjee() != null && !form13.getToo_hemjee().isEmpty()){
                    statement.setString(stIndex++, form13.getToo_hemjee());
                }
                if (form13.getDeejiin_too() != null && !form13.getDeejiin_too().isEmpty()){
                    statement.setString(stIndex++, form13.getDeejiin_too());
                }
                if (form13.getShinjilgee_hiisen() != null && !form13.getShinjilgee_hiisen().isEmpty()){
                    statement.setString(stIndex++, form13.getShinjilgee_hiisen());
                }
                if (form13.getShinjilsen_arga() != null && !form13.getShinjilsen_arga().isEmpty()){
                    statement.setString(stIndex++, form13.getShinjilsen_arga());
                }
                if (form13.getHergtseend_tohirson() != null && !form13.getHergtseend_tohirson().toString().isEmpty() && form13.getHergtseend_tohirson() != 0){
                    statement.setDouble(stIndex++, form13.getHergtseend_tohirson());
                }
                if (form13.getUstgasan() != null && !form13.getUstgasan().toString().isEmpty() && form13.getUstgasan() != 0){
                    statement.setDouble(stIndex++, form13.getUstgasan());
                }
                if (form13.getHaldbarguijuulelt() != null && !form13.getHaldbarguijuulelt().toString().isEmpty() && form13.getHaldbarguijuulelt() != 0){
                    statement.setDouble(stIndex++, form13.getHaldbarguijuulelt());
                }
                if (form13.getBrutsellyoz() != null && !form13.getBrutsellyoz().isEmpty()){
                    statement.setString(stIndex++, form13.getBrutsellyoz());
                }
                if (form13.getMastit() != null && !form13.getMastit().isEmpty()){
                    statement.setString(stIndex++, form13.getMastit());
                }
                if (form13.getTrixinell() != null && !form13.getTrixinell().isEmpty()){
                    statement.setString(stIndex++, form13.getTrixinell());
                }
                if (form13.getFinnoz() != null && !form13.getFinnoz().isEmpty()){
                    statement.setString(stIndex++, form13.getFinnoz());
                }
                if (form13.getSuriyee() != null && !form13.getSuriyee().isEmpty()){
                    statement.setString(stIndex++, form13.getSuriyee());
                }
                if (form13.getBoom() != null && !form13.getBoom().isEmpty()){
                    statement.setString(stIndex++, form13.getBoom());
                }
                if (form13.getBusad() != null && !form13.getBusad().isEmpty()){
                    statement.setString(stIndex++, form13.getBusad());
                }
                if (form13.getBugd() != null && !form13.getBugd().isEmpty()){
                    statement.setString(stIndex++, form13.getBugd());
                }
                if (form13.getDel_flg() != null && !form13.getDel_flg().isEmpty()){
                    statement.setString(stIndex++, form13.getDel_flg());
                }
                if (form13.getCre_by() != null && !form13.getCre_by().isEmpty()){
                    statement.setString(stIndex++, form13.getCre_by());
                }
                if (form13.getCre_at() != null && !form13.getCre_at().toString().isEmpty()){
                    statement.setDate(stIndex++, form13.getCre_at());
                }
                if (form13.getMod_by() != null && !form13.getMod_by().isEmpty()){
                    statement.setString(stIndex++, form13.getMod_by());
                }
                if (form13.getMod_at() != null && !form13.getMod_at().toString().isEmpty()){
                    statement.setDate(stIndex++, form13.getMod_at());
                }
                if (form13.getNo_data() != null && !form13.getNo_data().isEmpty()){
                    statement.setString(stIndex++, form13.getNo_data());
                }
                if (form13.getRecordDate() != null && form13.getSearchRecordDate() != null && !form13.getRecordDate().toString().isEmpty()){
                    statement.setDate(stIndex++, form13.getRecordDate());
                    statement.setDate(stIndex++, form13.getSearchRecordDate());
                }
                if (form13.getCitycode() != null && !form13.getCitycode().isEmpty()){
                    statement.setString(stIndex++, form13.getCitycode());
                }
                if (form13.getSumcode() != null && !form13.getSumcode().isEmpty()){
                    statement.setString(stIndex++, form13.getSumcode());
                }
                if (form13.getBag_code() != null && !form13.getBag_code().isEmpty()){
                    statement.setString(stIndex++, form13.getBag_code());
                }

                ResultSet resultSet = statement.executeQuery();

                form08EntitieList = new ArrayList<>();

                IComboService comboService = new ComboServiceImpl(boneCP);

                List<BasicDBObject> shargaType = comboService.getComboVals("SHARGA", "MN");
                int index=1;
                while (resultSet.next()) {
                    Form13aEntity entity = new Form13aEntity();

                    entity.setId(resultSet.getLong("id"));

                    entity.setUzleg_hiih(resultSet.getString("uzleg_hiih"));
                    entity.setHemjih_negj(resultSet.getString("hemjih_negj"));
                    entity.setToo_hemjee(resultSet.getString("too_hemjee"));
                    entity.setDeejiin_too(resultSet.getString("deejiin_too"));
                    entity.setShinjilgee_hiisen(resultSet.getString("shinjilgee_hiisen"));
                    entity.setShinjilsen_arga(resultSet.getString("shinjilsen_arga"));
                    entity.setHergtseend_tohirson(resultSet.getDouble("hergtseend_tohirson"));
                    entity.setUstgasan(resultSet.getDouble("ustgasan"));
                    entity.setHaldbarguijuulelt(resultSet.getDouble("haldbarguijuulelt"));
                    entity.setBrutsellyoz(resultSet.getString("brutsellyoz"));
                    entity.setMastit(resultSet.getString("mastit"));
                    entity.setTrixinell(resultSet.getString("trixinell"));
                    entity.setFinnoz(resultSet.getString("finnoz"));
                    entity.setSuriyee(resultSet.getString("suriyee"));
                    entity.setBoom(resultSet.getString("boom"));
                    entity.setBusad(resultSet.getString("busad"));
                    entity.setBugd(resultSet.getString("bugd"));
                    entity.setDel_flg(resultSet.getString("del_flg"));
                    entity.setCre_by(resultSet.getString("cre_by"));
                    entity.setCre_at(resultSet.getDate("cre_at"));
                    entity.setMod_by(resultSet.getString("mod_by"));
                    entity.setMod_at(resultSet.getDate("mod_at"));
                    entity.setNo_data(resultSet.getString("no_data"));
                    entity.setRecordDate(resultSet.getDate("record_date"));
                    entity.setCitycode(resultSet.getString("citycode"));
                    entity.setSumcode(resultSet.getString("sumcode"));
                    entity.setBag_code(resultSet.getString("bag_code"));

                    entity.put("shinjilsenarga", getTypeNer(shargaType, resultSet.getString("shinjilsen_arga")));
                    entity.put("index",index++);
                    form08EntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return form08EntitieList;
    }

    /**
     * Get name
     *
     * @param cmbType
     * @param mal_turul_mn
     * @return
     */
    private String getTypeNer(List<BasicDBObject> cmbType, String mal_turul_mn) {
        for (BasicDBObject obj :
                cmbType) {
            if (obj.get("id") != null && mal_turul_mn != null && obj.getString("id").equalsIgnoreCase(mal_turul_mn)) {
                return obj.getString("name");
            }
        }

        return "";
    }

    @Override
    public ErrorEntity updateData(Form13aEntity form13) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (form13 == null) {
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        query = "UPDATE `hospital`.`hform13a` " +
                "SET     ";

        if (form13.getUzleg_hiih() != null && !form13.getUzleg_hiih().isEmpty()){
            query += " `hform13a`.uzleg_hiih = ?, ";
        }

        if (form13.getHemjih_negj() != null && !form13.getHemjih_negj().isEmpty()){
            query += " `hform13a`.hemjih_negj = ?, ";
        }

        if (form13.getToo_hemjee() != null && !form13.getToo_hemjee().isEmpty()){
            query += " `hform13a`.too_hemjee = ?, ";
        }
        if (form13.getDeejiin_too() != null && !form13.getDeejiin_too().isEmpty()){
            query += " `hform13a`.deejiin_too = ?, ";
        }
        if (form13.getShinjilgee_hiisen() != null && !form13.getShinjilgee_hiisen().isEmpty()){
            query += " `hform13a`.shinjilgee_hiisen = ?, ";
        }
        if (form13.getShinjilsen_arga() != null && !form13.getShinjilsen_arga().isEmpty()){
            query += " `hform13a`.shinjilsen_arga = ?, ";
        }
        if (form13.getHergtseend_tohirson() != null && !form13.getHergtseend_tohirson().toString().isEmpty()){
            query += " `hform13a`.hergtseend_tohirson = ?, ";
        }
        if (form13.getUstgasan() != null && !form13.getUstgasan().toString().isEmpty()){
            query += " `hform13a`.ustgasan = ?, ";
        }
        if (form13.getHaldbarguijuulelt() != null && !form13.getHaldbarguijuulelt().toString().isEmpty()){
            query += " `hform13a`.haldbarguijuulelt = ?, ";
        }
        if (form13.getBrutsellyoz() != null && !form13.getBrutsellyoz().isEmpty()){
            query += " `hform13a`.brutsellyoz = ?, ";
        }
        if (form13.getMastit() != null && !form13.getMastit().isEmpty()){
            query += " `hform13a`.mastit = ?, ";
        }
        if (form13.getTrixinell() != null && !form13.getTrixinell().isEmpty()){
            query += " `hform13a`.trixinell = ?, ";
        }
        if (form13.getFinnoz() != null && !form13.getFinnoz().isEmpty()){
            query += " `hform13a`.finnoz = ?, ";
        }
        if (form13.getSuriyee() != null && !form13.getSuriyee().isEmpty()){
            query += " `hform13a`.suriyee = ?, ";
        }
        if (form13.getBoom() != null && !form13.getBoom().isEmpty()){
            query += " `hform13a`.boom = ?, ";
        }
        if (form13.getBusad() != null && !form13.getBusad().isEmpty()){
            query += " `hform13a`.busad = ?, ";
        }
        if (form13.getBugd() != null && !form13.getBugd().isEmpty()){
            query += " `hform13a`.bugd = ?, ";
        }
        if (form13.getDel_flg() != null && !form13.getDel_flg().isEmpty()){
            query += " `hform13a`.del_flg = ?, ";
        }
        if (form13.getCre_by() != null && !form13.getCre_by().isEmpty()){
            query += " `hform13a`.cre_by = ?, ";
        }
        if (form13.getCre_at() != null && !form13.getCre_at().toString().isEmpty()){
            query += " `hform13a`.cre_at = ?, ";
        }
        if (form13.getNo_data() != null && !form13.getNo_data().isEmpty()){
            query += " `hform13a`.no_data = ?, ";
        }
        if (form13.getRecordDate() != null && !form13.getRecordDate().toString().isEmpty()){
            query += " `hform13a`.record_date = ?, ";
        }
        if (form13.getCitycode() != null && !form13.getCitycode().isEmpty()){
            query += " `hform13a`.citycode = ?, ";
        }
        if (form13.getSumcode() != null && !form13.getSumcode().isEmpty()){
            query += " `hform13a`.sumcode = ?, ";
        }
        if (form13.getBag_code() != null && !form13.getBag_code().isEmpty()){
            query += " `hform13a`.bag_code = ?, ";
        }


        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";
        query += " WHERE  ID   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (form13.getUzleg_hiih() != null && !form13.getUzleg_hiih().isEmpty()){
                    statement.setString(stIndex++, form13.getUzleg_hiih());
                }

                if (form13.getHemjih_negj() != null && !form13.getHemjih_negj().isEmpty()){
                    statement.setString(stIndex++, form13.getHemjih_negj());
                }

                if (form13.getToo_hemjee() != null && !form13.getToo_hemjee().isEmpty()){
                    statement.setString(stIndex++, form13.getToo_hemjee());
                }
                if (form13.getDeejiin_too() != null && !form13.getDeejiin_too().isEmpty()){
                    statement.setString(stIndex++, form13.getDeejiin_too());
                }
                if (form13.getShinjilgee_hiisen() != null && !form13.getShinjilgee_hiisen().isEmpty()){
                    statement.setString(stIndex++, form13.getShinjilgee_hiisen());
                }
                if (form13.getShinjilsen_arga() != null && !form13.getShinjilsen_arga().isEmpty()){
                    statement.setString(stIndex++, form13.getShinjilsen_arga());
                }
                if (form13.getHergtseend_tohirson() != null && !form13.getHergtseend_tohirson().toString().isEmpty()){
                    statement.setDouble(stIndex++, form13.getHergtseend_tohirson());
                }
                if (form13.getUstgasan() != null && !form13.getUstgasan().toString().isEmpty()){
                    statement.setDouble(stIndex++, form13.getUstgasan());
                }
                if (form13.getHaldbarguijuulelt() != null && !form13.getHaldbarguijuulelt().toString().isEmpty()){
                    statement.setDouble(stIndex++, form13.getHaldbarguijuulelt());
                }
                if (form13.getBrutsellyoz() != null && !form13.getBrutsellyoz().isEmpty()){
                    statement.setString(stIndex++, form13.getBrutsellyoz());
                }
                if (form13.getMastit() != null && !form13.getMastit().isEmpty()){
                    statement.setString(stIndex++, form13.getMastit());
                }
                if (form13.getTrixinell() != null && !form13.getTrixinell().isEmpty()){
                    statement.setString(stIndex++, form13.getTrixinell());
                }
                if (form13.getFinnoz() != null && !form13.getFinnoz().isEmpty()){
                    statement.setString(stIndex++, form13.getFinnoz());
                }
                if (form13.getSuriyee() != null && !form13.getSuriyee().isEmpty()){
                    statement.setString(stIndex++, form13.getSuriyee());
                }
                if (form13.getBoom() != null && !form13.getBoom().isEmpty()){
                    statement.setString(stIndex++, form13.getBoom());
                }
                if (form13.getBusad() != null && !form13.getBusad().isEmpty()){
                    statement.setString(stIndex++, form13.getBusad());
                }
                if (form13.getBugd() != null && !form13.getBugd().isEmpty()){
                    statement.setString(stIndex++, form13.getBugd());
                }
                if (form13.getDel_flg() != null && !form13.getDel_flg().isEmpty()){
                    statement.setString(stIndex++, form13.getDel_flg());
                }
                if (form13.getCre_by() != null && !form13.getCre_by().isEmpty()){
                    statement.setString(stIndex++, form13.getCre_by());
                }
                if (form13.getCre_at() != null && !form13.getCre_at().toString().isEmpty()){
                    statement.setDate(stIndex++, form13.getCre_at());
                }
                if (form13.getNo_data() != null && !form13.getNo_data().isEmpty()){
                    statement.setString(stIndex++, form13.getNo_data());
                }
                if (form13.getRecordDate() != null && !form13.getRecordDate().toString().isEmpty()){
                    statement.setDate(stIndex++, form13.getRecordDate());
                }
                if (form13.getCitycode() != null && !form13.getCitycode().isEmpty()){
                    statement.setString(stIndex++, form13.getCitycode());
                }
                if (form13.getSumcode() != null && !form13.getSumcode().isEmpty()){
                    statement.setString(stIndex++, form13.getSumcode());
                }
                if (form13.getBag_code() != null && !form13.getBag_code().isEmpty()){
                    statement.setString(stIndex++, form13.getBag_code());
                }

                statement.setDate(stIndex++, form13.getMod_at());
                statement.setString(stIndex++, form13.getMod_by());
                statement.setLong(stIndex++, form13.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                    errObj.setErrText("Бичлэг амжилттай засагдлаа.");
                    errObj.setErrDesc("Бичлэг амжилттай засагдлаа.");
                    return errObj;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }


        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1061));
        errObj.setErrText("Амжилтгүй боллоо. Дахин оролдоно уу.");
        errObj.setErrDesc("Амжилтгүй боллоо. Дахин оролдоно уу.");

        return errObj;
    }

    @Override
    public List<Form13aEntity> getPieChartData(Form13aEntity Form13aEntity) throws SQLException {
        int stIndex = 1;
        List<Form13aEntity> form08EntitieList = null;

        /*String query_sel = "SELECT `hform08`.`id`,  " +
                "    `hform08`.`aimag`,  " +
                "    `hform08`.`sum`,  " +
                "    SUM(`hform08`.`zartsuulsan_hemjee_mn`) as zartsuulsan_hemjee,  " +
                "    c.cityname, c.cityname_en,  " +
                "    `h_sum`.sumname, `h_sum`.sumname_en  " +
                "FROM `hospital`.`hform08`  " +
                "INNER JOIN `h_city` AS c  " +
                "ON c.code = `hform08`.aimag  " +
                "INNER JOIN `h_sum`  " +
                "ON `h_sum`.sumcode = `hform08`.sum  " +
                "WHERE `hform08`.`delflg` = 'N' and `hform08`.`actflg` = 'Y' and `hform08`.`aimag` = ?  " +
                "GROUP BY `hform08`.`sum` ";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                *//*if (Form13aEntity.getAimag() != null && !Form13aEntity.getAimag().isEmpty()) {
                    statement.setString(stIndex++, Form13aEntity.getAimag());
                }*//*

                ResultSet resultSet = statement.executeQuery();

                form08EntitieList = new ArrayList<>();

                while (resultSet.next()) {
                    Form13aEntity entity = new Form13aEntity();

                    *//*entity.setId(resultSet.getLong("id"));
                    entity.setZartsuulsanHemjee(resultSet.getString("zartsuulsan_hemjee"));
                    entity.setZartsuulsanHemjee_en(resultSet.getString("zartsuulsan_hemjee"));
                    entity.setAimag(resultSet.getString("aimag"));
                    entity.setSum(resultSet.getString("sum"));
                    entity.setAimagNer(resultSet.getString("cityname"));
                    entity.setSumNer(resultSet.getString("sumname"));*//*

                    form08EntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }*/

        return form08EntitieList;
    }//


    @Override
    public ErrorEntity exportReport(String file, Form13aEntity entity) throws SQLException {
        ErrorEntity errorEntity = null;
        String fileName = "hform13a";

        try (Connection connection = boneCP.getConnection()){
            String JRXML_PATH = Config.getJrxmlPath();
            String REPORT_PATH = Config.getReportPath();

            try {
                Map parameters = new HashMap();

                String reportTitle = "МАЛ, АМЬТАН, ТҮҮХИЙ ЭД БҮТЭЭГДЭХҮҮН, МАЛЫН ТЭЖЭЭЛИЙН ҮЗЛЭГ ШИНЖИЛГЭЭНИЙ $$YEAR$$ ОНЫ $$SEASON$$ -Р УЛИРЛЫН МЭДЭЭ",
                        currentYear = "$$YEAR$$ он №....",
                        printDate = "$$YEAR$$ оны $$MONTH$$ сарын $$DAY$$";

                Calendar now = Calendar.getInstance();

                int year = now.get(Calendar.YEAR);
                int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
                int day = now.get(Calendar.DAY_OF_MONTH);

                printDate = printDate.replace("$$YEAR$$", String.valueOf(year));
                printDate = printDate.replace("$$MONTH$$", String.valueOf(month));
                printDate = printDate.replace("$$DAY$$", String.valueOf(day));

                currentYear = currentYear.replace("$$YEAR$$", String.valueOf(year));

                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(new Date(entity.getRecordDate().getTime()));

                reportTitle = reportTitle.replace("$$YEAR$$", String.valueOf(calendar1.get(Calendar.YEAR)));

                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(new Date(entity.getSearchRecordDate().getTime()));

                if (calendar2.get(Calendar.MONTH) < 4){
                    reportTitle = reportTitle.replace("$$SEASON$$", "1");
                }
                else if (calendar2.get(Calendar.MONTH) < 7){
                    reportTitle = reportTitle.replace("$$SEASON$$", "2");
                }
                else if (calendar2.get(Calendar.MONTH) < 10){
                    reportTitle = reportTitle.replace("$$SEASON$$", "3");
                }
                else if (calendar2.get(Calendar.MONTH) <= 12){
                    reportTitle = reportTitle.replace("$$SEASON$$", "4");
                }

                parameters.put("start_date", entity.getRecordDate());
                parameters.put("end_date", entity.getSearchRecordDate());
                parameters.put("citycode", (entity.getCitycode() != null && !entity.getCitycode().isEmpty()) ? entity.getCitycode() : null);
                parameters.put("sumcode", (entity.getSumcode() != null && !entity.getSumcode().isEmpty()) ? entity.getSumcode() : null);
                parameters.put("currentyear", currentYear);

                ICityService cityService = new CityServiceImpl(boneCP);
                City tmpCity = new City();
                tmpCity.setCode(entity.getCitycode());
                List<City> clist = cityService.selectAll(tmpCity);

                if (clist != null && clist.size() == 1) {
                    parameters.put("aimagner", clist.get(0).getCityname());
                } else {
                    parameters.put("aimagner", (entity.getCityName() == null) ? "" : entity.getCityName());
                }
                ISumService sumService = new SumServiceImpl(boneCP);
                Sum tmpSum = new Sum();
                tmpSum.setCitycode(entity.getCitycode());
                tmpSum.setSumcode(entity.getSumcode());
                List<Sum> sumList = sumService.selectAll(tmpSum);

                if (sumList != null && sumList.size() == 1){
                    parameters.put("sumner", sumList.get(0).getSumname());
                }
                else{
                    parameters.put("sumner", (entity.getSumName() == null) ? "" : entity.getSumName());
                }
                parameters.put("reporttitle", reportTitle);
                parameters.put("printdate", printDate);
                parameters.put("printdate", printDate);
                parameters.put("creby", (entity.getCre_by() != null && !entity.getCre_by().isEmpty()) ? entity.getCre_by() : null);
                System.out.println(parameters);
                PdfExcelUtil.generateReport(file, JRXML_PATH, REPORT_PATH, fileName, parameters, connection);

                System.out.println("File Generated");
            }catch (Exception ex){
                ex.printStackTrace();
            }
            finally {
                if (connection != null)connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1001));
            if(file.equals("pdf")){
                errorEntity.setErrText(fileName + ".pdf");
            }else if(file.equals("xls")){
                errorEntity.setErrText(fileName + ".xls");
            }
        }
        return errorEntity;
    }

    @Override
    public List<BasicDBObject> getColumnChartData(Form13aEntity Form13aEntity) throws SQLException {
        List<BasicDBObject> form08EntitieList = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        //dateFormat.parse(dv).getTime()
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(Form13aEntity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(Form13aEntity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)){
            return null;
        }



        String query_sel = "SELECT `mal_turul_mn`, COUNT(`id`) AS cnt, `hform08`.`aimag`, `hform08`.`sum`  " +
                "FROM `hospital`.`hform08`  " +
                "WHERE `hform08`.`delflg` = 'N' and `hform08`.`actflg` = 'Y' and `hform08`.`aimag` = ?  " +
                "and `hform08`.`sum` = ? and (`recorddate` BETWEEN ? AND ?)" +
                "GROUP BY `hform08`.`mal_turul_mn` ";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                /*statement.setString(1, Form13aEntity.getAimag());
                statement.setString(2, Form13aEntity.getSum());*/
                statement.setDate(3, Form13aEntity.getRecordDate());
                statement.setDate(4, Form13aEntity.getSearchRecordDate());

                ResultSet resultSet = statement.executeQuery();

                form08EntitieList = new ArrayList<>();

                IComboService comboService = new ComboServiceImpl(boneCP);
                List<BasicDBObject> cmbType = comboService.getComboVals("MAL", "MN");

                while (resultSet.next()) {
                    BasicDBObject entity = new BasicDBObject();

                    entity.put("malturul", resultSet.getString("mal_turul_mn"));
                    entity.put("count", resultSet.getString("cnt"));
                    entity.put("aimag", resultSet.getString("aimag"));
                    entity.put("sum", resultSet.getString("sum"));

                    form08EntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return form08EntitieList;
    }

    @Override
    public BasicDBObject getColHighChartData(Form13aEntity entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();
        List<String> categories = new ArrayList<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }

        try {

            IComboService comboService = new ComboServiceImpl(boneCP);

            List<BasicDBObject> cmbType = comboService.getComboNames("MALUVCHIN", "MN");
            System.out.println(cmbType);
            //Saraar n haruulah
            int month = calendar1.get(Calendar.MONTH) + 1;
            int year1 = calendar1.get(Calendar.YEAR);
            int year2 = calendar2.get(Calendar.YEAR);

            try (Connection connection = boneCP.getConnection()) {
                String query_sel = "SELECT `too_hemjee`, sum(`brutsellyoz`) AS brutsellyoz,  sum(`mastit`) AS mastit, sum('trixinell') as trixinell," +
                        "sum(`finnoz`) AS finnoz,  sum(`suriyee`) AS suriyee, sum(`boom`) AS boom, sum(`busad`) AS busad, `hform13a`.`citycode`, `hform13a`.`sumcode`  " +
                        "FROM `hospital`.`hform13a`  " +
                        "WHERE   ";

                if (entity != null && entity.getCitycode() != null &&!entity.getCitycode().isEmpty()) {
                    query_sel += " `hform13a`.`citycode` = ? AND ";
                }
                if (entity != null && entity.getSumcode() != null && !entity.getSumcode().isEmpty()) {
                    query_sel += " `hform13a`.`sumcode` = ? AND ";
                }
                if (entity != null && entity.getShinjilsen_arga() != null && !entity.getShinjilsen_arga().isEmpty()) {
                    query_sel += " `hform13a`.`shinjilsen_arga` = ? AND ";
                }
                if (entity != null && !entity.getUzleg_hiih().isEmpty()) {
                    query_sel += " `hform13a`.`uzleg_hiih` = ? AND ";
                }
                if (entity != null && !entity.getDel_flg().isEmpty()) {
                    query_sel += " `hform13a`.`del_flg` = ? AND ";
                }
                if (entity != null && !entity.getCre_by().isEmpty()) {
                    query_sel += " `hform13a`.`cre_by` = ? AND ";
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                    query_sel += " (`record_date` BETWEEN ? AND ?)";
                }

                HashMap<String, List<Double>> resultArray = new HashMap<>();


                do {
                    //do process
                    categories.add(year1 + "-" + month);

                    try {
                        PreparedStatement statement = connection.prepareStatement(query_sel);

                        java.sql.Date stdate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-01 00:00:00").getTime());

                        java.sql.Date enddate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-31 00:00:00").getTime());

                        if (month == 12 && year1 < year2) {
                            month = 1;
                            year1++;
                        } else {
                            month++;
                        }

                        int stindex = 1;

                        if (entity != null && entity.getCitycode() != null  && !entity.getCitycode().isEmpty()) {
                            statement.setString(stindex++, entity.getCitycode());
                        }
                        if (entity != null && entity.getSumcode() != null  && !entity.getSumcode().isEmpty()) {
                            statement.setString(stindex++, entity.getSumcode());
                        }
                        if (entity != null && entity.getShinjilsen_arga() != null  && !entity.getShinjilsen_arga().isEmpty()) {
                            statement.setString(stindex++, entity.getShinjilsen_arga());
                        }
                        if (entity != null && !entity.getUzleg_hiih().isEmpty()) {
                            statement.setString(stindex++, entity.getUzleg_hiih());
                        }
                        if (entity != null && !entity.getDel_flg().isEmpty()) {
                            statement.setString(stindex++, entity.getDel_flg());
                        }
                        if (entity != null && !entity.getCre_by().isEmpty()) {
                            statement.setString(stindex++, entity.getCre_by());
                        }
                        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                            statement.setDate(stindex++, stdate);
                            statement.setDate(stindex++, enddate);
                        }


                        ResultSet resultSet = statement.executeQuery();

                        HashMap<String, String> hashMap = new HashMap<>();

                        while (resultSet.next()) {
                            String brutsellyoz = resultSet.getString("brutsellyoz");
                            String mastit = resultSet.getString("mastit");
                            String trixinell = resultSet.getString("trixinell");
                            String finnoz = resultSet.getString("finnoz");
                            String suriyee = resultSet.getString("suriyee");
                            String boom = resultSet.getString("boom");
                            String busad = resultSet.getString("busad");
                            hashMap.put("brutsellyoz", brutsellyoz);
                            hashMap.put("mastit", mastit);
                            hashMap.put("trixinell", trixinell);
                            hashMap.put("finnoz", finnoz);
                            hashMap.put("suriyee", suriyee);
                            hashMap.put("boom", boom);
                            hashMap.put("busad", busad);
                        }//end while

                        List<Double> list = new ArrayList<>();
                        for (int i = 0; i < cmbType.size(); i++) {
                            BasicDBObject obj = cmbType.get(i);

                            if (obj.get("data") == null){
                                //new record
                                list = new ArrayList<>();
                                String val = hashMap.get(obj.getString("id"));
                                if (val == null) list.add(0.0);
                                else if (val.isEmpty()) list.add(0.0);
                                else list.add(Double.parseDouble(val));
                                obj.put("data", list);
                            }else{
                                //already exist

                                list = (List<Double>) obj.get("data");
                                String val = hashMap.get(obj.getString("id"));
                                if (val == null) list.add(0.0);
                                else if (val.isEmpty()) list.add(0.0);
                                else list.add(Double.parseDouble(val));
                            }
                        }

                        if (statement != null) statement.close();
                        if (resultSet != null) resultSet.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();

                        break;
                    }

                    ///
                } while ((month <= calendar2.get(Calendar.MONTH) + 1 && year1 <= year2) || year1 < year2);

                basicDBObject.put("categories", categories);
                basicDBObject.put("series", cmbType);

                connection.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }

    @Override
    public BasicDBObject getPieHighChartData(Form13aEntity entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }

        String query_sel = "SELECT `too_hemjee`, sum(`brutsellyoz`) AS brutsellyoz,  sum(`mastit`) AS mastit, sum('trixinell') as trixinell," +
                "sum(`finnoz`) AS finnoz,  sum(`suriyee`) AS suriyee, sum(`boom`) AS boom, sum(`busad`) AS busad, `hform13a`.`citycode`, `hform13a`.`sumcode`  " +
                "FROM `hospital`.`hform13a`  " +
                "WHERE   ";

        if (entity != null && entity.getCitycode() != null &&!entity.getCitycode().isEmpty()) {
            query_sel += " `hform13a`.`citycode` = ? AND ";
        }
        if (entity != null && entity.getSumcode() != null && !entity.getSumcode().isEmpty()) {
            query_sel += " `hform13a`.`sumcode` = ? AND ";
        }
        if (entity != null && entity.getShinjilsen_arga() != null && !entity.getShinjilsen_arga().isEmpty()) {
            query_sel += " `hform13a`.`shinjilsen_arga` = ? AND ";
        }
        if (entity != null && !entity.getUzleg_hiih().isEmpty()) {
            query_sel += " `hform13a`.`uzleg_hiih` = ? AND ";
        }
        if (entity != null && !entity.getDel_flg().isEmpty()) {
            query_sel += " `hform13a`.`del_flg` = ? AND ";
        }
        if (entity != null && !entity.getCre_by().isEmpty()) {
            query_sel += " `hform13a`.`cre_by` = ? AND ";
        }
        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
            query_sel += " (`record_date` BETWEEN ? AND ?)";
        }

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                int stindex = 1;

                if (entity != null && entity.getCitycode() != null  && !entity.getCitycode().isEmpty()) {
                    statement.setString(stindex++, entity.getCitycode());
                }
                if (entity != null && entity.getSumcode() != null  && !entity.getSumcode().isEmpty()) {
                    statement.setString(stindex++, entity.getSumcode());
                }
                if (entity != null && entity.getShinjilsen_arga() != null  && !entity.getShinjilsen_arga().isEmpty()) {
                    statement.setString(stindex++, entity.getShinjilsen_arga());
                }
                if (entity != null && !entity.getUzleg_hiih().isEmpty()) {
                    statement.setString(stindex++, entity.getUzleg_hiih());
                }
                if (entity != null && !entity.getDel_flg().isEmpty()) {
                    statement.setString(stindex++, entity.getDel_flg());
                }
                if (entity != null && !entity.getCre_by().isEmpty()) {
                    statement.setString(stindex++, entity.getCre_by());
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                    statement.setDate(stindex++, entity.getRecordDate());
                    statement.setDate(stindex++, entity.getSearchRecordDate());
                }
                ResultSet resultSet = statement.executeQuery();

                List<BasicDBObject> list = new ArrayList<>();

                while (resultSet.next()) {
                    BasicDBObject entity1 = new BasicDBObject();
                    BasicDBObject entity2 = new BasicDBObject();
                    BasicDBObject entity3 = new BasicDBObject();
                    BasicDBObject entity4 = new BasicDBObject();
                    BasicDBObject entity5 = new BasicDBObject();
                    BasicDBObject entity6 = new BasicDBObject();
                    BasicDBObject entity7 = new BasicDBObject();

                    entity1.put("name", "Бруцеллёз");
                    entity1.put("y", resultSet.getDouble("brutsellyoz"));
                    entity2.put("name", "Мастит");
                    entity2.put("y", resultSet.getDouble("mastit"));
                    entity7.put("name", "Трихинеллез");
                    entity7.put("y", resultSet.getDouble("trixinell"));
                    entity3.put("name", "Финноз");
                    entity3.put("y", resultSet.getDouble("finnoz"));
                    entity4.put("name", "Сүрьеэ");
                    entity4.put("y", resultSet.getDouble("suriyee"));
                    entity5.put("name", "Боом");
                    entity5.put("y", resultSet.getDouble("boom"));
                    entity6.put("name", "Бусад");
                    entity6.put("y", resultSet.getDouble("busad"));
                    list.add(entity1);
                    list.add(entity2);
                    list.add(entity7);
                    list.add(entity3);
                    list.add(entity4);
                    list.add(entity5);
                    list.add(entity6);
            }

                basicDBObject.put("data", list);

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }
}
