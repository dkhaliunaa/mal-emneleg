package com.hospital.app;


import com.enumclass.ErrorType;
import com.jolbox.bonecp.BoneCP;
import com.model.User;
import com.model.hos.ErrorEntity;
import com.model.hos.TarkhvarDataEntity;
import com.mongodb.BasicDBObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Tilyeujan on 9/2/2017.
 */
public class TarkhvarMedeeFormServiceImpl implements ITarkhvarMedeeFormService {
    /**
     * Objects
     */
    private BoneCP boneCP;
    private ErrorEntity errorEntity;


    public TarkhvarMedeeFormServiceImpl(BoneCP boneCP) {
        this.boneCP = boneCP;
    }

    @Override
    public ErrorEntity insertNewData(TarkhvarDataEntity entity) throws SQLException {
        PreparedStatement statement = null;

        if (entity == null) {
            errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));

            errorEntity.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errorEntity.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            
            return errorEntity;
        }

        String query_login = "INSERT INTO `hform01_tarhvar`(`hformkey`, `citycode`, `sumcode`, `bagcode`, " +
                "`malchinner`, `baigal_haldvariin`, `baktsinii`, `ds_hiiseneseh`, `ds_bag_gishuun_ners`, " +
                "`ds_ur_dun`, `zuvlomj`, `huchin_zuil_ognoo`, `urjliin_mal_hud`, `busad_mal_hud`, `bakcin_targu`, " +
                "`otor_nuudel`, `hun_sh_hod`, `teever_sh_hod`, `tabiul_mal_avsan`, `belcheer_neg`, `hudag_us`, " +
                "`busad_huch_zuil`, `shaltgaan_todgu`, `malchin_uuruu`, `sahalt_ail`, `mal_emneleg`, `uzleg_tandalt`, " +
                "`tandalt_shinjilgee`, `busad`, `tusgaarlasan_eseh`, `tusgaarlasan_ognoo`, `tus_horoo_bairlal`, " +
                "`delflg`, `actflg`, `mod_at`, `mod_by`, `cre_at`, `cre_by`, `record_date`) " +
                "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        try (Connection connection = boneCP.getConnection()) {
            try{
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, (entity.get(entity.HFORMKEY) != null) ? entity.getHformkey().trim() : "");
                statement.setString(2, (entity.get(entity.CITYCODE) != null) ? entity.getCitycode().trim() : "");
                statement.setString(3, (entity.get(entity.SUMCODE) != null) ? entity.getSumcode().trim() : "");
                statement.setString(4, (entity.get(entity.BAGCODE) != null) ? entity.getBagcode().trim() : "");
                statement.setString(5, (entity.get(entity.MALCHINNER) != null) ? entity.getMalchinner().trim() : "");
                statement.setString(6, (entity.get(entity.BAIGAL_HALDVARIIN) != null) ? entity.getBaigal_haldvariin().trim() : "");
                statement.setString(7, (entity.get(entity.BAKTSINII) != null) ? entity.getBaktsinii().trim() : "");
                statement.setString(8, (entity.get(entity.DS_HIISENESEH) != null) ? entity.getDs_hiiseneseh().trim() : "");
                statement.setString(9, (entity.get(entity.DS_BAG_GISHUUN_NERS) != null) ? entity.getDs_bag_gishuun_ners().trim() : "");
                statement.setString(10, (entity.get(entity.DS_UR_DUN) != null) ? entity.getDs_ur_dun().trim() : "");
                statement.setString(11, (entity.get(entity.ZUVLOMJ) != null) ? entity.getZuvlomj().trim() : "");
                statement.setString(12, (entity.get(entity.HUCHIN_ZUIL_OGNOO) != null) ? entity.getHuchin_zuil_ognoo().trim() : "");
                statement.setString(13, (entity.get(entity.URJLIIN_MAL_HUD) != null) ? entity.getUrjliin_mal_hud().trim() : "");
                statement.setString(14, (entity.get(entity.BUSAD_MAL_HUD) != null) ? entity.getBusad_mal_hud().trim() : "");
                statement.setString(15, (entity.get(entity.BAKCIN_TARGU) != null) ? entity.getBakcin_targu().trim() : "");
                statement.setString(16, (entity.get(entity.OTOR_NUUDEL) != null) ? entity.getOtor_nuudel().trim() : "");
                statement.setString(17, (entity.get(entity.HUN_SH_HOD) != null) ? entity.getHun_sh_hod().trim() : "");
                statement.setString(18, (entity.get(entity.TEEVER_SH_HOD) != null) ? entity.getTeever_sh_hod().trim() : "");
                statement.setString(19, (entity.get(entity.TABIUL_MAL_AVSAN) != null) ? entity.getTabiul_mal_avsan().trim() : "");
                statement.setString(20, (entity.get(entity.BELCHEER_NEG) != null) ? entity.getBelcheer_neg().trim() : "");
                statement.setString(21, (entity.get(entity.HUDAG_US) != null) ? entity.getHudag_us().trim() : "");
                statement.setString(22, (entity.get(entity.BUSAD_HUCH_ZUIL) != null) ? entity.getBusad_huch_zuil().trim() : "");
                statement.setString(23, (entity.get(entity.SHALTGAAN_TODGU) != null) ? entity.getShaltgaan_todgu().trim() : "");
                statement.setString(24, (entity.get(entity.MALCHIN_UURUU) != null) ? entity.getMalchin_uuruu().trim() : "");
                statement.setString(25, (entity.get(entity.SAHALT_AIL) != null) ? entity.getSahalt_ail().trim() : "");
                statement.setString(26, (entity.get(entity.MAL_EMNELEG) != null) ? entity.getMal_emneleg().trim() : "");
                statement.setString(27, (entity.get(entity.UZLEG_TANDALT) != null) ? entity.getUzleg_tandalt().trim() : "");
                statement.setString(28, (entity.get(entity.TANDALT_SHINJILGEE) != null) ? entity.getTandalt_shinjilgee().trim() : "");
                statement.setString(29, (entity.get(entity.BUSAD) != null) ? entity.getBusad().trim() : "");
                statement.setString(30, (entity.get(entity.TUSGAARLASAN_ESEH) != null) ? entity.getTusgaarlasan_eseh().trim() : "");
                statement.setDate(31, entity.getTusgaarlasan_ognoo());
                statement.setString(32, (entity.get(entity.TUS_HOROO_BAIRLAL) != null) ? entity.getTus_horoo_bairlal().trim() : "");
                statement.setString(33, (entity.get(entity.DELFLG) != null) ? entity.getDelflg().trim() : "");
                statement.setString(34, (entity.get(entity.ACTFLG) != null) ? entity.getActflg().trim() : "");
                statement.setDate(35, entity.getMod_at());
                statement.setString(36, (entity.get(entity.MOD_BY) != null) ? entity.getMod_by().trim() : "");
                statement.setDate(37, entity.getCre_at());
                statement.setString(38, (entity.get(entity.CRE_BY) != null) ? entity.getCre_by().trim() : "");
                statement.setDate(39, entity.getRecord_date());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1){
                    connection.commit();

                    errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));

                    errorEntity.setErrText("Бичлэг амжилттай хадгалагдлаа.");
                    errorEntity.setErrDesc("Бичлэг амжилттай хадгалагдлаа.");

                    return errorEntity;
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }finally {
                if (statement != null)statement.close();
                connection.close();
            }

        }catch (Exception ex){

        }

        errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1057));
        errorEntity.setErrText("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");
        errorEntity.setErrDesc("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");

        return errorEntity;
    }

    @Override
    public List<TarkhvarDataEntity> getListData(TarkhvarDataEntity entity) throws SQLException {
        int stIndex = 1;
        int index = 1;
        List<TarkhvarDataEntity> entityList = null;

        String query = "SELECT `hform01_tarhvar`.`id`, " +
                "    `hform01_tarhvar`.`hformkey`, " +
                "    `hform01_tarhvar`.`citycode`, " +
                "    `hform01_tarhvar`.`sumcode`, " +
                "    `hform01_tarhvar`.`bagcode`, " +
                "    `hform01_tarhvar`.`malchinner`, " +
                "    `hform01_tarhvar`.`baigal_haldvariin`, " +
                "    `hform01_tarhvar`.`baktsinii`, " +
                "    `hform01_tarhvar`.`ds_hiiseneseh`, " +
                "    `hform01_tarhvar`.`ds_bag_gishuun_ners`, " +
                "    `hform01_tarhvar`.`ds_ur_dun`, " +
                "    `hform01_tarhvar`.`zuvlomj`, " +
                "    `hform01_tarhvar`.`huchin_zuil_ognoo`, " +
                "    `hform01_tarhvar`.`urjliin_mal_hud`, " +
                "    `hform01_tarhvar`.`busad_mal_hud`, " +
                "    `hform01_tarhvar`.`bakcin_targu`, " +
                "    `hform01_tarhvar`.`otor_nuudel`, " +
                "    `hform01_tarhvar`.`hun_sh_hod`, " +
                "    `hform01_tarhvar`.`teever_sh_hod`, " +
                "    `hform01_tarhvar`.`tabiul_mal_avsan`, " +
                "    `hform01_tarhvar`.`belcheer_neg`, " +
                "    `hform01_tarhvar`.`hudag_us`, " +
                "    `hform01_tarhvar`.`busad_huch_zuil`, " +
                "    `hform01_tarhvar`.`shaltgaan_todgu`, " +
                "    `hform01_tarhvar`.`malchin_uuruu`, " +
                "    `hform01_tarhvar`.`sahalt_ail`, " +
                "    `hform01_tarhvar`.`mal_emneleg`, " +
                "    `hform01_tarhvar`.`uzleg_tandalt`, " +
                "    `hform01_tarhvar`.`tandalt_shinjilgee`, " +
                "    `hform01_tarhvar`.`busad`, " +
                "    `hform01_tarhvar`.`tusgaarlasan_eseh`, " +
                "    `hform01_tarhvar`.`tusgaarlasan_ognoo`, " +
                "    `hform01_tarhvar`.`tus_horoo_bairlal`, " +
                "    `hform01_tarhvar`.`delflg`, " +
                "    `hform01_tarhvar`.`actflg`, " +
                "    `hform01_tarhvar`.`mod_at`, " +
                "    `hform01_tarhvar`.`mod_by`, " +
                "    `hform01_tarhvar`.`cre_at`, " +
                "    `hform01_tarhvar`.`cre_by`, " +
                "    `hform01_tarhvar`.`record_date`, " +
                "`h_sum`.sumname, " +
                "`h_sum`.sumname_en, " +
                "`h_baghoroo`.horooname, " +
                "`h_baghoroo`.horooname_en, " +
                "`h_city`.cityname, " +
                "`h_city`.cityname_en " +
                "FROM `hospital`.`hform01_tarhvar` INNER JOIN `h_city` " +
                "ON `h_city`.code = `hform01_tarhvar`.citycode " +
                "INNER JOIN `h_sum` " +
                "ON `h_sum`.sumcode = `hform01_tarhvar`.sumcode " +
                "INNER JOIN `h_baghoroo` ON `hform01_tarhvar`.bagcode = `h_baghoroo`.horoocode " +
                "AND `h_baghoroo`.sumcode = `hform01_tarhvar`.`sumcode` " +
                "WHERE ";

        if (entity.getHformkey() != null && !entity.getHformkey().isEmpty()){
            query += " `hform01_tarhvar`.`hformkey` = ? AND ";
        }
        if (entity.getCitycode() != null && !entity.getCitycode().isEmpty()){
            query += " `hform01_tarhvar`.`citycode` = ? AND ";
        }
        if (entity.getSumcode() != null && !entity.getSumcode().isEmpty()){
            query += " `hform01_tarhvar`.`sumcode` = ? AND ";
        }
        if (entity.getBagcode() != null && !entity.getBagcode().isEmpty()){
            query += " `hform01_tarhvar`.`bagcode` = ? AND ";
        }
        if (entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
            query += " `hform01_tarhvar`.`delflg` = ? AND ";
        }
        if (entity.getActflg() != null && !entity.getActflg().isEmpty()){
            query += " `hform01_tarhvar`.`actflg` = ? AND ";
        }
        if (entity != null && entity.getCre_by() != null && !entity.getCre_by().isEmpty()){
            query += " `hform01_tarhvar`.`cre_by` = ? AND ";
        }

        query += " `hform01_tarhvar`.`id` > 0 ";
        query += " ORDER BY `hform01_tarhvar`.`cre_at` DESC ";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query);

                if (entity.getHformkey() != null && !entity.getHformkey().isEmpty()){
                    statement.setString(stIndex++, entity.getHformkey());
            }
                if (entity.getCitycode() != null && !entity.getCitycode().isEmpty()){
                    statement.setString(stIndex++, entity.getCitycode());
                }
                if (entity.getSumcode() != null && !entity.getSumcode().isEmpty()){
                    statement.setString(stIndex++, entity.getSumcode());
                }
                if (entity.getBagcode() != null && !entity.getBagcode().isEmpty()){
                    statement.setString(stIndex++, entity.getBagcode());
                }
                if (entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
                    statement.setString(stIndex++, entity.getDelflg());
                }
                if (entity.getActflg() != null && !entity.getActflg().isEmpty()){
                    statement.setString(stIndex++, entity.getActflg());
                }
                if (entity != null && entity.getCre_by() != null && !entity.getCre_by().isEmpty()){
                    statement.setString(stIndex++, entity.getCre_by());
                }


                ResultSet resultSet = statement.executeQuery();

                entityList = new ArrayList<>();

                while (resultSet.next()) {
                    TarkhvarDataEntity objEntity = new TarkhvarDataEntity();

                    objEntity.setId(resultSet.getLong("id"));
                    objEntity.setHformkey(resultSet.getString("hformkey"));
                    objEntity.setCitycode(resultSet.getString("citycode"));
                    objEntity.setSumcode(resultSet.getString("sumcode"));
                    objEntity.setBagcode(resultSet.getString("bagcode"));
                    objEntity.setMalchinner(resultSet.getString("malchinner"));
                    objEntity.setBaigal_haldvariin(resultSet.getString("baigal_haldvariin"));
                    objEntity.setBaktsinii(resultSet.getString("baktsinii"));
                    objEntity.setDs_hiiseneseh(resultSet.getString("ds_hiiseneseh"));
                    objEntity.setDs_bag_gishuun_ners(resultSet.getString("ds_bag_gishuun_ners"));
                    objEntity.setDs_ur_dun(resultSet.getString("ds_ur_dun"));
                    objEntity.setZuvlomj(resultSet.getString("zuvlomj"));
                    objEntity.setHuchin_zuil_ognoo(resultSet.getString("huchin_zuil_ognoo"));
                    objEntity.setUrjliin_mal_hud(resultSet.getString("urjliin_mal_hud"));
                    objEntity.setBusad_mal_hud(resultSet.getString("busad_mal_hud"));
                    objEntity.setBakcin_targu(resultSet.getString("bakcin_targu"));
                    objEntity.setOtor_nuudel(resultSet.getString("otor_nuudel"));
                    objEntity.setHun_sh_hod(resultSet.getString("hun_sh_hod"));
                    objEntity.setTeever_sh_hod(resultSet.getString("teever_sh_hod"));
                    objEntity.setTabiul_mal_avsan(resultSet.getString("tabiul_mal_avsan"));
                    objEntity.setBelcheer_neg(resultSet.getString("belcheer_neg"));
                    objEntity.setHudag_us(resultSet.getString("hudag_us"));
                    objEntity.setBusad_huch_zuil(resultSet.getString("busad_huch_zuil"));
                    objEntity.setShaltgaan_todgu(resultSet.getString("shaltgaan_todgu"));
                    objEntity.setMalchin_uuruu(resultSet.getString("malchin_uuruu"));
                    objEntity.setSahalt_ail(resultSet.getString("sahalt_ail"));
                    objEntity.setMal_emneleg(resultSet.getString("mal_emneleg"));
                    objEntity.setUzleg_tandalt(resultSet.getString("uzleg_tandalt"));
                    objEntity.setTandalt_shinjilgee(resultSet.getString("tandalt_shinjilgee"));
                    objEntity.setBusad(resultSet.getString("busad"));
                    objEntity.setTusgaarlasan_eseh(resultSet.getString("tusgaarlasan_eseh"));
                    objEntity.setTusgaarlasan_ognoo(resultSet.getDate("tusgaarlasan_ognoo"));
                    objEntity.setTus_horoo_bairlal(resultSet.getString("tus_horoo_bairlal"));

                    objEntity.setDelflg(resultSet.getString("delflg"));
                    objEntity.setActflg(resultSet.getString("actflg"));
                    objEntity.setCre_at(resultSet.getDate("cre_at"));
                    objEntity.setCre_by(resultSet.getString("cre_by"));
                    objEntity.setMod_at(resultSet.getDate("mod_at"));
                    objEntity.setMod_by(resultSet.getString("mod_by"));
                    objEntity.setRecord_date(resultSet.getDate("record_date"));

                    objEntity.put("cityname", resultSet.getString("cityname"));
                    objEntity.put("sumname", resultSet.getString("sumname"));
                    objEntity.put("horooname", resultSet.getString("horooname"));
                    objEntity.put("index", index++);
                    objEntity.put("strRecordOgnoo", dateFormat.format(objEntity.getRecord_date()));

                    entityList.add(objEntity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return entityList;
    }

    @Override
    public ErrorEntity updateData(TarkhvarDataEntity entity) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (entity == null) new ErrorEntity(ErrorType.INFO, Long.valueOf(1059));
        if (entity == null) {
            errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errorEntity.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errorEntity.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errorEntity;
        }

        query = "UPDATE `hospital`.`hform01_tarhvar` " +
                "SET     ";

        if (entity.getHformkey() != null && !entity.getHformkey().isEmpty())
            query += " `hformkey`=?," ;

        if (entity.getCitycode() != null && !entity.getCitycode().isEmpty())
            query += "`citycode`=?," ;

        if (entity.getSumcode() != null && !entity.getSumcode().isEmpty())
            query += "`sumcode`=?," ;

        if (entity.getBagcode() != null && !entity.getBagcode().isEmpty())
            query += "`bagcode`=?," ;

        if (entity.getMalchinner() != null && !entity.getMalchinner().isEmpty())
            query += "`malchinner`=?," ;

        if (entity.getBaigal_haldvariin() != null && !entity.getBaigal_haldvariin().isEmpty())
            query += "`baigal_haldvariin`=?," ;

        if (entity.getBaktsinii() != null && !entity.getBaktsinii().isEmpty())
            query += "`baktsinii`=?," ;

        if (entity.getDs_hiiseneseh() != null && !entity.getDs_hiiseneseh().isEmpty())
            query += "`ds_hiiseneseh`=?," ;

        if (entity.getDs_bag_gishuun_ners() != null && !entity.getDs_bag_gishuun_ners().isEmpty())
            query += "`ds_bag_gishuun_ners`=?," ;

        if (entity.getDs_ur_dun() != null && !entity.getDs_ur_dun().isEmpty())
            query += "`ds_ur_dun`=?," ;

        if (entity.getZuvlomj() != null && !entity.getZuvlomj().isEmpty())
            query += "`zuvlomj`=?," ;

        if (entity.getHuchin_zuil_ognoo() != null && !entity.getHuchin_zuil_ognoo().isEmpty())
            query += "`huchin_zuil_ognoo`=?," ;

        if (entity.getUrjliin_mal_hud() != null && !entity.getUrjliin_mal_hud().isEmpty())
            query += "`urjliin_mal_hud`=?," ;

        if (entity.getBusad_mal_hud() != null && !entity.getBusad_mal_hud().isEmpty())
            query += "`busad_mal_hud`=?," ;

        if (entity.getBakcin_targu() != null && !entity.getBakcin_targu().isEmpty())
            query += "`bakcin_targu`=?," ;

        if (entity.getOtor_nuudel() != null && !entity.getOtor_nuudel().isEmpty())
            query += "`otor_nuudel`=?," ;

        if (entity.getHun_sh_hod() != null && !entity.getHun_sh_hod().isEmpty())
            query += "`hun_sh_hod`=?," ;

        if (entity.getTeever_sh_hod() != null && !entity.getTeever_sh_hod().isEmpty())
            query += "`teever_sh_hod`=?," ;

        if (entity.getTabiul_mal_avsan() != null && !entity.getTabiul_mal_avsan().isEmpty())
            query += "`tabiul_mal_avsan`=?," ;

        if (entity.getBelcheer_neg() != null && !entity.getBelcheer_neg().isEmpty())
            query += "`belcheer_neg`=?," ;

        if (entity.getHudag_us() != null && !entity.getHudag_us().isEmpty())
            query += "`hudag_us`=?," ;

        if (entity.getBusad_huch_zuil() != null && !entity.getBusad_huch_zuil().isEmpty())
            query += "`busad_huch_zuil`=?," ;

        if (entity.getShaltgaan_todgu() != null && !entity.getShaltgaan_todgu().isEmpty())
            query += "`shaltgaan_todgu`=?," ;

        if (entity.getMalchin_uuruu() != null && !entity.getMalchin_uuruu().isEmpty())
            query += "`malchin_uuruu`=?," ;

        if (entity.getSahalt_ail() != null && !entity.getSahalt_ail().isEmpty())
            query += "`sahalt_ail`=?," ;

        if (entity.getMal_emneleg() != null && !entity.getMal_emneleg().isEmpty())
            query += "`mal_emneleg`=?," ;

        if (entity.getUzleg_tandalt() != null && !entity.getUzleg_tandalt().isEmpty())
            query += "`uzleg_tandalt`=?," ;

        if (entity.getTandalt_shinjilgee() != null && !entity.getTandalt_shinjilgee().isEmpty())
            query += "`tandalt_shinjilgee`=?," ;

        if (entity.getBusad() != null && !entity.getBusad().isEmpty())
            query += "`busad`=?," ;

        if (entity.getTusgaarlasan_eseh() != null && !entity.getTusgaarlasan_eseh().isEmpty())
            query += "`tusgaarlasan_eseh`=?," ;

        if (entity.getTusgaarlasan_ognoo() != null )
            query += "`tusgaarlasan_ognoo`=?," ;

        if (entity.getTus_horoo_bairlal() != null && !entity.getTus_horoo_bairlal().isEmpty())
            query += "`tus_horoo_bairlal`=?," ;

        if (entity.getDelflg() != null && !entity.getDelflg().isEmpty())
            query += "`delflg`=?," ;

        if (entity.getActflg() != null && !entity.getActflg().isEmpty())
            query += "`actflg`=?," ;

        if (entity.getRecord_date() != null)
            query += "`record_date`=?, ";

        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";
        query += " WHERE  ID   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (entity.getHformkey() != null && !entity.getHformkey().isEmpty())
                    statement.setString(stIndex++, entity.getHformkey());

                if (entity.getCitycode() != null && !entity.getCitycode().isEmpty())
                    statement.setString(stIndex++, entity.getCitycode());

                if (entity.getSumcode() != null && !entity.getSumcode().isEmpty())
                    statement.setString(stIndex++, entity.getSumcode());

                if (entity.getBagcode() != null && !entity.getBagcode().isEmpty())
                    statement.setString(stIndex++, entity.getBagcode());

                if (entity.getMalchinner() != null && !entity.getMalchinner().isEmpty())
                    statement.setString(stIndex++, entity.getMalchinner());

                if (entity.getBaigal_haldvariin() != null && !entity.getBaigal_haldvariin().isEmpty())
                    statement.setString(stIndex++, entity.getBaigal_haldvariin());

                if (entity.getBaktsinii() != null && !entity.getBaktsinii().isEmpty())
                    statement.setString(stIndex++, entity.getBaktsinii());

                if (entity.getDs_hiiseneseh() != null && !entity.getDs_hiiseneseh().isEmpty())
                    statement.setString(stIndex++, entity.getDs_hiiseneseh());

                if (entity.getDs_bag_gishuun_ners() != null && !entity.getDs_bag_gishuun_ners().isEmpty())
                    statement.setString(stIndex++, entity.getDs_bag_gishuun_ners());

                if (entity.getDs_ur_dun() != null && !entity.getDs_ur_dun().isEmpty())
                    statement.setString(stIndex++, entity.getDs_ur_dun());

                if (entity.getZuvlomj() != null && !entity.getZuvlomj().isEmpty())
                    statement.setString(stIndex++, entity.getZuvlomj());

                if (entity.getHuchin_zuil_ognoo() != null && !entity.getHuchin_zuil_ognoo().isEmpty())
                    statement.setString(stIndex++, entity.getHuchin_zuil_ognoo());

                if (entity.getUrjliin_mal_hud() != null && !entity.getUrjliin_mal_hud().isEmpty())
                    statement.setString(stIndex++, entity.getUrjliin_mal_hud());

                if (entity.getBusad_mal_hud() != null && !entity.getBusad_mal_hud().isEmpty())
                    statement.setString(stIndex++, entity.getBusad_mal_hud());

                if (entity.getBakcin_targu() != null && !entity.getBakcin_targu().isEmpty())
                    statement.setString(stIndex++, entity.getBakcin_targu());

                if (entity.getOtor_nuudel() != null && !entity.getOtor_nuudel().isEmpty())
                    statement.setString(stIndex++, entity.getOtor_nuudel());

                if (entity.getHun_sh_hod() != null && !entity.getHun_sh_hod().isEmpty())
                    statement.setString(stIndex++, entity.getHun_sh_hod());

                if (entity.getTeever_sh_hod() != null && !entity.getTeever_sh_hod().isEmpty())
                    statement.setString(stIndex++, entity.getTeever_sh_hod());

                if (entity.getTabiul_mal_avsan() != null && !entity.getTabiul_mal_avsan().isEmpty())
                    statement.setString(stIndex++, entity.getTabiul_mal_avsan());

                if (entity.getBelcheer_neg() != null && !entity.getBelcheer_neg().isEmpty())
                    statement.setString(stIndex++, entity.getBelcheer_neg());

                if (entity.getHudag_us() != null && !entity.getHudag_us().isEmpty())
                    statement.setString(stIndex++, entity.getHudag_us());

                if (entity.getBusad_huch_zuil() != null && !entity.getBusad_huch_zuil().isEmpty())
                    statement.setString(stIndex++, entity.getBusad_huch_zuil());

                if (entity.getShaltgaan_todgu() != null && !entity.getShaltgaan_todgu().isEmpty())
                    statement.setString(stIndex++, entity.getShaltgaan_todgu());

                if (entity.getMalchin_uuruu() != null && !entity.getMalchin_uuruu().isEmpty())
                    statement.setString(stIndex++, entity.getMalchin_uuruu());

                if (entity.getSahalt_ail() != null && !entity.getSahalt_ail().isEmpty())
                    statement.setString(stIndex++, entity.getSahalt_ail());

                if (entity.getMal_emneleg() != null && !entity.getMal_emneleg().isEmpty())
                    statement.setString(stIndex++, entity.getMal_emneleg());

                if (entity.getUzleg_tandalt() != null && !entity.getUzleg_tandalt().isEmpty())
                    statement.setString(stIndex++, entity.getUzleg_tandalt());

                if (entity.getTandalt_shinjilgee() != null && !entity.getTandalt_shinjilgee().isEmpty())
                    statement.setString(stIndex++, entity.getTandalt_shinjilgee());

                if (entity.getBusad() != null && !entity.getBusad().isEmpty())
                    statement.setString(stIndex++, entity.getBusad());

                if (entity.getTusgaarlasan_eseh() != null && !entity.getTusgaarlasan_eseh().isEmpty())
                    statement.setString(stIndex++, entity.getTusgaarlasan_eseh());

                if (entity.getTusgaarlasan_ognoo() != null )
                    statement.setDate(stIndex++, entity.getTusgaarlasan_ognoo());

                if (entity.getTus_horoo_bairlal() != null && !entity.getTus_horoo_bairlal().isEmpty())
                    statement.setString(stIndex++, entity.getTus_horoo_bairlal());

                if (entity.getDelflg() != null && !entity.getDelflg().isEmpty())
                    statement.setString(stIndex++, entity.getDelflg());

                if (entity.getActflg() != null && !entity.getActflg().isEmpty())
                    statement.setString(stIndex++, entity.getActflg());

                if (entity.getRecord_date() != null)
                    statement.setDate(stIndex++, entity.getRecord_date());

                statement.setDate(stIndex++, entity.getMod_at());
                statement.setString(stIndex++, entity.getMod_by());
                statement.setLong(stIndex++, entity.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                    errorEntity.setErrText("Бичлэг амжилттай засагдлаа.");
                    errorEntity.setErrDesc("Бичлэг амжилттай засагдлаа.");
                    return errorEntity;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }


        errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1061));
        errorEntity.setErrText("Амжилтгүй боллоо. Дахин оролдоно уу.");
        errorEntity.setErrDesc("Амжилтгүй боллоо. Дахин оролдоно уу.");

        return errorEntity;
    }

    @Override
    public ErrorEntity manageData(User user, TarkhvarDataEntity entity) {
        Date utilDate = new Date();
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

        try {
            TarkhvarDataEntity tmp = new TarkhvarDataEntity();

            tmp.setHformkey(entity.getHformkey());


            List<TarkhvarDataEntity> list = getListData(tmp);

            String command = "";

            if (list == null){
                command = "INS";
            }
            else if (list.size() == 0){
                command = "INS";
            }
            else if (list.size() > 0){
                command = "EDT";
            }

            entity.setMod_at(sqlDate);
            entity.setMod_by(user.getUsername());


            if (command.equalsIgnoreCase("INS"))
            {
                entity.setDelflg("N");
                entity.setActflg("Y");
                entity.setCre_by(user.getUsername());
                entity.setCre_at(sqlDate);

                errorEntity = insertNewData(entity);
            }
            else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL") ||
                    command.equalsIgnoreCase("ACT"))
            {
                entity.setId(list.get(0).getId());

                if (command.equalsIgnoreCase("DEL")){
                    entity.setDelflg("Y");
                }

                errorEntity = updateData(entity);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        finally {
            if (errorEntity == null){
                errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1002));
                errorEntity.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
                errorEntity.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

                return errorEntity;
            }
        }

        return errorEntity;
    }
}
