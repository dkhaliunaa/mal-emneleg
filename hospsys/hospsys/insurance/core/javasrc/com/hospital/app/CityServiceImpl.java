package com.hospital.app;

import com.enumclass.ErrorType;
import com.model.hos.City;
import com.model.hos.ErrorEntity;
import com.jolbox.bonecp.BoneCP;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 24be10264 on 9/13/2017.
 */
public class CityServiceImpl implements ICityService {

    private BoneCP boneCP;
    private ErrorEntity errObj;

    public CityServiceImpl (BoneCP boneCP) {
        this.boneCP = boneCP;
    }


    @Override
    public List<City> selectAll(City city) throws SQLException {
        int stIndex = 1;
        List<City> cityEntitieList = null;

        String query_sel = "SELECT * FROM `h_city` AS c WHERE ";

        if (city != null && city.getCode() != null && !city.getCode().isEmpty()){
            query_sel += " c.`code` = ? AND ";
        }
        if (city.getCountrycode() != null && !city.getCountrycode().isEmpty()){
            query_sel += " c.`countrycode` = ? AND ";
        }
        if (city.getCityname() != null && !city.getCityname().isEmpty()){
            query_sel += " c.`cityname` LIKE ? AND ";
        }
        if (city.getCitynameEn() != null && !city.getCitynameEn().isEmpty()){
            query_sel += " c.`cityname_en` LIKE ? AND ";
        }

        if (city.getCreBy() != null && !city.getCreBy().isEmpty()){
            query_sel += " c.`cre_by` = ? AND ";
        }

        if (city.getModBy() != null && !city.getModBy().isEmpty()){
            query_sel += " c.`mod_by` = ? AND ";
        }

        if (city != null && city.getCreAt() != null && city.getSearchDate() != null){
            query_sel += " (c.`cre_at` BETWEEN ? AND ?) AND ";
        }

        if (city != null && city.getModAt() != null && city.getSearchDate() != null){
            query_sel += " (c.`mod_at` BETWEEN ? AND ?) AND ";
        }
        if (city != null && city.getActflg() != null && !city.getActflg().isEmpty()){
            query_sel += " c.actflg = ? AND ";
        }
        if (city != null && city.getDelflg() != null && !city.getDelflg().isEmpty()){

            query_sel += "c.delflg = ? AND ";
        }

        query_sel += "c.id > 0 ORDER BY c.`cityname`";

        try (Connection connection = boneCP.getConnection()) {
            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                if (city.getCode() != null && !city.getCode().isEmpty()) {
                    statement.setString(stIndex++, city.getCode());
                }
                if (city.getCountrycode() != null && !city.getCountrycode().isEmpty()) {
                    statement.setString(stIndex++, city.getCountrycode());
                }
                if (city.getCityname() != null && !city.getCityname().isEmpty()) {
                    statement.setString(stIndex++, "%" + city.getCityname() + "%");
                }
                if (city.getCitynameEn() != null && !city.getCitynameEn().isEmpty()) {
                    statement.setString(stIndex++, "%" + city.getCitynameEn() + "%");
                }

                if (city.getCreBy() != null && !city.getCreBy().isEmpty()) {
                    statement.setString(stIndex++, city.getCreBy());
                }

                if (city.getModBy() != null && !city.getModBy().isEmpty()) {
                    statement.setString(stIndex++, city.getModBy());
                }

                if (city != null && city.getCreAt() != null && city.getSearchDate() != null) {
                    statement.setDate(stIndex++, city.getCreAt());
                    statement.setDate(stIndex++, city.getSearchDate());
                }

                if (city != null && city.getModAt() != null && city.getModAt() != null) {
                    statement.setDate(stIndex++, city.getModAt());
                    statement.setDate(stIndex++, city.getSearchDate());
                }
                if (city != null && city.getActflg() != null && !city.getActflg().isEmpty()){
                    statement.setString(stIndex++, city.getActflg());
                }
                if (city != null && city.getDelflg() != null && !city.getDelflg().isEmpty()){

                    statement.setString(stIndex++, city.getDelflg());
                }

                ResultSet resultSet = statement.executeQuery();

                cityEntitieList = new ArrayList<>();

                int index = 1;

                while (resultSet.next()) {
                    City entity = new City();

                    entity.setId(resultSet.getLong("id"));
                    entity.setCode(resultSet.getString("code"));
                    entity.setCountrycode(resultSet.getString("countrycode"));
                    entity.setCityname(resultSet.getString("cityname"));
                    entity.setCitydesc(resultSet.getString("citydesc"));
                    entity.setCitynameEn(resultSet.getString("cityname_en"));
                    entity.setDelflg(resultSet.getString("delflg"));
                    entity.setActflg(resultSet.getString("actflg"));
                    entity.setModAt(resultSet.getDate("mod_at"));
                    entity.setModby(resultSet.getString("mod_by"));
                    entity.setCreat(resultSet.getDate("cre_at"));
                    entity.setCreby(resultSet.getString("cre_by"));
                    entity.setLatitude(resultSet.getString("latitude"));
                    entity.setLongitude(resultSet.getString("longitude"));
                    entity.put("index", index++);

                    cityEntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return cityEntitieList;
    }

    @Override
    public ErrorEntity insertData(City city) throws SQLException {
        PreparedStatement statement = null;

        if (city == null) {
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        String query_login = "INSERT INTO `h_city`(`code`, `countrycode`, `cityname`, `citydesc`, `delflg`, " +
                "`actflg`, `cre_by`, `cre_at`, `mod_at`, `mod_by`, `cityname_en`, `latitude`, `longitude`) " +
                "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";

        try (Connection connection = boneCP.getConnection()) {
            try{
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, (city.getCode().trim() != null) ? city.getCode().trim(): city.getCode().trim());
                statement.setString(2, (city.getCountrycode() != null) ? city.getCountrycode().trim() : city.getCountrycode());
                statement.setString(3, (city.getCityname() != null) ? city.getCityname().trim():city.getCityname());
                statement.setString(4, (city.getCitydesc() != null) ? city.getCitydesc().trim(): city.getCitydesc());
                statement.setString(5, (city.getDelflg() != null) ? city.getDelflg().trim() : city.getDelflg());
                statement.setString(6, (city.getActflg() != null)? city.getActflg().trim() : city.getActflg());
                statement.setString(7, city.getCreBy().trim());
                statement.setDate(8, city.getCreAt());
                statement.setDate(9, city.getModAt());
                statement.setString(10, (city.getModBy() != null) ?city.getModBy().trim() : city.getModBy());
                statement.setString(11, (city.getCitynameEn() != null)? city.getCitynameEn().trim() : city.getCitynameEn());
                statement.setString(12, (city.getLatitude() != null) ? city.getLatitude().trim() : city.getLatitude());
                statement.setString(13, (city.getLongitude() != null) ? city.getLongitude().trim() : city.getLongitude());

                City searchCity = new City();
                searchCity.setCode(city.getCode());
                List<City> tmpList = selectAll(searchCity);
                if (tmpList.size() > 0){
                    city.setId(tmpList.get(0).getId());
                    city.setDelflg("N");
                    city.setActflg("Y");
                    return updateData(city);
                }else{
                    int insertCount = statement.executeUpdate();

                    if (insertCount == 1){
                        connection.commit();

                        errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));
                        errObj.setErrText("Бичлэг амжилттай хадгалагдлаа.");
                        errObj.setErrDesc("Бичлэг амжилттай хадгалагдлаа.");
                        return errObj;
                    }
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }finally {
                if (statement != null)statement.close();
                connection.close();
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }

        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1057));
        errObj.setErrText("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");
        errObj.setErrDesc("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");

        return errObj;
    }

    @Override
    public ErrorEntity updateData(City city) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (city == null) new ErrorEntity(ErrorType.INFO, Long.valueOf(1059));

        query = "UPDATE `hospital`.`h_city` " +
                "SET     ";

        if (city.getCode() != null && !city.getCode().isEmpty()){
            query += " `code` = ?, ";
        }

        if (city.getCityname() != null && !city.getCityname().isEmpty()){
            query += " `cityname` = ?, ";
        }

        if (city.getCitydesc() != null && !city.getCitydesc().toString().isEmpty()){
            query += " `citydesc` = ?, ";
        }
        if (city.getDelflg() != null && !city.getDelflg().toString().isEmpty()){
            query += " `delflg` = ?, ";
        }

        if (city.getActflg() != null && !city.getActflg().toString().isEmpty()){
            query += " `actflg` = ?, ";
        }
        if (city.getCitynameEn() != null && !city.getCitynameEn().toString().isEmpty()){
            query += " `cityname_en` = ?, ";
        }
        if (city.getLatitude() != null && !city.getLatitude().toString().isEmpty()){
            query += " `latitude` = ?, ";
        }
        if (city.getLongitude() != null && !city.getLongitude().toString().isEmpty()){
            query += " `longitude` = ?, ";
        }

        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";

        query += " WHERE  `id`   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (city.getCode() != null && !city.getCode().isEmpty()){
                    statement.setString(stIndex++, city.getCode().trim());
                }

                if (city.getCityname() != null && !city.getCityname().isEmpty()){
                    statement.setString(stIndex++, city.getCityname().trim());
                }

                if (city.getCitydesc() != null && !city.getCitydesc().toString().isEmpty()){
                    statement.setString(stIndex++, city.getCitydesc().trim());
                }
                if (city.getDelflg() != null && !city.getDelflg().toString().isEmpty()){
                    statement.setString(stIndex++, city.getDelflg().trim());
                }

                if (city.getActflg() != null && !city.getActflg().toString().isEmpty()){
                    statement.setString(stIndex++, city.getActflg().trim());
                }
                if (city.getCitynameEn() != null && !city.getCitynameEn().toString().isEmpty()){
                    statement.setString(stIndex++, city.getCitynameEn().trim());
                }
                if (city.getLatitude() != null && !city.getLatitude().toString().isEmpty()){
                    statement.setString(stIndex++, city.getLatitude().trim());
                }
                if (city.getLongitude() != null && !city.getLongitude().toString().isEmpty()){
                    statement.setString(stIndex++, city.getLongitude().trim());
                }

                statement.setDate(stIndex++, city.getModAt());
                statement.setString(stIndex++, city.getModBy().trim());
                statement.setLong(stIndex++, city.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();

                    errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                    errObj.setErrText("Бичлэг амжилттай засагдлаа.");
                    errObj.setErrDesc("Бичлэг амжилттай засагдлаа.");
                    return errObj;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }


        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1061));
        errObj.setErrText("Амжилтгүй боллоо. Дахин оролдоно уу.");
        errObj.setErrDesc("Амжилтгүй боллоо. Дахин оролдоно уу.");

        return errObj;
    }
}
