package com.hospital.app;

import com.model.User;
import com.model.hos.ErrorEntity;
import com.model.hos.TarkhvarDataEntity;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public interface ITarkhvarMedeeFormService {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");


    /**
     * Insert
     *
     * @param entity
     * @return
     * @throws SQLException
     */
    ErrorEntity insertNewData(TarkhvarDataEntity entity) throws SQLException;
    /**
     * Select all
     *
     * @param entity
     * @return
     * @throws SQLException
     */
    List<TarkhvarDataEntity> getListData(TarkhvarDataEntity entity) throws SQLException;


    /**
     * update form data
     *
     * @param entity
     * @return
     * @throws SQLException
     */
    ErrorEntity updateData(TarkhvarDataEntity entity) throws SQLException;

    /**
     * Manage data
     *
     * @param user
     * @param entity
     * @return
     */
    ErrorEntity manageData(User user, TarkhvarDataEntity entity);
}
