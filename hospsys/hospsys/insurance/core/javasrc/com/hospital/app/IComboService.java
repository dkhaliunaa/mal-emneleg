package com.hospital.app;

import com.model.hos.ComboEntity;
import com.model.hos.ErrorEntity;
import com.mongodb.BasicDBObject;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by 24be10264 on 8/18/2017.
 *
 *
 * Энэхүү класс нь hcmb дээр бүртгэлтэй байгаа combo бүртгэлээс тухайн төрлөөр нь
 * бүх жагсаалтыг авах сервис юм.
 */
public interface IComboService {

    /**
     * Combobox utga duurgeh
     *
     * @param type
     * @return
     * @throws Exception
     */
    List<BasicDBObject> getComboVals(String type, String langid) throws Exception;

    /**
     * Combobox utga duurgeh
     *
     * @param type
     * @return
     * @throws Exception
     */
    List<BasicDBObject> getShalgahGazarVals(String type, String langid) throws Exception;

    /**
     * Combobox utga duurgeh
     *
     * @param type
     * @return
     * @throws Exception
     */
    List<BasicDBObject> getComboNames(String type, String langid) throws Exception;

    /**
     * Get combo type name
     *
     * @param code
     * @param langid
     * @return
     * @throws SQLException
     */
    String getComboName(String code, String langid) throws SQLException;


    /**
     * Select All
     *
     * @param entity
     * @return
     * @throws SQLException
     */
    List<ComboEntity> selectAll(ComboEntity entity) throws SQLException;

    /**
     * Insert New Record
     *
     * @param entity
     * @return
     * @throws SQLException
     */
    ErrorEntity insertCmb(ComboEntity entity) throws SQLException;

    /**
     * Update Old Record
     *
     * @param entity
     * @return
     * @throws SQLException
     */
    ErrorEntity updateCmb(ComboEntity entity) throws SQLException;
}
