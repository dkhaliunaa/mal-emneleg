package com.hospital.app;

import com.model.hos.City;
import com.model.hos.ErrorEntity;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by 24be10264 on 9/13/2017.
 */
public interface ICityService{

    /**
     * Fetch
     *
     * @param city
     * @return
     * @throws SQLException
     */
    List<City> selectAll(City city) throws SQLException;

    /**
     * Insert
     *
     * @param city
     * @return
     * @throws SQLException
     */
    ErrorEntity insertData(City city) throws SQLException;

    /**
     * Update
     *
     * @param city
     * @return
     * @throws SQLException
     */
    ErrorEntity updateData(City city) throws SQLException;
}
