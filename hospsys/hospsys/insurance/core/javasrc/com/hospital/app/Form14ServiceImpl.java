package com.hospital.app;


import com.Config;
import com.enumclass.ErrorType;
import com.jolbox.bonecp.BoneCP;
import com.model.hos.*;
import com.mongodb.BasicDBObject;
import com.rpc.PdfExcelUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public class Form14ServiceImpl implements IForm14Service{
    /**
     * Objects
     */

    private BoneCP boneCP;
    private ErrorEntity errObj;


    public Form14ServiceImpl(BoneCP boneCP) {
        this.boneCP = boneCP;
    }

    @Override
    public ErrorEntity insertNewData(Form14Entity form14) throws SQLException {
        PreparedStatement statement = null;

        if (form14 == null) {
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        String query_login = "INSERT INTO `hospital`.`hform14` " +
                "(`shalgahGazar`, " +
                "`shalgahGazar_en`, " +
                "`uzlegToo`, " +
                "`baiguullagaToo`, " +
                "`ajAhuinNegjToo`, " +
                "`irgedToo`, " +
                "`huuliinBaigShiljsen`, " +
                "`zahirgaaHariToo`, " +
                "`torguuliHemjee`, " +
                "`tolbor`, " +
                "`torguuliar`, " +
                "`tolboroor`, " +
                "`delflg`, " +
                "`actflg`, " +
                "`mod_at`, " +
                "`mod_by`, " +
                "`cre_at`, " +
                "`recorddate`, " +
                "`cre_by`," +
                "`aimag`," +
                "`sum`," +
                "`aimag_en`," +
                "`sum_en`)" +
                "VALUES " +
                "(? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?,?, ?, ?, ?, ?, ?, ? )";

        try (Connection connection = boneCP.getConnection()) {
            try{
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, form14.getShalgahGazar());
                statement.setString(2, form14.getShalgahGazar_en());
                statement.setDouble(3, form14.getUzlegToo());
                statement.setDouble(4, form14.getBaiguullagaToo());
                statement.setDouble(5, form14.getAjAhuinNegjToo());
                statement.setDouble(6, form14.getIrgedToo());
                statement.setDouble(7, form14.getHuuliinBaigShiljsen());
                statement.setDouble(8, form14.getZahirgaaHariToo());
                statement.setBigDecimal(9, form14.getTorguuliHemjee());
                statement.setBigDecimal(10, form14.getTolbor());
                statement.setBigDecimal(11, form14.getTorguuliar());
                statement.setBigDecimal(12, form14.getTolboroor());
                statement.setString(13, form14.getDELFLG());
                statement.setString(14, form14.getACTFLG());
                statement.setDate(15, form14.getMODAT());
                statement.setString(16, form14.getMODBY());
                statement.setDate(17, form14.getCREAT());
                statement.setDate(18, form14.getRecordDate());
                statement.setString(19, form14.getCREBY());
                statement.setString(20, form14.getAimag());
                statement.setString(21, form14.getSum());
                statement.setString(22, form14.getAimag_en());
                statement.setString(23, form14.getSum_en());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1){
                    connection.commit();
                    errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));
                    errObj.setErrText("Бичлэг амжилттай хадгалагдлаа.");
                    errObj.setErrDesc("Бичлэг амжилттай хадгалагдлаа.");
                    return errObj;
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }finally {
                if (statement != null)statement.close();
                connection.close();
            }

        }catch (Exception ex){

        }

        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1057));
        errObj.setErrText("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");
        errObj.setErrDesc("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");

        return errObj;
    }

    @Override
    public List<Form14Entity> getListData(Form14Entity form14) throws SQLException {
        System.out.println(form14);
        int stIndex = 1;
        List<Form14Entity> form14EntitieList = null;

        String query = "SELECT `hform14`.`id`, " +
                "    `hform14`.`shalgahGazar`, " +
                "    `hform14`.`shalgahGazar_en`, " +
                "    `hform14`.`uzlegToo`, " +
                "    `hform14`.`baiguullagaToo`, " +
                "    `hform14`.`ajAhuinNegjToo`, " +
                "    `hform14`.`irgedToo`, " +
                "    `hform14`.`huuliinBaigShiljsen`, " +
                "    `hform14`.`zahirgaaHariToo`, " +
                "    `hform14`.`torguuliHemjee`, " +
                "    `hform14`.`tolbor`, " +
                "    `hform14`.`torguuliar`, " +
                "    `hform14`.`tolboroor`, " +
                "    `hform14`.`delflg`, " +
                "    `hform14`.`actflg`, " +
                "    `hform14`.`mod_at`, " +
                "    `hform14`.`mod_by`, " +
                "    `hform14`.`cre_at`, " +
                "    `hform14`.`recorddate`, " +
                "    `hform14`.`cre_by`, " +
                "    `hform14`.`aimag`, " +
                "    `hform14`.`sum`, " +
                "    `hform14`.`aimag_en`, " +
                "    `hform14`.`sum_en`, " +
                "     c.cityname, c.cityname_en, " +
                "    `h_sum`.sumname, `h_sum`.sumname_en, " +
                "    `h_sum`.latitude as sum_latitude, `h_sum`.longitude as sum_longitude  " +
                "FROM `hospital`.`hform14` INNER JOIN `h_city` AS c " +
                "ON c.code = `hform14`.aimag " +
                "INNER JOIN `h_sum` " +
                "ON `h_sum`.sumcode = `hform14`.sum " +
                "WHERE ";

        if (form14.getShalgahGazar() != null && !form14.getShalgahGazar().isEmpty()){
            query += " shalgahGazar = ? AND ";
        }

        if (form14.getShalgahGazar_en() != null && !form14.getShalgahGazar_en().isEmpty()){
            query += " shalgahGazar_en = ? AND ";
        }

        /* NER */
        if (form14.getUzlegToo() != null && !form14.getUzlegToo().toString().isEmpty() && form14.getUzlegToo() != 0){
            query += " uzlegToo = ? AND ";
        }
        if (form14.getBaiguullagaToo() != null && !form14.getBaiguullagaToo().toString().isEmpty() && form14.getBaiguullagaToo() !=0 ){
            query += " baiguullagaToo = ? AND ";
        }
        /* Zartsuulsan hemjee */
        if (form14.getAjAhuinNegjToo() != null && !form14.getAjAhuinNegjToo().toString().isEmpty() && form14.getAjAhuinNegjToo() !=0){
            query += " ajAhuinNegjToo = ? AND ";
        }
        if (form14.getIrgedToo() != null && !form14.getIrgedToo().toString().isEmpty() && form14.getIrgedToo() != 0){
            query += " irgedToo = ? AND ";
        }

        /* MAL TURUL */
        if (form14.getHuuliinBaigShiljsen() != null && !form14.getHuuliinBaigShiljsen().toString().isEmpty() && form14.getHuuliinBaigShiljsen() != 0){
            query += " huuliinBaigShiljsen = ? AND ";
        }
        if (form14.getZahirgaaHariToo() != null && !form14.getZahirgaaHariToo().toString().isEmpty() && form14.getZahirgaaHariToo() != 0){
            query += " zahirgaaHariToo = ? AND ";
        }

        /* Tuluvlugu */
        if (form14.getTorguuliHemjee() != null && !form14.getTorguuliHemjee().toString().isEmpty()  && form14.getTorguuliHemjee().longValue() != 0){
            query += " torguuliHemjee = ? AND ";
        }
        if (form14.getTolbor() != null && !form14.getTolbor().toString().isEmpty() && form14.getTolbor().longValue()!=0){
            query += " tolbor = ? AND ";
        }

        /* Guitsetgel */
        if (form14.getTorguuliar() != null && !form14.getTorguuliar().toString().isEmpty() && form14.getTorguuliar().longValue() !=0){
            query += " torguuliar = ? AND ";
        }
        if (form14.getTolboroor() != null && !form14.getTolboroor().toString().isEmpty() && form14.getTolboroor().longValue() != 0){
            query += " tolboroor = ? AND ";
        }

        if (form14 != null && form14.getAimag() != null && !form14.getAimag().isEmpty()){
            query += " `aimag` = ? AND ";
        }
        if (form14 != null && form14.getSum() != null && !form14.getSum().isEmpty()){
            query += " `sum` = ? AND ";
        }

        if (form14 != null && form14.getRecordDate() != null && form14.getSearchRecordDate() != null){
            query += " (`recorddate` BETWEEN ? AND ?) AND ";
        }

        if (form14 != null && form14.getCREBY() != null && !form14.getCREBY().isEmpty()){
            query += " `hform14`.`cre_by` = ? AND ";
        }

        query += " `hform14`.`delflg` = 'N'";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query);

                if (form14.getShalgahGazar() != null && !form14.getShalgahGazar().isEmpty()) {
                    statement.setString(stIndex++, form14.getShalgahGazar());
                }

                if (form14.getShalgahGazar_en() != null && !form14.getShalgahGazar_en().isEmpty()) {
                    statement.setString(stIndex++, form14.getShalgahGazar_en());
                }

            /* NER */
                if (form14.getUzlegToo() != null && !form14.getUzlegToo().toString().isEmpty() && form14.getUzlegToo() != 0) {
                    statement.setLong(stIndex++, form14.getUzlegToo());
                }
                if (form14.getBaiguullagaToo() != null && !form14.getBaiguullagaToo().toString().isEmpty() && form14.getBaiguullagaToo() !=0) {
                    statement.setDouble(stIndex++, form14.getBaiguullagaToo());
                }

            /* Zartsuulsan hemjee */
                if (form14.getAjAhuinNegjToo() != null && !form14.getAjAhuinNegjToo().toString().isEmpty() && form14.getAjAhuinNegjToo() !=0) {
                    statement.setLong(stIndex++, form14.getAjAhuinNegjToo());
                }
                if (form14.getIrgedToo() != null && !form14.getIrgedToo().toString().isEmpty() && form14.getIrgedToo() != 0) {
                    statement.setLong(stIndex++, form14.getIrgedToo());
                }

            /* MAL TURUL */
                if (form14.getHuuliinBaigShiljsen() != null && !form14.getHuuliinBaigShiljsen().toString().isEmpty() && form14.getHuuliinBaigShiljsen() != 0) {
                    statement.setLong(stIndex++, form14.getHuuliinBaigShiljsen());
                }
                if (form14.getZahirgaaHariToo() != null && !form14.getZahirgaaHariToo().toString().isEmpty() && form14.getZahirgaaHariToo() != 0) {
                    statement.setLong(stIndex++, form14.getZahirgaaHariToo());
                }

            /* Tuluvlugu */
                if (form14.getTorguuliHemjee() != null && !form14.getTorguuliHemjee().toString().isEmpty() && form14.getTorguuliHemjee().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form14.getTorguuliHemjee());
                }
                if (form14.getTolbor() != null && !form14.getTolbor().toString().isEmpty() && form14.getTolbor().longValue()!=0) {
                    statement.setBigDecimal(stIndex++, form14.getTolbor());
                }

            /* Guitsetgel */
                if (form14.getTorguuliar() != null && !form14.getTorguuliar().toString().isEmpty() && form14.getTorguuliar().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form14.getTorguuliar());
                }
                if (form14.getTolboroor() != null && !form14.getTolboroor().toString().toString().isEmpty() && form14.getTolboroor().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form14.getTolboroor());
                }


                if (form14 != null && form14.getAimag() != null && !form14.getAimag().isEmpty()){
                    statement.setString(stIndex++, form14.getAimag());
                }
                if (form14 != null && form14.getSum() != null && !form14.getSum().isEmpty()){
                    statement.setString(stIndex++, form14.getSum());
                }

                if (form14 != null && form14.getRecordDate() != null && form14.getSearchRecordDate() != null){
                    statement.setDate(stIndex++, form14.getRecordDate());
                    statement.setDate(stIndex++, form14.getSearchRecordDate());
                }
                if (form14 != null && form14.getCREBY() != null && !form14.getCREBY().isEmpty()){
                    statement.setString(stIndex++, form14.getCREBY());
                }
                ResultSet resultSet = statement.executeQuery();

                form14EntitieList = new ArrayList<>();

                int index = 1;
                while (resultSet.next()) {
                    Form14Entity entity = new Form14Entity();

                    entity.setId(resultSet.getLong("id"));
                    entity.setShalgahGazar(resultSet.getString("shalgahGazar"));
                    entity.setShalgahGazar_en(resultSet.getString("shalgahGazar_en"));
                    entity.setUzlegToo(resultSet.getLong("uzlegToo"));
                    entity.setBaiguullagaToo(resultSet.getLong("baiguullagaToo"));
                    entity.setAjAhuinNegjToo(resultSet.getLong("ajAhuinNegjToo"));
                    entity.setIrgedToo(resultSet.getLong("irgedToo"));
                    entity.setHuuliinBaigShiljsen(resultSet.getLong("huuliinBaigShiljsen"));
                    entity.setZahirgaaHariToo(resultSet.getLong("zahirgaaHariToo"));
                    entity.setTorguuliHemjee(resultSet.getBigDecimal("torguuliHemjee"));
                    entity.setTolbor(resultSet.getBigDecimal("tolbor"));
                    entity.setTorguuliar(resultSet.getBigDecimal("torguuliar"));
                    entity.setTolboroor(resultSet.getBigDecimal("tolboroor"));
                    entity.setDelflg(resultSet.getString("delflg"));
                    entity.setActflg(resultSet.getString("actflg"));
                    entity.setModat(resultSet.getDate("mod_at"));
                    entity.setModby(resultSet.getString("mod_by"));
                    entity.setCreat(resultSet.getDate("cre_at"));
                    entity.setRecordDate(resultSet.getDate("recorddate"));
                    entity.setCreby(resultSet.getString("cre_by"));
                    entity.setAimag(resultSet.getString("aimag"));
                    entity.setSum(resultSet.getString("sum"));
                    entity.setAimag_en(resultSet.getString("aimag_en"));
                    entity.setSum_en(resultSet.getString("sum_en"));

                    entity.put("sum_latitude", resultSet.getString("sum_latitude"));
                    entity.put("sum_longitude", resultSet.getString("sum_longitude"));
                    entity.put("index",index++);
                    //entity.setMalTurulNer(comboService.getComboName(resultSet.getString("mal_turul_mn"), ContextHelper.getLanguageCode()));

                    form14EntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return form14EntitieList;
    }

    @Override
    public ErrorEntity updateData(Form14Entity form14) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (form14 == null) {
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        query = "UPDATE `hospital`.`hform14` " +
                "SET     ";

        if (form14.getShalgahGazar() != null && !form14.getShalgahGazar().isEmpty()){
            query += " shalgahGazar = ?, ";
        }

        if (form14.getShalgahGazar_en() != null && !form14.getShalgahGazar_en().isEmpty()){
            query += " shalgahGazar_en = ?, ";
        }

        /* NER */
        if (form14.getUzlegToo() != null && !form14.getUzlegToo().toString().isEmpty() && form14.getUzlegToo() != 0){
            query += " uzlegToo = ?, ";
        }
        if (form14.getBaiguullagaToo() != null && !form14.getBaiguullagaToo().toString().isEmpty() && form14.getBaiguullagaToo() !=0 ){
            query += " baiguullagaToo = ?, ";
        }
        /* Zartsuulsan hemjee */
        if (form14.getAjAhuinNegjToo() != null && !form14.getAjAhuinNegjToo().toString().isEmpty() && form14.getAjAhuinNegjToo() !=0){
            query += " ajAhuinNegjToo = ?, ";
        }
        if (form14.getIrgedToo() != null && !form14.getIrgedToo().toString().isEmpty() && form14.getIrgedToo() != 0){
            query += " irgedToo = ?, ";
        }

        /* MAL TURUL */
        if (form14.getHuuliinBaigShiljsen() != null && !form14.getHuuliinBaigShiljsen().toString().isEmpty() && form14.getHuuliinBaigShiljsen() != 0){
            query += " huuliinBaigShiljsen = ?, ";
        }
        if (form14.getZahirgaaHariToo() != null && !form14.getZahirgaaHariToo().toString().isEmpty() && form14.getZahirgaaHariToo() != 0){
            query += " zahirgaaHariToo = ?, ";
        }

        /* Tuluvlugu */
        if (form14.getTorguuliHemjee() != null && !form14.getTorguuliHemjee().toString().isEmpty()  && form14.getTorguuliHemjee().longValue() != 0){
            query += " torguuliHemjee = ?,";
        }
        if (form14.getTolbor() != null && !form14.getTolbor().toString().isEmpty() && form14.getTolbor().longValue()!=0){
            query += " tolbor = ?, ";
        }

        /* Guitsetgel */
        if (form14.getTorguuliar() != null && !form14.getTorguuliar().toString().isEmpty() && form14.getTorguuliar().longValue() != 0){
            query += " torguuliar = ?, ";
        }
        if (form14.getTolboroor() != null && !form14.getTolboroor().toString().isEmpty() && form14.getTolboroor().longValue() != 0){
            query += " tolboroor = ?,";
        }

        /* Delete flag */
        if (form14.getDELFLG() != null && !form14.getDELFLG().toString().isEmpty()){
            query += " delflg = ?, ";
        }

        /* Active flag */
        if (form14.getACTFLG() != null && !form14.getACTFLG().toString().isEmpty()){
            query += " actflg = ?, ";
        }
        if (form14 != null && form14.getRecordDate() != null){
            query += " `recorddate` = ?, ";
        }
        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";
        query += " WHERE  ID   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (form14.getShalgahGazar() != null && !form14.getShalgahGazar().isEmpty()) {
                    statement.setString(stIndex++, form14.getShalgahGazar());
                }

                if (form14.getShalgahGazar_en() != null && !form14.getShalgahGazar_en().isEmpty()) {
                    statement.setString(stIndex++, form14.getShalgahGazar_en());
                }

            /* NER */
                if (form14.getUzlegToo() != null && !form14.getUzlegToo().toString().isEmpty() && form14.getUzlegToo() != 0) {
                    statement.setLong(stIndex++, form14.getUzlegToo());
                }
                if (form14.getBaiguullagaToo() != null && !form14.getBaiguullagaToo().toString().isEmpty() && form14.getBaiguullagaToo() !=0) {
                    statement.setDouble(stIndex++, form14.getBaiguullagaToo());
                }

            /* Zartsuulsan hemjee */
                if (form14.getAjAhuinNegjToo() != null && !form14.getAjAhuinNegjToo().toString().isEmpty() && form14.getAjAhuinNegjToo() !=0) {
                    statement.setLong(stIndex++, form14.getAjAhuinNegjToo());
                }
                if (form14.getIrgedToo() != null && !form14.getIrgedToo().toString().isEmpty() && form14.getIrgedToo() != 0) {
                    statement.setLong(stIndex++, form14.getIrgedToo());
                }

            /* MAL TURUL */
                if (form14.getHuuliinBaigShiljsen() != null && !form14.getHuuliinBaigShiljsen().toString().isEmpty() && form14.getHuuliinBaigShiljsen() != 0) {
                    statement.setLong(stIndex++, form14.getHuuliinBaigShiljsen());
                }
                if (form14.getZahirgaaHariToo() != null && !form14.getZahirgaaHariToo().toString().isEmpty() && form14.getZahirgaaHariToo() != 0) {
                    statement.setLong(stIndex++, form14.getZahirgaaHariToo());
                }

            /* Tuluvlugu */
                if (form14.getTorguuliHemjee() != null && !form14.getTorguuliHemjee().toString().isEmpty() && form14.getTorguuliHemjee().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form14.getTorguuliHemjee());
                }
                if (form14.getTolbor() != null && !form14.getTolbor().toString().isEmpty() && form14.getTolbor().longValue()!=0) {
                    statement.setBigDecimal(stIndex++, form14.getTolbor());
                }

            /* Guitsetgel */
                if (form14.getTorguuliar() != null && !form14.getTorguuliar().toString().isEmpty() && form14.getTorguuliar().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form14.getTorguuliar());
                }
                if (form14.getTolboroor() != null && !form14.getTolboroor().toString().toString().isEmpty() && form14.getTolboroor().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form14.getTolboroor());
                }

            /* Delete flag */
                if (form14.getDELFLG() != null && !form14.getDELFLG().toString().isEmpty()) {
                    statement.setString(stIndex++, form14.getDELFLG());
                }

            /* Active flag */
                if (form14.getACTFLG() != null && !form14.getACTFLG().toString().isEmpty()) {
                    statement.setString(stIndex++, form14.getACTFLG());
                }

                if (form14 != null && form14.getRecordDate() != null){
                    statement.setDate(stIndex++, form14.getRecordDate());
                }
                statement.setDate(stIndex++, form14.getMODAT());
                statement.setString(stIndex++, form14.getMODBY());
                statement.setLong(stIndex++, form14.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                    errObj.setErrText("Бичлэг амжилттай засагдлаа.");
                    errObj.setErrDesc("Бичлэг амжилттай засагдлаа.");
                    return errObj;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1061));
        errObj.setErrText("Амжилтгүй боллоо. Дахин оролдоно уу.");
        errObj.setErrDesc("Амжилтгүй боллоо. Дахин оролдоно уу.");

        return errObj;
    }

    @Override
    public List<Form14Entity> getPieChartData(Form14Entity form14Entity) throws SQLException {
        int stIndex = 1;
        List<Form14Entity> form14EntitieList = null;

        String query_sel = "SELECT `hform14`.`id`,  " +
                "    `hform14`.`aimag`,  " +
                "    `hform14`.`sum`,  " +
                "    SUM(`hform14`.`zartsuulsan_hemjee_mn`) as zartsuulsan_hemjee,  " +
                "    c.cityname, c.cityname_en,  " +
                "    `h_sum`.sumname, `h_sum`.sumname_en  " +
                "FROM `hospital`.`hform14`  " +
                "INNER JOIN `h_city` AS c  " +
                "ON c.code = `hform14`.aimag  " +
                "INNER JOIN `h_sum`  " +
                "ON `h_sum`.sumcode = `hform14`.sum  " +
                "WHERE `hform14`.`delflg` = 'N' and `hform14`.`actflg` = 'Y' and `hform14`.`aimag` = ?  " +
                "GROUP BY `hform14`.`sum` ";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                if (form14Entity.getAimag() != null && !form14Entity.getAimag().isEmpty()) {
                    statement.setString(stIndex++, form14Entity.getAimag());
                }

                ResultSet resultSet = statement.executeQuery();

                form14EntitieList = new ArrayList<>();

                while (resultSet.next()) {
                    Form14Entity entity = new Form14Entity();

                    entity.setId(resultSet.getLong("id"));
                    entity.setAimag(resultSet.getString("aimag"));
                    entity.setSum(resultSet.getString("sum"));
//                    entity.setAimagNer(resultSet.getString("cityname"));
//                    entity.setSumNer(resultSet.getString("sumname"));

                    form14EntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return form14EntitieList;
    }
    @Override
    public ErrorEntity exportReport(String file, Form14Entity form14) throws SQLException {
        ErrorEntity errorEntity = null;
        String fileName = "hform14";

        try (Connection connection = boneCP.getConnection()){
            String JRXML_PATH = Config.getJrxmlPath();
            String REPORT_PATH = Config.getReportPath();

            try {
                Map parameters = new HashMap();

                String reportTitle = "МАЛ ЭМНЭЛГИЙН АРИУН ЦЭВРИЙН ҮЗЛЭГ ШАЛГАЛТЫН $$YEAR$$ ОНЫ $$SEASON$$ -Р УЛИРАЛЫН МЭДЭЭ",
                        currentYear = "$$YEAR$$ он №....",
                        printDate = "$$YEAR$$ оны $$MONTH$$ сарын $$DAY$$";

                Calendar now = Calendar.getInstance();

                int year = now.get(Calendar.YEAR);
                int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
                int day = now.get(Calendar.DAY_OF_MONTH);

                printDate = printDate.replace("$$YEAR$$", String.valueOf(year));
                printDate = printDate.replace("$$MONTH$$", String.valueOf(month));
                printDate = printDate.replace("$$DAY$$", String.valueOf(day));

                currentYear = currentYear.replace("$$YEAR$$", String.valueOf(year));

                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(new Date(form14.getRecordDate().getTime()));

                reportTitle = reportTitle.replace("$$YEAR$$", String.valueOf(calendar1.get(Calendar.YEAR)));

                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(new Date(form14.getSearchRecordDate().getTime()));

                if (calendar2.get(Calendar.MONTH) < 4){
                    reportTitle = reportTitle.replace("$$SEASON$$", "1");
                }
                else if (calendar2.get(Calendar.MONTH) < 7){
                    reportTitle = reportTitle.replace("$$SEASON$$", "2");
                }
                else if (calendar2.get(Calendar.MONTH) < 10){
                    reportTitle = reportTitle.replace("$$SEASON$$", "3");
                }
                else if (calendar2.get(Calendar.MONTH) <= 12){
                    reportTitle = reportTitle.replace("$$SEASON$$", "4");
                }

                parameters.put("start_date", form14.getRecordDate());
                parameters.put("end_date", form14.getSearchRecordDate());
                parameters.put("citycode", (form14.getAimag() != null && !form14.getAimag().isEmpty()) ? form14.getAimag() : null);
                parameters.put("sumcode", (form14.getSum() != null && !form14.getSum().isEmpty()) ? form14.getSum() : null);
                parameters.put("currentyear", currentYear);

                ICityService cityService = new CityServiceImpl(boneCP);
                City tmpCity = new City();
                tmpCity.setCode(form14.getAimag());
                List<City> clist = cityService.selectAll(tmpCity);

                if (clist != null && clist.size() == 1) {
                    parameters.put("aimagner", clist.get(0).getCityname());
                } else {
                    parameters.put("aimagner", (form14.getAimagNer() == null) ? "" : form14.getAimagNer());
                }
                ISumService sumService = new SumServiceImpl(boneCP);
                Sum tmpSum = new Sum();
                tmpSum.setCitycode(form14.getAimag());
                tmpSum.setSumcode(form14.getSum());
                List<Sum> sumList = sumService.selectAll(tmpSum);

                if (sumList != null && sumList.size() == 1){
                    parameters.put("sumner", sumList.get(0).getSumname());
                }
                else{
                    parameters.put("sumner", (form14.getSumNer() == null) ? "" : form14.getSumNer());
                }

                parameters.put("printdate", printDate);
                parameters.put("reporttitle", reportTitle);
                parameters.put("creby", (form14.getCREBY() != null && !form14.getCREBY().isEmpty()) ? form14.getCREBY() : null);

                PdfExcelUtil.generateReport(file, JRXML_PATH, REPORT_PATH, fileName, parameters, connection);

                System.out.println("File Generated");
            }catch (Exception ex){
                ex.printStackTrace();
            }
            finally {
                if (connection != null)connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1001));
            if(file.equals("pdf")){
                errorEntity.setErrText(fileName + ".pdf");
            }else if(file.equals("xls")){
                errorEntity.setErrText(fileName + ".xls");
            }
        }
        return errorEntity;
    }

    @Override
    public BasicDBObject getColHighChartData(Form14Entity entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();
        List<String> categories = new ArrayList<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }

        try {

            IComboService comboService = new ComboServiceImpl(boneCP);

            List<BasicDBObject> cmbType = comboService.getComboNames("BAIGUULLAGA", "MN");
            System.out.println(cmbType);
            //Saraar n haruulah
            int month = calendar1.get(Calendar.MONTH) + 1;
            int year1 = calendar1.get(Calendar.YEAR);
            int year2 = calendar2.get(Calendar.YEAR);

            try (Connection connection = boneCP.getConnection()) {
                String query_sel = "SELECT `shalgahGazar`, sum(`uzlegToo`) AS uzleg,  sum(`baiguullagaToo`) AS baigullaga," +
                        "sum(`ajAhuinNegjToo`) AS ajahui,  sum(`irgedToo`) AS irged, `hform14`.`aimag`, `hform14`.`sum`  " +
                        "FROM `hospital`.`hform14`  " +
                        "WHERE   ";

                if (entity != null && entity.getAimag() != null &&!entity.getAimag().isEmpty()) {
                    query_sel += " `hform14`.`aimag` = ? AND ";
                }
                if (entity != null && entity.getSum() != null && !entity.getSum().isEmpty()) {
                    query_sel += " `hform14`.`sum` = ? AND ";
                }
                if (entity != null && entity.getShalgahGazar() != null && !entity.getShalgahGazar().isEmpty()) {
                    query_sel += " `hform14`.`shalgahGazar` = ? AND ";
                }
                if (entity != null && !entity.getDELFLG().isEmpty()) {
                    query_sel += " `hform14`.`delflg` = ? AND ";
                }
                if (entity != null && !entity.getACTFLG().isEmpty()) {
                    query_sel += " `hform14`.`actflg` = ? AND ";
                }
                if (entity != null && !entity.getCREBY().isEmpty()) {
                    query_sel += " `hform14`.`cre_by` = ? AND ";
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                    query_sel += " (`recorddate` BETWEEN ? AND ?)";
                }

                HashMap<String, List<Double>> resultArray = new HashMap<>();


                do {
                    //do process
                    categories.add(year1 + "-" + month);

                    try {
                        PreparedStatement statement = connection.prepareStatement(query_sel);

                        java.sql.Date stdate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-01 00:00:00").getTime());

                        java.sql.Date enddate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-31 00:00:00").getTime());

                        if (month == 12 && year1 < year2) {
                            month = 1;
                            year1++;
                        } else {
                            month++;
                        }

                        int stindex = 1;

                        if (entity != null && entity.getAimag() != null  && !entity.getAimag().isEmpty()) {
                            statement.setString(stindex++, entity.getAimag());
                        }
                        if (entity != null && entity.getSum() != null  && !entity.getSum().isEmpty()) {
                            statement.setString(stindex++, entity.getSum());
                        }
                        if (entity != null && entity.getShalgahGazar() != null  && !entity.getShalgahGazar().isEmpty()) {
                            statement.setString(stindex++, entity.getShalgahGazar());
                        }
                        if (entity != null && !entity.getDELFLG().isEmpty()) {
                            statement.setString(stindex++, entity.getDELFLG());
                        }
                        if (entity != null && !entity.getACTFLG().isEmpty()) {
                            statement.setString(stindex++, entity.getACTFLG());
                        }
                        if (entity != null && !entity.getCREBY().isEmpty()) {
                            statement.setString(stindex++, entity.getCREBY());
                        }
                        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                            statement.setDate(stindex++, stdate);
                            statement.setDate(stindex++, enddate);
                        }


                        ResultSet resultSet = statement.executeQuery();

                        HashMap<String, String> hashMap = new HashMap<>();

                        while (resultSet.next()) {
                            String uzleg = resultSet.getString("uzleg");
                            String irged = resultSet.getString("irged");
                            String baigullaga = resultSet.getString("baigullaga");
                            String ajahui = resultSet.getString("ajahui");
                            hashMap.put("uzlegToo", uzleg);
                            hashMap.put("irgedToo", irged);
                            hashMap.put("baiguullagaToo", baigullaga);
                            hashMap.put("ajAhuinNegjToo", ajahui);
                        }//end while

                        List<Double> list = new ArrayList<>();
                        for (int i = 0; i < cmbType.size(); i++) {
                            BasicDBObject obj = cmbType.get(i);

                            if (obj.get("data") == null){
                                //new record
                                list = new ArrayList<>();
                                String val = hashMap.get(obj.getString("id"));
                                if (val == null) list.add(0.0);
                                else if (val.isEmpty()) list.add(0.0);
                                else list.add(Double.parseDouble(val));
                                obj.put("data", list);
                            }else{
                                //already exist

                                list = (List<Double>) obj.get("data");
                                String val = hashMap.get(obj.getString("id"));
                                if (val == null) list.add(0.0);
                                else if (val.isEmpty()) list.add(0.0);
                                else list.add(Double.parseDouble(val));
                            }
                        }

                        if (statement != null) statement.close();
                        if (resultSet != null) resultSet.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();

                        break;
                    }

                    ///
                } while ((month <= calendar2.get(Calendar.MONTH) + 1 && year1 <= year2) || year1 < year2);

                basicDBObject.put("categories", categories);
                basicDBObject.put("series", cmbType);

                connection.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }

    @Override
    public BasicDBObject getPieHighChartData(Form14Entity entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }

        String query_sel = "SELECT `shalgahGazar`, sum(`uzlegToo`) AS uzleg,  sum(`baiguullagaToo`) AS baigullaga," +
                "sum(`ajAhuinNegjToo`) AS ajahui,  sum(`irgedToo`) AS irged, `hform14`.`aimag`, `hform14`.`sum`  " +
                "FROM `hospital`.`hform14`  " +
                "WHERE   ";

        if (entity != null && entity.getAimag() != null &&!entity.getAimag().isEmpty()) {
            query_sel += " `hform14`.`aimag` = ? AND ";
        }
        if (entity != null && entity.getSum() != null && !entity.getSum().isEmpty()) {
            query_sel += " `hform14`.`sum` = ? AND ";
        }
        if (entity != null && entity.getShalgahGazar() != null && !entity.getShalgahGazar().isEmpty()) {
            query_sel += " `hform14`.`shalgahGazar` = ? AND ";
        }
        if (entity != null && !entity.getDELFLG().isEmpty()) {
            query_sel += " `hform14`.`delflg` = ? AND ";
        }
        if (entity != null && !entity.getACTFLG().isEmpty()) {
            query_sel += " `hform14`.`actflg` = ? AND ";
        }
        if (entity != null && !entity.getCREBY().isEmpty()) {
            query_sel += " `hform14`.`cre_by` = ? AND ";
        }
        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
            query_sel += " (`recorddate` BETWEEN ? AND ?)";
        }

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                int stindex = 1;

                if (entity != null && entity.getAimag() != null  && !entity.getAimag().isEmpty()) {
                    statement.setString(stindex++, entity.getAimag());
                }
                if (entity != null && entity.getSum() != null  && !entity.getSum().isEmpty()) {
                    statement.setString(stindex++, entity.getSum());
                }
                if (entity != null && entity.getShalgahGazar() != null  && !entity.getShalgahGazar().isEmpty()) {
                    statement.setString(stindex++, entity.getShalgahGazar());
                }
                if (entity != null && !entity.getDELFLG().isEmpty()) {
                    statement.setString(stindex++, entity.getDELFLG());
                }
                if (entity != null && !entity.getACTFLG().isEmpty()) {
                    statement.setString(stindex++, entity.getACTFLG());
                }
                if (entity != null && !entity.getCREBY().isEmpty()) {
                    statement.setString(stindex++, entity.getCREBY());
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                    statement.setDate(stindex++, entity.getRecordDate());
                    statement.setDate(stindex++, entity.getSearchRecordDate());
                }
                ResultSet resultSet = statement.executeQuery();

                List<BasicDBObject> list = new ArrayList<>();

                while (resultSet.next()) {
                    BasicDBObject entity1 = new BasicDBObject();
                    BasicDBObject entity2 = new BasicDBObject();
                    BasicDBObject entity3 = new BasicDBObject();
                    BasicDBObject entity4 = new BasicDBObject();
                    entity1.put("name", "Үзлэгийн тоо");
                    entity1.put("y", resultSet.getDouble("uzleg"));
                    entity2.put("name", "Байгууллагын тоо");
                    entity2.put("y", resultSet.getDouble("baigullaga"));
                    entity3.put("name", "Иргэдийн тоо");
                    entity3.put("y", resultSet.getDouble("irged"));
                    entity4.put("name", "Аж ахуйн нэгжийн тоо");
                    entity4.put("y", resultSet.getDouble("ajahui"));
                    list.add(entity1);
                    list.add(entity2);
                    list.add(entity3);
                    list.add(entity4);
                }

                basicDBObject.put("data", list);

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }
}
