package com.hospital.app;


import com.Config;
import com.enumclass.ErrorType;
import com.jolbox.bonecp.BoneCP;
import com.model.hos.City;
import com.model.hos.ErrorEntity;
import com.model.hos.Form08HavsraltEntity;
import com.model.hos.Sum;
import com.mongodb.BasicDBObject;
import com.rpc.PdfExcelUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public class Form08HavsraltServiceImpl implements IForm08HavsraltService {
    /**
     * Objects
     */

    private BoneCP boneCP;


    public Form08HavsraltServiceImpl(BoneCP boneCP) {
        this.boneCP = boneCP;
    }

    @Override
    public ErrorEntity insertNewData(Form08HavsraltEntity form08) throws SQLException {
        PreparedStatement statement = null;

        if (form08 == null) new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));

        String query_login = "INSERT INTO `hospital`.`hform08_hav` " +
                "(`citycode`, " +
                "`sumcode`, " +
                "`bagcode`, " +
                "`gazarner`, " +
                "`urtrag`, " +
                "`orgorog`, " +
                "`golomttoo`, " +
                "`ovchin_ner`, " +
                "`mal_turul`, " +
                "`uvchilson`, " +
                "`uhsen`, " +
                "`edgersen`, " +
                "`ustgasan`, " +
                "`nydluulsan`, " +
                "`lab_ner`, " +
                "`shinjilgee_arga`, " +
                "`delflg`, " +
                "`cre_by`, " +
                "`cre_at`, " +
                "`mod_at`, " +
                "`mod_by`, " +
                "`record_date`) " +
                "VALUES " +
                "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?)";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, form08.getCitycode());
                statement.setString(2, form08.getSumcode());
                statement.setString(3, form08.getBagcode());
                statement.setString(4, form08.getGazarner());
                statement.setString(5, form08.getUrtrag());
                statement.setString(6, form08.getOrgorog());
                statement.setInt(7, form08.getGolomttoo());
                statement.setString(8, form08.getOvchin_ner());
                statement.setString(9, form08.getMal_turul());
                statement.setInt(10, form08.getUvchilson());
                statement.setInt(11, form08.getUhsen());
                statement.setInt(12, form08.getEdgersen());
                statement.setInt(13, form08.getUstgasan());
                statement.setInt(14, form08.getNydluulsan());
                statement.setString(15, form08.getLab_ner());
                statement.setString(16, form08.getShinjilgee_arga());
                statement.setString(17, form08.getDelflg());
                statement.setString(18, form08.getCreby());
                statement.setDate(19, form08.getCreat());
                statement.setDate(20, form08.getModAt());
                statement.setString(21, form08.getModby());
                statement.setDate(22, form08.getRecordDate());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    return new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }

        } catch (Exception ex) {

        }

        return new ErrorEntity(ErrorType.ERROR, Long.valueOf(1057));
    }

    @Override
    public List<Form08HavsraltEntity> getListData(Form08HavsraltEntity form08) throws SQLException {
        int stIndex = 1;
        List<Form08HavsraltEntity> form08EntitieList = null;

        String query = "SELECT `hform08_hav`.`id`, " +
                "    `hform08_hav`.`citycode`, " +
                "    `hform08_hav`.`sumcode`, " +
                "    `hform08_hav`.`bagcode`, " +
                "    `hform08_hav`.`gazarner`, " +
                "    `hform08_hav`.`urtrag`, " +
                "    `hform08_hav`.`orgorog`, " +
                "    `hform08_hav`.`golomttoo`, " +
                "    `hform08_hav`.`ovchin_ner`, " +
                "    `hform08_hav`.`mal_turul`, " +
                "    `hform08_hav`.`uvchilson`, " +
                "    `hform08_hav`.`uhsen`, " +
                "    `hform08_hav`.`edgersen`, " +
                "    `hform08_hav`.`ustgasan`, " +
                "    `hform08_hav`.`nydluulsan`, " +
                "    `hform08_hav`.`lab_ner`, " +
                "    `hform08_hav`.`shinjilgee_arga`, " +
                "    `hform08_hav`.`delflg`, " +
                "    `hform08_hav`.`cre_by`, " +
                "    `hform08_hav`.`cre_at`, " +
                "    `hform08_hav`.`mod_at`, " +
                "    `hform08_hav`.`mod_by`, " +
                "    `hform08_hav`.`record_date`, " +
                "    c.cityname, c.cityname_en, " +
                "    `h_sum`.sumname, `h_sum`.sumname_en, " +
                "   `h_baghoroo`.horooname, `h_baghoroo`.horooname_en " +
                "FROM `hospital`.`hform08_hav` INNER JOIN `h_city` AS c " +
                "ON c.code = `hform08_hav`.citycode " +
                "INNER JOIN `h_sum` " +
                "ON `h_sum`.sumcode = `hform08_hav`.sumcode " +
                "INNER JOIN `h_baghoroo` ON `hform08_hav`.bagcode = `h_baghoroo`.horoocode AND `h_baghoroo`.sumcode = `hform08_hav`.`sumcode` " +
                "WHERE ";

        if (form08.getCitycode() != null && !form08.getCitycode().isEmpty()) {
            query += " `hform08_hav`.citycode = ? AND ";
        }

        if (form08.getSumcode() != null && !form08.getSumcode().isEmpty()) {
            query += " `hform08_hav`.sumcode = ? AND ";
        }

        if (form08.getBagcode() != null && !form08.getBagcode().toString().isEmpty()) {
            query += " `hform08_hav`.bagcode = ? AND ";
        }
        if (form08.getGazarner() != null && !form08.getGazarner().toString().isEmpty()) {
            query += " `hform08_hav`.gazarner = ? AND ";
        }
        if (form08.getUrtrag() != null && !form08.getUrtrag().toString().isEmpty()) {
            query += " `hform08_hav`.urtrag = ? AND ";
        }
        if (form08.getOrgorog() != null && !form08.getOrgorog().toString().isEmpty()) {
            query += " `hform08_hav`.orgorog = ? AND ";
        }

        if (form08.getGolomttoo() != 0) {
            query += " `hform08_hav`.golomttoo = ? AND ";
        }
        if (form08.getOvchin_ner() != null && !form08.getOvchin_ner().toString().isEmpty()) {
            query += " `hform08_hav`.ovchin_ner = ? AND ";
        }

        if (form08.getMal_turul() != null && !form08.getMal_turul().isEmpty()) {
            query += " `hform08_hav`.mal_turul = ? AND ";
        }
        if (form08.getUvchilson() != 0) {
            query += " `hform08_hav`.uvchilson = ? AND ";
        }

        if (form08.getUhsen() != 0) {
            query += " `hform08_hav`.uhsen = ? AND ";
        }
        if (form08.getEdgersen() != 0) {
            query += " `hform08_hav`.edgersen = ? AND ";
        }

        if (form08.getUstgasan() != 0) {
            query += " `hform08_hav`.ustgasan = ? AND  ";
        }
        if (form08.getNydluulsan() != 0) {
            query += " `hform08_hav`.nydluulsan = ? AND ";
        }

        if (form08 != null && form08.getLab_ner() != null && !form08.getLab_ner().isEmpty()) {
            query += " `hform08_hav`.`lab_ner` = ? AND ";
        }
        if (form08 != null && form08.getShinjilgee_arga() != null && !form08.getShinjilgee_arga().isEmpty()) {
            query += " `hform08_hav`.`shinjilgee_arga` = ? AND ";
        }

        if (form08 != null && form08.getRecordDate() != null && form08.getSearchRecordDate() != null) {
            query += " (`hform08_hav`.`record_date` BETWEEN ? AND ?) AND ";
        }

        if (form08 != null && form08.getCreby() != null && !form08.getCreby().isEmpty()) {
            query += " `hform08_hav`.`cre_by` = ? AND ";
        }

        if (form08 != null && form08.getDelflg() != null && !form08.getDelflg().isEmpty()) {
            query += " `hform08_hav`.`delflg` = ? AND ";
        }

        if (form08 != null && form08.getCreat() != null && form08.getSearchRecordDate() != null) {
            query += " (`hform08_hav`.`cre_at` BETWEEN ? AND ?) AND ";
        }

        query += " `hform08_hav`.`id` > 0 ";
        query += " ORDER BY `hform08_hav`.`cre_at` DESC ";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query);

                if (form08.getCitycode() != null && !form08.getCitycode().isEmpty()) {
                    statement.setString(stIndex++, form08.getCitycode());
                }

                if (form08.getSumcode() != null && !form08.getSumcode().isEmpty()) {
                    statement.setString(stIndex++, form08.getSumcode());
                }

                if (form08.getBagcode() != null && !form08.getBagcode().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getBagcode());
                }
                if (form08.getGazarner() != null && !form08.getGazarner().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getGazarner());
                }
                if (form08.getUrtrag() != null && !form08.getUrtrag().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getUrtrag());
                }
                if (form08.getOrgorog() != null && !form08.getOrgorog().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getOrgorog());
                }

                if (form08.getGolomttoo() != 0) {
                    statement.setInt(stIndex++, form08.getGolomttoo());
                }
                if (form08.getOvchin_ner() != null && !form08.getOvchin_ner().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getOvchin_ner());
                }

                if (form08.getMal_turul() != null && !form08.getMal_turul().isEmpty()) {
                    statement.setString(stIndex++, form08.getMal_turul());
                }
                if (form08.getUvchilson() != 0) {
                    statement.setInt(stIndex++, form08.getUvchilson());
                }

                if (form08.getUhsen() != 0) {
                    statement.setInt(stIndex++, form08.getUhsen());
                }
                if (form08.getEdgersen() != 0) {
                    statement.setInt(stIndex++, form08.getEdgersen());
                }

                if (form08.getUstgasan() != 0) {
                    statement.setInt(stIndex++, form08.getUstgasan());
                }
                if (form08.getNydluulsan() != 0) {
                    statement.setInt(stIndex++, form08.getNydluulsan());
                }

                if (form08 != null && form08.getLab_ner() != null && !form08.getLab_ner().isEmpty()) {
                    statement.setString(stIndex++, form08.getLab_ner());
                }
                if (form08 != null && form08.getShinjilgee_arga() != null && !form08.getShinjilgee_arga().isEmpty()) {
                    statement.setString(stIndex++, form08.getShinjilgee_arga());
                }

                if (form08 != null && form08.getRecordDate() != null && form08.getSearchRecordDate() != null) {
                    statement.setDate(stIndex++, form08.getRecordDate());
                    statement.setDate(stIndex++, form08.getSearchRecordDate());
                }

                if (form08 != null && form08.getCreby() != null && !form08.getCreby().isEmpty()) {
                    statement.setString(stIndex++, form08.getCreby());
                }

                if (form08 != null && form08.getDelflg() != null && !form08.getDelflg().isEmpty()) {
                    statement.setString(stIndex++, form08.getDelflg());
                }

                if (form08 != null && form08.getCreat() != null && form08.getSearchRecordDate() != null) {
                    statement.setDate(stIndex++, form08.getCreat());
                    statement.setDate(stIndex++, form08.getSearchRecordDate());
                }

                ResultSet resultSet = statement.executeQuery();

                form08EntitieList = new ArrayList<>();

                IComboService comboService = new ComboServiceImpl(boneCP);
                List<BasicDBObject> cmbType = comboService.getComboVals("MAL", "MN");
                List<BasicDBObject> shgazarType = comboService.getComboVals("SHGAZ", "MN");
                List<BasicDBObject> shargaType = comboService.getComboVals("SHARGA", "MN");
                List<BasicDBObject> uvchinNer = comboService.getComboVals("UVCH", "MN");
                int index = 1;
                while (resultSet.next()) {
                    Form08HavsraltEntity entity = new Form08HavsraltEntity();

                    entity.setId(resultSet.getLong("id"));
                    entity.setCitycode(resultSet.getString("citycode"));
                    entity.setSumcode(resultSet.getString("sumcode"));
                    entity.setBagcode(resultSet.getString("bagcode"));
                    entity.setGazarner(resultSet.getString("gazarner"));
                    entity.setUrtrag(resultSet.getString("urtrag"));
                    entity.setOrgorog(resultSet.getString("orgorog"));
                    entity.setGolomttoo(resultSet.getInt("golomttoo"));
                    entity.setOvchin_ner(resultSet.getString("ovchin_ner"));
                    entity.setMal_turul(resultSet.getString("mal_turul"));
                    entity.setUvchilson(resultSet.getInt("uvchilson"));
                    entity.setUhsen(resultSet.getInt("uhsen"));
                    entity.setEdgersen(resultSet.getInt("edgersen"));
                    entity.setUstgasan(resultSet.getInt("ustgasan"));
                    entity.setNydluulsan(resultSet.getInt("nydluulsan"));
                    entity.setLab_ner(resultSet.getString("lab_ner"));
                    entity.setShinjilgee_arga(resultSet.getString("shinjilgee_arga"));
                    entity.setDelflg(resultSet.getString("delflg"));
                    entity.setModAt(resultSet.getDate("mod_at"));
                    entity.setModby(resultSet.getString("mod_by"));
                    entity.setCreat(resultSet.getDate("cre_at"));
                    entity.setCreby(resultSet.getString("cre_by"));
                    entity.setRecordDate(resultSet.getDate("record_date"));

                    entity.put("malturulner", getTypeNer(cmbType, resultSet.getString("mal_turul")));
                    entity.put("shinjilsenlabner", getTypeNer(shgazarType, resultSet.getString("lab_ner")));
                    entity.put("shinjilsenarga", getTypeNer(shargaType, resultSet.getString("shinjilgee_arga")));
                    entity.put("uvchinner", getTypeNer(uvchinNer, resultSet.getString("ovchin_ner")));

                    entity.setCityName(resultSet.getString("cityname"));
                    entity.setSumName(resultSet.getString("sumname"));
                    entity.setBagName(resultSet.getString("horooname"));
                    entity.put("index", index++);
                    form08EntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return form08EntitieList;
    }

    /**
     * Get name
     *
     * @param cmbType
     * @param mal_turul_mn
     * @return
     */
    private String getTypeNer(List<BasicDBObject> cmbType, String mal_turul_mn) {
        for (BasicDBObject obj :
                cmbType) {
            if (obj.get("id") != null && mal_turul_mn != null && obj.getString("id").equalsIgnoreCase(mal_turul_mn)) {
                return obj.getString("name");
            }
        }

        return "";
    }

    @Override
    public ErrorEntity updateData(Form08HavsraltEntity form08) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (form08 == null) new ErrorEntity(ErrorType.INFO, Long.valueOf(1059));

        query = "UPDATE `hospital`.`hform08_hav` " +
                "SET     ";

        if (form08.getCitycode() != null && !form08.getCitycode().isEmpty()) {
            query += " `hform08_hav`.citycode = ?, ";
        }

        if (form08.getSumcode() != null && !form08.getSumcode().isEmpty()) {
            query += " `hform08_hav`.sumcode = ?, ";
        }

        if (form08.getBagcode() != null && !form08.getBagcode().toString().isEmpty()) {
            query += " `hform08_hav`.bagcode = ?, ";
        }
        if (form08.getGazarner() != null && !form08.getGazarner().toString().isEmpty()) {
            query += " `hform08_hav`.gazarner = ?, ";
        }
        if (form08.getUrtrag() != null && !form08.getUrtrag().toString().isEmpty()) {
            query += " `hform08_hav`.urtrag = ?, ";
        }
        if (form08.getOrgorog() != null && !form08.getOrgorog().toString().isEmpty()) {
            query += " `hform08_hav`.orgorog = ?, ";
        }

        if (form08.getGolomttoo() != 0) {
            query += " `hform08_hav`.golomttoo = ?, ";
        }
        if (form08.getOvchin_ner() != null && !form08.getOvchin_ner().toString().isEmpty()) {
            query += " `hform08_hav`.ovchin_ner = ?, ";
        }

        if (form08.getMal_turul() != null && !form08.getMal_turul().isEmpty()) {
            query += " `hform08_hav`.mal_turul = ?, ";
        }
        if (form08.getUvchilson() != 0) {
            query += " `hform08_hav`.uvchilson = ?, ";
        }

        if (form08.getUhsen() != 0) {
            query += " `hform08_hav`.uhsen = ?, ";
        }
        if (form08.getEdgersen() != 0) {
            query += " `hform08_hav`.edgersen = ?, ";
        }

        if (form08.getUstgasan() != 0) {
            query += " `hform08_hav`.ustgasan = ?,  ";
        }
        if (form08.getNydluulsan() != 0) {
            query += " `hform08_hav`.nydluulsan = ?, ";
        }

        if (form08 != null && form08.getLab_ner() != null && !form08.getLab_ner().isEmpty()) {
            query += " `hform08_hav`.`lab_ner` = ?, ";
        }
        if (form08 != null && form08.getShinjilgee_arga() != null && !form08.getShinjilgee_arga().isEmpty()) {
            query += " `hform08_hav`.`shinjilgee_arga` = ?, ";
        }

        if (form08 != null && form08.getRecordDate() != null) {
            query += " `hform08_hav`.`record_date` = ?, ";
        }

        if (form08 != null && form08.getDelflg() != null && !form08.getDelflg().isEmpty()) {
            query += " `hform08_hav`.`delflg` = ?, ";
        }

        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";
        query += " WHERE  ID   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (form08.getCitycode() != null && !form08.getCitycode().isEmpty()) {
                    statement.setString(stIndex++, form08.getCitycode());
                }

                if (form08.getSumcode() != null && !form08.getSumcode().isEmpty()) {
                    statement.setString(stIndex++, form08.getSumcode());
                }

                if (form08.getBagcode() != null && !form08.getBagcode().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getBagcode());
                }
                if (form08.getGazarner() != null && !form08.getGazarner().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getGazarner());
                }
                if (form08.getUrtrag() != null && !form08.getUrtrag().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getUrtrag());
                }
                if (form08.getOrgorog() != null && !form08.getOrgorog().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getOrgorog());
                }

                if (form08.getGolomttoo() != 0) {
                    statement.setInt(stIndex++, form08.getGolomttoo());
                }
                if (form08.getOvchin_ner() != null && !form08.getOvchin_ner().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getOvchin_ner());
                }

                if (form08.getMal_turul() != null && !form08.getMal_turul().isEmpty()) {
                    statement.setString(stIndex++, form08.getMal_turul());
                }
                if (form08.getUvchilson() != 0) {
                    statement.setInt(stIndex++, form08.getUvchilson());
                }

                if (form08.getUhsen() != 0) {
                    statement.setInt(stIndex++, form08.getUhsen());
                }
                if (form08.getEdgersen() != 0) {
                    statement.setInt(stIndex++, form08.getEdgersen());
                }

                if (form08.getUstgasan() != 0) {
                    statement.setInt(stIndex++, form08.getUstgasan());
                }
                if (form08.getNydluulsan() != 0) {
                    statement.setInt(stIndex++, form08.getNydluulsan());
                }

                if (form08 != null && form08.getLab_ner() != null && !form08.getLab_ner().isEmpty()) {
                    statement.setString(stIndex++, form08.getLab_ner());
                }
                if (form08 != null && form08.getShinjilgee_arga() != null && !form08.getShinjilgee_arga().isEmpty()) {
                    statement.setString(stIndex++, form08.getShinjilgee_arga());
                }

                if (form08 != null && form08.getRecordDate() != null) {
                    statement.setDate(stIndex++, form08.getRecordDate());
                }

                if (form08 != null && form08.getDelflg() != null && !form08.getDelflg().isEmpty()) {
                    statement.setString(stIndex++, form08.getDelflg());
                }

                statement.setDate(stIndex++, form08.getModAt());
                statement.setString(stIndex++, form08.getModby());
                statement.setLong(stIndex++, form08.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    return new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        return new ErrorEntity(ErrorType.ERROR, Long.valueOf(1061));
    }

    @Override
    public List<BasicDBObject> getPieChartData(Form08HavsraltEntity entity) throws SQLException {
        int stIndex = 1;
        List<BasicDBObject> objectList = null;

        String query_sel = "SELECT ovchin_ner, COUNT(id) as cnt, citycode, sumcode FROM hospital.hform08_hav  WHERE";

        if (entity != null && !entity.getCitycode().isEmpty()) {
            query_sel += " `hform08_hav`.`citycode` = ? AND ";
        }
        if (entity != null && !entity.getDelflg().isEmpty()) {
            query_sel += " `hform08_hav`.`delflg` = ? AND ";
        }
        if (entity != null && !entity.getOvchin_ner().isEmpty()) {
            query_sel += " `hform08_hav`.`ovchin_ner` = ? AND ";
        }
        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
            query_sel += " (`record_date` BETWEEN ? AND ?) AND ";
        }

        query_sel += " `id` > 0  GROUP BY ovchin_ner";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                int stindex = 1;

                if (entity != null && !entity.getCitycode().isEmpty()) {
                    statement.setString(stindex++, entity.getCitycode());
                }
                if (entity != null && !entity.getDelflg().isEmpty()) {
                    statement.setString(stindex++, entity.getDelflg());
                }
                if (entity != null && !entity.getOvchin_ner().isEmpty()) {
                    statement.setString(stindex++, entity.getOvchin_ner());
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                    statement.setDate(stindex++, entity.getRecordDate());
                    statement.setDate(stindex++, entity.getSearchRecordDate());
                }

                ResultSet resultSet = statement.executeQuery();

                objectList = new ArrayList<>();
                IComboService comboService = new ComboServiceImpl(boneCP);
                List<BasicDBObject> cmbType = comboService.getComboVals("UVCH", "MN");

                while (resultSet.next()) {
                    BasicDBObject object = new BasicDBObject();

                    object.put("malturulner", getMalTurulNer(cmbType, resultSet.getString("ovchin_ner")));
                    object.put("malturul", resultSet.getString("ovchin_ner"));
                    object.put("count", resultSet.getString("cnt"));
                    object.put("aimag", resultSet.getString("citycode"));
                    object.put("sum", resultSet.getString("sumcode"));

                    objectList.add(object);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return objectList;
    }//

    /**
     * Get name
     *
     * @param cmbType
     * @param mal_turul_mn
     * @return
     */
    private String getMalTurulNer(List<BasicDBObject> cmbType, String mal_turul_mn) {
        for (BasicDBObject obj :
                cmbType) {
            if (obj.get("id") != null && mal_turul_mn != null && obj.getString("id").equalsIgnoreCase(mal_turul_mn)) {
                return obj.getString("name");
            }
        }

        return "";
    }

    @Override
    public ErrorEntity exportReport(Form08HavsraltEntity form08, String file) throws SQLException {
        ErrorEntity errorEntity = null;

        String fileName = "hform08havsralt";

        try (Connection connection = boneCP.getConnection()) {
            String JRXML_PATH = Config.getJrxmlPath();
            String REPORT_PATH = Config.getReportPath();

            try {
                Map parameters = new HashMap();

                String reportTitle = "МАЛ, АМЬТДЫН ХАЛДВАРТ ӨВЧНИЙ $$YEAR$$ ОНЫ $$MONTH$$ -Р САР, ЖИЛИЙН ЭЦСИЙН МЭДЭЭ",
                        printDate = "$$YEAR$$ оны $$MONTH$$ сарын $$DAY$$";

                Calendar now = Calendar.getInstance();

                int year = now.get(Calendar.YEAR);
                int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
                int day = now.get(Calendar.DAY_OF_MONTH);

                printDate = printDate.replace("$$YEAR$$", String.valueOf(year));
                printDate = printDate.replace("$$MONTH$$", String.valueOf(month));
                printDate = printDate.replace("$$DAY$$", String.valueOf(day));


                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(new Date(form08.getRecordDate().getTime()));

                reportTitle = reportTitle.replace("$$YEAR$$", String.valueOf(calendar1.get(Calendar.YEAR)));

                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(new Date(form08.getSearchRecordDate().getTime()));
                String Month = String.valueOf(calendar2.get(Calendar.MONTH));
                if (Month.equals("0")) {
                    reportTitle = reportTitle.replace("$$MONTH$$", String.valueOf(calendar2.get(Calendar.MONTH) + 1));
                } else {
                    reportTitle = reportTitle.replace("$$MONTH$$", String.valueOf(calendar2.get(Calendar.MONTH)));
                }
//                reportTitle = reportTitle.replace("$$MONTH$$", String.valueOf(calendar2.get(Calendar.MONTH)));

                parameters.put("start_date", form08.getRecordDate());
                parameters.put("end_date", form08.getSearchRecordDate());
                //parameters.put("citycode", (form08.getCitycode() != null && !form08.getCitycode().isEmpty()) ? form08.getCitycode() : null);
                if(form08.getCitycode() != null && !form08.getCitycode().toString().isEmpty()){
                    parameters.put("citycode",form08.getCitycode());
                }
                parameters.put("sumcode", (form08.getSumcode() != null && !form08.getSumcode().isEmpty()) ? form08.getSumcode() : null);

                ICityService cityService = new CityServiceImpl(boneCP);
                City tmpCity = new City();
                tmpCity.setCode(form08.getCitycode());
                List<City> clist = cityService.selectAll(tmpCity);

                if (clist != null && clist.size() == 1) {
                    parameters.put("aimagner", clist.get(0).getCityname());
                } else {
                    parameters.put("aimagner", (form08.getCityName() == null || form08.getCityName().isEmpty()) ? "Нийт" : form08.getCityName());
                }
                ISumService sumService = new SumServiceImpl(boneCP);
                Sum tmpSum = new Sum();
                tmpSum.setCitycode(form08.getCitycode());
                tmpSum.setSumcode(form08.getSumcode());
                List<Sum> sumList = sumService.selectAll(tmpSum);

                if (sumList != null && sumList.size() == 1){
                    parameters.put("sumner", sumList.get(0).getSumname());
                }
                else{
                    parameters.put("sumner", (form08.getSumName() == null) ? "" : form08.getSumName());
                }

                parameters.put("bagcode", (form08.getBagcode() != null && !form08.getBagcode().isEmpty())? form08.getBagcode() : null);

                parameters.put("printdate", printDate);
                parameters.put("reporttitle", reportTitle);
                parameters.put("creby", (form08.getCreby() != null && !form08.getCreby().isEmpty()) ? form08.getCreby() : null);

                parameters.put("malturul", (form08.getMal_turul() != null && !form08.getMal_turul().isEmpty()) ? form08.getMal_turul() : null);
                parameters.put("orgorog", (form08.getOrgorog() != null && !form08.getOrgorog().isEmpty()) ? form08.getOrgorog() : null);
                parameters.put("golomttoo", (form08.getGolomttoo() != 0) ? form08.getGolomttoo() : null);
                parameters.put("uvchner", (form08.getOvchin_ner() != null && !form08.getOvchin_ner().isEmpty()) ? form08.getOvchin_ner() : null);
                parameters.put("uvchilson", (form08.getUvchilson() != 0) ? form08.getUvchilson() : null);
                parameters.put("uhsen", (form08.getUhsen() != 0) ? form08.getUhsen() : null);
                parameters.put("edgersen", (form08.getEdgersen() != 0) ? form08.getEdgersen() : null);
                parameters.put("ustgasan", (form08.getUstgasan() != 0) ? form08.getUstgasan() : null);
                parameters.put("nydluulsan", (form08.getNydluulsan() != 0) ? form08.getNydluulsan() : null);
                parameters.put("lab_ner", (form08.getLab_ner() != null && !form08.getLab_ner().isEmpty()) ? form08.getLab_ner() : null);
                parameters.put("shinjilgee_arga", (form08.getShinjilgee_arga() != null && !form08.getShinjilgee_arga().isEmpty()) ? form08.getShinjilgee_arga() : null);

                PdfExcelUtil.generateReport(file, JRXML_PATH, REPORT_PATH, fileName, parameters, connection);


                System.out.println("File Generated");
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (connection != null) connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1001));
            if(file.equals("pdf")){
                errorEntity.setErrText(fileName + ".pdf");
            }else if(file.equals("xls")){
                errorEntity.setErrText(fileName + ".xls");
            }

        }
        return errorEntity;
    }

    @Override
    public BasicDBObject getColumnChartData(Form08HavsraltEntity entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        //dateFormat.parse(dv).getTime()
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }

        try {
            IComboService comboService = new ComboServiceImpl(boneCP);
            List<Integer[]> rowdataList = new ArrayList<>();
            List<BasicDBObject> cmbType = comboService.getComboVals("UVCH", "MN");

            basicDBObject.put("coldata", cmbType);

            //Saraar n haruulah
            int month = calendar1.get(Calendar.MONTH) + 1;
            int year1 = calendar1.get(Calendar.YEAR);
            int year2 = calendar2.get(Calendar.YEAR);

            try (Connection connection = boneCP.getConnection()) {
                String query_sel = "SELECT `ovchin_ner`, SUM(`uvchilson`) AS uvchilson, `hform08_hav`.`citycode`  " +
                        "FROM `hospital`.`hform08_hav`  " +
                        "WHERE  ";

                if (entity != null && !entity.getCitycode().isEmpty()) {
                    query_sel += " `hform08_hav`.`citycode` = ? AND ";
                }
                if (entity != null && !entity.getDelflg().isEmpty()) {
                    query_sel += " `hform08_hav`.`delflg` = ? AND ";
                }
                if (entity != null && !entity.getOvchin_ner().isEmpty()) {
                    query_sel += " `hform08_hav`.`ovchin_ner` = ? AND ";
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                    query_sel += " (`record_date` BETWEEN ? AND ?) AND ";
                }

                query_sel += " `hform08_hav`.`id` > 0  GROUP BY `hform08_hav`.`ovchin_ner` ORDER BY `hform08_hav`.`ovchin_ner`";

                do {
                    //do process
                    Integer[] rowdata = new Integer[cmbType.size() + 1];
                    //
                    //rowdata[0] = month + " -р сар";
                    rowdata[0] = month;

                    try {
                        PreparedStatement statement = connection.prepareStatement(query_sel);

                        java.sql.Date stdate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-01 00:00:00").getTime());

                        java.sql.Date enddate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-31 00:00:00").getTime());

                        if (month == 12 && year1 < year2) {
                            month = 1;
                            year1++;
                        } else {
                            month++;
                        }

                        int stindex = 1;

                        if (entity != null && !entity.getCitycode().isEmpty()) {
                            statement.setString(stindex++, entity.getCitycode());
                        }
                        if (entity != null && !entity.getDelflg().isEmpty()) {
                            statement.setString(stindex++, entity.getDelflg());
                        }
                        if (entity != null && !entity.getOvchin_ner().isEmpty()) {
                            statement.setString(stindex++, entity.getOvchin_ner());
                        }
                        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                            statement.setDate(stindex++, stdate);
                            statement.setDate(stindex++, enddate);
                        }


                        ResultSet resultSet = statement.executeQuery();

                        HashMap<String, String> hashMap = new HashMap<>();

                        while (resultSet.next()) {
                            String malturul = resultSet.getString("ovchin_ner");
                            String count = resultSet.getString("uvchilson");

                            hashMap.put(malturul, count);
                        }//end while

                        for (int i = 1; i <= cmbType.size(); i++) {
                            rowdata[i] = (hashMap.get(cmbType.get(i - 1).getString("id")) == null) ? 0 :
                                    Integer.parseInt(hashMap.get(cmbType.get(i - 1).getString("id")));
                        }

                        rowdataList.add(rowdata);

                        if (statement != null) statement.close();
                        if (resultSet != null) resultSet.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();

                        break;
                    }

                    ///
                } while (month <= calendar2.get(Calendar.MONTH) + 1);

                basicDBObject.put("rowdata", rowdataList);

                connection.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }

    @Override
    public BasicDBObject getColHighChartData(Form08HavsraltEntity entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();
        List<String> categories = new ArrayList<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        //dateFormat.parse(dv).getTime()
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }

        try {
            IComboService comboService = new ComboServiceImpl(boneCP);

            List<BasicDBObject> cmbType = comboService.getComboNames("UVCH", "MN");

            if (entity != null && !entity.getOvchin_ner().isEmpty()) {
                cmbType = comboService.getComboNames("MAL", "MN");
            }


            //Saraar n haruulah
            int month = calendar1.get(Calendar.MONTH) + 1;
            int year1 = calendar1.get(Calendar.YEAR);
            int year2 = calendar2.get(Calendar.YEAR);

            try (Connection connection = boneCP.getConnection()) {
                String query_sel = "SELECT `ovchin_ner`, SUM(`uvchilson`) AS uvchilson, `hform08_hav`.`citycode`  " +
                        "FROM `hospital`.`hform08_hav`  " +
                        "WHERE  ";

                if (entity != null && !entity.getOvchin_ner().isEmpty()) {
                    query_sel = "SELECT `mal_turul`, SUM(`uvchilson`) AS uvchilson, `hform08_hav`.`citycode`  " +
                            "FROM `hospital`.`hform08_hav`  " +
                            "WHERE  ";
                }

                if (entity != null && !entity.getCitycode().isEmpty()) {
                    query_sel += " `hform08_hav`.`citycode` = ? AND ";
                }
                if (entity != null && !entity.getDelflg().isEmpty()) {
                    query_sel += " `hform08_hav`.`delflg` = ? AND ";
                }
                if (entity != null && !entity.getOvchin_ner().isEmpty()) {
                    query_sel += " `hform08_hav`.`ovchin_ner` = ? AND ";
                }
                if (entity != null && !entity.getMal_turul().isEmpty()) {
                    query_sel += " `hform08_hav`.`mal_turul` = ? AND ";
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                    query_sel += " (`record_date` BETWEEN ? AND ?) AND ";
                }
                if (entity != null && !entity.getCitycode().isEmpty()) {
                    query_sel += " `hform08_hav`.`citycode` = ? AND ";
                }
                if (entity != null && !entity.getSumcode().isEmpty()) {
                    query_sel += " `hform08_hav`.`sumcode` = ? AND ";
                }
                if (entity != null && !entity.getBagcode().isEmpty()) {
                    query_sel += " `hform08_hav`.`bagcode` = ? AND ";
                }

                if (entity != null && !entity.getOvchin_ner().isEmpty()) {
                    query_sel += " `hform08_hav`.`id` > 0  GROUP BY `hform08_hav`.`mal_turul` ORDER BY `hform08_hav`.`mal_turul`";
                }
                else{
                    query_sel += " `hform08_hav`.`id` > 0  GROUP BY `hform08_hav`.`ovchin_ner` ORDER BY `hform08_hav`.`ovchin_ner`";
                }

                do {
                    //do process
                    categories.add(year1 + "-" + month);

                    try {
                        PreparedStatement statement = connection.prepareStatement(query_sel);

                        java.sql.Date stdate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-01 00:00:00").getTime());

                        java.sql.Date enddate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-31 00:00:00").getTime());

                        if (month == 12 && year1 < year2) {
                            month = 1;
                            year1++;
                        } else {
                            month++;
                        }

                        int stindex = 1;

                        if (entity != null && !entity.getCitycode().isEmpty()) {
                            statement.setString(stindex++, entity.getCitycode());
                        }
                        if (entity != null && !entity.getDelflg().isEmpty()) {
                            statement.setString(stindex++, entity.getDelflg());
                        }
                        if (entity != null && !entity.getOvchin_ner().isEmpty()) {
                            statement.setString(stindex++, entity.getOvchin_ner());
                        }
                        if (entity != null && !entity.getMal_turul().isEmpty()) {
                            statement.setString(stindex++, entity.getMal_turul());
                        }
                        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                            statement.setDate(stindex++, stdate);
                            statement.setDate(stindex++, enddate);
                        }
                        if (entity != null && !entity.getCitycode().isEmpty()) {
                            statement.setString(stindex++, entity.getCitycode());
                        }
                        if (entity != null && !entity.getSumcode().isEmpty()) {
                            statement.setString(stindex++, entity.getSumcode());
                        }
                        if (entity != null && !entity.getBagcode().isEmpty()) {
                            statement.setString(stindex++, entity.getBagcode());
                        }
                        ResultSet resultSet = statement.executeQuery();

                        HashMap<String, String> hashMap = new HashMap<>();

                        while (resultSet.next()) {
                            String uvchinner = "";
                            if (entity != null && !entity.getOvchin_ner().isEmpty()) {
                                uvchinner = resultSet.getString("mal_turul");
                            }else{
                                uvchinner = resultSet.getString("ovchin_ner");
                            }

                            String count = resultSet.getString("uvchilson");
                            hashMap.put(uvchinner, count);
                        }//end while

                        List<Double> list = null;

                        for (int i = 0; i < cmbType.size(); i++) {
                            BasicDBObject obj = cmbType.get(i);

                            if (obj.get("data") == null){
                                //new record
                                list = new ArrayList<>();
                                String val = hashMap.get(obj.getString("id"));
                                if (val == null) list.add(0.0);
                                else if (val.isEmpty()) list.add(0.0);
                                else list.add(Double.parseDouble(val));
                                obj.put("data", list);
                            }else{
                                //already exist
                                list = (List<Double>) obj.get("data");
                                String val = hashMap.get(obj.getString("id"));
                                if (val == null) list.add(0.0);
                                else if (val.isEmpty()) list.add(0.0);
                                else list.add(Double.parseDouble(val));
                            }

                        }

                        if (statement != null) statement.close();
                        if (resultSet != null) resultSet.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();

                        break;
                    }

                    ///
                } while ((month <= calendar2.get(Calendar.MONTH) + 1 && year1 <= year2) || year1 < year2);

                basicDBObject.put("categories", categories);
                basicDBObject.put("series", cmbType);

                connection.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return basicDBObject;
    }

    @Override
    public BasicDBObject getPieHighChartData(Form08HavsraltEntity entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();

        String query_sel = "";

        if (entity != null && !entity.getOvchin_ner().isEmpty()) {
            query_sel = "SELECT mal_turul, COUNT(id) as cnt, citycode, sumcode FROM hospital.hform08_hav  WHERE";
        }else{
            query_sel = "SELECT ovchin_ner, COUNT(id) as cnt, citycode, sumcode FROM hospital.hform08_hav  WHERE";
        }

        if (entity != null && !entity.getCitycode().isEmpty()) {
            query_sel += " `hform08_hav`.`citycode` = ? AND ";
        }
        if (entity != null && !entity.getDelflg().isEmpty()) {
            query_sel += " `hform08_hav`.`delflg` = ? AND ";
        }
        if (entity != null && !entity.getOvchin_ner().isEmpty()) {
            query_sel += " `hform08_hav`.`ovchin_ner` = ? AND ";
        }
        if (entity != null && !entity.getMal_turul().isEmpty()) {
            query_sel += " `hform08_hav`.`mal_turul` = ? AND ";
        }
        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
            query_sel += " (`record_date` BETWEEN ? AND ?) AND ";
        }
        if (entity != null && !entity.getCitycode().isEmpty()) {
            query_sel += " `hform08_hav`.`citycode` = ? AND ";
        }
        if (entity != null && !entity.getSumcode().isEmpty()) {
            query_sel += " `hform08_hav`.`sumcode` = ? AND ";
        }
        if (entity != null && !entity.getBagcode().isEmpty()) {
            query_sel += " `hform08_hav`.`bagcode` = ? AND ";
        }

        if (entity != null && !entity.getOvchin_ner().isEmpty()) {
            query_sel += " `id` > 0  GROUP BY mal_turul";
        }else{
            query_sel += " `id` > 0  GROUP BY ovchin_ner";
        }

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                int stindex = 1;

                if (entity != null && !entity.getCitycode().isEmpty()) {
                    statement.setString(stindex++, entity.getCitycode());
                }
                if (entity != null && !entity.getDelflg().isEmpty()) {
                    statement.setString(stindex++, entity.getDelflg());
                }
                if (entity != null && !entity.getOvchin_ner().isEmpty()) {
                    statement.setString(stindex++, entity.getOvchin_ner());
                }
                if (entity != null && !entity.getMal_turul().isEmpty()) {
                    statement.setString(stindex++, entity.getMal_turul());
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                    statement.setDate(stindex++, entity.getRecordDate());
                    statement.setDate(stindex++, entity.getSearchRecordDate());
                }
                if (entity != null && !entity.getCitycode().isEmpty()) {
                    statement.setString(stindex++, entity.getCitycode());
                }
                if (entity != null && !entity.getSumcode().isEmpty()) {
                    statement.setString(stindex++, entity.getSumcode());
                }
                if (entity != null && !entity.getBagcode().isEmpty()) {
                    statement.setString(stindex++, entity.getBagcode());
                }

                ResultSet resultSet = statement.executeQuery();

                IComboService comboService = new ComboServiceImpl(boneCP);
                List<BasicDBObject> list = new ArrayList<>();

                while (resultSet.next()) {
                    BasicDBObject entity2 = new BasicDBObject();

                    if (entity != null && !entity.getOvchin_ner().isEmpty()) {
                        List<BasicDBObject> cmbType = comboService.getComboNames("MAL", "MN");
                        entity2.put("name", getMalTurulNer(cmbType, resultSet.getString("mal_turul")));
                    }else{
                        List<BasicDBObject> cmbType = comboService.getComboNames("UVCH", "MN");
                        entity2.put("name", getMalTurulNer(cmbType, resultSet.getString("ovchin_ner")));
                    }

                    entity2.put("y", resultSet.getDouble("cnt"));

                    list.add(entity2);
                }

                basicDBObject.put("data", list);

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }
}
