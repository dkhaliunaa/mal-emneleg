package com.hospital.app;

import com.model.hos.ErrorEntity;
import com.model.hos.Sum;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by 24be10264 on 9/13/2017.
 */
public interface ISumService {


    /**
     * Fetch Sum
     *
     * @param sum
     * @return
     * @throws SQLException
     */
    List<Sum> selectAll(Sum sum) throws SQLException;

    /**
     * Insert new data
     *
     * @param sum
     * @return
     * @throws SQLException
     */
    ErrorEntity insertData(Sum sum) throws SQLException;

    /**
     * Update old data
     *
     * @param sum
     * @return
     * @throws SQLException
     */
    ErrorEntity updateData(Sum sum) throws SQLException;

}
