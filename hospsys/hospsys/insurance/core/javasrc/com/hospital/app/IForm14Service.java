package com.hospital.app;

import com.model.hos.ErrorEntity;
import com.model.hos.Form14Entity;
import com.model.hos.Form46Entity;
import com.mongodb.BasicDBObject;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public interface IForm14Service {

    /**
     * Insert new data
     *
     *
     * @param form14
     * @return
     * @throws Exception
     */
    ErrorEntity insertNewData(Form14Entity form14) throws SQLException;

    /**
     * Select all
     *
     * @param form14
     * @return
     * @throws SQLException
     */
    List<Form14Entity> getListData(Form14Entity form14) throws SQLException;


    /**
     * update form data
     *
     * @param form14
     * @return
     * @throws SQLException
     */
    ErrorEntity updateData(Form14Entity form14) throws SQLException;


    List<Form14Entity> getPieChartData(Form14Entity form14Entity) throws SQLException;

    ErrorEntity exportReport(String file, Form14Entity form14) throws SQLException;
    BasicDBObject getColHighChartData(Form14Entity Form14Entity) throws SQLException;

    BasicDBObject getPieHighChartData(Form14Entity Form14Entity) throws SQLException;
}
