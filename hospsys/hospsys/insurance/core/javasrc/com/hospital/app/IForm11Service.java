package com.hospital.app;

import com.model.hos.ErrorEntity;
import com.model.hos.Form11Entity;
import com.mongodb.BasicDBObject;

import java.sql.SQLException;
import java.util.List;

public interface IForm11Service {

    /**
     * Insert new data
     *
     *
     * @param form11
     * @return
     * @throws Exception
     */
    ErrorEntity insertNewData(Form11Entity form11) throws SQLException;

    /**
     * Select all
     *
     * @param form11
     * @return
     * @throws SQLException
     */
    List<Form11Entity> getListData(Form11Entity form11) throws SQLException;


    /**
     * update form data
     *
     * @param form11
     * @return
     * @throws SQLException
     */
    ErrorEntity updateData(Form11Entity form11) throws SQLException;


    List<BasicDBObject> getPieChartData(Form11Entity form11Entity) throws SQLException;


    BasicDBObject getColumnChartData(Form11Entity entity) throws SQLException;

    ErrorEntity exportReport(String file, Form11Entity form11) throws SQLException;

    BasicDBObject getColHighChartData(Form11Entity form11Entity) throws SQLException;

    BasicDBObject getPieHighChartData(Form11Entity form11Entity) throws SQLException;

}
