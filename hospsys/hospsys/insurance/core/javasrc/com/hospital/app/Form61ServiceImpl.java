package com.hospital.app;


import com.Config;
import com.enumclass.ErrorType;
import com.jolbox.bonecp.BoneCP;
import com.model.hos.*;
import com.mongodb.BasicDBObject;
import com.rpc.PdfExcelUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public class Form61ServiceImpl implements IForm61Service{
    /**
     * Objects
     */

    private BoneCP boneCP;
    private ErrorEntity errObj;

    public Form61ServiceImpl(BoneCP boneCP) {
        this.boneCP = boneCP;
    }

    @Override
    public ErrorEntity insertNewData(Form61Entity form61) throws SQLException {
        PreparedStatement statement = null;

        if (form61 == null) {
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        String query_login = "INSERT INTO `hospital`.`hform61` " +
                "(`zooriToo`, " +
                "`zooriTalbai`, " +
                "`malHaashaaToo`, " +
                "`malHaashaaTalbai`, " +
                "`hudagToo`, " +
                "`hudagHemjee`, " +
                "`tejAguuToo`, " +
                "`tejAguuTalbai`, " +
                "`tuuhiiEdHadToo`, " +
                "`tuuhiiEdHadHemjee`, " +
                "`malTonogHergsel`, " +
                "`belcheerTalbai`, " +
                "`niitTalbai`, " +
                "`teeverToo`, " +
                "`zorchigchToo`, " +
                "`delflg`, " +
                "`actflg`, " +
                "`mod_at`, " +
                "`mod_by`, " +
                "`cre_at`, " +
                "`recorddate`, " +
                "`cre_by`," +
                "`aimag`," +
                "`sum`," +
                "`aimag_en`," +
                "`sum_en`)" +
                "VALUES " +
                "(? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?,?, ?, ?, ?, ?,?, ?,?)";

        try (Connection connection = boneCP.getConnection()) {
            try{
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setLong(1, form61.getZooriToo());
                statement.setBigDecimal(2, form61.getZooriTalbai());
                statement.setLong(3, form61.getMalHaashaaToo());
                statement.setBigDecimal(4, form61.getMalHaashaaTalbai());
                statement.setLong(5, form61.getHudagToo());
                statement.setBigDecimal(6, form61.getHudagHemjee());
                statement.setLong(7, form61.getTejAguuToo());
                statement.setBigDecimal(8, form61.getTejAguuTalbai());
                statement.setLong(9, form61.getTuuhiiEdHadToo());
                statement.setBigDecimal(10, form61.getTuuhiiEdHadHemjee());
                statement.setLong(11, form61.getMalTonogHergsel());
                statement.setBigDecimal(12, form61.getBelcheerTalbai());
                statement.setBigDecimal(13, form61.getNiitTalbai());
                statement.setLong(14, form61.getTeevertoo());
                statement.setLong(15, form61.getZorchigchtoo());
                statement.setString(16, form61.getDelflg());
                statement.setString(17, form61.getActflg());
                statement.setDate(18, form61.getModAt());
                statement.setString(19, form61.getModby());
                statement.setDate(20, form61.getCreat());
                statement.setDate(21, form61.getRecordDate());
                statement.setString(22, form61.getCreby());
                statement.setString(23, form61.getAimag());
                statement.setString(24, form61.getSum());
                statement.setString(25, form61.getAimag_en());
                statement.setString(26, form61.getSum_en());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1){
                    connection.commit();
                    errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));
                    errObj.setErrText("Бичлэг амжилттай хадгалагдлаа.");
                    errObj.setErrDesc("Бичлэг амжилттай хадгалагдлаа.");
                    return errObj;
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }finally {
                if (statement != null)statement.close();
                connection.close();
            }

        }catch (Exception ex){

        }

        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1057));
        errObj.setErrText("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");
        errObj.setErrDesc("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");

        return errObj;
    }

    @Override
    public List<Form61Entity> getListData(Form61Entity form61) throws SQLException {
        int stIndex = 1;
        List<Form61Entity> form61EntitieList = null;

        String query = "SELECT `hform61`.`id`, " +
                "    `hform61`.`zooriToo`, " +
                "    `hform61`.`zooriTalbai`, " +
                "    `hform61`.`malHaashaaToo`, " +
                "    `hform61`.`malHaashaaTalbai`, " +
                "    `hform61`.`hudagToo`, " +
                "    `hform61`.`hudagHemjee`, " +
                "    `hform61`.`tejAguuToo`, " +
                "    `hform61`.`tejAguuTalbai`, " +
                "    `hform61`.`tuuhiiEdHadToo`, " +
                "    `hform61`.`tuuhiiEdHadHemjee`, " +
                "    `hform61`.`malTonogHergsel`, " +
                "    `hform61`.`belcheerTalbai`, " +
                "    `hform61`.`niitTalbai`, " +
                "    `hform61`.`teeverToo`, " +
                "    `hform61`.`zorchigchToo`, " +
                "    `hform61`.`delflg`, " +
                "    `hform61`.`actflg`, " +
                "    `hform61`.`mod_at`, " +
                "    `hform61`.`mod_by`, " +
                "    `hform61`.`cre_at`, " +
                "    `hform61`.`recorddate`, " +
                "    `hform61`.`cre_by`, " +
                "    `hform61`.`aimag`, " +
                "    `hform61`.`sum`, " +
                "    `hform61`.`aimag_en`, " +
                "    `hform61`.`sum_en`, " +
                "    c.cityname, c.cityname_en, " +
                "    `h_sum`.sumname, `h_sum`.sumname_en, " +
                "    `h_sum`.latitude as sum_latitude, `h_sum`.longitude as sum_longitude  " +
                "FROM `hospital`.`hform61` INNER JOIN `h_city` AS c " +
                "ON c.code = `hform61`.aimag " +
                "INNER JOIN `h_sum` " +
                "ON `h_sum`.sumcode = `hform61`.sum " +
                "WHERE ";

        if (form61.getZooriToo() != null && !form61.getZooriToo().toString().isEmpty() && form61.getZooriToo() != 0){
            query += " zooriToo = ? AND ";
        }

        if (form61.getZooriTalbai() != null && !form61.getZooriTalbai().toString().isEmpty() && form61.getZooriTalbai().longValue() != 0){
            query += " zooriTalbai = ? AND ";
        }

        /* NER */
        if (form61.getMalHaashaaTalbai() != null && !form61.getMalHaashaaTalbai().toString().isEmpty() && form61.getMalHaashaaTalbai().longValue() != 0){
            query += " malHaashaaTalbai = ? AND ";
        }
        if (form61.getMalHaashaaToo() != null && !form61.getMalHaashaaToo().toString().isEmpty() && form61.getMalHaashaaToo() != 0){
            query += " malHaashaaToo = ? AND ";
        }
        /* Zartsuulsan hemjee */
        if (form61.getHudagToo() != null && !form61.getHudagToo().toString().isEmpty() && form61.getHudagToo() != 0){
            query += " hudagToo = ? AND ";
        }
        if (form61.getHudagHemjee() != null && !form61.getHudagHemjee().toString().isEmpty() && form61.getHudagHemjee().longValue() != 0){
            query += " hudagHemjee = ? AND ";
        }

        /* MAL TURUL */
        if (form61.getTejAguuToo() != null && !form61.getTejAguuToo().toString().isEmpty() && form61.getTejAguuToo() != 0){
            query += " tejAguuToo = ? AND ";
        }
        if (form61.getTejAguuTalbai() != null && !form61.getTejAguuTalbai().toString().isEmpty() && form61.getTejAguuTalbai().longValue() != 0){
            query += " tejAguuTalbai = ? AND ";
        }

        /* Tuluvlugu */
        if (form61.getTuuhiiEdHadToo() != null && !form61.getTuuhiiEdHadToo().toString().isEmpty() && form61.getTuuhiiEdHadToo() != 0){
            query += " tuuhiiEdHadToo = ? AND ";
        }
        if (form61.getTuuhiiEdHadHemjee() != null && !form61.getTuuhiiEdHadHemjee().toString().isEmpty() && form61.getTuuhiiEdHadHemjee().longValue() != 0){
            query += " tuuhiiEdHadHemjee = ? AND ";
        }

        /* Guitsetgel */
        if (form61.getMalTonogHergsel() != null && !form61.getMalTonogHergsel().toString().isEmpty() && form61.getMalTonogHergsel() != 0){
            query += " malTonogHergsel = ? AND ";
        }
        if (form61.getBelcheerTalbai() != null && !form61.getBelcheerTalbai().toString().isEmpty() && form61.getBelcheerTalbai().longValue() != 0){
            query += " belcheerTalbai = ? AND ";
        }

        /* Huwi */
        if (form61.getNiitTalbai() != null && !form61.getNiitTalbai().toString().isEmpty() && form61.getNiitTalbai().longValue() != 0){
            query += " niitTalbai = ? AND  ";
        }

        if (form61.getTeevertoo() != null && !form61.getTeevertoo().toString().isEmpty() && form61.getTeevertoo().longValue() != 0){
            query += " teeverToo = ? AND  ";
        }

        if (form61.getZorchigchtoo() != null && !form61.getZorchigchtoo().toString().isEmpty() && form61.getZorchigchtoo().longValue() != 0){
            query += " zorchigchToo = ? AND  ";
        }

        if (form61 != null && form61.getAimag() != null && !form61.getAimag().isEmpty()){
            query += " `aimag` = ? AND ";
        }
        if (form61 != null && form61.getSum() != null && !form61.getSum().isEmpty()){
            query += " `sum` = ? AND ";
        }

        if (form61 != null && form61.getRecordDate() != null && form61.getSearchRecordDate() != null){
            query += " (`recorddate` BETWEEN ? AND ?) AND ";
        }

        if (form61 != null && form61.getCreby() != null && !form61.getCreby().isEmpty()){
            query += " `hform61`.`cre_by` = ? AND ";
        }

        query += " `hform61`.`delflg` = 'N'";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query);

                if (form61.getZooriToo() != null && !form61.getZooriToo().toString().toString().isEmpty() && form61.getZooriToo() != 0) {
                    statement.setLong(stIndex++, form61.getZooriToo());
                }

                if (form61.getZooriTalbai() != null && !form61.getZooriTalbai().toString().toString().isEmpty() && form61.getZooriTalbai().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form61.getZooriTalbai());
                }

            /* NER */
                if (form61.getMalHaashaaToo() != null && !form61.getMalHaashaaToo().toString().isEmpty() && form61.getMalHaashaaToo() != 0) {
                    statement.setLong(stIndex++, form61.getMalHaashaaToo());
                }
                if (form61.getMalHaashaaTalbai() != null && !form61.getMalHaashaaTalbai().toString().isEmpty() && form61.getMalHaashaaTalbai().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form61.getMalHaashaaTalbai());
                }

            /* Zartsuulsan hemjee */
                if (form61.getHudagToo() != null && !form61.getHudagToo().toString().isEmpty() && form61.getHudagToo() != 0) {
                    statement.setLong(stIndex++, form61.getHudagToo());
                }
                if (form61.getHudagHemjee() != null && !form61.getHudagHemjee().toString().isEmpty() && form61.getHudagHemjee().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form61.getHudagHemjee());
                }

            /* MAL TURUL */
                if (form61.getTejAguuToo() != null && !form61.getTejAguuToo().toString().toString().isEmpty() && form61.getTejAguuToo() != 0) {
                    statement.setLong(stIndex++, form61.getTejAguuToo());
                }
                if (form61.getTejAguuTalbai() != null && !form61.getTejAguuTalbai().toString().isEmpty() && form61.getTejAguuTalbai().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form61.getTejAguuTalbai());
                }

            /* Tuluvlugu */
                if (form61.getTuuhiiEdHadToo() != null && !form61.getTuuhiiEdHadToo().toString().toString().isEmpty() && form61.getTuuhiiEdHadToo() != 0) {
                    statement.setLong(stIndex++, form61.getTuuhiiEdHadToo());
                }
                if (form61.getTuuhiiEdHadHemjee() != null && !form61.getTuuhiiEdHadHemjee().toString().isEmpty() && form61.getTuuhiiEdHadHemjee().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form61.getTuuhiiEdHadHemjee());
                }

            /* Guitsetgel */
                if (form61.getMalTonogHergsel() != null && !form61.getMalTonogHergsel().toString().toString().isEmpty() && form61.getMalTonogHergsel() != 0) {
                    statement.setLong(stIndex++, form61.getMalTonogHergsel());
                }
                if (form61.getBelcheerTalbai() != null && !form61.getBelcheerTalbai().toString().isEmpty() && form61.getBelcheerTalbai().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form61.getBelcheerTalbai());
                }

            /* Huwi */
                if (form61.getNiitTalbai() != null && !form61.getNiitTalbai().toString().isEmpty() && form61.getNiitTalbai().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form61.getNiitTalbai());
                }

                if (form61.getTeevertoo() != null && !form61.getTeevertoo().toString().isEmpty() && form61.getTeevertoo().longValue() != 0) {
                    statement.setLong(stIndex++, form61.getTeevertoo());
                }

                if (form61.getZorchigchtoo() != null && !form61.getZorchigchtoo().toString().isEmpty() && form61.getZorchigchtoo().longValue() != 0) {
                    statement.setLong(stIndex++, form61.getZorchigchtoo());
                }

                if (form61 != null && form61.getAimag() != null && !form61.getAimag().isEmpty()){
                    statement.setString(stIndex++, form61.getAimag());
                }
                if (form61 != null && form61.getSum() != null && !form61.getSum().isEmpty()){
                    statement.setString(stIndex++, form61.getSum());
                }

                if (form61 != null && form61.getRecordDate() != null && form61.getSearchRecordDate() != null){
                    statement.setDate(stIndex++, form61.getRecordDate());
                    statement.setDate(stIndex++, form61.getSearchRecordDate());
                }
                if (form61 != null && form61.getCreby() != null && !form61.getCreby().isEmpty()){
                    statement.setString(stIndex++, form61.getCreby());
                }

                ResultSet resultSet = statement.executeQuery();

                form61EntitieList = new ArrayList<>();
int index =1 ;
                while (resultSet.next()) {
                    Form61Entity entity = new Form61Entity();

                    entity.setId(resultSet.getLong("id"));
                    entity.setZooriToo(resultSet.getLong("zooriToo"));
                    entity.setZooriTalbai(resultSet.getBigDecimal("zooriTalbai"));
                    entity.setMalHaashaaToo(resultSet.getLong("malHaashaaToo"));
                    entity.setMalHaashaaTalbai(resultSet.getBigDecimal("malHaashaaTalbai"));
                    entity.setHudagToo(resultSet.getLong("hudagToo"));
                    entity.setHudagHemjee(resultSet.getBigDecimal("hudagHemjee"));
                    entity.setTejAguuToo(resultSet.getLong("tejAguuToo"));
                    entity.setTejAguuTalbai(resultSet.getBigDecimal("tejAguuTalbai"));
                    entity.setTuuhiiEdHadToo(resultSet.getLong("tuuhiiEdHadToo"));
                    entity.setTuuhiiEdHadHemjee(resultSet.getBigDecimal("tuuhiiEdHadHemjee"));
                    entity.setMalTonogHergsel(resultSet.getLong("malTonogHergsel"));
                    entity.setBelcheerTalbai(resultSet.getBigDecimal("belcheerTalbai"));
                    entity.setNiitTalbai(resultSet.getBigDecimal("niitTalbai"));
                    entity.setTeevertoo(resultSet.getLong("teeverToo"));
                    entity.setZorchigchtoo(resultSet.getLong("zorchigchToo"));
                    entity.setDelflg(resultSet.getString("delflg"));
                    entity.setActflg(resultSet.getString("actflg"));
                    entity.setModAt(resultSet.getDate("mod_at"));
                    entity.setModby(resultSet.getString("mod_by"));
                    entity.setCreat(resultSet.getDate("cre_at"));
                    entity.setRecordDate(resultSet.getDate("recorddate"));
                    entity.setCreby(resultSet.getString("cre_by"));
                    entity.setAimag(resultSet.getString("aimag"));
                    entity.setSum(resultSet.getString("sum"));
                    entity.setAimag_en(resultSet.getString("aimag_en"));
                    entity.setSum_en(resultSet.getString("sum_en"));
                    entity.put("sumname", resultSet.getString("sumname"));
                    entity.put("sum_latitude", resultSet.getString("sum_latitude"));
                    entity.put("sum_longitude", resultSet.getString("sum_longitude"));
                    entity.put("index",index++);
                    form61EntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return form61EntitieList;
    }

    @Override
    public ErrorEntity updateData(Form61Entity form61) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (form61 == null) {
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        query = "UPDATE `hospital`.`hform61` " +
                "SET     ";

        if (form61.getZooriToo() != null && !form61.getZooriToo().toString().isEmpty() && form61.getZooriToo() != 0){
            query += " zooriToo = ? , ";
        }

        if (form61.getZooriTalbai() != null && !form61.getZooriTalbai().toString().isEmpty() && form61.getZooriTalbai().longValue() != 0){
            query += " zooriTalbai = ? , ";
        }

        /* NER */
        if (form61.getMalHaashaaTalbai() != null && !form61.getMalHaashaaTalbai().toString().isEmpty() && form61.getMalHaashaaTalbai().longValue() != 0){
            query += " malHaashaaTalbai = ? , ";
        }
        if (form61.getMalHaashaaToo() != null && !form61.getMalHaashaaToo().toString().isEmpty() && form61.getMalHaashaaToo() != 0){
            query += " malHaashaaToo = ? , ";
        }
        /* Zartsuulsan hemjee */
        if (form61.getHudagToo() != null && !form61.getHudagToo().toString().isEmpty() && form61.getHudagToo() != 0){
            query += " hudagToo = ? , ";
        }
        if (form61.getHudagHemjee() != null && !form61.getHudagHemjee().toString().isEmpty() && form61.getHudagHemjee().longValue() != 0){
            query += " hudagHemjee = ? , ";
        }

        /* MAL TURUL */
        if (form61.getTejAguuToo() != null && !form61.getTejAguuToo().toString().isEmpty() && form61.getTejAguuToo() != 0){
            query += " tejAguuToo = ? , ";
        }
        if (form61.getTejAguuTalbai() != null && !form61.getTejAguuTalbai().toString().isEmpty() && form61.getTejAguuTalbai().longValue() != 0){
            query += " tejAguuTalbai = ? , ";
        }

        /* Tuluvlugu */
        if (form61.getTuuhiiEdHadToo() != null && !form61.getTuuhiiEdHadToo().toString().isEmpty() && form61.getTuuhiiEdHadToo() != 0){
            query += " tuuhiiEdHadToo = ? , ";
        }
        if (form61.getTuuhiiEdHadHemjee() != null && !form61.getTuuhiiEdHadHemjee().toString().isEmpty() && form61.getTuuhiiEdHadHemjee().longValue() != 0){
            query += " tuuhiiEdHadHemjee = ? , ";
        }

        /* Guitsetgel */
        if (form61.getMalTonogHergsel() != null && !form61.getMalTonogHergsel().toString().isEmpty() && form61.getMalTonogHergsel() != 0){
            query += " malTonogHergsel = ? , ";
        }
        if (form61.getBelcheerTalbai() != null && !form61.getBelcheerTalbai().toString().isEmpty() && form61.getBelcheerTalbai().longValue() != 0){
            query += " belcheerTalbai = ? , ";
        }

        /* Huwi */
        if (form61.getNiitTalbai() != null && !form61.getNiitTalbai().toString().isEmpty() && form61.getNiitTalbai().longValue() != 0){
            query += " niitTalbai = ? ,  ";
        }

        if (form61.getTeevertoo() != null && !form61.getTeevertoo().toString().isEmpty() && form61.getTeevertoo().longValue() != 0){
            query += " teeverToo = ? ,  ";
        }

        if (form61.getZorchigchtoo() != null && !form61.getZorchigchtoo().toString().isEmpty() && form61.getZorchigchtoo().longValue() != 0){
            query += " zorchigchToo = ? ,  ";
        }

        /* Delete flag */
        if (form61.getDelflg() != null && !form61.getDelflg().toString().isEmpty()){
            query += " delflg = ?, ";
        }

        /* Active flag */
        if (form61.getActflg() != null && !form61.getActflg().toString().isEmpty()){
            query += " actflg = ?, ";
        }

        if (form61 != null && form61.getRecordDate() != null){
            query += " `recorddate` = ?, ";
        }
        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";
        query += " WHERE  ID   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (form61.getZooriToo() != null && !form61.getZooriToo().toString().toString().isEmpty() && form61.getZooriToo() != 0) {
                    statement.setLong(stIndex++, form61.getZooriToo());
                }

                if (form61.getZooriTalbai() != null && !form61.getZooriTalbai().toString().toString().isEmpty() && form61.getZooriTalbai().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form61.getZooriTalbai());
                }

            /* NER */
                if (form61.getMalHaashaaToo() != null && !form61.getMalHaashaaToo().toString().isEmpty() && form61.getMalHaashaaToo() != 0) {
                    statement.setLong(stIndex++, form61.getMalHaashaaToo());
                }
                if (form61.getMalHaashaaTalbai() != null && !form61.getMalHaashaaTalbai().toString().isEmpty() && form61.getMalHaashaaTalbai().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form61.getMalHaashaaTalbai());
                }

            /* Zartsuulsan hemjee */
                if (form61.getHudagToo() != null && !form61.getHudagToo().toString().isEmpty() && form61.getHudagToo() != 0) {
                    statement.setLong(stIndex++, form61.getHudagToo());
                }
                if (form61.getHudagHemjee() != null && !form61.getHudagHemjee().toString().isEmpty() && form61.getHudagHemjee().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form61.getHudagHemjee());
                }

            /* MAL TURUL */
                if (form61.getTejAguuToo() != null && !form61.getTejAguuToo().toString().toString().isEmpty() && form61.getTejAguuToo() != 0) {
                    statement.setLong(stIndex++, form61.getTejAguuToo());
                }
                if (form61.getTejAguuTalbai() != null && !form61.getTejAguuTalbai().toString().isEmpty() && form61.getTejAguuTalbai().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form61.getTejAguuTalbai());
                }

            /* Tuluvlugu */
                if (form61.getTuuhiiEdHadToo() != null && !form61.getTuuhiiEdHadToo().toString().toString().isEmpty() && form61.getTuuhiiEdHadToo() != 0) {
                    statement.setLong(stIndex++, form61.getTuuhiiEdHadToo());
                }
                if (form61.getTuuhiiEdHadHemjee() != null && !form61.getTuuhiiEdHadHemjee().toString().isEmpty() && form61.getTuuhiiEdHadHemjee().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form61.getTuuhiiEdHadHemjee());
                }

            /* Guitsetgel */
                if (form61.getMalTonogHergsel() != null && !form61.getMalTonogHergsel().toString().toString().isEmpty() && form61.getMalTonogHergsel() != 0) {
                    statement.setLong(stIndex++, form61.getMalTonogHergsel());
                }
                if (form61.getBelcheerTalbai() != null && !form61.getBelcheerTalbai().toString().isEmpty() && form61.getBelcheerTalbai().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form61.getBelcheerTalbai());
                }

            /* Huwi */
                if (form61.getNiitTalbai() != null && !form61.getNiitTalbai().toString().isEmpty() && form61.getNiitTalbai().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form61.getNiitTalbai());
                }

                if (form61.getTeevertoo() != null && !form61.getTeevertoo().toString().isEmpty() && form61.getTeevertoo().longValue() != 0) {
                    statement.setLong(stIndex++, form61.getTeevertoo());
                }

                if (form61.getZorchigchtoo() != null && !form61.getZorchigchtoo().toString().isEmpty() && form61.getZorchigchtoo().longValue() != 0) {
                    statement.setLong(stIndex++, form61.getZorchigchtoo());
                }

            /* Delete flag */
                if (form61.getDelflg() != null && !form61.getDelflg().toString().isEmpty()) {
                    statement.setString(stIndex++, form61.getDelflg());
                }

            /* Active flag */
                if (form61.getActflg() != null && !form61.getActflg().toString().isEmpty()) {
                    statement.setString(stIndex++, form61.getActflg());
                }

                if (form61 != null && form61.getRecordDate() != null){
                    statement.setDate(stIndex++, form61.getRecordDate());
                }

                statement.setDate(stIndex++, form61.getModAt());
                statement.setString(stIndex++, form61.getModby());
                statement.setLong(stIndex++, form61.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                    errObj.setErrText("Бичлэг амжилттай засагдлаа.");
                    errObj.setErrDesc("Бичлэг амжилттай засагдлаа.");
                    return errObj;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }


        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1061));
        errObj.setErrText("Амжилтгүй боллоо. Дахин оролдоно уу.");
        errObj.setErrDesc("Амжилтгүй боллоо. Дахин оролдоно уу.");

        return errObj;
    }

    @Override
    public ErrorEntity exportReport(String file, Form61Entity form61) throws SQLException {
        ErrorEntity errorEntity = null;
        String fileName = "hform61";
        try (Connection connection = boneCP.getConnection()){
            String JRXML_PATH = Config.getJrxmlPath();
            String REPORT_PATH = Config.getReportPath();

            try {
                Map parameters = new HashMap();

                String reportTitle = "МАЛ ЭМНЭЛГИЙН АРИУТГАЛ ХАЛДВАРГҮЙЖҮҮЛЭЛТИЙН $$YEAR$$ ОНЫ $$SEASON$$ -Р УЛИРАЛ, ЖИЛИЙН ЭЦСИЙН МЭДЭЭ",
                        currentYear = "$$YEAR$$ он №....",
                        printDate = "$$YEAR$$ оны $$MONTH$$ сарын $$DAY$$";

                Calendar now = Calendar.getInstance();

                int year = now.get(Calendar.YEAR);
                int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
                int day = now.get(Calendar.DAY_OF_MONTH);

                printDate = printDate.replace("$$YEAR$$", String.valueOf(year));
                printDate = printDate.replace("$$MONTH$$", String.valueOf(month));
                printDate = printDate.replace("$$DAY$$", String.valueOf(day));

                currentYear = currentYear.replace("$$YEAR$$", String.valueOf(year));

                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(new Date(form61.getRecordDate().getTime()));

                reportTitle = reportTitle.replace("$$YEAR$$", String.valueOf(calendar1.get(Calendar.YEAR)));

                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(new Date(form61.getSearchRecordDate().getTime()));

                if (calendar2.get(Calendar.MONTH) < 4){
                    reportTitle = reportTitle.replace("$$SEASON$$", "1");
                }
                else if (calendar2.get(Calendar.MONTH) < 7){
                    reportTitle = reportTitle.replace("$$SEASON$$", "2");
                }
                else if (calendar2.get(Calendar.MONTH) < 10){
                    reportTitle = reportTitle.replace("$$SEASON$$", "3");
                }
                else if (calendar2.get(Calendar.MONTH) <= 12){
                    reportTitle = reportTitle.replace("$$SEASON$$", "4");
                }

                parameters.put("start_date", form61.getRecordDate());
                parameters.put("end_date", form61.getSearchRecordDate());
                parameters.put("citycode", (form61.getAimag() != null && !form61.getAimag().isEmpty()) ? form61.getAimag() : null);
                parameters.put("sumcode", (form61.getSum() != null && !form61.getSum().isEmpty()) ? form61.getSum() : null);
                parameters.put("currentyear", currentYear);

                ICityService cityService = new CityServiceImpl(boneCP);
                City tmpCity = new City();
                tmpCity.setCode(form61.getAimag());
                List<City> clist = cityService.selectAll(tmpCity);

                if (clist != null && clist.size() == 1) {
                    parameters.put("aimagner", clist.get(0).getCityname());
                } else {
                    parameters.put("aimagner", (form61.getAimagNer() == null) ? "" : form61.getAimagNer());
                }
                ISumService sumService = new SumServiceImpl(boneCP);
                Sum tmpSum = new Sum();
                tmpSum.setCitycode(form61.getAimag());
                tmpSum.setSumcode(form61.getSum());
                List<Sum> sumList = sumService.selectAll(tmpSum);

                if (sumList != null && sumList.size() == 1){
                    parameters.put("sumner", sumList.get(0).getSumname());
                }
                else{
                    parameters.put("sumner", (form61.getSumNer() == null) ? "" : form61.getSumNer());
                }

                parameters.put("printdate", printDate);
                parameters.put("reporttitle", reportTitle);
                parameters.put("creby", (form61.getCreby() != null && !form61.getCreby().isEmpty()) ? form61.getCreby() : null);

                PdfExcelUtil.generateReport(file, JRXML_PATH, REPORT_PATH, fileName, parameters, connection);

                System.out.println("File Generated");
            }catch (Exception ex){
                ex.printStackTrace();
            }
            finally {
                if (connection != null)connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1001));
            if(file.equals("pdf")){
                errorEntity.setErrText(fileName + ".pdf");
            }else if(file.equals("xls")){
                errorEntity.setErrText(fileName + ".xls");
            }
        }
        return errorEntity;
    }

    @Override
    public List<Form61Entity> getPieChartData(Form61Entity form61Entity) throws SQLException {
        int stIndex = 1;
        List<Form61Entity> form61EntitieList = null;

        String query_sel = "SELECT `hform61`.`id`,  " +
                "    `hform61`.`aimag`,  " +
                "    `hform61`.`sum`,  " +
                "    SUM(`hform61`.`zartsuulsan_hemjee_mn`) as zartsuulsan_hemjee,  " +
                "    c.cityname, c.cityname_en,  " +
                "    `h_sum`.sumname, `h_sum`.sumname_en  " +
                "FROM `hospital`.`hform61`  " +
                "INNER JOIN `h_city` AS c  " +
                "ON c.code = `hform61`.aimag  " +
                "INNER JOIN `h_sum`  " +
                "ON `h_sum`.sumcode = `hform61`.sum  " +
                "WHERE `hform61`.`delflg` = 'N' and `hform61`.`actflg` = 'Y' and `hform61`.`aimag` = ?  " +
                "GROUP BY `hform61`.`sum` ";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                if (form61Entity.getAimag() != null && !form61Entity.getAimag().isEmpty()) {
                    statement.setString(stIndex++, form61Entity.getAimag());
                }

                ResultSet resultSet = statement.executeQuery();

                form61EntitieList = new ArrayList<>();

                while (resultSet.next()) {
                    Form61Entity entity = new Form61Entity();

                    entity.setId(resultSet.getLong("id"));
                    entity.setAimag(resultSet.getString("aimag"));
                    entity.setSum(resultSet.getString("sum"));
//                    entity.setAimagNer(resultSet.getString("cityname"));
//                    entity.setSumNer(resultSet.getString("sumname"));

                    form61EntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return form61EntitieList;
    }

    @Override
    public BasicDBObject getColHighChartData(Form61Entity entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();
        List<String> categories = new ArrayList<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }

        try {

            IComboService comboService = new ComboServiceImpl(boneCP);

            List<BasicDBObject> cmbType = comboService.getComboNames("BARILGATYPE", "MN");
            System.out.println(cmbType);
            //Saraar n haruulah
            int month = calendar1.get(Calendar.MONTH) + 1;
            int year1 = calendar1.get(Calendar.YEAR);
            int year2 = calendar2.get(Calendar.YEAR);

            try (Connection connection = boneCP.getConnection()) {
                String query_sel = "SELECT `*`, sum(`zooriToo`) AS zoori,  sum(`malHaashaaToo`) AS malHaashaa," +
                        "sum(`hudagToo`) AS hudag, sum(`tejAguuToo`) AS tejAguu, `hform61`.`aimag`, `hform61`.`sum`  " +
                        "FROM `hospital`.`hform61`  " +
                        "WHERE   ";

                if (entity != null && entity.getAimag() != null &&!entity.getAimag().isEmpty()) {
                    query_sel += " `hform61`.`aimag` = ? AND ";
                }
                if (entity != null && entity.getSum() != null && !entity.getSum().isEmpty()) {
                    query_sel += " `hform61`.`sum` = ? AND ";
                }
                if (entity != null && !entity.getDelflg().isEmpty()) {
                    query_sel += " `hform61`.`delflg` = ? AND ";
                }
                if (entity != null && !entity.getActflg().isEmpty()) {
                    query_sel += " `hform61`.`actflg` = ? AND ";
                }
                if (entity != null && !entity.getCreby().isEmpty()) {
                    query_sel += " `hform61`.`cre_by` = ? AND ";
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                    query_sel += " (`recorddate` BETWEEN ? AND ?)";
                }

                HashMap<String, List<Double>> resultArray = new HashMap<>();


                do {
                    //do process
                    categories.add(year1 + "-" + month);

                    try {
                        PreparedStatement statement = connection.prepareStatement(query_sel);

                        java.sql.Date stdate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-01 00:00:00").getTime());

                        java.sql.Date enddate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-31 00:00:00").getTime());

                        if (month == 12 && year1 < year2) {
                            month = 1;
                            year1++;
                        } else {
                            month++;
                        }

                        int stindex = 1;

                        if (entity != null && entity.getAimag() != null  && !entity.getAimag().isEmpty()) {
                            statement.setString(stindex++, entity.getAimag());
                        }
                        if (entity != null && entity.getSum() != null  && !entity.getSum().isEmpty()) {
                            statement.setString(stindex++, entity.getSum());
                        }
                        if (entity != null && !entity.getDelflg().isEmpty()) {
                            statement.setString(stindex++, entity.getDelflg());
                        }
                        if (entity != null && !entity.getActflg().isEmpty()) {
                            statement.setString(stindex++, entity.getActflg());
                        }
                        if (entity != null && !entity.getCreby().isEmpty()) {
                            statement.setString(stindex++, entity.getCreby());
                        }
                        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                            statement.setDate(stindex++, stdate);
                            statement.setDate(stindex++, enddate);
                        }


                        ResultSet resultSet = statement.executeQuery();

                        HashMap<String, String> hashMap = new HashMap<>();

                        while (resultSet.next()) {
                            String zoori = resultSet.getString("zoori");
                            String malHaashaa = resultSet.getString("malHaashaa");
                            String hudag = resultSet.getString("hudag");
                            String tejAguu = resultSet.getString("tejAguu");
                            hashMap.put("zoori", zoori);
                            hashMap.put("malHaashaa", malHaashaa);
                            hashMap.put("hudag", hudag);
                            hashMap.put("tejAguu", tejAguu);
                        }//end while

                        List<Double> list = new ArrayList<>();
                        for (int i = 0; i < cmbType.size(); i++) {
                            BasicDBObject obj = cmbType.get(i);

                            if (obj.get("data") == null){
                                //new record
                                list = new ArrayList<>();
                                String val = hashMap.get(obj.getString("id"));
                                if (val == null) list.add(0.0);
                                else if (val.isEmpty()) list.add(0.0);
                                else list.add(Double.parseDouble(val));
                                obj.put("data", list);
                            }else{
                                //already exist

                                list = (List<Double>) obj.get("data");
                                String val = hashMap.get(obj.getString("id"));
                                if (val == null) list.add(0.0);
                                else if (val.isEmpty()) list.add(0.0);
                                else list.add(Double.parseDouble(val));
                            }
                        }

                        if (statement != null) statement.close();
                        if (resultSet != null) resultSet.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();

                        break;
                    }

                    ///
                } while ((month <= calendar2.get(Calendar.MONTH) + 1 && year1 <= year2) || year1 < year2);

                basicDBObject.put("categories", categories);
                basicDBObject.put("series", cmbType);

                connection.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }

    @Override
    public BasicDBObject getPieHighChartData(Form61Entity entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }

        String query_sel = "SELECT `*`, sum(`zooriToo`) AS zoori,  sum(`malHaashaaToo`) AS malHashaa," +
                "sum(`hudagToo`) AS hudag, sum(`tejAguuToo`) AS tejAguu, `hform61`.`aimag`, `hform61`.`sum`  " +
                "FROM `hospital`.`hform61`  " +
                "WHERE   ";

        if (entity != null && entity.getAimag() != null &&!entity.getAimag().isEmpty()) {
            query_sel += " `hform61`.`aimag` = ? AND ";
        }
        if (entity != null && entity.getSum() != null && !entity.getSum().isEmpty()) {
            query_sel += " `hform61`.`sum` = ? AND ";
        }
        if (entity != null && !entity.getDelflg().isEmpty()) {
            query_sel += " `hform61`.`delflg` = ? AND ";
        }
        if (entity != null && !entity.getActflg().isEmpty()) {
            query_sel += " `hform61`.`actflg` = ? AND ";
        }
        if (entity != null && !entity.getCreby().isEmpty()) {
            query_sel += " `hform61`.`cre_by` = ? AND ";
        }
        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
            query_sel += " (`recorddate` BETWEEN ? AND ?)";
        }

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                int stindex = 1;

                if (entity != null && entity.getAimag() != null  && !entity.getAimag().isEmpty()) {
                    statement.setString(stindex++, entity.getAimag());
                }
                if (entity != null && entity.getSum() != null  && !entity.getSum().isEmpty()) {
                    statement.setString(stindex++, entity.getSum());
                }
                if (entity != null && !entity.getDelflg().isEmpty()) {
                    statement.setString(stindex++, entity.getDelflg());
                }
                if (entity != null && !entity.getActflg().isEmpty()) {
                    statement.setString(stindex++, entity.getActflg());
                }
                if (entity != null && !entity.getCreby().isEmpty()) {
                    statement.setString(stindex++, entity.getCreby());
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                    statement.setDate(stindex++, entity.getRecordDate());
                    statement.setDate(stindex++, entity.getSearchRecordDate());
                }
                ResultSet resultSet = statement.executeQuery();

                List<BasicDBObject> list = new ArrayList<>();

                while (resultSet.next()) {
                    BasicDBObject entity1 = new BasicDBObject();
                    BasicDBObject entity2 = new BasicDBObject();
                    BasicDBObject entity3 = new BasicDBObject();
                    BasicDBObject entity4 = new BasicDBObject();
                    entity1.put("name", "Зоорь агуулах");
                    entity1.put("y", resultSet.getDouble("zoori"));
                    entity2.put("name", "Mалын хашаа хороо");
                    entity2.put("y", resultSet.getDouble("malHashaa"));
                    entity3.put("name", "Xудаг уст цэг");
                    entity3.put("y", resultSet.getDouble("hudag"));
                    entity4.put("name", "Tүүхий эд бүтээгдэхүүн хадгалах цэг");
                    entity4.put("y", resultSet.getDouble("tejAguu"));
                    list.add(entity1);
                    list.add(entity2);
                    list.add(entity3);
                    list.add(entity4);
                }

                basicDBObject.put("data", list);

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }
}
