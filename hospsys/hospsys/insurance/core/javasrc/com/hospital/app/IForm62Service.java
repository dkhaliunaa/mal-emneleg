package com.hospital.app;

import com.model.hos.ErrorEntity;
import com.model.hos.Form61Entity;
import com.model.hos.Form62Entity;
import com.mongodb.BasicDBObject;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public interface IForm62Service {

    /**
     * Insert new data
     *
     *
     * @param form62
     * @return
     * @throws Exception
     */
    ErrorEntity insertNewData(Form62Entity form62) throws SQLException;

    /**
     * Select all
     *
     * @param form62
     * @return
     * @throws SQLException
     */
    List<Form62Entity> getListData(Form62Entity form62) throws SQLException;


    /**
     * update form data
     *
     * @param form62
     * @return
     * @throws SQLException
     */
    ErrorEntity updateData(Form62Entity form62) throws SQLException;

    ErrorEntity exportReport(String file, Form62Entity form62) throws SQLException;


    List<Form62Entity> getPieChartData(Form62Entity form62Entity) throws SQLException;

    BasicDBObject getColHighChartData(Form62Entity Form62Entity) throws SQLException;

    BasicDBObject getPieHighChartData(Form62Entity Form62Entity) throws SQLException;
}
