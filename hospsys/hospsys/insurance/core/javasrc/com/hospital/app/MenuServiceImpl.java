package com.hospital.app;

import com.model.User;
import com.model.hos.ErrorEntity;
import com.model.hos.MenuEntity;
import com.jolbox.bonecp.BoneCP;
import com.mongodb.BasicDBObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MenuServiceImpl implements IMenuService {
    private BoneCP boneCP;

    public MenuServiceImpl (BoneCP boneCP) {
        this.boneCP = boneCP;
    }

    @Override
    public List<MenuEntity> selectAll(MenuEntity menu) throws SQLException {
        int stIndex = 1;
        List<MenuEntity> menuEntitieList = null;

        String query_sel = "SELECT * FROM `h_menu` AS c WHERE ";

        if (menu != null && menu.getMenuId() != null && !menu.getMenuId().isEmpty()){
            query_sel += " c.`menu_id` = ? AND ";
        }
        if (menu.getMenuNameMN() != null && !menu.getMenuNameMN().isEmpty()){
            query_sel += " c.`menu_name_mn` = ? AND ";
        }
        if (menu.getMenuNameEN() != null && !menu.getMenuNameEN().isEmpty()){
            query_sel += " c.`menu_name_en` = ? AND ";
        }
        if (menu.getDelflg() != null && !menu.getDelflg().isEmpty()){
            query_sel += " c.`delflg` = ? AND ";
        }

        if (menu.getActflg() != null && !menu.getActflg().isEmpty()){
            query_sel += " c.`actflg` = ? AND ";
        }

        query_sel += "c.id > 0 GROUP BY `c`.`menu_name_mn` ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                if (menu != null && menu.getMenuId() != null && !menu.getMenuId().isEmpty()){
                    statement.setString(stIndex++, menu.getMenuId());
                }
                if (menu.getMenuNameMN() != null && !menu.getMenuNameMN().isEmpty()){
                    statement.setString(stIndex++, menu.getMenuNameMN());
                }
                if (menu.getMenuNameEN() != null && !menu.getMenuNameEN().isEmpty()){
                    statement.setString(stIndex++, menu.getMenuNameEN());
                }
                if (menu.getDelflg() != null && !menu.getDelflg().isEmpty()){
                    statement.setString(stIndex++, menu.getDelflg());
                }

                if (menu.getActflg() != null && !menu.getActflg().isEmpty()){
                    statement.setString(stIndex++, menu.getActflg());
                }

                ResultSet resultSet = statement.executeQuery();

                menuEntitieList = new ArrayList<>();

                int index = 1;

                while (resultSet.next()) {
                    MenuEntity entity = new MenuEntity();

                    entity.setId(resultSet.getLong("id"));
                    entity.setMenuid(resultSet.getString("menu_id"));
                    entity.setMenunamemn(resultSet.getString("menu_name_mn"));
                    entity.setMenunameen(resultSet.getString("menu_name_en"));
                    entity.setMenudesc(resultSet.getString("menu_desc"));
                    entity.setDelflg(resultSet.getString("delflg"));
                    entity.setModat(resultSet.getDate("mod_at"));
                    entity.setModby(resultSet.getString("mod_by"));
                    entity.setCreat(resultSet.getDate("cre_at"));
                    entity.setCreby(resultSet.getString("cre_by"));
                    entity.put("index", index++);

                    menuEntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return menuEntitieList;
    }

    @Override
    public ErrorEntity insertData(MenuEntity menu) throws SQLException {
        return null;
    }

    @Override
    public ErrorEntity updateData(MenuEntity menu) throws SQLException {
        return null;
    }

    @Override
    public List<BasicDBObject> getUserAccessMenuList(User user) {


        return null;
    }
}
