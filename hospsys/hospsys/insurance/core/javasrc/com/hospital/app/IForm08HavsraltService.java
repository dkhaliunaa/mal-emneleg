package com.hospital.app;

import com.model.hos.ErrorEntity;
import com.model.hos.Form08HavsraltEntity;
import com.mongodb.BasicDBObject;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public interface IForm08HavsraltService {

    /**
     * Insert new data
     *
     *
     * @param form08
     * @return
     * @throws Exception
     */
    ErrorEntity insertNewData(Form08HavsraltEntity form08) throws SQLException;

    /**
     * Select all
     *
     * @param form08
     * @return
     * @throws SQLException
     */
    List<Form08HavsraltEntity> getListData(Form08HavsraltEntity form08) throws SQLException;


    /**
     * update form data
     *
     * @param form08
     * @return
     * @throws SQLException
     */
    ErrorEntity updateData(Form08HavsraltEntity form08) throws SQLException;


    List<BasicDBObject> getPieChartData(Form08HavsraltEntity Form08HavsraltEntity) throws SQLException;


    ErrorEntity exportReport(Form08HavsraltEntity form08, String file) throws SQLException;

    /**
     * Column Chart Data processing...
     *
     * @param Form08HavsraltEntity
     * @return
     * @throws SQLException
     */
    BasicDBObject getColumnChartData(Form08HavsraltEntity Form08HavsraltEntity) throws SQLException;


    BasicDBObject getColHighChartData(Form08HavsraltEntity Form08HavsraltEntity) throws SQLException;

    BasicDBObject getPieHighChartData(Form08HavsraltEntity Form08HavsraltEntity) throws SQLException;
}
