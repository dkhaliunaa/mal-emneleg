package com.hospital.app;

import com.enumclass.ErrorType;
import com.model.hos.BagHorooEntity;
import com.model.hos.ErrorEntity;
import com.jolbox.bonecp.BoneCP;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 24be10264 on 9/27/2017.
 */
public class BagHorooServiceImpl implements IBagHorooService {
    private BoneCP boneCP;
    private ErrorEntity errObj;

    public BagHorooServiceImpl (BoneCP boneCP) {
        this.boneCP = boneCP;
    }

    @Override
    public List<BagHorooEntity> selectAll(BagHorooEntity selbagHoroo) throws SQLException {
        int stIndex = 1;
        List<BagHorooEntity> selbagHorooEntitieList = null;

        String query_sel = "SELECT `h_baghoroo`.`id`, `h_baghoroo`.`horoocode`, `h_baghoroo`.`sumcode`, " +
                "`h_baghoroo`.`horooname`, `h_baghoroo`.`horoodesc`, " +
                "`h_baghoroo`.`horooname_en`, `h_baghoroo`.`delflg`, `h_baghoroo`.`actflg`, " +
                "`h_baghoroo`.`cre_at`, `h_baghoroo`.`cre_by`, `h_baghoroo`.`mod_at`, `h_baghoroo`.`mod_by`, " +
                "`h_baghoroo`.`latitude`, `h_baghoroo`.`longitude`, " +
                "h_city.`cityname`, h_city.`cityname_en`, `h_sum`.`sumname`, `h_sum`.`sumname_en`, h_city.code " +
                "FROM `h_baghoroo` INNER JOIN h_sum ON h_baghoroo.sumcode = h_sum.sumcode " +
                "INNER JOIN h_city ON h_sum.citycode = h_city.code WHERE ";

        if (selbagHoroo != null && selbagHoroo.getHoroocode() != null && !selbagHoroo.getHoroocode().isEmpty()){
            query_sel += " `h_baghoroo`.`horoocode` = ? AND ";
        }
        if (selbagHoroo.getSumcode() != null && !selbagHoroo.getSumcode().isEmpty()){
            query_sel += " `h_baghoroo`.`sumcode` = ? AND ";
        }
        if (selbagHoroo.getHorooname() != null && !selbagHoroo.getHorooname().isEmpty()){
            query_sel += " `h_baghoroo`.`horooname` LIKE ? AND ";
        }
        if (selbagHoroo.getHoroodesc() != null && !selbagHoroo.getHoroodesc().isEmpty()){
            query_sel += " `h_baghoroo`.`horoodesc` LIKE ? AND ";
        }

        if (selbagHoroo.getHorooname_en() != null && !selbagHoroo.getHorooname_en().isEmpty()){
            query_sel += " `h_baghoroo`.`horooname_en` = ? AND ";
        };

        if (selbagHoroo.getMod_by() != null && !selbagHoroo.getMod_by().isEmpty()){
            query_sel += " `h_baghoroo`.`mod_by` = ? AND ";
        }

        if (selbagHoroo != null && selbagHoroo.getCre_at() != null && selbagHoroo.getSearchCdate() != null){
            query_sel += " (`h_baghoroo`.`cre_at` BETWEEN ? AND ?) AND ";
        }

        if (selbagHoroo != null && selbagHoroo.getMod_at() != null && selbagHoroo.getSearchMdate() != null){
            query_sel += " (`h_baghoroo`.`mod_at` BETWEEN ? AND ?) AND ";
        }
        if (selbagHoroo != null && selbagHoroo.getActflg() != null && !selbagHoroo.getActflg().isEmpty()){
            query_sel += " `h_baghoroo`.actflg = ? AND ";
        }
        if (selbagHoroo != null && selbagHoroo.getDelflg() != null && !selbagHoroo.getDelflg().isEmpty()){

            query_sel += "`h_baghoroo`.delflg = ? AND ";
        }

        query_sel += "`h_baghoroo`.id > 0 ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                if (selbagHoroo != null && selbagHoroo.getHoroocode() != null && !selbagHoroo.getHoroocode().isEmpty()){
                    statement.setString(stIndex++, selbagHoroo.getHoroocode());
                }
                if (selbagHoroo.getSumcode() != null && !selbagHoroo.getSumcode().isEmpty()){
                    statement.setString(stIndex++, selbagHoroo.getSumcode());
                }
                if (selbagHoroo.getHorooname() != null && !selbagHoroo.getHorooname().isEmpty()){
                    statement.setString(stIndex++, selbagHoroo.getHorooname());
                }
                if (selbagHoroo.getHoroodesc() != null && !selbagHoroo.getHoroodesc().isEmpty()){
                    statement.setString(stIndex++, selbagHoroo.getHoroodesc());
                }

                if (selbagHoroo.getHorooname_en() != null && !selbagHoroo.getHorooname_en().isEmpty()){
                    statement.setString(stIndex++, selbagHoroo.getHorooname_en());
                };

                if (selbagHoroo.getMod_by() != null && !selbagHoroo.getMod_by().isEmpty()){
                    statement.setString(stIndex++, selbagHoroo.getMod_by());
                }

                if (selbagHoroo != null && selbagHoroo.getCre_at() != null && selbagHoroo.getSearchCdate() != null){
                    statement.setDate(stIndex++, selbagHoroo.getCre_at());
                    statement.setDate(stIndex++, selbagHoroo.getSearchCdate());
                }

                if (selbagHoroo != null && selbagHoroo.getMod_at() != null && selbagHoroo.getSearchMdate() != null){
                    statement.setDate(stIndex++, selbagHoroo.getMod_at());
                    statement.setDate(stIndex++, selbagHoroo.getSearchMdate());
                }

                if (selbagHoroo != null && selbagHoroo.getActflg() != null && !selbagHoroo.getActflg().isEmpty()){
                    statement.setString(stIndex++, selbagHoroo.getActflg());
                }
                if (selbagHoroo != null && selbagHoroo.getDelflg() != null && !selbagHoroo.getDelflg().isEmpty()){

                    statement.setString(stIndex++, selbagHoroo.getDelflg());
                }

                ResultSet resultSet = statement.executeQuery();

                selbagHorooEntitieList = new ArrayList<>();

                int index = 1;

                while (resultSet.next()) {
                    BagHorooEntity entity = new BagHorooEntity();

                    entity.setId(resultSet.getLong("id"));
                    entity.setHoroocode(resultSet.getString("horoocode"));
                    entity.setSumcode(resultSet.getString("sumcode"));
                    entity.setHorooname(resultSet.getString("horooname"));
                    entity.setHoroodesc(resultSet.getString("horoodesc"));
                    entity.setHorooname_en(resultSet.getString("horooname_en"));
                    entity.setDelflg(resultSet.getString("delflg"));
                    entity.setActflg(resultSet.getString("actflg"));
                    entity.setMod_at(resultSet.getDate("mod_at"));
                    entity.setMod_by(resultSet.getString("mod_by"));
                    entity.setCre_at(resultSet.getDate("cre_at"));
                    entity.setCre_by(resultSet.getString("cre_by"));
                    entity.setCityName(resultSet.getString("cityname"));
                    entity.setSumName(resultSet.getString("sumname"));
                    entity.setCityCode(resultSet.getString("code"));
                    entity.setLatitude(resultSet.getString("latitude"));
                    entity.setLongitude(resultSet.getString("longitude"));
                    entity.put("index", index++);

                    selbagHorooEntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return selbagHorooEntitieList;
    }

    @Override
    public ErrorEntity insertData(BagHorooEntity entity) throws SQLException {
        PreparedStatement statement = null;

        if (entity == null){
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        String query_login = "INSERT INTO `h_baghoroo`(`horoocode`, `sumcode`, `horooname`, `horoodesc`, `horooname_en`, " +
                "`delflg`, `actflg`, `cre_at`, `cre_by`, `mod_at`, `mod_by`, `h_baghoroo`.`latitude`, `h_baghoroo`.`longitude`) " +
                "VALUES (?,?,?,?,?,?,?,?,?,?,?, ?, ?)";

        try (Connection connection = boneCP.getConnection()) {
            try{
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, (entity.getHoroocode().trim() != null) ? entity.getHoroocode().trim(): entity.getHoroocode().trim());
                statement.setString(2, (entity.getSumcode() != null) ? entity.getSumcode().trim() : entity.getSumcode());
                statement.setString(3, (entity.getHorooname() != null) ? entity.getHorooname().trim():entity.getHorooname());
                statement.setString(4, (entity.getHoroodesc() != null) ? entity.getHoroodesc().trim(): entity.getHoroodesc());
                statement.setString(5, (entity.getHorooname_en() != null) ? entity.getHorooname_en().trim() : entity.getHorooname_en());
                statement.setString(6, (entity.getDelflg() != null)? entity.getDelflg().trim() : entity.getDelflg());
                statement.setString(7, (entity.getActflg() != null)? entity.getActflg().trim() : entity.getActflg());
                statement.setDate(8, entity.getCre_at());
                statement.setString(9, entity.getCre_by());
                statement.setDate(10, entity.getMod_at());
                statement.setString(11, (entity.getMod_by() != null)? entity.getMod_by().trim() : entity.getMod_by());
                statement.setString(12, (entity.getLatitude() != null) ? entity.getLatitude().trim() : entity.getLatitude());
                statement.setString(13, (entity.getLongitude() != null) ? entity.getLongitude().trim() : entity.getLongitude());

                BagHorooEntity searchCity = new BagHorooEntity();
                searchCity.setHoroocode(entity.getHoroocode());
                searchCity.setSumcode(entity.getSumcode());
                List<BagHorooEntity> tmpList = selectAll(searchCity);
                if (tmpList.size() > 0){
                    entity.setId(tmpList.get(0).getId());
                    entity.setDelflg("N");
                    entity.setActflg("Y");
                    return updateData(entity);
                }else{
                    int insertCount = statement.executeUpdate();

                    if (insertCount == 1){
                        connection.commit();
                        errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));
                        errObj.setErrText("Бичлэг амжилттай хадгалагдлаа.");
                        errObj.setErrDesc("Бичлэг амжилттай хадгалагдлаа.");
                        return errObj;
                    }
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }finally {
                if (statement != null)statement.close();
                connection.close();
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }

        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1057));
        errObj.setErrText("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");
        errObj.setErrDesc("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");

        return errObj;
    }

    @Override
    public ErrorEntity updateData(BagHorooEntity entity) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (entity == null) {
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        query = "UPDATE `hospital`.`h_baghoroo` " +
                "SET     ";

        if (entity.getHoroocode() != null && !entity.getHoroocode().isEmpty()){
            query += " `horoocode` = ?, ";
        }

        if (entity.getHorooname() != null && !entity.getHorooname().isEmpty()){
            query += " `horooname` = ?, ";
        }

        if (entity.getSumcode() != null && !entity.getSumcode().toString().isEmpty()){
            query += " `sumcode` = ?, ";
        }
        if (entity.getDelflg() != null && !entity.getDelflg().toString().isEmpty()){
            query += " `delflg` = ?, ";
        }

        if (entity.getActflg() != null && !entity.getActflg().toString().isEmpty()){
            query += " `actflg` = ?, ";
        }
        if (entity.getHoroodesc() != null && !entity.getHoroodesc().toString().isEmpty()){
            query += " `horoodesc` = ?, ";
        }

        if (entity.getLatitude() != null && !entity.getLatitude().toString().isEmpty()){
            query += " `latitude` = ?, ";
        }
        if (entity.getLongitude() != null && !entity.getLongitude().toString().isEmpty()){
            query += " `longitude` = ?, ";
        }

        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";

        query += " WHERE  `id`   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (entity.getHoroocode() != null && !entity.getHoroocode().isEmpty()){
                    statement.setString(stIndex++, entity.getHoroocode().trim());
                }

                if (entity.getHorooname() != null && !entity.getHorooname().isEmpty()){
                    statement.setString(stIndex++, entity.getHorooname().trim());
                }

                if (entity.getSumcode() != null && !entity.getSumcode().toString().isEmpty()){
                    statement.setString(stIndex++, entity.getSumcode().trim());
                }
                if (entity.getDelflg() != null && !entity.getDelflg().toString().isEmpty()){
                    statement.setString(stIndex++, entity.getDelflg().trim());
                }

                if (entity.getActflg() != null && !entity.getActflg().toString().isEmpty()){
                    statement.setString(stIndex++, entity.getActflg().trim());
                }
                if (entity.getHoroodesc() != null && !entity.getHoroodesc().toString().isEmpty()){
                    statement.setString(stIndex++, entity.getHoroodesc().trim());
                }

                if (entity.getLatitude() != null && !entity.getLatitude().toString().isEmpty()){
                    statement.setString(stIndex++, entity.getLatitude().trim());
                }
                if (entity.getLongitude() != null && !entity.getLongitude().toString().isEmpty()){
                    statement.setString(stIndex++, entity.getLongitude().trim());
                }

                statement.setDate(stIndex++, entity.getMod_at());
                statement.setString(stIndex++, entity.getMod_by().trim());
                statement.setLong(stIndex++, entity.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                    errObj.setErrText("Бичлэг амжилттай засагдлаа.");
                    errObj.setErrDesc("Бичлэг амжилттай засагдлаа.");
                    return errObj;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }


        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1061));
        errObj.setErrText("Амжилтгүй боллоо. Дахин оролдоно уу.");
        errObj.setErrDesc("Амжилтгүй боллоо. Дахин оролдоно уу.");

        return errObj;
    }
}
