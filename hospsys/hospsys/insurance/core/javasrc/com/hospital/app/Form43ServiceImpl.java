package com.hospital.app;


import com.Config;
import com.enumclass.ErrorType;
import com.jolbox.bonecp.BoneCP;
import com.model.hos.City;
import com.model.hos.ErrorEntity;
import com.model.hos.Form43Entity;
import com.model.hos.Sum;
import net.sf.jasperreports.engine.JRStyle;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.design.JRDesignStyle;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public class Form43ServiceImpl implements IForm43Service{
    /**
     * Objects
     */

    private BoneCP boneCP;


    public Form43ServiceImpl(BoneCP boneCP) {
        this.boneCP = boneCP;
    }

    @Override
    public ErrorEntity insertNewData(Form43Entity form43) throws SQLException {
        PreparedStatement statement = null;

        if (form43 == null) new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));

        String query_login = "INSERT INTO `hospital`.`hform43` " +
                "(`ajahuinNegj_mn`, " +
                "`tusgaiZovshoorol`, " +
                "`ajahuinNegj_en`, " +
                "`emBeldmelNer_mn`, " +
                "`emBeldmelNer_en`, " +
                "`hemjihNegj_mn`, " +
                "`hemjihNegj_en`, " +
                "`Parmakop`, " +
                "`uildverlesenToo`, " +
                "`negjUne`, " +
                "`niitUne`, " +
                "`delflg`, " +
                "`actflg`, " +
                "`mod_at`, " +
                "`mod_by`, " +
                "`cre_at`, " +
                "`recorddate`, " +
                "`cre_by`," +
                "`aimag`," +
                "`sum`," +
                "`aimag_en`," +
                "`sum_en`)" +
                "VALUES " +
                "(? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?,?, ?, ?, ?, ?, ?,?,? )";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, form43.getAjahuinNegj());
                statement.setDate(2,form43.getTusgaiZovshoorol());
                statement.setString(3, form43.getAjahuinNegj_en());
                statement.setString(4, form43.getEmBeldmelNer());
                statement.setString(5, form43.getEmBeldmelNer_en());
                statement.setString(6, form43.getHemjihNegj());
                statement.setString(7, form43.getHemjihNegj_en());
                statement.setLong(8,form43.getParmakop());
                statement.setLong(9, form43.getUildverlesenToo());
                statement.setString(10, form43.getNegjUne());
                statement.setString(11, form43.getNiitUne());
                statement.setString(12, form43.getDelflg());
                statement.setString(13, form43.getActflg());
                statement.setDate(14, form43.getModAt());
                statement.setString(15, form43.getModby());
                statement.setDate(16, form43.getCreat());
                statement.setDate(17,form43.getRecordDate());
                statement.setString(18, form43.getCreby());
                statement.setString(19, form43.getAimag());
                statement.setString(20, form43.getSum());
                statement.setString(21, form43.getAimag_en());
                statement.setString(22, form43.getSum_en());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    return new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }

        } catch (Exception ex) {

        }

        return new ErrorEntity(ErrorType.ERROR, Long.valueOf(1057));
    }

    @Override
    public List<Form43Entity> getListData(Form43Entity form43) throws SQLException {
        int stIndex = 1;
        List<Form43Entity> form43EntitieList = null;

        String query = "SELECT `hform43`.`id`, " +
                "    `hform43`.`ajahuinNegj_mn`, " +
                "    `hform43`.`tusgaiZovshoorol`, " +
                "    `hform43`.`ajahuinNegj_en`, " +
                "    `hform43`.`emBeldmelNer_mn`, " +
                "    `hform43`.`emBeldmelNer_en`, " +
                "    `hform43`.`hemjihNegj_mn`, " +
                "    `hform43`.`hemjihNegj_en`, " +
                "    `hform43`.`Parmakop`, " +
                "    `hform43`.`uildverlesenToo`, " +
                "    `hform43`.`negjUne`, " +
                "    `hform43`.`niitUne`, " +
                "    `hform43`.`delflg`, " +
                "    `hform43`.`actflg`, " +
                "    `hform43`.`mod_at`, " +
                "    `hform43`.`mod_by`, " +
                "    `hform43`.`cre_at`, " +
                "    `hform43`.`recorddate`, " +
                "    `hform43`.`cre_by`, " +
                "    `hform43`.`aimag`, " +
                "    `hform43`.`sum`, " +
                "    `hform43`.`aimag_en`, " +
                "    `hform43`.`sum_en`, " +
                "    c.cityname, c.cityname_en, " +
                "    `h_sum`.sumname, `h_sum`.sumname_en " +
                "FROM `hospital`.`hform43` INNER JOIN `h_city` AS c " +
                "ON c.code = `hform43`.aimag " +
                "INNER JOIN `h_sum` " +
                "ON `h_sum`.sumcode = `hform43`.sum " +
                "WHERE ";

        if (form43.getAjahuinNegj() != null && !form43.getAjahuinNegj().isEmpty()) {
            query += " ajahuinNegj_mn = ? AND ";
        }

        if (form43.getAjahuinNegj_en() != null && !form43.getAjahuinNegj_en().isEmpty()) {
            query += " ajahuinNegj_en = ? AND ";
        }

        /* NER */
        if (form43.getEmBeldmelNer() != null && !form43.getEmBeldmelNer().toString().isEmpty()) {
            query += " emBeldmelNer_mn = ? AND ";
        }
        if (form43.getEmBeldmelNer_en() != null && !form43.getEmBeldmelNer_en().toString().isEmpty()) {
            query += " emBeldmelNer_en = ? AND ";
        }
        /* Zartsuulsan hemjee */
        if (form43.getHemjihNegj() != null && !form43.getHemjihNegj().toString().isEmpty()) {
            query += " hemjihNegj_mn = ? AND ";
        }
        if (form43.getHemjihNegj_en() != null && !form43.getHemjihNegj_en().toString().isEmpty()) {
            query += " hemjihNegj_en = ? AND ";
        }

        /* MAL TURUL */
        if (form43.getUildverlesenToo() != null && !form43.getUildverlesenToo().toString().isEmpty() && form43.getUildverlesenToo().longValue() != 0) {
            query += " uildverlesenToo = ? AND ";
        }
        if (form43.getNegjUne() != null && !form43.getNegjUne().toString().isEmpty() && form43.getNegjUne() != "0") {
            query += " negjUne = ? AND ";
        }

        /* Tuluvlugu */
        if (form43.getNiitUne() != null && !form43.getNiitUne().isEmpty() && form43.getNiitUne() != "0") {
            query += " niitUne = ? AND ";
        }

        if (form43 != null && form43.getAimag() != null && !form43.getAimag().isEmpty()) {
            query += " `aimag` = ? AND ";
        }
        if (form43 != null && form43.getSum() != null && !form43.getSum().isEmpty()) {
            query += " `sum` = ? AND ";
        }

        if (form43 != null && form43.getRecordDate() != null && form43.getSearchRecordDate() != null){
            query += " (`recorddate` BETWEEN ? AND ?) AND ";
        }

        if (form43 != null && form43.getCreby() != null && !form43.getCreby().isEmpty()){
            query += " `hform43`.`cre_by` = ? AND ";
        }
        if(form43 != null && form43.getTusgaiZovshoorol() !=null && form43.getTusgaiZovshoorol().toString().isEmpty()){
            query += " `hform43`.`tusgaiZovshoorol` = ? AND ";
        }

        if(form43 != null && form43.getParmakop() !=null && form43.getParmakop().toString().isEmpty()){
            query += " `hform43`.`Parmakop` = ? AND ";
        }

        query += " `hform43`.`delflg` = 'N' ORDER BY `cre_at` DESC";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query);

                if (form43.getAjahuinNegj() != null && !form43.getAjahuinNegj().isEmpty()) {
                    statement.setString(stIndex++, form43.getAjahuinNegj());
                }

                if (form43.getAjahuinNegj_en() != null && !form43.getAjahuinNegj_en().isEmpty()) {
                    statement.setString(stIndex++, form43.getAjahuinNegj_en());
                }

            /* NER */
                if (form43.getEmBeldmelNer() != null && !form43.getEmBeldmelNer().toString().isEmpty()) {
                    statement.setString(stIndex++, form43.getEmBeldmelNer());
                }
                if (form43.getEmBeldmelNer_en() != null && !form43.getEmBeldmelNer_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form43.getEmBeldmelNer_en());
                }

            /* Zartsuulsan hemjee */
                if (form43.getHemjihNegj() != null && !form43.getHemjihNegj().toString().isEmpty()) {
                    statement.setString(stIndex++, form43.getHemjihNegj());
                }
                if (form43.getHemjihNegj_en() != null && !form43.getHemjihNegj_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form43.getHemjihNegj_en());
                }

            /* MAL TURUL */
                if (form43.getUildverlesenToo() != null && !form43.getUildverlesenToo().toString().isEmpty() && form43.getUildverlesenToo().longValue() != 0) {
                    statement.setLong(stIndex++, form43.getUildverlesenToo());
                }
                if (form43.getNegjUne() != null && !form43.getNegjUne().isEmpty() && form43.getNegjUne() != "0") {
                    statement.setString(stIndex++, form43.getNegjUne());
                }

            /* Tuluvlugu */
                if (form43.getNiitUne() != null && !form43.getNiitUne().isEmpty() && form43.getNiitUne() != "0") {
                    statement.setString(stIndex++, form43.getNiitUne());
                }
                if (form43 != null && form43.getAimag() != null && !form43.getAimag().isEmpty()) {
                    statement.setString(stIndex++, form43.getAimag());
                }
                if (form43 != null && form43.getSum() != null && !form43.getSum().isEmpty()) {
                    statement.setString(stIndex++, form43.getSum());
                }

                if (form43 != null && form43.getRecordDate() != null && form43.getSearchRecordDate() != null){
                    statement.setDate(stIndex++, form43.getRecordDate());
                    statement.setDate(stIndex++, form43.getSearchRecordDate());
                }
                if (form43 != null && form43.getCreby() != null && !form43.getCreby().isEmpty()){
                    statement.setString(stIndex++, form43.getCreby());
                }

                if(form43 != null && form43.getTusgaiZovshoorol() !=null && form43.getTusgaiZovshoorol().toString().isEmpty()){
                    statement.setDate(stIndex++, form43.getTusgaiZovshoorol());
                }

                if (form43.getParmakop() != null && !form43.getParmakop().toString().isEmpty() && form43.getParmakop() != 0){
                    statement.setLong(stIndex++, form43.getParmakop());
                }

                ResultSet resultSet = statement.executeQuery();

                form43EntitieList = new ArrayList<>();
                int index=1;

                while (resultSet.next()) {
                    Form43Entity entity = new Form43Entity();

                    entity.setId(resultSet.getLong("id"));
                    entity.setAjahuinNegj(resultSet.getString("ajahuinNegj_mn"));
                    entity.setTusgaiZovshoorol(resultSet.getDate("tusgaiZovshoorol"));
                    entity.setAjahuinNegj_en(resultSet.getString("ajahuinNegj_en"));
                    entity.setEmBeldmelNer(resultSet.getString("emBeldmelNer_mn"));
                    entity.setEmBeldmelNer_en(resultSet.getString("emBeldmelNer_en"));
                    entity.setHemjihNegj(resultSet.getString("hemjihNegj_mn"));
                    entity.setHemjihNegj_en(resultSet.getString("hemjihNegj_en"));
                    entity.setParmakop(resultSet.getLong("Parmakop"));
                    entity.setUildverlesenToo(resultSet.getLong("uildverlesenToo"));
                    entity.setNegjUne(resultSet.getString("negjUne"));
                    entity.setNiitUne(resultSet.getString("niitUne"));

                    entity.setDelflg(resultSet.getString("delflg"));
                    entity.setActflg(resultSet.getString("actflg"));
                    entity.setModAt(resultSet.getDate("mod_at"));
                    entity.setModby(resultSet.getString("mod_by"));
                    entity.setCreat(resultSet.getDate("cre_at"));
                    entity.setRecordDate(resultSet.getDate("recorddate"));
                    entity.setCreby(resultSet.getString("cre_by"));
                    entity.setAimag(resultSet.getString("aimag"));
                    entity.setSum(resultSet.getString("sum"));
                    entity.setAimag_en(resultSet.getString("aimag_en"));
                    entity.setSum_en(resultSet.getString("sum_en"));
                    entity.put("index",index++);
                    //entity.setMalTurulNer(comboService.getComboName(resultSet.getString("mal_turul_mn"), ContextHelper.getLanguageCode()));

                    form43EntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return form43EntitieList;
    }

    @Override
    public ErrorEntity updateData(Form43Entity form43) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (form43 == null) new ErrorEntity(ErrorType.INFO, Long.valueOf(1059));

        query = "UPDATE `hospital`.`hform43` " +
                "SET     ";

        if (form43.getAjahuinNegj() != null && !form43.getAjahuinNegj().isEmpty()) {
            query += " ajahuinNegj_mn = ?, ";
        }

        if (form43.getAjahuinNegj_en() != null && !form43.getAjahuinNegj_en().isEmpty()) {
            query += " ajahuinNegj_en = ? , ";
        }

        /* NER */
        if (form43.getEmBeldmelNer() != null && !form43.getEmBeldmelNer().toString().isEmpty()) {
            query += " emBeldmelNer_mn = ? , ";
        }
        if (form43.getEmBeldmelNer_en() != null && !form43.getEmBeldmelNer_en().toString().isEmpty()) {
            query += " emBeldmelNer_en = ? , ";
        }
        /* Zartsuulsan hemjee */
        if (form43.getHemjihNegj() != null && !form43.getHemjihNegj().toString().isEmpty()) {
            query += " hemjihNegj_mn = ? , ";
        }
        if (form43.getHemjihNegj_en() != null && !form43.getHemjihNegj_en().toString().isEmpty()) {
            query += " hemjihNegj_en = ? , ";
        }

        /* MAL TURUL */
        if (form43.getUildverlesenToo() != null && !form43.getUildverlesenToo().toString().isEmpty() && form43.getUildverlesenToo().longValue() != 0) {
            query += " uildverlesenToo = ? , ";
        }
        if (form43.getNegjUne() != null && !form43.getNegjUne().toString().isEmpty() && form43.getNegjUne() != "0") {
            query += " negjUne = ? , ";
        }

        /* Tuluvlugu */
        if (form43.getNiitUne() != null && !form43.getNiitUne().isEmpty() && form43.getNiitUne() != "0") {
            query += " niitUne = ? , ";
        }

        /* Delete flag */
        if (form43.getDelflg() != null && !form43.getDelflg().toString().isEmpty()) {
            query += " delflg = ?, ";
        }

        /* Active flag */
        if (form43.getActflg() != null && !form43.getActflg().toString().isEmpty()) {
            query += " actflg = ?, ";
        }

        if (form43 != null && form43.getRecordDate() != null){
            query += " `recorddate` = ?, ";
        }

        if(form43 != null && form43.getTusgaiZovshoorol() !=null && form43.getTusgaiZovshoorol().toString().isEmpty()){
            query += " `hform43`.`tusgaiZovshoorol` = ?, ";
        }

        if(form43.getParmakop() != null && !form43.getParmakop().toString().isEmpty() && form43.getParmakop() != 0){
            query += " `hform43`.`Parmakop` = ?, ";
        }
        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";
        query += " WHERE  ID   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (form43.getAjahuinNegj() != null && !form43.getAjahuinNegj().isEmpty()) {
                    statement.setString(stIndex++, form43.getAjahuinNegj());
                }

                if (form43.getAjahuinNegj_en() != null && !form43.getAjahuinNegj_en().isEmpty()) {
                    statement.setString(stIndex++, form43.getAjahuinNegj_en());
                }

            /* NER */
                if (form43.getEmBeldmelNer() != null && !form43.getEmBeldmelNer().toString().isEmpty()) {
                    statement.setString(stIndex++, form43.getEmBeldmelNer());
                }
                if (form43.getEmBeldmelNer_en() != null && !form43.getEmBeldmelNer_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form43.getEmBeldmelNer_en());
                }

            /* Zartsuulsan hemjee */
                if (form43.getHemjihNegj() != null && !form43.getHemjihNegj().toString().isEmpty()) {
                    statement.setString(stIndex++, form43.getHemjihNegj());
                }
                if (form43.getHemjihNegj_en() != null && !form43.getHemjihNegj_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form43.getHemjihNegj_en());
                }

            /* MAL TURUL */
                if (form43.getUildverlesenToo() != null && !form43.getUildverlesenToo().toString().isEmpty() && form43.getUildverlesenToo().longValue() != 0) {
                    statement.setLong(stIndex++, form43.getUildverlesenToo());
                }
                if (form43.getNegjUne() != null && !form43.getNegjUne().isEmpty() && form43.getNegjUne() != "0") {
                    statement.setString(stIndex++, form43.getNegjUne());
                }

            /* Tuluvlugu */
                if (form43.getNiitUne() != null && !form43.getNiitUne().isEmpty() && form43.getNiitUne() != "0") {
                    statement.setString(stIndex++, form43.getNiitUne());
                }
            /* Delete flag */
                if (form43.getDelflg() != null && !form43.getDelflg().toString().isEmpty()) {
                    statement.setString(stIndex++, form43.getDelflg());
                }
            /* Active flag */
                if (form43.getActflg() != null && !form43.getActflg().toString().isEmpty()) {
                    statement.setString(stIndex++, form43.getActflg());
                }

                if (form43 != null && form43.getRecordDate() != null){
                    statement.setDate(stIndex++, form43.getRecordDate());
                }

                if(form43 != null && form43.getTusgaiZovshoorol() !=null && form43.getTusgaiZovshoorol().toString().isEmpty()){
                    statement.setDate(stIndex++, form43.getTusgaiZovshoorol());
                }
                if (form43.getParmakop() != null && !form43.getParmakop().toString().isEmpty() && form43.getParmakop() != 0){
                    statement.setLong(stIndex++, form43.getParmakop());
                }
                statement.setDate(stIndex++, form43.getModAt());
                statement.setString(stIndex++, form43.getModby());
                statement.setLong(stIndex++, form43.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    return new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        return new ErrorEntity(ErrorType.ERROR, Long.valueOf(1061));
    }
    @Override
    public List<Form43Entity> getPieChartData(Form43Entity form43Entity) throws SQLException {
        int stIndex = 1;
        List<Form43Entity> form43EntitieList = null;

        String query_sel = "SELECT `hform43`.`id`,  " +
                "    `hform43`.`aimag`,  " +
                "    `hform43`.`sum`,  " +
                "    SUM(`hform43`.`zartsuulsan_hemjee_mn`) as zartsuulsan_hemjee,  " +
                "    c.cityname, c.cityname_en,  " +
                "    `h_sum`.sumname, `h_sum`.sumname_en  " +
                "FROM `hospital`.`hform43`  " +
                "INNER JOIN `h_city` AS c  " +
                "ON c.code = `hform43`.aimag  " +
                "INNER JOIN `h_sum`  " +
                "ON `h_sum`.sumcode = `hform43`.sum  " +
                "WHERE `hform43`.`delflg` = 'N' and `hform43`.`actflg` = 'Y' and `hform43`.`aimag` = ?  " +
                "GROUP BY `hform43`.`sum` ";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                if (form43Entity.getAimag() != null && !form43Entity.getAimag().isEmpty()) {
                    statement.setString(stIndex++, form43Entity.getAimag());
                }

                ResultSet resultSet = statement.executeQuery();

                form43EntitieList = new ArrayList<>();

                while (resultSet.next()) {
                    Form43Entity entity = new Form43Entity();

                    entity.setId(resultSet.getLong("id"));
                    entity.setAimag(resultSet.getString("aimag"));
                    entity.setSum(resultSet.getString("sum"));
//                    entity.setAimagNer(resultSet.getString("cityname"));
//                    entity.setSumNer(resultSet.getString("sumname"));

                    form43EntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return form43EntitieList;
    }

    @Override
    public ErrorEntity exportReport(Form43Entity form43) throws SQLException {
        ErrorEntity errorEntity = null;

        try (Connection connection = boneCP.getConnection()){
            String JRXML_PATH = Config.getJrxmlPath();
            String REPORT_PATH = Config.getReportPath();

            try {
                Map parameters = new HashMap();

                String reportTitle = "ЭМ БЭЛДМЭЛ ҮЙЛДВЭРЛЭЛИЙН $$YEAR$$ ОНЫ МЭДЭЭ",
                        currentYear = "$$YEAR$$ он №....",
                        printDate = "$$YEAR$$ оны $$MONTH$$ сарын $$DAY$$";

                Calendar now = Calendar.getInstance();

                int year = now.get(Calendar.YEAR);
                int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
                int day = now.get(Calendar.DAY_OF_MONTH);

                printDate = printDate.replace("$$YEAR$$", String.valueOf(year));
                printDate = printDate.replace("$$MONTH$$", String.valueOf(month));
                printDate = printDate.replace("$$DAY$$", String.valueOf(day));

                currentYear = currentYear.replace("$$YEAR$$", String.valueOf(year));

                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(new Date(form43.getRecordDate().getTime()));

                reportTitle = reportTitle.replace("$$YEAR$$", String.valueOf(calendar1.get(Calendar.YEAR)));

                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(new Date(form43.getSearchRecordDate().getTime()));

                if (calendar2.get(Calendar.MONTH) < 4){
                    reportTitle = reportTitle.replace("$$SEASON$$", "1");
                }
                else if (calendar2.get(Calendar.MONTH) < 7){
                    reportTitle = reportTitle.replace("$$SEASON$$", "2");
                }
                else if (calendar2.get(Calendar.MONTH) < 10){
                    reportTitle = reportTitle.replace("$$SEASON$$", "3");
                }
                else if (calendar2.get(Calendar.MONTH) <= 12){
                    reportTitle = reportTitle.replace("$$SEASON$$", "4");
                }

                parameters.put("start_date", form43.getRecordDate());
                parameters.put("end_date", form43.getSearchRecordDate());
                parameters.put("citycode", form43.getAimag());
                parameters.put("sumcode", form43.getSum());
                parameters.put("currentyear", currentYear);

                ICityService cityService = new CityServiceImpl(boneCP);
                City tmpCity = new City();
                tmpCity.setCode(form43.getAimag());
                List<City> clist = cityService.selectAll(tmpCity);

                if (clist != null && clist.size() == 1) {
                    parameters.put("aimagner", clist.get(0).getCityname());
                } else {
                    parameters.put("aimagner", (form43.getAimagNer() == null) ? "" : form43.getAimagNer());
                }
                ISumService sumService = new SumServiceImpl(boneCP);
                Sum tmpSum = new Sum();
                tmpSum.setCitycode(form43.getAimag());
                tmpSum.setSumcode(form43.getSum());
                List<Sum> sumList = sumService.selectAll(tmpSum);

                if (sumList != null && sumList.size() == 1){
                    parameters.put("sumner", sumList.get(0).getSumname());
                }
                else{
                    parameters.put("sumner", (form43.getSumNer() == null) ? "" : form43.getSumNer());
                }

                parameters.put("printdate", printDate);
                parameters.put("reporttitle", reportTitle);

                JasperPrint jasperPrint = JasperFillManager.fillReport(JRXML_PATH + "/hform43.jasper",
                        parameters,
                        connection);

                JRStyle jrStyle = new JRDesignStyle();
                jrStyle.setFontName("DejaVu Serif");
                jrStyle.setPdfEncoding("Identity-H");
                jrStyle.setPdfEmbedded(true);
                jasperPrint.setDefaultStyle(jrStyle);

            /* outputStream to create PDF */
                OutputStream outputStream = new FileOutputStream(new File(REPORT_PATH+"/hform43.pdf"));
            /* Write content to PDF file */
                JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);

                System.out.println("File Generated");
            }catch (Exception ex){
                ex.printStackTrace();
            }
            finally {
                if (connection != null)connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1001));
            errorEntity.setErrText("hform43.pdf");
        }
        return errorEntity;
    }

}
