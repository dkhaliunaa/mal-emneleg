package com.hospital.app;


import com.Config;
import com.enumclass.ErrorType;
import com.jolbox.bonecp.BoneCP;
import com.model.hos.City;
import com.model.hos.ErrorEntity;
import com.model.hos.Form09HavsraltEntity;
import com.model.hos.Sum;
import com.mongodb.BasicDBObject;
import com.rpc.PdfExcelUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public class Form09HavsraltServiceImpl implements IForm09HavsraltService{
    /**
     * Objects
     */

    private BoneCP boneCP;


    public Form09HavsraltServiceImpl(BoneCP boneCP) {
        this.boneCP = boneCP;
    }

    @Override
    public ErrorEntity insertNewData(Form09HavsraltEntity form09) throws SQLException {
        PreparedStatement statement = null;

        if (form09 == null) new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));

        String query_login = "INSERT INTO `hospital`.`hform09_hav` " +
                "(`citycode`, " +
                "`sumcode`, " +
                "`bagcode`, " +
                "`gazarner`, " +
                "`urtrag`, " +
                "`orgorog`, " +
                "`golomttoo`, " +
                "`ovchin_ner`, " +
                "`mal_turul`, " +
                "`uvchilson`, " +
                "`uhsen`, " +
                "`edgersen`, " +
                "`delflg`, " +
                "`cre_by`, " +
                "`cre_at`, " +
                "`mod_at`, " +
                "`mod_by`, " +
                "`record_date`) " +
                "VALUES " +
                "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try (Connection connection = boneCP.getConnection()) {
            try{
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, form09.getCitycode());
                statement.setString(2, form09.getSumcode());
                statement.setString(3, form09.getBagcode());
                statement.setString(4, form09.getGazarner());
                statement.setString(5, form09.getUrtrag());
                statement.setString(6, form09.getOrgorog());
                statement.setInt(7, form09.getGolomttoo());
                statement.setString(8, form09.getOvchin_ner());
                statement.setString(9, form09.getMal_turul());
                statement.setInt(10, form09.getUvchilson());
                statement.setInt(11, form09.getUhsen());
                statement.setInt(12, form09.getEdgersen());
                statement.setString(13, form09.getDelflg());
                statement.setString(14, form09.getCreby());
                statement.setDate(15, form09.getCreat());
                statement.setDate(16, form09.getModAt());
                statement.setString(17, form09.getModby());
                statement.setDate(18, form09.getRecordDate());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1){
                    connection.commit();
                    return new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }finally {
                if (statement != null)statement.close();
                connection.close();
            }

        }catch (Exception ex){

        }

        return new ErrorEntity(ErrorType.ERROR, Long.valueOf(1057));
    }

    @Override
    public List<Form09HavsraltEntity> getListData(Form09HavsraltEntity form09) throws SQLException {
        int stIndex = 1;
        List<Form09HavsraltEntity> form09EntitieList = null;

        String query = "SELECT `hform09_hav`.`id`, " +
                "    `hform09_hav`.`citycode`, " +
                "    `hform09_hav`.`sumcode`, " +
                "    `hform09_hav`.`bagcode`, " +
                "    `hform09_hav`.`gazarner`, " +
                "    `hform09_hav`.`urtrag`, " +
                "    `hform09_hav`.`orgorog`, " +
                "    `hform09_hav`.`golomttoo`, " +
                "    `hform09_hav`.`ovchin_ner`, " +
                "    `hform09_hav`.`mal_turul`, " +
                "    `hform09_hav`.`uvchilson`, " +
                "    `hform09_hav`.`uhsen`, " +
                "    `hform09_hav`.`edgersen`, " +
                "    `hform09_hav`.`delflg`, " +
                "    `hform09_hav`.`cre_by`, " +
                "    `hform09_hav`.`cre_at`, " +
                "    `hform09_hav`.`mod_at`, " +
                "    `hform09_hav`.`mod_by`, " +
                "    `hform09_hav`.`record_date`, " +
                "    c.cityname, c.cityname_en, " +
                "    `h_sum`.sumname, `h_sum`.sumname_en, " +
                "   `h_baghoroo`.horooname, `h_baghoroo`.horooname_en " +
                "FROM `hospital`.`hform09_hav` INNER JOIN `h_city` AS c " +
                "ON c.code = `hform09_hav`.citycode " +
                "INNER JOIN `h_sum` " +
                "ON `h_sum`.sumcode = `hform09_hav`.sumcode " +
                "INNER JOIN `h_baghoroo` ON `hform09_hav`.bagcode = `h_baghoroo`.horoocode AND `h_baghoroo`.sumcode = `hform09_hav`.`sumcode`" +
                "WHERE ";

        if (form09.getCitycode() != null && !form09.getCitycode().isEmpty()){
            query += " `hform09_hav`.citycode = ? AND ";
        }

        if (form09.getSumcode() != null && !form09.getSumcode().isEmpty()){
            query += " `hform09_hav`.sumcode = ? AND ";
        }

        if (form09.getBagcode() != null && !form09.getBagcode().toString().isEmpty()){
            query += " `hform09_hav`.bagcode = ? AND ";
        }
        if (form09.getGazarner() != null && !form09.getGazarner().toString().isEmpty()){
            query += " `hform09_hav`.gazarner = ? AND ";
        }
        if (form09.getUrtrag() != null && !form09.getUrtrag().toString().isEmpty()){
            query += " `hform09_hav`.urtrag = ? AND ";
        }
        if (form09.getOrgorog() != null && !form09.getOrgorog().toString().isEmpty()){
            query += " `hform09_hav`.orgorog = ? AND ";
        }

        if (form09.getGolomttoo() != 0){
            query += " `hform09_hav`.golomttoo = ? AND ";
        }
        if (form09.getOvchin_ner() != null && !form09.getOvchin_ner().toString().isEmpty()){
            query += " `hform09_hav`.ovchin_ner = ? AND ";
        }

        if (form09.getMal_turul() != null && !form09.getMal_turul().isEmpty()){
            query += " `hform09_hav`.mal_turul = ? AND ";
        }
        if (form09.getUvchilson() != 0){
            query += " `hform09_hav`.uvchilson = ? AND ";
        }

        if (form09.getUhsen() != 0){
            query += " `hform09_hav`.uhsen = ? AND ";
        }
        if (form09.getEdgersen() != 0){
            query += " `hform09_hav`.edgersen = ? AND ";
        }

        if (form09 != null && form09.getRecordDate() != null && form09.getSearchRecordDate() != null){
            query += " (`hform09_hav`.`record_date` BETWEEN ? AND ?) AND ";
        }

        if (form09 != null && form09.getCreby() != null && !form09.getCreby().isEmpty()){
            query += " `hform09_hav`.`cre_by` = ? AND ";
        }

        if (form09 != null && form09.getDelflg() != null && !form09.getDelflg().isEmpty()){
            query += " `hform09_hav`.`delflg` = ? AND ";
        }

        if (form09 != null && form09.getCreat() != null && form09.getSearchRecordDate() != null){
            query += " (`hform09_hav`.`cre_at` BETWEEN ? AND ?) AND ";
        }

        query += " `hform09_hav`.`id` > 0 ";
        query += " ORDER BY `hform09_hav`.`cre_at` DESC ";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query);

                if (form09.getCitycode() != null && !form09.getCitycode().isEmpty()){
                    statement.setString(stIndex++, form09.getCitycode());
                }

                if (form09.getSumcode() != null && !form09.getSumcode().isEmpty()){
                    statement.setString(stIndex++, form09.getSumcode());
                }

                if (form09.getBagcode() != null && !form09.getBagcode().toString().isEmpty()){
                    statement.setString(stIndex++, form09.getBagcode());
                }
                if (form09.getGazarner() != null && !form09.getGazarner().toString().isEmpty()){
                    statement.setString(stIndex++, form09.getGazarner());
                }
                if (form09.getUrtrag() != null && !form09.getUrtrag().toString().isEmpty()){
                    statement.setString(stIndex++, form09.getUrtrag());
                }
                if (form09.getOrgorog() != null && !form09.getOrgorog().toString().isEmpty()){
                    statement.setString(stIndex++, form09.getOrgorog());
                }

                if (form09.getGolomttoo() != 0){
                    statement.setInt(stIndex++, form09.getGolomttoo());
                }
                if (form09.getOvchin_ner() != null && !form09.getOvchin_ner().toString().isEmpty()){
                    statement.setString(stIndex++, form09.getOvchin_ner());
                }

                if (form09.getMal_turul() != null && !form09.getMal_turul().isEmpty()){
                    System.out.println(form09.getMal_turul());
                    statement.setString(stIndex++, form09.getMal_turul());
                }
                if (form09.getUvchilson() != 0){
                    statement.setInt(stIndex++, form09.getUvchilson());
                                                                                                                                                                                                                                                                                                                        }

                if (form09.getUhsen() != 0){
                    statement.setInt(stIndex++, form09.getUhsen());
                }
                if (form09.getEdgersen() != 0){
                    statement.setInt(stIndex++, form09.getEdgersen());
                }


                if (form09 != null && form09.getRecordDate() != null && form09.getSearchRecordDate() != null){
                    statement.setDate(stIndex++, form09.getRecordDate());
                    statement.setDate(stIndex++, form09.getSearchRecordDate());
                }

                if (form09 != null && form09.getCreby() != null && !form09.getCreby().isEmpty()){
                    statement.setString(stIndex++, form09.getCreby());
                }

                if (form09 != null && form09.getDelflg() != null && !form09.getDelflg().isEmpty()){
                    statement.setString(stIndex++, form09.getDelflg());
                }

                if (form09 != null && form09.getCreat() != null && form09.getSearchRecordDate() != null){
                    statement.setDate(stIndex++, form09.getCreat());
                    statement.setDate(stIndex++, form09.getSearchRecordDate());
                }

                ResultSet resultSet = statement.executeQuery();

                form09EntitieList = new ArrayList<>();

                IComboService comboService = new ComboServiceImpl(boneCP);
                List<BasicDBObject> cmbType = comboService.getComboVals("MAL", "MN");
                List<BasicDBObject> shgazarType = comboService.getComboVals("SHGAZ", "MN");
                List<BasicDBObject> shargaType = comboService.getComboVals("SHARGA", "MN");
                List<BasicDBObject> uvchinNer = comboService.getComboVals("UVCH", "MN");

                int index=1;
                while (resultSet.next()) {
                    Form09HavsraltEntity entity = new Form09HavsraltEntity();

                    entity.setId(resultSet.getLong("id"));
                    entity.setCitycode(resultSet.getString("citycode"));
                    entity.setSumcode(resultSet.getString("sumcode"));
                    entity.setBagcode(resultSet.getString("bagcode"));
                    entity.setGazarner(resultSet.getString("gazarner"));
                    entity.setUrtrag(resultSet.getString("urtrag"));
                    entity.setOrgorog(resultSet.getString("orgorog"));
                    entity.setGolomttoo(resultSet.getInt("golomttoo"));
                    entity.setOvchin_ner(resultSet.getString("ovchin_ner"));
                    entity.setMal_turul(resultSet.getString("mal_turul"));
                    entity.setUvchilson(resultSet.getInt("uvchilson"));
                    entity.setUhsen(resultSet.getInt("uhsen"));
                    entity.setEdgersen(resultSet.getInt("edgersen"));
                    entity.setDelflg(resultSet.getString("delflg"));
                    entity.setModAt(resultSet.getDate("mod_at"));
                    entity.setModby(resultSet.getString("mod_by"));
                    entity.setCreat(resultSet.getDate("cre_at"));
                    entity.setCreby(resultSet.getString("cre_by"));
                    entity.setRecordDate(resultSet.getDate("record_date"));

                    entity.put("malturulner", getTypeNer(cmbType, resultSet.getString("mal_turul")));
                    entity.put("uvchinner", getTypeNer(uvchinNer, resultSet.getString("ovchin_ner")));

                    entity.setCityName(resultSet.getString("cityname"));
                    entity.setSumName(resultSet.getString("sumname"));
                    entity.setBagName(resultSet.getString("horooname"));
                    entity.put("index",index++);
                    form09EntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return form09EntitieList;
    }

    /**
     * Get name
     *
     * @param cmbType
     * @param mal_turul_mn
     * @return
     */
    private String getTypeNer(List<BasicDBObject> cmbType, String mal_turul_mn) {
        for (BasicDBObject obj :
                cmbType) {
            if (obj.get("id") != null && mal_turul_mn != null && obj.getString("id").equalsIgnoreCase(mal_turul_mn)) {
                return obj.getString("name");
            }
        }

        return "";
    }

    @Override
    public ErrorEntity updateData(Form09HavsraltEntity form09) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (form09 == null) new ErrorEntity(ErrorType.INFO, Long.valueOf(1059));

        query = "UPDATE `hospital`.`hform09_hav` " +
                "SET     ";

        if (form09.getCitycode() != null && !form09.getCitycode().isEmpty()){
            query += " `hform09_hav`.citycode = ?, ";
        }

        if (form09.getSumcode() != null && !form09.getSumcode().isEmpty()){
            query += " `hform09_hav`.sumcode = ?, ";
        }

        if (form09.getBagcode() != null && !form09.getBagcode().toString().isEmpty()){
            query += " `hform09_hav`.bagcode = ?, ";
        }
        if (form09.getGazarner() != null && !form09.getGazarner().toString().isEmpty()){
            query += " `hform09_hav`.gazarner = ?, ";
        }
        if (form09.getUrtrag() != null && !form09.getUrtrag().toString().isEmpty()){
            query += " `hform09_hav`.urtrag = ?, ";
        }
        if (form09.getOrgorog() != null && !form09.getOrgorog().toString().isEmpty()){
            query += " `hform09_hav`.orgorog = ?, ";
        }

        if (form09.getGolomttoo() != 0){
            query += " `hform09_hav`.golomttoo = ?, ";
        }
        if (form09.getOvchin_ner() != null && !form09.getOvchin_ner().toString().isEmpty()){
            query += " `hform09_hav`.ovchin_ner = ?, ";
        }

        if (form09.getMal_turul() != null && !form09.getMal_turul().isEmpty()){
            query += " `hform09_hav`.mal_turul = ?, ";
        }
        if (form09.getUvchilson() != 0){
            query += " `hform09_hav`.uvchilson = ?, ";
        }

        if (form09.getUhsen() != 0){
            query += " `hform09_hav`.uhsen = ?, ";
        }
        if (form09.getEdgersen() != 0){
            query += " `hform09_hav`.edgersen = ?, ";
        }

        if (form09 != null && form09.getRecordDate() != null){
            query += " `hform09_hav`.`record_date` = ?, ";
        }

        if (form09 != null && form09.getDelflg() != null && !form09.getDelflg().isEmpty()){
            query += " `hform09_hav`.`delflg` = ?, ";
        }

        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";
        query += " WHERE  ID   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (form09.getCitycode() != null && !form09.getCitycode().isEmpty()){
                    statement.setString(stIndex++, form09.getCitycode());
                }

                if (form09.getSumcode() != null && !form09.getSumcode().isEmpty()){
                    statement.setString(stIndex++, form09.getSumcode());
                }

                if (form09.getBagcode() != null && !form09.getBagcode().toString().isEmpty()){
                    statement.setString(stIndex++, form09.getBagcode());
                }
                if (form09.getGazarner() != null && !form09.getGazarner().toString().isEmpty()){
                    statement.setString(stIndex++, form09.getGazarner());
                }
                if (form09.getUrtrag() != null && !form09.getUrtrag().toString().isEmpty()){
                    statement.setString(stIndex++, form09.getUrtrag());
                }
                if (form09.getOrgorog() != null && !form09.getOrgorog().toString().isEmpty()){
                    statement.setString(stIndex++, form09.getOrgorog());
                }

                if (form09.getGolomttoo() != 0){
                    statement.setInt(stIndex++, form09.getGolomttoo());
                }
                if (form09.getOvchin_ner() != null && !form09.getOvchin_ner().toString().isEmpty()){
                    statement.setString(stIndex++, form09.getOvchin_ner());
                }

                if (form09.getMal_turul() != null && !form09.getMal_turul().isEmpty()){
                    statement.setString(stIndex++, form09.getMal_turul());
                }
                if (form09.getUvchilson() != 0){
                    statement.setInt(stIndex++, form09.getUvchilson());
                }

                if (form09.getUhsen() != 0){
                    statement.setInt(stIndex++, form09.getUhsen());
                }
                if (form09.getEdgersen() != 0){
                    statement.setInt(stIndex++, form09.getEdgersen());
                }

                if (form09 != null && form09.getRecordDate() != null){
                    statement.setDate(stIndex++, form09.getRecordDate());
                }

                if (form09 != null && form09.getDelflg() != null && !form09.getDelflg().isEmpty()){
                    statement.setString(stIndex++, form09.getDelflg());
                }

                statement.setDate(stIndex++, form09.getModAt());
                statement.setString(stIndex++, form09.getModby());
                statement.setLong(stIndex++, form09.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    return new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }


        return new ErrorEntity(ErrorType.ERROR, Long.valueOf(1061));
    }

    @Override
    public List<BasicDBObject> getPieChartData(Form09HavsraltEntity entity) throws SQLException {
        int stIndex = 1;
        List<BasicDBObject> formEntitieList = null;

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }


        String query_sel = "SELECT `ovchin_ner`, COUNT(`id`) AS cnt, `hform09_hav`.`citycode`, `hform09_hav`.`sumcode`  " +
                "FROM `hospital`.`hform09_hav`  " +
                "WHERE   ";

        if (entity != null && !entity.getCitycode().isEmpty()){
            query_sel += " `hform09_hav`.`citycode` = ? AND ";
        }
        if (entity != null && !entity.getSumcode().isEmpty()){
            query_sel += " `hform09_hav`.`sumcode` = ? AND ";
        }
        if (entity != null && !entity.getDelflg().isEmpty()){
            query_sel += " `hform09_hav`.`delflg` = ? AND ";
        }
        if (entity != null && !entity.getOvchin_ner().isEmpty()){
            query_sel += " `hform09_hav`.`ovchin_ner` = ? AND ";
        }
        if (entity != null && !entity.getCreby().isEmpty()){
            query_sel += " `hform09_hav`.`cre_by` = ? AND ";
        }
        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null){
            query_sel += " (`record_date` BETWEEN ? AND ?) AND ";
        }

        query_sel += " `id` > 0  GROUP BY `hform09_hav`.`ovchin_ner`";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                int stindex = 1;

                if (entity != null && !entity.getCitycode().isEmpty()){
                    statement.setString(stindex++, entity.getCitycode());
                }
                if (entity != null && !entity.getSumcode().isEmpty()){
                    statement.setString(stindex++, entity.getSumcode());
                }
                if (entity != null && !entity.getDelflg().isEmpty()){
                    statement.setString(stindex++, entity.getDelflg());
                }
                if (entity != null && !entity.getOvchin_ner().isEmpty()){
                    statement.setString(stindex++, entity.getOvchin_ner());
                }
                if (entity != null && !entity.getCreby().isEmpty()){
                    statement.setString(stindex++, entity.getCreby());
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null){
                    statement.setDate(stindex++, entity.getRecordDate());
                    statement.setDate(stindex++, entity.getSearchRecordDate());
                }

                ResultSet resultSet = statement.executeQuery();

                formEntitieList = new ArrayList<>();

                IComboService comboService = new ComboServiceImpl(boneCP);
                List<BasicDBObject> cmbType = comboService.getComboVals("MAL", "MN");
                List<BasicDBObject> cmbTypeUvch = comboService.getComboVals("UVCH", "MN");

                while (resultSet.next()) {
                    BasicDBObject obj = new BasicDBObject();

                    obj.put("malturulner", getMalTurulNer(cmbTypeUvch, resultSet.getString("ovchin_ner")));
                    obj.put("malturul", resultSet.getString("ovchin_ner"));
                    obj.put("count", resultSet.getString("cnt"));
                    obj.put("aimag", resultSet.getString("citycode"));
                    obj.put("sum", resultSet.getString("sumcode"));

                    formEntitieList.add(obj);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return formEntitieList;
    }//

    private String getMalTurulNer(List<BasicDBObject> cmbType, String mal_turul_mn) {
        for (BasicDBObject obj :
                cmbType) {
            if (obj.get("id") != null && mal_turul_mn != null && obj.getString("id").equalsIgnoreCase(mal_turul_mn)) {
                return obj.getString("name");
            }
        }

        return "";
    }
    /**
     * Get name
     *
     * @param cmbType
     * @param tarilga_ner_mn
     * @return
     */
    private String getTarilgaNer(List<BasicDBObject> cmbType, String tarilga_ner_mn) {
        for (BasicDBObject obj :
                cmbType) {
            if (obj.get("name") != null && tarilga_ner_mn != null && obj.getString("name").equalsIgnoreCase(tarilga_ner_mn)) {
                return obj.getString("name");
            }
        }

        return "";
    }



    @Override
    public ErrorEntity exportReport(String file,Form09HavsraltEntity form09) throws SQLException {
        ErrorEntity errorEntity = null;
        String fileName = "hform09havsralt";
        try (Connection connection = boneCP.getConnection()){
            String JRXML_PATH = Config.getJrxmlPath();
            String REPORT_PATH = Config.getReportPath();

            try {
                Map parameters = new HashMap();

                String reportTitle = "МАЛ, АМЬТДЫН ПАРАЗИТТАХ ӨВЧНИЙ $$YEAR$$ ОНЫ $$MONTH$$ -Р САР, ЖИЛИЙН ЭЦСИЙН МЭДЭЭ",
                        printDate = "$$YEAR$$ оны $$MONTH$$ сарын $$DAY$$";

                Calendar now = Calendar.getInstance();

                int year = now.get(Calendar.YEAR);
                int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
                int day = now.get(Calendar.DAY_OF_MONTH);

                printDate = printDate.replace("$$YEAR$$", String.valueOf(year));
                printDate = printDate.replace("$$MONTH$$", String.valueOf(month));
                printDate = printDate.replace("$$DAY$$", String.valueOf(day));


                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(new Date(form09.getRecordDate().getTime()));

                reportTitle = reportTitle.replace("$$YEAR$$", String.valueOf(calendar1.get(Calendar.YEAR)));

                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(new Date(form09.getSearchRecordDate().getTime()));

                reportTitle = reportTitle.replace("$$MONTH$$", String.valueOf(calendar2.get(Calendar.MONTH)));
                System.out.println(form09.getMal_turul());
                parameters.put("malturul", (form09.getMal_turul() != null && !form09.getMal_turul().isEmpty()) ? form09.getMal_turul() : null);
                parameters.put("start_date", form09.getRecordDate());
                parameters.put("end_date", form09.getSearchRecordDate());
                parameters.put("citycode", (form09.getCitycode() != null && !form09.getCitycode().isEmpty()) ? form09.getCitycode() : null);
                parameters.put("sumcode", (form09.getSumcode() != null && !form09.getSumcode().isEmpty()) ? form09.getSumcode() : null);
                parameters.put("uvchner", (form09.getOvchin_ner() != null && !form09.getOvchin_ner().isEmpty()) ? form09.getOvchin_ner() : null);
                CityServiceImpl cityService = new CityServiceImpl(boneCP);
                City tmpCity = new City();
                tmpCity.setCode(form09.getCitycode());
                List<City> clist = cityService.selectAll(tmpCity);
                if (clist != null && clist.size() == 1) {
                    parameters.put("aimagner", clist.get(0).getCityname());
                } else {
                    parameters.put("aimagner", (form09.getCityName() == null || form09.getCityName().isEmpty()) ? "Нийт" : form09.getCityName());
                }
                ISumService sumService = new SumServiceImpl(boneCP);
                Sum tmpSum = new Sum();
                tmpSum.setCitycode(form09.getCitycode());
                tmpSum.setSumcode(form09.getSumcode());
                List<Sum> sumList = sumService.selectAll(tmpSum);

                if (sumList != null && sumList.size() == 1){
                    parameters.put("sumner", sumList.get(0).getSumname());
                }
                else{
                    parameters.put("sumner", (form09.getSumName() == null) ? "" : form09.getSumName());
                }

                parameters.put("printdate", printDate);
                parameters.put("reporttitle", reportTitle);
                parameters.put("creby", (form09.getCreby() != null && !form09.getCreby().isEmpty()) ? form09.getCreby() : null);

                PdfExcelUtil.generateReport(file, JRXML_PATH, REPORT_PATH, fileName, parameters, connection);

                System.out.println("File Generated");
            }catch (Exception ex){
                ex.printStackTrace();
            }
            finally {
                if (connection != null)connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1001));
            if(file.equals("pdf")){
                errorEntity.setErrText(fileName + ".pdf");
            }else if(file.equals("xls")){
                errorEntity.setErrText(fileName + ".xls");
            }
        }
        return errorEntity;
    }

    @Override
    public BasicDBObject getColumnChartData(Form09HavsraltEntity entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();
        List<String> categories = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        //dateFormat.parse(dv).getTime()
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }

        try {
            IComboService comboService = new ComboServiceImpl(boneCP);
            List<BasicDBObject> cmbType = comboService.getComboNames("UVCH", "MN");
            //Saraar n haruulah
            int month = calendar1.get(Calendar.MONTH) + 1;
            int year1 = calendar1.get(Calendar.YEAR);
            int year2 = calendar2.get(Calendar.YEAR);

            try (Connection connection = boneCP.getConnection()) {
                String query_sel = "SELECT `ovchin_ner`, COUNT(`id`) AS cnt, `hform09_hav`.`citycode`, `hform09_hav`.`sumcode`  " +
                        "FROM `hospital`.`hform09_hav`  " +
                        "WHERE  ";

                if (entity != null && !entity.getCitycode().isEmpty()){
                    query_sel += " `hform09_hav`.`citycode` = ? AND ";
                }
                if (entity != null && !entity.getSumcode().isEmpty()){
                    query_sel += " `hform09_hav`.`sumcode` = ? AND ";
                }
                if (entity != null && !entity.getDelflg().isEmpty()){
                    query_sel += " `hform09_hav`.`delflg` = ? AND ";
                }
                if (entity != null && !entity.getOvchin_ner().isEmpty()){
                    query_sel += " `hform09_hav`.`ovchin_ner` = ? AND ";
                }
                if (entity != null && !entity.getCreby().isEmpty()){
                    query_sel += " `hform09_hav`.`cre_by` = ? AND ";
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null){
                    query_sel += " (`record_date` BETWEEN ? AND ?) AND ";
                }

                query_sel += " `id` > 0  GROUP BY `hform09_hav`.`ovchin_ner` ORDER BY `hform09_hav`.`ovchin_ner`";

                do {
                    //do process
                    categories.add(year1 + "-" + month);

                    try {
                        PreparedStatement statement = connection.prepareStatement(query_sel);

                        java.sql.Date stdate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-01 00:00:00").getTime());

                        java.sql.Date enddate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-31 00:00:00").getTime());

                        if (month == 12 && year1 < year2) {
                            month = 1;
                            year1++;
                        } else {
                            month++;
                        }

                        int stindex = 1;

                        if (entity != null && !entity.getCitycode().isEmpty()){
                            statement.setString(stindex++, entity.getCitycode());
                        }
                        if (entity != null && !entity.getSumcode().isEmpty()){
                            statement.setString(stindex++, entity.getSumcode());
                        }
                        if (entity != null && !entity.getDelflg().isEmpty()){
                            statement.setString(stindex++, entity.getDelflg());
                        }
                        if (entity != null && !entity.getOvchin_ner().isEmpty()){
                            statement.setString(stindex++, entity.getOvchin_ner());
                        }
                        if (entity != null && !entity.getCreby().isEmpty()){
                            statement.setString(stindex++, entity.getCreby());
                        }
                        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null){
                            statement.setDate(stindex++, stdate);
                            statement.setDate(stindex++, enddate);
                        }


                        ResultSet resultSet = statement.executeQuery();

                        HashMap<String, String> hashMap = new HashMap<>();

                        while (resultSet.next()) {
                            String uvchinner = resultSet.getString("ovchin_ner");
                            String count = resultSet.getString("cnt");

                            hashMap.put(uvchinner, count);
                        }//end while

                        List<Double> list = null;

                        for (int i = 0; i < cmbType.size(); i++) {
                            //rowdata[i] = (hashMap.get(cmbType.get(i - 1).getString("id")) == null) ? 0 : Integer.parseInt(hashMap.get(cmbType.get(i - 1).getString("id")));
                            BasicDBObject obj = cmbType.get(i);

                            if (obj.get("data") == null){
                                //new record
                                list = new ArrayList<>();
                                String val = hashMap.get(obj.getString("id"));
                                if (val == null) list.add(0.0);
                                else if (val.isEmpty()) list.add(0.0);
                                else list.add(Double.parseDouble(val));
                                obj.put("data", list);
                            }else{
                                //already exist
                                list = (List<Double>) obj.get("data");
                                String val = hashMap.get(obj.getString("id"));
                                if (val == null) list.add(0.0);
                                else if (val.isEmpty()) list.add(0.0);
                                else list.add(Double.parseDouble(val));
                            }
                        }

                        if (statement != null) statement.close();
                        if (resultSet != null) resultSet.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();

                        break;
                    }

                    ///
                } while (month <= calendar2.get(Calendar.MONTH) + 1);

                basicDBObject.put("categories", categories);
                basicDBObject.put("series", cmbType);

                connection.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }
    @Override
    public BasicDBObject getPieHighChartData(Form09HavsraltEntity entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();

        String query_sel = "SELECT ovchin_ner, COUNT(id) as cnt, citycode, sumcode FROM hospital.hform09_hav  WHERE";
        if (entity != null && !entity.getOvchin_ner().isEmpty()) {
            query_sel = "SELECT `mal_turul`, SUM(`uvchilson`) AS cnt, `hform09_hav`.`citycode`  " +
                    "FROM `hospital`.`hform09_hav`  " +
                    "WHERE  ";
        }
        if (entity != null && !entity.getCitycode().isEmpty()) {
            query_sel += " `hform09_hav`.`citycode` = ? AND ";
        }
        if (entity != null && !entity.getDelflg().isEmpty()) {
            query_sel += " `hform09_hav`.`delflg` = ? AND ";
        }
        if (entity != null && !entity.getOvchin_ner().isEmpty()) {
            query_sel += " `hform09_hav`.`ovchin_ner` = ? AND ";
        }
        if (entity != null && !entity.getMal_turul().isEmpty()) {
            query_sel += " `hform09_hav`.`mal_turul` = ? AND ";
        }
        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
            query_sel += " (`record_date` BETWEEN ? AND ?) AND ";
        }


        if (entity != null && !entity.getOvchin_ner().isEmpty()) {
            query_sel += " `hform09_hav`.`id` > 0  GROUP BY `hform09_hav`.`mal_turul` ORDER BY `hform09_hav`.`mal_turul`";
        }
        else{
            query_sel += " `hform09_hav`.`id` > 0  GROUP BY `hform09_hav`.`ovchin_ner` ORDER BY `hform09_hav`.`ovchin_ner`";
        }
        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                int stindex = 1;

                if (entity != null && !entity.getCitycode().isEmpty()) {
                    statement.setString(stindex++, entity.getCitycode());
                }
                if (entity != null && !entity.getDelflg().isEmpty()) {
                    statement.setString(stindex++, entity.getDelflg());
                }
                if (entity != null && !entity.getOvchin_ner().isEmpty()) {
                    statement.setString(stindex++, entity.getOvchin_ner());
                }
                if (entity != null && !entity.getMal_turul().isEmpty()) {
                    statement.setString(stindex++, entity.getMal_turul());
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                    statement.setDate(stindex++, entity.getRecordDate());
                    statement.setDate(stindex++, entity.getSearchRecordDate());
                }

                ResultSet resultSet = statement.executeQuery();

                IComboService comboService = new ComboServiceImpl(boneCP);
                List<BasicDBObject> cmbType = comboService.getComboNames("PARUVCH", "MN");
                if (entity != null && !entity.getOvchin_ner().isEmpty()) {
                    cmbType = comboService.getComboNames("MAL", "MN");
                }
                List<BasicDBObject> list = new ArrayList<>();

                while (resultSet.next()) {
                    BasicDBObject entity2 = new BasicDBObject();

                    if (entity != null && !entity.getOvchin_ner().isEmpty()) {
                        entity2.put("name", getMalTurulNer(cmbType, resultSet.getString("mal_turul")));
                    }else{
                        entity2.put("name", getTarilgaNer(cmbType, resultSet.getString("ovchin_ner")));
                    }
                    entity2.put("y", resultSet.getDouble("cnt"));
                    list.add(entity2);
                }

                basicDBObject.put("data", list);

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }

    @Override
    public BasicDBObject getColHighChartData(Form09HavsraltEntity entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();
        List<String> categories = new ArrayList<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        //dateFormat.parse(dv).getTime()
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }

        try {
            IComboService comboService = new ComboServiceImpl(boneCP);

            List<BasicDBObject> cmbType = comboService.getComboNames("PARUVCH", "MN");
            if (entity != null && !entity.getOvchin_ner().isEmpty()) {
                cmbType = comboService.getComboNames("MAL", "MN");
            }
            //Saraar n haruulah
            int month = calendar1.get(Calendar.MONTH) + 1;
            int year1 = calendar1.get(Calendar.YEAR);
            int year2 = calendar2.get(Calendar.YEAR);

            try (Connection connection = boneCP.getConnection()) {
                String query_sel = "SELECT ovchin_ner, COUNT(id) as cnt, citycode, sumcode FROM hospital.hform09_hav  WHERE";
                if (entity != null && !entity.getOvchin_ner().isEmpty()) {
                    query_sel = "SELECT `mal_turul`, SUM(`uvchilson`) AS cnt, `hform09_hav`.`citycode`  " +
                            "FROM `hospital`.`hform09_hav`  " +
                            "WHERE  ";
                }
                if (entity != null && !entity.getCitycode().isEmpty()) {
                    query_sel += " `hform09_hav`.`citycode` = ? AND ";
                }
                if (entity != null && !entity.getDelflg().isEmpty()) {
                    query_sel += " `hform09_hav`.`delflg` = ? AND ";
                }
                if (entity != null && !entity.getOvchin_ner().isEmpty()) {
                    query_sel += " `hform09_hav`.`ovchin_ner` = ? AND ";
                }
                if (entity != null && !entity.getMal_turul().isEmpty()) {
                    query_sel += " `hform09_hav`.`mal_turul` = ? AND ";
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                    query_sel += " (`record_date` BETWEEN ? AND ?) AND ";
                }


                if (entity != null && !entity.getOvchin_ner().isEmpty()) {
                    query_sel += " `hform09_hav`.`id` > 0  GROUP BY `hform09_hav`.`mal_turul` ORDER BY `hform09_hav`.`mal_turul`";
                }
                else{
                    query_sel += " `hform09_hav`.`id` > 0  GROUP BY `hform09_hav`.`ovchin_ner` ORDER BY `hform09_hav`.`ovchin_ner`";
                }
                do {
                    //do process
                    categories.add(year1 + "-" + month);

                    try {
                        PreparedStatement statement = connection.prepareStatement(query_sel);

                        java.sql.Date stdate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-01 00:00:00").getTime());

                        java.sql.Date enddate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-31 00:00:00").getTime());

                        if (month == 12 && year1 < year2) {
                            month = 1;
                            year1++;
                        } else {
                            month++;
                        }

                        int stindex = 1;

                        if (entity != null && !entity.getCitycode().isEmpty()) {
                            statement.setString(stindex++, entity.getCitycode());
                        }
                        if (entity != null && !entity.getDelflg().isEmpty()) {
                            statement.setString(stindex++, entity.getDelflg());
                        }
                        if (entity != null && !entity.getOvchin_ner().isEmpty()) {
                            statement.setString(stindex++, entity.getOvchin_ner());
                        }
                        if (entity != null && !entity.getMal_turul().isEmpty()) {
                            statement.setString(stindex++, entity.getMal_turul());
                        }
                        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                            statement.setDate(stindex++, stdate);
                            statement.setDate(stindex++, enddate);
                        }

                        ResultSet resultSet = statement.executeQuery();

                        HashMap<String, String> hashMap = new HashMap<>();

                        while (resultSet.next()) {
                            String uvchinner = "";
                            if (entity != null && !entity.getOvchin_ner().isEmpty()) {
                                uvchinner = resultSet.getString("mal_turul");
                            }else{
                                uvchinner = resultSet.getString("ovchin_ner");
                            }

                            String count = resultSet.getString("cnt");
                            hashMap.put(uvchinner, count);
                        }//end while

                        List<Double> list = null;

                        if (entity != null && !entity.getMal_turul().isEmpty()) {
                            for (int i = 0; i < cmbType.size(); i++) {
                                BasicDBObject obj = cmbType.get(i);

                                if (obj.get("data") == null){
                                    //new record
                                    list = new ArrayList<>();
                                    String val = hashMap.get(obj.getString("name"));
                                    if (val == null) list.add(0.0);
                                    else if (val.isEmpty()) list.add(0.0);
                                    else list.add(Double.parseDouble(val));
                                    obj.put("data", list);
                                }else{
                                    //already exist

                                    list = (List<Double>) obj.get("data");
                                    String val = hashMap.get(obj.getString("name"));
                                    if (val == null) list.add(0.0);
                                    else if (val.isEmpty()) list.add(0.0);
                                    else list.add(Double.parseDouble(val));
                                }

                            }
                        }else if (entity != null && entity.getMal_turul().isEmpty() && entity.getOvchin_ner().isEmpty()){
                            for (int i = 0; i < cmbType.size(); i++) {
                                BasicDBObject obj = cmbType.get(i);
                                if (obj.get("data") == null){
                                    //new record
                                    list = new ArrayList<>();
                                    String val = hashMap.get(obj.getString("name"));
                                    if (val == null) list.add(0.0);
                                    else if (val.isEmpty()) list.add(0.0);
                                    else list.add(Double.parseDouble(val));
                                    obj.put("data", list);
                                }else{
                                    //already exist
                                    list = (List<Double>) obj.get("data");
                                    String val = hashMap.get(obj.getString("name"));
                                    if (val == null) list.add(0.0);
                                    else if (val.isEmpty()) list.add(0.0);
                                    else list.add(Double.parseDouble(val));
                                }

                            }
                        } else {
                            for (int i = 0; i < cmbType.size(); i++) {
                                BasicDBObject obj = cmbType.get(i);
                                System.out.println(obj);

                                if (obj.get("data") == null){
                                    //new record
                                    list = new ArrayList<>();
                                    String val = hashMap.get(obj.getString("id"));
                                    if (val == null) list.add(0.0);
                                    else if (val.isEmpty()) list.add(0.0);
                                    else list.add(Double.parseDouble(val));
                                    obj.put("data", list);
                                }else{
                                    //already exist
                                    list = (List<Double>) obj.get("data");
                                    String val = hashMap.get(obj.getString("id"));
                                    if (val == null) list.add(0.0);
                                    else if (val.isEmpty()) list.add(0.0);
                                    else list.add(Double.parseDouble(val));
                                }

                            }
                        }

                        if (statement != null) statement.close();
                        if (resultSet != null) resultSet.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();

                        break;
                    }

                    ///
                } while ((month <= calendar2.get(Calendar.MONTH) + 1 && year1 <= year2) || year1 < year2);

                basicDBObject.put("categories", categories);
                basicDBObject.put("series", cmbType);

                connection.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }
}
