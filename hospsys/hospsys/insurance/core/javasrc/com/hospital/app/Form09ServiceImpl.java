package com.hospital.app;


import com.Config;
import com.enumclass.ErrorType;
import com.jolbox.bonecp.BoneCP;
import com.model.hos.City;
import com.model.hos.ErrorEntity;
import com.model.hos.Form09Entity;
import com.model.hos.Sum;
import com.mongodb.BasicDBObject;
import com.rpc.PdfExcelUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public class Form09ServiceImpl implements IForm09Service{
    /**
     * Objects
     */

    private BoneCP boneCP;
    private ErrorEntity errObj;


    public Form09ServiceImpl(BoneCP boneCP) {
        this.boneCP = boneCP;
    }

    @Override
    public ErrorEntity insertNewData(Form09Entity form09) throws SQLException {
        PreparedStatement statement = null;

        if (form09 == null){
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        String query_login = "INSERT INTO `hospital`.`hform09` " +
                "(`tarilga_ner_mn`, " +
                "`tarilga_ner_en`, " +
                "`ner_mn`, " +
                "`ner_en`, " +
                "`mal_turul_mn`, " +
                "`mal_turul_en`, " +
                "`tuluvluguu_mn`, " +
                "`tuluvluguu_en`, " +
                "`guitsetgel_mn`, " +
                "`guitsetgel_en`, " +
                "`huwi_mn`, " +
                "`huwi_en`, " +
                "`olgosonTun`, " +
                "`hereglesenTun`, " +
                "`ustgasanTun`, " +
                "`nuutsulsenTun`, " +
                "`delflg`, " +
                "`actflg`, " +
                "`mod_at`, " +
                "`mod_by`, " +
                "`cre_at`, " +
                "`recorddate`, " +
                "`cre_by`," +
                "`zartsuulsan_hemjee_mn`," +
                "`zartsuulsan_hemjee_en`, " +
                "`aimag`," +
                "`sum`," +
                "`aimag_en`," +
                "`sum_en`)" +
                "VALUES " +
                "(? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )";

        try (Connection connection = boneCP.getConnection()) {
            try{
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, form09.getTarilgaNer());
                statement.setString(2, form09.getTarilgaNer_en());
                statement.setString(3, form09.getNer());
                statement.setString(4, form09.getNer_en());
                statement.setString(5, form09.getMalTurul());
                statement.setString(6, form09.getMalTurul_en());
                statement.setString(7, form09.getTolovlogoo());
                statement.setString(8, form09.getTolovlogoo_en());
                statement.setString(9, form09.getGuitsetgel());
                statement.setString(10, form09.getGuitsetgel_en());
                statement.setString(11, form09.getHuvi());
                statement.setString(12, form09.getHuvi_en());
                statement.setLong(13, form09.getOLGOSON_TUN());
                statement.setLong(14, form09.getHEREGLESEN_TUN());
                statement.setLong(15, form09.getUSTGASAN_TUN());
                statement.setLong(16, form09.getNUUTSULSEN_TUN());
                statement.setString(17, form09.getDELFLG());
                statement.setString(18, form09.getACTFLG());
                statement.setDate(19, form09.getMODAT());
                statement.setString(20, form09.getMODBY());
                statement.setDate(21, form09.getCreat());
                statement.setDate(22, form09.getRecordDate());
                statement.setString(23, form09.getCREBY());
                statement.setString(24, form09.getZartsuulsanHemjee());
                statement.setString(25, form09.getZartsuulsanHemjee_en());
                statement.setString(26, form09.getAimag());
                statement.setString(27, form09.getSum());
                statement.setString(28, form09.getAimag_en());
                statement.setString(29, form09.getSum_en());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1){
                    connection.commit();

                    errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));
                    errObj.setErrText("Бичлэг амжилттай хадгалагдлаа.");
                    errObj.setErrDesc("Бичлэг амжилттай хадгалагдлаа.");
                    return errObj;
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }finally {
                if (statement != null)statement.close();
                connection.close();
            }

        }catch (Exception ex){

        }

        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1057));
        errObj.setErrText("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");
        errObj.setErrDesc("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");

        return errObj;
    }

    @Override
    public List<Form09Entity> getListData(Form09Entity form09) throws SQLException {
        int stIndex = 1;
        List<Form09Entity> form09EntitieList = null;

        String query = "SELECT `hform09`.`id`, " +
                "    `hform09`.`tarilga_ner_mn`, " +
                "    `hform09`.`tarilga_ner_en`, " +
                "    `hform09`.`mur_dugaar_mn`, " +
                "    `hform09`.`mur_dugaar_en`, " +
                "    `hform09`.`ner_mn`, " +
                "    `hform09`.`ner_en`, " +
                "    `hform09`.`zartsuulsan_hemjee_mn`, " +
                "    `hform09`.`zartsuulsan_hemjee_en`, " +
                "    `hform09`.`mal_turul_mn`, " +
                "    `hform09`.`mal_turul_en`, " +
                "    `hform09`.`tuluvluguu_mn`, " +
                "    `hform09`.`tuluvluguu_en`, " +
                "    `hform09`.`guitsetgel_mn`, " +
                "    `hform09`.`guitsetgel_en`, " +
                "    `hform09`.`huwi_mn`, " +
                "    `hform09`.`huwi_en`, " +
                "    `hform09`.`olgosonTun`, " +
                "    `hform09`.`hereglesenTun`, " +
                "    `hform09`.`ustgasanTun`, " +
                "    `hform09`.`nuutsulsenTun`, " +
                "    `hform09`.`delflg`, " +
                "    `hform09`.`actflg`, " +
                "    `hform09`.`mod_at`, " +
                "    `hform09`.`mod_by`, " +
                "    `hform09`.`cre_at`, " +
                "    `hform09`.`recorddate`, " +
                "    `hform09`.`cre_by`, " +
                "    `hform09`.`aimag`, " +
                "    `hform09`.`sum`, " +
                "    `hform09`.`aimag_en`, " +
                "    `hform09`.`sum_en`, " +
                "    c.cityname, c.cityname_en, " +
                "    `h_sum`.sumname, `h_sum`.sumname_en, " +
                "    `h_sum`.latitude as sum_latitude, `h_sum`.longitude as sum_longitude  " +
                "FROM `hospital`.`hform09` INNER JOIN `h_city` AS c " +
                "ON c.code = `hform09`.aimag " +
                "INNER JOIN `h_sum` " +
                "ON `h_sum`.sumcode = `hform09`.sum " +
                "WHERE ";

        if (form09.getTarilgaNer() != null && !form09.getTarilgaNer().isEmpty()){
            query += " tarilga_ner_mn = ? AND ";
        }

        if (form09.getTarilgaNer_en() != null && !form09.getTarilgaNer_en().isEmpty()){
            query += " tarilga_ner_en = ? AND ";
        }

        /* NER */
        if (form09.getNer() != null && !form09.getNer().toString().isEmpty()){
            query += " ner_mn = ? AND ";
        }
        if (form09.getNer_en() != null && !form09.getNer_en().toString().isEmpty()){
            query += " ner_en = ? AND ";
        }
        /* Zartsuulsan hemjee */
        if (form09.getZartsuulsanHemjee() != null && !form09.getZartsuulsanHemjee().toString().isEmpty()){
            query += " zartsuulsan_hemjee_mn = ? AND ";
        }
        if (form09.getZartsuulsanHemjee_en() != null && !form09.getZartsuulsanHemjee_en().toString().isEmpty()){
            query += " zartsuulsan_hemjee_en = ? AND ";
        }

        /* MAL TURUL */
        if (form09.getMalTurul() != null && !form09.getMalTurul().isEmpty()){
            query += " mal_turul_mn = ? AND ";
        }
        if (form09.getMalTurul_en() != null && !form09.getMalTurul_en().toString().isEmpty()){
            query += " mal_turul_en = ? AND ";
        }

        /* Tuluvlugu */
        if (form09.getTolovlogoo() != null && !form09.getTolovlogoo().isEmpty()){
            query += " tuluvluguu_mn = ? AND ";
        }
        if (form09.getTolovlogoo_en() != null && !form09.getTolovlogoo_en().toString().isEmpty()){
            query += " tuluvluguu_en = ? AND ";
        }

        /* Guitsetgel */
        if (form09.getGuitsetgel() != null && !form09.getGuitsetgel().isEmpty()){
            query += " guitsetgel_mn = ? AND ";
        }
        if (form09.getGuitsetgel_en() != null && !form09.getGuitsetgel_en().toString().isEmpty()){
            query += " guitsetgel_en = ? AND ";
        }

        /* Huwi */
        if (form09.getHuvi() != null && !form09.getHuvi().isEmpty()){
            query += " huwi_mn = ? AND  ";
        }
        if (form09.getHuvi_en() != null && !form09.getHuvi_en().toString().isEmpty()){
            query += " huwi_en = ? AND ";
        }

        if (form09.getOLGOSON_TUN() != null && !form09.getOLGOSON_TUN().toString().isEmpty() && form09.getOLGOSON_TUN().longValue() != 0) {
            query += " olgosonTun = ? AND ";
        }
        if (form09.getHEREGLESEN_TUN() != null && !form09.getHEREGLESEN_TUN().toString().isEmpty() && form09.getHEREGLESEN_TUN().longValue() != 0) {
            query += " hereglesenTun = ? AND ";
        }
        if (form09.getUSTGASAN_TUN() != null && !form09.getUSTGASAN_TUN().toString().isEmpty() && form09.getUSTGASAN_TUN().longValue() != 0) {
            query += " ustgasanTun = ? AND ";
        }
        if (form09.getNUUTSULSEN_TUN() != null && !form09.getNUUTSULSEN_TUN().toString().isEmpty() && form09.getNUUTSULSEN_TUN().longValue() != 0) {
            query += " nuutsulsenTun = ? AND ";
        }

        if (form09 != null && form09.getAimag() != null && !form09.getAimag().isEmpty()){
            query += " `aimag` = ? AND ";
        }
        if (form09 != null && form09.getSum() != null && !form09.getSum().isEmpty()){
            query += " `sum` = ? AND ";
        }

        if (form09 != null && form09.getRecordDate() != null && form09.getSearchRecordDate() != null){
            query += " (`recorddate` BETWEEN ? AND ?) AND ";
        }

        if (form09 != null && form09.getCREBY() != null && !form09.getCREBY().isEmpty()){
            query += " `hform09`.`cre_by` = ? AND ";
        }

        query += " `hform09`.`delflg` = 'N'";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query);

                if (form09.getTarilgaNer() != null && !form09.getTarilgaNer().isEmpty()) {
                    statement.setString(stIndex++, form09.getTarilgaNer());
                }

                if (form09.getTarilgaNer_en() != null && !form09.getTarilgaNer_en().isEmpty()) {
                    statement.setString(stIndex++, form09.getTarilgaNer_en());
                }

            /* NER */
                if (form09.getNer() != null && !form09.getNer().toString().isEmpty()) {
                    statement.setString(stIndex++, form09.getNer());
                }
                if (form09.getNer_en() != null && !form09.getNer_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form09.getNer_en());
                }

            /* Zartsuulsan hemjee */
                if (form09.getZartsuulsanHemjee() != null && !form09.getZartsuulsanHemjee().toString().isEmpty()) {
                    statement.setString(stIndex++, form09.getZartsuulsanHemjee());
                }
                if (form09.getZartsuulsanHemjee_en() != null && !form09.getZartsuulsanHemjee_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form09.getZartsuulsanHemjee_en());
                }

            /* MAL TURUL */
                if (form09.getMalTurul() != null && !form09.getMalTurul().isEmpty()) {
                    statement.setString(stIndex++, form09.getMalTurul());
                }
                if (form09.getMalTurul_en() != null && !form09.getMalTurul_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form09.getMalTurul_en());
                }

            /* Tuluvlugu */
                if (form09.getTolovlogoo() != null && !form09.getTolovlogoo().isEmpty()) {
                    statement.setString(stIndex++, form09.getTolovlogoo());
                }
                if (form09.getTolovlogoo_en() != null && !form09.getTolovlogoo_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form09.getTolovlogoo_en());
                }

            /* Guitsetgel */
                if (form09.getGuitsetgel() != null && !form09.getGuitsetgel().isEmpty()) {
                    statement.setString(stIndex++, form09.getGuitsetgel());
                }
                if (form09.getGuitsetgel_en() != null && !form09.getGuitsetgel_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form09.getGuitsetgel_en());
                }

            /* Huwi */
                if (form09.getHuvi() != null && !form09.getHuvi().isEmpty()) {
                    statement.setString(stIndex++, form09.getHuvi());
                }
                if (form09.getHuvi_en() != null && !form09.getHuvi_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form09.getHuvi_en());
                }

                if (form09.getOLGOSON_TUN() != null && !form09.getOLGOSON_TUN().toString().isEmpty() && form09.getOLGOSON_TUN().longValue() != 0) {
                    statement.setLong(stIndex++, form09.getOLGOSON_TUN());
                }

                if (form09.getHEREGLESEN_TUN() != null && !form09.getHEREGLESEN_TUN().toString().isEmpty() && form09.getHEREGLESEN_TUN().longValue() != 0) {
                    statement.setLong(stIndex++, form09.getHEREGLESEN_TUN());
                }

                if (form09.getUSTGASAN_TUN() != null && !form09.getUSTGASAN_TUN().toString().isEmpty() && form09.getUSTGASAN_TUN().longValue() != 0) {
                    statement.setLong(stIndex++, form09.getUSTGASAN_TUN());
                }

                if (form09.getNUUTSULSEN_TUN() != null && !form09.getNUUTSULSEN_TUN().toString().isEmpty() && form09.getNUUTSULSEN_TUN().longValue() != 0) {
                    statement.setLong(stIndex++, form09.getNUUTSULSEN_TUN());
                }

                if (form09 != null && form09.getAimag() != null && !form09.getAimag().isEmpty()){
                    statement.setString(stIndex++, form09.getAimag());
                }
                if (form09 != null && form09.getSum() != null && !form09.getSum().isEmpty()){
                    statement.setString(stIndex++, form09.getSum());
                }

                if (form09 != null && form09.getRecordDate() != null && form09.getSearchRecordDate() != null){
                    statement.setDate(stIndex++, form09.getRecordDate());
                    statement.setDate(stIndex++, form09.getSearchRecordDate());
                }
                if (form09 != null && form09.getCREBY() != null && !form09.getCREBY().isEmpty()){
                    statement.setString(stIndex++, form09.getCREBY());
                }

                ResultSet resultSet = statement.executeQuery();

                form09EntitieList = new ArrayList<>();
                IComboService comboService = new ComboServiceImpl(boneCP);
                List<BasicDBObject> cmbType = comboService.getComboVals("MAL", "MN");
                int index=1;
                while (resultSet.next()) {
                    Form09Entity entity = new Form09Entity();

                    entity.setId(resultSet.getLong("id"));
                    entity.setTarilgaNer(resultSet.getString("tarilga_ner_mn"));
                    entity.setTarilgaNer_en(resultSet.getString("tarilga_ner_en"));
                    entity.setNer(resultSet.getString("ner_mn"));
                    entity.setNer_en(resultSet.getString("ner_en"));
                    entity.setMalTurul(resultSet.getString("mal_turul_mn"));
                    entity.setMalTurul_en(resultSet.getString("mal_turul_en"));
                    entity.setTolovlogoo(resultSet.getString("tuluvluguu_mn"));
                    entity.setTolovlogoo_en(resultSet.getString("tuluvluguu_en"));
                    entity.setGuitsetgel(resultSet.getString("guitsetgel_mn"));
                    entity.setGuitsetgel_en(resultSet.getString("guitsetgel_en"));
                    entity.setHuvi(resultSet.getString("huwi_mn"));
                    entity.setHuvi_en(resultSet.getString("huwi_en"));
                    entity.setOLGOSON_TUN(resultSet.getLong("olgosonTun"));
                    entity.setHEREGLESEN_TUN(resultSet.getLong("hereglesenTun"));
                    entity.setUSTGASAN_TUN(resultSet.getLong("ustgasanTun"));
                    entity.setNUUTSULSEN_TUN(resultSet.getLong("nuutsulsenTun"));
                    entity.setDelflg(resultSet.getString("delflg"));
                    entity.setActflg(resultSet.getString("actflg"));
                    entity.setModat(resultSet.getDate("mod_at"));
                    entity.setModby(resultSet.getString("mod_by"));
                    entity.setCreat(resultSet.getDate("cre_at"));
                    entity.setRecordDate(resultSet.getDate("recorddate"));
                    entity.setCreby(resultSet.getString("cre_by"));
                    entity.setZartsuulsanHemjee(resultSet.getString("zartsuulsan_hemjee_mn"));
                    entity.setZartsuulsanHemjee_en(resultSet.getString("zartsuulsan_hemjee_en"));
                    entity.setAimag(resultSet.getString("aimag"));
                    entity.setSum(resultSet.getString("sum"));
                    entity.setAimag_en(resultSet.getString("aimag_en"));
                    entity.setSum_en(resultSet.getString("sum_en"));
                    entity.put("sum_latitude", resultSet.getString("sum_latitude"));
                    entity.put("sum_longitude", resultSet.getString("sum_longitude"));
                    entity.setMalTurulNer(getMalTurulNer(cmbType, resultSet.getString("mal_turul_mn")));
                    entity.put("index",index++);
                    form09EntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return form09EntitieList;
    }

    /**
     * Get name
     *
     * @param cmbType
     * @param mal_turul_mn
     * @return
     */
    private String getMalTurulNer(List<BasicDBObject> cmbType, String mal_turul_mn) {
        for (BasicDBObject obj :
                cmbType) {
            if (obj.get("id") != null && mal_turul_mn != null && obj.getString("id").equalsIgnoreCase(mal_turul_mn)) {
                return obj.getString("name");
            }
        }

        return "";
    }

    /**
     * Get name
     *
     * @param cmbType
     * @param tarilga_ner_mn
     * @return
     */
    private String getTarilgaNer(List<BasicDBObject> cmbType, String tarilga_ner_mn) {
        for (BasicDBObject obj :
                cmbType) {
            if (obj.get("name") != null && tarilga_ner_mn != null && obj.getString("name").equalsIgnoreCase(tarilga_ner_mn)) {
                return obj.getString("name");
            }
        }

        return "";
    }

    @Override
    public ErrorEntity updateData(Form09Entity form09) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (form09 == null) {
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        query = "UPDATE `hospital`.`hform09` " +
                "SET     ";

        if (form09.getTarilgaNer() != null && !form09.getTarilgaNer().isEmpty()){
            query += " tarilga_ner_mn = ?, ";
        }

        if (form09.getTarilgaNer_en() != null && !form09.getTarilgaNer_en().isEmpty()){
            query += " tarilga_ner_en = ?, ";
        }

        /* NER */
        if (form09.getNer() != null && !form09.getNer().toString().isEmpty()){
            query += " ner_mn = ?, ";
        }
        if (form09.getNer_en() != null && !form09.getNer_en().toString().isEmpty()){
            query += " ner_en = ?, ";
        }
        /* Zartsuulsan hemjee */
        if (form09.getZartsuulsanHemjee() != null && !form09.getZartsuulsanHemjee().toString().isEmpty()){
            query += " zartsuulsan_hemjee_mn = ?, ";
        }
        if (form09.getZartsuulsanHemjee_en() != null && !form09.getZartsuulsanHemjee_en().toString().isEmpty()){
            query += " zartsuulsan_hemjee_en = ?, ";
        }

        /* MAL TURUL */
        if (form09.getMalTurul() != null && !form09.getMalTurul().isEmpty()){
            query += " mal_turul_mn = ?, ";
        }
        if (form09.getMalTurul_en() != null && !form09.getMalTurul_en().toString().isEmpty()){
            query += " mal_turul_en = ?, ";
        }

        /* Tuluvlugu */
        if (form09.getTolovlogoo() != null && !form09.getTolovlogoo().isEmpty()){
            query += " tuluvluguu_mn = ?, ";
        }
        if (form09.getTolovlogoo_en() != null && !form09.getTolovlogoo_en().toString().isEmpty()){
            query += " tuluvluguu_en = ?, ";
        }

        /* Guitsetgel */
        if (form09.getGuitsetgel() != null && !form09.getGuitsetgel().isEmpty()){
            query += " guitsetgel_mn = ?, ";
        }
        if (form09.getGuitsetgel_en() != null && !form09.getGuitsetgel_en().toString().isEmpty()){
            query += " guitsetgel_en = ?, ";
        }

        /* Huwi */
        if (form09.getHuvi() != null && !form09.getHuvi().isEmpty()){
            query += " huwi_mn = ?, ";
        }
        if (form09.getHuvi_en() != null && !form09.getHuvi_en().toString().isEmpty()){
            query += " huwi_en = ?, ";
        }

        if (form09.getOLGOSON_TUN() != null && !form09.getOLGOSON_TUN().toString().isEmpty() && form09.getOLGOSON_TUN().longValue() != 0) {
            query += " olgosonTun = ? , ";
        }
        if (form09.getHEREGLESEN_TUN() != null && !form09.getHEREGLESEN_TUN().toString().isEmpty() && form09.getHEREGLESEN_TUN().longValue() != 0) {
            query += " hereglesenTun = ? , ";
        }
        if (form09.getUSTGASAN_TUN() != null && !form09.getUSTGASAN_TUN().toString().isEmpty() && form09.getUSTGASAN_TUN().longValue() != 0) {
            query += " ustgasanTun = ? , ";
        }
        if (form09.getNUUTSULSEN_TUN() != null && !form09.getNUUTSULSEN_TUN().toString().isEmpty() && form09.getNUUTSULSEN_TUN().longValue() != 0) {
            query += " nuutsulsenTun = ? , ";
        }

        /* Delete flag */
        if (form09.getDELFLG() != null && !form09.getDELFLG().toString().isEmpty()){
            query += " delflg = ?, ";
        }

        /* Active flag */
        if (form09.getACTFLG() != null && !form09.getACTFLG().toString().isEmpty()){
            query += " actflg = ?, ";
        }
        if (form09 != null && form09.getRecordDate() != null){
            query += " `recorddate` = ?, ";
        }
        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";
        query += " WHERE  ID   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (form09.getTarilgaNer() != null && !form09.getTarilgaNer().isEmpty()) {
                    statement.setString(stIndex++, form09.getTarilgaNer());
                }

                if (form09.getTarilgaNer_en() != null && !form09.getTarilgaNer_en().isEmpty()) {
                    statement.setString(stIndex++, form09.getTarilgaNer_en());
                }

            /* NER */
                if (form09.getNer() != null && !form09.getNer().toString().isEmpty()) {
                    statement.setString(stIndex++, form09.getNer());
                }
                if (form09.getNer_en() != null && !form09.getNer_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form09.getNer_en());
                }

            /* Zartsuulsan hemjee */
                if (form09.getZartsuulsanHemjee() != null && !form09.getZartsuulsanHemjee().toString().isEmpty()) {
                    statement.setString(stIndex++, form09.getZartsuulsanHemjee());
                }
                if (form09.getZartsuulsanHemjee_en() != null && !form09.getZartsuulsanHemjee_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form09.getZartsuulsanHemjee_en());
                }

            /* MAL TURUL */
                if (form09.getMalTurul() != null && !form09.getMalTurul().isEmpty()) {
                    statement.setString(stIndex++, form09.getMalTurul());
                }
                if (form09.getMalTurul_en() != null && !form09.getMalTurul_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form09.getMalTurul_en());
                }

            /* Tuluvlugu */
                if (form09.getTolovlogoo() != null && !form09.getTolovlogoo().isEmpty()) {
                    statement.setString(stIndex++, form09.getTolovlogoo());
                }
                if (form09.getTolovlogoo_en() != null && !form09.getTolovlogoo_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form09.getTolovlogoo_en());
                }

            /* Guitsetgel */
                if (form09.getGuitsetgel() != null && !form09.getGuitsetgel().isEmpty()) {
                    statement.setString(stIndex++, form09.getGuitsetgel());
                }
                if (form09.getGuitsetgel_en() != null && !form09.getGuitsetgel_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form09.getGuitsetgel_en());
                }

            /* Huwi */
                if (form09.getHuvi() != null && !form09.getHuvi().isEmpty()) {
                    statement.setString(stIndex++, form09.getHuvi());
                }
                if (form09.getHuvi_en() != null && !form09.getHuvi_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form09.getHuvi_en());
                }

                if (form09.getOLGOSON_TUN() != null && !form09.getOLGOSON_TUN().toString().isEmpty() && form09.getOLGOSON_TUN().longValue() != 0) {
                    statement.setLong(stIndex++, form09.getOLGOSON_TUN());
                }

                if (form09.getHEREGLESEN_TUN() != null && !form09.getHEREGLESEN_TUN().toString().isEmpty() && form09.getHEREGLESEN_TUN().longValue() != 0) {
                    statement.setLong(stIndex++, form09.getHEREGLESEN_TUN());
                }

                if (form09.getUSTGASAN_TUN() != null && !form09.getUSTGASAN_TUN().toString().isEmpty() && form09.getUSTGASAN_TUN().longValue() != 0) {
                    statement.setLong(stIndex++, form09.getUSTGASAN_TUN());
                }

                if (form09.getNUUTSULSEN_TUN() != null && !form09.getNUUTSULSEN_TUN().toString().isEmpty() && form09.getNUUTSULSEN_TUN().longValue() != 0) {
                    statement.setLong(stIndex++, form09.getNUUTSULSEN_TUN());
                }

            /* Delete flag */
                if (form09.getDELFLG() != null && !form09.getDELFLG().toString().isEmpty()) {
                    statement.setString(stIndex++, form09.getDELFLG());
                }

            /* Active flag */
                if (form09.getACTFLG() != null && !form09.getACTFLG().toString().isEmpty()) {
                    statement.setString(stIndex++, form09.getACTFLG());
                }

                if (form09 != null && form09.getRecordDate() != null){
                    statement.setDate(stIndex++, form09.getRecordDate());
                }
                statement.setDate(stIndex++, form09.getMODAT());
                statement.setString(stIndex++, form09.getMODBY());
                statement.setLong(stIndex++, form09.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                    errObj.setErrText("Бичлэг амжилттай засагдлаа.");
                    errObj.setErrDesc("Бичлэг амжилттай засагдлаа.");
                    return errObj;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }


        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1061));
        errObj.setErrText("Амжилтгүй боллоо. Дахин оролдоно уу.");
        errObj.setErrDesc("Амжилтгүй боллоо. Дахин оролдоно уу.");

        return errObj;
    }

    @Override
    public List<BasicDBObject> getPieChartData(Form09Entity entity) throws SQLException {
        int stIndex = 1;
        List<BasicDBObject> formEntitieList = null;

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }


        String query_sel = "SELECT `mal_turul_mn`, COUNT(`id`) AS cnt, `hform09`.`aimag`, `hform09`.`sum`  " +
                "FROM `hospital`.`hform09`  " +
                "WHERE   ";

        if (entity != null && !entity.getAimag().isEmpty()){
            query_sel += " `hform09`.`aimag` = ? AND ";
        }
        if (entity != null && !entity.getSum().isEmpty()){
            query_sel += " `hform09`.`sum` = ? AND ";
        }
        if (entity != null && !entity.getDELFLG().isEmpty()){
            query_sel += " `hform09`.`delflg` = ? AND ";
        }
        if (entity != null && !entity.getACTFLG().isEmpty()){
            query_sel += " `hform09`.`actflg` = ? AND ";
        }
        if (entity != null && !entity.getMalTurul().isEmpty()){
            query_sel += " `hform09`.`mal_turul_mn` = ? AND ";
        }
        if (entity != null && !entity.getCREBY().isEmpty()){
            query_sel += " `hform09`.`cre_by` = ? AND ";
        }
        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null){
            query_sel += " (`recorddate` BETWEEN ? AND ?) AND ";
        }

        query_sel += " `id` > 0  GROUP BY `hform09`.`mal_turul_mn`";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                int stindex = 1;

                if (entity != null && !entity.getAimag().isEmpty()){
                    statement.setString(stindex++, entity.getAimag());
                }
                if (entity != null && !entity.getSum().isEmpty()){
                    statement.setString(stindex++, entity.getSum());
                }
                if (entity != null && !entity.getDELFLG().isEmpty()){
                    statement.setString(stindex++, entity.getDELFLG());
                }
                if (entity != null && !entity.getACTFLG().isEmpty()){
                    statement.setString(stindex++, entity.getACTFLG());
                }
                if (entity != null && !entity.getMalTurul().isEmpty()){
                    statement.setString(stindex++, entity.getMalTurul());
                }
                if (entity != null && !entity.getCREBY().isEmpty()){
                    statement.setString(stindex++, entity.getCREBY());
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null){
                    statement.setDate(stindex++, entity.getRecordDate());
                    statement.setDate(stindex++, entity.getSearchRecordDate());
                }

                ResultSet resultSet = statement.executeQuery();

                formEntitieList = new ArrayList<>();

                IComboService comboService = new ComboServiceImpl(boneCP);
                List<BasicDBObject> cmbType = comboService.getComboVals("MAL", "MN");

                while (resultSet.next()) {
                    BasicDBObject obj = new BasicDBObject();

                    obj.put("malturulner", getMalTurulNer(cmbType, resultSet.getString("mal_turul_mn")));
                    obj.put("malturul", resultSet.getString("mal_turul_mn"));
                    obj.put("count", resultSet.getString("cnt"));
                    obj.put("aimag", resultSet.getString("aimag"));
                    obj.put("sum", resultSet.getString("sum"));

                    formEntitieList.add(obj);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return formEntitieList;
    }
    @Override
    public ErrorEntity exportReport(String file, Form09Entity form09) throws SQLException {
        ErrorEntity errorEntity = null;
        String fileName = "hform09";

        try (Connection connection = boneCP.getConnection()){
            String JRXML_PATH = Config.getJrxmlPath();
            String REPORT_PATH = Config.getReportPath();

            try {
                Map parameters = new HashMap();

                String reportTitle = "ХАЛДВАРТ ӨВЧНӨӨС СЭРГИЙЛЭХ АРГА ХЭМЖЭЭНИЙ $$SEASON$$ -Р УЛИРАЛ, ЖИЛИЙН ЭЦСИЙН МЭДЭЭ",
                        currentYear = "$$YEAR$$ он №....",
                        printDate = "$$YEAR$$ оны $$MONTH$$ сарын $$DAY$$";

                Calendar now = Calendar.getInstance();

                int year = now.get(Calendar.YEAR);
                int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
                int day = now.get(Calendar.DAY_OF_MONTH);

                printDate = printDate.replace("$$YEAR$$", String.valueOf(year));
                printDate = printDate.replace("$$MONTH$$", String.valueOf(month));
                printDate = printDate.replace("$$DAY$$", String.valueOf(day));

                currentYear = currentYear.replace("$$YEAR$$", String.valueOf(year));

                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(new Date(form09.getRecordDate().getTime()));

                reportTitle = reportTitle.replace("$$YEAR$$", String.valueOf(calendar1.get(Calendar.YEAR)));

                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(new Date(form09.getSearchRecordDate().getTime()));

                if (calendar2.get(Calendar.MONTH) < 4){
                    reportTitle = reportTitle.replace("$$SEASON$$", "1");
                }
                else if (calendar2.get(Calendar.MONTH) < 7){
                    reportTitle = reportTitle.replace("$$SEASON$$", "2");
                }
                else if (calendar2.get(Calendar.MONTH) < 10){
                    reportTitle = reportTitle.replace("$$SEASON$$", "3");
                }
                else if (calendar2.get(Calendar.MONTH) <= 12){
                    reportTitle = reportTitle.replace("$$SEASON$$", "4");
                }

                parameters.put("start_date", form09.getRecordDate());
                parameters.put("end_date", form09.getSearchRecordDate());
                parameters.put("citycode", (form09.getAimag() != null && !form09.getAimag().isEmpty()) ? form09.getAimag() : null);
                parameters.put("sumcode", (form09.getSum() != null && !form09.getSum().isEmpty()) ? form09.getSum() : null);
                parameters.put("currentyear", currentYear);

                ICityService cityService = new CityServiceImpl(boneCP);
                City tmpCity = new City();
                tmpCity.setCode(form09.getAimag());
                List<City> clist = cityService.selectAll(tmpCity);

                if (clist != null && clist.size() == 1) {
                    parameters.put("aimagner", clist.get(0).getCityname());
                } else {
                    parameters.put("aimagner", (form09.getAimagNer() == null) ? "" : form09.getAimagNer());
                }
                ISumService sumService = new SumServiceImpl(boneCP);
                Sum tmpSum = new Sum();
                tmpSum.setCitycode(form09.getAimag());
                tmpSum.setSumcode(form09.getSum());
                List<Sum> sumList = sumService.selectAll(tmpSum);

                if (sumList != null && sumList.size() == 1){
                    parameters.put("sumner", sumList.get(0).getSumname());
                }
                else{
                    parameters.put("sumner", (form09.getSumNer() == null) ? "" : form09.getSumNer());
                }

                parameters.put("printdate", printDate);
                parameters.put("reporttitle", reportTitle);
                parameters.put("creby", (form09.getCREBY() != null && !form09.getCREBY().isEmpty()) ? form09.getCREBY() : null);

                PdfExcelUtil.generateReport(file, JRXML_PATH, REPORT_PATH, fileName, parameters, connection);

                System.out.println("File Generated");
            }catch (Exception ex){
                ex.printStackTrace();
            }
            finally {
                if (connection != null)connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1001));
            if(file.equals("pdf")){
                errorEntity.setErrText(fileName + ".pdf");
            }else if(file.equals("xls")){
                errorEntity.setErrText(fileName + ".xls");
            }
        }
        return errorEntity;
    }


    @Override
    public BasicDBObject getColumnChartData(Form09Entity entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        //dateFormat.parse(dv).getTime()
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }

        try {
            IComboService comboService = new ComboServiceImpl(boneCP);
            List<Integer[]> rowdataList = new ArrayList<>();
            List<BasicDBObject> cmbType = comboService.getComboVals("MAL", "MN");

            basicDBObject.put("coldata", cmbType);

            //Saraar n haruulah
            int month = calendar1.get(Calendar.MONTH) + 1;
            int year1 = calendar1.get(Calendar.YEAR);
            int year2 = calendar2.get(Calendar.YEAR);

            try (Connection connection = boneCP.getConnection()) {
                String query_sel = "SELECT `mal_turul_mn`, COUNT(`id`) AS cnt, `hform09`.`aimag`, `hform09`.`sum`  " +
                        "FROM `hospital`.`hform09`  " +
                        "WHERE  ";

                if (entity != null && !entity.getAimag().isEmpty()){
                    query_sel += " `hform09`.`aimag` = ? AND ";
                }
                if (entity != null && !entity.getSum().isEmpty()){
                    query_sel += " `hform09`.`sum` = ? AND ";
                }
                if (entity != null && !entity.getDELFLG().isEmpty()){
                    query_sel += " `hform09`.`delflg` = ? AND ";
                }
                if (entity != null && !entity.getACTFLG().isEmpty()){
                    query_sel += " `hform09`.`actflg` = ? AND ";
                }
                if (entity != null && !entity.getMalTurul().isEmpty()){
                    query_sel += " `hform09`.`mal_turul_mn` = ? AND ";
                }
                if (entity != null && !entity.getCREBY().isEmpty()){
                    query_sel += " `hform09`.`cre_by` = ? AND ";
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null){
                    query_sel += " (`recorddate` BETWEEN ? AND ?) AND ";
                }

                query_sel += " `id` > 0  GROUP BY `hform09`.`mal_turul_mn` ORDER BY `hform09`.`mal_turul_mn`";

                do {
                    //do process
                    Integer[] rowdata = new Integer[cmbType.size() + 1];
                    //
                    //rowdata[0] = month + " -р сар";
                    rowdata[0] = month;

                    try {
                        PreparedStatement statement = connection.prepareStatement(query_sel);

                        java.sql.Date stdate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-01 00:00:00").getTime());

                        java.sql.Date enddate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-31 00:00:00").getTime());

                        if (month == 12 && year1 < year2) {
                            month = 1;
                            year1++;
                        } else {
                            month++;
                        }

                        int stindex = 1;

                        if (entity != null && !entity.getAimag().isEmpty()){
                            statement.setString(stindex++, entity.getAimag());
                        }
                        if (entity != null && !entity.getSum().isEmpty()){
                            statement.setString(stindex++, entity.getSum());
                        }
                        if (entity != null && !entity.getDELFLG().isEmpty()){
                            statement.setString(stindex++, entity.getDELFLG());
                        }
                        if (entity != null && !entity.getACTFLG().isEmpty()){
                            statement.setString(stindex++, entity.getACTFLG());
                        }
                        if (entity != null && !entity.getMalTurul().isEmpty()){
                            statement.setString(stindex++, entity.getMalTurul());
                        }
                        if (entity != null && !entity.getCREBY().isEmpty()){
                            statement.setString(stindex++, entity.getCREBY());
                        }
                        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null){
                            statement.setDate(stindex++, stdate);
                            statement.setDate(stindex++, enddate);
                        }


                        ResultSet resultSet = statement.executeQuery();

                        HashMap<String, String> hashMap = new HashMap<>();

                        while (resultSet.next()) {
                            String malturul = resultSet.getString("mal_turul_mn");
                            String count = resultSet.getString("cnt");

                            hashMap.put(malturul, count);
                        }//end while

                        for (int i = 1; i <= cmbType.size(); i++) {
                            rowdata[i] = (hashMap.get(cmbType.get(i - 1).getString("id")) == null) ? 0 :
                                    Integer.parseInt(hashMap.get(cmbType.get(i - 1).getString("id")));
                        }

                        rowdataList.add(rowdata);

                        if (statement != null) statement.close();
                        if (resultSet != null) resultSet.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();

                        break;
                    }

                    ///
                } while (month <= calendar2.get(Calendar.MONTH) + 1);

                basicDBObject.put("rowdata", rowdataList);

                connection.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }

    @Override
    public BasicDBObject getColHighChartData(Form09Entity form09Entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();
        List<String> categories = new ArrayList<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(form09Entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(form09Entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }

        try {
            IComboService comboService = new ComboServiceImpl(boneCP);
            List<BasicDBObject> cmbType =comboService.getComboNames("MAL", "MN");

             if (form09Entity != null && !form09Entity.getMalTurul().isEmpty()) {
                 cmbType = comboService.getComboNames("UVCH", "MN");
             }

            //Saraar n haruulah
            int month = calendar1.get(Calendar.MONTH) + 1;
            int year1 = calendar1.get(Calendar.YEAR);
            int year2 = calendar2.get(Calendar.YEAR);
            try (Connection connection = boneCP.getConnection()) {
                String query_sel = "SELECT `mal_turul_mn`, COUNT(`id`) AS cnt, `hform09`.`aimag`, `hform09`.`sum`  " +
                "FROM `hospital`.`hform09`  " +
                "WHERE  ";

                if (form09Entity != null && !form09Entity.getMalTurul().isEmpty()) {
                    query_sel = "SELECT `tarilga_ner_mn`, SUM(`zartsuulsan_hemjee_mn`) AS cnt, `hform09`.`aimag`, `hform09`.`sum`" +
                            "FROM `hospital`.`hform09`  " +
                            "WHERE  ";
                }

                if (form09Entity != null && !form09Entity.getAimag().isEmpty()) {
                    query_sel += " `hform09`.`aimag` = ? AND ";
                }
                if (form09Entity != null && !form09Entity.getSum().isEmpty()) {
                    query_sel += " `hform09`.`sum` = ? AND ";
                }
                if (form09Entity != null && !form09Entity.getDELFLG().isEmpty()) {
                    query_sel += " `hform09`.`delflg` = ? AND ";
                }
                 if (form09Entity != null && !form09Entity.getTarilgaNer().isEmpty()) {
                     query_sel += " `hform09`.`tarilga_ner_mn` = ? AND ";
                 }

                if (form09Entity != null && !form09Entity.getACTFLG().isEmpty()) {
                    query_sel += " `hform09`.`actflg` = ? AND ";
                }
                if (form09Entity != null && !form09Entity.getMalTurul().isEmpty()) {
                    query_sel += " `hform09`.`mal_turul_mn` = ? AND ";
                }
                if (form09Entity != null && !form09Entity.getCREBY().isEmpty()) {
                    query_sel += " `hform09`.`cre_by` = ? AND ";
                }
                if (form09Entity != null && form09Entity.getRecordDate() != null && form09Entity.getSearchRecordDate() != null) {
                    query_sel += " (`recorddate` BETWEEN ? AND ?) AND ";
                }



                if (form09Entity != null && !form09Entity.getMalTurul().isEmpty()) {
                    query_sel += " `id` > 0  GROUP BY `hform09`.`tarilga_ner_mn` ORDER BY `hform09`.`tarilga_ner_mn`";
                }
                else{
                    query_sel += " `id` > 0  GROUP BY `hform09`.`mal_turul_mn` ORDER BY `hform09`.`mal_turul_mn`";
                }

                do {
                    //do process
                    categories.add(year1 + "-" + month);

                    try {
                        PreparedStatement statement = connection.prepareStatement(query_sel);

                        java.sql.Date stdate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-01 00:00:00").getTime());

                        java.sql.Date enddate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-31 00:00:00").getTime());

                        if (month == 12 && year1 < year2) {
                            month = 1;
                            year1++;
                        } else {
                            month++;
                        }

                        int stindex = 1;

                        if (form09Entity != null && !form09Entity.getAimag().isEmpty()) {
                            statement.setString(stindex++, form09Entity.getAimag());
                        }
                        if (form09Entity != null && !form09Entity.getSum().isEmpty()) {
                            statement.setString(stindex++, form09Entity.getSum());
                        }
                        if (form09Entity != null && !form09Entity.getDELFLG().isEmpty()) {
                            statement.setString(stindex++, form09Entity.getDELFLG());
                        }
                        if (form09Entity != null && !form09Entity.getACTFLG().isEmpty()) {
                            statement.setString(stindex++, form09Entity.getACTFLG());
                        }
                         if (form09Entity != null && !form09Entity.getTarilgaNer().isEmpty()) {
                             statement.setString(stindex++, form09Entity.getTarilgaNer());
                         }
                        if (form09Entity != null && !form09Entity.getMalTurul().isEmpty()) {
                            statement.setString(stindex++, form09Entity.getMalTurul());
                        }
                        if (form09Entity != null && !form09Entity.getCREBY().isEmpty()) {
                            statement.setString(stindex++, form09Entity.getCREBY());
                        }
                        if (form09Entity != null && form09Entity.getRecordDate() != null && form09Entity.getSearchRecordDate() != null) {
                            statement.setDate(stindex++, stdate);
                            statement.setDate(stindex++, enddate);
                        }
                        ResultSet resultSet = statement.executeQuery();

                        HashMap<String, String> hashMap = new HashMap<>();

                        while (resultSet.next()) {
                            // System.out.println(resultSet.getString("tarilga_ner_mn"));
                              String uvchinner = "";
                              if (form09Entity != null && !form09Entity.getMalTurul().isEmpty()) {
                                  uvchinner = resultSet.getString("tarilga_ner_mn");
                              }else{
                                  uvchinner = resultSet.getString("mal_turul_mn");
                              }
                            String count = resultSet.getString("cnt");
                            hashMap.put(uvchinner, count);
                        }//end while
                        List<Double> list = null;
                        if (form09Entity != null && !form09Entity.getMalTurul().isEmpty()) {
                            for (int i = 0; i < cmbType.size(); i++) {
                                BasicDBObject obj = cmbType.get(i);

                                if (obj.get("data") == null){
                                    //new record
                                    list = new ArrayList<>();
                                    String val = hashMap.get(obj.getString("name"));
                                    if (val == null) list.add(0.0);
                                    else if (val.isEmpty()) list.add(0.0);
                                    else list.add(Double.parseDouble(val));
                                    obj.put("data", list);
                                }else{
                                    //already exist

                                    list = (List<Double>) obj.get("data");
                                    String val = hashMap.get(obj.getString("name"));
                                    if (val == null) list.add(0.0);
                                    else if (val.isEmpty()) list.add(0.0);
                                    else list.add(Double.parseDouble(val));
                                }

                            }
                        }else{
                            for (int i = 0; i < cmbType.size(); i++) {
                                BasicDBObject obj = cmbType.get(i);

                                if (obj.get("data") == null){
                                    //new record
                                    list = new ArrayList<>();
                                    String val = hashMap.get(obj.getString("id"));
                                    if (val == null) list.add(0.0);
                                    else if (val.isEmpty()) list.add(0.0);
                                    else list.add(Double.parseDouble(val));
                                    obj.put("data", list);
                                }else{
                                    //already exist
                                    list = (List<Double>) obj.get("data");
                                    String val = hashMap.get(obj.getString("id"));
                                    if (val == null) list.add(0.0);
                                    else if (val.isEmpty()) list.add(0.0);
                                    else list.add(Double.parseDouble(val));
                                }

                            }
                        }
                        if (statement != null) statement.close();
                        if (resultSet != null) resultSet.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();

                        break;
                    }
                } while ((month <= calendar2.get(Calendar.MONTH) + 1 && year1 <= year2) || year1 < year2);

                basicDBObject.put("categories", categories);
                basicDBObject.put("series", cmbType);
                connection.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }

    @Override
    public BasicDBObject getPieHighChartData(Form09Entity form09Entity) throws SQLException {

        BasicDBObject basicDBObject = new BasicDBObject();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(form09Entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(form09Entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }


        String query_sel = "SELECT `mal_turul_mn`, COUNT(`id`) AS cnt, `hform09`.`aimag`, `hform09`.`sum`  " +
                "FROM `hospital`.`hform09`  " +
                "WHERE  ";

        if (form09Entity != null && !form09Entity.getMalTurul().isEmpty()) {
            query_sel = "SELECT `tarilga_ner_mn`, SUM(`zartsuulsan_hemjee_mn`) AS cnt, `hform09`.`aimag`, `hform09`.`sum`" +
                    "FROM `hospital`.`hform09`  " +
                    "WHERE  ";
        }

        if (form09Entity != null && !form09Entity.getAimag().isEmpty()) {
            query_sel += " `hform09`.`aimag` = ? AND ";
        }
        if (form09Entity != null && !form09Entity.getSum().isEmpty()) {
            query_sel += " `hform09`.`sum` = ? AND ";
        }
        if (form09Entity != null && !form09Entity.getDELFLG().isEmpty()) {
            query_sel += " `hform09`.`delflg` = ? AND ";
        }
        if (form09Entity != null && !form09Entity.getTarilgaNer().isEmpty()) {
            query_sel += " `hform09`.`tarilga_ner_mn` = ? AND ";
        }

        if (form09Entity != null && !form09Entity.getACTFLG().isEmpty()) {
            query_sel += " `hform09`.`actflg` = ? AND ";
        }
        if (form09Entity != null && !form09Entity.getMalTurul().isEmpty()) {
            query_sel += " `hform09`.`mal_turul_mn` = ? AND ";
        }
        if (form09Entity != null && !form09Entity.getCREBY().isEmpty()) {
            query_sel += " `hform09`.`cre_by` = ? AND ";
        }
        if (form09Entity != null && form09Entity.getRecordDate() != null && form09Entity.getSearchRecordDate() != null) {
            query_sel += " (`recorddate` BETWEEN ? AND ?) AND ";
        }



        if (form09Entity != null && !form09Entity.getMalTurul().isEmpty()) {
            query_sel += " `id` > 0  GROUP BY `hform09`.`tarilga_ner_mn` ORDER BY `hform09`.`tarilga_ner_mn`";
        }
        else{
            query_sel += " `id` > 0  GROUP BY `hform09`.`mal_turul_mn` ORDER BY `hform09`.`mal_turul_mn`";
        }

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                int stindex = 1;

                if (form09Entity != null && !form09Entity.getAimag().isEmpty()) {
                    statement.setString(stindex++, form09Entity.getAimag());
                }
                if (form09Entity != null && !form09Entity.getSum().isEmpty()) {
                    statement.setString(stindex++, form09Entity.getSum());
                }
                if (form09Entity != null && !form09Entity.getDELFLG().isEmpty()) {
                    statement.setString(stindex++, form09Entity.getDELFLG());
                }
                if (form09Entity != null && !form09Entity.getACTFLG().isEmpty()) {
                    statement.setString(stindex++, form09Entity.getACTFLG());
                }
                if (form09Entity != null && !form09Entity.getMalTurul().isEmpty()) {
                    statement.setString(stindex++, form09Entity.getMalTurul());
                }
                if (form09Entity != null && !form09Entity.getTarilgaNer().isEmpty()) {
                    statement.setString(stindex++, form09Entity.getTarilgaNer());
                }
                if (form09Entity != null && !form09Entity.getCREBY().isEmpty()) {
                    statement.setString(stindex++, form09Entity.getCREBY());
                }
                if (form09Entity != null && form09Entity.getRecordDate() != null && form09Entity.getSearchRecordDate() != null) {
                    statement.setDate(stindex++, form09Entity.getRecordDate());
                    statement.setDate(stindex++, form09Entity.getSearchRecordDate());
                }

                ResultSet resultSet = statement.executeQuery();

                IComboService comboService = new ComboServiceImpl(boneCP);
                List<BasicDBObject> cmbType =comboService.getComboNames("MAL", "MN");

                if (form09Entity != null && !form09Entity.getMalTurul().isEmpty()) {
                    cmbType = comboService.getComboNames("UVCH", "MN");
                }
                List<BasicDBObject> list = new ArrayList<>();

                while (resultSet.next()) {
                    BasicDBObject entity = new BasicDBObject();
                    if (form09Entity != null && !form09Entity.getMalTurul().isEmpty()) {
                        entity.put("name", getTarilgaNer(cmbType, resultSet.getString("tarilga_ner_mn")));
                    } else {
                        entity.put("name", getMalTurulNer(cmbType, resultSet.getString("mal_turul_mn")));
                    }
                    entity.put("y", resultSet.getDouble("cnt"));

                    list.add(entity);
                }

                basicDBObject.put("data", list);

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }
}
