package com.hospital.app;

import com.enumclass.ErrorType;
import com.jolbox.bonecp.BoneCP;
import com.model.hos.ErrorEntity;
import com.model.hos.NewsEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class NewsServiceImpl implements INewsService {
    /**
     * Objects
     */

    private BoneCP boneCP;


    public NewsServiceImpl (BoneCP boneCP) {
        this.boneCP = boneCP;
    }

    @Override
    public ErrorEntity insertNewData(NewsEntity news) throws SQLException {
        PreparedStatement statement = null;

        if (news == null) new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));

        String query_login = "INSERT INTO `hospital`.`h_news` " +
                "(`title_mn`, " +
                "`title_en`, " +
                "`content_mn`, " +
                "`content_en`, " +
                "`delflg`, " +
                "`actflg`, " +
                "`mod_at`, " +
                "`mod_by`, " +
                "`cre_at`, " +
                "`cre_by`)" +
                " VALUES " +
                "(? , ? , ? , ? , ? , ? , ? , ? , ? , ? )";

        try (Connection connection = boneCP.getConnection()) {
            try{
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, news.getTitle());
                statement.setString(2, news.getTitle_en());
                statement.setString(3, news.getContent());
                statement.setString(4, news.getContent_en());
                statement.setString(5, news.getDelflg());
                statement.setString(6, news.getActflg());
                statement.setDate(7, news.getModAt());
                statement.setString(8, news.getModby());
                statement.setDate(9, news.getCreat());
                statement.setString(10, news.getCreby());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1){
                    connection.commit();
                    return new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }finally {
                if (statement != null)statement.close();
                connection.close();
            }

        }catch (Exception ex){

        }

        return new ErrorEntity(ErrorType.ERROR, Long.valueOf(1057));
    }

    @Override
    public List<NewsEntity> getListData(NewsEntity news) throws SQLException {
        int stIndex = 1;
        List<NewsEntity> newsEntitieList = null;

        String query = "SELECT `h_news`.`id`, " +
                "    `h_news`.`title_mn`, " +
                "    `h_news`.`title_en`, " +
                "    `h_news`.`content_mn`, " +
                "    `h_news`.`content_en`, " +
                "    `h_news`.`delflg`, " +
                "    `h_news`.`actflg`, " +
                "    `h_news`.`mod_at`, " +
                "    `h_news`.`mod_by`, " +
                "    `h_news`.`cre_at`, " +
                "    `h_news`.`cre_by` " +
                "FROM `hospital`.`h_news` " +
                "WHERE ";

        if (news.getTitle() != null && !news.getTitle().isEmpty()){
            query += " title_mn = ? AND ";
        }

        if (news.getTitle_en() != null && !news.getTitle_en().isEmpty()){
            query += " title_en = ? AND ";
        }

        if (news.getContent() != null && !news.getContent().toString().isEmpty()){
            query += " content_mn = ? AND ";
        }
        if (news.getContent_en() != null && !news.getContent_en().toString().isEmpty()){
            query += " content_en = ? AND ";
        }

        query += " `h_news`.`delflg` = 'N' ORDER BY `cre_at` DESC ";

        System.out.println(query);

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query);

                if (news.getTitle() != null && !news.getTitle().isEmpty()) {
                    statement.setString(stIndex++, news.getTitle());
                }

                if (news.getTitle_en() != null && !news.getTitle_en().isEmpty()) {
                    statement.setString(stIndex++, news.getTitle_en());
                }

                if (news.getContent() != null && !news.getContent().toString().isEmpty()) {
                    statement.setString(stIndex++, news.getContent());
                }
                if (news.getContent_en() != null && !news.getContent_en().toString().isEmpty()) {
                    statement.setString(stIndex++, news.getContent_en());
                }

                ResultSet resultSet = statement.executeQuery();

                newsEntitieList = new ArrayList<>();

                IComboService comboService = new ComboServiceImpl(boneCP);

                while (resultSet.next()) {
                    NewsEntity entity = new NewsEntity();

                    entity.setId(resultSet.getLong("id"));
                    entity.setTitle(resultSet.getString("title_mn"));
                    entity.setTitle_en(resultSet.getString("title_en"));
                    entity.setContent(resultSet.getString("content_mn"));
                    entity.setContent_en(resultSet.getString("content_en"));
                    entity.setDelflg(resultSet.getString("delflg"));
                    entity.setActflg(resultSet.getString("actflg"));
                    entity.setModAt(resultSet.getDate("mod_at"));
                    entity.setModby(resultSet.getString("mod_by"));
                    entity.setCreat(resultSet.getDate("cre_at"));
                    entity.setCreby(resultSet.getString("cre_by"));

                    newsEntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return newsEntitieList;
    }

    @Override
    public ErrorEntity updateData(NewsEntity news) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (news == null) new ErrorEntity(ErrorType.INFO, Long.valueOf(1059));

        query = "UPDATE `hospital`.`h_news` " +
                "SET     ";

        if (news.getTitle() != null && !news.getTitle().isEmpty()){
            query += " title_mn = ?, ";
        }

        if (news.getTitle_en() != null && !news.getTitle_en().isEmpty()){
            query += " title_en = ?, ";
        }

        if (news.getContent() != null && !news.getContent().toString().isEmpty()){
            query += " content_mn = ?, ";
        }
        if (news.getContent_en() != null && !news.getContent_en().toString().isEmpty()){
            query += " content_en = ?, ";
        }

        /* Delete flag */
        if (news.getDelflg() != null && !news.getDelflg().toString().isEmpty()){
            query += " delflg = ?, ";
        }

        /* Active flag */
        if (news.getActflg() != null && !news.getActflg().toString().isEmpty()){
            query += " actflg = ?, ";
        }

        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";
        query += " WHERE  ID   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (news.getTitle() != null && !news.getTitle().isEmpty()) {
                    statement.setString(stIndex++, news.getTitle());
                }

                if (news.getTitle_en() != null && !news.getTitle_en().isEmpty()) {
                    statement.setString(stIndex++, news.getTitle_en());
                }

                if (news.getContent() != null && !news.getContent().toString().isEmpty()) {
                    statement.setString(stIndex++, news.getContent());
                }
                if (news.getContent_en() != null && !news.getContent_en().toString().isEmpty()) {
                    statement.setString(stIndex++, news.getContent_en());
                }

            /* Delete flag */
                if (news.getDelflg() != null && !news.getDelflg().toString().isEmpty()) {
                    statement.setString(stIndex++, news.getDelflg());
                }

            /* Active flag */
                if (news.getActflg() != null && !news.getActflg().toString().isEmpty()) {
                    statement.setString(stIndex++, news.getActflg());
                }

                statement.setDate(stIndex++, news.getModAt());
                statement.setString(stIndex++, news.getModby());
                statement.setLong(stIndex++, news.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    return new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }


        return new ErrorEntity(ErrorType.ERROR, Long.valueOf(1061));
    }

    @Override
    public Integer getNewsCount() throws SQLException {
        int newsCnt = 0;
        String query = "SELECT count(*) as newscnt FROM `h_news` WHERE delflg = 'N'";

        try (Connection connection = boneCP.getConnection()) {
            int stindex = 1;

            try {
                connection.setAutoCommit(false);
                PreparedStatement statement = connection.prepareStatement(query);

                ResultSet resultSet = statement.executeQuery();

                while (resultSet.next()) {
                    newsCnt = resultSet.getInt("newscnt");
                }


                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (connection != null){
                    connection.close();
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return newsCnt;
    }

}