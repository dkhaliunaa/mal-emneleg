package com.hospital.app;


import com.enumclass.ErrorType;
import com.jolbox.bonecp.BoneCP;
import com.model.User;
import com.model.hos.ErrorEntity;
import com.model.hos.TemtsehAhZardalEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Tilyeujan on 9/2/2017.
 */
public class TemcehArgaHemjeeZardalFormServiceImpl implements ITemcehArgaHemjeeZardalFormService {
    /**
     * Objects
     */
    private BoneCP boneCP;
    private ErrorEntity errorEntity;


    public TemcehArgaHemjeeZardalFormServiceImpl(BoneCP boneCP) {
        this.boneCP = boneCP;
    }

    @Override
    public ErrorEntity insertNewData(TemtsehAhZardalEntity entity) throws SQLException {
        PreparedStatement statement = null;

        if (entity == null) {
            errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));

            errorEntity.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errorEntity.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            
            return errorEntity;
        }

        String query_login = "INSERT INTO `hform01_temceh_ah_zardal`(`form01key`, `citycode`, `sumcode`, `bagcode`, " +
                "`malchinner`, `ajil_heseg`, `mal_emch`, `busad_humuus`, `technic_heregsel`, `meun`, `meut`, " +
                "`mea`, `tur_zb`, `nuhun_olg_zardal`, `record_date`, `delflg`, `actflg`, " +
                "`mod_at`, `mod_by`, `cre_at`, `cre_by`) " +
                "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        try (Connection connection = boneCP.getConnection()) {
            try{
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, (entity.get(entity.FORM01KEY) != null) ? entity.getForm01key().trim() : "");
                statement.setString(2, (entity.get(entity.CITYCODE) != null) ? entity.getCitycode().trim() : "");
                statement.setString(3, (entity.get(entity.SUMCODE) != null) ? entity.getSumcode().trim() : "");
                statement.setString(4, (entity.get(entity.BAGCODE) != null) ? entity.getBagcode().trim() : "");
                statement.setString(5, (entity.get(entity.MALCHINNER) != null) ? entity.getMalchinner().trim() : "");
                statement.setString(6, (entity.get(entity.AJIL_HESEG) != null) ? entity.getAjil_heseg().trim() : "");
                statement.setString(7, (entity.get(entity.MAL_EMCH) != null) ? entity.getMal_emch().trim() : "");
                statement.setString(8, (entity.get(entity.BUSAD_HUMUUS) != null) ? entity.getBusad_humuus().trim() : "");
                statement.setString(9, (entity.get(entity.TECHNIC_HEREGSEL) != null) ? entity.getTechnic_heregsel().trim() : "");
                statement.setString(10, (entity.get(entity.MEUN) != null) ? entity.getMeun().trim() : "");
                statement.setString(11, (entity.get(entity.MEUT) != null) ? entity.getMeut().trim() : "");
                statement.setString(12, (entity.get(entity.MEA) != null) ? entity.getMea().trim() : "");
                statement.setString(13, (entity.get(entity.TUR_ZB) != null) ? entity.getTur_zb().trim() : "");
                statement.setString(14, (entity.get(entity.NUHUN_OLG_ZARDAL) != null) ? entity.getNuhun_olg_zardal().trim() : "");
                statement.setDate(15, entity.getRecord_date());
                statement.setString(16, (entity.get(entity.DELFLG) != null) ? entity.getDelflg().trim() : "");
                statement.setString(17, (entity.get(entity.ACTFLG) != null) ? entity.getActflg().trim() : "");
                statement.setDate(18, entity.getMod_at());
                statement.setString(19, (entity.get(entity.MOD_BY) != null) ? entity.getMod_by().trim() : "");
                statement.setDate(20, entity.getCre_at());
                statement.setString(21, (entity.get(entity.CRE_BY) != null) ? entity.getCre_by().trim() : "");

                int insertCount = statement.executeUpdate();

                if (insertCount == 1){
                    connection.commit();

                    errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));

                    errorEntity.setErrText("Бичлэг амжилттай хадгалагдлаа.");
                    errorEntity.setErrDesc("Бичлэг амжилттай хадгалагдлаа.");

                    return errorEntity;
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }finally {
                if (statement != null)statement.close();
                connection.close();
            }

        }catch (Exception ex){

        }

        errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1057));
        errorEntity.setErrText("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");
        errorEntity.setErrDesc("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");

        return errorEntity;
    }

    @Override
    public List<TemtsehAhZardalEntity> getListData(TemtsehAhZardalEntity entity) throws SQLException {
        int stIndex = 1;
        int index = 1;
        List<TemtsehAhZardalEntity> entityList = null;

        String query = "SELECT `hform01_temceh_ah_zardal`.`id`, " +
                "    `hform01_temceh_ah_zardal`.`form01key`, " +
                "    `hform01_temceh_ah_zardal`.`citycode`, " +
                "    `hform01_temceh_ah_zardal`.`sumcode`, " +
                "    `hform01_temceh_ah_zardal`.`bagcode`, " +
                "    `hform01_temceh_ah_zardal`.`malchinner`, " +
                "    `hform01_temceh_ah_zardal`.`ajil_heseg`, " +
                "    `hform01_temceh_ah_zardal`.`mal_emch`, " +
                "    `hform01_temceh_ah_zardal`.`busad_humuus`, " +
                "    `hform01_temceh_ah_zardal`.`technic_heregsel`, " +
                "    `hform01_temceh_ah_zardal`.`meun`, " +
                "    `hform01_temceh_ah_zardal`.`meut`, " +
                "    `hform01_temceh_ah_zardal`.`mea`, " +
                "    `hform01_temceh_ah_zardal`.`tur_zb`, " +
                "    `hform01_temceh_ah_zardal`.`nuhun_olg_zardal`, " +
                "    `hform01_temceh_ah_zardal`.`record_date`, " +
                "    `hform01_temceh_ah_zardal`.`delflg`, " +
                "    `hform01_temceh_ah_zardal`.`actflg`, " +
                "    `hform01_temceh_ah_zardal`.`mod_at`, " +
                "    `hform01_temceh_ah_zardal`.`mod_by`, " +
                "    `hform01_temceh_ah_zardal`.`cre_at`, " +
                "    `hform01_temceh_ah_zardal`.`cre_by`, " +
                "`h_sum`.sumname, " +
                "`h_sum`.sumname_en, " +
                "`h_baghoroo`.horooname, " +
                "`h_baghoroo`.horooname_en, " +
                "`h_city`.cityname, " +
                "`h_city`.cityname_en " +
                "FROM `hospital`.`hform01_temceh_ah_zardal` INNER JOIN `h_city` " +
                "ON `h_city`.code = `hform01_temceh_ah_zardal`.citycode " +
                "INNER JOIN `h_sum` " +
                "ON `h_sum`.sumcode = `hform01_temceh_ah_zardal`.sumcode " +
                "INNER JOIN `h_baghoroo` ON `hform01_temceh_ah_zardal`.bagcode = `h_baghoroo`.horoocode " +
                "AND `h_baghoroo`.sumcode = `hform01_temceh_ah_zardal`.`sumcode` " +
                "WHERE ";

        if (entity.getForm01key() != null && !entity.getForm01key().isEmpty()){
            query += " `hform01_temceh_ah_zardal`.`form01key` = ? AND ";
        }
        if (entity.getCitycode() != null && !entity.getCitycode().isEmpty()){
            query += " `hform01_temceh_ah_zardal`.`citycode` = ? AND ";
        }
        if (entity.getSumcode() != null && !entity.getSumcode().isEmpty()){
            query += " `hform01_temceh_ah_zardal`.`sumcode` = ? AND ";
        }
        if (entity.getBagcode() != null && !entity.getBagcode().isEmpty()){
            query += " `hform01_temceh_ah_zardal`.`bagcode` = ? AND ";
        }
        if (entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
            query += " `hform01_temceh_ah_zardal`.`delflg` = ? AND ";
        }
        if (entity.getActflg() != null && !entity.getActflg().isEmpty()){
            query += " `hform01_temceh_ah_zardal`.`actflg` = ? AND ";
        }
        if (entity != null && entity.getCre_by() != null && !entity.getCre_by().isEmpty()){
            query += " `hform01_temceh_ah_zardal`.`cre_by` = ? AND ";
        }

        query += " `hform01_temceh_ah_zardal`.`id` > 0 ";
        query += " ORDER BY `hform01_temceh_ah_zardal`.`cre_at` DESC ";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query);

                if (entity.getForm01key() != null && !entity.getForm01key().isEmpty()){
                    statement.setString(stIndex++, entity.getForm01key());
            }
                if (entity.getCitycode() != null && !entity.getCitycode().isEmpty()){
                    statement.setString(stIndex++, entity.getCitycode());
                }
                if (entity.getSumcode() != null && !entity.getSumcode().isEmpty()){
                    statement.setString(stIndex++, entity.getSumcode());
                }
                if (entity.getBagcode() != null && !entity.getBagcode().isEmpty()){
                    statement.setString(stIndex++, entity.getBagcode());
                }
                if (entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
                    statement.setString(stIndex++, entity.getDelflg());
                }
                if (entity.getActflg() != null && !entity.getActflg().isEmpty()){
                    statement.setString(stIndex++, entity.getActflg());
                }
                if (entity != null && entity.getCre_by() != null && !entity.getCre_by().isEmpty()){
                    statement.setString(stIndex++, entity.getCre_by());
                }


                ResultSet resultSet = statement.executeQuery();

                entityList = new ArrayList<>();

                while (resultSet.next()) {
                    TemtsehAhZardalEntity objEntity = new TemtsehAhZardalEntity();

                    objEntity.setId(resultSet.getLong("id"));
                    objEntity.setForm01key(resultSet.getString("form01key"));
                    objEntity.setCitycode(resultSet.getString("citycode"));
                    objEntity.setSumcode(resultSet.getString("sumcode"));
                    objEntity.setBagcode(resultSet.getString("bagcode"));
                    objEntity.setMalchinner(resultSet.getString("malchinner"));
                    objEntity.setAjil_heseg(resultSet.getString("ajil_heseg"));
                    objEntity.setMal_emch(resultSet.getString("mal_emch"));
                    objEntity.setBusad_humuus(resultSet.getString("busad_humuus"));
                     objEntity.setTechnic_heregsel(resultSet.getString("technic_heregsel"));
                     objEntity.setMeun(resultSet.getString("meun"));
                     objEntity.setMeut(resultSet.getString("meut"));
                     objEntity.setMea(resultSet.getString("mea"));
                     objEntity.setTur_zb(resultSet.getString("tur_zb"));
                     objEntity.setNuhun_olg_zardal(resultSet.getString("nuhun_olg_zardal"));

                    objEntity.setDelflg(resultSet.getString("delflg"));
                    objEntity.setActflg(resultSet.getString("actflg"));
                    objEntity.setCre_at(resultSet.getDate("cre_at"));
                    objEntity.setCre_by(resultSet.getString("cre_by"));
                    objEntity.setMod_at(resultSet.getDate("mod_at"));
                    objEntity.setMod_by(resultSet.getString("mod_by"));
                    objEntity.setRecord_date(resultSet.getDate("record_date"));

                    objEntity.put("cityname", resultSet.getString("cityname"));
                    objEntity.put("sumname", resultSet.getString("sumname"));
                    objEntity.put("horooname", resultSet.getString("horooname"));
                    objEntity.put("index", index++);
                    objEntity.put("strRecordOgnoo", dateFormat.format(objEntity.getRecord_date()));

                    entityList.add(objEntity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return entityList;
    }

    @Override
    public ErrorEntity updateData(TemtsehAhZardalEntity entity) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (entity == null) new ErrorEntity(ErrorType.INFO, Long.valueOf(1059));
        if (entity == null) {
            errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errorEntity.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errorEntity.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errorEntity;
        }

        query = "UPDATE `hospital`.`hform01_temceh_ah_zardal` " +
                "SET     ";

        if (entity.getForm01key() != null && !entity.getForm01key().isEmpty())
            query += " `form01key`=?," ;

        if (entity.getCitycode() != null && !entity.getCitycode().isEmpty())
            query += "`citycode`=?," ;

        if (entity.getSumcode() != null && !entity.getSumcode().isEmpty())
            query += "`sumcode`=?," ;

        if (entity.getBagcode() != null && !entity.getBagcode().isEmpty())
            query += "`bagcode`=?," ;

        if (entity.getMalchinner() != null && !entity.getMalchinner().isEmpty())
            query += "`malchinner`=?," ;

        if (entity.getAjil_heseg() != null && !entity.getAjil_heseg().isEmpty())
            query += "`ajil_heseg`=?,";

        if (entity.getMal_emch() != null && !entity.getMal_emch().isEmpty())
            query += "`mal_emch`=?,";

        if (entity.getBusad_humuus() != null && !entity.getBusad_humuus().isEmpty())
            query += "`busad_humuus`=?,";

        if (entity.getTechnic_heregsel() != null && !entity.getTechnic_heregsel().isEmpty())
            query += "`technic_heregsel`=?,";

        if (entity.getMeun() != null && !entity.getMeun().isEmpty())
            query += "`meun`=?,";

        if (entity.getMeut() != null && !entity.getMeut().isEmpty())
            query += "`meut`=?,";

        if (entity.getMea() != null && !entity.getMea().isEmpty())
            query += "`mea`=?,";

        if (entity.getTur_zb() != null && !entity.getTur_zb().isEmpty())
            query += "`tur_zb`=?,";

        if (entity.getNuhun_olg_zardal() != null && !entity.getNuhun_olg_zardal().isEmpty())
            query += "`nuhun_olg_zardal`=?," ;


        if (entity.getDelflg() != null && !entity.getDelflg().isEmpty())
            query += "`delflg`=?," ;

        if (entity.getActflg() != null && !entity.getActflg().isEmpty())
            query += "`actflg`=?," ;

        if (entity.getRecord_date() != null)
            query += "`record_date`=?, ";

        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";
        query += " WHERE  ID   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (entity.getForm01key() != null && !entity.getForm01key().isEmpty())
                    statement.setString(stIndex++, entity.getForm01key());

                if (entity.getCitycode() != null && !entity.getCitycode().isEmpty())
                    statement.setString(stIndex++, entity.getCitycode());

                if (entity.getSumcode() != null && !entity.getSumcode().isEmpty())
                    statement.setString(stIndex++, entity.getSumcode());

                if (entity.getBagcode() != null && !entity.getBagcode().isEmpty())
                    statement.setString(stIndex++, entity.getBagcode());

                if (entity.getMalchinner() != null && !entity.getMalchinner().isEmpty())
                    statement.setString(stIndex++, entity.getMalchinner());

                if (entity.getAjil_heseg() != null && !entity.getAjil_heseg().isEmpty())
                    statement.setString(stIndex++, entity.getAjil_heseg());

                if (entity.getMal_emch() != null && !entity.getMal_emch().isEmpty())
                    statement.setString(stIndex++, entity.getMal_emch());

                if (entity.getBusad_humuus() != null && !entity.getBusad_humuus().isEmpty())
                    statement.setString(stIndex++, entity.getBusad_humuus());

                if (entity.getTechnic_heregsel() != null && !entity.getTechnic_heregsel().isEmpty())
                    statement.setString(stIndex++, entity.getTechnic_heregsel());

                if (entity.getMeun() != null && !entity.getMeun().isEmpty())
                    statement.setString(stIndex++, entity.getMeun());

                if (entity.getMeut() != null && !entity.getMeut().isEmpty())
                    statement.setString(stIndex++, entity.getMeut());

                if (entity.getMea() != null && !entity.getMea().isEmpty())
                    statement.setString(stIndex++, entity.getMea());

                if (entity.getTur_zb() != null && !entity.getTur_zb().isEmpty())
                    statement.setString(stIndex++, entity.getTur_zb());

                if (entity.getNuhun_olg_zardal() != null && !entity.getNuhun_olg_zardal().isEmpty())
                    statement.setString(stIndex++, entity.getNuhun_olg_zardal());

                if (entity.getDelflg() != null && !entity.getDelflg().isEmpty())
                    statement.setString(stIndex++, entity.getDelflg());

                if (entity.getActflg() != null && !entity.getActflg().isEmpty())
                    statement.setString(stIndex++, entity.getActflg());

                if (entity.getRecord_date() != null)
                    statement.setDate(stIndex++, entity.getRecord_date());

                statement.setDate(stIndex++, entity.getMod_at());
                statement.setString(stIndex++, entity.getMod_by());
                statement.setLong(stIndex++, entity.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                    errorEntity.setErrText("Бичлэг амжилттай засагдлаа.");
                    errorEntity.setErrDesc("Бичлэг амжилттай засагдлаа.");
                    return errorEntity;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }


        errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1061));
        errorEntity.setErrText("Амжилтгүй боллоо. Дахин оролдоно уу.");
        errorEntity.setErrDesc("Амжилтгүй боллоо. Дахин оролдоно уу.");

        return errorEntity;
    }

    @Override
    public ErrorEntity manageData(User user, TemtsehAhZardalEntity entity) {
        Date utilDate = new Date();
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

        try {
            TemtsehAhZardalEntity tmp = new TemtsehAhZardalEntity();

            tmp.setForm01key(entity.getForm01key());

            List<TemtsehAhZardalEntity> list = getListData(tmp);

            String command = "";

            if (list == null){
                command = "INS";
            }
            else if (list.size() == 0){
                command = "INS";
            }
            else if (list.size() > 0){
                command = "EDT";
            }

            entity.setMod_at(sqlDate);
            entity.setMod_by(user.getUsername());


            if (command.equalsIgnoreCase("INS"))
            {
                entity.setDelflg("N");
                entity.setActflg("Y");
                entity.setCre_by(user.getUsername());
                entity.setCre_at(sqlDate);

                errorEntity = insertNewData(entity);
            }
            else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL") ||
                    command.equalsIgnoreCase("ACT"))
            {
                entity.setId(list.get(0).getId());

                if (command.equalsIgnoreCase("DEL")){
                    entity.setDelflg("Y");
                }

                errorEntity = updateData(entity);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        finally {
            if (errorEntity == null){
                errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1002));
                errorEntity.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
                errorEntity.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

                return errorEntity;
            }
        }

        return errorEntity;
    }
}
