package com.hospital.app;


import com.Config;
import com.enumclass.ErrorType;
import com.jolbox.bonecp.BoneCP;
import com.model.hos.City;
import com.model.hos.ErrorEntity;
import com.model.hos.Form08Entity;
import com.model.hos.Sum;
import com.mongodb.BasicDBObject;
import com.rpc.PdfExcelUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public class Form08ServiceImpl implements IForm08Service {
    /**
     * Objects
     */

    private BoneCP boneCP;
    private ErrorEntity errObj;

    public Form08ServiceImpl(BoneCP boneCP) {
        this.boneCP = boneCP;
    }

    @Override
    public ErrorEntity insertNewData(Form08Entity form08) throws SQLException {
        PreparedStatement statement = null;

        if (form08 == null) {
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        String query_login = "INSERT INTO `hospital`.`hform08` " +
                "(`arga_hemjee_ner_mn`, " +
                "`arga_hemjee_ner_en`, " +
                "`ner_mn`, " +
                "`ner_en`, " +
                "`mal_turul_mn`, " +
                "`mal_turul_en`, " +
                "`tuluvluguu_mn`, " +
                "`tuluvluguu_en`, " +
                "`guitsetgel_mn`, " +
                "`guitsetgel_en`, " +
                "`huwi_mn`, " +
                "`huwi_en`, " +
                "`delflg`, " +
                "`actflg`, " +
                "`mod_at`, " +
                "`mod_by`, " +
                "`cre_at`, " +
                "`cre_by`," +
                "`zartsuulsan_hemjee_mn`," +
                "`zartsuulsan_hemjee_en`, " +
                "`aimag`," +
                "`sum`," +
                "`aimag_en`," +
                "`sum_en`, " +
                "`recorddate`)" +
                "VALUES " +
                "(? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?,?, ?, ?, ?, ?, ?, ? )";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, form08.getArgaHemjeeNer());
                statement.setString(2, form08.getArgaHemjeeNer_en());
                statement.setString(3, form08.getNer());
                statement.setString(4, form08.getNer_en());
                statement.setString(5, form08.getMalTurul());
                statement.setString(6, form08.getMalTurul_en());
                statement.setString(7, form08.getTolovlogoo());
                statement.setString(8, form08.getTolovlogoo_en());
                statement.setString(9, form08.getGuitsetgel());
                statement.setString(10, form08.getGuitsetgel_en());
                statement.setString(11, form08.getHuvi());
                statement.setString(12, form08.getHuvi_en());
                statement.setString(13, form08.getDelflg());
                statement.setString(14, form08.getActflg());
                statement.setDate(15, form08.getModAt());
                statement.setString(16, form08.getModby());
                statement.setDate(17, form08.getCreat());
                statement.setString(18, form08.getCreby());
                statement.setString(19, form08.getZartsuulsanHemjee());
                statement.setString(20, form08.getZartsuulsanHemjee_en());
                statement.setString(21, form08.getAimag());
                statement.setString(22, form08.getSum());
                statement.setString(23, form08.getAimag_en());
                statement.setString(24, form08.getSum_en());
                statement.setDate(25, form08.getRecordDate());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));
                    errObj.setErrText("Бичлэг амжилттай хадгалагдлаа.");
                    errObj.setErrDesc("Бичлэг амжилттай хадгалагдлаа.");
                    return errObj;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }

        } catch (Exception ex) {

        }

        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1057));
        errObj.setErrText("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");
        errObj.setErrDesc("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");

        return errObj;
    }

    @Override
    public List<Form08Entity> getListData(Form08Entity form08) throws SQLException {
        int stIndex = 1;
        List<Form08Entity> form08EntitieList = null;

        String query = "SELECT `hform08`.`id`, " +
                "    `hform08`.`arga_hemjee_ner_mn`, " +
                "    `hform08`.`arga_hemjee_ner_en`, " +
                "    `hform08`.`mur_dugaar_mn`, " +
                "    `hform08`.`mur_dugaar_en`, " +
                "    `hform08`.`ner_mn`, " +
                "    `hform08`.`ner_en`, " +
                "    `hform08`.`zartsuulsan_hemjee_mn`, " +
                "    `hform08`.`zartsuulsan_hemjee_en`, " +
                "    `hform08`.`mal_turul_mn`, " +
                "    `hform08`.`mal_turul_en`, " +
                "    `hform08`.`tuluvluguu_mn`, " +
                "    `hform08`.`tuluvluguu_en`, " +
                "    `hform08`.`guitsetgel_mn`, " +
                "    `hform08`.`guitsetgel_en`, " +
                "    `hform08`.`huwi_mn`, " +
                "    `hform08`.`huwi_en`, " +
                "    `hform08`.`delflg`, " +
                "    `hform08`.`actflg`, " +
                "    `hform08`.`mod_at`, " +
                "    `hform08`.`mod_by`, " +
                "    `hform08`.`cre_at`, " +
                "    `hform08`.`cre_by`, " +
                "    `hform08`.`aimag`, " +
                "    `hform08`.`aimag_en`, " +
                "   `hform08`.`recorddate`, " +
                "   `hform08`.`sum`, " +
                "    c.cityname, c.cityname_en " +
                "FROM `hospital`.`hform08` INNER JOIN `h_city` AS c " +
                "ON c.code = `hform08`.aimag " +
                "WHERE ";

        if (form08.getArgaHemjeeNer() != null && !form08.getArgaHemjeeNer().isEmpty()) {
            query += " arga_hemjee_ner_mn = ? AND ";
        }

        if (form08.getArgaHemjeeNer_en() != null && !form08.getArgaHemjeeNer_en().isEmpty()) {
            query += " arga_hemjee_ner_en = ? AND ";
        }

        /* NER */
        if (form08.getNer() != null && !form08.getNer().toString().isEmpty()) {
            query += " ner_mn = ? AND ";
        }
        if (form08.getNer_en() != null && !form08.getNer_en().toString().isEmpty()) {
            query += " ner_en = ? AND ";
        }
        /* Zartsuulsan hemjee */
        if (form08.getZartsuulsanHemjee() != null && !form08.getZartsuulsanHemjee().toString().isEmpty()) {
            query += " zartsuulsan_hemjee_mn = ? AND ";
        }
        if (form08.getZartsuulsanHemjee_en() != null && !form08.getZartsuulsanHemjee_en().toString().isEmpty()) {
            query += " zartsuulsan_hemjee_en = ? AND ";
        }

        /* MAL TURUL */
        if (form08.getMalTurul() != null && !form08.getMalTurul().isEmpty()) {
            query += " mal_turul_mn = ? AND ";
        }
        if (form08.getMalTurul_en() != null && !form08.getMalTurul_en().toString().isEmpty()) {
            query += " mal_turul_en = ? AND ";
        }

        /* Tuluvlugu */
        if (form08.getTolovlogoo() != null && !form08.getTolovlogoo().isEmpty()) {
            query += " tuluvluguu_mn = ? AND ";
        }
        if (form08.getTolovlogoo_en() != null && !form08.getTolovlogoo_en().toString().isEmpty()) {
            query += " tuluvluguu_en = ? AND ";
        }

        /* Guitsetgel */
        if (form08.getGuitsetgel() != null && !form08.getGuitsetgel().isEmpty()) {
            query += " guitsetgel_mn = ? AND ";
        }
        if (form08.getGuitsetgel_en() != null && !form08.getGuitsetgel_en().toString().isEmpty()) {
            query += " guitsetgel_en = ? AND ";
        }

        /* Huwi */
        if (form08.getHuvi() != null && !form08.getHuvi().isEmpty()) {
            query += " huwi_mn = ? AND  ";
        }
        if (form08.getHuvi_en() != null && !form08.getHuvi_en().toString().isEmpty()) {
            query += " huwi_en = ? AND ";
        }
        if (form08 != null && form08.getAimag() != null && !form08.getAimag().isEmpty()) {
            query += " `aimag` = ? AND ";
        }

        if (form08 != null && form08.getRecordDate() != null && form08.getSearchRecordDate() != null) {
            query += " (`recorddate` BETWEEN ? AND ?) AND ";
        }

        if (form08 != null && form08.getCreby() != null && !form08.getCreby().isEmpty()) {
            query += " `hform08`.`cre_by` = ? AND ";
        }

        if (form08 != null && form08.getSum() != null && !form08.getSum().isEmpty()) {
            query += " `hform08`.`sum` = ? AND ";
        }

        query += " `hform08`.`delflg` = 'N' ORDER BY `cre_at` DESC ";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query);
                if (form08.getArgaHemjeeNer() != null && !form08.getArgaHemjeeNer().isEmpty()) {
                    statement.setString(stIndex++, form08.getArgaHemjeeNer());
                }

                if (form08.getArgaHemjeeNer_en() != null && !form08.getArgaHemjeeNer_en().isEmpty()) {
                    statement.setString(stIndex++, form08.getArgaHemjeeNer_en());
                }

            /* NER */
                if (form08.getNer() != null && !form08.getNer().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getNer());
                }
                if (form08.getNer_en() != null && !form08.getNer_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getNer_en());
                }

            /* Zartsuulsan hemjee */
                if (form08.getZartsuulsanHemjee() != null && !form08.getZartsuulsanHemjee().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getZartsuulsanHemjee());
                }
                if (form08.getZartsuulsanHemjee_en() != null && !form08.getZartsuulsanHemjee_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getZartsuulsanHemjee_en());
                }

            /* MAL TURUL */
                if (form08.getMalTurul() != null && !form08.getMalTurul().isEmpty()) {
                    statement.setString(stIndex++, form08.getMalTurul());
                }
                if (form08.getMalTurul_en() != null && !form08.getMalTurul_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getMalTurul_en());
                }

            /* Tuluvlugu */
                if (form08.getTolovlogoo() != null && !form08.getTolovlogoo().isEmpty()) {
                    statement.setString(stIndex++, form08.getTolovlogoo());
                }
                if (form08.getTolovlogoo_en() != null && !form08.getTolovlogoo_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getTolovlogoo_en());
                }

            /* Guitsetgel */
                if (form08.getGuitsetgel() != null && !form08.getGuitsetgel().isEmpty()) {
                    statement.setString(stIndex++, form08.getGuitsetgel());
                }
                if (form08.getGuitsetgel_en() != null && !form08.getGuitsetgel_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getGuitsetgel_en());
                }

            /* Huwi */
                if (form08.getHuvi() != null && !form08.getHuvi().isEmpty()) {
                    statement.setString(stIndex++, form08.getHuvi());
                }
                if (form08.getHuvi_en() != null && !form08.getHuvi_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getHuvi_en());
                }
                if (form08 != null && form08.getAimag() != null && !form08.getAimag().isEmpty()) {
                    statement.setString(stIndex++, form08.getAimag());
                }

                if (form08 != null && form08.getRecordDate() != null && form08.getSearchRecordDate() != null) {
                    statement.setDate(stIndex++, form08.getRecordDate());
                    statement.setDate(stIndex++, form08.getSearchRecordDate());
                }
                if (form08 != null && form08.getCreby() != null && !form08.getCreby().isEmpty()) {
                    statement.setString(stIndex++, form08.getCreby());
                }

                if (form08 != null && form08.getSum() != null && !form08.getSum().isEmpty()) {
                    statement.setString(stIndex++, form08.getSum());
                }

                ResultSet resultSet = statement.executeQuery();

                form08EntitieList = new ArrayList<>();

                IComboService comboService = new ComboServiceImpl(boneCP);
                List<BasicDBObject> cmbType = comboService.getComboVals("MAL", "MN");
                int index = 1;
                while (resultSet.next()) {
                    Form08Entity entity = new Form08Entity();

                    entity.setId(resultSet.getLong("id"));
                    entity.setArgaHemjeeNer(resultSet.getString("arga_hemjee_ner_mn"));
                    entity.setArgaHemjeeNer_en(resultSet.getString("arga_hemjee_ner_en"));
                    entity.setNer(resultSet.getString("ner_mn"));
                    entity.setNer_en(resultSet.getString("ner_en"));
                    entity.setMalTurul(resultSet.getString("mal_turul_mn"));
                    entity.setMalTurul_en(resultSet.getString("mal_turul_en"));
                    entity.setTolovlogoo(resultSet.getString("tuluvluguu_mn"));
                    entity.setTolovlogoo_en(resultSet.getString("tuluvluguu_en"));
                    entity.setGuitsetgel(resultSet.getString("guitsetgel_mn"));
                    entity.setGuitsetgel_en(resultSet.getString("guitsetgel_en"));
                    entity.setHuvi(resultSet.getString("huwi_mn"));
                    entity.setHuvi_en(resultSet.getString("huwi_en"));
                    entity.setDelflg(resultSet.getString("delflg"));
                    entity.setActflg(resultSet.getString("actflg"));
                    entity.setModAt(resultSet.getDate("mod_at"));
                    entity.setModby(resultSet.getString("mod_by"));
                    entity.setCreat(resultSet.getDate("cre_at"));
                    entity.setCreby(resultSet.getString("cre_by"));
                    entity.setZartsuulsanHemjee(resultSet.getString("zartsuulsan_hemjee_mn"));
                    entity.setZartsuulsanHemjee_en(resultSet.getString("zartsuulsan_hemjee_en"));
                    entity.setAimag(resultSet.getString("aimag"));
                    entity.setAimag_en(resultSet.getString("aimag_en"));
                    entity.setAimagNer(resultSet.getString("cityname"));
                    entity.setRecordDate(resultSet.getDate("recorddate"));
                    entity.setSum(resultSet.getString("sum"));
                    entity.setMalTurulNer(getMalTurulNer(cmbType, resultSet.getString("mal_turul_mn")));
                    entity.put("index", index++);

                    form08EntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return form08EntitieList;
    }

    /**
     * Get name
     *
     * @param cmbType
     * @param mal_turul_mn
     * @return
     */
    private String getMalTurulNer(List<BasicDBObject> cmbType, String mal_turul_mn) {
        for (BasicDBObject obj :
                cmbType) {
            if (obj.get("id") != null && mal_turul_mn != null && obj.getString("id").equalsIgnoreCase(mal_turul_mn)) {
                return obj.getString("name");
            }
        }

        return "";
    }

    @Override
    public ErrorEntity updateData(Form08Entity form08) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (form08 == null) {
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        query = "UPDATE `hospital`.`hform08` " +
                "SET     ";

        if (form08.getAimag() != null && !form08.getAimag().isEmpty()) {
            query += " aimag = ?, ";
        }

        if (form08.getArgaHemjeeNer() != null && !form08.getArgaHemjeeNer().isEmpty()) {
            query += " arga_hemjee_ner_mn = ?, ";
        }

        if (form08.getArgaHemjeeNer_en() != null && !form08.getArgaHemjeeNer_en().isEmpty()) {
            query += " arga_hemjee_ner_en = ?, ";
        }

        /* NER */
        if (form08.getNer() != null && !form08.getNer().toString().isEmpty()) {
            query += " ner_mn = ?, ";
        }
        if (form08.getNer_en() != null && !form08.getNer_en().toString().isEmpty()) {
            query += " ner_en = ?, ";
        }
        /* Zartsuulsan hemjee */
        if (form08.getZartsuulsanHemjee() != null && !form08.getZartsuulsanHemjee().toString().isEmpty()) {
            query += " zartsuulsan_hemjee_mn = ?, ";
        }
        if (form08.getZartsuulsanHemjee_en() != null && !form08.getZartsuulsanHemjee_en().toString().isEmpty()) {
            query += " zartsuulsan_hemjee_en = ?, ";
        }

        /* MAL TURUL */
        if (form08.getMalTurul() != null && !form08.getMalTurul().isEmpty()) {
            query += " mal_turul_mn = ?, ";
        }
        if (form08.getMalTurul_en() != null && !form08.getMalTurul_en().toString().isEmpty()) {
            query += " mal_turul_en = ?, ";
        }

        /* Tuluvlugu */
        if (form08.getTolovlogoo() != null && !form08.getTolovlogoo().isEmpty()) {
            query += " tuluvluguu_mn = ?, ";
        }
        if (form08.getTolovlogoo_en() != null && !form08.getTolovlogoo_en().toString().isEmpty()) {
            query += " tuluvluguu_en = ?, ";
        }

        /* Guitsetgel */
        if (form08.getGuitsetgel() != null && !form08.getGuitsetgel().isEmpty()) {
            query += " guitsetgel_mn = ?, ";
        }
        if (form08.getGuitsetgel_en() != null && !form08.getGuitsetgel_en().toString().isEmpty()) {
            query += " guitsetgel_en = ?, ";
        }

        /* Huwi */
        if (form08.getHuvi() != null && !form08.getHuvi().isEmpty()) {
            query += " huwi_mn = ?, ";
        }
        if (form08.getHuvi_en() != null && !form08.getHuvi_en().toString().isEmpty()) {
            query += " huwi_en = ?, ";
        }
        /* Delete flag */
        if (form08.getDelflg() != null && !form08.getDelflg().toString().isEmpty()) {
            query += " delflg = ?, ";
        }

        /* Active flag */
        if (form08.getActflg() != null && !form08.getActflg().toString().isEmpty()) {
            query += " actflg = ?, ";
        }

        if (form08 != null && form08.getRecordDate() != null) {
            query += " `recorddate` = ?, ";
        }

        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";
        query += " WHERE  ID   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (form08.getAimag() != null && !form08.getAimag().isEmpty()) {
                    statement.setString(stIndex++, form08.getAimag());
                }
                if (form08.getArgaHemjeeNer() != null && !form08.getArgaHemjeeNer().isEmpty()) {
                    statement.setString(stIndex++, form08.getArgaHemjeeNer());
                }

                if (form08.getArgaHemjeeNer_en() != null && !form08.getArgaHemjeeNer_en().isEmpty()) {
                    statement.setString(stIndex++, form08.getArgaHemjeeNer_en());
                }

            /* NER */
                if (form08.getNer() != null && !form08.getNer().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getNer());
                }
                if (form08.getNer_en() != null && !form08.getNer_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getNer_en());
                }

            /* Zartsuulsan hemjee */
                if (form08.getZartsuulsanHemjee() != null && !form08.getZartsuulsanHemjee().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getZartsuulsanHemjee());
                }
                if (form08.getZartsuulsanHemjee_en() != null && !form08.getZartsuulsanHemjee_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getZartsuulsanHemjee_en());
                }

            /* MAL TURUL */
                if (form08.getMalTurul() != null && !form08.getMalTurul().isEmpty()) {
                    statement.setString(stIndex++, form08.getMalTurul());
                }
                if (form08.getMalTurul_en() != null && !form08.getMalTurul_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getMalTurul_en());
                }

            /* Tuluvlugu */
                if (form08.getTolovlogoo() != null && !form08.getTolovlogoo().isEmpty()) {
                    statement.setString(stIndex++, form08.getTolovlogoo());
                }
                if (form08.getTolovlogoo_en() != null && !form08.getTolovlogoo_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getTolovlogoo_en());
                }

            /* Guitsetgel */
                if (form08.getGuitsetgel() != null && !form08.getGuitsetgel().isEmpty()) {
                    statement.setString(stIndex++, form08.getGuitsetgel());
                }
                if (form08.getGuitsetgel_en() != null && !form08.getGuitsetgel_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getGuitsetgel_en());
                }

            /* Huwi */
                if (form08.getHuvi() != null && !form08.getHuvi().isEmpty()) {
                    statement.setString(stIndex++, form08.getHuvi());
                }
                if (form08.getHuvi_en() != null && !form08.getHuvi_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getHuvi_en());
                }
            /* Delete flag */
                if (form08.getDelflg() != null && !form08.getDelflg().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getDelflg());
                }

            /* Active flag */
                if (form08.getActflg() != null && !form08.getActflg().toString().isEmpty()) {
                    statement.setString(stIndex++, form08.getActflg());
                }
                if (form08 != null && form08.getRecordDate() != null) {
                    statement.setDate(stIndex++, form08.getRecordDate());
                }

                statement.setDate(stIndex++, form08.getModAt());
                statement.setString(stIndex++, form08.getModby());
                statement.setLong(stIndex++, form08.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                    errObj.setErrText("Бичлэг амжилттай засагдлаа.");
                    errObj.setErrDesc("Бичлэг амжилттай засагдлаа.");
                    return errObj;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1061));
        errObj.setErrText("Амжилтгүй боллоо. Дахин оролдоно уу.");
        errObj.setErrDesc("Амжилтгүй боллоо. Дахин оролдоно уу.");

        return errObj;
    }

    @Override
    public List<BasicDBObject> getPieChartData(Form08Entity form08Entity) throws SQLException {
        List<BasicDBObject> form08EntitieList = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(form08Entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(form08Entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }


        String query_sel = "SELECT `mal_turul_mn`, COUNT(`id`) AS cnt, `hform08`.`aimag`, `hform08`.`sum`  " +
                "FROM `hospital`.`hform08`  " +
                "WHERE   ";

        if (form08Entity != null && !form08Entity.getAimag().isEmpty()) {
            query_sel += " `hform08`.`aimag` = ? AND ";
        }
        if (form08Entity != null && !form08Entity.getSum().isEmpty()) {
            query_sel += " `hform08`.`sum` = ? AND ";
        }
        if (form08Entity != null && !form08Entity.getDelflg().isEmpty()) {
            query_sel += " `hform08`.`delflg` = ? AND ";
        }
        if (form08Entity != null && !form08Entity.getActflg().isEmpty()) {
            query_sel += " `hform08`.`actflg` = ? AND ";
        }
        if (form08Entity != null && !form08Entity.getMalTurul().isEmpty()) {
            query_sel += " `hform08`.`mal_turul_mn` = ? AND ";
        }
        if (form08Entity != null && !form08Entity.getCreby().isEmpty()) {
            query_sel += " `hform08`.`cre_by` = ? AND ";
        }
        if (form08Entity != null && form08Entity.getRecordDate() != null && form08Entity.getSearchRecordDate() != null) {
            query_sel += " (`recorddate` BETWEEN ? AND ?) AND ";
        }

        query_sel += " `id` > 0  GROUP BY `hform08`.`mal_turul_mn`";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                int stindex = 1;

                if (form08Entity != null && !form08Entity.getAimag().isEmpty()) {
                    statement.setString(stindex++, form08Entity.getAimag());
                }
                if (form08Entity != null && !form08Entity.getSum().isEmpty()) {
                    statement.setString(stindex++, form08Entity.getSum());
                }
                if (form08Entity != null && !form08Entity.getDelflg().isEmpty()) {
                    statement.setString(stindex++, form08Entity.getDelflg());
                }
                if (form08Entity != null && !form08Entity.getActflg().isEmpty()) {
                    statement.setString(stindex++, form08Entity.getActflg());
                }
                if (form08Entity != null && !form08Entity.getMalTurul().isEmpty()) {
                    statement.setString(stindex++, form08Entity.getMalTurul());
                }
                if (form08Entity != null && !form08Entity.getCreby().isEmpty()) {
                    statement.setString(stindex++, form08Entity.getCreby());
                }
                if (form08Entity != null && form08Entity.getRecordDate() != null && form08Entity.getSearchRecordDate() != null) {
                    statement.setDate(stindex++, form08Entity.getRecordDate());
                    statement.setDate(stindex++, form08Entity.getSearchRecordDate());
                }

                ResultSet resultSet = statement.executeQuery();

                form08EntitieList = new ArrayList<>();

                IComboService comboService = new ComboServiceImpl(boneCP);
                List<BasicDBObject> cmbType = comboService.getComboVals("MAL", "MN");

                while (resultSet.next()) {
                    BasicDBObject entity = new BasicDBObject();

                    entity.put("malturulner", getMalTurulNer(cmbType, resultSet.getString("mal_turul_mn")));
                    entity.put("malturul", resultSet.getString("mal_turul_mn"));
                    entity.put("count", resultSet.getString("cnt"));
                    entity.put("aimag", resultSet.getString("aimag"));
                    entity.put("sum", resultSet.getString("sum"));

                    form08EntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return form08EntitieList;
    }//


    @Override
    public ErrorEntity exportReport(String file, Form08Entity form08) throws SQLException {
        ErrorEntity errorEntity = null;

        String fileName = "hform08";

        try (Connection connection = boneCP.getConnection()) {
            String JRXML_PATH = Config.getJrxmlPath();
            String REPORT_PATH = Config.getReportPath();

            try {
                Map parameters = new HashMap();

                String reportTitle = "ПАРАЗИТТАХ ӨВЧНӨӨС СЭРГИЙЛЭХ АРГА ХЭМЖЭЭНИЙ $$YEAR$$ ОНЫ $$SEASON$$ -Р УЛИРАЛ, ЖИЛИЙН ЭЦСИЙН МЭДЭЭ",
                        currentYear = "$$YEAR$$ он №....",
                        printDate = "$$YEAR$$ оны $$MONTH$$ сарын $$DAY$$";

                Calendar now = Calendar.getInstance();

                int year = now.get(Calendar.YEAR);
                int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
                int day = now.get(Calendar.DAY_OF_MONTH);

                printDate = printDate.replace("$$YEAR$$", String.valueOf(year));
                printDate = printDate.replace("$$MONTH$$", String.valueOf(month));
                printDate = printDate.replace("$$DAY$$", String.valueOf(day));

                currentYear = currentYear.replace("$$YEAR$$", String.valueOf(year));

                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(new Date(form08.getRecordDate().getTime()));

                reportTitle = reportTitle.replace("$$YEAR$$", String.valueOf(calendar1.get(Calendar.YEAR)));

                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(new Date(form08.getSearchRecordDate().getTime()));

                if (calendar2.get(Calendar.MONTH) < 4) {
                    reportTitle = reportTitle.replace("$$SEASON$$", "1");
                } else if (calendar2.get(Calendar.MONTH) < 7) {
                    reportTitle = reportTitle.replace("$$SEASON$$", "2");
                } else if (calendar2.get(Calendar.MONTH) < 10) {
                    reportTitle = reportTitle.replace("$$SEASON$$", "3");
                } else if (calendar2.get(Calendar.MONTH) <= 12) {
                    reportTitle = reportTitle.replace("$$SEASON$$", "4");
                }
                String sumner = "";
                if (form08.getAimag() == null || form08.getAimagNer() == null || form08.getAimag().isEmpty() || form08.getAimagNer().isEmpty()) {
                    sumner = "";
                } else {
                    if (form08.getSumNer() != null && form08.getSumNer().isEmpty()) {
                        ISumService sumService = new SumServiceImpl(boneCP);
                        Sum tmpsum = new Sum();
                        tmpsum.setSumcode(form08.getSum());
                        List<Sum> sumList = sumService.selectAll(tmpsum);
                        if (sumList.size() > 0) {
                            sumner = sumList.get(0).getSumname();
                        }
                    } else {
                        sumner = form08.getSumNer();
                    }
                }

                parameters.put("start_date", form08.getRecordDate());
                parameters.put("end_date", form08.getSearchRecordDate());
                parameters.put("citycode", (form08.getAimag() != null && !form08.getAimag().isEmpty()) ? form08.getAimag() : null);
                parameters.put("sumcode", (form08.getSum() != null && !form08.getSum().isEmpty()) ? form08.getSum() : null);
                parameters.put("currentyear", currentYear);
                if (form08.getAimag() != null || form08.getAimagNer() != null || form08.getAimag().isEmpty() || form08.getAimagNer().isEmpty()) {
                    parameters.put("aimagner", (form08.getAimagNer() == null) ? "" : form08.getAimagNer());
                } else {
                    ICityService cityService = new CityServiceImpl(boneCP);
                    City tmpCity = new City();
                    tmpCity.setCode(form08.getAimag());
                    List<City> clist = cityService.selectAll(tmpCity);

                    if (clist != null && clist.size() == 1) {
                        parameters.put("aimagner", clist.get(0).getCityname());
                    } else {
                        parameters.put("aimagner", (form08.getAimagNer() == null) ? "" : form08.getAimagNer());
                    }
                }

                if (form08.getAimag() == null || form08.getAimagNer() == null || form08.getAimag().isEmpty() || form08.getAimagNer().isEmpty()) {
                    parameters.put("sumner", (form08.getSumNer() == null) ? "" : form08.getSumNer());
                } else {
                    ISumService sumService = new SumServiceImpl(boneCP);
                    Sum tmpSum = new Sum();
                    tmpSum.setCitycode(form08.getAimag());
                    tmpSum.setSumcode(form08.getSum());
                    List<Sum> sumList = sumService.selectAll(tmpSum);

                    if (sumList != null && sumList.size() == 1){
                        parameters.put("sumner", sumList.get(0).getSumname());
                    }
                    else{
                        parameters.put("sumner", (form08.getSumNer() == null) ? "" : form08.getSumNer());
                    }
                }


                parameters.put("printdate", printDate);
                parameters.put("reporttitle", reportTitle);
                parameters.put("creby", (form08.getCreby() != null && !form08.getCreby().isEmpty()) ? form08.getCreby() : null);

                parameters.put("mal_turul_mn", (form08.getMalTurul() != null && !form08.getMalTurul().isEmpty()) ? form08.getMalTurul() : null);
                parameters.put("tuluvluguu_mn", (form08.getTolovlogoo() != null && !form08.getTolovlogoo().isEmpty()) ? form08.getTolovlogoo() : null);
                parameters.put("guitsetgel_mn", (form08.getGuitsetgel() != null && !form08.getGuitsetgel().isEmpty()) ? form08.getGuitsetgel() : null);
                parameters.put("huwi_mn", (form08.getHuvi() != null && !form08.getHuvi().isEmpty()) ? form08.getHuvi() : 0);


                PdfExcelUtil.generateReport(file, JRXML_PATH, REPORT_PATH, fileName, parameters, connection);

                System.out.println("File Generated");
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (connection != null) connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1001));

            if (file.equals("pdf")) {
                errorEntity.setErrText(fileName + ".pdf");
            } else if (file.equals("xls")) {
                errorEntity.setErrText(fileName + ".xls");
            }
        }
        return errorEntity;
    }

    @Override
    public BasicDBObject getColumnChartData(Form08Entity form08Entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        //dateFormat.parse(dv).getTime()
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(form08Entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(form08Entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }

        try {
            IComboService comboService = new ComboServiceImpl(boneCP);
            List<Integer[]> rowdataList = new ArrayList<>();
            List<BasicDBObject> cmbType = comboService.getComboVals("MAL", "MN");

            basicDBObject.put("coldata", cmbType);

            //Saraar n haruulah
            int month = calendar1.get(Calendar.MONTH) + 1;
            int year1 = calendar1.get(Calendar.YEAR);
            int year2 = calendar2.get(Calendar.YEAR);

            try (Connection connection = boneCP.getConnection()) {
                String query_sel = "SELECT `mal_turul_mn`, COUNT(`id`) AS cnt, `hform08`.`aimag`, `hform08`.`sum`  " +
                        "FROM `hospital`.`hform08`  " +
                        "WHERE  ";

                if (form08Entity != null && !form08Entity.getAimag().isEmpty()) {
                    query_sel += " `hform08`.`aimag` = ? AND ";
                }
                if (form08Entity != null && !form08Entity.getSum().isEmpty()) {
                    query_sel += " `hform08`.`sum` = ? AND ";
                }
                if (form08Entity != null && !form08Entity.getDelflg().isEmpty()) {
                    query_sel += " `hform08`.`delflg` = ? AND ";
                }
                if (form08Entity != null && !form08Entity.getActflg().isEmpty()) {
                    query_sel += " `hform08`.`actflg` = ? AND ";
                }
                if (form08Entity != null && !form08Entity.getMalTurul().isEmpty()) {
                    query_sel += " `hform08`.`mal_turul_mn` = ? AND ";
                }
                if (form08Entity != null && !form08Entity.getCreby().isEmpty()) {
                    query_sel += " `hform08`.`cre_by` = ? AND ";
                }
                if (form08Entity != null && form08Entity.getRecordDate() != null && form08Entity.getSearchRecordDate() != null) {
                    query_sel += " (`recorddate` BETWEEN ? AND ?) AND ";
                }

                query_sel += " `id` > 0  GROUP BY `hform08`.`mal_turul_mn` ORDER BY `hform08`.`mal_turul_mn`";

                do {
                    //do process
                    Integer[] rowdata = new Integer[cmbType.size() + 1];
                    //
                    //rowdata[0] = month + " -р сар";
                    rowdata[0] = month;

                    try {
                        PreparedStatement statement = connection.prepareStatement(query_sel);

                        java.sql.Date stdate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-01 00:00:00").getTime());

                        java.sql.Date enddate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-31 00:00:00").getTime());

                        if (month == 12 && year1 < year2) {
                            month = 1;
                            year1++;
                        } else {
                            month++;
                        }

                        int stindex = 1;

                        if (form08Entity != null && !form08Entity.getAimag().isEmpty()) {
                            statement.setString(stindex++, form08Entity.getAimag());
                        }
                        if (form08Entity != null && !form08Entity.getSum().isEmpty()) {
                            statement.setString(stindex++, form08Entity.getSum());
                        }
                        if (form08Entity != null && !form08Entity.getDelflg().isEmpty()) {
                            statement.setString(stindex++, form08Entity.getDelflg());
                        }
                        if (form08Entity != null && !form08Entity.getActflg().isEmpty()) {
                            statement.setString(stindex++, form08Entity.getActflg());
                        }
                        if (form08Entity != null && !form08Entity.getMalTurul().isEmpty()) {
                            statement.setString(stindex++, form08Entity.getMalTurul());
                        }
                        if (form08Entity != null && !form08Entity.getCreby().isEmpty()) {
                            statement.setString(stindex++, form08Entity.getCreby());
                        }
                        if (form08Entity != null && form08Entity.getRecordDate() != null && form08Entity.getSearchRecordDate() != null) {
                            statement.setDate(stindex++, stdate);
                            statement.setDate(stindex++, enddate);
                        }


                        ResultSet resultSet = statement.executeQuery();

                        HashMap<String, String> hashMap = new HashMap<>();

                        while (resultSet.next()) {
                            String malturul = resultSet.getString("mal_turul_mn");
                            String count = resultSet.getString("cnt");

                            hashMap.put(malturul, count);
                        }//end while

                        for (int i = 1; i <= cmbType.size(); i++) {
                            //rowdata[i] = (hashMap.get(cmbType.get(i-1).getString("id")) == null) ? new Random().nextInt(1000) : Integer.parseInt( hashMap.get(cmbType.get(i-1).getString("id")));
                            rowdata[i] = (hashMap.get(cmbType.get(i - 1).getString("id")) == null) ? 0 : Integer.parseInt(hashMap.get(cmbType.get(i - 1).getString("id")));
                        }

                        rowdataList.add(rowdata);

                        if (statement != null) statement.close();
                        if (resultSet != null) resultSet.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();

                        break;
                    }

                    ///
                } while (month <= calendar2.get(Calendar.MONTH) + 1);

                basicDBObject.put("rowdata", rowdataList);

                connection.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }

    @Override
    public BasicDBObject getColHighChartData(Form08Entity form08Entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();
        List<String> categories = new ArrayList<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(form08Entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(form08Entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }

        try {
            IComboService comboService = new ComboServiceImpl(boneCP);
            List<BasicDBObject> cmbType = comboService.getComboNames("MAL", "MN");

            //Saraar n haruulah
            int month = calendar1.get(Calendar.MONTH) + 1;
            int year1 = calendar1.get(Calendar.YEAR);
            int year2 = calendar2.get(Calendar.YEAR);

            try (Connection connection = boneCP.getConnection()) {
                String query_sel = "SELECT `mal_turul_mn`, COUNT(`id`) AS cnt, `hform08`.`aimag`, `hform08`.`sum`  " +
                        "FROM `hospital`.`hform08`  " +
                        "WHERE  ";

                if (form08Entity != null && !form08Entity.getAimag().isEmpty()) {
                    query_sel += " `hform08`.`aimag` = ? AND ";
                }
                if (form08Entity != null && !form08Entity.getSum().isEmpty()) {
                    query_sel += " `hform08`.`sum` = ? AND ";
                }
                if (form08Entity != null && !form08Entity.getDelflg().isEmpty()) {
                    query_sel += " `hform08`.`delflg` = ? AND ";
                }
                if (form08Entity != null && !form08Entity.getActflg().isEmpty()) {
                    query_sel += " `hform08`.`actflg` = ? AND ";
                }
                if (form08Entity != null && !form08Entity.getMalTurul().isEmpty()) {
                    query_sel += " `hform08`.`mal_turul_mn` = ? AND ";
                }
                if (form08Entity != null && !form08Entity.getCreby().isEmpty()) {
                    query_sel += " `hform08`.`cre_by` = ? AND ";
                }
                if (form08Entity != null && form08Entity.getRecordDate() != null && form08Entity.getSearchRecordDate() != null) {
                    query_sel += " (`recorddate` BETWEEN ? AND ?) AND ";
                }

                query_sel += " `id` > 0  GROUP BY `hform08`.`mal_turul_mn` ORDER BY `hform08`.`mal_turul_mn`";

                do {
                    //do process
                    categories.add(year1 + "-" + month);

                    try {
                        PreparedStatement statement = connection.prepareStatement(query_sel);

                        java.sql.Date stdate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-01 00:00:00").getTime());

                        java.sql.Date enddate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-31 00:00:00").getTime());

                        if (month == 12 && year1 < year2) {
                            month = 1;
                            year1++;
                        } else {
                            month++;
                        }

                        int stindex = 1;

                        if (form08Entity != null && !form08Entity.getAimag().isEmpty()) {
                            statement.setString(stindex++, form08Entity.getAimag());
                        }
                        if (form08Entity != null && !form08Entity.getSum().isEmpty()) {
                            statement.setString(stindex++, form08Entity.getSum());
                        }
                        if (form08Entity != null && !form08Entity.getDelflg().isEmpty()) {
                            statement.setString(stindex++, form08Entity.getDelflg());
                        }
                        if (form08Entity != null && !form08Entity.getActflg().isEmpty()) {
                            statement.setString(stindex++, form08Entity.getActflg());
                        }
                        if (form08Entity != null && !form08Entity.getMalTurul().isEmpty()) {
                            statement.setString(stindex++, form08Entity.getMalTurul());
                        }
                        if (form08Entity != null && !form08Entity.getCreby().isEmpty()) {
                            statement.setString(stindex++, form08Entity.getCreby());
                        }
                        if (form08Entity != null && form08Entity.getRecordDate() != null && form08Entity.getSearchRecordDate() != null) {
                            statement.setDate(stindex++, stdate);
                            statement.setDate(stindex++, enddate);
                        }


                        ResultSet resultSet = statement.executeQuery();

                        HashMap<String, String> hashMap = new HashMap<>();

                        while (resultSet.next()) {
                            String malturul = resultSet.getString("mal_turul_mn");
                            String count = resultSet.getString("cnt");

                            hashMap.put(malturul, count);
                        }//end while

                        List<Double> list = null;

                        for (int i = 0; i < cmbType.size(); i++) {
                            //rowdata[i] = (hashMap.get(cmbType.get(i - 1).getString("id")) == null) ? 0 : Integer.parseInt(hashMap.get(cmbType.get(i - 1).getString("id")));
                            BasicDBObject obj = cmbType.get(i);

                            if (obj.get("data") == null){
                                //new record
                                list = new ArrayList<>();
                                String val = hashMap.get(obj.getString("id"));
                                if (val == null) list.add(0.0);
                                else if (val.isEmpty()) list.add(0.0);
                                else list.add(Double.parseDouble(val));
                                obj.put("data", list);
                            }else{
                                //already exist
                                list = (List<Double>) obj.get("data");
                                String val = hashMap.get(obj.getString("id"));
                                if (val == null) list.add(0.0);
                                else if (val.isEmpty()) list.add(0.0);
                                else list.add(Double.parseDouble(val));
                            }
                        }

                        if (statement != null) statement.close();
                        if (resultSet != null) resultSet.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();

                        break;
                    }

                    ///
                } while ((month <= calendar2.get(Calendar.MONTH) + 1 && year1 <= year2) || year1 < year2);

                basicDBObject.put("categories", categories);
                basicDBObject.put("series", cmbType);

                connection.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }

    @Override
    public BasicDBObject getPieHighChartData(Form08Entity form08Entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(form08Entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(form08Entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }


        String query_sel = "SELECT `mal_turul_mn`, COUNT(`id`) AS cnt, `hform08`.`aimag`, `hform08`.`sum`  " +
                "FROM `hospital`.`hform08`  " +
                "WHERE   ";

        if (form08Entity != null && !form08Entity.getAimag().isEmpty()) {
            query_sel += " `hform08`.`aimag` = ? AND ";
        }
        if (form08Entity != null && !form08Entity.getSum().isEmpty()) {
            query_sel += " `hform08`.`sum` = ? AND ";
        }
        if (form08Entity != null && !form08Entity.getDelflg().isEmpty()) {
            query_sel += " `hform08`.`delflg` = ? AND ";
        }
        if (form08Entity != null && !form08Entity.getActflg().isEmpty()) {
            query_sel += " `hform08`.`actflg` = ? AND ";
        }
        if (form08Entity != null && !form08Entity.getMalTurul().isEmpty()) {
            query_sel += " `hform08`.`mal_turul_mn` = ? AND ";
        }
        if (form08Entity != null && !form08Entity.getCreby().isEmpty()) {
            query_sel += " `hform08`.`cre_by` = ? AND ";
        }
        if (form08Entity != null && form08Entity.getRecordDate() != null && form08Entity.getSearchRecordDate() != null) {
            query_sel += " (`recorddate` BETWEEN ? AND ?) AND ";
        }

        query_sel += " `id` > 0  GROUP BY `hform08`.`mal_turul_mn`";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                int stindex = 1;

                if (form08Entity != null && !form08Entity.getAimag().isEmpty()) {
                    statement.setString(stindex++, form08Entity.getAimag());
                }
                if (form08Entity != null && !form08Entity.getSum().isEmpty()) {
                    statement.setString(stindex++, form08Entity.getSum());
                }
                if (form08Entity != null && !form08Entity.getDelflg().isEmpty()) {
                    statement.setString(stindex++, form08Entity.getDelflg());
                }
                if (form08Entity != null && !form08Entity.getActflg().isEmpty()) {
                    statement.setString(stindex++, form08Entity.getActflg());
                }
                if (form08Entity != null && !form08Entity.getMalTurul().isEmpty()) {
                    statement.setString(stindex++, form08Entity.getMalTurul());
                }
                if (form08Entity != null && !form08Entity.getCreby().isEmpty()) {
                    statement.setString(stindex++, form08Entity.getCreby());
                }
                if (form08Entity != null && form08Entity.getRecordDate() != null && form08Entity.getSearchRecordDate() != null) {
                    statement.setDate(stindex++, form08Entity.getRecordDate());
                    statement.setDate(stindex++, form08Entity.getSearchRecordDate());
                }

                ResultSet resultSet = statement.executeQuery();

                IComboService comboService = new ComboServiceImpl(boneCP);
                List<BasicDBObject> cmbType = comboService.getComboNames("MAL", "MN");
                List<BasicDBObject> list = new ArrayList<>();

                while (resultSet.next()) {
                    BasicDBObject entity = new BasicDBObject();

                    entity.put("name", getMalTurulNer(cmbType, resultSet.getString("mal_turul_mn")));
                    entity.put("y", resultSet.getDouble("cnt"));

                    list.add(entity);
                }

                basicDBObject.put("data", list);

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }
}
