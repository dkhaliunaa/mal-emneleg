package com.hospital.app;

import com.jolbox.bonecp.BoneCP;
import com.model.Role;
import com.model.User;
import com.model.hos.MenuEntity;
import com.model.hos.MenuRoles;
import com.mongodb.BasicDBObject;

import java.util.ArrayList;
import java.util.List;

public class CheckUserRoleControl {
    private BoneCP boneCP;
    private IRoleService roleService;
    private IMenuRolesService service;
    private IMenuService menuService;

    public CheckUserRoleControl(BoneCP boneCP) throws Exception{
        this.boneCP = boneCP;
    }

    /**
     * Check user role
     *
     * @param user
     * @param menuId
     * @return
     * @throws Exception
     */
    public boolean checkRole(User user, String menuId, int quality) throws Exception{
        boolean result = false;
        try{
            if (user == null || menuId == null) return false;

            //Check User Role Quality
            Role role = new Role();
            role.setCode(user.getRoleCode());
            roleService = new RoleServiceImpl(this.boneCP);
            List<Role> userRoles = roleService.selectAll(role);
            if (userRoles == null){
                return false;
            }

            if(userRoles.size() <= 0 ){
                return false;
            }

            if (userRoles.get(0).getQuality() < quality){
                return false;
            }

            if (user.getRoleCode() != null && user.getRoleCode().equals("SUPERADMIN")){
                return true;
            }

            service = new MenuRolesServiceImpl(this.boneCP);

            MenuRoles menuRoles = new MenuRoles();
            menuRoles.setFormId(menuId);
            menuRoles.setRolecode(user.getRoleCode());

            List<MenuRoles> userMenuList = service.selectAll(menuRoles);

            if (userMenuList.size() > 0) result = true;
            else{
                result = checkDefth(user.getRoleCode(), menuRoles);
            }
        }catch (Exception ex){
            ex.printStackTrace();
            result = false;
        }

        return result;
    }//end of function


    private boolean checkDefth(String roleId, MenuRoles menuRoles) throws Exception{
        boolean result = false;

        try{
            Role role = new Role();
            role.setParent(roleId);
            List<Role> roles = roleService.selectAll(role);

            if (roles != null){
                for (Role child: roles){
                    menuRoles.setRolecode(child.getCode());

                    List<MenuRoles> userMenuList = service.selectAll(menuRoles);

                    if (userMenuList.size() > 0){
                        return true;
                    }else{
                        result = checkDefth(child.getCode(), menuRoles);
                    }

                    if (result) break;
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return result;
    }

    public List<BasicDBObject> getUserAccessMenuList(User user) {
        List<BasicDBObject> objectList = new ArrayList<>();
        try{
            List<MenuRoles> userMenuList = null;

            if (user == null) return null;

            service = new MenuRolesServiceImpl(this.boneCP);
            menuService = new MenuServiceImpl(this.boneCP);

            MenuRoles menuRoles = new MenuRoles();
            MenuEntity menuEntity = new MenuEntity();

            if (user.getRoleCode() != null && user.getRoleCode().equals("SUPERADMIN")){
                List<MenuEntity> menuEntities = menuService.selectAll(menuEntity);

                if (menuEntities != null){
                    for (MenuEntity mr :
                            menuEntities) {
                        BasicDBObject bdb = new BasicDBObject();

                        bdb.put("id", mr.getMenuId());

                        objectList.add(bdb);
                    }
                }
            }else{
                userMenuList = service.selectAll(menuRoles);

                if (userMenuList != null){
                    for (MenuRoles mr :
                            userMenuList) {
                        BasicDBObject bdb = new BasicDBObject();

                        bdb.put("id", mr.getMenuId());

                        objectList.add(bdb);
                    }
                }
            }


        }catch (Exception ex){
            ex.printStackTrace();
        }
        return objectList;
    }
}
