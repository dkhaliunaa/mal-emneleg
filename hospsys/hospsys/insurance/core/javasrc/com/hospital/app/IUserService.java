package com.hospital.app;

import com.model.hos.ErrorEntity;
import com.model.User;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by 24be10264 on 8/18/2017.
 */
public interface IUserService {

    /**
     * Burtgeltei baigaa buh hereglegch nariin jagsaalt
     *
     * @return
     * @throws Exception
     */
    List<User> getUserList(User user) throws Exception;

    /**
     * Shineer user burtgeh
     *
     * @param newuser
     * @return
     * @throws Exception
     */
    ErrorEntity insertUser(User newuser) throws Exception;

    /**
     * Update user informations
     *
     * @param upuser
     * @return
     * @throws SQLException
     */
    ErrorEntity updateUserData(User upuser) throws SQLException;


    Integer getUsersCount()throws SQLException;
}
