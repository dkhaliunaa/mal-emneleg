package com.hospital.app;

import com.model.hos.ErrorEntity;
import com.model.hos.MenuRoles;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by 24be10264 on 9/13/2017.
 */
public interface IMenuRolesService {

    /**
     * Fetch
     *
     * @param entity
     * @return
     * @throws SQLException
     */
    List<MenuRoles> selectAll(MenuRoles entity) throws SQLException;

    /**
     * Insert
     *
     * @param entity
     * @return
     * @throws SQLException
     */
    ErrorEntity insertData(MenuRoles entity) throws SQLException;

    /**
     * Update
     *
     * @param entity
     * @return
     * @throws SQLException
     */
    ErrorEntity updateData(MenuRoles entity) throws SQLException;
}
