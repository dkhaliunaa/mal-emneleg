package com.hospital.app;

import com.model.hos.ErrorEntity;
import com.model.hos.Form09Entity;
import com.mongodb.BasicDBObject;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public interface IForm09Service {

    /**
     * Insert new data
     *
     *
     * @param form09
     * @return
     * @throws Exception
     */
    ErrorEntity insertNewData(Form09Entity form09) throws SQLException;

    /**
     * Select all
     *
     * @param form09
     * @return
     * @throws SQLException
     */
    List<Form09Entity> getListData(Form09Entity form09) throws SQLException;


    /**
     * update form data
     *
     * @param form09
     * @return
     * @throws SQLException
     */
    ErrorEntity updateData(Form09Entity form09) throws SQLException;


    List<BasicDBObject> getPieChartData(Form09Entity form09Entity) throws SQLException;

    ErrorEntity exportReport(String file, Form09Entity form09) throws SQLException;

    BasicDBObject getColumnChartData(Form09Entity entity) throws SQLException;

    BasicDBObject getColHighChartData(Form09Entity form09Entity) throws SQLException;

    BasicDBObject getPieHighChartData(Form09Entity form09Entity) throws SQLException;
}
