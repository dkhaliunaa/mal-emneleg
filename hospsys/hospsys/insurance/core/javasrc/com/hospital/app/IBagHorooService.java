package com.hospital.app;

import com.model.hos.BagHorooEntity;
import com.model.hos.ErrorEntity;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by 24be10264 on 9/27/2017.
 */
public interface IBagHorooService {
    /**
     * Fetch
     *
     * @param selbagHoroo
     * @return
     * @throws SQLException
     */
    List<BagHorooEntity> selectAll(BagHorooEntity selbagHoroo) throws SQLException;

    /**
     * Insert
     *
     * @param insBagHoroo
     * @return
     * @throws SQLException
     */
    ErrorEntity insertData(BagHorooEntity insBagHoroo) throws SQLException;

    /**
     * Update
     *
     * @param updateBagHoroo
     * @return
     * @throws SQLException
     */
    ErrorEntity updateData(BagHorooEntity updateBagHoroo) throws SQLException;
}
