package com.hospital.app;

import com.enumclass.ErrorType;
import com.jolbox.bonecp.BoneCP;
import com.model.hos.ErrorEntity;
import com.model.hos.MenuRoles;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 24be10264 on 9/13/2017.
 */
public class MenuRolesServiceImpl implements IMenuRolesService {

    private BoneCP boneCP;


    public MenuRolesServiceImpl(BoneCP boneCP) {
        this.boneCP = boneCP;
    }


    @Override
    public List<MenuRoles> selectAll(MenuRoles entity) throws SQLException {
        int stIndex = 1;
        List<MenuRoles> menuRolesList = null;

        String query_sel = "SELECT c.`id`, " +
                "    c.`rolecode`, " +
                "    c.`menuid`, " +
                "    c.`cre_at`, " +
                "    c.`cre_by`, " +
                "    c.`delflg`, " +
                "    m.menu_name_mn, " +
                "    m.menu_name_en, " +
                "    m.formid, " +
                "    r.role_name " +
                "FROM `hospital`.`menu_roles` AS c  " +
                "INNER JOIN `h_menu` AS m ON c.`menuid` = m.`menu_id` " +
                "INNER JOIN `h_role` AS r ON c.`rolecode` = r.`role_code` WHERE ";

        if (entity != null && entity.getId() != null && !entity.getId().toString().isEmpty() && entity.getId() != 0){
            query_sel += " c.`id` = ? AND ";
        }
        if (entity != null && entity.getRolecode() != null && !entity.getRolecode().isEmpty()){
            query_sel += " c.`rolecode` = ? AND ";
        }
        if (entity != null && entity.getMenuId() != null && !entity.getMenuId().isEmpty()){
            query_sel += " c.`menuid` = ? AND ";
        }
        if (entity != null && entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
            query_sel += " c.`delflg` = ? AND ";
        }
        if (entity != null && entity.getFormId() != null && !entity.getFormId().isEmpty()){
            query_sel += " m.`formid` = ? AND ";
        }

        query_sel += "c.id > 0 ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                if (entity != null && entity.getId() != null && !entity.getId().toString().isEmpty() && entity.getId() != 0){
                    statement.setLong(stIndex++, entity.getId());
                }
                if (entity != null && entity.getRolecode() != null && !entity.getRolecode().isEmpty()){
                    statement.setString(stIndex++, entity.getRolecode());
                }
                if (entity != null && entity.getMenuId() != null && !entity.getMenuId().isEmpty()){
                    statement.setString(stIndex++, entity.getMenuId());
                }
                if (entity != null && entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
                    statement.setString(stIndex++, entity.getDelflg());
                }
                if (entity != null && entity.getFormId() != null && !entity.getFormId().isEmpty()){
                    statement.setString(stIndex++, entity.getFormId());
                }

                ResultSet resultSet = statement.executeQuery();

                menuRolesList = new ArrayList<>();

                int index = 1;

                while (resultSet.next()) {
                    MenuRoles entityObj = new MenuRoles();

                    entityObj.setId(resultSet.getLong("id"));
                    entityObj.setRolecode(resultSet.getString("rolecode"));
                    entityObj.setMenuId(resultSet.getString("menuid"));
                    entityObj.setCreAt(resultSet.getDate("cre_at"));
                    entityObj.setCreBy(resultSet.getString("cre_by"));
                    entityObj.setDelflg(resultSet.getString("delflg"));
                    entityObj.setMenuName(resultSet.getString("menu_name_mn"));
                    entityObj.setRoleName(resultSet.getString("role_name"));

                    entityObj.put("index", index++);

                    menuRolesList.add(entityObj);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return menuRolesList;
    }

    @Override
    public ErrorEntity insertData(MenuRoles menuRoles) throws SQLException {
        PreparedStatement statement = null;

        if (menuRoles == null) new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));

        String query_login = "INSERT INTO `hospital`.`menu_roles` " +
                "(`rolecode`, " +
                "`menuid`, " +
                "`cre_at`, " +
                "`cre_by`, " +
                "`delflg`) " +
                "VALUES " +
                "(?,?,?,?,?) ";

        try (Connection connection = boneCP.getConnection()) {
            try{
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, (menuRoles.getRolecode().trim() != null) ? menuRoles.getRolecode().trim(): menuRoles.getRolecode().trim());
                statement.setString(2, (menuRoles.getMenuId().trim() != null) ? menuRoles.getMenuId().trim(): menuRoles.getMenuId().trim());
                statement.setDate(3, menuRoles.getCreAt());
                statement.setString(4, (menuRoles.getCreBy().trim() != null) ? menuRoles.getCreBy().trim(): menuRoles.getCreBy().trim());
                statement.setString(5, (menuRoles.getDelflg().trim() != null) ? menuRoles.getDelflg().trim(): menuRoles.getDelflg().trim());

                MenuRoles searchMenuRoles = new MenuRoles();
                searchMenuRoles.setRolecode(menuRoles.getRolecode());
                searchMenuRoles.setMenuId(menuRoles.getMenuId());

                List<MenuRoles> tmpList = selectAll(searchMenuRoles);
                if (tmpList.size() > 0){
                    menuRoles.setId(tmpList.get(0).getId());
                    menuRoles.setDelflg("N");
                    return updateData(menuRoles);
                }else{
                    int insertCount = statement.executeUpdate();

                    if (insertCount == 1){
                        connection.commit();
                        return new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));
                    }
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }finally {
                if (statement != null)statement.close();
                connection.close();
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }

        return new ErrorEntity(ErrorType.ERROR, Long.valueOf(1057));
    }//end of insert function

    @Override
    public ErrorEntity updateData(MenuRoles entity) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (entity == null) new ErrorEntity(ErrorType.INFO, Long.valueOf(1059));

        query = "UPDATE `hospital`.`menu_roles` AS c " +
                "SET     ";

        if (entity != null && entity.getRolecode() != null && !entity.getRolecode().isEmpty()){
            query += " c.`rolecode` = ?, ";
        }
        if (entity != null && entity.getMenuId() != null && !entity.getMenuId().isEmpty()){
            query += " c.`menuid` = ?, ";
        }
        if (entity != null && entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
            query += " c.`delflg` = ?, ";
        }

        query += " c.`cre_by` = ? ";
        query += " WHERE  `id` = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (entity != null && entity.getRolecode() != null && !entity.getRolecode().isEmpty()){
                    statement.setString(stIndex++, entity.getRolecode());
                }
                if (entity != null && entity.getMenuId() != null && !entity.getMenuId().isEmpty()){
                    statement.setString(stIndex++, entity.getMenuId());
                }
                if (entity != null && entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
                    statement.setString(stIndex++, entity.getDelflg());
                }

                statement.setString(stIndex++, entity.getCreBy());
                statement.setLong(stIndex++, entity.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    return new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }


        return new ErrorEntity(ErrorType.ERROR, Long.valueOf(1061));
    }
}
