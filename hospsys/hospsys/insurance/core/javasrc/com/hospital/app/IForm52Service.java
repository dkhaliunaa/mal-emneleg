package com.hospital.app;

import com.model.hos.ErrorEntity;
import com.model.hos.Form52Entity;
import com.mongodb.BasicDBObject;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public interface IForm52Service {

    /**
     * Insert new data
     *
     *
     * @param form52
     * @return
     * @throws Exception
     */
    ErrorEntity insertNewData(Form52Entity form52) throws SQLException;

    /**
     * Select all
     *
     * @param form52
     * @return
     * @throws SQLException
     */
    List<Form52Entity> getListData(Form52Entity form52) throws SQLException;


    /**
     * update form data
     *
     * @param form52
     * @return
     * @throws SQLException
     */
    ErrorEntity updateData(Form52Entity form52) throws SQLException;


    List<Form52Entity> getPieChartData(Form52Entity form52Entity) throws SQLException;


    ErrorEntity exportReport(String file, Form52Entity form52) throws SQLException;

    BasicDBObject getColHighChartData(Form52Entity Form52Entity) throws SQLException;

    BasicDBObject getPieHighChartData(Form52Entity Form52Entity) throws SQLException;

}
