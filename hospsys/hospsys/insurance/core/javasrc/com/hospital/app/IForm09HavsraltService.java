package com.hospital.app;

import com.model.hos.ErrorEntity;
import com.model.hos.Form09HavsraltEntity;
import com.mongodb.BasicDBObject;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public interface IForm09HavsraltService {

    /**
     * Insert new data
     *
     *
     * @param form09
     * @return
     * @throws Exception
     */
    ErrorEntity insertNewData(Form09HavsraltEntity form09) throws SQLException;

    /**
     * Select all
     *
     * @param form09
     * @return
     * @throws SQLException
     */
    List<Form09HavsraltEntity> getListData(Form09HavsraltEntity form09) throws SQLException;


    /**
     * update form data
     *
     * @param form09
     * @return
     * @throws SQLException
     */
    ErrorEntity updateData(Form09HavsraltEntity form09) throws SQLException;


    List<BasicDBObject> getPieChartData(Form09HavsraltEntity Form09HavsraltEntity) throws SQLException;


    ErrorEntity exportReport(String file,Form09HavsraltEntity form09) throws SQLException;

    /**
     * Column Chart Data processing...
     *
     * @param Form09HavsraltEntity
     * @return
     * @throws SQLException
     */
    BasicDBObject getColumnChartData(Form09HavsraltEntity Form09HavsraltEntity) throws SQLException;
    BasicDBObject getPieHighChartData(Form09HavsraltEntity Form09HavsraltEntity) throws SQLException;
    BasicDBObject getColHighChartData(Form09HavsraltEntity Form09HavsraltEntity) throws SQLException;
}
