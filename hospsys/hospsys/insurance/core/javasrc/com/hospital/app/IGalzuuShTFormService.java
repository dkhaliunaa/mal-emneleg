package com.hospital.app;

import com.model.hos.ErrorEntity;
import com.model.hos.GalzuuShTFormEntity;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public interface IGalzuuShTFormService {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    /**
     * Insert new data
     *
     *
     * @param entity
     * @return
     * @throws Exception
     */
    ErrorEntity insertNewData(GalzuuShTFormEntity entity) throws SQLException;

    /**
     * Select all
     *
     * @param entity
     * @return
     * @throws SQLException
     */
    List<GalzuuShTFormEntity> getListData(GalzuuShTFormEntity entity) throws SQLException;


    /**
     * update form data
     *
     * @param entity
     * @return
     * @throws SQLException
     */
    ErrorEntity updateData(GalzuuShTFormEntity entity) throws SQLException;
}
