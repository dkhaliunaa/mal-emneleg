package com.hospital.app;

import com.model.hos.ErrorEntity;
import com.model.hos.Form13aEntity;
import com.model.hos.Form14Entity;
import com.mongodb.BasicDBObject;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public interface IForm13aService {

    /**
     * Insert new data
     *
     *
     * @param entity
     * @return
     * @throws Exception
     */
    ErrorEntity insertNewData(Form13aEntity entity) throws SQLException;

    /**
     * Select all
     *
     * @param entity
     * @return
     * @throws SQLException
     */
    List<Form13aEntity> getListData(Form13aEntity entity) throws SQLException;


    /**
     * update form data
     *
     * @param entity
     * @return
     * @throws SQLException
     */
    ErrorEntity updateData(Form13aEntity entity) throws SQLException;


    List<Form13aEntity> getPieChartData(Form13aEntity Form13aEntity) throws SQLException;


    ErrorEntity exportReport(String file, Form13aEntity entity) throws SQLException;

    /**
     * Column Chart Data processing...
     *
     * @param Form13aEntity
     * @return
     * @throws SQLException
     */
    List<BasicDBObject> getColumnChartData(Form13aEntity Form13aEntity) throws SQLException;

    BasicDBObject getColHighChartData(Form13aEntity Form13aEntity) throws SQLException;

    BasicDBObject getPieHighChartData(Form13aEntity Form13aEntity) throws SQLException;
}
