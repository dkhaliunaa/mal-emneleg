package com.hospital.app;


import com.enumclass.ErrorType;
import com.jolbox.bonecp.BoneCP;
import com.model.User;
import com.model.hos.ErrorEntity;
import com.model.hos.MedremtgiiDataEntity;
import com.model.hos.SuregMedeeFormEntity;
import com.mongodb.BasicDBObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Tilyeujan on 9/2/2017.
 */
public class GalzuuSuregMedeeFormServiceImpl implements IGalzuuSuregMedeeFormService {
    /**
     * Objects
     */
    private BoneCP boneCP;
    private ErrorEntity errorEntity;


    public GalzuuSuregMedeeFormServiceImpl(BoneCP boneCP) {
        this.boneCP = boneCP;
    }


    @Override
    public List<SuregMedeeFormEntity> selectTreeData(User user, SuregMedeeFormEntity entity) {
        try{
            if (user == null) return null;

            if (!user.getRoleCode().equalsIgnoreCase("SUPERADMIN") && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                entity.setCitycode(user.getCity());
                entity.setSumcode(user.getSum());
                entity.setCre_by(user.getUsername());
            }

            List<SuregMedeeFormEntity> resList = this.getListData(entity);

            IMedremtgiiDataFormService service = new MedremtgiiDataFormServiceImpl(boneCP);

            if (resList.size() == 1){
                SuregMedeeFormEntity tmp = resList.get(0);

                MedremtgiiDataEntity tmp1 = new MedremtgiiDataEntity();

                tmp1.setSureg_key(tmp.getSureg_key());

                tmp1.setRec_type("M");
                tmp.setMedremtgiiDataList(service.getListData(tmp1));

                tmp1.setRec_type("D");
                tmp.setUvchilsonMalList(service.getListData(tmp1));

                tmp1.setRec_type("A");
                tmp.setUhsenList(service.getListData(tmp1));

                tmp1.setRec_type("R");
                tmp.setUstgasanList(service.getListData(tmp1));
            }

            return resList;
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return null;
    }


    public ErrorEntity manageData(User user, SuregMedeeFormEntity entity, List<MedremtgiiDataEntity> entityList) throws Exception{

        IMedremtgiiDataFormService service = new MedremtgiiDataFormServiceImpl(boneCP);

        SuregMedeeFormEntity tmp = new SuregMedeeFormEntity();
        tmp.setForm01key(entity.getForm01key());
        List<SuregMedeeFormEntity> sme = getListData(tmp);

        String command = "";

        if (sme == null){
            command = "INS";
        }
        else if (sme.size() == 0){
            command = "INS";
        }
        else if (sme.size() > 0){
            command = "EDT";
        }

        Date utilDate = new Date();
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

        entity.setMod_at(sqlDate);
        entity.setMod_by(user.getUsername());

        //Sureg medeelel insert hiih
        if (command.equalsIgnoreCase("INS"))
        {
            entity.setDelflg("N");
            entity.setActflg("Y");
            entity.setCre_by(user.getUsername());
            entity.setCre_at(sqlDate);

            errorEntity = insertNewData(entity);
        }
        else if (command.equalsIgnoreCase("EDT") ||
                command.equalsIgnoreCase("DEL") ||
                command.equalsIgnoreCase("ACT"))
        {
            entity.setId(sme.get(0).getId());
            if (command.equalsIgnoreCase("DEL")){
                entity.setDelflg("Y");
            }
            errorEntity = updateData(entity);
        }

        // Medremtgii data insert
        if (errorEntity.getErrCode() == 1056 || errorEntity.getErrCode() == 1060){
            for (MedremtgiiDataEntity md :
                    entityList) {
                MedremtgiiDataEntity dataEntity = new MedremtgiiDataEntity();

                dataEntity.setSureg_key(entity.getSureg_key());
                dataEntity.setRec_type(md.getRec_type());
                dataEntity.setMtype(md.getMtype());
                dataEntity.setM_age(md.getM_age());

                List<MedremtgiiDataEntity> listData = service.getListData(dataEntity);

                String cmd = "";

                if (listData == null){
                    cmd = "INS";
                }
                else if (listData.size() == 0){
                    cmd = "INS";
                }
                else if (listData.size() > 0){
                    cmd = "EDT";
                }

                md.setMod_by(user.getUsername());
                md.setMod_at(sqlDate);
                md.setRecord_date(entity.getRecord_date());

                if (cmd.equalsIgnoreCase("INS"))
                {
                    md.setDelflg("N");
                    md.setActflg("Y");
                    md.setCre_by(user.getUsername());
                    md.setCre_at(sqlDate);

                    errorEntity = service.insertNewData(md);
                }
                else if (cmd.equalsIgnoreCase("EDT") ||
                        cmd.equalsIgnoreCase("DEL") ||
                        cmd.equalsIgnoreCase("ACT"))
                {
                    md.setId(listData.get(0).getId());

                    if (cmd.equalsIgnoreCase("DEL")){
                        entity.setDelflg("Y");
                    }
                    errorEntity = service.updateData(md);
                }
            }// end of for
        }

        return errorEntity;
    }

    @Override
    public ErrorEntity insertNewData(SuregMedeeFormEntity entity) throws SQLException {
        PreparedStatement statement = null;

        if (entity == null) {
            errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));

            errorEntity.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errorEntity.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            
            return errorEntity;
        }

        String query_login = "INSERT INTO `hform01_sureg_medee`(`form01key`, `citycode`, `sumcode`, `bagcode`, " +
                "`malchinner`, `delflg`, `actflg`, `cre_at`, `cre_by`, `mod_at`, `mod_by`, `record_date`, " +
                "`sureg_key`, `str1`, `str2`) " +
                "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        try (Connection connection = boneCP.getConnection()) {
            try{
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, (entity.get(entity.FORM01KEY) != null) ? entity.getForm01key().trim() : "");
                statement.setString(2, (entity.get(entity.CITYCODE) != null) ? entity.getCitycode().trim() : "");
                statement.setString(3, (entity.get(entity.SUMCODE) != null) ? entity.getSumcode().trim() : "");
                statement.setString(4, (entity.get(entity.BAGCODE) != null) ? entity.getBagcode().trim() : "");
                statement.setString(5, (entity.get(entity.MALCHINNER) != null) ? entity.getMalchinner().trim() : "");
                statement.setString(6, (entity.get(entity.DELFLG) != null) ? entity.getDelflg().trim() : "");
                statement.setString(7, (entity.get(entity.ACTFLG) != null) ? entity.getActflg().trim() : "");
                statement.setDate(8, entity.getCre_at());
                statement.setString(9, (entity.get(entity.CRE_BY) != null) ? entity.getCre_by().trim() : "");
                statement.setDate(10, entity.getMod_at());
                statement.setString(11, (entity.get(entity.MOD_BY) != null) ? entity.getMod_by().trim() : "");
                statement.setDate(12, entity.getRecord_date());
                statement.setString(13, (entity.get(entity.SUREG_KEY) != null) ? entity.getSureg_key().trim() : "");
                statement.setString(14, (entity.get(entity.STR1) != null) ? entity.getStr1().trim() : "");
                statement.setString(15, (entity.get(entity.STR2) != null) ? entity.getStr2().trim() : "");

                int insertCount = statement.executeUpdate();

                if (insertCount == 1){
                    connection.commit();

                    errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));

                    errorEntity.setErrText("Бичлэг амжилттай хадгалагдлаа.");
                    errorEntity.setErrDesc("Бичлэг амжилттай хадгалагдлаа.");

                    return errorEntity;
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }finally {
                if (statement != null)statement.close();
                connection.close();
            }

        }catch (Exception ex){

        }

        errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1057));
        errorEntity.setErrText("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");
        errorEntity.setErrDesc("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");

        return errorEntity;
    }

    @Override
    public List<SuregMedeeFormEntity> getListData(SuregMedeeFormEntity entity) throws SQLException {
        int stIndex = 1;
        int index = 1;
        List<SuregMedeeFormEntity> entityList = null;

        String query = "SELECT `hform01_sureg_medee`.`id`, " +
                "    `hform01_sureg_medee`.`form01key`, " +
                "    `hform01_sureg_medee`.`citycode`, " +
                "    `hform01_sureg_medee`.`sumcode`, " +
                "    `hform01_sureg_medee`.`bagcode`, " +
                "    `hform01_sureg_medee`.`malchinner`, " +
                "    `hform01_sureg_medee`.`delflg`, " +
                "    `hform01_sureg_medee`.`actflg`, " +
                "    `hform01_sureg_medee`.`cre_at`, " +
                "    `hform01_sureg_medee`.`cre_by`, " +
                "    `hform01_sureg_medee`.`mod_at`, " +
                "    `hform01_sureg_medee`.`mod_by`, " +
                "    `hform01_sureg_medee`.`record_date`, " +
                "    `hform01_sureg_medee`.`sureg_key`, " +
                "    `hform01_sureg_medee`.`str1`, " +
                "    `hform01_sureg_medee`.`str2`, " +
                "`h_sum`.sumname, " +
                "`h_sum`.sumname_en, " +
                "`h_baghoroo`.horooname, " +
                "`h_baghoroo`.horooname_en, " +
                "`h_city`.cityname, " +
                "`h_city`.cityname_en " +
                "FROM `hospital`.`hform01_sureg_medee` INNER JOIN `h_city` " +
                "ON `h_city`.code = `hform01_sureg_medee`.citycode " +
                "INNER JOIN `h_sum` " +
                "ON `h_sum`.sumcode = `hform01_sureg_medee`.sumcode " +
                "INNER JOIN `h_baghoroo` ON `hform01_sureg_medee`.bagcode = `h_baghoroo`.horoocode " +
                "AND `h_baghoroo`.sumcode = `hform01_sureg_medee`.`sumcode` " +
                "WHERE ";

        if (entity.getForm01key() != null && !entity.getForm01key().isEmpty()){
            query += " `hform01_sureg_medee`.`form01key` = ? AND ";
        }
        if (entity.getCitycode() != null && !entity.getCitycode().isEmpty()){
            query += " `hform01_sureg_medee`.`citycode` = ? AND ";
        }
        if (entity.getSumcode() != null && !entity.getSumcode().isEmpty()){
            query += " `hform01_sureg_medee`.`sumcode` = ? AND ";
        }
        if (entity.getBagcode() != null && !entity.getBagcode().isEmpty()){
            query += " `hform01_sureg_medee`.`bagcode` = ? AND ";
        }
        if (entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
            query += " `hform01_sureg_medee`.delflg = ? AND ";
        }
        if (entity.getActflg() != null && !entity.getActflg().isEmpty()){
            query += " `hform01_sureg_medee`.actflg = ? AND ";
        }
        if (entity != null && entity.getCre_by() != null && !entity.getCre_by().isEmpty()){
            query += " `hform01_sureg_medee`.cre_by = ? AND ";
        }
        if (entity != null && entity.getSureg_key() != null && !entity.getSureg_key().isEmpty()){
            query += " `hform01_sureg_medee`.`sureg_key` = ? AND ";
        }

        query += " `hform01_sureg_medee`.`id` > 0 ";
        query += " ORDER BY `hform01_sureg_medee`.`cre_at` DESC ";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query);

                if (entity.getForm01key() != null && !entity.getForm01key().isEmpty()){
                    statement.setString(stIndex++, entity.getForm01key());
                }
                if (entity.getCitycode() != null && !entity.getCitycode().isEmpty()){
                    statement.setString(stIndex++, entity.getCitycode());
                }
                if (entity.getSumcode() != null && !entity.getSumcode().isEmpty()){
                    statement.setString(stIndex++, entity.getSumcode());
                }
                if (entity.getBagcode() != null && !entity.getBagcode().isEmpty()){
                    statement.setString(stIndex++, entity.getBagcode());
                }
                if (entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
                    statement.setString(stIndex++, entity.getDelflg());
                }
                if (entity.getActflg() != null && !entity.getActflg().isEmpty()){
                    statement.setString(stIndex++, entity.getActflg());
                }
                if (entity != null && entity.getCre_by() != null && !entity.getCre_by().isEmpty()){
                    statement.setString(stIndex++, entity.getCre_by());
                }
                if (entity != null && entity.getSureg_key() != null && !entity.getSureg_key().isEmpty()){
                    statement.setString(stIndex++, entity.getSureg_key());
                }


                ResultSet resultSet = statement.executeQuery();

                entityList = new ArrayList<>();

                while (resultSet.next()) {
                    SuregMedeeFormEntity objEntity = new SuregMedeeFormEntity();

                    objEntity.setId(resultSet.getLong("id"));
                    objEntity.setForm01key(resultSet.getString("form01key"));
                    objEntity.setCitycode(resultSet.getString("citycode"));
                    objEntity.setSumcode(resultSet.getString("sumcode"));
                    objEntity.setBagcode(resultSet.getString("bagcode"));
                    objEntity.setMalchinner(resultSet.getString("malchinner"));
                    objEntity.setSureg_key(resultSet.getString("sureg_key"));
                    objEntity.setStr1(resultSet.getString("str1"));
                    objEntity.setStr2(resultSet.getString("str2"));
                    objEntity.setDelflg(resultSet.getString("delflg"));
                    objEntity.setCre_at(resultSet.getDate("cre_at"));
                    objEntity.setCre_by(resultSet.getString("cre_by"));
                    objEntity.setMod_at(resultSet.getDate("mod_at"));
                    objEntity.setMod_by(resultSet.getString("mod_by"));
                    objEntity.setRecord_date(resultSet.getDate("record_date"));
                    objEntity.setActflg(resultSet.getString("actflg"));

                    objEntity.put("cityname", resultSet.getString("cityname"));
                    objEntity.put("sumname", resultSet.getString("sumname"));
                    objEntity.put("horooname", resultSet.getString("horooname"));
                    objEntity.put("index", index++);
                    objEntity.put("strRecordOgnoo", dateFormat.format(objEntity.getRecord_date()));

                    entityList.add(objEntity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return entityList;
    }

    /**
     * Get name
     *
     * @param cmbType
     * @param mal_turul_mn
     * @return
     */
    private String getTypeNer(List<BasicDBObject> cmbType, String mal_turul_mn) {
        for (BasicDBObject obj :
                cmbType) {
            if (obj.get("id") != null && mal_turul_mn != null && obj.getString("id").equalsIgnoreCase(mal_turul_mn)) {
                return obj.getString("name");
            }
        }

        return "";
    }

    @Override
    public ErrorEntity updateData(SuregMedeeFormEntity entity) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (entity == null) new ErrorEntity(ErrorType.INFO, Long.valueOf(1059));
        if (entity == null) {
            errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errorEntity.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errorEntity.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errorEntity;
        }

        query = "UPDATE `hospital`.`hform01_sureg_medee` " +
                "SET     ";

        if (entity.getForm01key() != null && !entity.getForm01key().isEmpty()){
            query += " `hform01_sureg_medee`.`form01key` = ?, ";
        }
        if (entity.getCitycode() != null && !entity.getCitycode().isEmpty()){
            query += " `hform01_sureg_medee`.`citycode` = ?, ";
        }
        if (entity.getSumcode() != null && !entity.getSumcode().isEmpty()){
            query += " `hform01_sureg_medee`.`sumcode` = ?, ";
        }
        if (entity.getBagcode() != null && !entity.getBagcode().isEmpty()){
            query += " `hform01_sureg_medee`.`bagcode` = ?, ";
        }
        if (entity.getMalchinner() != null && !entity.getMalchinner().isEmpty()){
            query += " `hform01_sureg_medee`.`malchinner` = ?, ";
        }
        if (entity.getSureg_key() != null && !entity.getSureg_key().isEmpty()){
            query += " `hform01_sureg_medee`.`sureg_key` = ?, ";
        }
        if (entity.getStr1() != null && !entity.getStr1().isEmpty()){
            query += " `hform01_sureg_medee`.`str1` = ?, ";
        }
        if (entity.getStr2() != null && !entity.getStr2().isEmpty()){
            query += " `hform01_sureg_medee`.`str2` = ?, ";
        }
        if (entity.getStr1() != null && !entity.getStr1().isEmpty()){
            query += " `hform01_sureg_medee`.`str1` = ?, ";
        }
        if (entity.getStr2() != null && !entity.getStr2().isEmpty()){
            query += " `hform01_sureg_medee`.`str2` = ?, ";
        }

        if (entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
            query += " `hform01_sureg_medee`.delflg = ?, ";
        }
        if (entity.getActflg() != null && !entity.getActflg().isEmpty()){
            query += " `hform01_sureg_medee`.actflg = ?, ";
        }
        if (entity.getRecord_date() != null){
            query += " `hform01_sureg_medee`.`record_date` = ?,";
        }

        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";
        query += " WHERE  ID   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (entity.getForm01key() != null && !entity.getForm01key().isEmpty()){
                    statement.setString(stIndex++, entity.getForm01key());
                }
                if (entity.getCitycode() != null && !entity.getCitycode().isEmpty()){
                    statement.setString(stIndex++, entity.getCitycode());
                }
                if (entity.getSumcode() != null && !entity.getSumcode().isEmpty()){
                    statement.setString(stIndex++, entity.getSumcode());
                }
                if (entity.getBagcode() != null && !entity.getBagcode().isEmpty()){
                    statement.setString(stIndex++, entity.getBagcode());
                }
                if (entity.getMalchinner() != null && !entity.getMalchinner().isEmpty()){
                    statement.setString(stIndex++, entity.getMalchinner());
                }
                if (entity.getSureg_key() != null && !entity.getSureg_key().isEmpty()){
                    statement.setString(stIndex++, entity.getSureg_key());
                }
                if (entity.getStr1() != null && !entity.getStr1().isEmpty()){
                    statement.setString(stIndex++, entity.getStr1());
                }
                if (entity.getStr2() != null && !entity.getStr2().isEmpty()){
                    statement.setString(stIndex++, entity.getStr2());
                }
                if (entity.getStr1() != null && !entity.getStr1().isEmpty()){
                    statement.setString(stIndex++, entity.getStr1());
                }
                if (entity.getStr2() != null && !entity.getStr2().isEmpty()){
                    statement.setString(stIndex++, entity.getStr2());
                }

                if (entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
                    statement.setString(stIndex++, entity.getDelflg());
                }
                if (entity.getActflg() != null && !entity.getActflg().isEmpty()){
                    statement.setString(stIndex++, entity.getActflg());
                }
                if (entity.getRecord_date() != null){
                    statement.setDate(stIndex++, entity.getRecord_date());
                }

                statement.setDate(stIndex++, entity.getMod_at());
                statement.setString(stIndex++, entity.getMod_by());
                statement.setLong(stIndex++, entity.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                    errorEntity.setErrText("Бичлэг амжилттай засагдлаа.");
                    errorEntity.setErrDesc("Бичлэг амжилттай засагдлаа.");
                    return errorEntity;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }


        errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1061));
        errorEntity.setErrText("Амжилтгүй боллоо. Дахин оролдоно уу.");
        errorEntity.setErrDesc("Амжилтгүй боллоо. Дахин оролдоно уу.");

        return errorEntity;
    }
}
