package com.hospital.app;


import com.Config;
import com.enumclass.ErrorType;
import com.jolbox.bonecp.BoneCP;
import com.model.hos.*;
import com.mongodb.BasicDBObject;
import com.rpc.PdfExcelUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public class Form51ServiceImpl implements IForm51Service{
    /**
     * Objects
     */

    private BoneCP boneCP;
    private ErrorEntity errObj;


    public Form51ServiceImpl(BoneCP boneCP) {
        this.boneCP = boneCP;
    }

    @Override
    public ErrorEntity insertNewData(Form51Entity form51) throws SQLException {
        PreparedStatement statement = null;

        if (form51 == null){
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        String query_login = "INSERT INTO `hospital`.`hform51` " +
                "(`sum`, " +
                "`sum_en`, " +
                "`sumBagNer`, " +
                "`sumBagNer_en`, " +
                "`niitMalToo`, " +
                "`bogToo`, " +
                "`bodToo`, " +
                "`tolToo`, " +
                "`ehniiUldegdel`, " +
                "`orlogo`, " +
                "`zarlaga`, " +
                "`jilEUldegdel`, " +
                "`delflg`, " +
                "`actflg`, " +
                "`mod_at`, " +
                "`mod_by`, " +
                "`cre_at`, " +
                "`recorddate`," +
                "`cre_by`," +
                "`aimag`," +
                "`aimag_en`)" +
                "VALUES " +
                "(? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?,?, ?,?)";

        try (Connection connection = boneCP.getConnection()) {
            try{
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, form51.getSum());
                statement.setString(2, form51.getSum_en());
                statement.setString(3, form51.getSumBagNer());
                statement.setString(4, form51.getSumBagNer_en());
                statement.setLong(5, form51.getNiitMalToo());
                statement.setLong(6, form51.getBogToo());
                statement.setLong(7, form51.getBodToo());
                statement.setLong(8, form51.getTolToo());
                statement.setBigDecimal(9, form51.getEhniiUldegdel());
                statement.setBigDecimal(10, form51.getOrlogo());
                statement.setBigDecimal(11, form51.getZarlaga());
                statement.setBigDecimal(12, form51.getJilEUldegdel());
                statement.setString(13, form51.getDelflg());
                statement.setString(14, form51.getActflg());
                statement.setDate(15, form51.getModAt());
                statement.setString(16, form51.getModby());
                statement.setDate(17, form51.getCreat());
                statement.setDate(18, form51.getRecordDate());
                statement.setString(19, form51.getCreby());
                statement.setString(20, form51.getAimag());
                statement.setString(21, form51.getAimag_en());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1){
                    connection.commit();
                    errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));
                    errObj.setErrText("Бичлэг амжилттай хадгалагдлаа.");
                    errObj.setErrDesc("Бичлэг амжилттай хадгалагдлаа.");
                    return errObj;
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }finally {
                if (statement != null)statement.close();
                connection.close();
            }

        }catch (Exception ex){

        }

        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1057));
        errObj.setErrText("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");
        errObj.setErrDesc("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");

        return errObj;
    }

    @Override
    public List<Form51Entity> getListData(Form51Entity form51) throws SQLException {
        int stIndex = 1;
        List<Form51Entity> form51EntitieList = null;

        String query = "SELECT `hform51`.`id`, " +
                "    `hform51`.`sum`, " +
                "    `hform51`.`sum_en`, " +
                "    `hform51`.`sumBagNer`, " +
                "    `hform51`.`sumBagNer_en`, " +
                "    `hform51`.`niitMalToo`, " +
                "    `hform51`.`bogToo`, " +
                "    `hform51`.`bodToo`, " +
                "    `hform51`.`tolToo`, " +
                "    `hform51`.`ehniiUldegdel`, " +
                "    `hform51`.`orlogo`, " +
                "    `hform51`.`zarlaga`, " +
                "    `hform51`.`jilEUldegdel`, " +
                "    `hform51`.`delflg`, " +
                "    `hform51`.`actflg`, " +
                "    `hform51`.`mod_at`, " +
                "    `hform51`.`mod_by`, " +
                "    `hform51`.`cre_at`, " +
                "    `hform51`.`recorddate`, " +
                "    `hform51`.`cre_by`, " +
                "    `hform51`.`aimag`, " +
                "    `hform51`.`aimag_en`, " +
                "    c.cityname, c.cityname_en, " +
                "    `h_sum`.sumname, `h_sum`.sumname_en," +
                "    `h_baghoroo`.horooname, `h_baghoroo`.horooname_en, " +
                "    `h_sum`.latitude as sum_latitude, `h_sum`.longitude as sum_longitude  " +
                "FROM `hospital`.`hform51` INNER JOIN `h_city` AS c " +
                "ON c.code = `hform51`.aimag " +
                "INNER JOIN `h_sum` " +
                "ON `h_sum`.sumcode = `hform51`.sum " +
                "INNER JOIN `h_baghoroo` ON `hform51`.sumBagNer = `h_baghoroo`.horoocode AND `h_baghoroo`.sumcode = `hform51`.`sum`" +
                "WHERE ";

        if (form51.getSum() != null && !form51.getSum().isEmpty()){
            query += " sum = ? AND ";
        }

        if (form51.getSum_en() != null && !form51.getSum_en().isEmpty()){
            query += " sum_en = ? AND ";
        }

        /* NER */
        if (form51.getSumBagNer() != null && !form51.getSumBagNer().toString().isEmpty()){
            query += " sumBagNer = ? AND ";
        }
        if (form51.getSumBagNer_en() != null && !form51.getSumBagNer_en().toString().isEmpty()){
            query += " sumBagNer_en = ? AND ";
        }
        /* Zartsuulsan hemjee */
        if (form51.getNiitMalToo() != null && !form51.getNiitMalToo().toString().isEmpty() && form51.getNiitMalToo() != 0){
            query += " niitMalToo = ? AND ";
        }
        if (form51.getBogToo() != null && !form51.getBogToo().toString().isEmpty() && form51.getBogToo() != 0){
            query += " bogToo = ? AND ";
        }

        /* MAL TURUL */
        if (form51.getBodToo() != null && !form51.getBodToo().toString().isEmpty() && form51.getBodToo() != 0){
            query += " bodToo = ? AND ";
        }
        if (form51.getTolToo() != null && !form51.getTolToo().toString().isEmpty() && form51.getBogToo() != 0){
            query += " tolToo = ? AND ";
        }

        /* Tuluvlugu */
        if (form51.getEhniiUldegdel() != null && !form51.getEhniiUldegdel().toString().isEmpty() && form51.getEhniiUldegdel().longValue() != 0){
            query += " ehniiUldegdel = ? AND ";
        }
        if (form51.getOrlogo() != null && !form51.getOrlogo().toString().isEmpty() && form51.getOrlogo().longValue() != 0){
            query += " orlogo = ? AND ";
        }

        /* Guitsetgel */
        if (form51.getZarlaga() != null && !form51.getZarlaga().toString().isEmpty() && form51.getZarlaga().longValue() != 0){
            query += " zarlaga = ? AND ";
        }
        if (form51.getJilEUldegdel() != null && !form51.getJilEUldegdel().toString().isEmpty() && form51.getJilEUldegdel().longValue() != 0){
            query += " jilEUldegdel = ? AND ";
        }

        if (form51 != null && form51.getAimag() != null && !form51.getAimag().isEmpty()){
            query += " `aimag` = ? AND ";
        }
        if (form51 != null && form51.getAimag_en() != null && !form51.getAimag_en().isEmpty()){
            query += " `aimag_en` = ? AND ";
        }
        if (form51 != null && form51.getSum() != null && !form51.getSum().isEmpty()){
            query += " `sum` = ? AND ";
        }

        if (form51 != null && form51.getRecordDate() != null && form51.getSearchRecordDate() != null){
            query += " (`recorddate` BETWEEN ? AND ?) AND ";
        }

        if (form51 != null && form51.getCreby() != null && !form51.getCreby().isEmpty()){
            query += " `hform51`.`cre_by` = ? AND ";
        }

        query += " `hform51`.`delflg` = 'N'";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query);

                if (form51.getSum() != null && !form51.getSum().isEmpty()) {
                    statement.setString(stIndex++, form51.getSum());
                }

                if (form51.getSum_en() != null && !form51.getSum_en().isEmpty()) {
                    statement.setString(stIndex++, form51.getSum_en());
                }

            /* NER */
                if (form51.getSumBagNer() != null && !form51.getSumBagNer().toString().isEmpty()) {
                    statement.setString(stIndex++, form51.getSumBagNer());
                }
                if (form51.getSumBagNer_en() != null && !form51.getSumBagNer_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form51.getSumBagNer_en());
                }

            /* Zartsuulsan hemjee */
                if (form51.getNiitMalToo() != null && !form51.getNiitMalToo().toString().isEmpty() && form51.getNiitMalToo() != 0) {
                    statement.setLong(stIndex++, form51.getNiitMalToo());
                }
                if (form51.getBogToo() != null && !form51.getBogToo().toString().isEmpty() && form51.getBogToo() != 0) {
                    statement.setLong(stIndex++, form51.getBogToo());
                }

            /* MAL TURUL */
                if (form51.getBodToo() != null && !form51.getBodToo().toString().isEmpty() && form51.getBodToo() != 0) {
                    statement.setLong(stIndex++, form51.getBodToo());
                }
                if (form51.getTolToo() != null && !form51.getTolToo().toString().isEmpty() && form51.getTolToo() != 0) {
                    statement.setLong(stIndex++, form51.getTolToo());
                }

            /* Tuluvlugu */
                if (form51.getEhniiUldegdel() != null && !form51.getEhniiUldegdel().toString().isEmpty() && form51.getEhniiUldegdel().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form51.getEhniiUldegdel());
                }
                if (form51.getOrlogo() != null && !form51.getOrlogo().toString().isEmpty() && form51.getOrlogo().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form51.getOrlogo());
                }

            /* Guitsetgel */
                if (form51.getZarlaga() != null && !form51.getZarlaga().toString().isEmpty() && form51.getZarlaga().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form51.getZarlaga());
                }
                if (form51.getJilEUldegdel() != null && !form51.getJilEUldegdel().toString().isEmpty() && form51.getJilEUldegdel().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form51.getJilEUldegdel());
                }

                if (form51 != null && form51.getAimag() != null && !form51.getAimag().isEmpty()){
                    statement.setString(stIndex++, form51.getAimag());
                }

                if (form51 != null && form51.getAimag_en() != null && !form51.getAimag_en().isEmpty()){
                    statement.setString(stIndex++, form51.getAimag_en());
                }

                if (form51 != null && form51.getSum() != null && !form51.getSum().isEmpty()){
                    statement.setString(stIndex++, form51.getSum());
                }

                if (form51 != null && form51.getRecordDate() != null && form51.getSearchRecordDate() != null){
                    statement.setDate(stIndex++, form51.getRecordDate());
                    statement.setDate(stIndex++, form51.getSearchRecordDate());
                }
                if (form51 != null && form51.getCreby() != null && !form51.getCreby().isEmpty()){
                    statement.setString(stIndex++, form51.getCreby());
                }

                ResultSet resultSet = statement.executeQuery();

                form51EntitieList = new ArrayList<>();
                int index =1;
                while (resultSet.next()) {
                    Form51Entity entity = new Form51Entity();

                    entity.setId(resultSet.getLong("id"));
                    entity.setSum(resultSet.getString("sum"));
                    entity.setSum_en(resultSet.getString("sum_en"));
                    entity.setSumBagNer(resultSet.getString("sumBagNer"));
                    entity.setSumBagNer_en(resultSet.getString("sumBagNer_en"));
                    entity.setNiitMalToo(resultSet.getLong("niitMalToo"));
                    entity.setBogToo(resultSet.getLong("bogToo"));
                    entity.setBodToo(resultSet.getLong("bodToo"));
                    entity.setTolToo(resultSet.getLong("tolToo"));
                    entity.setEhniiUldegdel(resultSet.getBigDecimal("ehniiUldegdel"));
                    entity.setOrlogo(resultSet.getBigDecimal("orlogo"));
                    entity.setZarlaga(resultSet.getBigDecimal("zarlaga"));
                    entity.setJilEUldegdel(resultSet.getBigDecimal("jilEUldegdel"));
                    entity.setDelflg(resultSet.getString("delflg"));
                    entity.setActflg(resultSet.getString("actflg"));
                    entity.setModAt(resultSet.getDate("mod_at"));
                    entity.setModby(resultSet.getString("mod_by"));
                    entity.setCreat(resultSet.getDate("cre_at"));
                    entity.setRecordDate(resultSet.getDate("recorddate"));
                    entity.setCreby(resultSet.getString("cre_by"));
                    entity.setAimag(resultSet.getString("aimag"));
                    entity.setAimag_en(resultSet.getString("aimag_en"));
                    //entity.setMalTurulNer(comboService.getComboName(resultSet.getString("mal_turul_mn"), ContextHelper.getLanguageCode()));
                    entity.put("sumname", resultSet.getString("sumname"));
                    entity.put("baghorooname", resultSet.getString("horooname"));
                    entity.put("sum_latitude", resultSet.getString("sum_latitude"));
                    entity.put("sum_longitude", resultSet.getString("sum_longitude"));
                    entity.put("index",index++);
                    form51EntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return form51EntitieList;
    }

    @Override
    public ErrorEntity updateData(Form51Entity form51) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (form51 == null) {
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        query = "UPDATE `hospital`.`hform51` " +
                "SET     ";

        if (form51.getSum() != null && !form51.getSum().isEmpty()){
            query += " sum = ?, ";
        }

        if (form51.getSum_en() != null && !form51.getSum_en().isEmpty()){
            query += " sum_en = ? , ";
        }

        /* NER */
        if (form51.getSumBagNer() != null && !form51.getSumBagNer().toString().isEmpty() && form51.getSumBagNer() != "0"){
            query += " sumBagNer = ? , ";
        }
        if (form51.getSumBagNer_en() != null && !form51.getSumBagNer_en().toString().isEmpty()){
            query += " sumBagNer_en = ? , ";
        }
        /* Zartsuulsan hemjee */
        if (form51.getNiitMalToo() != null && !form51.getNiitMalToo().toString().isEmpty() && form51.getNiitMalToo() != 0){
            query += " niitMalToo = ? , ";
        }
        if (form51.getBogToo() != null && !form51.getBogToo().toString().isEmpty() && form51.getBogToo() != 0){
            query += " bogToo = ? , ";
        }

        /* MAL TURUL */
        if (form51.getBodToo() != null && !form51.getBodToo().toString().isEmpty() && form51.getBodToo() != 0){
            query += " bodToo = ? , ";
        }
        if (form51.getTolToo() != null && !form51.getTolToo().toString().isEmpty() && form51.getBogToo() != 0){
            query += " tolToo = ? , ";
        }

        /* Tuluvlugu */
        if (form51.getEhniiUldegdel() != null && !form51.getEhniiUldegdel().toString().isEmpty() && form51.getEhniiUldegdel().longValue() != 0){
            query += " ehniiUldegdel = ? , ";
        }
        if (form51.getOrlogo() != null && !form51.getOrlogo().toString().isEmpty() && form51.getOrlogo().longValue() != 0){
            query += " orlogo = ? , ";
        }

        /* Guitsetgel */
        if (form51.getZarlaga() != null && !form51.getZarlaga().toString().isEmpty() && form51.getZarlaga().longValue() != 0){
            query += " zarlaga = ? , ";
        }
        if (form51.getJilEUldegdel() != null && !form51.getJilEUldegdel().toString().isEmpty() && form51.getJilEUldegdel().longValue() != 0){
            query += " jilEUldegdel = ? , ";
        }

        /* Delete flag */
        if (form51.getDelflg() != null && !form51.getDelflg().toString().isEmpty()){
            query += " delflg = ?, ";
        }

        /* Active flag */
        if (form51.getActflg() != null && !form51.getActflg().toString().isEmpty()){
            query += " actflg = ?, ";
        }

        if (form51 != null && form51.getRecordDate() != null){
            query += " `recorddate` = ?, ";
        }
        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";
        query += " WHERE  ID   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (form51.getSum() != null && !form51.getSum().isEmpty()) {
                    statement.setString(stIndex++, form51.getSum());
                }

                if (form51.getSum_en() != null && !form51.getSum_en().isEmpty()) {
                    statement.setString(stIndex++, form51.getSum_en());
                }

            /* NER */
                if (form51.getSumBagNer() != null && !form51.getSumBagNer().toString().isEmpty()) {
                    statement.setString(stIndex++, form51.getSumBagNer());
                }
                if (form51.getSumBagNer_en() != null && !form51.getSumBagNer_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form51.getSumBagNer_en());
                }

            /* Zartsuulsan hemjee */
                if (form51.getNiitMalToo() != null && !form51.getNiitMalToo().toString().isEmpty() && form51.getNiitMalToo() != 0) {
                    statement.setLong(stIndex++, form51.getNiitMalToo());
                }
                if (form51.getBogToo() != null && !form51.getBogToo().toString().isEmpty() && form51.getBogToo() != 0) {
                    statement.setLong(stIndex++, form51.getBogToo());
                }

            /* MAL TURUL */
                if (form51.getBodToo() != null && !form51.getBodToo().toString().isEmpty() && form51.getBodToo() != 0) {
                    statement.setLong(stIndex++, form51.getBodToo());
                }
                if (form51.getTolToo() != null && !form51.getTolToo().toString().isEmpty() && form51.getTolToo() != 0) {
                    statement.setLong(stIndex++, form51.getTolToo());
                }

            /* Tuluvlugu */
                if (form51.getEhniiUldegdel() != null && !form51.getEhniiUldegdel().toString().isEmpty() && form51.getEhniiUldegdel().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form51.getEhniiUldegdel());
                }
                if (form51.getOrlogo() != null && !form51.getOrlogo().toString().isEmpty() && form51.getOrlogo().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form51.getOrlogo());
                }

            /* Guitsetgel */
                if (form51.getZarlaga() != null && !form51.getZarlaga().toString().isEmpty() && form51.getZarlaga().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form51.getZarlaga());
                }
                if (form51.getJilEUldegdel() != null && !form51.getJilEUldegdel().toString().isEmpty() && form51.getJilEUldegdel().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form51.getJilEUldegdel());
                }

                if (form51 != null && form51.getAimag() != null && !form51.getAimag().isEmpty()){
                    statement.setString(stIndex++, form51.getAimag());
                }

                if (form51 != null && form51.getAimag_en() != null && !form51.getAimag_en().isEmpty()){
                    statement.setString(stIndex++, form51.getAimag_en());
                }

            /* Delete flag */
                if (form51.getDelflg() != null && !form51.getDelflg().toString().isEmpty()) {
                    statement.setString(stIndex++, form51.getDelflg());
                }

            /* Active flag */
                if (form51.getActflg() != null && !form51.getActflg().toString().isEmpty()) {
                    statement.setString(stIndex++, form51.getActflg());
                }

                if (form51 != null && form51.getRecordDate() != null){
                    statement.setDate(stIndex++, form51.getRecordDate());
                }

                statement.setDate(stIndex++, form51.getModAt());
                statement.setString(stIndex++, form51.getModby());
                statement.setLong(stIndex++, form51.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                    errObj.setErrText("Бичлэг амжилттай засагдлаа.");
                    errObj.setErrDesc("Бичлэг амжилттай засагдлаа.");
                    return errObj;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }


        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1061));
        errObj.setErrText("Амжилтгүй боллоо. Дахин оролдоно уу.");
        errObj.setErrDesc("Амжилтгүй боллоо. Дахин оролдоно уу.");

        return errObj;
    }

    @Override
    public List<Form51Entity> getPieChartData(Form51Entity form51Entity) throws SQLException {
        int stIndex = 1;
        List<Form51Entity> form51EntitieList = null;

        String query_sel = "SELECT `hform51`.`id`,  " +
                "    `hform51`.`aimag`,  " +
                "    `hform51`.`sum`,  " +
                "    SUM(`hform51`.`zartsuulsan_hemjee_mn`) as zartsuulsan_hemjee,  " +
                "    c.cityname, c.cityname_en,  " +
                "    `h_sum`.sumname, `h_sum`.sumname_en  " +
                "FROM `hospital`.`hform51`  " +
                "INNER JOIN `h_city` AS c  " +
                "ON c.code = `hform51`.aimag  " +
                "INNER JOIN `h_sum`  " +
                "ON `h_sum`.sumcode = `hform51`.sum  " +
                "WHERE `hform51`.`delflg` = 'N' and `hform51`.`actflg` = 'Y' and `hform51`.`aimag` = ?  " +
                "GROUP BY `hform51`.`sum` ";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                if (form51Entity.getAimag() != null && !form51Entity.getAimag().isEmpty()) {
                    statement.setString(stIndex++, form51Entity.getAimag());
                }

                ResultSet resultSet = statement.executeQuery();

                form51EntitieList = new ArrayList<>();

                while (resultSet.next()) {
//                    Form51Entity entity = new Form51Entity();
//
//                    entity.setId(resultSet.getLong("id"));
//                    entity.setZartsuulsanHemjee(resultSet.getString("zartsuulsan_hemjee"));
//                    entity.setZartsuulsanHemjee_en(resultSet.getString("zartsuulsan_hemjee"));
//                    entity.setAimag(resultSet.getString("aimag"));
//                    entity.setSum(resultSet.getString("sum"));
//                    entity.setAimagNer(resultSet.getString("cityname"));
//                    entity.setSumNer(resultSet.getString("sumname"));
//
//                    form51EntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return form51EntitieList;
    }//


    @Override
    public ErrorEntity exportReport(String file, Form51Entity form51) throws SQLException {
        ErrorEntity errorEntity = null;
        String fileName = "hform51";
        try (Connection connection = boneCP.getConnection()){
            String JRXML_PATH = Config.getJrxmlPath();
            String REPORT_PATH = Config.getReportPath();

            try {
                Map parameters = new HashMap();

                String reportTitle = "МАЛ Эмнэлгийн УРЬДЧИЛСАН СЭРГИЙЛЭХ АРГА ХЭМЖЭЭНИЙ ЭМ, ТАРИЛГЫН ЗАРЦУУЛАЛТ, ҮЛДЭГДЛИЙН $$YEAR$$ ОНЫ НЭГДСЭН ТАЙЛАН",
                        currentYear = "$$YEAR$$ он №....",
                        printDate = "$$YEAR$$ оны $$MONTH$$ сарын $$DAY$$";

                Calendar now = Calendar.getInstance();

                int year = now.get(Calendar.YEAR);
                int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
                int day = now.get(Calendar.DAY_OF_MONTH);

                printDate = printDate.replace("$$YEAR$$", String.valueOf(year));
                printDate = printDate.replace("$$MONTH$$", String.valueOf(month));
                printDate = printDate.replace("$$DAY$$", String.valueOf(day));

                currentYear = currentYear.replace("$$YEAR$$", String.valueOf(year));

                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(new Date(form51.getRecordDate().getTime()));

                reportTitle = reportTitle.replace("$$YEAR$$", String.valueOf(calendar1.get(Calendar.YEAR)));

                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(new Date(form51.getSearchRecordDate().getTime()));

                if (calendar2.get(Calendar.MONTH) < 4){
                    reportTitle = reportTitle.replace("$$SEASON$$", "1");
                }
                else if (calendar2.get(Calendar.MONTH) < 7){
                    reportTitle = reportTitle.replace("$$SEASON$$", "2");
                }
                else if (calendar2.get(Calendar.MONTH) < 10){
                    reportTitle = reportTitle.replace("$$SEASON$$", "3");
                }
                else if (calendar2.get(Calendar.MONTH) <= 12){
                    reportTitle = reportTitle.replace("$$SEASON$$", "4");
                }

                parameters.put("start_date", form51.getRecordDate());
                parameters.put("end_date", form51.getSearchRecordDate());
                parameters.put("citycode", (form51.getAimag() != null && !form51.getAimag().isEmpty()) ? form51.getAimag() : null);
                parameters.put("sumcode", (form51.getSum() != null && !form51.getSum().isEmpty()) ? form51.getSum() : null);
                parameters.put("currentyear", currentYear);

                ICityService cityService = new CityServiceImpl(boneCP);
                City tmpCity = new City();
                tmpCity.setCode(form51.getAimag());
                List<City> clist = cityService.selectAll(tmpCity);

                if (clist != null && clist.size() == 1) {
                    parameters.put("aimagner", clist.get(0).getCityname());
                } else {
                    parameters.put("aimagner", (form51.getAimagNer() == null) ? "" : form51.getAimagNer());
                }
                ISumService sumService = new SumServiceImpl(boneCP);
                Sum tmpSum = new Sum();
                tmpSum.setCitycode(form51.getAimag());
                tmpSum.setSumcode(form51.getSum());
                List<Sum> sumList = sumService.selectAll(tmpSum);

                if (sumList != null && sumList.size() == 1){
                    parameters.put("sumner", sumList.get(0).getSumname());
                }
                else{
                    parameters.put("sumner", (form51.getSumNer() == null) ? "" : form51.getSumNer());
                }

                parameters.put("printdate", printDate);
                parameters.put("reporttitle", reportTitle);
                parameters.put("creby", (form51.getCreby() != null && !form51.getCreby().isEmpty()) ? form51.getCreby() : null);

                PdfExcelUtil.generateReport(file, JRXML_PATH, REPORT_PATH, fileName, parameters, connection);


                System.out.println("File Generated");
            }catch (Exception ex){
                ex.printStackTrace();
            }
            finally {
                if (connection != null)connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1001));
            if(file.equals("pdf")){
                errorEntity.setErrText(fileName + ".pdf");
            }else if(file.equals("xls")){
                errorEntity.setErrText(fileName + ".xls");
            }
        }
        return errorEntity;
    }

    @Override
    public BasicDBObject getColHighChartData(Form51Entity entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();
        List<String> categories = new ArrayList<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }

        try {

            IComboService comboService = new ComboServiceImpl(boneCP);

            List<BasicDBObject> cmbType = comboService.getComboNames("MALCATEGORY", "MN");
            System.out.println(cmbType);
            //Saraar n haruulah
            int month = calendar1.get(Calendar.MONTH) + 1;
            int year1 = calendar1.get(Calendar.YEAR);
            int year2 = calendar2.get(Calendar.YEAR);

            try (Connection connection = boneCP.getConnection()) {
                String query_sel = "SELECT `niitMalToo`, sum(`bogToo`) AS bogToo,  sum(`bodToo`) AS bodToo," +
                        "sum(`tolToo`) AS tolToo, `hform51`.`aimag`, `hform51`.`sum`  " +
                        "FROM `hospital`.`hform51`  " +
                        "WHERE   ";

                if (entity != null && entity.getAimag() != null &&!entity.getAimag().isEmpty()) {
                    query_sel += " `hform51`.`aimag` = ? AND ";
                }
                if (entity != null && entity.getSum() != null && !entity.getSum().isEmpty()) {
                    query_sel += " `hform51`.`sum` = ? AND ";
                }
                if (entity != null && !entity.getDelflg().isEmpty()) {
                    query_sel += " `hform51`.`delflg` = ? AND ";
                }
                if (entity != null && !entity.getActflg().isEmpty()) {
                    query_sel += " `hform51`.`actflg` = ? AND ";
                }
                if (entity != null && !entity.getCreby().isEmpty()) {
                    query_sel += " `hform51`.`cre_by` = ? AND ";
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                    query_sel += " (`recorddate` BETWEEN ? AND ?)";
                }

                HashMap<String, List<Double>> resultArray = new HashMap<>();


                do {
                    //do process
                    categories.add(year1 + "-" + month);

                    try {
                        PreparedStatement statement = connection.prepareStatement(query_sel);

                        java.sql.Date stdate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-01 00:00:00").getTime());

                        java.sql.Date enddate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-31 00:00:00").getTime());

                        if (month == 12 && year1 < year2) {
                            month = 1;
                            year1++;
                        } else {
                            month++;
                        }

                        int stindex = 1;

                        if (entity != null && entity.getAimag() != null  && !entity.getAimag().isEmpty()) {
                            statement.setString(stindex++, entity.getAimag());
                        }
                        if (entity != null && entity.getSum() != null  && !entity.getSum().isEmpty()) {
                            statement.setString(stindex++, entity.getSum());
                        }
                        if (entity != null && !entity.getDelflg().isEmpty()) {
                            statement.setString(stindex++, entity.getDelflg());
                        }
                        if (entity != null && !entity.getActflg().isEmpty()) {
                            statement.setString(stindex++, entity.getActflg());
                        }
                        if (entity != null && !entity.getCreby().isEmpty()) {
                            statement.setString(stindex++, entity.getCreby());
                        }
                        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                            statement.setDate(stindex++, stdate);
                            statement.setDate(stindex++, enddate);
                        }


                        ResultSet resultSet = statement.executeQuery();

                        HashMap<String, String> hashMap = new HashMap<>();

                        while (resultSet.next()) {
                            String bog = resultSet.getString("bogToo");
                            String bod = resultSet.getString("bodToo");
                            String tul = resultSet.getString("tolToo");
                            hashMap.put("bog", bog);
                            hashMap.put("bod", bod);
                            hashMap.put("tul", tul);
                        }//end while

                        List<Double> list = new ArrayList<>();
                        for (int i = 0; i < cmbType.size(); i++) {
                            BasicDBObject obj = cmbType.get(i);

                            if (obj.get("data") == null){
                                //new record
                                list = new ArrayList<>();
                                String val = hashMap.get(obj.getString("id"));
                                if (val == null) list.add(0.0);
                                else if (val.isEmpty()) list.add(0.0);
                                else list.add(Double.parseDouble(val));
                                obj.put("data", list);
                            }else{
                                //already exist

                                list = (List<Double>) obj.get("data");
                                String val = hashMap.get(obj.getString("id"));
                                if (val == null) list.add(0.0);
                                else if (val.isEmpty()) list.add(0.0);
                                else list.add(Double.parseDouble(val));
                            }
                        }

                        if (statement != null) statement.close();
                        if (resultSet != null) resultSet.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();

                        break;
                    }

                    ///
                } while ((month <= calendar2.get(Calendar.MONTH) + 1 && year1 <= year2) || year1 < year2);

                basicDBObject.put("categories", categories);
                basicDBObject.put("series", cmbType);

                connection.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }

    @Override
    public BasicDBObject getPieHighChartData(Form51Entity entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }

        String query_sel = "SELECT `niitMalToo`, sum(`bogToo`) AS bogToo,  sum(`bodToo`) AS bodToo," +
                "sum(`tolToo`) AS tolToo, `hform51`.`aimag`, `hform51`.`sum`  " +
                "FROM `hospital`.`hform51`  " +
                "WHERE   ";

        if (entity != null && entity.getAimag() != null &&!entity.getAimag().isEmpty()) {
            query_sel += " `hform51`.`aimag` = ? AND ";
        }
        if (entity != null && entity.getSum() != null && !entity.getSum().isEmpty()) {
            query_sel += " `hform51`.`sum` = ? AND ";
        }
        if (entity != null && !entity.getDelflg().isEmpty()) {
            query_sel += " `hform51`.`delflg` = ? AND ";
        }
        if (entity != null && !entity.getActflg().isEmpty()) {
            query_sel += " `hform51`.`actflg` = ? AND ";
        }
        if (entity != null && !entity.getCreby().isEmpty()) {
            query_sel += " `hform51`.`cre_by` = ? AND ";
        }
        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
            query_sel += " (`recorddate` BETWEEN ? AND ?)";
        }

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                int stindex = 1;

                if (entity != null && entity.getAimag() != null  && !entity.getAimag().isEmpty()) {
                    statement.setString(stindex++, entity.getAimag());
                }
                if (entity != null && entity.getSum() != null  && !entity.getSum().isEmpty()) {
                    statement.setString(stindex++, entity.getSum());
                }
                if (entity != null && !entity.getDelflg().isEmpty()) {
                    statement.setString(stindex++, entity.getDelflg());
                }
                if (entity != null && !entity.getActflg().isEmpty()) {
                    statement.setString(stindex++, entity.getActflg());
                }
                if (entity != null && !entity.getCreby().isEmpty()) {
                    statement.setString(stindex++, entity.getCreby());
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                    statement.setDate(stindex++, entity.getRecordDate());
                    statement.setDate(stindex++, entity.getSearchRecordDate());
                }
                ResultSet resultSet = statement.executeQuery();

                List<BasicDBObject> list = new ArrayList<>();

                while (resultSet.next()) {
                    BasicDBObject entity1 = new BasicDBObject();
                    BasicDBObject entity2 = new BasicDBObject();
                    BasicDBObject entity3 = new BasicDBObject();
                    entity1.put("name", "Бог");
                    entity1.put("y", resultSet.getDouble("bogToo"));
                    entity2.put("name", "Бод");
                    entity2.put("y", resultSet.getDouble("bodToo"));
                    entity3.put("name", "Төл");
                    entity3.put("y", resultSet.getDouble("tolToo"));
                    list.add(entity1);
                    list.add(entity2);
                    list.add(entity3);
                }

                basicDBObject.put("data", list);

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }
}
