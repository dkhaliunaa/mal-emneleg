package com.hospital.app;

import com.model.hos.ErrorEntity;
import com.model.hos.NewsEntity;
import java.sql.SQLException;
import java.util.List;

public interface INewsService {

    /**
     * Insert new data
     *
     *
     * @param news
     * @return
     * @throws Exception
     */
    ErrorEntity insertNewData(NewsEntity news) throws SQLException;

    /**
     * Select all
     *
     * @param news
     * @return
     * @throws SQLException
     */
    List<NewsEntity> getListData(NewsEntity news) throws SQLException;


    /**
     * update form data
     *
     * @param news
     * @return
     * @throws SQLException
     */
    ErrorEntity updateData(NewsEntity news) throws SQLException;


    Integer getNewsCount() throws SQLException;

}