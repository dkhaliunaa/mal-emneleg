package com.hospital.app;

import com.model.hos.ErrorEntity;
import com.model.hos.GalzuuFormEntity;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public interface IGalzuuFormService {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    /**
     * Insert new data
     *
     *
     * @param entity
     * @return
     * @throws Exception
     */
    ErrorEntity insertNewData(GalzuuFormEntity entity) throws SQLException;

    /**
     * Select all
     *
     * @param entity
     * @return
     * @throws SQLException
     */
    List<GalzuuFormEntity> getListData(GalzuuFormEntity entity) throws SQLException;


    /**
     * update form data
     *
     * @param entity
     * @return
     * @throws SQLException
     */
    ErrorEntity updateData(GalzuuFormEntity entity) throws SQLException;

    ErrorEntity exportReport(String file, GalzuuFormEntity entity) throws SQLException;
}
