package com.hospital.app;

import com.model.hos.ErrorEntity;
import com.model.hos.Form08HavsraltEntity;
import com.model.hos.Form46Entity;
import com.mongodb.BasicDBObject;

import java.sql.SQLException;
import java.util.List;

public interface IForm46Service {

    /**
     * Insert new data
     *
     *
     * @param form46
     * @return
     * @throws Exception
     */
    ErrorEntity insertNewData(Form46Entity form46) throws SQLException;

    /**
     * Select all
     *
     * @param form46
     * @return
     * @throws SQLException
     */
    List<Form46Entity> getListData(Form46Entity form46) throws SQLException;


    /**
     * update form data
     *
     * @param form46
     * @return
     * @throws SQLException
     */
    ErrorEntity updateData(Form46Entity form46) throws SQLException;


    List<Form46Entity> getPieChartData(Form46Entity form46Entity) throws SQLException;


    ErrorEntity exportReport(String file, Form46Entity form46) throws SQLException;
    BasicDBObject getColHighChartData(Form46Entity Form46Entity) throws SQLException;

    BasicDBObject getPieHighChartData(Form46Entity Form46Entity) throws SQLException;
}
