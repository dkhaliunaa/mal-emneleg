package com.hospital.app;

import com.model.hos.ErrorEntity;
import com.model.hos.Form52Entity;
import com.model.hos.Form53Entity;
import com.mongodb.BasicDBObject;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public interface IForm53Service {

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * Insert new data
     *
     *
     * @param form53
     * @return
     * @throws Exception
     */
    ErrorEntity insertNewData(Form53Entity form53) throws SQLException;

    /**
     * Select all
     *
     * @param form53
     * @return
     * @throws SQLException
     */
    List<Form53Entity> getListData(Form53Entity form53) throws SQLException;


    /**
     * update form data
     *
     * @param form53
     * @return
     * @throws SQLException
     */
    ErrorEntity updateData(Form53Entity form53) throws SQLException;


    List<Form53Entity> getPieChartData(Form53Entity form53Entity) throws SQLException;


    ErrorEntity exportReport(String file, Form53Entity form53) throws SQLException;

    BasicDBObject getColHighChartData(Form53Entity Form53Entity) throws SQLException;

    BasicDBObject getPieHighChartData(Form53Entity Form53Entity) throws SQLException;

}
