package com.hospital.app;


import com.Config;
import com.enumclass.ErrorType;
import com.jolbox.bonecp.BoneCP;
import com.model.hos.*;
import com.mongodb.BasicDBObject;
import com.rpc.PdfExcelUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public class Form62ServiceImpl implements IForm62Service {
    /**
     * Objects
     */

    private BoneCP boneCP;
    private ErrorEntity errObj;

    public Form62ServiceImpl(BoneCP boneCP) {
        this.boneCP = boneCP;
    }

    @Override
    public ErrorEntity insertNewData(Form62Entity form62) throws SQLException {
        PreparedStatement statement = null;

        if (form62 == null){
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        String query_login = "INSERT INTO `hospital`.`hform62` " +
                "(`ajAhuinNer`, " +
                "`ajAhuinNer_en`, " +
                "`ezenNer`, " +
                "`ezenNer_en`, " +
                "`mergejil`, " +
                "`mergejil_en`, " +
                "`ihEmch`, " +
                "`ihEmch_en`, " +
                "`bagaEmch`, " +
                "`bagaEmch_en`, " +
                "`malZuich`, " +
                "`malZuich_en`, " +
                "`orhToo`," +
                "`malToo`, " +
                "`negjMalToo`," +
                "`mergejiltenMalToo`, " +
                "`bair`," +
                "`bair_en`, " +
                "`unaa`," +
                "`unaa_en`, " +
                "`ontsSain`," +
                "`ontsSain_en`, " +
                "`delflg`, " +
                "`actflg`, " +
                "`mod_at`, " +
                "`mod_by`, " +
                "`cre_at`, " +
                "`recorddate`, " +
                "`cre_by`," +
                "`aimag`," +
                "`sum`," +
                "`aimag_en`," +
                "`sum_en`)" +
                "VALUES " +
                "(? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? ,? , ? , ? , ? , ? , ? ,? , ? , ? , ? , ? , ? , ? , ? , ? , \n" +
                "? , ? , ? , ? , ?,? )";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, form62.getAjAhuinNer());
                statement.setString(2, form62.getAjAhuinNer_en());
                statement.setString(3, form62.getEzenNer());
                statement.setString(4, form62.getEzenNer_en());
                statement.setString(5, form62.getMergejil());
                statement.setString(6, form62.getMergejil_en());
                statement.setString(7, form62.getIhEmch());
                statement.setString(8, form62.getIhEmch_en());
                statement.setString(9, form62.getBagaEmch());
                statement.setString(10, form62.getBagaEmch_en());
                statement.setString(11, form62.getMalZuich());
                statement.setString(12, form62.getMalZuich_en());
                statement.setLong(13, form62.getOrhToo());
                statement.setLong(14, form62.getMalToo());
                statement.setLong(15, form62.getNegjMalToo());
                statement.setBigDecimal(16, form62.getMergejiltenMalToo());
                statement.setString(17, form62.getBair());
                statement.setString(18, form62.getBair_en());
                statement.setString(19, form62.getUnaa());
                statement.setString(20, form62.getUnaa_en());
                statement.setString(21, form62.getOntsSain());
                statement.setString(22, form62.getOntsSain_en());
                statement.setString(23, form62.getDelflg());
                statement.setString(24, form62.getActflg());
                statement.setDate(25, form62.getModAt());
                statement.setString(26, form62.getModby());
                statement.setDate(27, form62.getCreat());
                statement.setDate(28, form62.getRecordDate());
                statement.setString(29, form62.getCreby());
                statement.setString(30, form62.getAimag());
                statement.setString(31, form62.getSum());
                statement.setString(32, form62.getAimag_en());
                statement.setString(33, form62.getSum_en());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));
                    errObj.setErrText("Бичлэг амжилттай хадгалагдлаа.");
                    errObj.setErrDesc("Бичлэг амжилттай хадгалагдлаа.");
                    return errObj;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }

        } catch (Exception ex) {

        }

        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1057));
        errObj.setErrText("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");
        errObj.setErrDesc("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");

        return errObj;
    }

    @Override
    public List<Form62Entity> getListData(Form62Entity form62) throws SQLException {
        int stIndex = 1;
        List<Form62Entity> form62EntitieList = null;

        String query = "SELECT `hform62`.`id`, " +
                "    `hform62`.`ajAhuinNer`, " +
                "    `hform62`.`ajAhuinNer_en`, " +
                "    `hform62`.`ezenNer`, " +
                "    `hform62`.`ezenNer_en`, " +
                "    `hform62`.`mergejil`, " +
                "    `hform62`.`mergejil_en`, " +
                "    `hform62`.`ihEmch`, " +
                "    `hform62`.`ihEmch_en`, " +
                "    `hform62`.`bagaEmch`, " +
                "    `hform62`.`bagaEmch_en`, " +
                "    `hform62`.`malZuich`, " +
                "    `hform62`.`malZuich_en`, " +
                "    `hform62`.`orhToo`, " +
                "    `hform62`.`malToo`, " +
                "    `hform62`.`negjMalToo`, " +
                "    `hform62`.`mergejiltenMalToo`, " +
                "    `hform62`.`bair`, " +
                "    `hform62`.`bair_en`, " +
                "    `hform62`.`unaa`, " +
                "    `hform62`.`unaa_en`, " +
                "    `hform62`.`ontsSain`, " +
                "    `hform62`.`ontsSain_en`, " +
                "    `hform62`.`delflg`, " +
                "    `hform62`.`actflg`, " +
                "    `hform62`.`mod_at`, " +
                "    `hform62`.`mod_by`, " +
                "    `hform62`.`cre_at`, " +
                "    `hform62`.`recorddate`, " +
                "    `hform62`.`cre_by`, " +
                "    `hform62`.`aimag`, " +
                "    `hform62`.`sum`, " +
                "    `hform62`.`aimag_en`, " +
                "    `hform62`.`sum_en`, " +
                "    c.cityname, c.cityname_en, " +
                "    `h_sum`.sumname, `h_sum`.sumname_en, " +
                "    `h_sum`.latitude as sum_latitude, `h_sum`.longitude as sum_longitude  " +
                "FROM `hospital`.`hform62` INNER JOIN `h_city` AS c " +
                "ON c.code = `hform62`.aimag " +
                "INNER JOIN `h_sum` " +
                "ON `h_sum`.sumcode = `hform62`.sum " +
                "WHERE ";

        if (form62.getAjAhuinNer() != null && !form62.getAjAhuinNer().isEmpty()) {
            query += " ajAhuinNer = ? AND ";
        }

        if (form62.getAjAhuinNer_en() != null && !form62.getAjAhuinNer_en().isEmpty()) {
            query += " ajAhuinNer_en = ? AND ";
        }

        /* NER */
        if (form62.getEzenNer() != null && !form62.getEzenNer().isEmpty()) {
            query += " ezenNer = ? AND ";
        }

        if (form62.getEzenNer_en() != null && !form62.getEzenNer_en().isEmpty()) {
            query += " ezenNer_en = ? AND ";
        }

        /* Zartsuulsan hemjee */
        if (form62.getMergejil() != null && !form62.getMergejil().isEmpty()) {
            query += " mergejil = ? AND ";
        }

        if (form62.getMergejil_en() != null && !form62.getMergejil_en().isEmpty()) {
            query += " mergejil_en = ? AND ";
        }

        /* MAL TURUL */
        if (form62.getIhEmch() != null && !form62.getIhEmch().isEmpty()) {
            query += " ihEmch = ? AND ";
        }

        if (form62.getIhEmch_en() != null && !form62.getIhEmch_en().isEmpty()) {
            query += " ihEmch_en = ? AND ";
        }

        /* Tuluvlugu */
        if (form62.getBagaEmch() != null && !form62.getBagaEmch().isEmpty()) {
            query += " bagaEmch = ? AND ";
        }

        if (form62.getBagaEmch_en() != null && !form62.getBagaEmch_en().isEmpty()) {
            query += " bagaEmch_en = ? AND ";
        }

        /* Guitsetgel */
        if (form62.getMalZuich() != null && !form62.getMalZuich().isEmpty()) {
            query += " malZuich = ? AND ";
        }

        if (form62.getMalZuich_en() != null && !form62.getMalZuich_en().isEmpty()) {
            query += " malZuich_en = ? AND ";
        }

        /* Huwi */
        if (form62.getOrhToo() != null && !form62.getOrhToo().toString().isEmpty() && form62.getOrhToo().longValue() != 0) {
            query += " orhToo = ? AND  ";
        }

        if (form62.getMalToo() != null && !form62.getMalToo().toString().isEmpty() && form62.getMalToo().longValue() != 0) {
            query += " malToo = ? AND ";
        }

        /* Tuluvlugu */
        if (form62.getNegjMalToo() != null && !form62.getNegjMalToo().toString().isEmpty() && form62.getNegjMalToo().longValue() != 0) {
            query += " negjMalToo = ? AND ";
        }
        if (form62.getMergejiltenMalToo() != null && !form62.getMergejiltenMalToo().toString().isEmpty() && form62.getMergejiltenMalToo().longValue() != 0) {
            query += " mergejiltenMalToo = ? AND ";
        }

        /* Guitsetgel */
        if (form62.getBair() != null && !form62.getBair().isEmpty()) {
            query += " bair = ? AND ";
        }
        if (form62.getBair_en() != null && !form62.getBair_en().toString().isEmpty()) {
            query += " bair_en = ? AND ";
        }

        /* Huwi */
        if (form62.getUnaa() != null && !form62.getUnaa().isEmpty()) {
            query += " unaa = ? AND  ";
        }

        if (form62.getUnaa_en() != null && !form62.getUnaa_en().isEmpty()) {
            query += " unaa_en = ? AND  ";
        }

        if (form62.getOntsSain() != null && !form62.getOntsSain().isEmpty()) {
            query += " ontsSain = ? AND  ";
        }

        if (form62.getOntsSain_en() != null && !form62.getOntsSain_en().isEmpty()) {
            query += " ontsSain_en = ? AND  ";
        }

        if (form62 != null && form62.getAimag() != null && !form62.getAimag().isEmpty()) {
            query += " `aimag` = ? AND ";
        }
        if (form62 != null && form62.getSum() != null && !form62.getSum().isEmpty()) {
            query += " `sum` = ? AND ";
        }

        if (form62 != null && form62.getRecordDate() != null && form62.getSearchRecordDate() != null) {
            query += " (`recorddate` BETWEEN ? AND ?) AND ";
        }

        if (form62 != null && form62.getCreby() != null && !form62.getCreby().isEmpty()) {
            query += " `hform62`.`cre_by` = ? AND ";
        }

        query += " `hform62`.`delflg` = 'N'";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query);

                if (form62.getAjAhuinNer() != null && !form62.getAjAhuinNer().isEmpty()) {
                    statement.setString(stIndex++, form62.getAjAhuinNer());
                }

                if (form62.getAjAhuinNer_en() != null && !form62.getAjAhuinNer_en().isEmpty()) {
                    statement.setString(stIndex++, form62.getAjAhuinNer_en());
                }

            /* NER */
                if (form62.getEzenNer() != null && !form62.getEzenNer().toString().isEmpty()) {
                    statement.setString(stIndex++, form62.getEzenNer());
                }
                if (form62.getEzenNer_en() != null && !form62.getEzenNer_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form62.getEzenNer_en());
                }

            /* Zartsuulsan hemjee */
                if (form62.getMergejil() != null && !form62.getMergejil().toString().isEmpty()) {
                    statement.setString(stIndex++, form62.getMergejil());
                }
                if (form62.getMergejil_en() != null && !form62.getMergejil_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form62.getMergejil_en());
                }

            /* MAL TURUL */
                if (form62.getIhEmch() != null && !form62.getIhEmch().isEmpty()) {
                    statement.setString(stIndex++, form62.getIhEmch());
                }
                if (form62.getIhEmch_en() != null && !form62.getIhEmch_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form62.getIhEmch_en());
                }

            /* Tuluvlugu */
                if (form62.getBagaEmch() != null && !form62.getBagaEmch().isEmpty()) {
                    statement.setString(stIndex++, form62.getBagaEmch());
                }
                if (form62.getBagaEmch_en() != null && !form62.getBagaEmch_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form62.getBagaEmch_en());
                }

            /* Guitsetgel */
                if (form62.getMalZuich() != null && !form62.getMalZuich().isEmpty()) {
                    statement.setString(stIndex++, form62.getMalZuich());
                }
                if (form62.getMalZuich_en() != null && !form62.getMalZuich_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form62.getMalZuich_en());
                }

            /* Huwi */
                if (form62.getOrhToo() != null && !form62.getOrhToo().toString().isEmpty() && form62.getOrhToo().longValue() != 0) {
                    statement.setLong(stIndex++, form62.getOrhToo());
                }
                if (form62.getMalToo() != null && !form62.getMalToo().toString().isEmpty() && form62.getMalToo().longValue() != 0) {
                    statement.setLong(stIndex++, form62.getMalToo());
                }

                if (form62.getMergejiltenMalToo() != null && !form62.getMergejiltenMalToo().toString().isEmpty() && form62.getMergejiltenMalToo().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form62.getMergejiltenMalToo());
                }
                if (form62.getBair() != null && !form62.getBair().isEmpty()) {
                    statement.setString(stIndex++, form62.getBair());
                }


                if (form62.getUnaa() != null && !form62.getUnaa().isEmpty()) {
                    statement.setString(stIndex++, form62.getUnaa());
                }

                if (form62.getUnaa_en() != null && !form62.getUnaa_en().isEmpty()) {
                    statement.setString(stIndex++, form62.getUnaa_en());
                }

                if (form62.getOntsSain() != null && !form62.getOntsSain().isEmpty()) {
                    statement.setString(stIndex++, form62.getOntsSain());
                }

                if (form62.getOntsSain_en() != null && !form62.getOntsSain_en().isEmpty()) {
                    statement.setString(stIndex++, form62.getOntsSain_en());
                }

                if (form62 != null && form62.getAimag() != null && !form62.getAimag().isEmpty()) {
                    statement.setString(stIndex++, form62.getAimag());
                }
                if (form62 != null && form62.getSum() != null && !form62.getSum().isEmpty()) {
                    statement.setString(stIndex++, form62.getSum());
                }

                if (form62 != null && form62.getRecordDate() != null && form62.getSearchRecordDate() != null) {
                    statement.setDate(stIndex++, form62.getRecordDate());
                    statement.setDate(stIndex++, form62.getSearchRecordDate());
                }
                if (form62 != null && form62.getCreby() != null && !form62.getCreby().isEmpty()) {
                    statement.setString(stIndex++, form62.getCreby());
                }
                ResultSet resultSet = statement.executeQuery();

                form62EntitieList = new ArrayList<>();
                IComboService comboService = new ComboServiceImpl(boneCP);
                List<BasicDBObject> unelgee = comboService.getComboVals("RES", "MN");
                int index = 1;
                while (resultSet.next()) {
                    Form62Entity entity = new Form62Entity();

                    entity.setId(resultSet.getLong("id"));
                    entity.setAjAhuinNer(resultSet.getString("ajAhuinNer"));
                    entity.setAjAhuinNer_en(resultSet.getString("ajAhuinNer_en"));
                    entity.setEzenNer(resultSet.getString("ezenNer"));
                    entity.setEzenNer_en(resultSet.getString("ezenNer_en"));
                    entity.setMergejil(resultSet.getString("mergejil"));
                    entity.setMergejil_en(resultSet.getString("mergejil_en"));
                    entity.setIhEmch(resultSet.getString("ihEmch"));
                    entity.setIhEmch_en(resultSet.getString("ihEmch_en"));
                    entity.setBagaEmch(resultSet.getString("bagaEmch"));
                    entity.setBagaEmch_en(resultSet.getString("bagaEmch_en"));
                    entity.setMalZuich(resultSet.getString("malZuich"));
                    entity.setMalZuich_en(resultSet.getString("malZuich_en"));
                    entity.setOrhToo(resultSet.getLong("orhToo"));
                    entity.setMalToo(resultSet.getLong("malToo"));
                    entity.setNegjMalToo(resultSet.getLong("negjMalToo"));
                    entity.setMergejiltenMalToo(resultSet.getBigDecimal("mergejiltenMalToo"));
                    entity.setBair(resultSet.getString("bair"));
                    entity.setBair_en(resultSet.getString("bair_en"));
                    entity.setUnaa(resultSet.getString("unaa"));
                    entity.setUnaa_en(resultSet.getString("unaa_en"));
                    entity.setOntsSain(resultSet.getString("ontsSain"));
                    entity.setOntsSain_en(resultSet.getString("ontsSain_en"));

                    entity.setDelflg(resultSet.getString("delflg"));
                    entity.setActflg(resultSet.getString("actflg"));
                    entity.setModAt(resultSet.getDate("mod_at"));
                    entity.setModby(resultSet.getString("mod_by"));
                    entity.setCreat(resultSet.getDate("cre_at"));
                    entity.setRecordDate(resultSet.getDate("recorddate"));
                    entity.setCreby(resultSet.getString("cre_by"));
                    entity.setAimag(resultSet.getString("aimag"));
                    entity.setSum(resultSet.getString("sum"));
                    entity.setAimag_en(resultSet.getString("aimag_en"));
                    entity.setSum_en(resultSet.getString("sum_en"));
                    entity.put("sumname", resultSet.getString("sumname"));

                    entity.put("sum_latitude", resultSet.getString("sum_latitude"));
                    entity.put("sum_longitude", resultSet.getString("sum_longitude"));
                    entity.put("index",index++);

                    entity.put("onts_Sain", getTypeNer(unelgee, resultSet.getString("ontsSain")));

                    form62EntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return form62EntitieList;
    }

    private String getTypeNer(List<BasicDBObject> cmbType, String mal_turul_mn) {
        for (BasicDBObject obj :
                cmbType) {
            if (obj.get("id") != null && mal_turul_mn != null && obj.getString("id").equalsIgnoreCase(mal_turul_mn)) {
                return obj.getString("name");
            }
        }

        return "";
    }

    @Override
    public ErrorEntity updateData(Form62Entity form62) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (form62 == null) {
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        query = "UPDATE `hospital`.`hform62` " +
                "SET     ";

        if (form62.getAjAhuinNer() != null && !form62.getAjAhuinNer().isEmpty()) {
            query += " ajAhuinNer = ? , ";
        }

        if (form62.getAjAhuinNer_en() != null && !form62.getAjAhuinNer_en().isEmpty()) {
            query += " ajAhuinNer_en = ? , ";
        }

        /* NER */
        if (form62.getEzenNer() != null && !form62.getEzenNer().isEmpty()) {
            query += " ezenNer = ? , ";
        }

        if (form62.getEzenNer_en() != null && !form62.getEzenNer_en().isEmpty()) {
            query += " ezenNer_en = ? , ";
        }

        /* Zartsuulsan hemjee */
        if (form62.getMergejil() != null && !form62.getMergejil().isEmpty()) {
            query += " mergejil = ? , ";
        }

        if (form62.getMergejil_en() != null && !form62.getMergejil_en().isEmpty()) {
            query += " mergejil_en = ? , ";
        }

        /* MAL TURUL */
        if (form62.getIhEmch() != null && !form62.getIhEmch().isEmpty()) {
            query += " ihEmch = ? , ";
        }

        if (form62.getIhEmch_en() != null && !form62.getIhEmch_en().isEmpty()) {
            query += " ihEmch_en = ? , ";
        }

        /* Tuluvlugu */
        if (form62.getBagaEmch() != null && !form62.getBagaEmch().isEmpty()) {
            query += " bagaEmch = ? , ";
        }

        if (form62.getBagaEmch_en() != null && !form62.getBagaEmch_en().isEmpty()) {
            query += " bagaEmch_en = ? , ";
        }

        /* Guitsetgel */
        if (form62.getMalZuich() != null && !form62.getMalZuich().isEmpty()) {
            query += " malZuich = ? , ";
        }

        if (form62.getMalZuich_en() != null && !form62.getMalZuich_en().isEmpty()) {
            query += " malZuich_en = ? , ";
        }

        /* Huwi */
        if (form62.getOrhToo() != null && !form62.getOrhToo().toString().isEmpty() && form62.getOrhToo().longValue() != 0) {
            query += " orhToo = ? ,  ";
        }

        if (form62.getMalToo() != null && !form62.getMalToo().toString().isEmpty() && form62.getMalToo().longValue() != 0) {
            query += " malToo = ? , ";
        }

        /* Tuluvlugu */
        if (form62.getNegjMalToo() != null && !form62.getNegjMalToo().toString().isEmpty() && form62.getNegjMalToo().longValue() != 0) {
            query += " negjMalToo = ? , ";
        }
        if (form62.getMergejiltenMalToo() != null && !form62.getMergejiltenMalToo().toString().isEmpty() && form62.getMergejiltenMalToo().longValue() != 0) {
            query += " mergejiltenMalToo = ? , ";
        }

        /* Guitsetgel */
        if (form62.getBair() != null && !form62.getBair().isEmpty()) {
            query += " bair = ? , ";
        }
        if (form62.getBair_en() != null && !form62.getBair_en().toString().isEmpty()) {
            query += " bair_en = ? , ";
        }

        /* Huwi */
        if (form62.getUnaa() != null && !form62.getUnaa().isEmpty()) {
            query += " unaa = ? ,  ";
        }

        if (form62.getUnaa_en() != null && !form62.getUnaa_en().isEmpty()) {
            query += " unaa_en = ? ,  ";
        }

        if (form62.getOntsSain() != null && !form62.getOntsSain().isEmpty()) {
            query += " ontsSain = ? ,  ";
        }

        if (form62.getOntsSain_en() != null && !form62.getOntsSain_en().isEmpty()) {
            query += " ontsSain_en = ? ,  ";
        }

        /* Delete flag */
        if (form62.getDelflg() != null && !form62.getDelflg().toString().isEmpty()) {
            query += " delflg = ?, ";
        }

        /* Active flag */
        if (form62.getActflg() != null && !form62.getActflg().toString().isEmpty()) {
            query += " actflg = ?, ";
        }
        if (form62 != null && form62.getRecordDate() != null) {
            query += " `recorddate` = ?, ";
        }
        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";
        query += " WHERE  ID   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (form62.getAjAhuinNer() != null && !form62.getAjAhuinNer().isEmpty()) {
                    statement.setString(stIndex++, form62.getAjAhuinNer());
                }

                if (form62.getAjAhuinNer_en() != null && !form62.getAjAhuinNer_en().isEmpty()) {
                    statement.setString(stIndex++, form62.getAjAhuinNer_en());
                }

            /* NER */
                if (form62.getEzenNer() != null && !form62.getEzenNer().toString().isEmpty()) {
                    statement.setString(stIndex++, form62.getEzenNer());
                }
                if (form62.getEzenNer_en() != null && !form62.getEzenNer_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form62.getEzenNer_en());
                }

            /* Zartsuulsan hemjee */
                if (form62.getMergejil() != null && !form62.getMergejil().toString().isEmpty()) {
                    statement.setString(stIndex++, form62.getMergejil());
                }
                if (form62.getMergejil_en() != null && !form62.getMergejil_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form62.getMergejil_en());
                }

            /* MAL TURUL */
                if (form62.getIhEmch() != null && !form62.getIhEmch().isEmpty()) {
                    statement.setString(stIndex++, form62.getIhEmch());
                }
                if (form62.getIhEmch_en() != null && !form62.getIhEmch_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form62.getIhEmch_en());
                }

            /* Tuluvlugu */
                if (form62.getBagaEmch() != null && !form62.getBagaEmch().isEmpty()) {
                    statement.setString(stIndex++, form62.getBagaEmch());
                }
                if (form62.getBagaEmch_en() != null && !form62.getBagaEmch_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form62.getBagaEmch_en());
                }

            /* Guitsetgel */
                if (form62.getMalZuich() != null && !form62.getMalZuich().isEmpty()) {
                    statement.setString(stIndex++, form62.getMalZuich());
                }
                if (form62.getMalZuich_en() != null && !form62.getMalZuich_en().toString().isEmpty()) {
                    statement.setString(stIndex++, form62.getMalZuich_en());
                }

            /* Huwi */
                if (form62.getOrhToo() != null && !form62.getOrhToo().toString().isEmpty() && form62.getOrhToo().longValue() != 0) {
                    statement.setLong(stIndex++, form62.getOrhToo());
                }
                if (form62.getMalToo() != null && !form62.getMalToo().toString().isEmpty() && form62.getMalToo().longValue() != 0) {
                    statement.setLong(stIndex++, form62.getMalToo());
                }

                if (form62.getMergejiltenMalToo() != null && !form62.getMergejiltenMalToo().toString().isEmpty() && form62.getMergejiltenMalToo().longValue() != 0) {
                    statement.setBigDecimal(stIndex++, form62.getMergejiltenMalToo());
                }
                if (form62.getBair() != null && !form62.getBair().isEmpty()) {
                    statement.setString(stIndex++, form62.getBair());
                }


                if (form62.getUnaa() != null && !form62.getUnaa().isEmpty()) {
                    statement.setString(stIndex++, form62.getUnaa());
                }

                if (form62.getUnaa_en() != null && !form62.getUnaa_en().isEmpty()) {
                    statement.setString(stIndex++, form62.getUnaa_en());
                }

                if (form62.getOntsSain() != null && !form62.getOntsSain().isEmpty()) {
                    statement.setString(stIndex++, form62.getOntsSain());
                }

                if (form62.getOntsSain_en() != null && !form62.getOntsSain_en().isEmpty()) {
                    statement.setString(stIndex++, form62.getOntsSain_en());
                }


            /* Delete flag */
                if (form62.getDelflg() != null && !form62.getDelflg().toString().isEmpty()) {
                    statement.setString(stIndex++, form62.getDelflg());
                }

            /* Active flag */
                if (form62.getActflg() != null && !form62.getActflg().toString().isEmpty()) {
                    statement.setString(stIndex++, form62.getActflg());
                }

                if (form62 != null && form62.getRecordDate() != null) {
                    statement.setDate(stIndex++, form62.getRecordDate());
                }
                statement.setDate(stIndex++, form62.getModAt());
                statement.setString(stIndex++, form62.getModby());
                statement.setLong(stIndex++, form62.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                    errObj.setErrText("Бичлэг амжилттай засагдлаа.");
                    errObj.setErrDesc("Бичлэг амжилттай засагдлаа.");
                    return errObj;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1061));
        errObj.setErrText("Амжилтгүй боллоо. Дахин оролдоно уу.");
        errObj.setErrDesc("Амжилтгүй боллоо. Дахин оролдоно уу.");

        return errObj;
    }

    @Override
    public List<Form62Entity> getPieChartData(Form62Entity form62Entity) throws SQLException {
        int stIndex = 1;
        List<Form62Entity> form62EntitieList = null;

        String query_sel = "SELECT `hform62`.`id`,  " +
                "    `hform62`.`aimag`,  " +
                "    `hform62`.`sum`,  " +
                "    SUM(`hform62`.`zartsuulsan_hemjee_mn`) as zartsuulsan_hemjee,  " +
                "    c.cityname, c.cityname_en,  " +
                "    `h_sum`.sumname, `h_sum`.sumname_en  " +
                "FROM `hospital`.`hform62`  " +
                "INNER JOIN `h_city` AS c  " +
                "ON c.code = `hform62`.aimag  " +
                "INNER JOIN `h_sum`  " +
                "ON `h_sum`.sumcode = `hform62`.sum  " +
                "WHERE `hform62`.`delflg` = 'N' and `hform62`.`actflg` = 'Y' and `hform62`.`aimag` = ?  " +
                "GROUP BY `hform62`.`sum` ";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                if (form62Entity.getAimag() != null && !form62Entity.getAimag().isEmpty()) {
                    statement.setString(stIndex++, form62Entity.getAimag());
                }

                ResultSet resultSet = statement.executeQuery();

                form62EntitieList = new ArrayList<>();

                while (resultSet.next()) {
                    Form62Entity entity = new Form62Entity();

                    entity.setId(resultSet.getLong("id"));
                    entity.setAimag(resultSet.getString("aimag"));
                    entity.setSum(resultSet.getString("sum"));
//                    entity.setAimagNer(resultSet.getString("cityname"));
//                    entity.setSumNer(resultSet.getString("sumname"));

                    form62EntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return form62EntitieList;
    }

    @Override
    public ErrorEntity exportReport(String file, Form62Entity form62) throws SQLException {
        ErrorEntity errorEntity = null;
        String fileName = "hform62";
        try (Connection connection = boneCP.getConnection()) {
            String JRXML_PATH = Config.getJrxmlPath();
            String REPORT_PATH = Config.getReportPath();

            try {
                Map parameters = new HashMap();

                String reportTitle = "МАЛ ЭМНЭЛГИЙН ҮЙЛЧИЛГЭЭНИЙ НЭГЖИЙН $$YEAR$$ ОНЫ МЭДЭЭ",
                        currentYear = "$$YEAR$$ он №....",
                        printDate = "$$YEAR$$ оны $$MONTH$$ сарын $$DAY$$";

                Calendar now = Calendar.getInstance();

                int year = now.get(Calendar.YEAR);
                int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
                int day = now.get(Calendar.DAY_OF_MONTH);

                printDate = printDate.replace("$$YEAR$$", String.valueOf(year));
                printDate = printDate.replace("$$MONTH$$", String.valueOf(month));
                printDate = printDate.replace("$$DAY$$", String.valueOf(day));

                currentYear = currentYear.replace("$$YEAR$$", String.valueOf(year));

                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(new Date(form62.getRecordDate().getTime()));

                reportTitle = reportTitle.replace("$$YEAR$$", String.valueOf(calendar1.get(Calendar.YEAR)));

                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(new Date(form62.getSearchRecordDate().getTime()));

                if (calendar2.get(Calendar.MONTH) < 4) {
                    reportTitle = reportTitle.replace("$$SEASON$$", "1");
                } else if (calendar2.get(Calendar.MONTH) < 7) {
                    reportTitle = reportTitle.replace("$$SEASON$$", "2");
                } else if (calendar2.get(Calendar.MONTH) < 10) {
                    reportTitle = reportTitle.replace("$$SEASON$$", "3");
                } else if (calendar2.get(Calendar.MONTH) <= 12) {
                    reportTitle = reportTitle.replace("$$SEASON$$", "4");
                }

                parameters.put("start_date", form62.getRecordDate());
                parameters.put("end_date", form62.getSearchRecordDate());
                parameters.put("citycode", (form62.getAimag() != null && !form62.getAimag().isEmpty()) ? form62.getAimag() : null);
                parameters.put("sumcode", (form62.getSum() != null && !form62.getSum().isEmpty()) ? form62.getSum() : null);
                parameters.put("currentyear", currentYear);

                ICityService cityService = new CityServiceImpl(boneCP);
                City tmpCity = new City();
                tmpCity.setCode(form62.getAimag());
                List<City> clist = cityService.selectAll(tmpCity);

                if (clist != null && clist.size() == 1) {
                    parameters.put("aimagner", clist.get(0).getCityname());
                } else {
                    parameters.put("aimagner", (form62.getAimagNer() == null) ? "" : form62.getAimagNer());
                }
                ISumService sumService = new SumServiceImpl(boneCP);
                Sum tmpSum = new Sum();
                tmpSum.setCitycode(form62.getAimag());
                tmpSum.setSumcode(form62.getSum());
                List<Sum> sumList = sumService.selectAll(tmpSum);

                if (sumList != null && sumList.size() == 1){
                    parameters.put("sumner", sumList.get(0).getSumname());
                }
                else{
                    parameters.put("sumner", (form62.getSumNer() == null) ? "" : form62.getSumNer());
                }

                parameters.put("printdate", printDate);
                parameters.put("reporttitle", reportTitle);
                parameters.put("creby", (form62.getCreby() != null && !form62.getCreby().isEmpty()) ? form62.getCreby() : null);

                PdfExcelUtil.generateReport(file, JRXML_PATH, REPORT_PATH, fileName, parameters, connection);

                System.out.println("File Generated");
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (connection != null) connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1001));
            if(file.equals("pdf")){
                errorEntity.setErrText(fileName + ".pdf");
            }else if(file.equals("xls")){
                errorEntity.setErrText(fileName + ".xls");
            }
        }
        return errorEntity;
    }

    @Override
    public BasicDBObject getColHighChartData(Form62Entity entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();
        List<String> categories = new ArrayList<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }

        try {

            IComboService comboService = new ComboServiceImpl(boneCP);

            List<BasicDBObject> cmbType = comboService.getComboNames("EMCHTYPE", "MN");
            //Saraar n haruulah
            int month = calendar1.get(Calendar.MONTH) + 1;
            int year1 = calendar1.get(Calendar.YEAR);
            int year2 = calendar2.get(Calendar.YEAR);

            try (Connection connection = boneCP.getConnection()) {
                String query_sel = "SELECT `*`, sum(`ihEmch`) AS ihEmch,  sum(`bagaEmch`) AS bagaEmch," +
                        "sum(`malZuich`) AS malZuich, `hform62`.`aimag`, `hform62`.`sum`  " +
                        "FROM `hospital`.`hform62`  " +
                        "WHERE   ";

                if (entity != null && entity.getAimag() != null &&!entity.getAimag().isEmpty()) {
                    query_sel += " `hform62`.`aimag` = ? AND ";
                }
                if (entity != null && entity.getSum() != null && !entity.getSum().isEmpty()) {
                    query_sel += " `hform62`.`sum` = ? AND ";
                }
                if (entity != null && !entity.getDelflg().isEmpty()) {
                    query_sel += " `hform62`.`delflg` = ? AND ";
                }
                if (entity != null && !entity.getActflg().isEmpty()) {
                    query_sel += " `hform62`.`actflg` = ? AND ";
                }
                if (entity != null && !entity.getCreby().isEmpty()) {
                    query_sel += " `hform62`.`cre_by` = ? AND ";
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                    query_sel += " (`recorddate` BETWEEN ? AND ?)";
                }

                HashMap<String, List<Double>> resultArray = new HashMap<>();


                do {
                    //do process
                    categories.add(year1 + "-" + month);

                    try {
                        PreparedStatement statement = connection.prepareStatement(query_sel);

                        java.sql.Date stdate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-01 00:00:00").getTime());

                        java.sql.Date enddate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-31 00:00:00").getTime());

                        if (month == 12 && year1 < year2) {
                            month = 1;
                            year1++;
                        } else {
                            month++;
                        }

                        int stindex = 1;

                        if (entity != null && entity.getAimag() != null  && !entity.getAimag().isEmpty()) {
                            statement.setString(stindex++, entity.getAimag());
                        }
                        if (entity != null && entity.getSum() != null  && !entity.getSum().isEmpty()) {
                            statement.setString(stindex++, entity.getSum());
                        }
                        if (entity != null && !entity.getDelflg().isEmpty()) {
                            statement.setString(stindex++, entity.getDelflg());
                        }
                        if (entity != null && !entity.getActflg().isEmpty()) {
                            statement.setString(stindex++, entity.getActflg());
                        }
                        if (entity != null && !entity.getCreby().isEmpty()) {
                            statement.setString(stindex++, entity.getCreby());
                        }
                        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                            statement.setDate(stindex++, stdate);
                            statement.setDate(stindex++, enddate);
                        }


                        ResultSet resultSet = statement.executeQuery();

                        HashMap<String, String> hashMap = new HashMap<>();

                        while (resultSet.next()) {
                            String ihEmch = resultSet.getString("ihEmch");
                            String bagaEmch = resultSet.getString("bagaEmch");
                            String malZuich = resultSet.getString("malZuich");
                            hashMap.put("ihEmch", ihEmch);
                            hashMap.put("bagaEmch", bagaEmch);
                            hashMap.put("malZuich", malZuich);
                        }//end while

                        List<Double> list = new ArrayList<>();
                        for (int i = 0; i < cmbType.size(); i++) {
                            BasicDBObject obj = cmbType.get(i);

                            if (obj.get("data") == null){
                                //new record
                                list = new ArrayList<>();
                                String val = hashMap.get(obj.getString("id"));
                                if (val == null) list.add(0.0);
                                else if (val.isEmpty()) list.add(0.0);
                                else list.add(Double.parseDouble(val));
                                obj.put("data", list);
                            }else{
                                //already exist

                                list = (List<Double>) obj.get("data");
                                String val = hashMap.get(obj.getString("id"));
                                if (val == null) list.add(0.0);
                                else if (val.isEmpty()) list.add(0.0);
                                else list.add(Double.parseDouble(val));
                            }
                        }

                        if (statement != null) statement.close();
                        if (resultSet != null) resultSet.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();

                        break;
                    }

                    ///
                } while ((month <= calendar2.get(Calendar.MONTH) + 1 && year1 <= year2) || year1 < year2);

                basicDBObject.put("categories", categories);
                basicDBObject.put("series", cmbType);

                connection.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }

    @Override
    public BasicDBObject getPieHighChartData(Form62Entity entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }

        String query_sel = "SELECT `*`, sum(`ihEmch`) AS ihEmch,  sum(`bagaEmch`) AS bagaEmch," +
                "sum(`malZuich`) AS malZuich, `hform62`.`aimag`, `hform62`.`sum`  " +
                "FROM `hospital`.`hform62`  " +
                "WHERE   ";

        if (entity != null && entity.getAimag() != null &&!entity.getAimag().isEmpty()) {
            query_sel += " `hform62`.`aimag` = ? AND ";
        }
        if (entity != null && entity.getSum() != null && !entity.getSum().isEmpty()) {
            query_sel += " `hform62`.`sum` = ? AND ";
        }
        if (entity != null && !entity.getDelflg().isEmpty()) {
            query_sel += " `hform62`.`delflg` = ? AND ";
        }
        if (entity != null && !entity.getActflg().isEmpty()) {
            query_sel += " `hform62`.`actflg` = ? AND ";
        }
        if (entity != null && !entity.getCreby().isEmpty()) {
            query_sel += " `hform62`.`cre_by` = ? AND ";
        }
        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
            query_sel += " (`recorddate` BETWEEN ? AND ?)";
        }

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                int stindex = 1;

                if (entity != null && entity.getAimag() != null  && !entity.getAimag().isEmpty()) {
                    statement.setString(stindex++, entity.getAimag());
                }
                if (entity != null && entity.getSum() != null  && !entity.getSum().isEmpty()) {
                    statement.setString(stindex++, entity.getSum());
                }
                if (entity != null && !entity.getDelflg().isEmpty()) {
                    statement.setString(stindex++, entity.getDelflg());
                }
                if (entity != null && !entity.getActflg().isEmpty()) {
                    statement.setString(stindex++, entity.getActflg());
                }
                if (entity != null && !entity.getCreby().isEmpty()) {
                    statement.setString(stindex++, entity.getCreby());
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                    statement.setDate(stindex++, entity.getRecordDate());
                    statement.setDate(stindex++, entity.getSearchRecordDate());
                }
                ResultSet resultSet = statement.executeQuery();

                List<BasicDBObject> list = new ArrayList<>();

                while (resultSet.next()) {
                    BasicDBObject entity1 = new BasicDBObject();
                    BasicDBObject entity2 = new BasicDBObject();
                    BasicDBObject entity3 = new BasicDBObject();
                    entity1.put("name", "Их эмч");
                    entity1.put("y", resultSet.getDouble("ihEmch"));
                    entity2.put("name", "Бага эмч");
                    entity2.put("y", resultSet.getDouble("bagaEmch"));
                    entity3.put("name", "Мал зүйч");
                    entity3.put("y", resultSet.getDouble("malZuich"));
                    list.add(entity1);
                    list.add(entity2);
                    list.add(entity3);
                }

                basicDBObject.put("data", list);

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }
}
