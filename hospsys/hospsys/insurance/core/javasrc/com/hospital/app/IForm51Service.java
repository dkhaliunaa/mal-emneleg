package com.hospital.app;

import com.model.hos.ErrorEntity;
import com.model.hos.Form51Entity;
import com.model.hos.Form52Entity;
import com.mongodb.BasicDBObject;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public interface IForm51Service {

    /**
     * Insert new data
     *
     *
     * @param form51
     * @return
     * @throws Exception
     */
    ErrorEntity insertNewData(Form51Entity form51) throws SQLException;

    /**
     * Select all
     *
     * @param form51
     * @return
     * @throws SQLException
     */
    List<Form51Entity> getListData(Form51Entity form51) throws SQLException;


    /**
     * update form data
     *
     * @param form51
     * @return
     * @throws SQLException
     */
    ErrorEntity updateData(Form51Entity form51) throws SQLException;


    List<Form51Entity> getPieChartData(Form51Entity form51Entity) throws SQLException;


    ErrorEntity exportReport(String file, Form51Entity form51) throws SQLException;

    BasicDBObject getColHighChartData(Form51Entity Form51Entity) throws SQLException;

    BasicDBObject getPieHighChartData(Form51Entity Form51Entity) throws SQLException;
}
