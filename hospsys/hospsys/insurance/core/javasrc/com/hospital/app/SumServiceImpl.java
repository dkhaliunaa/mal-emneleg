package com.hospital.app;

import com.enumclass.ErrorType;
import com.model.hos.ErrorEntity;
import com.model.hos.Sum;
import com.jolbox.bonecp.BoneCP;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 24be10264 on 9/13/2017.
 */
public class SumServiceImpl implements ISumService {
    private BoneCP boneCP;
    private ErrorEntity errObj;

    public SumServiceImpl (BoneCP boneCP) {
        this.boneCP = boneCP;
    }

    @Override
    public List<Sum> selectAll(Sum sum) throws SQLException {
        int stIndex = 1;
        List<Sum> sumEntitieList = null;

        String query_sel = "SELECT c.`id`, " +
                "c.`sumcode`, c.`citycode`, c.`sumname`, c.`sumname_en`, c.`sumdesc`, c.`delflg`, c.`actflg`, c.`cre_at`, c.`latitude`,c.`longitude`, " +
                "c.`cre_by`, c.`mod_at`, c.`mod_by`, ct.`cityname` FROM `h_sum` AS c INNER JOIN `h_city` AS ct ON c.citycode = ct.code WHERE ";

        if (sum.getSumcode() != null && !sum.getSumcode().isEmpty()){
            query_sel += " c.`sumcode` = ? AND ";
        }
        if (sum.getCitycode() != null && !sum.getCitycode().isEmpty()){
            query_sel += " c.`citycode` = ? AND ";
        }
        if (sum.getSumname() != null && !sum.getSumname().isEmpty()){
            query_sel += " c.`sumname` LIKE ? AND ";
        }
        if (sum.getSumname_en() != null && !sum.getSumname_en().isEmpty()){
            query_sel += " c.`sumname_en` LIKE ? AND ";
        }

        if (sum.getCreBy() != null && !sum.getCreBy().isEmpty()){
            query_sel += " c.`cre_by` = ? AND ";
        }

        if (sum.getModBy() != null && !sum.getModBy().isEmpty()){
            query_sel += " c.`mod_by` = ? AND ";
        }

        if (sum != null && sum.getCreAt() != null && sum.getSearchCDate() != null){
            query_sel += " (c.`cre_at` BETWEEN ? AND ?) AND ";
        }

        if (sum != null && sum.getModAt() != null && sum.getSearchMDate() != null){
            query_sel += " (c.`mod_at` BETWEEN ? AND ?) AND ";
        }

        if (sum != null && sum.getActFlg() != null && !sum.getActFlg().isEmpty()){
            query_sel += " c.actflg = ? AND ";
        }
        if (sum != null && sum.getDelFlg() != null && !sum.getDelFlg().isEmpty()){

            query_sel += "c.delflg = ? AND ";
        }

        query_sel += "c.id > 0 ORDER BY c.`sumname`";

        try (Connection connection = boneCP.getConnection()) {
            try{
                PreparedStatement statement = connection.prepareStatement(query_sel);
                System.out.println(statement);
                if (sum.getSumcode() != null && !sum.getSumcode().isEmpty()){
                    statement.setString(stIndex ++ , sum.getSumcode());
                }
                if (sum.getCitycode() != null && !sum.getCitycode().isEmpty()){
                    statement.setString(stIndex ++ , sum.getCitycode());
                }
                if (sum.getSumname() != null && !sum.getSumname().isEmpty()){
                    statement.setString(stIndex ++ , "%"+sum.getSumname()+"%");
                }
                if (sum.getSumname_en() != null && !sum.getSumname_en().isEmpty()){
                    statement.setString(stIndex ++ , "%"+sum.getSumname_en()+"%");
                }

                if (sum.getCreBy() != null && !sum.getCreBy().isEmpty()){
                    statement.setString(stIndex ++ , sum.getCreBy());
                }

                if (sum.getModBy() != null && !sum.getModBy().isEmpty()){
                    statement.setString(stIndex ++ , sum.getModBy());
                }

                if (sum != null && sum.getCreAt() != null && sum.getSearchCDate() != null){
                    statement.setDate(stIndex ++ , sum.getCreAt());
                    statement.setDate(stIndex ++ , sum.getSearchCDate());
                }

                if (sum != null && sum.getModAt() != null && sum.getSearchMDate() != null){
                    statement.setDate(stIndex ++ , sum.getModAt());
                    statement.setDate(stIndex ++ , sum.getSearchMDate());
                }

                if (sum != null && sum.getActFlg() != null && !sum.getActFlg().isEmpty()){
                    statement.setString(stIndex ++ , sum.getActFlg());
                }
                if (sum != null && sum.getDelFlg() != null && !sum.getDelFlg().isEmpty()){

                    statement.setString(stIndex ++ , sum.getDelFlg());
                }
                ResultSet resultSet = statement.executeQuery();
                sumEntitieList = new ArrayList<>();

                int index = 1;

                while (resultSet.next()){
                    Sum entity = new Sum();

                    entity.setId(resultSet.getLong("id"));
                    entity.setSumcode(resultSet.getString("sumcode"));
                    entity.setCitycode(resultSet.getString("citycode"));
                    entity.setSumname(resultSet.getString("sumname"));
                    entity.setSumdesc(resultSet.getString("sumdesc"));
                    entity.setSumname_en(resultSet.getString("sumname_en"));
                    entity.setDelFlg(resultSet.getString("delflg"));
                    entity.setActFlg(resultSet.getString("actflg"));
                    entity.setModAt(resultSet.getDate("mod_at"));
                    entity.setModBy(resultSet.getString("mod_by"));
                    entity.setCreAt(resultSet.getDate("cre_at"));
                    entity.setCreBy(resultSet.getString("cre_by"));
                    entity.setCityName(resultSet.getString("cityname"));
                    entity.setLatitude(resultSet.getString("latitude"));
                    entity.setLongitude(resultSet.getString("longitude"));
                    entity.put("index", index++);

                    sumEntitieList.add(entity);
                }

                if (statement != null)statement.close();
                if (resultSet != null) resultSet.close();

            }catch (Exception ex){
                ex.printStackTrace();
            }finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return sumEntitieList;
    }//end of function

    @Override
    public ErrorEntity insertData(Sum sum) throws SQLException {
        PreparedStatement statement = null;

        if (sum == null){
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        String query_login = "INSERT INTO `h_sum`(`sumcode`, `citycode`, `sumname`, `sumname_en`, `sumdesc`, `delflg`, " +
                "`actflg`, `cre_at`, `cre_by`, `mod_at`, `mod_by`, latitude, longitude) " +
                "VALUES (?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try (Connection connection = boneCP.getConnection()) {
            try{
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, (sum.getSumcode() != null)?sum.getSumcode().trim():sum.getSumcode());
                statement.setString(2, (sum.getCitycode() != null)?sum.getCitycode().trim():sum.getCitycode());
                statement.setString(3, (sum.getSumname()!=null)?sum.getSumname().trim():sum.getSumname());
                statement.setString(4, (sum.getSumname_en() != null)?sum.getSumname_en().trim():sum.getSumname_en());
                statement.setString(5, (sum.getSumdesc()!=null)?sum.getSumdesc().trim():sum.getSumdesc());
                statement.setString(6, (sum.getDelFlg()!=null)?sum.getDelFlg().trim():sum.getDelFlg());
                statement.setString(7, (sum.getActFlg()!=null)? sum.getActFlg().trim():sum.getActFlg());
                statement.setDate(8, sum.getCreAt());
                statement.setString(9, (sum.getCreBy() != null) ? sum.getCreBy().trim():sum.getCreBy());
                statement.setDate(10, sum.getModAt());
                statement.setString(11, (sum.getModBy() != null) ? sum.getModBy().trim(): sum.getModBy());
                statement.setString(12, (sum.getLatitude() != null) ? sum.getLatitude().trim(): sum.getLatitude());
                statement.setString(13, (sum.getLongitude() != null) ? sum.getLongitude().trim(): sum.getLongitude());

                Sum searchsum = new Sum();
                searchsum.setSumcode(sum.getSumcode());
                List<Sum> tmpList = selectAll(searchsum);

                if (tmpList.size() > 0){
                    sum.setId(tmpList.get(0).getId());
                    sum.setDelFlg("N");
                    sum.setActFlg("Y");
                    return updateData(sum);
                }else{
                    int insertCount = statement.executeUpdate();

                    if (insertCount == 1){
                        connection.commit();
                        errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));
                        errObj.setErrText("Бичлэг амжилттай хадгалагдлаа.");
                        errObj.setErrDesc("Бичлэг амжилттай хадгалагдлаа.");
                        return errObj;
                    }
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }finally {
                if (statement != null)statement.close();
                connection.close();
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }

        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1057));
        errObj.setErrText("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");
        errObj.setErrDesc("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");

        return errObj;
    }

    @Override
    public ErrorEntity updateData(Sum sum) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (sum == null){
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        query = "UPDATE `hospital`.`h_sum` AS c " +
                "SET     ";

        if (sum.getSumcode() != null && !sum.getSumcode().isEmpty()){
            query += " c.`sumcode` = ?, ";
        }
        if (sum.getCitycode() != null && !sum.getCitycode().isEmpty()){
            query += " c.`citycode` = ?, ";
        }
        if (sum.getSumname() != null && !sum.getSumname().isEmpty()){
            query += " c.`sumname` = ?, ";
        }
        if (sum.getSumname_en() != null && !sum.getSumname_en().isEmpty()){
            query += " c.`sumname_en` = ?, ";
        }

        if (sum != null && sum.getActFlg() != null && !sum.getActFlg().isEmpty()){
            query += " c.actflg = ?, ";
        }
        if (sum != null && sum.getDelFlg() != null && !sum.getDelFlg().isEmpty()){

            query += "c.delflg = ?,  ";
        }
        if (sum != null && sum.getLatitude() != null && !sum.getLatitude().isEmpty()){
            query += " c.latitude = ?, ";
        }
        if (sum != null && sum.getLongitude() != null && !sum.getLongitude().isEmpty()){
            query += " c.longitude = ?, ";
        }

        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";

        query += " WHERE  `id`   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (sum.getSumcode() != null && !sum.getSumcode().isEmpty()){
                    statement.setString(stIndex++, sum.getSumcode().trim());
                }
                if (sum.getCitycode() != null && !sum.getCitycode().isEmpty()){
                    statement.setString(stIndex++, sum.getCitycode().trim());
                }
                if (sum.getSumname() != null && !sum.getSumname().isEmpty()){
                    statement.setString(stIndex++, sum.getSumname().trim());
                }
                if (sum.getSumname_en() != null && !sum.getSumname_en().isEmpty()){
                    statement.setString(stIndex++, sum.getSumname_en().trim());
                }

                if (sum != null && sum.getActFlg() != null && !sum.getActFlg().isEmpty()){
                    statement.setString(stIndex++, sum.getActFlg().trim());
                }
                if (sum != null && sum.getDelFlg() != null && !sum.getDelFlg().isEmpty()){
                    statement.setString(stIndex++, sum.getDelFlg().trim());
                }
                if (sum != null && sum.getLatitude() != null && !sum.getLatitude().isEmpty()){
                    statement.setString(stIndex++, sum.getLatitude().trim());
                }
                if (sum != null && sum.getLongitude() != null && !sum.getLongitude().isEmpty()){
                    statement.setString(stIndex++, sum.getLongitude().trim());
                }

                statement.setDate(stIndex++, sum.getModAt());
                statement.setString(stIndex++, sum.getModBy().trim());
                statement.setLong(stIndex++, sum.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();

                    errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                    errObj.setErrText("Бичлэг амжилттай засагдлаа.");
                    errObj.setErrDesc("Бичлэг амжилттай засагдлаа.");
                    return errObj;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }


        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1061));
        errObj.setErrText("Амжилтгүй боллоо. Дахин оролдоно уу.");
        errObj.setErrDesc("Амжилтгүй боллоо. Дахин оролдоно уу.");

        return errObj;
    }
}
