package com.hospital.app;

import com.model.User;
import com.model.hos.ErrorEntity;
import com.model.hos.MedremtgiiDataEntity;
import com.model.hos.SuregMedeeFormEntity;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public interface IGalzuuSuregMedeeFormService {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    /**
     *
     *
     * @param suregKey
     * @param user
     * @param entity
     * @param entityList
     * @return
     * @throws Exception
     */
    ErrorEntity manageData(User user, SuregMedeeFormEntity entity, List<MedremtgiiDataEntity> entityList) throws Exception;
    /**
     * Insert new data
     *
     *
     * @param entity
     * @return
     * @throws Exception
     */
    ErrorEntity insertNewData(SuregMedeeFormEntity entity) throws SQLException;

    /**
     * Select all
     *
     * @param entity
     * @return
     * @throws SQLException
     */
    List<SuregMedeeFormEntity> getListData(SuregMedeeFormEntity entity) throws SQLException;


    /**
     * update form data
     *
     * @param entity
     * @return
     * @throws SQLException
     */
    ErrorEntity updateData(SuregMedeeFormEntity entity) throws SQLException;

    /**
     * Get All Select Tree Data
     *
     * @param user
     * @param entity
     * @return
     */
    List<SuregMedeeFormEntity> selectTreeData(User user, SuregMedeeFormEntity entity);
}
