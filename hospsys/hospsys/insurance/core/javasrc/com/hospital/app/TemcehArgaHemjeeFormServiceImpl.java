package com.hospital.app;


import com.enumclass.ErrorType;
import com.jolbox.bonecp.BoneCP;
import com.model.User;
import com.model.hos.ErrorEntity;
import com.model.hos.TemtsehArgaHemEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Tilyeujan on 9/2/2017.
 */
public class TemcehArgaHemjeeFormServiceImpl implements ITemcehArgaHemjeeFormService {
    /**
     * Objects
     */
    private BoneCP boneCP;
    private ErrorEntity errorEntity;


    public TemcehArgaHemjeeFormServiceImpl(BoneCP boneCP) {
        this.boneCP = boneCP;
    }

    @Override
    public ErrorEntity insertNewData(TemtsehArgaHemEntity entity) throws SQLException {
        PreparedStatement statement = null;

        if (entity == null) {
            errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));

            errorEntity.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errorEntity.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errorEntity;
        }

        String query_login = "INSERT INTO `hform01_temceh_ah`(`form01key`, `citycode`, `sumcode`, `bagcode`, `malchinner`, " +
                "`bakcinOgnoo`, `bakcin_uusel`, `hamragdsan_mal_too`, `uvch_ilersen_eseh`, `hed_ho_daraa`, " +
                "`bakcinjuulalt_ognoo`, `mal_emch_negj_ner`, `hald_hiisen_eseh`, `heden_udaa`, `ymar_bodis`, " +
                "`ymar_arga`, `talbai`, `zar_bodis`, `ecsn_hald_ognoo`, `mmndemuztanheseh`, `muvchtmheseh`, " +
                "`gam_taraasa_eseh`, `zuv_serma_ugsun_eseh`, `on_mash_hud_on_gar_eseh`, `busad_sum_med_eseh`, " +
                "`uvch_burt_tal_med_eseh`, `record_date`, `delflg`, `actflg`, `cre_at`, `cre_by`, `mod_at`, `mod_by`, hed_mal_uvch) " +
                "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        try (Connection connection = boneCP.getConnection()) {
            try{
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, (entity.get(entity.FORM01KEY) != null) ? entity.getForm01key().trim() : "");
                statement.setString(2, (entity.get(entity.CITYCODE) != null) ? entity.getCitycode().trim() : "");
                statement.setString(3, (entity.get(entity.SUMCODE) != null) ? entity.getSumcode().trim() : "");
                statement.setString(4, (entity.get(entity.BAGCODE) != null) ? entity.getBagcode().trim() : "");
                statement.setString(5, (entity.get(entity.MALCHINNER) != null) ? entity.getMalchinner().trim() : "");
                statement.setDate(6, entity.getBakcinOgnoo());
                statement.setString(7, (entity.get(entity.BAKCIN_UUSEL) != null) ? entity.getBakcin_uusel().trim() : "");
                statement.setString(8, (entity.get(entity.HAMRAGDSAN_MAL_TOO) != null) ? entity.getHamragdsan_mal_too().trim() : "");
                statement.setString(9, (entity.get(entity.UVCH_ILERSEN_ESEH) != null) ? entity.getUvch_ilersen_eseh().trim() : "");
                statement.setString(10, (entity.get(entity.HED_HO_DARAA) != null) ? entity.getHed_ho_daraa().trim() : "");
                statement.setString(11, (entity.get(entity.BAKCINJUULALT_OGNOO) != null) ? entity.getBakcinjuulalt_ognoo().trim() : "");
                statement.setString(12, (entity.get(entity.MAL_EMCH_NEGJ_NER) != null) ? entity.getMal_emch_negj_ner().trim() : "");
                statement.setString(13, (entity.get(entity.HALD_HIISEN_ESEH) != null) ? entity.getHald_hiisen_eseh().trim() : "");
                statement.setInt(14, entity.getHeden_udaa());
                statement.setString(15, (entity.get(entity.YMAR_BODIS) != null) ? entity.getYmar_bodis().trim() : "");
                statement.setString(16, (entity.get(entity.YMAR_ARGA) != null) ? entity.getYmar_arga().trim() : "");
                statement.setString(17, (entity.get(entity.TALBAI) != null) ? entity.getTalbai().trim() : "");
                statement.setString(18, (entity.get(entity.ZAR_BODIS) != null) ? entity.getZar_bodis().trim() : "");
                statement.setDate(19, entity.getEcsn_hald_ognoo());
                statement.setString(20, (entity.get(entity.MMNDEMUZTANHESEH) != null) ? entity.getMmndemuztanheseh().trim() : "");
                statement.setString(21, (entity.get(entity.MUVCHTMHESEH) != null) ? entity.getMuvchtmheseh().trim() : "");
                statement.setString(22, (entity.get(entity.GAM_TARAASA_ESEH) != null) ? entity.getGam_taraasa_eseh().trim() : "");
                statement.setString(23, (entity.get(entity.ZUV_SERMA_UGSUN_ESEH) != null) ? entity.getZuv_serma_ugsun_eseh().trim() : "");
                statement.setString(24, (entity.get(entity.ON_MASH_HUD_ON_GAR_ESEH) != null) ? entity.getOn_mash_hud_on_gar_eseh().trim() : "");
                statement.setString(25, (entity.get(entity.BUSAD_SUM_MED_ESEH) != null) ? entity.getBusad_sum_med_eseh().trim() : "");
                statement.setString(26, (entity.get(entity.UVCH_BURT_TAL_MED_ESEH) != null) ? entity.getUvch_burt_tal_med_eseh().trim() : "");
                statement.setDate(27, entity.getRecord_date());
                statement.setString(28, (entity.get(entity.DELFLG) != null) ? entity.getDelflg().trim() : "");
                statement.setString(29, (entity.get(entity.ACTFLG) != null) ? entity.getActflg().trim() : "");
                statement.setDate(30, entity.getCre_at());
                statement.setString(31, (entity.get(entity.CRE_BY) != null) ? entity.getCre_by().trim() : "");
                statement.setDate(32, entity.getMod_at());
                statement.setString(33, (entity.get(entity.MOD_BY) != null) ? entity.getMod_by().trim() : "");
                statement.setString(34, (entity.get(entity.HEDEN_MAL_UVCHILSON) != null) ? entity.getHedenMalUvchilson().trim() : "");


                int insertCount = statement.executeUpdate();

                if (insertCount == 1){
                    connection.commit();

                    errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));

                    errorEntity.setErrText("Бичлэг амжилттай хадгалагдлаа.");
                    errorEntity.setErrDesc("Бичлэг амжилттай хадгалагдлаа.");

                    return errorEntity;
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }finally {
                if (statement != null)statement.close();
                connection.close();
            }

        }catch (Exception ex){

        }

        errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1057));
        errorEntity.setErrText("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");
        errorEntity.setErrDesc("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");

        return errorEntity;
    }

    @Override
    public List<TemtsehArgaHemEntity> getListData(TemtsehArgaHemEntity entity) throws SQLException {
        int stIndex = 1;
        int index = 1;
        List<TemtsehArgaHemEntity> entityList = null;

        String query = "SELECT `hform01_temceh_ah`.`id`, " +
                "    `hform01_temceh_ah`.`form01key`, " +
                "    `hform01_temceh_ah`.`citycode`, " +
                "    `hform01_temceh_ah`.`sumcode`, " +
                "    `hform01_temceh_ah`.`bagcode`, " +
                "    `hform01_temceh_ah`.`malchinner`, " +
                "    `hform01_temceh_ah`.`bakcinOgnoo`, " +
                "    `hform01_temceh_ah`.`bakcin_uusel`, " +
                "    `hform01_temceh_ah`.`hamragdsan_mal_too`, " +
                "    `hform01_temceh_ah`.`uvch_ilersen_eseh`, " +
                "    `hform01_temceh_ah`.`hed_ho_daraa`, " +
                "    `hform01_temceh_ah`.`hed_mal_uvch`, " +
                "    `hform01_temceh_ah`.`bakcinjuulalt_ognoo`, " +
                "    `hform01_temceh_ah`.`mal_emch_negj_ner`, " +
                "    `hform01_temceh_ah`.`hald_hiisen_eseh`, " +
                "    `hform01_temceh_ah`.`heden_udaa`, " +
                "    `hform01_temceh_ah`.`ymar_bodis`, " +
                "    `hform01_temceh_ah`.`ymar_arga`, " +
                "    `hform01_temceh_ah`.`talbai`, " +
                "    `hform01_temceh_ah`.`zar_bodis`, " +
                "    `hform01_temceh_ah`.`ecsn_hald_ognoo`, " +
                "    `hform01_temceh_ah`.`mmndemuztanheseh`, " +
                "    `hform01_temceh_ah`.`muvchtmheseh`, " +
                "    `hform01_temceh_ah`.`gam_taraasa_eseh`, " +
                "    `hform01_temceh_ah`.`zuv_serma_ugsun_eseh`, " +
                "    `hform01_temceh_ah`.`on_mash_hud_on_gar_eseh`, " +
                "    `hform01_temceh_ah`.`busad_sum_med_eseh`, " +
                "    `hform01_temceh_ah`.`uvch_burt_tal_med_eseh`, " +
                "    `hform01_temceh_ah`.`record_date`, " +
                "    `hform01_temceh_ah`.`delflg`, " +
                "    `hform01_temceh_ah`.`actflg`, " +
                "    `hform01_temceh_ah`.`cre_at`, " +
                "    `hform01_temceh_ah`.`cre_by`, " +
                "    `hform01_temceh_ah`.`mod_at`, " +
                "    `hform01_temceh_ah`.`mod_by`," +
                "`h_sum`.sumname, " +
                "`h_sum`.sumname_en, " +
                "`h_baghoroo`.horooname, " +
                "`h_baghoroo`.horooname_en, " +
                "`h_city`.cityname, " +
                "`h_city`.cityname_en " +
                "FROM `hospital`.`hform01_temceh_ah` INNER JOIN `h_city` " +
                "ON `h_city`.code = `hform01_temceh_ah`.citycode " +
                "INNER JOIN `h_sum` " +
                "ON `h_sum`.sumcode = `hform01_temceh_ah`.sumcode " +
                "INNER JOIN `h_baghoroo` ON `hform01_temceh_ah`.bagcode = `h_baghoroo`.horoocode " +
                "AND `h_baghoroo`.sumcode = `hform01_temceh_ah`.`sumcode` " +
                "WHERE ";

        if (entity.getForm01key() != null && !entity.getForm01key().isEmpty()){
            query += " `hform01_temceh_ah`.`form01key` = ? AND ";
        }
        if (entity.getCitycode() != null && !entity.getCitycode().isEmpty()){
            query += " `hform01_temceh_ah`.`citycode` = ? AND ";
        }
        if (entity.getSumcode() != null && !entity.getSumcode().isEmpty()){
            query += " `hform01_temceh_ah`.`sumcode` = ? AND ";
        }
        if (entity.getBagcode() != null && !entity.getBagcode().isEmpty()){
            query += " `hform01_temceh_ah`.`bagcode` = ? AND ";
        }
        if (entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
            query += " `hform01_temceh_ah`.`delflg` = ? AND ";
        }
        if (entity.getActflg() != null && !entity.getActflg().isEmpty()){
            query += " `hform01_temceh_ah`.`actflg` = ? AND ";
        }
        if (entity != null && entity.getCre_by() != null && !entity.getCre_by().isEmpty()){
            query += " `hform01_temceh_ah`.`cre_by` = ? AND ";
        }

        query += " `hform01_temceh_ah`.`id` > 0 ";
        query += " ORDER BY `hform01_temceh_ah`.`cre_at` DESC ";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query);

                if (entity.getForm01key() != null && !entity.getForm01key().isEmpty()){
                    statement.setString(stIndex++, entity.getForm01key());
                }
                if (entity.getCitycode() != null && !entity.getCitycode().isEmpty()){
                    statement.setString(stIndex++, entity.getCitycode());
                }
                if (entity.getSumcode() != null && !entity.getSumcode().isEmpty()){
                    statement.setString(stIndex++, entity.getSumcode());
                }
                if (entity.getBagcode() != null && !entity.getBagcode().isEmpty()){
                    statement.setString(stIndex++, entity.getBagcode());
                }
                if (entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
                    statement.setString(stIndex++, entity.getDelflg());
                }
                if (entity.getActflg() != null && !entity.getActflg().isEmpty()){
                    statement.setString(stIndex++, entity.getActflg());
                }
                if (entity != null && entity.getCre_by() != null && !entity.getCre_by().isEmpty()){
                    statement.setString(stIndex++, entity.getCre_by());
                }


                ResultSet resultSet = statement.executeQuery();

                entityList = new ArrayList<>();

                while (resultSet.next()) {
                    TemtsehArgaHemEntity objEntity = new TemtsehArgaHemEntity();

                    objEntity.setId(resultSet.getLong("id"));
                    objEntity.setForm01key(resultSet.getString("form01key"));
                    objEntity.setCitycode(resultSet.getString("citycode"));
                    objEntity.setSumcode(resultSet.getString("sumcode"));
                    objEntity.setBagcode(resultSet.getString("bagcode"));
                    objEntity.setMalchinner(resultSet.getString("malchinner"));
                    objEntity.setBakcinOgnoo(resultSet.getDate("bakcinOgnoo"));
                    objEntity.setBakcin_uusel(resultSet.getString("bakcin_uusel"));
                    objEntity.setHamragdsan_mal_too(resultSet.getString("hamragdsan_mal_too"));
                    objEntity.setUvch_ilersen_eseh(resultSet.getString("uvch_ilersen_eseh"));
                    objEntity.setHed_ho_daraa(resultSet.getString("hed_ho_daraa"));
                    objEntity.setHedenMalUvchilson(resultSet.getString("hed_mal_uvch"));
                    objEntity.setBakcinjuulalt_ognoo(resultSet.getString("bakcinjuulalt_ognoo"));
                    objEntity.setMal_emch_negj_ner(resultSet.getString("mal_emch_negj_ner"));
                    objEntity.setHald_hiisen_eseh(resultSet.getString("hald_hiisen_eseh"));
                    objEntity.setHeden_udaa(resultSet.getInt("heden_udaa"));
                    objEntity.setYmar_bodis(resultSet.getString("ymar_bodis"));
                    objEntity.setYmar_arga(resultSet.getString("ymar_arga"));
                    objEntity.setTalbai(resultSet.getString("talbai"));
                    objEntity.setZar_bodis(resultSet.getString("zar_bodis"));
                    objEntity.setEcsn_hald_ognoo(resultSet.getDate("ecsn_hald_ognoo"));
                    objEntity.setMmndemuztanheseh(resultSet.getString("mmndemuztanheseh"));
                    objEntity.setMuvchtmheseh(resultSet.getString("muvchtmheseh"));
                    objEntity.setGam_taraasa_eseh(resultSet.getString("gam_taraasa_eseh"));
                    objEntity.setZuv_serma_ugsun_eseh(resultSet.getString("zuv_serma_ugsun_eseh"));
                    objEntity.setOn_mash_hud_on_gar_eseh(resultSet.getString("on_mash_hud_on_gar_eseh"));
                    objEntity.setBusad_sum_med_eseh(resultSet.getString("busad_sum_med_eseh"));
                    objEntity.setUvch_burt_tal_med_eseh(resultSet.getString("uvch_burt_tal_med_eseh"));

                    objEntity.setDelflg(resultSet.getString("delflg"));
                    objEntity.setActflg(resultSet.getString("actflg"));
                    objEntity.setCre_at(resultSet.getDate("cre_at"));
                    objEntity.setCre_by(resultSet.getString("cre_by"));
                    objEntity.setMod_at(resultSet.getDate("mod_at"));
                    objEntity.setMod_by(resultSet.getString("mod_by"));
                    objEntity.setRecord_date(resultSet.getDate("record_date"));

                    objEntity.put("cityname", resultSet.getString("cityname"));
                    objEntity.put("sumname", resultSet.getString("sumname"));
                    objEntity.put("horooname", resultSet.getString("horooname"));
                    objEntity.put("index", index++);
                    objEntity.put("strRecordOgnoo", dateFormat.format(objEntity.getRecord_date()));
                    objEntity.put("strBakcinOgnoo", dateFormat.format(objEntity.getBakcinOgnoo()));
                    objEntity.put("strEcsnHaldOgnoo", dateFormat.format(objEntity.getEcsn_hald_ognoo()));

                    entityList.add(objEntity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return entityList;
    }

    @Override
    public ErrorEntity updateData(TemtsehArgaHemEntity entity) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (entity == null) new ErrorEntity(ErrorType.INFO, Long.valueOf(1059));
        if (entity == null) {
            errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errorEntity.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errorEntity.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errorEntity;
        }

        query = "UPDATE `hospital`.`hform01_temceh_ah` " +
                "SET     ";

        if (entity.getForm01key() != null && !entity.getForm01key().isEmpty())
            query += " `form01key`=?," ;

        if (entity.getCitycode() != null && !entity.getCitycode().isEmpty())
            query += "`citycode`=?," ;

        if (entity.getSumcode() != null && !entity.getSumcode().isEmpty())
            query += "`sumcode`=?," ;

        if (entity.getBagcode() != null && !entity.getBagcode().isEmpty())
            query += "`bagcode`=?," ;

        if (entity.getMalchinner() != null && !entity.getMalchinner().isEmpty())
            query += "`malchinner`=?," ;

        if (entity.getBakcinOgnoo() != null)
            query += " `bakcinOgnoo`=?,";

        if (entity.getBakcin_uusel() != null && !entity.getBakcin_uusel().isEmpty())
            query += "`bakcin_uusel`=?,";

        if (entity.getHamragdsan_mal_too() != null && !entity.getHamragdsan_mal_too().isEmpty())
            query += "`hamragdsan_mal_too`=?,";

        if (entity.getUvch_ilersen_eseh() != null && !entity.getUvch_ilersen_eseh().isEmpty())
            query += "`uvch_ilersen_eseh`=?,";

        if (entity.getHed_ho_daraa() != null && !entity.getHed_ho_daraa().isEmpty())
            query += "`hed_ho_daraa`=?,";

        if (entity.getHedenMalUvchilson() != null && !entity.getHedenMalUvchilson().isEmpty())
            query += "`hed_mal_uvch`=?,";

        if (entity.getBakcinjuulalt_ognoo() != null && !entity.getBakcinjuulalt_ognoo().isEmpty())
            query += "`bakcinjuulalt_ognoo`=?,";

        if (entity.getMal_emch_negj_ner() != null && !entity.getMal_emch_negj_ner().isEmpty())
            query += "`mal_emch_negj_ner`=?,";

        if (entity.getHald_hiisen_eseh() != null && !entity.getHald_hiisen_eseh().isEmpty())
            query += "`hald_hiisen_eseh`=?,";

        if (entity.getHeden_udaa() != 0)
            query += "`heden_udaa`=?,";

        if (entity.getYmar_bodis() != null && !entity.getYmar_bodis().isEmpty())
            query += "`ymar_bodis`=?,";

        if (entity.getYmar_arga() != null && !entity.getYmar_arga().isEmpty())
            query += "`ymar_arga`=?,";

        if (entity.getTalbai() != null && !entity.getTalbai().isEmpty())
            query += "`talbai`=?,";

        if (entity.getZar_bodis() != null && !entity.getZar_bodis().isEmpty())
            query += "`zar_bodis`=?,";

        if (entity.getEcsn_hald_ognoo() != null)
            query += "`ecsn_hald_ognoo`=?,";

        if (entity.getMmndemuztanheseh() != null && !entity.getMmndemuztanheseh().isEmpty())
            query += "`mmndemuztanheseh`=?,";

        if (entity.getMuvchtmheseh() != null && !entity.getMuvchtmheseh().isEmpty())
            query += "`muvchtmheseh`=?,";

        if (entity.getGam_taraasa_eseh() != null && !entity.getGam_taraasa_eseh().isEmpty())
            query += "`gam_taraasa_eseh`=?,";

        if (entity.getZuv_serma_ugsun_eseh() != null && !entity.getZuv_serma_ugsun_eseh().isEmpty())
            query += "`zuv_serma_ugsun_eseh`=?,";

        if (entity.getOn_mash_hud_on_gar_eseh() != null && !entity.getOn_mash_hud_on_gar_eseh().isEmpty())
            query += "`on_mash_hud_on_gar_eseh`=?,";

        if (entity.getBusad_sum_med_eseh() != null && !entity.getBusad_sum_med_eseh().isEmpty())
            query += "`busad_sum_med_eseh`=?,";

        if (entity.getUvch_burt_tal_med_eseh() != null && !entity.getUvch_burt_tal_med_eseh().isEmpty())
            query += "`uvch_burt_tal_med_eseh`=?,";

        if (entity.getDelflg() != null && !entity.getDelflg().isEmpty())
            query += "`delflg`=?," ;

        if (entity.getActflg() != null && !entity.getActflg().isEmpty())
            query += "`actflg`=?," ;

        if (entity.getRecord_date() != null)
            query += "`record_date`=?, ";

        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";
        query += " WHERE  ID   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (entity.getForm01key() != null && !entity.getForm01key().isEmpty())
                    statement.setString(stIndex++, entity.getForm01key());

                if (entity.getCitycode() != null && !entity.getCitycode().isEmpty())
                    statement.setString(stIndex++, entity.getCitycode());

                if (entity.getSumcode() != null && !entity.getSumcode().isEmpty())
                    statement.setString(stIndex++, entity.getSumcode());

                if (entity.getBagcode() != null && !entity.getBagcode().isEmpty())
                    statement.setString(stIndex++, entity.getBagcode());

                if (entity.getMalchinner() != null && !entity.getMalchinner().isEmpty())
                    statement.setString(stIndex++, entity.getMalchinner());

                if (entity.getBakcinOgnoo() != null)
                    statement.setDate(stIndex++, entity.getBakcinOgnoo());

                if (entity.getBakcin_uusel() != null && !entity.getBakcin_uusel().isEmpty())
                    statement.setString(stIndex++, entity.getBakcin_uusel());

                if (entity.getHamragdsan_mal_too() != null && !entity.getHamragdsan_mal_too().isEmpty())
                    statement.setString(stIndex++, entity.getHamragdsan_mal_too());

                if (entity.getUvch_ilersen_eseh() != null && !entity.getUvch_ilersen_eseh().isEmpty())
                    statement.setString(stIndex++, entity.getUvch_ilersen_eseh());

                if (entity.getHed_ho_daraa() != null && !entity.getHed_ho_daraa().isEmpty())
                    statement.setString(stIndex++, entity.getHed_ho_daraa());

                if (entity.getHedenMalUvchilson() != null && !entity.getHedenMalUvchilson().isEmpty())
                    statement.setString(stIndex++, entity.getHedenMalUvchilson());

                if (entity.getBakcinjuulalt_ognoo() != null && !entity.getBakcinjuulalt_ognoo().isEmpty())
                    statement.setString(stIndex++, entity.getBakcinjuulalt_ognoo());

                if (entity.getMal_emch_negj_ner() != null && !entity.getMal_emch_negj_ner().isEmpty())
                    statement.setString(stIndex++, entity.getMal_emch_negj_ner());

                if (entity.getHald_hiisen_eseh() != null && !entity.getHald_hiisen_eseh().isEmpty())
                    statement.setString(stIndex++, entity.getHald_hiisen_eseh());

                if (entity.getHeden_udaa() != 0)
                    statement.setInt(stIndex++, entity.getHeden_udaa());

                if (entity.getYmar_bodis() != null && !entity.getYmar_bodis().isEmpty())
                    statement.setString(stIndex++, entity.getYmar_bodis());

                if (entity.getYmar_arga() != null && !entity.getYmar_arga().isEmpty())
                    statement.setString(stIndex++, entity.getYmar_arga());

                if (entity.getTalbai() != null && !entity.getTalbai().isEmpty())
                    statement.setString(stIndex++, entity.getTalbai());

                if (entity.getZar_bodis() != null && !entity.getZar_bodis().isEmpty())
                    statement.setString(stIndex++, entity.getZar_bodis());

                if (entity.getEcsn_hald_ognoo() != null)
                    statement.setDate(stIndex++, entity.getEcsn_hald_ognoo());

                if (entity.getMmndemuztanheseh() != null && !entity.getMmndemuztanheseh().isEmpty())
                    statement.setString(stIndex++, entity.getMmndemuztanheseh());

                if (entity.getMuvchtmheseh() != null && !entity.getMuvchtmheseh().isEmpty())
                    statement.setString(stIndex++, entity.getMuvchtmheseh());

                if (entity.getGam_taraasa_eseh() != null && !entity.getGam_taraasa_eseh().isEmpty())
                    statement.setString(stIndex++, entity.getGam_taraasa_eseh());

                if (entity.getZuv_serma_ugsun_eseh() != null && !entity.getZuv_serma_ugsun_eseh().isEmpty())
                    statement.setString(stIndex++, entity.getZuv_serma_ugsun_eseh());

                if (entity.getOn_mash_hud_on_gar_eseh() != null && !entity.getOn_mash_hud_on_gar_eseh().isEmpty())
                    statement.setString(stIndex++, entity.getOn_mash_hud_on_gar_eseh());

                if (entity.getBusad_sum_med_eseh() != null && !entity.getBusad_sum_med_eseh().isEmpty())
                    statement.setString(stIndex++, entity.getBusad_sum_med_eseh());

                if (entity.getUvch_burt_tal_med_eseh() != null && !entity.getUvch_burt_tal_med_eseh().isEmpty())
                    statement.setString(stIndex++, entity.getUvch_burt_tal_med_eseh());

                if (entity.getDelflg() != null && !entity.getDelflg().isEmpty())
                    statement.setString(stIndex++, entity.getDelflg());

                if (entity.getActflg() != null && !entity.getActflg().isEmpty())
                    statement.setString(stIndex++, entity.getActflg());

                if (entity.getRecord_date() != null)
                    statement.setDate(stIndex++, entity.getRecord_date());

                statement.setDate(stIndex++, entity.getMod_at());
                statement.setString(stIndex++, entity.getMod_by());
                statement.setLong(stIndex++, entity.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                    errorEntity.setErrText("Бичлэг амжилттай засагдлаа.");
                    errorEntity.setErrDesc("Бичлэг амжилттай засагдлаа.");
                    return errorEntity;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }


        errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1061));
        errorEntity.setErrText("Амжилтгүй боллоо. Дахин оролдоно уу.");
        errorEntity.setErrDesc("Амжилтгүй боллоо. Дахин оролдоно уу.");

        return errorEntity;
    }

    @Override
    public ErrorEntity manageData(User user, TemtsehArgaHemEntity entity) {
        Date utilDate = new Date();
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

        try {
            TemtsehArgaHemEntity tmp = new TemtsehArgaHemEntity();

            tmp.setForm01key(entity.getForm01key());


            List<TemtsehArgaHemEntity> list = getListData(tmp);

            String command = "";

            if (list == null){
                command = "INS";
            }
            else if (list.size() == 0){
                command = "INS";
            }
            else if (list.size() > 0){
                command = "EDT";
            }

            entity.setMod_at(sqlDate);
            entity.setMod_by(user.getUsername());


            if (command.equalsIgnoreCase("INS"))
            {
                entity.setDelflg("N");
                entity.setActflg("Y");
                entity.setCre_by(user.getUsername());
                entity.setCre_at(sqlDate);

                errorEntity = insertNewData(entity);
            }
            else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL") ||
                    command.equalsIgnoreCase("ACT"))
            {
                entity.setId(list.get(0).getId());

                if (command.equalsIgnoreCase("DEL")){
                    entity.setDelflg("Y");
                }

                errorEntity = updateData(entity);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        finally {
            if (errorEntity == null){
                errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1002));
                errorEntity.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
                errorEntity.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

                return errorEntity;
            }
        }

        return errorEntity;
    }
}
