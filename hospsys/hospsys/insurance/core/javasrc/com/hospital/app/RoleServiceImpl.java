package com.hospital.app;

import com.model.Role;
import com.model.hos.ErrorEntity;
import com.jolbox.bonecp.BoneCP;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RoleServiceImpl implements IRoleService {

    private BoneCP boneCP;

    public RoleServiceImpl (BoneCP boneCP) {
        this.boneCP = boneCP;
    }

    @Override
    public List<Role> selectAll(Role role) throws SQLException {
        int stIndex = 1;
        List<Role> roleEntitieList = null;

        String query_sel = "SELECT * FROM `h_role` AS c WHERE ";

        if (role != null && role.getCode() != null && !role.getCode().isEmpty()){
            query_sel += " c.`role_code` = ? AND ";
        }
        if (role.getName() != null && !role.getName().isEmpty()){
            query_sel += " c.`role_name` = ? AND ";
        }
        if (role.getParent() != null && !role.getParent().isEmpty()){
            query_sel += " c.`parent_role` = ? AND ";
        }

        query_sel += "c.id > 0 ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                if (role != null && role.getCode() != null && !role.getCode().isEmpty()){
                    statement.setString(stIndex++, role.getCode());
                }
                if (role.getName() != null && !role.getName().isEmpty()){
                    statement.setString(stIndex++, role.getName());
                }
                if (role.getParent() != null && !role.getParent().isEmpty()){
                    statement.setString(stIndex++, role.getParent());
                }

                ResultSet resultSet = statement.executeQuery();

                roleEntitieList = new ArrayList<>();

                int index = 1;

                while (resultSet.next()) {
                    Role entity = new Role();

                    entity.setId(resultSet.getLong("id"));
                    entity.setCode(resultSet.getString("role_code"));
                    entity.setName(resultSet.getString("role_name"));
                    entity.setDesc(resultSet.getString("role_desc"));
                    entity.setParent(resultSet.getString("parent_role"));
                    entity.setQuality(resultSet.getInt("quality"));
                    entity.put("index", index++);

                    roleEntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return roleEntitieList;
    }

    @Override
    public ErrorEntity insertData(Role role) throws SQLException {
        return null;
    }

    @Override
    public ErrorEntity updateData(Role role) throws SQLException {
        return null;
    }
}
