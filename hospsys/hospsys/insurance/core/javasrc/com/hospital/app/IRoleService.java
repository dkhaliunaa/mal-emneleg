package com.hospital.app;

import com.model.Role;
import com.model.hos.ErrorEntity;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by 24be10264 on 9/13/2017.
 */
public interface IRoleService {

    /**
     * Fetch
     *
     * @param role
     * @return
     * @throws SQLException
     */
    List<Role> selectAll(Role role) throws SQLException;

    /**
     * Insert
     *
     * @param role
     * @return
     * @throws SQLException
     */
    ErrorEntity insertData(Role role) throws SQLException;

    /**
     * Update
     *
     * @param role
     * @return
     * @throws SQLException
     */
    ErrorEntity updateData(Role role) throws SQLException;
}
