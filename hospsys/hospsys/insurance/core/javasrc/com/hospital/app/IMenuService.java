package com.hospital.app;

import com.model.User;
import com.model.hos.ErrorEntity;
import com.model.hos.MenuEntity;
import com.mongodb.BasicDBObject;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by 24be10264 on 9/13/2017.
 */
public interface IMenuService {

    /**
     * Fetch
     *
     * @param menu
     * @return
     * @throws SQLException
     */
    List<MenuEntity> selectAll(MenuEntity menu) throws SQLException;

    /**
     * Insert
     *
     * @param menu
     * @return
     * @throws SQLException
     */
    ErrorEntity insertData(MenuEntity menu) throws SQLException;

    /**
     * Update
     *
     * @param menu
     * @return
     * @throws SQLException
     */
    ErrorEntity updateData(MenuEntity menu) throws SQLException;

    /**
     * Tuhain hunii harah blmjtoi menu list
     *
     * @param user
     * @return
     */
    List<BasicDBObject> getUserAccessMenuList(User user);
}
