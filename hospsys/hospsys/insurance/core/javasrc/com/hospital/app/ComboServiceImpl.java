package com.hospital.app;

import com.enumclass.ErrorType;
import com.jolbox.bonecp.BoneCP;
import com.model.hos.ComboEntity;
import com.model.hos.ErrorEntity;
import com.mongodb.BasicDBObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 24be10264 on 8/18/2017.
 */
public class ComboServiceImpl implements IComboService{

    private BoneCP boneCP;
    private ErrorEntity errObj;

    public ComboServiceImpl (BoneCP boneCP) {
        this.boneCP = boneCP;
    }
    public ComboServiceImpl(){

    }

    @Override
    public List<BasicDBObject> getComboVals(String type, String langid) throws Exception {
        List<BasicDBObject> cmbList = null;

        String query_login = "SELECT * FROM h_cmb AS us WHERE us.delflg = 'N' and us.cmbtype = ? and langid = ? ORDER BY cmbname";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_login);

                statement.setString(1, type);
                statement.setString(2, langid);

                ResultSet resultSet = statement.executeQuery();

                cmbList = new ArrayList<>();

                while (resultSet.next()) {
                    BasicDBObject basicDBObject = new BasicDBObject();

                    basicDBObject.put("id", resultSet.getString("cmbval"));
                    basicDBObject.put("name", resultSet.getString("cmbname"));

                    basicDBObject.put("label", resultSet.getString("cmbname"));
                    basicDBObject.put("type", "number");

                    cmbList.add(basicDBObject);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return cmbList;
    }

    @Override
    public List<BasicDBObject> getComboNames(String type, String langid) throws Exception {
        List<BasicDBObject> cmbList = null;

        String query_login = "SELECT * FROM h_cmb AS us WHERE us.delflg = 'N' and us.cmbtype = ? and langid = ? ORDER BY cmbname";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_login);

                statement.setString(1, type);
                statement.setString(2, langid);

                ResultSet resultSet = statement.executeQuery();

                cmbList = new ArrayList<>();

                while (resultSet.next()) {
                    BasicDBObject basicDBObject = new BasicDBObject();

                    basicDBObject.put("id", resultSet.getString("cmbval"));
                    basicDBObject.put("name", resultSet.getString("cmbname"));

                    cmbList.add(basicDBObject);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return cmbList;
    }

    @Override
    public List<BasicDBObject> getShalgahGazarVals(String type, String langid) throws Exception {
        List<BasicDBObject> cmbList = null;

        String query_login = "SELECT DISTINCT shalgahGazar FROM hform14 AS us WHERE us.delflg = 'N' ORDER BY shalgahGazar";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_login);

                ResultSet resultSet = statement.executeQuery();

                cmbList = new ArrayList<>();

                while (resultSet.next()) {
                    BasicDBObject basicDBObject = new BasicDBObject();

                    basicDBObject.put("id", resultSet.getString("shalgahGazar"));
                    basicDBObject.put("name", resultSet.getString("shalgahGazar"));

                    cmbList.add(basicDBObject);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return cmbList;
    }

    @Override
    public String getComboName(String code, String langid) throws SQLException {
        String result = "";
        String query_login = "SELECT * FROM h_cmb AS us WHERE us.delflg = 'N' and us.cmbval = ? and langid = ? ORDER BY cmbname";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_login);

                statement.setString(1, code);
                statement.setString(2, langid);

                ResultSet resultSet = statement.executeQuery();

                while (resultSet.next()) {

                    result = resultSet.getString("cmbname");
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return result;
    }

    @Override
    public List<ComboEntity> selectAll(ComboEntity entity) throws SQLException {
        int stIndex = 1;
        List<ComboEntity> entities = null;

        String query_sel = "SELECT `id`, `cmbtype`, `cmbval`, `cmbname`, `cmbdesc`, `langid`, `delflg`, `actflg`, " +
                "`cre_at`, `cre_by`, `mod_at`, `mod_by`, `parentid` FROM `h_cmb` WHERE ";


        if (entity.getCmbtype() != null && !entity.getCmbtype().isEmpty()){
            query_sel += " `cmbtype` = ? AND ";
        }

        if (entity.getCmbval() != null && !entity.getCmbval().isEmpty()){
            query_sel += " `cmbval` = ? AND ";
        }

        if (entity.getCmbname() != null && !entity.getCmbname().isEmpty()){
            query_sel += " `cmbname` = ? AND ";
        }

        if (entity.getLangid() != null && !entity.getLangid().isEmpty()){
            query_sel += " `langid` = ? AND ";
        }

        if (entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
            query_sel += " `delflg` = ? AND ";
        }

        if (entity.getActflg() != null && !entity.getActflg().isEmpty()){
            query_sel += " `actflg` = ? AND ";
        }

        if (entity != null && entity.getCreAt() != null && entity.getCreAt() != null){
            query_sel += " (`cre_at` BETWEEN ? AND ?) AND ";
        }

        if (entity != null && entity.getCreBy() != null && !entity.getCreBy().isEmpty()){
            query_sel += " `cre_by` = ? AND ";
        }
        if (entity != null && entity.getModBy() != null && !entity.getModBy().isEmpty()){
            query_sel += " `mod_by` = ? AND ";
        }

        query_sel += " `id` > 0 ORDER BY cre_at DESC ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                if (entity.getCmbtype() != null && !entity.getCmbtype().isEmpty()){
                    statement.setString(stIndex++, entity.getCmbtype());
                }

                if (entity.getCmbval() != null && !entity.getCmbval().isEmpty()){
                    statement.setString(stIndex++, entity.getCmbval());
                }

                if (entity.getCmbname() != null && !entity.getCmbname().isEmpty()){
                    statement.setString(stIndex++, entity.getCmbname());
                }

                if (entity.getLangid() != null && !entity.getLangid().isEmpty()){
                    statement.setString(stIndex++, entity.getLangid());
                }

                if (entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
                    statement.setString(stIndex++, entity.getDelflg());
                }

                if (entity.getActflg() != null && !entity.getActflg().isEmpty()){
                    statement.setString(stIndex++, entity.getActflg());
                }

                if (entity != null && entity.getCreAt() != null && entity.getCreAt() != null){
                    statement.setDate(stIndex++, entity.getCreAt());
                    statement.setDate(stIndex++, entity.getCreAt());
                }

                if (entity != null && entity.getCreBy() != null && !entity.getCreBy().isEmpty()){
                    statement.setString(stIndex++, entity.getCreBy());
                }
                if (entity != null && entity.getModBy() != null && !entity.getModBy().isEmpty()){
                    statement.setString(stIndex++, entity.getModBy());
                }

                ResultSet resultSet = statement.executeQuery();

                entities = new ArrayList<>();

                int index = 1;

                while (resultSet.next()) {
                    ComboEntity cmb = new ComboEntity();

                    cmb.setId(resultSet.getLong("id"));
                    cmb.setCmbtype(resultSet.getString("cmbtype"));
                    cmb.setCmbval(resultSet.getString("cmbval"));
                    cmb.setCmbname(resultSet.getString("cmbname"));
                    cmb.setCmbdesc(resultSet.getString("cmbdesc"));
                    cmb.setLangid(resultSet.getString("langid"));
                    cmb.setDelflg(resultSet.getString("delflg"));
                    cmb.setActflg(resultSet.getString("actflg"));
                    cmb.setCreAt(resultSet.getDate("cre_at"));
                    cmb.setCreBy(resultSet.getString("cre_by"));
                    cmb.setModAt(resultSet.getDate("mod_at"));
                    cmb.setModBy(resultSet.getString("mod_by"));

                    cmb.put("index", index++);

                    entities.add(cmb);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return entities;
    }

    @Override
    public ErrorEntity insertCmb(ComboEntity entity) throws SQLException {
        PreparedStatement statement = null;

        if (entity == null) {
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        String query_login = "INSERT INTO `h_cmb`(`cmbtype`, `cmbval`, `cmbname`, `cmbdesc`, `langid`, " +
                "`delflg`, `actflg`, `cre_at`, `cre_by`, `mod_at`, `mod_by`, `parentid`) " +
                "VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, entity.getCmbtype());
                statement.setString(2, entity.getCmbval());
                statement.setString(3, entity.getCmbname());
                statement.setString(4, entity.getCmbdesc());
                statement.setString(5, entity.getLangid());
                statement.setString(6, entity.getDelflg());
                statement.setString(7, entity.getActflg());
                statement.setDate(8, entity.getCreAt());
                statement.setString(9, entity.getCreBy());
                statement.setDate(10, entity.getModAt());
                statement.setString(11, entity.getModBy());
                statement.setString(12, "");

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));
                    errObj.setErrText("Бичлэг амжилттай хадгалагдлаа.");
                    errObj.setErrDesc("Бичлэг амжилттай хадгалагдлаа.");
                    return errObj;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }

        } catch (Exception ex) {

        }

        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1057));
        errObj.setErrText("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");
        errObj.setErrDesc("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");

        return errObj;
    }

    @Override
    public ErrorEntity updateCmb(ComboEntity entity) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (entity == null) {
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        query = "UPDATE `hospital`.`h_cmb` " +
                "SET     ";

        if (entity.getCmbtype() != null && !entity.getCmbtype().isEmpty()){
            query += " `cmbtype` = ?, ";
        }

        if (entity.getCmbval() != null && !entity.getCmbval().isEmpty()){
            query += " `cmbval` = ?, ";
        }

        if (entity.getCmbname() != null && !entity.getCmbname().isEmpty()){
            query += " `cmbname` = ?, ";
        }
        if (entity.getCmbdesc() != null && !entity.getCmbdesc().isEmpty()){
            query += " `cmbdesc` = ?, ";
        }

        if (entity.getLangid() != null && !entity.getLangid().isEmpty()){
            query += " `langid` = ?, ";
        }

        if (entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
            query += " `delflg` = ?, ";
        }

        if (entity.getActflg() != null && !entity.getActflg().isEmpty()){
            query += " `actflg` = ?, ";
        }


        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";
        query += " WHERE  ID   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (entity.getCmbtype() != null && !entity.getCmbtype().isEmpty()){
                    statement.setString(stIndex++, entity.getCmbtype());
                }

                if (entity.getCmbval() != null && !entity.getCmbval().isEmpty()){
                    statement.setString(stIndex++, entity.getCmbval());
                }

                if (entity.getCmbname() != null && !entity.getCmbname().isEmpty()){
                    statement.setString(stIndex++, entity.getCmbname());
                }

                if (entity.getCmbdesc() != null && !entity.getCmbdesc().isEmpty()){
                    statement.setString(stIndex++, entity.getCmbdesc());
                }

                if (entity.getLangid() != null && !entity.getLangid().isEmpty()){
                    statement.setString(stIndex++, entity.getLangid());
                }

                if (entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
                    statement.setString(stIndex++, entity.getDelflg());
                }

                if (entity.getActflg() != null && !entity.getActflg().isEmpty()){
                    statement.setString(stIndex++, entity.getActflg());
                }

                statement.setDate(stIndex++, entity.getModAt());
                statement.setString(stIndex++, entity.getModBy());
                statement.setLong(stIndex++, entity.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                    errObj.setErrText("Бичлэг амжилттай засагдлаа.");
                    errObj.setErrDesc("Бичлэг амжилттай засагдлаа.");
                    return errObj;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1061));
        errObj.setErrText("Амжилтгүй боллоо. Дахин оролдоно уу.");
        errObj.setErrDesc("Амжилтгүй боллоо. Дахин оролдоно уу.");

        return errObj;
    }
}
