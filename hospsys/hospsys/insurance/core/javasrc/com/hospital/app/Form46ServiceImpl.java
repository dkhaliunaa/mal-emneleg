package com.hospital.app;

import com.Config;
import com.enumclass.ErrorType;
import com.jolbox.bonecp.BoneCP;
import com.model.hos.*;
import com.mongodb.BasicDBObject;
import com.rpc.PdfExcelUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Form46ServiceImpl implements IForm46Service {

    private BoneCP boneCP;
    private ErrorEntity errObj;

    public Form46ServiceImpl(BoneCP boneCP){
        this.boneCP = boneCP;
    }

    @Override
    public ErrorEntity insertNewData(Form46Entity form46) throws SQLException {
        PreparedStatement statement = null;

        if (form46 == null){
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        String query_login = "INSERT INTO `hospital`.`hform46` " +
                "(`bagHorooNer_mn`, " +
                "`bagHorooNer_en`, " +
                "`uherNiitToo_mn`, " +
                "`uherNiitToo_en`, " +
                "`tugalToo_mn`, " +
                "`tugalToo_en`, " +
                "`busadToo_mn`, " +
                "`busadToo_en`, " +
                "`guurtaiNiitToo_mn`, " +
                "`guurtaiNiitToo_en`, " +
                "`guurtaiTugalToo_mn`, " +
                "`guurtaiTugalToo_en`, " +
                "`guurtaiBusadToo_mn`, " +
                "`guurtaiBusadToo_en`, " +
                "`dundajHuvi_mn`, " +
                "`dundajHuvi_en`, " +
                "`tugalHuvi_mn`, " +
                "`tugalHuvi_en`, " +
                "`busadHuvi_mn`, " +
                "`busadHuvi_en`, " +
                "`delflg`, " +
                "`actflg`, " +
                "`mod_at`, " +
                "`recorddate`, " +
                "`mod_by`, " +
                "`cre_at`, " +
                "`cre_by`, " +
                "`aimag`," +
                "`sum`," +
                "`aimag_en`," +
                "`sum_en`)" +
                "VALUES " +
                "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, form46.getBagHorooNer());
                statement.setString(2, form46.getBagHorooNer_en());
                statement.setString(3, form46.getUherNiitToo());
                statement.setLong(4, form46.getUherNiitToo_en());
                statement.setLong(5, form46.getTugalToo());
                statement.setLong(6, form46.getTugalToo_en());
                statement.setLong(7, form46.getBusadToo());
                statement.setLong(8, form46.getBusadToo_en());
                statement.setString(9, form46.getGuurtaiNiitToo());
                statement.setLong(10, form46.getGuurtaiNiitToo_en());
                statement.setLong(11, form46.getGuurtaiTugalToo());
                statement.setLong(12, form46.getGuurtaiTugalToo_en());
                statement.setLong(13, form46.getGuurtaiBusadToo());
                statement.setLong(14, form46.getGuurtaiBusadToo_en());
                statement.setString(15, form46.getDundajHuvi());
                statement.setBigDecimal(16, form46.getDundajHuvi_en());
                statement.setString(17, form46.getTugalHuvi());
                statement.setString(18, form46.getTugalHuvi_en());
                statement.setString(19, form46.getBusadHuvi());
                statement.setString(20, form46.getBusadHuvi_en());
                statement.setString(21, form46.getDelflg());
                statement.setString(22, form46.getActflg());
                statement.setDate(23, form46.getModAt());
                statement.setDate(24, form46.getRecordDate());
                statement.setString(25, form46.getModby());
                statement.setDate(26, form46.getCreat());
                statement.setString(27, form46.getCreby());
                statement.setString(28, form46.getAimag());
                statement.setString(29, form46.getSum());
                statement.setString(30, form46.getAimag_en());
                statement.setString(31, form46.getSum_en());


                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));
                    errObj.setErrText("Бичлэг амжилттай хадгалагдлаа.");
                    errObj.setErrDesc("Бичлэг амжилттай хадгалагдлаа.");
                    return errObj;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }


            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1057));
            errObj.setErrText("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");
            errObj.setErrDesc("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");

            return errObj;
        }
    }

    @Override
    public List<Form46Entity> getListData(Form46Entity form46) throws SQLException {
        int stIndex = 1;
        List<Form46Entity> form46EntitieList = null;

        String query = "SELECT `hform46`.`id`, " +
                "    `hform46`.`bagHorooNer_mn`, " +
                "    `hform46`.`bagHorooNer_en`, " +
                "    `hform46`.`uherNiitToo_mn`, " +
                "    `hform46`.`uherNiitToo_en`, " +
                "    `hform46`.`tugalToo_mn`, " +
                "    `hform46`.`tugalToo_en`, " +
                "    `hform46`.`busadToo_mn`, " +
                "    `hform46`.`busadToo_en`, " +
                "    `hform46`.`guurtaiNiitToo_mn`, " +
                "    `hform46`.`guurtaiNiitToo_en`, " +
                "    `hform46`.`guurtaiTugalToo_mn`, " +
                "    `hform46`.`guurtaiTugalToo_en`, " +
                "    `hform46`.`guurtaiBusadToo_mn`, " +
                "    `hform46`.`guurtaiBusadToo_en`, " +
                "    `hform46`.`dundajHuvi_mn`, " +
                "    `hform46`.`dundajHuvi_en`, " +
                "    `hform46`.`tugalHuvi_mn`, " +
                "    `hform46`.`tugalHuvi_en`, " +
                "    `hform46`.`busadHuvi_mn`, " +
                "    `hform46`.`busadHuvi_en`, " +
                "    `hform46`.`delflg`, " +
                "    `hform46`.`actflg`, " +
                "    `hform46`.`mod_at`, " +
                "    `hform46`.`mod_by`, " +
                "    `hform46`.`cre_at`, " +
                "    `hform46`.`recorddate`, " +
                "    `hform46`.`cre_by`, " +
                "    `hform46`.`aimag`, " +
                "    `hform46`.`sum`, " +
                "    `hform46`.`aimag_en`, " +
                "    `hform46`.`sum_en`, " +
                "    c.cityname, c.cityname_en, " +
                "    `h_sum`.sumname, `h_sum`.sumname_en," +
                "    `h_baghoroo`.horooname, `h_baghoroo`.horooname_en,  " +
                "    `h_sum`.latitude as sum_latitude, `h_sum`.longitude as sum_longitude  " +
                "FROM `hospital`.`hform46` INNER JOIN `h_city` AS c " +
                "ON c.code = `hform46`.aimag " +
                "INNER JOIN `h_sum` " +
                "ON `h_sum`.sumcode = `hform46`.sum " +
                "INNER JOIN `h_baghoroo` ON `hform46`.bagHorooNer_mn = `h_baghoroo`.horoocode AND `h_baghoroo`.sumcode = `hform46`.`sum` " +
                "WHERE ";

        if (form46.getBagHorooNer() != null && !form46.getBagHorooNer().isEmpty()){
            query += " bagHorooNer_mn = ? AND ";
        }

        if (form46.getBagHorooNer_en() != null && !form46.getBagHorooNer_en().isEmpty()){
            query += " bagHorooNer_en = ? AND ";
        }

        if (form46.getUherNiitToo() != null && !form46.getUherNiitToo().toString().isEmpty()){
            query += " uherNiitToo_mn = ? AND ";
        }
        if (form46.getUherNiitToo_en() != null && !form46.getUherNiitToo_en().toString().isEmpty()){
            query += " uherNiitToo_en = ? AND ";
        }

        if (form46.getTugalToo() != null && !form46.getTugalToo().toString().isEmpty() && form46.getTugalToo() !=0){
            query += " tugalToo_mn = ? AND ";
        }
        if (form46.getTugalToo_en() != null && !form46.getTugalToo_en().toString().isEmpty()){
            query += " tugalToo_en = ? AND ";
        }

        if (form46.getBusadToo() != null && !form46.getBusadToo().toString().isEmpty() && form46.getBusadToo() !=0){
            query += " busadToo_mn = ? AND ";
        }
        if (form46.getBusadToo_en() != null && !form46.getBusadToo_en().toString().isEmpty()){
            query += " busadToo_en = ? AND ";
        }

        if (form46.getGuurtaiBusadToo() != null && !form46.getGuurtaiBusadToo().toString().isEmpty() && form46.getGuurtaiBusadToo() !=0){
            query += " guurtaiNiitToo_mn = ? AND ";
        }
        if (form46.getGuurtaiBusadToo_en() != null && !form46.getGuurtaiBusadToo_en().toString().isEmpty()){
            query += " guurtaiNiitToo_en = ? AND ";
        }

        if (form46.getGuurtaiTugalToo() != null && !form46.getGuurtaiTugalToo().toString().isEmpty() && form46.getGuurtaiTugalToo() !=0){
            query += " guurtaiTugalToo_mn = ? AND ";
        }
        if (form46.getGuurtaiTugalToo_en() != null && !form46.getGuurtaiTugalToo_en().toString().isEmpty()){
            query += " guurtaiTugalToo_en = ? AND ";
        }

        if (form46.getGuurtaiBusadToo() != null && !form46.getGuurtaiBusadToo().toString().isEmpty() && form46.getGuurtaiBusadToo() !=0){
            query += " guurtaiBusadToo_mn = ? AND  ";
        }
        if (form46.getGuurtaiBusadToo_en() != null && !form46.getGuurtaiBusadToo_en().toString().isEmpty()){
            query += " guurtaiBusadToo_en = ? AND ";
        }

        if (form46.getDundajHuvi() != null && !form46.getDundajHuvi().toString().isEmpty()){
            query += " dundajHuvi_mn = ? AND  ";
        }
        if (form46.getDundajHuvi_en() != null && !form46.getDundajHuvi_en().toString().isEmpty()){
            query += " dundajHuvi_en = ? AND ";
        }

        if (form46.getTugalHuvi() != null && !form46.getTugalHuvi().toString().isEmpty()){
            query += " tugalHuvi_mn = ? AND  ";
        }
        if (form46.getTugalHuvi_en() != null && !form46.getTugalHuvi_en().toString().isEmpty()){
            query += " tugalHuvi_en = ? AND ";
        }

        if (form46.getBusadHuvi() != null && !form46.getBusadHuvi().toString().isEmpty()){
            query += " busadHuvi_mn = ? AND  ";
        }
        if (form46.getBusadHuvi_en() != null && !form46.getBusadHuvi_en().toString().isEmpty()){
            query += " busadHuvi_en = ? AND ";
        }

        if (form46 != null && form46.getAimag() != null && !form46.getAimag().isEmpty()){
            query += " `aimag` = ? AND ";
        }

        if (form46 != null && form46.getSum() != null && !form46.getSum().isEmpty()){
            query += " `sum` = ? AND ";
        }

        if (form46 != null && form46.getRecordDate() != null && form46.getSearchRecordDate() != null){
            query += " (`recorddate` BETWEEN ? AND ?) AND ";
        }

        if (form46 != null && form46.getCreby() != null && !form46.getCreby().isEmpty()){
            query += " `hform46`.`cre_by` = ? AND ";
        }

        query += " `hform46`.`delflg` = 'N' ORDER BY `cre_at` DESC ";

        try(Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query);

                if (form46.getBagHorooNer() != null && !form46.getBagHorooNer().isEmpty()) {
                    statement.setString(stIndex++, form46.getBagHorooNer());
                }

                if (form46.getBagHorooNer_en() != null && !form46.getBagHorooNer_en().isEmpty()) {
                    statement.setString(stIndex++, form46.getBagHorooNer_en());
                }

                if (form46.getUherNiitToo() != null && !form46.getUherNiitToo().toString().isEmpty()) {
                    statement.setString(stIndex++, String.valueOf(form46.getUherNiitToo()));
                }
                if (form46.getUherNiitToo_en() != null && !form46.getUherNiitToo_en().toString().isEmpty()) {
                    statement.setString(stIndex++, String.valueOf(form46.getUherNiitToo_en()));
                }

                if (form46.getTugalToo() != null && !form46.getTugalToo().toString().isEmpty() && form46.getTugalToo() !=0) {
                    statement.setString(stIndex++, String.valueOf(form46.getTugalToo()));
                }
                if (form46.getTugalToo_en() != null && !form46.getTugalToo_en().toString().isEmpty()) {
                    statement.setString(stIndex++, String.valueOf(form46.getTugalToo_en()));
                }

                if (form46.getBusadToo() != null && !form46.getBusadToo().toString().isEmpty() && form46.getBusadToo() !=0) {
                    statement.setString(stIndex++, String.valueOf(form46.getBusadToo()));
                }
                if (form46.getBusadToo_en() != null && !form46.getBusadToo_en().toString().isEmpty()) {
                    statement.setString(stIndex++, String.valueOf(form46.getBusadToo_en()));
                }

                if (form46.getGuurtaiNiitToo() != null && !form46.getGuurtaiNiitToo().toString().isEmpty()) {
                    statement.setString(stIndex++, String.valueOf(form46.getGuurtaiNiitToo()));
                }
                if (form46.getGuurtaiNiitToo_en() != null && !form46.getGuurtaiNiitToo_en().toString().isEmpty()) {
                    statement.setString(stIndex++, String.valueOf(form46.getGuurtaiNiitToo_en()));
                }

                if (form46.getGuurtaiTugalToo() != null && !form46.getGuurtaiTugalToo().toString().isEmpty() && form46.getGuurtaiTugalToo() !=0) {
                    statement.setString(stIndex++, String.valueOf(form46.getGuurtaiTugalToo()));
                }
                if (form46.getGuurtaiTugalToo_en() != null && !form46.getGuurtaiTugalToo_en().toString().isEmpty()) {
                    statement.setString(stIndex++, String.valueOf(form46.getGuurtaiTugalToo_en()));
                }

                if (form46.getGuurtaiBusadToo() != null && !form46.getGuurtaiBusadToo().toString().isEmpty() && form46.getGuurtaiBusadToo() !=0) {
                    statement.setString(stIndex++, String.valueOf(form46.getGuurtaiBusadToo()));
                }
                if (form46.getGuurtaiBusadToo_en() != null && !form46.getGuurtaiBusadToo_en().toString().isEmpty()) {
                    statement.setString(stIndex++, String.valueOf(form46.getGuurtaiBusadToo_en()));
                }

                if (form46.getDundajHuvi() != null && !form46.getDundajHuvi().toString().isEmpty()) {
                    statement.setString(stIndex++, String.valueOf(form46.getDundajHuvi()));
                }
                if (form46.getDundajHuvi_en() != null && !form46.getDundajHuvi_en().toString().isEmpty()) {
                    statement.setString(stIndex++, String.valueOf(form46.getDundajHuvi_en()));
                }

                if (form46.getTugalHuvi() != null && !form46.getTugalHuvi().toString().isEmpty()) {
                    statement.setString(stIndex++, String.valueOf(form46.getTugalHuvi()));
                }
                if (form46.getTugalHuvi_en() != null && !form46.getTugalHuvi_en().toString().isEmpty()) {
                    statement.setString(stIndex++, String.valueOf(form46.getTugalHuvi_en()));
                }

                if (form46.getBusadHuvi() != null && !form46.getBusadHuvi().toString().isEmpty()) {
                    statement.setString(stIndex++, String.valueOf(form46.getBusadHuvi()));
                }
                if (form46.getBusadHuvi_en() != null && !form46.getBusadHuvi_en().toString().isEmpty()) {
                    statement.setString(stIndex++, String.valueOf(form46.getBusadHuvi_en()));
                }

                if (form46 != null && form46.getAimag() != null && !form46.getAimag().isEmpty()){
                    statement.setString(stIndex++, form46.getAimag());
                }
                if (form46 != null && form46.getSum() != null && !form46.getSum().isEmpty()){
                    statement.setString(stIndex++, form46.getSum());
                }

                if (form46 != null && form46.getRecordDate() != null && form46.getSearchRecordDate() != null){
                    statement.setDate(stIndex++, form46.getRecordDate());
                    statement.setDate(stIndex++, form46.getSearchRecordDate());
                }

                if (form46 != null && form46.getCreby() != null && !form46.getCreby().isEmpty()){
                    statement.setString(stIndex++, form46.getCreby());
                }

                ResultSet resultSet = statement.executeQuery();

                form46EntitieList = new ArrayList<>();

                IComboService comboService = new ComboServiceImpl(boneCP);
                List<BasicDBObject> cmbType = comboService.getComboVals("MAL", "MN");
                int index=1;
                while (resultSet.next()) {
                    Form46Entity entity = new Form46Entity();

                    entity.setId(resultSet.getLong("id"));
                    entity.setBagHorooNer(resultSet.getString("bagHorooNer_mn"));
                    entity.setBagHorooNer_en(resultSet.getString("bagHorooNer_en"));
                    entity.setUherNiitToo(resultSet.getString("uherNiitToo_mn"));
                    entity.setUherNiitToo_en(resultSet.getLong("uherNiitToo_en"));
                    entity.setTugalToo(resultSet.getLong("tugalToo_mn"));
                    entity.setTugalToo_en(resultSet.getLong("tugalToo_en"));
                    entity.setBusadToo(resultSet.getLong("busadToo_mn"));
                    entity.setBusadToo_en(resultSet.getLong("busadToo_en"));
                    entity.setGuurtaiNiitToo(resultSet.getString("guurtaiNiitToo_mn"));
                    entity.setGuurtaiNiitToo_en(resultSet.getLong("guurtaiNiitToo_en"));
                    entity.setGuurtaiTugalToo(resultSet.getLong("guurtaiTugalToo_mn"));
                    entity.setGuurtaiTugalToo_en(resultSet.getLong("guurtaiTugalToo_en"));
                    entity.setGuurtaiBusadToo(resultSet.getLong("guurtaiBusadToo_mn"));
                    entity.setGuurtaiBusadToo_en(resultSet.getLong("guurtaiBusadToo_en"));
                    entity.setDundajHuvi(resultSet.getString("dundajHuvi_mn"));
                    entity.setDundajHuvi_en(resultSet.getBigDecimal("dundajHuvi_en"));
                    entity.setTugalHuvi(resultSet.getString("tugalHuvi_mn"));
                    entity.setTugalHuvi_en(resultSet.getString("tugalHuvi_en"));
                    entity.setBusadHuvi(resultSet.getString("busadHuvi_mn"));
                    entity.setBusadHuvi_en(resultSet.getString("busadHuvi_en"));
                    entity.setDelflg(resultSet.getString("delflg"));
                    entity.setActflg(resultSet.getString("actflg"));
                    entity.setModAt(resultSet.getDate("mod_at"));
                    entity.setModby(resultSet.getString("mod_by"));
                    entity.setCreat(resultSet.getDate("cre_at"));
                    entity.setCreby(resultSet.getString("cre_by"));
                    entity.setAimag(resultSet.getString("aimag"));
                    entity.setSum(resultSet.getString("sum"));
                    entity.setAimag_en(resultSet.getString("aimag_en"));
                    entity.setSum_en(resultSet.getString("sum_en"));
                    entity.setAimagNer(resultSet.getString("cityname"));
                    entity.setSumNer(resultSet.getString("sumname"));
                    entity.setRecordDate(resultSet.getDate("recorddate"));
                    entity.put("baghorooname", resultSet.getString("horooname"));

                    entity.put("sum_latitude", resultSet.getString("sum_latitude"));
                    entity.put("sum_longitude", resultSet.getString("sum_longitude"));
                    entity.put("index",index++);
                    form46EntitieList.add(entity);
                }

                if (statement != null)statement.close();
                if (resultSet != null) resultSet.close();
            }catch (Exception ex){
                ex.printStackTrace();
            }finally {
                connection.close();
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }

        return form46EntitieList;
    }

    @Override
    public ErrorEntity updateData(Form46Entity form46) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (form46 == null){
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        query = "UPDATE `hospital`.`hform46` " +
                "SET     ";

        if (form46.getBagHorooNer() != null && !form46.getBagHorooNer().isEmpty()){
            query += " `bagHorooNer_mn` = ?, ";
        }
        if (form46.getBagHorooNer_en() != null && !form46.getBagHorooNer_en().isEmpty()){
            query += " `bagHorooNer_en` = ?, ";
        }

        if (form46.getUherNiitToo() != null && !form46.getUherNiitToo().toString().isEmpty()){
            query += " `uherNiitToo_mn` = ?, ";
        }
        if (form46.getUherNiitToo_en() != null && !form46.getUherNiitToo_en().toString().isEmpty()){
            query += " `uherNiitToo_en` = ?, ";
        }

        if (form46.getTugalToo() != null && !form46.getTugalToo().toString().isEmpty()){
            query += " `tugalToo_mn` = ?, ";
        }
        if (form46.getTugalToo_en() != null && !form46.getTugalToo_en().toString().isEmpty()){
            query += " `tugalToo_en` = ?, ";
        }

        if (form46.getBusadToo() != null && !form46.getBusadToo().toString().isEmpty()){
            query += " `busadToo_mn` = ?, ";
        }
        if (form46.getBusadToo_en() != null && !form46.getBusadToo_en().toString().isEmpty()){
            query += " `busadToo_en` = ?, ";
        }

        if (form46.getGuurtaiNiitToo() != null && !form46.getGuurtaiNiitToo().toString().isEmpty()){
            query += " `guurtaiNiitToo_mn` = ?, ";
        }
        if (form46.getGuurtaiNiitToo_en() != null && !form46.getGuurtaiNiitToo_en().toString().isEmpty()){
            query += " `guurtaiNiitToo_en` = ?, ";
        }

        if (form46.getGuurtaiTugalToo() != null && !form46.getGuurtaiTugalToo().toString().isEmpty()){
            query += " `guurtaiTugalToo_mn` = ?, ";
        }
        if (form46.getGuurtaiTugalToo_en() != null && !form46.getGuurtaiTugalToo_en().toString().isEmpty()){
            query += " `guurtaiTugalToo_en` = ?, ";
        }

        if (form46.getGuurtaiBusadToo() != null && !form46.getGuurtaiBusadToo().toString().isEmpty()){
            query += " `guurtaiBusadToo_mn` = ?, ";
        }
        if (form46.getGuurtaiBusadToo_en() != null && !form46.getGuurtaiBusadToo_en().toString().isEmpty()){
            query += " `guurtaiBusadToo_en` = ?, ";
        }

        if (form46.getDundajHuvi() != null && !form46.getDundajHuvi().toString().isEmpty()){
            query += " `dundajHuvi_mn` = ?, ";
        }
        if (form46.getDundajHuvi_en() != null && !form46.getDundajHuvi_en().toString().isEmpty()){
            query += " `dundajHuvi_en` = ?, ";
        }

        if (form46.getTugalHuvi() != null && !form46.getTugalHuvi().toString().isEmpty()){
            query += " `tugalHuvi_mn` = ?, ";
        }
        if (form46.getTugalHuvi_en() != null && !form46.getTugalHuvi_en().toString().isEmpty()){
            query += " `tugalHuvi_en` = ?, ";
        }

        if (form46.getBusadHuvi() != null && !form46.getBusadHuvi().toString().isEmpty()){
            query += " `busadHuvi_mn` = ?, ";
        }
        if (form46.getBusadHuvi_en() != null && !form46.getBusadHuvi_en().toString().isEmpty()){
            query += " `busadHuvi_en` = ?, ";
        }

        if (form46.getDelflg() != null && !form46.getDelflg().toString().isEmpty()){
            query += " `delflg` = ?, ";
        }
        if (form46.getActflg() != null && !form46.getActflg().toString().isEmpty()){
            query += " `actflg` = ?, ";
        }

        if (form46 != null && form46.getRecordDate() != null){
            query += " `recorddate` = ?, ";
        }


        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";
        query += " WHERE  ID   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try{
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (form46.getBagHorooNer() != null && !form46.getBagHorooNer().isEmpty()){
                    statement.setString(stIndex ++, form46.getBagHorooNer());
                }

                if (form46.getBagHorooNer_en() != null && !form46.getBagHorooNer_en().isEmpty()){
                    statement.setString(stIndex ++, form46.getBagHorooNer_en());
                }

                if (form46.getUherNiitToo() != null && !form46.getUherNiitToo().toString().isEmpty()){
                    statement.setString(stIndex ++, form46.getUherNiitToo());
                }
                if (form46.getUherNiitToo_en() != null && !form46.getUherNiitToo_en().toString().isEmpty()){
                    statement.setLong(stIndex ++, form46.getUherNiitToo_en());
                }

                if (form46.getTugalToo() != null && !form46.getTugalToo().toString().isEmpty()){
                    statement.setLong(stIndex ++, form46.getTugalToo());
                }
                if (form46.getTugalToo_en() != null && !form46.getTugalToo_en().toString().isEmpty()){
                    statement.setLong(stIndex ++, form46.getTugalToo_en());
                }

                if (form46.getBusadToo() != null && !form46.getBusadToo().toString().isEmpty()){
                    statement.setLong(stIndex ++, form46.getBusadToo());
                }
                if (form46.getBusadToo_en() != null && !form46.getBusadToo_en().toString().isEmpty()){
                    statement.setLong(stIndex ++, form46.getBusadToo_en());
                }

                if (form46.getGuurtaiNiitToo() != null && !form46.getGuurtaiNiitToo().toString().isEmpty()){
                    statement.setString(stIndex ++, form46.getGuurtaiNiitToo());
                }
                if (form46.getGuurtaiNiitToo_en() != null && !form46.getGuurtaiNiitToo_en().toString().isEmpty()){
                    statement.setLong(stIndex ++, form46.getGuurtaiNiitToo_en());
                }

                if (form46.getGuurtaiTugalToo() != null && !form46.getGuurtaiTugalToo().toString().isEmpty()){
                    statement.setLong(stIndex ++, form46.getGuurtaiTugalToo());
                }
                if (form46.getGuurtaiTugalToo_en() != null && !form46.getGuurtaiTugalToo_en().toString().isEmpty()){
                    statement.setLong(stIndex ++, form46.getGuurtaiTugalToo_en());
                }

                if (form46.getGuurtaiBusadToo() != null && !form46.getGuurtaiBusadToo().toString().isEmpty()){
                    statement.setLong(stIndex ++, form46.getGuurtaiBusadToo());
                }
                if (form46.getGuurtaiBusadToo_en() != null && !form46.getGuurtaiBusadToo_en().toString().isEmpty()){
                    statement.setLong(stIndex ++, form46.getGuurtaiBusadToo_en());
                }

                if (form46.getDundajHuvi() != null && !form46.getDundajHuvi().toString().isEmpty()){
                    statement.setString(stIndex ++, form46.getDundajHuvi());
                }
                if (form46.getDundajHuvi_en() != null && !form46.getDundajHuvi_en().toString().isEmpty()){
                    statement.setBigDecimal(stIndex ++, form46.getDundajHuvi_en());
                }

                if (form46.getTugalHuvi() != null && !form46.getTugalHuvi().toString().isEmpty()){
                    statement.setString(stIndex ++, form46.getTugalHuvi());
                }
                if (form46.getTugalHuvi_en() != null && !form46.getTugalHuvi_en().toString().isEmpty()){
                    statement.setString(stIndex ++, form46.getTugalHuvi_en());
                }

                if (form46.getBusadHuvi() != null && !form46.getBusadHuvi().toString().isEmpty()){
                    statement.setString(stIndex ++, form46.getBusadHuvi());
                }
                if (form46.getBusadHuvi_en() != null && !form46.getBusadHuvi_en().toString().isEmpty()){
                    statement.setString(stIndex ++, form46.getBusadHuvi_en());
                }

                if (form46.getDelflg() != null && !form46.getDelflg().toString().isEmpty()){
                    statement.setString(stIndex ++, form46.getDelflg());
                }
                if (form46.getActflg() != null && !form46.getActflg().toString().isEmpty()){
                    statement.setString(stIndex ++, form46.getActflg());
                }

                if (form46 != null && form46.getRecordDate() != null){
                    statement.setDate(stIndex++, form46.getRecordDate());
                }

                statement.setDate(stIndex ++, form46.getModAt());
                statement.setString(stIndex ++, form46.getModby());
                statement.setLong(stIndex ++, form46.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1){
                    connection.commit();
                    errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                    errObj.setErrText("Бичлэг амжилттай засагдлаа.");
                    errObj.setErrDesc("Бичлэг амжилттай засагдлаа.");
                    return errObj;
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }finally {
                if (statement != null) statement.close();
                connection.close();
            }
        }catch (Exception ex) {
            ex.printStackTrace();
        }


        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1061));
        errObj.setErrText("Амжилтгүй боллоо. Дахин оролдоно уу.");
        errObj.setErrDesc("Амжилтгүй боллоо. Дахин оролдоно уу.");

        return errObj;
    }

    @Override
    public List<Form46Entity> getPieChartData(Form46Entity form46Entity) throws SQLException {
        int stIndex = 1;
        List<Form46Entity> form46EntitieList = null;

        String query_sel = "SELECT `hform46`.`id`,  " +
                "    `hform46`.`aimag`,  " +
                "    `hform46`.`sum`,  " +
                "    SUM(`hform46`.`zartsuulsan_hemjee_mn`) as zartsuulsan_hemjee,  " +
                "    c.cityname, c.cityname_en,  " +
                "    `h_sum`.sumname, `h_sum`.sumname_en  " +
                "FROM `hospital`.`hform46`  " +
                "INNER JOIN `h_city` AS c  " +
                "ON c.code = `hform46`.aimag  " +
                "INNER JOIN `h_sum`  " +
                "ON `h_sum`.sumcode = `hform46`.sum  " +
                "WHERE `hform46`.`delflg` = 'N' and `hform46`.`actflg` = 'Y' and `hform46`.`aimag` = ?  " +
                "GROUP BY `hform46`.`sum` ";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                if (form46Entity.getAimag() != null && !form46Entity.getAimag().isEmpty()) {
                    statement.setString(stIndex++, form46Entity.getAimag());
                }

                ResultSet resultSet = statement.executeQuery();

                form46EntitieList = new ArrayList<>();

                while (resultSet.next()) {
                    Form46Entity entity = new Form46Entity();

                    entity.setId(resultSet.getLong("id"));
                    entity.setAimag(resultSet.getString("aimag"));
                    entity.setSum(resultSet.getString("sum"));
//                    entity.setAimagNer(resultSet.getString("cityname"));
//                    entity.setSumNer(resultSet.getString("sumname"));

                    form46EntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return form46EntitieList;
    }

    @Override
    public ErrorEntity exportReport(String file, Form46Entity form46) throws SQLException {
        ErrorEntity errorEntity = null;
        String fileName = "hform46";

        try (Connection connection = boneCP.getConnection()){
            String JRXML_PATH = Config.getJrxmlPath();
            String REPORT_PATH = Config.getReportPath();

            try {
                Map parameters = new HashMap();

                String reportTitle = "ҮХРИЙН АРЬСНЫ ГУУРТАХ ӨВЧНИЙ $$YEAR$$ ОНЫ МЭДЭЭ",
                        currentYear = "$$YEAR$$ он №....",
                        printDate = "$$YEAR$$ оны $$MONTH$$ сарын $$DAY$$";

                Calendar now = Calendar.getInstance();

                int year = now.get(Calendar.YEAR);
                int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
                int day = now.get(Calendar.DAY_OF_MONTH);

                printDate = printDate.replace("$$YEAR$$", String.valueOf(year));
                printDate = printDate.replace("$$MONTH$$", String.valueOf(month));
                printDate = printDate.replace("$$DAY$$", String.valueOf(day));

                currentYear = currentYear.replace("$$YEAR$$", String.valueOf(year));

                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(new Date(form46.getRecordDate().getTime()));

                reportTitle = reportTitle.replace("$$YEAR$$", String.valueOf(calendar1.get(Calendar.YEAR)));

                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(new Date(form46.getSearchRecordDate().getTime()));

                if (calendar2.get(Calendar.MONTH) < 4){
                    reportTitle = reportTitle.replace("$$SEASON$$", "1");
                }
                else if (calendar2.get(Calendar.MONTH) < 7){
                    reportTitle = reportTitle.replace("$$SEASON$$", "2");
                }
                else if (calendar2.get(Calendar.MONTH) < 10){
                    reportTitle = reportTitle.replace("$$SEASON$$", "3");
                }
                else if (calendar2.get(Calendar.MONTH) <= 12){
                    reportTitle = reportTitle.replace("$$SEASON$$", "4");
                }

                parameters.put("start_date", form46.getRecordDate());
                parameters.put("end_date", form46.getSearchRecordDate());
                parameters.put("citycode", (form46.getAimag() != null && !form46.getAimag().isEmpty()) ? form46.getAimag() : null);
                parameters.put("sumcode", (form46.getSum() != null && !form46.getSum().isEmpty()) ? form46.getSum() : null);
                parameters.put("currentyear", currentYear);

                ICityService cityService = new CityServiceImpl(boneCP);
                City tmpCity = new City();
                tmpCity.setCode(form46.getAimag());
                List<City> clist = cityService.selectAll(tmpCity);

                if (clist != null && clist.size() == 1) {
                    parameters.put("aimagner", clist.get(0).getCityname());
                } else {
                    parameters.put("aimagner", (form46.getAimagNer() == null) ? "" : form46.getAimagNer());
                }
                ISumService sumService = new SumServiceImpl(boneCP);
                Sum tmpSum = new Sum();
                tmpSum.setCitycode(form46.getAimag());
                tmpSum.setSumcode(form46.getSum());
                List<Sum> sumList = sumService.selectAll(tmpSum);

                if (sumList != null && sumList.size() == 1){
                    parameters.put("sumner", sumList.get(0).getSumname());
                }
                else{
                    parameters.put("sumner", (form46.getSumNer() == null) ? "" : form46.getSumNer());
                }

                parameters.put("printdate", printDate);
                parameters.put("reporttitle", reportTitle);
                parameters.put("creby", (form46.getCreby() != null && !form46.getCreby().isEmpty()) ? form46.getCreby() : null);

                PdfExcelUtil.generateReport(file, JRXML_PATH, REPORT_PATH, fileName, parameters, connection);

                System.out.println("File Generated");
            }catch (Exception ex){
                ex.printStackTrace();
            }
            finally {
                if (connection != null)connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1001));

            if(file.equals("pdf")){
                errorEntity.setErrText(fileName + ".pdf");
            }else if(file.equals("xls")){
                errorEntity.setErrText(fileName + ".xls");
            }
        }
        return errorEntity;
    }

    @Override
    public BasicDBObject getColHighChartData(Form46Entity entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();
        List<String> categories = new ArrayList<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }

        try {

            IComboService comboService = new ComboServiceImpl(boneCP);

            List<BasicDBObject> cmbType = comboService.getComboNames("UHERGUURST", "MN");
            System.out.println(cmbType);
            //Saraar n haruulah
            int month = calendar1.get(Calendar.MONTH) + 1;
            int year1 = calendar1.get(Calendar.YEAR);
            int year2 = calendar2.get(Calendar.YEAR);

            try (Connection connection = boneCP.getConnection()) {
                String query_sel = "SELECT `uherNiitToo_mn`, sum(`tugalToo_mn`) AS tugal,  sum(`busadToo_mn`) AS busad," +
                        "sum(`guurtaiTugalToo_mn`) AS guursttugal,  sum(`guurtaiBusadToo_mn`) AS guurstbusad, `hform46`.`aimag`, `hform46`.`sum`  " +
                            "FROM `hospital`.`hform46`  " +
                            "WHERE   ";

                if (entity != null && entity.getAimag() != null &&!entity.getAimag().isEmpty()) {
                    query_sel += " `hform46`.`aimag` = ? AND ";
                }
                if (entity != null && entity.getSum() != null && !entity.getSum().isEmpty()) {
                    query_sel += " `hform46`.`sum` = ? AND ";
                }
                if (entity != null && !entity.getDelflg().isEmpty()) {
                    query_sel += " `hform46`.`delflg` = ? AND ";
                }
                if (entity != null && !entity.getActflg().isEmpty()) {
                    query_sel += " `hform46`.`actflg` = ? AND ";
                }
                if (entity != null && !entity.getCreby().isEmpty()) {
                    query_sel += " `hform46`.`cre_by` = ? AND ";
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                    query_sel += " (`recorddate` BETWEEN ? AND ?)";
                }

                HashMap<String, List<Double>> resultArray = new HashMap<>();


                do {
                    //do process
                    categories.add(year1 + "-" + month);

                    try {
                        PreparedStatement statement = connection.prepareStatement(query_sel);

                        java.sql.Date stdate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-01 00:00:00").getTime());

                        java.sql.Date enddate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-31 00:00:00").getTime());

                        if (month == 12 && year1 < year2) {
                            month = 1;
                            year1++;
                        } else {
                            month++;
                        }

                        int stindex = 1;

                        if (entity != null && entity.getAimag() != null  && !entity.getAimag().isEmpty()) {
                            statement.setString(stindex++, entity.getAimag());
                        }
                        if (entity != null && entity.getSum() != null  && !entity.getSum().isEmpty()) {
                            statement.setString(stindex++, entity.getSum());
                        }
                        if (entity != null && !entity.getDelflg().isEmpty()) {
                            statement.setString(stindex++, entity.getDelflg());
                        }
                        if (entity != null && !entity.getActflg().isEmpty()) {
                            statement.setString(stindex++, entity.getActflg());
                        }
                        if (entity != null && !entity.getCreby().isEmpty()) {
                            statement.setString(stindex++, entity.getCreby());
                        }
                        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                            statement.setDate(stindex++, stdate);
                            statement.setDate(stindex++, enddate);
                        }


                        ResultSet resultSet = statement.executeQuery();

                        HashMap<String, String> hashMap = new HashMap<>();

                        while (resultSet.next()) {
                            String tugal = resultSet.getString("tugal");
                            String busad = resultSet.getString("busad");
                            String guursttugal = resultSet.getString("guursttugal");
                            String guurstbusad = resultSet.getString("guurstbusad");
                            hashMap.put("tugal", tugal);
                            hashMap.put("busad", busad);
                            hashMap.put("guurstsantugal", guursttugal);
                            hashMap.put("guurstsanbusad", guurstbusad);
                        }//end while
                        List<Double> list = new ArrayList<>();
                        for (int i = 0; i < cmbType.size(); i++) {
                            BasicDBObject obj = cmbType.get(i);

                            if (obj.get("data") == null){
                                //new record
                                list = new ArrayList<>();
                                String val = hashMap.get(obj.getString("id"));
                                if (val == null) list.add(0.0);
                                else if (val.isEmpty()) list.add(0.0);
                                else list.add(Double.parseDouble(val));
                                obj.put("data", list);
                            }else{
                                //already exist

                                list = (List<Double>) obj.get("data");
                                String val = hashMap.get(obj.getString("id"));
                                if (val == null) list.add(0.0);
                                else if (val.isEmpty()) list.add(0.0);
                                else list.add(Double.parseDouble(val));
                            }
                        }
                        System.out.println( hashMap);

                        if (statement != null) statement.close();
                        if (resultSet != null) resultSet.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();

                        break;
                    }

                    ///
                } while ((month <= calendar2.get(Calendar.MONTH) + 1 && year1 <= year2) || year1 < year2);

                basicDBObject.put("categories", categories);
                basicDBObject.put("series", cmbType);

                connection.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }

    @Override
    public BasicDBObject getPieHighChartData(Form46Entity entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }

        String query_sel = "";
        if (entity!= null && entity.getPie() != null && !entity.getPie().isEmpty() && entity.getPie().equals("1.0")) {
            query_sel = "SELECT `uherNiitToo_mn`, sum(`tugalToo_mn`) AS tugal,  sum(`busadToo_mn`) AS busad, `hform46`.`aimag`, `hform46`.`sum`  " +
                    "FROM `hospital`.`hform46`  " +
                    "WHERE   ";
        } else {
            query_sel = "SELECT `uherNiitToo_mn`, sum(`guurtaiTugalToo_mn`) AS tugal,  sum(`guurtaiBusadToo_mn`) AS busad, `hform46`.`aimag`, `hform46`.`sum`  " +
                    "FROM `hospital`.`hform46`  " +
                    "WHERE   ";
        }
        if (entity != null && entity.getAimag() != null &&!entity.getAimag().isEmpty()) {
            query_sel += " `hform46`.`aimag` = ? AND ";
        }
        if (entity != null && entity.getSum() != null && !entity.getSum().isEmpty()) {
            query_sel += " `hform46`.`sum` = ? AND ";
        }
        if (entity != null && !entity.getDelflg().isEmpty()) {
            query_sel += " `hform46`.`delflg` = ? AND ";
        }
        if (entity != null && !entity.getActflg().isEmpty()) {
            query_sel += " `hform46`.`actflg` = ? AND ";
        }
        if (entity != null && !entity.getCreby().isEmpty()) {
            query_sel += " `hform46`.`cre_by` = ? AND ";
        }
        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
            query_sel += " (`recorddate` BETWEEN ? AND ?)";
        }

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                int stindex = 1;

                if (entity != null && entity.getAimag() != null && !entity.getAimag().isEmpty()) {
                    statement.setString(stindex++, entity.getAimag());
                }
                if (entity != null && entity.getSum() != null && !entity.getSum().isEmpty()) {
                    statement.setString(stindex++, entity.getSum());
                }
                if (entity != null && !entity.getDelflg().isEmpty()) {
                    statement.setString(stindex++, entity.getDelflg());
                }
                if (entity != null && !entity.getActflg().isEmpty()) {
                    statement.setString(stindex++, entity.getActflg());
                }
                if (entity != null && !entity.getCreby().isEmpty()) {
                    statement.setString(stindex++, entity.getCreby());
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                    statement.setDate(stindex++, entity.getRecordDate());
                    statement.setDate(stindex++, entity.getSearchRecordDate());
                }
                ResultSet resultSet = statement.executeQuery();

                List<BasicDBObject> list = new ArrayList<>();

                while (resultSet.next()) {
                    BasicDBObject entity1 = new BasicDBObject();
                    BasicDBObject entity2 = new BasicDBObject();
                    entity1.put("name", "Тугал, Бяруу");
                    entity1.put("y", resultSet.getDouble("tugal"));
                    entity2.put("name", "Бусад");
                    entity2.put("y", resultSet.getDouble("busad"));

                    list.add(entity1);
                    list.add(entity2);
                }

                basicDBObject.put("data", list);

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }

}