package com.hospital.app;

import com.model.hos.ErrorEntity;
import com.model.hos.Form08Entity;
import com.mongodb.BasicDBObject;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public interface IForm08Service {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    /**
     * Insert new data
     *
     *
     * @param form08
     * @return
     * @throws Exception
     */
    ErrorEntity insertNewData(Form08Entity form08) throws SQLException;

    /**
     * Select all
     *
     * @param form08
     * @return
     * @throws SQLException
     */
    List<Form08Entity> getListData(Form08Entity form08) throws SQLException;


    /**
     * update form data
     *
     * @param form08
     * @return
     * @throws SQLException
     */
    ErrorEntity updateData(Form08Entity form08) throws SQLException;


    List<BasicDBObject> getPieChartData(Form08Entity form08Entity) throws SQLException;


    ErrorEntity exportReport(String file, Form08Entity form08) throws SQLException;

    /**
     * Column Chart Data processing...
     *
     * @param form08Entity
     * @return
     * @throws SQLException
     */
    BasicDBObject getColumnChartData(Form08Entity form08Entity) throws SQLException;


    BasicDBObject getColHighChartData(Form08Entity form08Entity) throws SQLException;

    BasicDBObject getPieHighChartData(Form08Entity form08Entity) throws SQLException;
}
