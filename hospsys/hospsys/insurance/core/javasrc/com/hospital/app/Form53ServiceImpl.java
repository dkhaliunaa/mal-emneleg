package com.hospital.app;


import com.Config;
import com.enumclass.ErrorType;
import com.jolbox.bonecp.BoneCP;
import com.model.hos.*;
import com.mongodb.BasicDBObject;
import com.rpc.PdfExcelUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public class Form53ServiceImpl implements IForm53Service {
    /**
     * Objects
     */

    private BoneCP boneCP;
    private ErrorEntity errObj;

    public Form53ServiceImpl(BoneCP boneCP) {
        this.boneCP = boneCP;
    }

    @Override
    public ErrorEntity insertNewData(Form53Entity form53) throws SQLException {
        PreparedStatement statement = null;

        if (form53 == null) {
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        String query_login = "INSERT INTO `hospital`.`hform53` " +
                "(`uvchinNer`, " +
                "`uvchinNer_En`, " +
                "`bugdUvchilsen`, " +
                "`bugdUhsen`, " +
                "`bugdEdgersen`, " +
                "`botgoUvchilsen`, " +
                "`botgoUhsen`, " +
                "`botgoEdgersen`, " +
                "`unagaUvchilsen`, " +
                "`unagaUhsen`, " +
                "`unagaEdgersen`, " +
                "`tugalUvchilsen`, " +
                "`tugalUhsen`, " +
                "`tugalEdgersen`, " +
                "`hurgaUvchilsen`, " +
                "`hurgaUhsen`, " +
                "`hurgaEdgersen`, " +
                "`ishigUvchilsen`, " +
                "`ishigUhsen`, " +
                "`ishigEdgersen`, " +
                "`busadUvchilsen`, " +
                "`busadUhsen`, " +
                "`busadEdgersen`, " +
                "`del_flg`, " +
                "`actflg`, " +
                "`mod_at`, " +
                "`mod_by`, " +
                "`cre_at`, " +
                "`recorddate`," +
                "`cre_by`," +
                "`aimag`," +
                "`aimag_en`," +
                "`sum`," +
                "`sum_en`)" +
                "VALUES " +
                "(? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? ,? , ? ,? ,? , ? , ? , ? , ? , ? , ? , ? , ? ,? , ? , ? , ? )";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, form53.getUvchinNer());
                statement.setString(2, form53.getUvchinNer_En());
                statement.setLong(3, form53.getBugdUvchilsen());
                statement.setLong(4, form53.getBugdUhsen());
                statement.setLong(5, form53.getBugdEdgersen());
                statement.setLong(6, form53.getBotgoUvchilsen());
                statement.setLong(7, form53.getBotgoUhsen());
                statement.setLong(8, form53.getBotgoEdgersen());
                statement.setLong(9, form53.getUnagaUvchilsen());
                statement.setLong(10, form53.getUnagaUhsen());
                statement.setLong(11, form53.getUnagaEdgersen());
                statement.setLong(12, form53.getTugalUvchilsen());
                statement.setLong(13, form53.getTugalUhsen());
                statement.setLong(14, form53.getTugalEdgersen());
                statement.setLong(15, form53.getHurgaUvchilsen());
                statement.setLong(16, form53.getHurgaUhsen());
                statement.setLong(17, form53.getHurgaEdgersen());
                statement.setLong(18, form53.getIshigUvchilsen());
                statement.setLong(19, form53.getIshigUhsen());
                statement.setLong(20, form53.getIshigEdgersen());
                statement.setLong(21, form53.getBusadUvchilsen());
                statement.setLong(22, form53.getBusadUhsen());
                statement.setLong(23, form53.getBusadEdgersen());
                statement.setString(24, form53.getDelflg());
                statement.setString(25, form53.getActflg());
                statement.setDate(26, form53.getModAt());
                statement.setString(27, form53.getModby());
                statement.setDate(28, form53.getCreat());
                statement.setDate(29, form53.getRecordDate());
                statement.setString(30, form53.getCreby());
                statement.setString(31, form53.getAimag());
                statement.setString(32, form53.getAimag_en());
                statement.setString(33, form53.getSum());
                statement.setString(34, form53.getSum_en());


                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));
                    errObj.setErrText("Бичлэг амжилттай хадгалагдлаа.");
                    errObj.setErrDesc("Бичлэг амжилттай хадгалагдлаа.");
                    return errObj;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }

        } catch (Exception ex) {

        }

        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1057));
        errObj.setErrText("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");
        errObj.setErrDesc("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");

        return errObj;
    }

    @Override
    public List<Form53Entity> getListData(Form53Entity form53) throws SQLException {
        int stIndex = 1;
        List<Form53Entity> form53EntitieList = null;

        String query = "SELECT `hform53`.`id`, " +
                "    `hform53`.`uvchinNer`, " +
                "    `hform53`.`uvchinNer_En`, " +
                "    `hform53`.`bugdUvchilsen`, " +
                "    `hform53`.`bugdUhsen`, " +
                "    `hform53`.`bugdEdgersen`, " +
                "    `hform53`.`botgoUvchilsen`, " +
                "    `hform53`.`botgoUhsen`, " +
                "    `hform53`.`botgoEdgersen`, " +
                "    `hform53`.`unagaUvchilsen`, " +
                "    `hform53`.`unagaUhsen`, " +
                "    `hform53`.`unagaEdgersen`, " +
                "    `hform53`.`tugalUvchilsen`, " +
                "    `hform53`.`tugalUhsen`, " +
                "    `hform53`.`tugalEdgersen`, " +
                "    `hform53`.`hurgaUvchilsen`, " +
                "    `hform53`.`hurgaUhsen`, " +
                "    `hform53`.`hurgaEdgersen`, " +
                "    `hform53`.`ishigUvchilsen`, " +
                "    `hform53`.`ishigUhsen`, " +
                "    `hform53`.`ishigEdgersen`, " +
                "    `hform53`.`busadUvchilsen`, " +
                "    `hform53`.`busadUhsen`, " +
                "    `hform53`.`busadEdgersen`, " +
                "    `hform53`.`del_flg`, " +
                "    `hform53`.`actflg`, " +
                "    `hform53`.`mod_at`, " +
                "    `hform53`.`mod_by`, " +
                "    `hform53`.`cre_at`, " +
                "    `hform53`.`recorddate`, " +
                "    `hform53`.`cre_by`, " +
                "    `hform53`.`sum`, " +
                "    `hform53`.`sum_en`, " +
                "    `hform53`.`aimag`, " +
                "    `hform53`.`aimag_en`, " +
                "    c.cityname, c.cityname_en, " +
                "    `h_sum`.sumname, `h_sum`.sumname_en " +
                "FROM `hospital`.`hform53` INNER JOIN `h_city` AS c " +
                "ON c.code = `hform53`.aimag " +
                "INNER JOIN `h_sum` " +
                "ON `h_sum`.sumcode = `hform53`.sum " +
                "WHERE ";

//        /* NER */
        if (form53.getUvchinNer() != null && !form53.getUvchinNer().toString().isEmpty()) {
            query += " uvchinNer = ? AND ";
        }
        if (form53.getUvchinNer_En() != null && !form53.getUvchinNer_En().toString().isEmpty()) {
            query += " uvchinNer_En = ? AND ";
        }
        /* Zartsuulsan hemjee */
        if (form53.getBugdUvchilsen() != null && !form53.getBugdUvchilsen().toString().isEmpty() && form53.getBugdUvchilsen() != 0) {
            query += " bugdUvchilsen = ? AND ";
        }
        if (form53.getBugdUhsen() != null && !form53.getBugdUhsen().toString().isEmpty() && form53.getBugdUhsen() != 0) {
            query += " bugdUhsen = ? AND ";
        }

        /* MAL TURUL */
        if (form53.getBugdEdgersen() != null && !form53.getBugdEdgersen().toString().isEmpty() && form53.getBugdEdgersen() != 0) {
            query += " bugdEdgersen = ? AND ";
        }
        if (form53.getBotgoUvchilsen() != null && !form53.getBotgoUvchilsen().toString().isEmpty() && form53.getBotgoUvchilsen() != 0) {
            query += " botgoUvchilsen = ? AND ";
        }

        /* Tuluvlugu */
        if (form53.getBotgoUhsen() != null && !form53.getBotgoUhsen().toString().isEmpty() && form53.getBotgoUhsen() != 0) {
            query += " botgoUhsen = ? AND ";
        }
        if (form53.getBotgoEdgersen() != null && !form53.getBotgoEdgersen().toString().isEmpty() && form53.getBotgoEdgersen() != 0) {
            query += " botgoEdgersen = ? AND ";
        }

        /* Guitsetgel */
        if (form53.getUnagaUvchilsen() != null && !form53.getUnagaUvchilsen().toString().isEmpty() && form53.getUnagaUvchilsen() != 0) {
            query += " unagaUvchilsen = ? AND ";
        }
        if (form53.getUnagaUhsen() != null && !form53.getUnagaUhsen().toString().isEmpty() && form53.getUnagaUhsen() != 0) {
            query += " unagaUhsen = ? AND ";
        }

        //ASD
        /* Zartsuulsan hemjee */
        if (form53.getUnagaEdgersen() != null && !form53.getUnagaEdgersen().toString().isEmpty() && form53.getUnagaEdgersen() != 0) {
            query += " unagaEdgersen = ? AND ";
        }
        if (form53.getTugalUvchilsen() != null && !form53.getTugalUvchilsen().toString().isEmpty() && form53.getTugalUvchilsen() != 0) {
            query += " tugalUvchilsen = ? AND ";
        }

        /* MAL TURUL */
        if (form53.getTugalUhsen() != null && !form53.getTugalUhsen().toString().isEmpty() && form53.getTugalUhsen() != 0) {
            query += " tugalUhsen = ? AND ";
        }
        if (form53.getTugalEdgersen() != null && !form53.getTugalEdgersen().toString().isEmpty() && form53.getTugalEdgersen() != 0) {
            query += " tugalEdgersen = ? AND ";
        }

        /* Tuluvlugu */
        if (form53.getHurgaUvchilsen() != null && !form53.getHurgaUvchilsen().toString().isEmpty() && form53.getHurgaUvchilsen() != 0) {
            query += " hurgaUvchilsen = ? AND ";
        }
        if (form53.getHurgaUhsen() != null && !form53.getHurgaUhsen().toString().isEmpty() && form53.getHurgaUhsen() != 0) {
            query += " hurgaUhsen = ? AND ";
        }

        /* Guitsetgel */
        if (form53.getHurgaEdgersen() != null && !form53.getHurgaEdgersen().toString().isEmpty() && form53.getHurgaEdgersen() != 0) {
            query += " hurgaEdgersen = ? AND ";
        }
        if (form53.getIshigUvchilsen() != null && !form53.getIshigUvchilsen().toString().isEmpty() && form53.getIshigUvchilsen() != 0) {
            query += " ishigUvchilsen = ? AND ";
        }

        if (form53.getIshigUhsen() != null && !form53.getIshigUhsen().toString().isEmpty() && form53.getIshigUhsen() != 0) {
            query += " ishigUhsen = ? AND ";
        }
        if (form53.getIshigEdgersen() != null && !form53.getIshigEdgersen().toString().isEmpty() && form53.getIshigEdgersen() != 0) {
            query += " ishigEdgersen = ? AND ";
        }


        if (form53.getBusadUvchilsen() != null && !form53.getBusadUvchilsen().toString().isEmpty() && form53.getBusadUvchilsen() != 0) {
            query += " busadUvchilsen = ? AND ";
        }

        if (form53.getBusadUhsen() != null && !form53.getBusadUhsen().toString().isEmpty() && form53.getBusadUhsen() != 0) {
            query += " busadUhsen = ? AND ";
        }
        if (form53.getBusadEdgersen() != null && !form53.getBusadEdgersen().toString().isEmpty() && form53.getBusadEdgersen() != 0) {
            query += " busadEdgersen = ? AND ";
        }
        //ASD

        if (form53 != null && form53.getAimag() != null && !form53.getAimag().isEmpty()) {
            query += " `aimag` = ? AND ";
        }
        if (form53 != null && form53.getAimag_en() != null && !form53.getAimag_en().isEmpty()) {
            query += " `aimag_en` = ? AND ";
        }
        if (form53.getSum() != null && !form53.getSum().isEmpty()) {
            query += " sum = ? AND ";
        }

        if (form53.getSum_en() != null && !form53.getSum_en().isEmpty()) {
            query += " sum_en = ? AND ";
        }

        if (form53 != null && form53.getRecordDate() != null && form53.getSearchRecordDate() != null) {
            query += " (`recorddate` BETWEEN ? AND ?) AND ";
        }

        if (form53 != null && form53.getCreby() != null && !form53.getCreby().isEmpty()) {
            query += " `hform53`.`cre_by` = ? AND ";
        }

        query += " `hform53`.`del_flg` = 'N'";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query);

//            /* NER */
                if (form53.getUvchinNer() != null && !form53.getUvchinNer().toString().isEmpty()) {
                    statement.setString(stIndex++, form53.getUvchinNer());
                }
                if (form53.getUvchinNer_En() != null && !form53.getUvchinNer_En().toString().isEmpty()) {
                    statement.setString(stIndex++, form53.getUvchinNer_En());
                }

            /* Zartsuulsan hemjee */
                if (form53.getBugdUvchilsen() != null && !form53.getBugdUvchilsen().toString().isEmpty() && form53.getBugdUvchilsen() != 0) {
                    statement.setLong(stIndex++, form53.getBugdUvchilsen());
                }
                if (form53.getBugdUhsen() != null && !form53.getBugdUhsen().toString().isEmpty() && form53.getBugdUhsen() != 0) {
                    statement.setLong(stIndex++, form53.getBugdUhsen());
                }

            /* MAL TURUL */
                if (form53.getBugdEdgersen() != null && !form53.getBugdEdgersen().toString().isEmpty() && form53.getBugdEdgersen() != 0) {
                    statement.setLong(stIndex++, form53.getBugdEdgersen());
                }
                if (form53.getBotgoUvchilsen() != null && !form53.getBotgoUvchilsen().toString().isEmpty() && form53.getBotgoUvchilsen() != 0) {
                    statement.setLong(stIndex++, form53.getBotgoUvchilsen());
                }

            /* Tuluvlugu */
                if (form53.getBotgoUhsen() != null && !form53.getBotgoUhsen().toString().isEmpty() && form53.getBotgoUhsen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getBotgoUhsen());
                }
                if (form53.getBotgoEdgersen() != null && !form53.getBotgoEdgersen().toString().isEmpty() && form53.getBotgoEdgersen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getBotgoEdgersen());
                }

            /* Guitsetgel */
                if (form53.getUnagaUvchilsen() != null && !form53.getUnagaUvchilsen().toString().isEmpty() && form53.getUnagaUvchilsen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getUnagaUvchilsen());
                }
                if (form53.getUnagaUhsen() != null && !form53.getUnagaUhsen().toString().isEmpty() && form53.getUnagaUhsen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getUnagaUhsen());
                }

                if (form53.getUnagaEdgersen() != null && !form53.getUnagaEdgersen().toString().isEmpty() && form53.getUnagaEdgersen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getUnagaEdgersen());
                }
                //////////////////////////////////////////////////
                if (form53.getTugalUvchilsen() != null && !form53.getTugalUvchilsen().toString().isEmpty() && form53.getTugalUvchilsen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getTugalUvchilsen());
                }
                if (form53.getTugalUhsen() != null && !form53.getTugalUhsen().toString().isEmpty() && form53.getTugalUhsen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getTugalUhsen());
                }

                if (form53.getTugalEdgersen() != null && !form53.getTugalEdgersen().toString().isEmpty() && form53.getTugalEdgersen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getTugalEdgersen());
                }
                if (form53.getHurgaUvchilsen() != null && !form53.getHurgaUvchilsen().toString().isEmpty() && form53.getHurgaUvchilsen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getHurgaUvchilsen());
                }
                if (form53.getHurgaUhsen() != null && !form53.getHurgaUhsen().toString().isEmpty() && form53.getHurgaUhsen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getHurgaUhsen());
                }

                if (form53.getHurgaEdgersen() != null && !form53.getHurgaEdgersen().toString().isEmpty() && form53.getHurgaEdgersen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getHurgaEdgersen());
                }
                if (form53.getIshigUvchilsen() != null && !form53.getIshigUvchilsen().toString().isEmpty() && form53.getIshigUvchilsen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getIshigUvchilsen());
                }
                if (form53.getIshigUhsen() != null && !form53.getIshigUhsen().toString().isEmpty() && form53.getIshigUhsen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getIshigUhsen());
                }

                if (form53.getIshigEdgersen() != null && !form53.getIshigEdgersen().toString().isEmpty() && form53.getIshigEdgersen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getIshigEdgersen());
                }
                if (form53.getBusadUvchilsen() != null && !form53.getBusadUvchilsen().toString().isEmpty() && form53.getBusadUvchilsen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getBusadUvchilsen());
                }
                if (form53.getBusadUhsen() != null && !form53.getBusadUhsen().toString().isEmpty() && form53.getBusadUhsen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getBusadUhsen());
                }

                if (form53.getBusadEdgersen() != null && !form53.getBusadEdgersen().toString().isEmpty() && form53.getBusadEdgersen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getBusadEdgersen());
                }
                ////////////////////////////////////////////////////

                if (form53 != null && form53.getAimag() != null && !form53.getAimag().isEmpty()) {
                    statement.setString(stIndex++, form53.getAimag());
                }

                if (form53 != null && form53.getAimag_en() != null && !form53.getAimag_en().isEmpty()) {
                    statement.setString(stIndex++, form53.getAimag_en());
                }

                if (form53 != null && form53.getSum() != null && !form53.getSum().isEmpty()) {
                    statement.setString(stIndex++, form53.getSum());
                }

                if (form53 != null && form53.getSum_en() != null && !form53.getSum_en().isEmpty()) {
                    statement.setString(stIndex++, form53.getSum_en());
                }

                if (form53 != null && form53.getRecordDate() != null && form53.getSearchRecordDate() != null) {
                    statement.setDate(stIndex++, form53.getRecordDate());
                    statement.setDate(stIndex++, form53.getSearchRecordDate());
                }
                if (form53 != null && form53.getCreby() != null && !form53.getCreby().isEmpty()) {
                    statement.setString(stIndex++, form53.getCreby());
                }

                ResultSet resultSet = statement.executeQuery();

                form53EntitieList = new ArrayList<>();

                IComboService comboService = new ComboServiceImpl(boneCP);
                List<BasicDBObject> uvchinNer = comboService.getComboVals("HGUVCH", "MN");
                int index = 1;
                while (resultSet.next()) {
                    Form53Entity entity = new Form53Entity();

                    entity.setId(resultSet.getLong("id"));
                    entity.setUvchinNer(resultSet.getString("uvchinNer"));
                    entity.setUvchinNer_En(resultSet.getString("uvchinNer_En"));
                    entity.setBugdUvchilsen(resultSet.getLong("bugdUvchilsen"));
                    entity.setBugdUhsen(resultSet.getLong("bugdUhsen"));
                    entity.setBugdEdgersen(resultSet.getLong("bugdEdgersen"));
                    entity.setBotgoUvchilsen(resultSet.getLong("botgoUvchilsen"));
                    entity.setBotgoUhsen(resultSet.getLong("botgoUhsen"));
                    entity.setBotgoEdgersen(resultSet.getLong("botgoEdgersen"));
                    entity.setUnagaUvchilsen(resultSet.getLong("unagaUvchilsen"));
                    entity.setUnagaUhsen(resultSet.getLong("unagaUhsen"));
                    entity.setUnagaEdgersen(resultSet.getLong("unagaEdgersen"));
                    entity.setTugalUvchilsen(resultSet.getLong("tugalUvchilsen"));
                    entity.setTugalUhsen(resultSet.getLong("tugalUhsen"));
                    entity.setTugalEdgersen(resultSet.getLong("tugalEdgersen"));
                    entity.setHurgaUvchilsen(resultSet.getLong("hurgaUvchilsen"));
                    entity.setHurgaUhsen(resultSet.getLong("hurgaUhsen"));
                    entity.setHurgaEdgersen(resultSet.getLong("hurgaEdgersen"));
                    entity.setIshigUvchilsen(resultSet.getLong("ishigUvchilsen"));
                    entity.setIshigUhsen(resultSet.getLong("ishigUhsen"));
                    entity.setIshigEdgersen(resultSet.getLong("ishigEdgersen"));
                    entity.setBusadUvchilsen(resultSet.getLong("busadUvchilsen"));
                    entity.setBusadUhsen(resultSet.getLong("busadUhsen"));
                    entity.setBusadEdgersen(resultSet.getLong("busadEdgersen"));
                    entity.setDelflg(resultSet.getString("del_flg"));
                    entity.setActflg(resultSet.getString("actflg"));
                    entity.setModAt(resultSet.getDate("mod_at"));
                    entity.setModby(resultSet.getString("mod_by"));
                    entity.setCreat(resultSet.getDate("cre_at"));
                    entity.setRecordDate(resultSet.getDate("recorddate"));
                    entity.setCreby(resultSet.getString("cre_by"));
                    entity.setSum(resultSet.getString("sum"));
                    entity.setSum_en(resultSet.getString("sum_en"));
                    entity.setAimag(resultSet.getString("aimag"));
                    entity.setAimag_en(resultSet.getString("aimag_en"));
                    entity.put("uvchinner", getTypeNer(uvchinNer, resultSet.getString("uvchinNer")));
                    entity.put("index",index++);
                    entity.put("strRecordDate", dateFormat.format(entity.getRecordDate()));

                    form53EntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return form53EntitieList;
    }

    /**
     * Get name
     *
     * @param cmbType
     * @param mal_turul_mn
     * @return
     */
    private String getTypeNer(List<BasicDBObject> cmbType, String mal_turul_mn) {
        for (BasicDBObject obj :
                cmbType) {
            if (obj.get("id") != null && mal_turul_mn != null && obj.getString("id").equalsIgnoreCase(mal_turul_mn)) {
                return obj.getString("name");
            }
        }

        return "";
    }

    @Override
    public ErrorEntity updateData(Form53Entity form53) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (form53 == null){
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        query = "UPDATE `hospital`.`hform53` " +
                "SET     ";

        if (form53.getSum() != null && !form53.getSum().isEmpty()) {
            query += " sum = ?, ";
        }

        if (form53.getSum_en() != null && !form53.getSum_en().isEmpty()) {
            query += " sum_en = ? , ";
        }

        if (form53.getUvchinNer() != null && !form53.getUvchinNer().toString().isEmpty()) {
            query += " uvchinNer = ? , ";
        }
        if (form53.getUvchinNer_En() != null && !form53.getUvchinNer_En().toString().isEmpty()) {
            query += " uvchinNer_En = ? , ";
        }
        /* Zartsuulsan hemjee */
        if (form53.getBugdUvchilsen() != null && !form53.getBugdUvchilsen().toString().isEmpty() && form53.getBugdUvchilsen() != 0) {
            query += " bugdUvchilsen = ? , ";
        }
        if (form53.getBugdUhsen() != null && !form53.getBugdUhsen().toString().isEmpty() && form53.getBugdUhsen() != 0) {
            query += " bugdUhsen = ? , ";
        }

        /* MAL TURUL */
        if (form53.getBugdEdgersen() != null && !form53.getBugdEdgersen().toString().isEmpty() && form53.getBugdEdgersen() != 0) {
            query += " bugdEdgersen = ? , ";
        }
        if (form53.getBotgoUvchilsen() != null && !form53.getBotgoUvchilsen().toString().isEmpty() && form53.getBotgoUvchilsen() != 0) {
            query += " botgoUvchilsen = ? , ";
        }

        /* Tuluvlugu */
        if (form53.getBotgoUhsen() != null && !form53.getBotgoUhsen().toString().isEmpty() && form53.getBotgoUhsen() != 0) {
            query += " botgoUhsen = ? , ";
        }
        if (form53.getBotgoEdgersen() != null && !form53.getBotgoEdgersen().toString().isEmpty() && form53.getBotgoEdgersen() != 0) {
            query += " botgoEdgersen = ? , ";
        }

        /* Guitsetgel */
        if (form53.getUnagaUvchilsen() != null && !form53.getUnagaUvchilsen().toString().isEmpty() && form53.getUnagaUvchilsen() != 0) {
            query += " unagaUvchilsen = ? , ";
        }
        if (form53.getUnagaUhsen() != null && !form53.getUnagaUhsen().toString().isEmpty() && form53.getUnagaUhsen() != 0) {
            query += " unagaUhsen = ? , ";
        }

        //ASD
        /* Zartsuulsan hemjee */
        if (form53.getUnagaEdgersen() != null && !form53.getUnagaEdgersen().toString().isEmpty() && form53.getUnagaEdgersen() != 0) {
            query += " unagaEdgersen = ? , ";
        }
        if (form53.getTugalUvchilsen() != null && !form53.getTugalUvchilsen().toString().isEmpty() && form53.getTugalUvchilsen() != 0) {
            query += " tugalUvchilsen = ? , ";
        }

        /* MAL TURUL */
        if (form53.getTugalUhsen() != null && !form53.getTugalUhsen().toString().isEmpty() && form53.getTugalUhsen() != 0) {
            query += " tugalUhsen = ? , ";
        }
        if (form53.getTugalEdgersen() != null && !form53.getTugalEdgersen().toString().isEmpty() && form53.getTugalEdgersen() != 0) {
            query += " tugalEdgersen = ? , ";
        }

        /* Tuluvlugu */
        if (form53.getHurgaUvchilsen() != null && !form53.getHurgaUvchilsen().toString().isEmpty() && form53.getHurgaUvchilsen() != 0) {
            query += " hurgaUvchilsen = ? , ";
        }
        if (form53.getHurgaUhsen() != null && !form53.getHurgaUhsen().toString().isEmpty() && form53.getHurgaUhsen() != 0) {
            query += " hurgaUhsen = ? , ";
        }

        /* Guitsetgel */
        if (form53.getHurgaEdgersen() != null && !form53.getHurgaEdgersen().toString().isEmpty() && form53.getHurgaEdgersen() != 0) {
            query += " hurgaEdgersen = ? , ";
        }
        if (form53.getIshigUvchilsen() != null && !form53.getIshigUvchilsen().toString().isEmpty() && form53.getIshigUvchilsen() != 0) {
            query += " ishigUvchilsen = ? , ";
        }

        if (form53.getIshigUhsen() != null && !form53.getIshigUhsen().toString().isEmpty() && form53.getIshigUhsen() != 0) {
            query += " ishigUhsen = ? , ";
        }
        if (form53.getIshigEdgersen() != null && !form53.getIshigEdgersen().toString().isEmpty() && form53.getIshigEdgersen() != 0) {
            query += " ishigEdgersen = ? , ";
        }


        if (form53.getBusadUvchilsen() != null && !form53.getBusadUvchilsen().toString().isEmpty() && form53.getBusadUvchilsen() != 0) {
            query += " busadUvchilsen = ? , ";
        }

        if (form53.getBusadUhsen() != null && !form53.getBusadUhsen().toString().isEmpty() && form53.getBusadUhsen() != 0) {
            query += " busadUhsen = ? , ";
        }
        if (form53.getBusadEdgersen() != null && !form53.getBusadEdgersen().toString().isEmpty() && form53.getBusadEdgersen() != 0) {
            query += " busadEdgersen = ? , ";
        }

        /* Delete flag */
        if (form53.getDelflg() != null && !form53.getDelflg().toString().isEmpty()) {
            query += " del_flg = ?, ";
        }

        /* Active flag */
        if (form53.getActflg() != null && !form53.getActflg().toString().isEmpty()) {
            query += " actflg = ?, ";
        }

        if (form53 != null && form53.getRecordDate() != null) {
            query += " `recorddate` = ?, ";
        }
        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";
        query += " WHERE  ID   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (form53.getUvchinNer() != null && !form53.getUvchinNer().toString().isEmpty()) {
                    statement.setString(stIndex++, form53.getUvchinNer());
                }
                if (form53.getUvchinNer_En() != null && !form53.getUvchinNer_En().toString().isEmpty()) {
                    statement.setString(stIndex++, form53.getUvchinNer_En());
                }

            /* Zartsuulsan hemjee */
                if (form53.getBugdUvchilsen() != null && !form53.getBugdUvchilsen().toString().isEmpty() && form53.getBugdUvchilsen() != 0) {
                    statement.setLong(stIndex++, form53.getBugdUvchilsen());
                }
                if (form53.getBugdUhsen() != null && !form53.getBugdUhsen().toString().isEmpty() && form53.getBugdUhsen() != 0) {
                    statement.setLong(stIndex++, form53.getBugdUhsen());
                }

            /* MAL TURUL */
                if (form53.getBugdEdgersen() != null && !form53.getBugdEdgersen().toString().isEmpty() && form53.getBugdEdgersen() != 0) {
                    statement.setLong(stIndex++, form53.getBugdEdgersen());
                }
                if (form53.getBotgoUvchilsen() != null && !form53.getBotgoUvchilsen().toString().isEmpty() && form53.getBotgoUvchilsen() != 0) {
                    statement.setLong(stIndex++, form53.getBotgoUvchilsen());
                }

            /* Tuluvlugu */
                if (form53.getBotgoUhsen() != null && !form53.getBotgoUhsen().toString().isEmpty() && form53.getBotgoUhsen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getBotgoUhsen());
                }
                if (form53.getBotgoEdgersen() != null && !form53.getBotgoEdgersen().toString().isEmpty() && form53.getBotgoEdgersen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getBotgoEdgersen());
                }

            /* Guitsetgel */
                if (form53.getUnagaUvchilsen() != null && !form53.getUnagaUvchilsen().toString().isEmpty() && form53.getUnagaUvchilsen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getUnagaUvchilsen());
                }
                if (form53.getUnagaUhsen() != null && !form53.getUnagaUhsen().toString().isEmpty() && form53.getUnagaUhsen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getUnagaUhsen());
                }

                if (form53.getUnagaEdgersen() != null && !form53.getUnagaEdgersen().toString().isEmpty() && form53.getUnagaEdgersen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getUnagaEdgersen());
                }
                //////////////////////////////////////////////////
                if (form53.getTugalUvchilsen() != null && !form53.getTugalUvchilsen().toString().isEmpty() && form53.getTugalUvchilsen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getTugalUvchilsen());
                }
                if (form53.getTugalUhsen() != null && !form53.getTugalUhsen().toString().isEmpty() && form53.getTugalUhsen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getTugalUhsen());
                }

                if (form53.getTugalEdgersen() != null && !form53.getTugalEdgersen().toString().isEmpty() && form53.getTugalEdgersen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getTugalEdgersen());
                }
                if (form53.getHurgaUvchilsen() != null && !form53.getHurgaUvchilsen().toString().isEmpty() && form53.getHurgaUvchilsen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getHurgaUvchilsen());
                }
                if (form53.getHurgaUhsen() != null && !form53.getHurgaUhsen().toString().isEmpty() && form53.getHurgaUhsen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getHurgaUhsen());
                }

                if (form53.getHurgaEdgersen() != null && !form53.getHurgaEdgersen().toString().isEmpty() && form53.getHurgaEdgersen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getHurgaEdgersen());
                }
                if (form53.getIshigUvchilsen() != null && !form53.getIshigUvchilsen().toString().isEmpty() && form53.getIshigUvchilsen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getIshigUvchilsen());
                }
                if (form53.getIshigUhsen() != null && !form53.getIshigUhsen().toString().isEmpty() && form53.getIshigUhsen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getIshigUhsen());
                }

                if (form53.getIshigEdgersen() != null && !form53.getIshigEdgersen().toString().isEmpty() && form53.getIshigEdgersen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getIshigEdgersen());
                }
                if (form53.getBusadUvchilsen() != null && !form53.getBusadUvchilsen().toString().isEmpty() && form53.getBusadUvchilsen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getBusadUvchilsen());
                }
                if (form53.getBusadUhsen() != null && !form53.getBusadUhsen().toString().isEmpty() && form53.getBusadUhsen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getBusadUhsen());
                }

                if (form53.getBusadEdgersen() != null && !form53.getBusadEdgersen().toString().isEmpty() && form53.getBusadEdgersen().longValue() != 0) {
                    statement.setLong(stIndex++, form53.getBusadEdgersen());
                }
                ////////////////////////////////////////////////////

                if (form53 != null && form53.getAimag() != null && !form53.getAimag().isEmpty()) {
                    statement.setString(stIndex++, form53.getAimag());
                }

                if (form53 != null && form53.getAimag_en() != null && !form53.getAimag_en().isEmpty()) {
                    statement.setString(stIndex++, form53.getAimag_en());
                }

                if (form53 != null && form53.getSum() != null && !form53.getSum().isEmpty()) {
                    statement.setString(stIndex++, form53.getSum());
                }

                if (form53 != null && form53.getSum_en() != null && !form53.getSum_en().isEmpty()) {
                    statement.setString(stIndex++, form53.getSum_en());
                }

                if (form53 != null && form53.getAimag() != null && !form53.getAimag().isEmpty()) {
                    statement.setString(stIndex++, form53.getAimag());
                }

                if (form53 != null && form53.getAimag_en() != null && !form53.getAimag_en().isEmpty()) {
                    statement.setString(stIndex++, form53.getAimag_en());
                }

            /* Delete flag */
                if (form53.getDelflg() != null && !form53.getDelflg().toString().isEmpty()) {
                    statement.setString(stIndex++, form53.getDelflg());
                }

            /* Active flag */
                if (form53.getActflg() != null && !form53.getActflg().toString().isEmpty()) {
                    statement.setString(stIndex++, form53.getActflg());
                }

                if (form53 != null && form53.getRecordDate() != null) {
                    statement.setDate(stIndex++, form53.getRecordDate());
                }

                statement.setDate(stIndex++, form53.getModAt());
                statement.setString(stIndex++, form53.getModby());
                statement.setLong(stIndex++, form53.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    errObj = new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                    errObj.setErrText("Бичлэг амжилттай засагдлаа.");
                    errObj.setErrDesc("Бичлэг амжилттай засагдлаа.");
                    return errObj;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1061));
        errObj.setErrText("Амжилтгүй боллоо. Дахин оролдоно уу.");
        errObj.setErrDesc("Амжилтгүй боллоо. Дахин оролдоно уу.");

        return errObj;
    }

    @Override
    public List<Form53Entity> getPieChartData(Form53Entity form53Entity) throws SQLException {
        int stIndex = 1;
        List<Form53Entity> form53EntitieList = null;

        String query_sel = "SELECT `hform53`.`id`,  " +
                "    `hform53`.`aimag`,  " +
                "    `hform53`.`sum`,  " +
                "    SUM(`hform53`.`zartsuulsan_hemjee_mn`) as zartsuulsan_hemjee,  " +
                "    c.cityname, c.cityname_en,  " +
                "    `h_sum`.sumname, `h_sum`.sumname_en  " +
                "FROM `hospital`.`hform53`  " +
                "INNER JOIN `h_city` AS c  " +
                "ON c.code = `hform53`.aimag  " +
                "INNER JOIN `h_sum`  " +
                "ON `h_sum`.sumcode = `hform53`.sum  " +
                "WHERE `hform53`.`del_flg` = 'N' and `hform53`.`actflg` = 'Y' and `hform53`.`aimag` = ?  " +
                "GROUP BY `hform53`.`sum` ";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                if (form53Entity.getAimag() != null && !form53Entity.getAimag().isEmpty()) {
                    statement.setString(stIndex++, form53Entity.getAimag());
                }

                ResultSet resultSet = statement.executeQuery();

                form53EntitieList = new ArrayList<>();

                while (resultSet.next()) {
//                    Form53Entity entity = new Form53Entity();
//
//                    entity.setId(resultSet.getLong("id"));
//                    entity.setZartsuulsanHemjee(resultSet.getString("zartsuulsan_hemjee"));
//                    entity.setZartsuulsanHemjee_en(resultSet.getString("zartsuulsan_hemjee"));
//                    entity.setAimag(resultSet.getString("aimag"));
//                    entity.setSum(resultSet.getString("sum"));
//                    entity.setAimagNer(resultSet.getString("cityname"));
//                    entity.setSumNer(resultSet.getString("sumname"));
//
//                    form53EntitieList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return form53EntitieList;
    }//


    @Override
    public ErrorEntity exportReport(String file, Form53Entity form53) throws SQLException {
        ErrorEntity errorEntity = null;
        String fileName = "hform53";
        try (Connection connection = boneCP.getConnection()) {
            String JRXML_PATH = Config.getJrxmlPath();
            String REPORT_PATH = Config.getReportPath();

            try {
                Map parameters = new HashMap();

                String reportTitle = "МАЛ АМЬТДЫН ХАЛДВАРГҮЙ ӨВЧНИЙ $$YEAR$$ ОНЫ ХАГАС ЖИЛИЙН, ЖИЛИЙН ЭЦСИЙН МЭДЭЭ",
                        currentYear = "$$YEAR$$ он №....",
                        printDate = "$$YEAR$$ оны $$MONTH$$ сарын $$DAY$$";

                Calendar now = Calendar.getInstance();

                int year = now.get(Calendar.YEAR);
                int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
                int day = now.get(Calendar.DAY_OF_MONTH);

                printDate = printDate.replace("$$YEAR$$", String.valueOf(year));
                printDate = printDate.replace("$$MONTH$$", String.valueOf(month));
                printDate = printDate.replace("$$DAY$$", String.valueOf(day));

                currentYear = currentYear.replace("$$YEAR$$", String.valueOf(year));

                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(new Date(form53.getRecordDate().getTime()));

                reportTitle = reportTitle.replace("$$YEAR$$", String.valueOf(calendar1.get(Calendar.YEAR)));

                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(new Date(form53.getSearchRecordDate().getTime()));

                if (calendar2.get(Calendar.MONTH) < 4) {
                    reportTitle = reportTitle.replace("$$SEASON$$", "1");
                } else if (calendar2.get(Calendar.MONTH) < 7) {
                    reportTitle = reportTitle.replace("$$SEASON$$", "2");
                } else if (calendar2.get(Calendar.MONTH) < 10) {
                    reportTitle = reportTitle.replace("$$SEASON$$", "3");
                } else if (calendar2.get(Calendar.MONTH) <= 12) {
                    reportTitle = reportTitle.replace("$$SEASON$$", "4");
                }

                parameters.put("start_date", form53.getRecordDate());
                parameters.put("end_date", form53.getSearchRecordDate());
                parameters.put("citycode", (form53.getAimag() != null && !form53.getAimag().isEmpty()) ? form53.getAimag() : null);
                parameters.put("sumcode", (form53.getSum() != null && !form53.getSum().isEmpty()) ? form53.getSum() : null);
                parameters.put("currentyear", currentYear);

                CityServiceImpl cityService = new CityServiceImpl(boneCP);
                City tmpCity = new City();
                tmpCity.setCode(form53.getAimag());
                List<City> clist = cityService.selectAll(tmpCity);

                if (clist != null && clist.size() == 1 ){
                    parameters.put("aimagner", clist.get(0).getCityname());
                }else{
                    parameters.put("aimagner", (form53.getAimagNer() == null) ? "" : form53.getAimagNer());
                }

                ISumService sumService = new SumServiceImpl(boneCP);
                Sum tmpSum = new Sum();
                tmpSum.setCitycode(form53.getAimag());
                tmpSum.setSumcode(form53.getSum());
                List<Sum> sumList = sumService.selectAll(tmpSum);

                if (sumList != null && sumList.size() == 1){
                    parameters.put("sumner", sumList.get(0).getSumname());
                }
                else{
                    parameters.put("sumner", (form53.getSumNer() == null) ? "" : form53.getSumNer());
                }

                parameters.put("printdate", printDate);
                parameters.put("reporttitle", reportTitle);
                parameters.put("creby", (form53.getCreby() != null && !form53.getCreby().isEmpty()) ? form53.getCreby() : null);

                PdfExcelUtil.generateReport(file, JRXML_PATH, REPORT_PATH, fileName, parameters, connection);


                System.out.println("File Generated");
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (connection != null) connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1001));
            if(file.equals("pdf")){
                errorEntity.setErrText(fileName + ".pdf");
            }else if(file.equals("xls")){
                errorEntity.setErrText(fileName + ".xls");
            }
        }
        return errorEntity;
    }

    @Override
    public BasicDBObject getColHighChartData(Form53Entity entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();
        List<String> categories = new ArrayList<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        //dateFormat.parse(dv).getTime()
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(entity.getRecordDate().getTime()));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(new Date(entity.getSearchRecordDate().getTime()));

        if (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {
            return null;
        }

        try {
            IComboService comboService = new ComboServiceImpl(boneCP);

            List<BasicDBObject> cmbType = new ArrayList<>();
            if (entity != null && !entity.getMalNer().isEmpty()) {
                cmbType = comboService.getComboNames("TYPEFORM52MAL", "MN");
            } else {
                cmbType = comboService.getComboNames("VALFORM53MAL", "MN");
            }

            //Saraar n haruulah
            int month = calendar1.get(Calendar.MONTH) + 1;
            int year1 = calendar1.get(Calendar.YEAR);
            int year2 = calendar2.get(Calendar.YEAR);

            try (Connection connection = boneCP.getConnection()) {
                String query_sel = "";
                if (entity != null && !entity.getMalNer().isEmpty()) {
                    if (entity.getMalNer().equals("unaga")) {
                        query_sel = "SELECT uvchinNer,  sum(`unagaUvchilsen`) AS uvchilsen,  sum(`unagaUhsen`) AS uhsen," +
                                "sum(`unagaEdgersen`) AS edgersen, aimag, sum FROM hospital.hform53  WHERE";
                    } else if (entity.getMalNer().equals("botgo")) {
                        query_sel = "SELECT uvchinNer,  sum(`botgoUvchilsen`) AS uvchilsen,  sum(`botgoUhsen`) AS uhsen," +
                                "sum(`botgoEdgersen`) AS edgersen, aimag, sum FROM hospital.hform53  WHERE";
                    } else if (entity.getMalNer().equals("tugal")) {
                        query_sel = "SELECT uvchinNer,  sum(`tugalUvchilsen`) AS uvchilsen,  sum(`tugalUhsen`) AS uhsen," +
                                "sum(`tugalEdgersen`) AS edgersen, aimag, sum FROM hospital.hform53  WHERE";
                    } else if (entity.getMalNer().equals("hurga")) {
                        query_sel = "SELECT uvchinNer,  sum(`hurgaUvchilsen`) AS uvchilsen,  sum(`hurgaUhsen`) AS uhsen," +
                                "sum(`hurgaEdgersen`) AS edgersen, aimag, sum FROM hospital.hform53  WHERE";
                    } else if (entity.getMalNer().equals("ishig")) {
                        query_sel = "SELECT uvchinNer,  sum(`ishigUvchilsen`) AS uvchilsen,  sum(`ishigUhsen`) AS uhsen," +
                                "sum(`ishigEdgersen`) AS edgersen, aimag, sum FROM hospital.hform53  WHERE";
                    } else if (entity.getMalNer().equals("busad")) {
                        query_sel = "SELECT uvchinNer,  sum(`busadUvchilsen`) AS uvchilsen,  sum(`busadUhsen`) AS uhsen," +
                                "sum(`busadEdgersen`) AS edgersen, aimag, sum FROM hospital.hform53  WHERE";
                    }
                }else{
                    query_sel = "SELECT `uvchinNer`, sum(`botgoUvchilsen`) AS botgoUvchilsen,  sum(`botgoUhsen`) AS botgoUhsen," +
                            "sum(`botgoEdgersen`) AS botgoEdgersen, " +
                            "sum(`unagaUvchilsen`) AS unagaUvchilsen,  sum(`unagaUhsen`) AS unagaUhsen," +
                            "sum(`unagaEdgersen`) AS unagaEdgersen," +
                            "sum(`tugalUvchilsen`) AS tugalUvchilsen,  sum(`tugalUhsen`) AS tugalUhsen," +
                            "sum(`tugalEdgersen`) AS tugalEdgersen," +
                            "sum(`hurgaUvchilsen`) AS hurgaUvchilsen,  sum(`hurgaEdgersen`) AS hurgaEdgersen," +
                            "sum(`hurgaUhsen`) AS hurgaUhsen," +
                            "sum(`ishigUvchilsen`) AS ishigUvchilsen,  sum(`ishigUhsen`) AS ishigUhsen," +
                            "sum(`ishigEdgersen`) AS ishigEdgersen," +
                            "sum(`busadUvchilsen`) AS busadUvchilsen,  sum(`busadUhsen`) AS busadUhsen," +
                            "sum(`busadEdgersen`) AS busadEdgersen,aimag, sum FROM hospital.hform53  WHERE";
                }

                if (entity != null && entity.getAimag()!= null && !entity.getAimag().isEmpty()) {
                    query_sel += " `hform53`.`aimag` = ? AND ";
                }
                if (entity != null && !entity.getSum().isEmpty() && entity.getSum() != null ) {
                    query_sel += " `hform53`.`sum` = ? AND ";
                }
                if (entity != null && !entity.getDelflg().isEmpty()) {
                    query_sel += " `hform53`.`del_flg` = ? AND ";
                }
                if (entity != null && !entity.getUvchinNer().isEmpty() && entity.getUvchinNer() != null ) {
                    query_sel += " `hform53`.`uvchinNer` = ? AND ";
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                    query_sel += " (`recorddate` BETWEEN ? AND ?)";
                }

                do {
                    //do process
                    categories.add(year1 + "-" + month);

                    try {
                        PreparedStatement statement = connection.prepareStatement(query_sel);

                        java.sql.Date stdate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-01 00:00:00").getTime());

                        java.sql.Date enddate = new java.sql.Date(dateFormat.parse(year1 + "-" + month + "-31 00:00:00").getTime());

                        if (month == 12 && year1 < year2) {
                            month = 1;
                            year1++;
                        } else {
                            month++;
                        }

                        int stindex = 1;

                        if (entity != null && entity.getAimag()!= null && !entity.getAimag().isEmpty()) {
                            statement.setString(stindex++, entity.getAimag());
                        }
                        if (entity != null && !entity.getDelflg().isEmpty()) {
                            statement.setString(stindex++, entity.getDelflg());
                        }
                        if (entity != null && !entity.getUvchinNer().isEmpty() && entity.getUvchinNer() != null ) {
                            statement.setString(stindex++, entity.getUvchinNer());
                        }
                        if (entity != null && !entity.getSum().isEmpty() && entity.getSum() != null ) {
                            statement.setString(stindex++, entity.getSum());
                        }
                        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                            statement.setDate(stindex++, stdate);
                            statement.setDate(stindex++, enddate);
                        }

                        ResultSet resultSet = statement.executeQuery();

                        HashMap<String, String> hashMap = new HashMap<>();

                        if (entity != null && !entity.getMalNer().isEmpty()) {
                            while (resultSet.next()) {
                                String uhsen = resultSet.getString("uhsen");
                                String uvchilsen = resultSet.getString("uvchilsen");
                                String edgersen = resultSet.getString("edgersen");
                                hashMap.put("uhsen", uhsen);
                                hashMap.put("uvchilsen", uvchilsen);
                                hashMap.put("edgersen", edgersen);

                            }//end while
                        } else {
                            while (resultSet.next()) {
                                String botgoUvchilsen = resultSet.getString("botgoUvchilsen");
                                String botgoUhsen = resultSet.getString("botgoUhsen");
                                String botgoEdgersen = resultSet.getString("botgoEdgersen");
                                String unagaUvchilsen = resultSet.getString("unagaUvchilsen");
                                String unagaUhsen = resultSet.getString("unagaUhsen");
                                String unagaEdgersen = resultSet.getString("unagaEdgersen");
                                String tugalUvchilsen = resultSet.getString("tugalUvchilsen");
                                String tugalUhsen = resultSet.getString("tugalUhsen");
                                String tugalEdgersen = resultSet.getString("tugalEdgersen");
                                String hurgaUvchilsen = resultSet.getString("hurgaUvchilsen");
                                String hurgaUhsen = resultSet.getString("hurgaUhsen");
                                String hurgaEdgersen = resultSet.getString("hurgaEdgersen");
                                String ishigUvchilsen = resultSet.getString("ishigUvchilsen");
                                String ishigUhsen = resultSet.getString("ishigUhsen");
                                String ishigEdgersen = resultSet.getString("ishigEdgersen");
                                String busadUvchilsen = resultSet.getString("busadUvchilsen");
                                String busadUhsen = resultSet.getString("busadUhsen");
                                String busadEdgersen = resultSet.getString("busadEdgersen");

                                hashMap.put("botgoUvchilsen", botgoUvchilsen);
                                hashMap.put("botgoUhsen", botgoUhsen);
                                hashMap.put("botgoEdgersen", botgoEdgersen);
                                hashMap.put("unagaUvchilsen", unagaUvchilsen);
                                hashMap.put("unagaUhsen", unagaUhsen);
                                hashMap.put("unagaEdgersen", unagaEdgersen);
                                hashMap.put("tugalUvchilsen", tugalUvchilsen);
                                hashMap.put("tugalUhsen", tugalUhsen);
                                hashMap.put("tugalEdgersen", tugalEdgersen);
                                hashMap.put("hurgaUvchilsen", hurgaUvchilsen);
                                hashMap.put("hurgaUhsen", hurgaUhsen);
                                hashMap.put("hurgaEdgersen", hurgaEdgersen);
                                hashMap.put("ishigUvchilsen", ishigUvchilsen);
                                hashMap.put("ishigUhsen", ishigUhsen);
                                hashMap.put("ishigEdgersen", ishigEdgersen);
                                hashMap.put("busadUvchilsen", busadUvchilsen);
                                hashMap.put("busadUhsen", busadUhsen);
                                hashMap.put("busadEdgersen", busadEdgersen);

                            }//end while
                        }
                        List<Double> list = null;

                        for (int i = 0; i < cmbType.size(); i++) {
                            BasicDBObject obj = cmbType.get(i);

                            if (obj.get("data") == null){
                                //new record
                                list = new ArrayList<>();
                                String val = hashMap.get(obj.getString("id"));
                                if (val == null) list.add(0.0);
                                else if (val.isEmpty()) list.add(0.0);
                                else list.add(Double.parseDouble(val));
                                obj.put("data", list);
                            }else{
                                //already exist
                                list = (List<Double>) obj.get("data");
                                String val = hashMap.get(obj.getString("id"));
                                if (val == null) list.add(0.0);
                                else if (val.isEmpty()) list.add(0.0);
                                else list.add(Double.parseDouble(val));
                            }

                        }

                        if (statement != null) statement.close();
                        if (resultSet != null) resultSet.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();

                        break;
                    }

                    ///
                } while ((month <= calendar2.get(Calendar.MONTH) + 1 && year1 <= year2) || year1 < year2);

                basicDBObject.put("categories", categories);
                basicDBObject.put("series", cmbType);

                connection.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }

    @Override
    public BasicDBObject getPieHighChartData(Form53Entity entity) throws SQLException {
        BasicDBObject basicDBObject = new BasicDBObject();

        String query_sel = "";

        if (entity != null && !entity.getMalNer().isEmpty()) {
            if (entity.getMalNer().equals("unaga")) {
                query_sel = "SELECT uvchinNer,  sum(`unagaUvchilsen`) AS uvchilsen,  sum(`unagaUhsen`) AS uhsen," +
                        "sum(`unagaEdgersen`) AS edgersen, aimag, sum FROM hospital.hform53  WHERE";
            } else if (entity.getMalNer().equals("botgo")) {
                query_sel = "SELECT uvchinNer,  sum(`botgoUvchilsen`) AS uvchilsen,  sum(`botgoUhsen`) AS uhsen," +
                        "sum(`botgoEdgersen`) AS edgersen, aimag, sum FROM hospital.hform53  WHERE";
            } else if (entity.getMalNer().equals("tugal")) {
                query_sel = "SELECT uvchinNer,  sum(`tugalUvchilsen`) AS uvchilsen,  sum(`tugalUhsen`) AS uhsen," +
                        "sum(`tugalEdgersen`) AS edgersen, aimag, sum FROM hospital.hform53  WHERE";
            } else if (entity.getMalNer().equals("hurga")) {
                query_sel = "SELECT uvchinNer,  sum(`hurgaUvchilsen`) AS uvchilsen,  sum(`hurgaUhsen`) AS uhsen," +
                        "sum(`hurgaEdgersen`) AS edgersen, aimag, sum FROM hospital.hform53  WHERE";
            } else if (entity.getMalNer().equals("ishig")) {
                query_sel = "SELECT uvchinNer,  sum(`ishigUvchilsen`) AS uvchilsen,  sum(`ishigUhsen`) AS uhsen," +
                        "sum(`ishigEdgersen`) AS edgersen, aimag, sum FROM hospital.hform53  WHERE";
            } else if (entity.getMalNer().equals("busad")) {
                query_sel = "SELECT uvchinNer,  sum(`busadUvchilsen`) AS uvchilsen,  sum(`busadUhsen`) AS uhsen," +
                        "sum(`busadEdgersen`) AS edgersen, aimag, sum FROM hospital.hform53  WHERE";
            }
        }else{
            query_sel = "SELECT `uvchinNer`, sum(`botgoUvchilsen`) AS botgoUvchilsen,  sum(`botgoUhsen`) AS botgoUhsen," +
                    "sum(`botgoEdgersen`) AS botgoEdgersen, " +
                    "sum(`unagaUvchilsen`) AS unagaUvchilsen,  sum(`unagaUhsen`) AS unagaUhsen," +
                    "sum(`unagaEdgersen`) AS unagaEdgersen," +
                    "sum(`tugalUvchilsen`) AS tugalUvchilsen,  sum(`tugalUhsen`) AS tugalUhsen," +
                    "sum(`tugalEdgersen`) AS tugalEdgersen," +
                    "sum(`hurgaUvchilsen`) AS hurgaUvchilsen,  sum(`hurgaEdgersen`) AS hurgaEdgersen," +
                    "sum(`hurgaUhsen`) AS hurgaUhsen," +
                    "sum(`ishigUvchilsen`) AS ishigUvchilsen,  sum(`ishigUhsen`) AS ishigUhsen," +
                    "sum(`ishigEdgersen`) AS ishigEdgersen," +
                    "sum(`busadUvchilsen`) AS busadUvchilsen,  sum(`busadUhsen`) AS busadUhsen," +
                    "sum(`busadEdgersen`) AS busadEdgersen,aimag, sum FROM hospital.hform53  WHERE";
        }

        if (entity != null && entity.getAimag()!= null && !entity.getAimag().isEmpty()) {
            query_sel += " `hform53`.`aimag` = ? AND ";
        }
        if (entity != null && !entity.getSum().isEmpty() && entity.getSum() != null ) {
            query_sel += " `hform53`.`sum` = ? AND ";
        }
        if (entity != null && !entity.getDelflg().isEmpty()) {
            query_sel += " `hform53`.`del_flg` = ? AND ";
        }
        if (entity != null && !entity.getUvchinNer().isEmpty() && entity.getUvchinNer() != null ) {
            query_sel += " `hform53`.`uvchinNer` = ? AND ";
        }
        if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
            query_sel += " (`recorddate` BETWEEN ? AND ?)";
        }

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                int stindex = 1;

                if (entity != null && entity.getAimag()!= null && !entity.getAimag().isEmpty()) {
                    statement.setString(stindex++, entity.getAimag());
                }
                if (entity != null && !entity.getDelflg().isEmpty()) {
                    statement.setString(stindex++, entity.getDelflg());
                }
                if (entity != null && !entity.getUvchinNer().isEmpty() && entity.getUvchinNer() != null ) {
                    statement.setString(stindex++, entity.getUvchinNer());
                }
                if (entity != null && !entity.getSum().isEmpty() && entity.getSum() != null ) {
                    statement.setString(stindex++, entity.getSum());
                }
                if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                    statement.setDate(stindex++, entity.getRecordDate());
                    statement.setDate(stindex++, entity.getSearchRecordDate());
                }

                System.out.println(statement);
                ResultSet resultSet = statement.executeQuery();

                IComboService comboService = new ComboServiceImpl(boneCP);
                List<BasicDBObject> list = new ArrayList<>();

                while (resultSet.next()) {

                    if (entity != null && !entity.getMalNer().isEmpty()) {
                        BasicDBObject entity1 = new BasicDBObject();
                        BasicDBObject entity2 = new BasicDBObject();
                        BasicDBObject entity3 = new BasicDBObject();

                        entity1.put("name", "Өвчилсэн");
                        entity1.put("y", resultSet.getDouble("uvchilsen"));
                        entity2.put("name", "Үхсэн");
                        entity2.put("y", resultSet.getDouble("uhsen"));
                        entity3.put("name", "Эдгэрсэн");
                        entity3.put("y", resultSet.getDouble("edgersen"));
                        list.add(entity1);
                        list.add(entity2);
                        list.add(entity3);
                    }else {
                        BasicDBObject entity1 = new BasicDBObject();
                        BasicDBObject entity2 = new BasicDBObject();
                        BasicDBObject entity3 = new BasicDBObject();
                        BasicDBObject entity4 = new BasicDBObject();
                        BasicDBObject entity5 = new BasicDBObject();
                        BasicDBObject entity6 = new BasicDBObject();
                        BasicDBObject entity7 = new BasicDBObject();
                        BasicDBObject entity8 = new BasicDBObject();
                        BasicDBObject entity9 = new BasicDBObject();
                        BasicDBObject entity10 = new BasicDBObject();
                        BasicDBObject entity11 = new BasicDBObject();
                        BasicDBObject entity12 = new BasicDBObject();
                        BasicDBObject entity13 = new BasicDBObject();
                        BasicDBObject entity14 = new BasicDBObject();
                        BasicDBObject entity15 = new BasicDBObject();
                        BasicDBObject entity16 = new BasicDBObject();
                        BasicDBObject entity17 = new BasicDBObject();
                        BasicDBObject entity18 = new BasicDBObject();

                        entity1.put("name", "Өвчилсэн ботго");
                        entity1.put("y", resultSet.getDouble("botgoUvchilsen"));
                        entity2.put("name", "Үхсэн ботго");
                        entity2.put("y", resultSet.getDouble("botgoUhsen"));
                        entity3.put("name", "Эдгэрсэн ботго");
                        entity3.put("y", resultSet.getDouble("botgoEdgersen"));
                        entity4.put("name", "Өвчилсөн унага");
                        entity4.put("y", resultSet.getDouble("unagaUvchilsen"));
                        entity5.put("name", "Үхсэн унага");
                        entity5.put("y", resultSet.getDouble("unagaUhsen"));
                        entity6.put("name", "Эдгэрсэн унага");
                        entity6.put("y", resultSet.getDouble("unagaEdgersen"));
                        entity7.put("name", "Өвчилсэн үхэр");
                        entity7.put("y", resultSet.getDouble("tugalUvchilsen"));
                        entity8.put("name", "Үхсэн үхэр");
                        entity8.put("y", resultSet.getDouble("tugalUhsen"));
                        entity9.put("name", "Эдгэрсэн үхэр");
                        entity9.put("y", resultSet.getDouble("tugalEdgersen"));
                        entity10.put("name", "Өвчилсөн хонь");
                        entity10.put("y", resultSet.getDouble("hurgaUvchilsen"));
                        entity11.put("name", "Үхсэн хонь");
                        entity11.put("y", resultSet.getDouble("hurgaUhsen"));
                        entity12.put("name", "Эдгэрсэн хонь");
                        entity12.put("y", resultSet.getDouble("hurgaEdgersen"));
                        entity13.put("name", "Өвчилсөн ямаа");
                        entity13.put("y", resultSet.getDouble("ishigUvchilsen"));
                        entity14.put("name", "Үхсэн ямаа");
                        entity14.put("y", resultSet.getDouble("ishigUhsen"));
                        entity15.put("name", "Эдгэрсэн ямаа");
                        entity15.put("y", resultSet.getDouble("ishigEdgersen"));
                        entity16.put("name", "Бусад өвчилсөн");
                        entity16.put("y", resultSet.getDouble("busadUvchilsen"));
                        entity17.put("name", "Бусад үхсэн");
                        entity17.put("y", resultSet.getDouble("busadUhsen"));
                        entity18.put("name", "Бусад эдгэрсэн");
                        entity18.put("y", resultSet.getDouble("busadEdgersen"));
                        list.add(entity1);
                        list.add(entity2);
                        list.add(entity3);
                        list.add(entity4);
                        list.add(entity5);
                        list.add(entity6);
                        list.add(entity7);
                        list.add(entity8);
                        list.add(entity9);
                        list.add(entity10);
                        list.add(entity11);
                        list.add(entity12);
                        list.add(entity13);
                        list.add(entity14);
                        list.add(entity15);
                        list.add(entity16);
                        list.add(entity17);
                        list.add(entity18);
                    }

                }

                basicDBObject.put("data", list);

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return basicDBObject;
    }
}
