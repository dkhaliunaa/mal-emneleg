package com.hospital.app;


import com.Config;
import com.enumclass.ErrorType;
import com.jolbox.bonecp.BoneCP;
import com.model.hos.ErrorEntity;
import com.model.hos.FileEntity;
import com.mongodb.BasicDBObject;
import net.sf.jasperreports.engine.JRStyle;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.design.JRDesignStyle;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by 24be10264 on 9/2/2017.
 */
public class UploadFileServiceImpl implements IUploadFileService{
    /**
     * Objects
     */

    private BoneCP boneCP;
    private ErrorEntity errObj;

    public UploadFileServiceImpl(BoneCP boneCP) {
        this.boneCP = boneCP;
    }

    @Override
    public ErrorEntity insertNewData(FileEntity entity) throws SQLException {
        PreparedStatement statement = null;

        if (entity == null){
            errObj = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errObj.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errObj.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errObj;
        }

        String query_login = "INSERT INTO `h_file`(`fyear`, `fmonth`, `fdesc`, `fname`, `fsize`, `cre_by`, `cre_at`) " +
                "VALUES (?,?,?,?,?,?, ?)";

        try (Connection connection = boneCP.getConnection()) {
            try{
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, entity.getYear());
                statement.setString(2, entity.getMonth());
                statement.setString(3, entity.getFdesc());
                statement.setString(4, entity.getFname());
                statement.setString(5, entity.getSize());
                statement.setString(6, entity.getCre_by());
                statement.setDate(7, entity.getCre_at());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1){
                    connection.commit();
                    ErrorEntity errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));
                    errorEntity.setErrText("Файлын мэдээлэл амжилттай хадгалагдлаа.");
                    return errorEntity;
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }finally {
                if (statement != null)statement.close();
                connection.close();
            }

        }catch (Exception ex){

        }
        ErrorEntity errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1057));
        errorEntity.setErrText("Амжилтгүй боллоо. Дахин оролдоно уу.");
        return errorEntity;
    }

    @Override
    public List<FileEntity> getListData(FileEntity entity) throws SQLException {
        int stIndex = 1;
        List<FileEntity> entityList = null;

        String query = "SELECT `id`, `fyear`, `fmonth`, `fdesc`, `fname`, `fsize`, `delflg`, `actflg`, " +
                "`cre_by`, `cre_at`, `mod_by`, `mod_at` " +
                "FROM `h_file` WHERE ";

        if (entity != null && entity.getCre_by() != null && !entity.getCre_by().isEmpty()){
            query += " `h_file`.`cre_by` = ? AND ";
        }

        query += " `h_file`.`delflg` = 'N' ORDER BY `cre_at` DESC ";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query);

                if (entity != null && entity.getCre_by() != null && !entity.getCre_by().isEmpty()){
                    statement.setString(stIndex++, entity.getCre_by());
                }

                ResultSet resultSet = statement.executeQuery();

                entityList = new ArrayList<>();

                IComboService comboService = new ComboServiceImpl(boneCP);
                List<BasicDBObject> cmbType = comboService.getComboVals("MAL", "MN");

                int index = 1;
                while (resultSet.next()) {
                    FileEntity newEntity = new FileEntity();

                    newEntity.setId(resultSet.getLong("id"));
                    newEntity.setYear(resultSet.getString("fyear"));
                    newEntity.setMonth(resultSet.getString("fmonth"));
                    newEntity.setFdesc(resultSet.getString("fdesc"));
                    newEntity.setFname(resultSet.getString("fname"));
                    newEntity.setSize(resultSet.getString("fsize"));
                    newEntity.setDelflg(resultSet.getString("delflg"));
                    newEntity.setActflg(resultSet.getString("actflg"));
                    newEntity.setCre_by(resultSet.getString("cre_by"));
                    newEntity.setCre_at(resultSet.getDate("cre_at"));
                    newEntity.setMod_by(resultSet.getString("mod_by"));
                    newEntity.setMod_at(resultSet.getDate("mod_at"));
                    newEntity.put("index", index ++);

                    entityList.add(newEntity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return entityList;
    }


    @Override
    public ErrorEntity updateData(FileEntity entity) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (entity == null) new ErrorEntity(ErrorType.INFO, Long.valueOf(1059));

        query = "UPDATE `hospital`.`h_file` " +
                "SET     ";

        if (entity != null && entity.getYear() != null && !entity.getYear().isEmpty()){
            query += " `fyear` = ?, ";
        }
        if (entity != null && entity.getMonth() != null && !entity.getMonth().isEmpty()){
            query += " `fmonth` = ?, ";
        }
        if (entity != null && entity.getFdesc() != null && !entity.getFdesc().isEmpty()){
            query += " `fdesc` = ?, ";
        }
        if (entity != null && entity.getFname() != null && !entity.getFname().isEmpty()){
            query += " `fname` = ?, ";
        }
        if (entity != null && entity.getSize() != null && !entity.getSize().isEmpty()){
            query += " `fsize` = ?, ";
        }
        if (entity != null && entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
            query += " `delflg` = ?, ";
        }
        if (entity != null && entity.getActflg() != null && !entity.getActflg().isEmpty()){
            query += " `actflg` = ?, ";
        }

        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";
        query += " WHERE  ID   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (entity != null && entity.getYear() != null && !entity.getYear().isEmpty()){
                    statement.setString(stIndex++, entity.getYear());
                }
                if (entity != null && entity.getMonth() != null && !entity.getMonth().isEmpty()){
                    statement.setString(stIndex++, entity.getMonth());
                }
                if (entity != null && entity.getFdesc() != null && !entity.getFdesc().isEmpty()){
                    statement.setString(stIndex++, entity.getFdesc());
                }
                if (entity != null && entity.getFname() != null && !entity.getFname().isEmpty()){
                    statement.setString(stIndex++, entity.getFname());
                }
                if (entity != null && entity.getSize() != null && !entity.getSize().isEmpty()){
                    statement.setString(stIndex++, entity.getSize());
                }
                if (entity != null && entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
                    statement.setString(stIndex++, entity.getDelflg());
                }
                if (entity != null && entity.getActflg() != null && !entity.getActflg().isEmpty()){
                    statement.setString(stIndex++, entity.getActflg());
                }

                statement.setDate(stIndex++, entity.getMod_at());
                statement.setString(stIndex++, entity.getMod_by());
                statement.setLong(stIndex++, entity.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    return new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }


        return new ErrorEntity(ErrorType.ERROR, Long.valueOf(1061));
    }

    public Integer getFileCount()throws SQLException{
        int fileCnt = 0;
        String query = "SELECT count(*) as filecnt FROM `h_file` WHERE delflg = 'N'";

        try (Connection connection = boneCP.getConnection()) {
            int stindex = 1;

            try {
                connection.setAutoCommit(false);
                PreparedStatement statement = connection.prepareStatement(query);

                ResultSet resultSet = statement.executeQuery();

                while (resultSet.next()) {
                    fileCnt = resultSet.getInt("filecnt");
                }


                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (connection != null){
                    connection.close();
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return fileCnt;
    }
}
