package com.hospital.app;


import com.enumclass.ErrorType;
import com.jolbox.bonecp.BoneCP;
import com.model.hos.ErrorEntity;
import com.model.hos.MedremtgiiDataEntity;
import com.mongodb.BasicDBObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tilyeujan on 9/2/2017.
 */
public class MedremtgiiDataFormServiceImpl implements IMedremtgiiDataFormService {
    /**
     * Objects
     */
    private BoneCP boneCP;
    private ErrorEntity errorEntity;


    public MedremtgiiDataFormServiceImpl(BoneCP boneCP) {
        this.boneCP = boneCP;
    }

    @Override
    public ErrorEntity insertNewData(MedremtgiiDataEntity entity) throws SQLException {
        PreparedStatement statement = null;

        if (entity == null) {
            errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));

            errorEntity.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errorEntity.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            
            return errorEntity;
        }

        String query_login = "INSERT INTO `hform01_medremtgii_data`(`sureg_key`, `mtype`, `m_age`, `age_sex`, " +
                "`uher`, `honi`, `ymaa`, `temee`, `gahai`, `busad`, `zerleg_amitad`, `delflg`, `actflg`, `cre_at`, " +
                "`cre_by`, `mod_at`, `mod_by`, `record_date`, `rec_type`) " +
                "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        try (Connection connection = boneCP.getConnection()) {
            try{
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, (entity.get(entity.SUREG_KEY) != null) ? entity.getSureg_key().trim() : "");
                statement.setString(2, (entity.get(entity.MTYPE) != null) ? entity.getMtype().trim() : "");
                statement.setString(3, (entity.get(entity.M_AGE) != null) ? entity.getM_age().trim() : "");
                statement.setString(4, (entity.get(entity.AGE_SEX) != null) ? entity.getAge_sex().trim() : "");
                statement.setInt(5, (entity.get(entity.UHER) != null) ? Integer.parseInt(entity.getUher()) : 0);
                statement.setInt(6, (entity.get(entity.HONI) != null) ? Integer.parseInt(entity.getHoni().trim()) : 0);
                statement.setInt(7, (entity.get(entity.YMAA) != null) ? Integer.parseInt(entity.getYmaa().trim()) : 0);
                statement.setInt(8, (entity.get(entity.TEMEE) != null) ? Integer.parseInt(entity.getTemee().trim()) : 0);
                statement.setInt(9, (entity.get(entity.GAHAI) != null) ? Integer.parseInt(entity.getGahai().trim()) : 0);
                statement.setInt(10, (entity.get(entity.BUSAD) != null) ? Integer.parseInt(entity.getBusad().trim()) : 0);
                statement.setInt(11, (entity.get(entity.ZERLEG_AMITAD) != null) ? Integer.parseInt(entity.getZerleg_amitad().trim()) : 0);
                statement.setString(12, (entity.get(entity.DELFLG) != null) ? entity.getDelflg().trim() : "");
                statement.setString(13, (entity.get(entity.ACTFLG) != null) ? entity.getActflg().trim() : "");
                statement.setDate(14, entity.getCre_at());
                statement.setString(15, (entity.get(entity.CRE_BY) != null) ? entity.getCre_by().trim() : "");
                statement.setDate(16, entity.getMod_at());
                statement.setString(17, (entity.get(entity.MOD_BY) != null) ? entity.getMod_by().trim() : "");
                statement.setDate(18, entity.getRecord_date());
                statement.setString(19, (entity.get(entity.REC_TYPE) != null) ? entity.getRec_type().trim() : "");

                int insertCount = statement.executeUpdate();

                if (insertCount == 1){
                    connection.commit();

                    errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));

                    errorEntity.setErrText("Бичлэг амжилттай хадгалагдлаа.");
                    errorEntity.setErrDesc("Бичлэг амжилттай хадгалагдлаа.");

                    return errorEntity;
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }finally {
                if (statement != null)statement.close();
                connection.close();
            }

        }catch (Exception ex){

        }

        errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1057));
        errorEntity.setErrText("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");
        errorEntity.setErrDesc("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");

        return errorEntity;
    }

    @Override
    public List<MedremtgiiDataEntity> getListData(MedremtgiiDataEntity entity) throws SQLException {
        int stIndex = 1;
        int index = 1;
        List<MedremtgiiDataEntity> entityList = null;

        String query = "SELECT `hform01_medremtgii_data`.`id`," +
                "    `hform01_medremtgii_data`.`sureg_key`," +
                "    `hform01_medremtgii_data`.`mtype`," +
                "    `hform01_medremtgii_data`.`m_age`," +
                "    `hform01_medremtgii_data`.`age_sex`," +
                "    `hform01_medremtgii_data`.`uher`," +
                "    `hform01_medremtgii_data`.`honi`," +
                "    `hform01_medremtgii_data`.`ymaa`," +
                "    `hform01_medremtgii_data`.`temee`," +
                "    `hform01_medremtgii_data`.`gahai`," +
                "    `hform01_medremtgii_data`.`busad`," +
                "    `hform01_medremtgii_data`.`zerleg_amitad`," +
                "    `hform01_medremtgii_data`.`delflg`," +
                "    `hform01_medremtgii_data`.`actflg`," +
                "    `hform01_medremtgii_data`.`cre_at`," +
                "    `hform01_medremtgii_data`.`cre_by`," +
                "    `hform01_medremtgii_data`.`mod_at`," +
                "    `hform01_medremtgii_data`.`mod_by`," +
                "    `hform01_medremtgii_data`.`record_date`," +
                "    `hform01_medremtgii_data`.`rec_type` " +
                "FROM `hospital`.`hform01_medremtgii_data` " +
                "WHERE ";

        if (entity.getSureg_key() != null && !entity.getSureg_key().isEmpty()){
            query += " `hform01_medremtgii_data`.`sureg_key` = ? AND ";
        }
        if (entity.getMtype() != null && !entity.getMtype().isEmpty()){
            query += " `hform01_medremtgii_data`.`mtype` = ? AND ";
        }
        if (entity.getM_age() != null && !entity.getM_age().isEmpty()){
            query += " `hform01_medremtgii_data`.`m_age` = ? AND ";
        }
        if (entity.getAge_sex() != null && !entity.getAge_sex().isEmpty()){
            query += " `hform01_medremtgii_data`.`age_sex` = ? AND ";
        }
        if (entity != null && entity.getRec_type() != null && !entity.getRec_type().isEmpty()){
            query += " `hform01_medremtgii_data`.rec_type = ? AND ";
        }

        if (entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
            query += " `hform01_medremtgii_data`.delflg = ? AND ";
        }
        if (entity.getActflg() != null && !entity.getActflg().isEmpty()){
            query += " `hform01_medremtgii_data`.actflg = ? AND ";
        }
        if (entity != null && entity.getCre_by() != null && !entity.getCre_by().isEmpty()){
            query += " `hform01_medremtgii_data`.cre_by = ? AND ";
        }

        query += " `hform01_medremtgii_data`.`id` > 0 ";
        query += " ORDER BY `hform01_medremtgii_data`.`cre_at` DESC ";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query);

                if (entity.getSureg_key() != null && !entity.getSureg_key().isEmpty()){
                    statement.setString(stIndex++, entity.getSureg_key());
                }
                if (entity.getMtype() != null && !entity.getMtype().isEmpty()){
                    statement.setString(stIndex++, entity.getMtype());
                }
                if (entity.getM_age() != null && !entity.getM_age().isEmpty()){
                    statement.setString(stIndex++, entity.getM_age());
                }
                if (entity.getAge_sex() != null && !entity.getAge_sex().isEmpty()){
                    statement.setString(stIndex++, entity.getAge_sex());
                }
                if (entity != null && entity.getRec_type() != null && !entity.getRec_type().isEmpty()){
                    statement.setString(stIndex++, entity.getRec_type());
                }

                if (entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
                    statement.setString(stIndex++, entity.getDelflg());
                }
                if (entity.getActflg() != null && !entity.getActflg().isEmpty()){
                    statement.setString(stIndex++, entity.getActflg());
                }
                if (entity != null && entity.getCre_by() != null && !entity.getCre_by().isEmpty()){
                    statement.setString(stIndex++, entity.getCre_by());
                }


                ResultSet resultSet = statement.executeQuery();

                entityList = new ArrayList<>();

                while (resultSet.next()) {
                    MedremtgiiDataEntity objEntity = new MedremtgiiDataEntity();

                    objEntity.setId(resultSet.getLong("id"));
                    objEntity.setSureg_key(resultSet.getString("sureg_key"));
                    objEntity.setMtype(resultSet.getString("mtype"));
                    objEntity.setM_age(resultSet.getString("m_age"));
                    objEntity.setAge_sex(resultSet.getString("age_sex"));
                    objEntity.setUher(resultSet.getString("uher"));
                    objEntity.setHoni(resultSet.getString("honi"));
                    objEntity.setYmaa(resultSet.getString("ymaa"));
                    objEntity.setTemee(resultSet.getString("temee"));
                    objEntity.setGahai(resultSet.getString("gahai"));
                    objEntity.setBusad(resultSet.getString("busad"));
                    objEntity.setZerleg_amitad(resultSet.getString("zerleg_amitad"));
                    objEntity.setRec_type(resultSet.getString("rec_type"));
                    objEntity.setDelflg(resultSet.getString("delflg"));
                    objEntity.setCre_at(resultSet.getDate("cre_at"));
                    objEntity.setCre_by(resultSet.getString("cre_by"));
                    objEntity.setMod_at(resultSet.getDate("mod_at"));
                    objEntity.setMod_by(resultSet.getString("mod_by"));
                    objEntity.setRecord_date(resultSet.getDate("record_date"));
                    objEntity.setActflg(resultSet.getString("actflg"));

                    objEntity.put("_index", index++);
                    objEntity.put("strRecordOgnoo", dateFormat.format(objEntity.getRecord_date()));

                    entityList.add(objEntity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return entityList;
    }

    /**
     * Get name
     *
     * @param cmbType
     * @param mal_turul_mn
     * @return
     */
    private String getTypeNer(List<BasicDBObject> cmbType, String mal_turul_mn) {
        for (BasicDBObject obj :
                cmbType) {
            if (obj.get("id") != null && mal_turul_mn != null && obj.getString("id").equalsIgnoreCase(mal_turul_mn)) {
                return obj.getString("name");
            }
        }

        return "";
    }

    @Override
    public ErrorEntity updateData(MedremtgiiDataEntity entity) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (entity == null) new ErrorEntity(ErrorType.INFO, Long.valueOf(1059));
        if (entity == null) {
            errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errorEntity.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errorEntity.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errorEntity;
        }

        query = "UPDATE `hospital`.`hform01_medremtgii_data` " +
                "SET     ";

        if (entity != null && !entity.getMtype().isEmpty())
            query += "`mtype`=?,";
        if (entity != null && !entity.getM_age().isEmpty())
            query += "`m_age`=?,";
        if (entity != null && !entity.getAge_sex().isEmpty())
            query +="`age_sex`=?,";
        if (entity != null && !entity.getUher().isEmpty())
            query +="`uher`=?,";
        if (entity != null && !entity.getHoni().isEmpty())
            query +="`honi`=?,";
        if (entity != null && !entity.getYmaa().isEmpty())
            query += "`ymaa`=?,";
        if (entity != null && !entity.getTemee().isEmpty())
            query +="`temee`=?,";
        if (entity != null && !entity.getGahai().isEmpty())
            query +="`gahai`=?,";
        if (entity != null && !entity.getBusad().isEmpty())
            query +="`busad`=?,";
        if (entity != null && !entity.getZerleg_amitad().isEmpty())
            query +="`zerleg_amitad`=?,";
        if (entity != null && !entity.getDelflg().isEmpty())
            query +="`delflg`=?,";
        if (entity != null && !entity.getActflg().isEmpty())
            query +="`actflg`=?,";
        if (entity != null && entity.getRecord_date()!= null)
            query +="`record_date`=?,";


        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";
        query += " WHERE  ID   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (entity != null && !entity.getMtype().isEmpty())
                    statement.setString(stIndex++, entity.getMtype());

                if (entity != null && !entity.getM_age().isEmpty())
                    statement.setString(stIndex++, entity.getM_age());

                if (entity != null && !entity.getAge_sex().isEmpty())
                    statement.setString(stIndex++, entity.getAge_sex());

                if (entity != null && !entity.getUher().isEmpty())
                    statement.setString(stIndex++, entity.getUher());

                if (entity != null && !entity.getHoni().isEmpty())
                    statement.setString(stIndex++, entity.getHoni());

                if (entity != null && !entity.getYmaa().isEmpty())
                    statement.setString(stIndex++, entity.getYmaa());

                if (entity != null && !entity.getTemee().isEmpty())
                    statement.setString(stIndex++, entity.getTemee());

                if (entity != null && !entity.getGahai().isEmpty())
                    statement.setString(stIndex++, entity.getGahai());

                if (entity != null && !entity.getBusad().isEmpty())
                    statement.setString(stIndex++, entity.getBusad());

                if (entity != null && !entity.getZerleg_amitad().isEmpty())
                    statement.setString(stIndex++, entity.getZerleg_amitad());

                if (entity != null && !entity.getDelflg().isEmpty())
                    statement.setString(stIndex++, entity.getDelflg());

                if (entity != null && !entity.getActflg().isEmpty())
                    statement.setString(stIndex++, entity.getActflg());

                if (entity != null && entity.getRecord_date()!= null)
                    statement.setDate(stIndex++, entity.getRecord_date());

                statement.setDate(stIndex++, entity.getMod_at());
                statement.setString(stIndex++, entity.getMod_by());
                statement.setLong(stIndex++, entity.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                    errorEntity.setErrText("Бичлэг амжилттай засагдлаа.");
                    errorEntity.setErrDesc("Бичлэг амжилттай засагдлаа.");
                    return errorEntity;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }


        errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1061));
        errorEntity.setErrText("Амжилтгүй боллоо. Дахин оролдоно уу.");
        errorEntity.setErrDesc("Амжилтгүй боллоо. Дахин оролдоно уу.");

        return errorEntity;
    }
}
