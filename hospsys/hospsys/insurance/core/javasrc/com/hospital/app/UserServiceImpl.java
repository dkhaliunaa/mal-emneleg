package com.hospital.app;

import com.enumclass.ErrorType;
import com.model.hos.ErrorEntity;
import com.jolbox.bonecp.BoneCP;
import com.model.User;
import com.mongodb.BasicDBObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 24be10264 on 8/18/2017.
 */
public class UserServiceImpl implements IUserService{

    private BoneCP boneCP;


    public UserServiceImpl (BoneCP boneCP) {
        this.boneCP = boneCP;
    }

    public UserServiceImpl (){

    }

    @Override
    public List<User> getUserList(User user) throws Exception {
        List<User> userList = null;

        String query = "SELECT us.`id`, `username`, `password`, `userrolecode`, `first_name`, `last_name`, `full_name`, " +
                "`middle_name`, `nick_name`, `gender`, `birth_day`, us.`cre_time`, us.`cre_by`, us.`mod_time`, " +
                "us.`mod_by`, `del_flg`, `registration`, `chatid`, us.`citycode`, us.`sumcode`, h_city.cityname, " +
                "h_city.cityname_en, h_sum.sumname, h_sum.sumname_en FROM `husers` AS us INNER JOIN h_city " +
                "ON us.citycode = h_city.code INNER JOIN h_sum ON us.sumcode = h_sum.sumcode WHERE  ";

        if (user != null && user.getUsername() != null && !user.getUsername().isEmpty()){
            query += " us.`username` = ? AND ";
        }

        if (user != null && user.getFirstName() != null && !user.getFirstName().isEmpty()){
            query += " us.`first_name` = ? AND ";
        }
        if (user != null && user.getLastName() != null && !user.getLastName().isEmpty()){
            query += " us.`last_name` = ? AND ";
        }
        if (user != null && user.getRoleCode() != null && !user.getRoleCode().isEmpty()){
            query += " us.`userrolecode` = ? AND ";
        }
        if (user != null && user.getGender() != null && !user.getGender().isEmpty()){
            query += " us.`gender` = ? AND ";
        }
        if (user != null && user.getCreBy() != null && !user.getCreBy().isEmpty()){
            query += " us.`cre_by` = ? AND ";
        }
        if (user != null && user.getDelFlg() != null && !user.getDelFlg().isEmpty()){
            query += " us.`del_flg` = ? AND ";
        }
        if (user != null && user.getRegId() != null && !user.getRegId().isEmpty()){
            query += " us.`registration` = ? AND ";
        }
        if (user != null && user.getCity() != null && !user.getCity().isEmpty()){
            query += " us.`citycode` = ? AND ";
        }
        if (user != null && user.getSum() != null && !user.getSum().isEmpty()){
            query += " us.`sumcode` = ? AND ";
        }
        if (user != null && user.getChatId() != null && !user.getChatId().isEmpty()){
            query += " us.`chatid` = ? AND ";
        }

        query += " us.`id` > 0 ORDER BY cre_time DESC ";

        try (Connection connection = boneCP.getConnection()) {
            int stindex = 1;
            int index = 1;

            try {
                PreparedStatement statement = connection.prepareStatement(query);

                if (user != null && user.getUsername() != null && !user.getUsername().isEmpty()){
                    statement.setString(stindex ++, user.getUsername());
                }

                if (user != null && user.getFirstName() != null && !user.getFirstName().isEmpty()){
                    statement.setString(stindex ++, user.getFirstName());
                }
                if (user != null && user.getLastName() != null && !user.getLastName().isEmpty()){
                    statement.setString(stindex ++, user.getLastName());
                }
                if (user != null && user.getRoleCode() != null && !user.getRoleCode().isEmpty()){
                    statement.setString(stindex ++, user.getRoleCode());
                }
                if (user != null && user.getGender() != null && !user.getGender().isEmpty()){
                    statement.setString(stindex ++, user.getGender());
                }
                if (user != null && user.getCreBy() != null && !user.getCreBy().isEmpty()){
                    statement.setString(stindex ++, user.getCreBy());
                }
                if (user != null && user.getDelFlg() != null && !user.getDelFlg().isEmpty()){
                    statement.setString(stindex ++, user.getDelFlg());
                }
                if (user != null && user.getRegId() != null && !user.getRegId().isEmpty()){
                    statement.setString(stindex ++, user.getRegId());
                }
                if (user != null && user.getCity() != null && !user.getCity().isEmpty()){
                    statement.setString(stindex ++, user.getCity());
                }
                if (user != null && user.getSum() != null && !user.getSum().isEmpty()){
                    statement.setString(stindex ++, user.getSum());
                }
                if (user != null && user.getChatId() != null && !user.getChatId().isEmpty()){
                    statement.setString(stindex ++, user.getChatId());
                }

                ResultSet resultSet = statement.executeQuery();

                userList = new ArrayList<>();

                while (resultSet.next()) {
                    User tmpuser = new User();

                    tmpuser.setId(resultSet.getLong("id"));
                    tmpuser.setUsername(resultSet.getString("username"));
                    tmpuser.setRoleCode(resultSet.getString("userrolecode"));
                    tmpuser.setFirstName(resultSet.getString("first_name"));
                    tmpuser.setLastName(resultSet.getString("last_name"));
                    tmpuser.setMiddleName(resultSet.getString("middle_name"));
                    tmpuser.setNickName(resultSet.getString("nick_name"));
                    tmpuser.setGender(resultSet.getString("gender"));
                    tmpuser.setBirthDate(resultSet.getDate("birth_day"));
                    tmpuser.setRegId(resultSet.getString("registration"));
                    tmpuser.setCity(resultSet.getString("citycode"));
                    tmpuser.setSum(resultSet.getString("sumcode"));

                    tmpuser.setDelFlg(resultSet.getString("del_flg"));
                    tmpuser.setCreAt(resultSet.getDate("cre_time"));
                    tmpuser.setCreBy(resultSet.getString("cre_by"));
                    tmpuser.setModAt(resultSet.getDate("mod_time"));
                    tmpuser.setModBy(resultSet.getString("mod_by"));

                    tmpuser.setCityName(resultSet.getString("cityname"));
                    tmpuser.setCityNameEn(resultSet.getString("cityname_en"));
                    tmpuser.setSumName(resultSet.getString("sumname"));
                    tmpuser.setSumNameEn(resultSet.getString("sumname_en"));
                    tmpuser.setChatId(resultSet.getString("chatid"));
                    tmpuser.put("index", index++);
                    userList.add(tmpuser);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (connection != null){
                    connection.close();
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return userList;
    }

    @Override
    public ErrorEntity insertUser(User newuser) throws Exception {
        PreparedStatement statement = null;

        String query_login = "INSERT INTO `hospital`.`husers` " +
                "(`username`, " +
                "`password`, " +
                "`userrolecode`, " +
                "`first_name`, " +
                "`last_name`, " +
                "`full_name`, " +
                "`middle_name`, " +
                "`nick_name`, " +
                "`gender`, " +
                "`birth_day`, " +
                "`cre_time`, " +
                "`cre_by`, " +
                "`mod_time`, " +
                "`mod_by`, " +
                "`del_flg`, " +
                "`registration`," +
                "`citycode`, " +
                "`sumcode`," +
                "`chatid`) " +
                "VALUES (" +
                " ?, " +
                " ?, " +
                " ?, " +
                " ?, " +
                " ?, " +
                " ?, " +
                " ?, " +
                " ?, " +
                " ?, " +
                " ?, " +
                " ?, " +
                " ?, " +
                " ?, " +
                " ?, " +
                " ?, " +
                " ?," +
                " ?, " +
                " ?, ? )";

        try (Connection connection = boneCP.getConnection()) {

            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, newuser.getUsername().trim());
                statement.setString(2, newuser.getPassword().trim());
                statement.setString(3, newuser.getRoleCode().trim());
                statement.setString(4, newuser.getFirstName().trim());
                statement.setString(5, newuser.getLastName().trim());
                statement.setString(6, newuser.getLastName() + " " + newuser.getFirstName());
                statement.setString(7, newuser.getMiddleName());
                statement.setString(8, newuser.getNickName());
                statement.setString(9, newuser.getGender().trim());
                statement.setDate(10, newuser.getBirthDate());
                statement.setDate(11, newuser.getCreAt());
                statement.setString(12, newuser.getCreBy().trim());
                statement.setDate(13, newuser.getModAt());
                statement.setString(14, newuser.getModBy());
                statement.setString(15, newuser.getDelFlg().trim());
                statement.setString(16, newuser.getRegId().trim());
                statement.setString(17, (newuser.getCity() != null) ? newuser.getCity().trim(): newuser.getCity());
                statement.setString(18, (newuser.getSum() != null) ? newuser.getSum().trim(): newuser.getSum());
                statement.setString(19, (newuser.getChatId() != null) ? newuser.getChatId().trim(): newuser.getChatId());


                User searchUser = new User();
                searchUser.setRegId(newuser.getRegId());
                searchUser.setUsername(newuser.getUsername());

                List<User> tmpList = getUserList(searchUser);

                if (tmpList.size() > 0) {
                    newuser.setId(tmpList.get(0).getId());
                    newuser.setDelFlg("N");
                    newuser.setActFlg("Y");

                    return updateUserData(newuser);
                }else {
                    int insertCount = statement.executeUpdate();

                    if (insertCount == 1) {
                        connection.commit();
                        return new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                if (connection != null) connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }


        return new ErrorEntity(ErrorType.ERROR, Long.valueOf(1057));
    }//end of function

    @Override
    public ErrorEntity updateUserData(User upuser) throws SQLException {

        String query = "UPDATE `husers` AS us SET ";

        if (upuser != null && upuser.getUsername() != null && !upuser.getUsername().isEmpty()){
            query += " us.`username` = ?, ";
        }

        if (upuser != null && upuser.getFirstName() != null && !upuser.getFirstName().isEmpty()){
            query += " us.`first_name` = ?, ";
        }
        if (upuser != null && upuser.getLastName() != null && !upuser.getLastName().isEmpty()){
            query += " us.`last_name` = ?, ";
        }
        if (upuser != null && upuser.getRoleCode() != null && !upuser.getRoleCode().isEmpty()){
            query += " us.`userrolecode` = ?, ";
        }
        if (upuser != null && upuser.getGender() != null && !upuser.getGender().isEmpty()){
            query += " us.`gender` = ?, ";
        }
        if (upuser != null && upuser.getCreBy() != null && !upuser.getCreBy().isEmpty()){
            query += " us.`cre_by` = ?, ";
        }
        if (upuser != null && upuser.getDelFlg() != null && !upuser.getDelFlg().isEmpty()){
            query += " us.`del_flg` = ?, ";
        }
        if (upuser != null && upuser.getRegId() != null && !upuser.getRegId().isEmpty()){
            query += " us.`registration` = ?, ";
        }
        if (upuser != null && upuser.getCity() != null && !upuser.getCity().isEmpty()){
            query += " us.`citycode` = ?, ";
        }
        if (upuser != null && upuser.getSum() != null && !upuser.getSum().isEmpty()){
            query += " us.`sumcode` = ?, ";
        }
        if (upuser != null && upuser.getPassword() != null && !upuser.getPassword().isEmpty()){
            query += " us.`password` = ?, ";
        }
        if (upuser != null && upuser.getChatId() != null && !upuser.getChatId().isEmpty()){
            query += " us.`chatid` = ?, ";
        }

        query += " mod_time = ?,  ";
        query += " mod_by = ?   ";

        query += " WHERE `id` = ? ";

        try (Connection connection = boneCP.getConnection()) {
            int stindex = 1;

            try {
                connection.setAutoCommit(false);
                PreparedStatement statement = connection.prepareStatement(query);

                if (upuser != null && upuser.getUsername() != null && !upuser.getUsername().isEmpty()){
                    statement.setString(stindex ++, upuser.getUsername());
                }

                if (upuser != null && upuser.getFirstName() != null && !upuser.getFirstName().isEmpty()){
                    statement.setString(stindex ++, upuser.getFirstName());
                }
                if (upuser != null && upuser.getLastName() != null && !upuser.getLastName().isEmpty()){
                    statement.setString(stindex ++, upuser.getLastName());
                }
                if (upuser != null && upuser.getRoleCode() != null && !upuser.getRoleCode().isEmpty()){
                    statement.setString(stindex ++, upuser.getRoleCode());
                }
                if (upuser != null && upuser.getGender() != null && !upuser.getGender().isEmpty()){
                    statement.setString(stindex ++, upuser.getGender());
                }
                if (upuser != null && upuser.getCreBy() != null && !upuser.getCreBy().isEmpty()){
                    statement.setString(stindex ++, upuser.getCreBy());
                }
                if (upuser != null && upuser.getDelFlg() != null && !upuser.getDelFlg().isEmpty()){
                    statement.setString(stindex ++, upuser.getDelFlg());
                }
                if (upuser != null && upuser.getRegId() != null && !upuser.getRegId().isEmpty()){
                    statement.setString(stindex ++, upuser.getRegId());
                }
                if (upuser != null && upuser.getCity() != null && !upuser.getCity().isEmpty()){
                    statement.setString(stindex ++, upuser.getCity());
                }
                if (upuser != null && upuser.getSum() != null && !upuser.getSum().isEmpty()){
                    statement.setString(stindex ++, upuser.getSum());
                }
                if (upuser != null && upuser.getPassword() != null && !upuser.getPassword().isEmpty()){
                    statement.setString(stindex ++, upuser.getPassword());
                }
                if (upuser != null && upuser.getChatId() != null && !upuser.getChatId().isEmpty()){
                    statement.setString(stindex ++, upuser.getChatId());
                }

                statement.setDate(stindex ++, upuser.getModAt());
                statement.setString(stindex ++, upuser.getModBy());
                statement.setLong(stindex ++, upuser.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    return new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                }


                if (statement != null) statement.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (connection != null){
                    connection.close();
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return new ErrorEntity(ErrorType.ERROR, Long.valueOf(1061));
    }

    public Integer getUsersCount()throws SQLException{
        int userCnt = 0;
        String query = "SELECT count(*) as usrcnt FROM `husers` WHERE del_flg = 'N'";

        try (Connection connection = boneCP.getConnection()) {
            int stindex = 1;

            try {
                connection.setAutoCommit(false);
                PreparedStatement statement = connection.prepareStatement(query);

                ResultSet resultSet = statement.executeQuery();

                while (resultSet.next()) {
                    userCnt = resultSet.getInt("usrcnt");
                }


                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (connection != null){
                    connection.close();
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return userCnt;
    }
}
