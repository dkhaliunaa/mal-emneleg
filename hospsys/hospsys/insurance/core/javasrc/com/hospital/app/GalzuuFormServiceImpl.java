package com.hospital.app;


import com.Config;
import com.enumclass.ErrorType;
import com.jolbox.bonecp.BoneCP;
import com.model.hos.City;
import com.model.hos.ErrorEntity;
import com.model.hos.GalzuuFormEntity;
import com.model.hos.Sum;
import com.mongodb.BasicDBObject;
import com.rpc.PdfExcelUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by Tilyeujan on 9/2/2017.
 */
public class GalzuuFormServiceImpl implements IGalzuuFormService {
    /**
     * Objects
     */
    private BoneCP boneCP;
    private ErrorEntity errorEntity;


    public GalzuuFormServiceImpl(BoneCP boneCP) {
        this.boneCP = boneCP;
    }

    @Override
    public ErrorEntity insertNewData(GalzuuFormEntity entity) throws SQLException {
        PreparedStatement statement = null;

        if (entity == null) {
            errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));

            errorEntity.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errorEntity.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            
            return errorEntity;
        }

        String query_login = "INSERT INTO `hform01`(`frmkey`, `citycode`, `sumcode`, `bagname`, `gazarner`, `golomttoo`, " +
                "`malchinner`, `uvchlol_burt_urh_hari`, `coordinate_n`, `coordinate_e`, `duudlagaognoo`, " +
                "`uvchlolehelsen_ognoo`, `uvchlolburt_ognoo`, `lab_bat_ognoo`, `lab_bat_hevshil`, `sharga`, " +
                "`shurdun`, `uvch_uher`, `uvch_honi`, `uvch_ymaa`, `uvch_busad`, `uhsen_uher`, `uhsen_honi`, `uhsen_ymaa`, " +
                "`uhsen_busad`, `ustgasan_uher`, `ustgasan_honi`, `ustgasan_ymaa`, `ustgasan_busad`, `horio_ceer_eseh`, " +
                "`horio_ceer_ognoo`, `horio_ceer_cucalsan_ognoo`, `ustgasan_eseh`, `ustgal_hiisen_ognoo`, `delflg`, " +
                "`cre_at`, `cre_by`, `mod_at`, `mod_by`, `record_date`, `actflg`) " +
                "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        try (Connection connection = boneCP.getConnection()) {
            try{
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, entity.getFrmkey());
                statement.setString(2, entity.getCitycode());
                statement.setString(3, entity.getSumcode());
                statement.setString(4, entity.getBagname());
                statement.setString(5, entity.getGazarner());
                statement.setDouble(6, entity.getGolomttoo());
                statement.setString(7, entity.getMalchinner());
                statement.setString(8, entity.getUvchlol_burt_urh_hari());
                statement.setString(9, entity.getCoordinate_n());
                statement.setString(10, entity.getCoordinate_e());
                statement.setDate(11, entity.getDuudlagaognoo());
                statement.setDate(12, entity.getUvchlolehelsen_ognoo());
                statement.setDate(13, entity.getUvchlolburt_ognoo());
                statement.setDate(14, entity.getLab_bat_ognoo());
                statement.setString(15, entity.getLab_bat_hevshil());
                statement.setString(16, entity.getSharga());
                statement.setString(17, entity.getShurdun());
                statement.setDouble(18, entity.getUvch_uher());
                statement.setDouble(19, entity.getUvch_honi());
                statement.setDouble(20, entity.getUvch_ymaa());
                statement.setDouble(21, entity.getUvch_busad());
                statement.setDouble(22, entity.getUhsen_uher());
                statement.setDouble(23, entity.getUhsen_honi());
                statement.setDouble(24, entity.getUhsen_ymaa());
                statement.setDouble(25, entity.getUhsen_busad());
                statement.setDouble(26, entity.getUstgasan_uher());
                statement.setDouble(27, entity.getUstgasan_honi());
                statement.setDouble(28, entity.getUstgasan_ymaa());
                statement.setDouble(29, entity.getUstgasan_busad());
                statement.setString(30, entity.getHorio_ceer_eseh());
                statement.setDate(31, entity.getHorio_ceer_ognoo());
                statement.setDate(32, entity.getHorio_ceer_cucalsan_ognoo());
                statement.setString(33, entity.getUstgasan_eseh());
                statement.setDate(34, entity.getUstgal_hiisen_ognoo());
                statement.setString(35, entity.getDelflg());
                statement.setDate(36, entity.getCre_at());
                statement.setString(37, entity.getCre_by());
                statement.setDate(38, entity.getMod_at());
                statement.setString(39, entity.getMod_by());
                statement.setDate(40, entity.getRecord_date());
                statement.setString(41, entity.getActflg());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1){
                    connection.commit();

                    errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));

                    errorEntity.setErrText("Бичлэг амжилттай хадгалагдлаа.");
                    errorEntity.setErrDesc("Бичлэг амжилттай хадгалагдлаа.");

                    return errorEntity;
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }finally {
                if (statement != null)statement.close();
                connection.close();
            }

        }catch (Exception ex){

        }

        errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1057));
        errorEntity.setErrText("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");
        errorEntity.setErrDesc("Бичлэг амжилтгүй хадгалагдлаа. Дахин оролдоно уу.");

        return errorEntity;
    }

    @Override
    public List<GalzuuFormEntity> getListData(GalzuuFormEntity entity) throws SQLException {
        int stIndex = 1;
        int index = 1;
        List<GalzuuFormEntity> entityList = null;

        String query = "SELECT `hform01`.`id`, `frmkey`, `hform01`.`citycode`, `hform01`.`sumcode`, `hform01`.`bagname`, `gazarner`, `golomttoo`, `malchinner`, " +
                "`uvchlol_burt_urh_hari`, `coordinate_n`, `coordinate_e`, `duudlagaognoo`, `uvchlolehelsen_ognoo`, " +
                "`uvchlolburt_ognoo`, `lab_bat_ognoo`, `lab_bat_hevshil`, `sharga`, `shurdun`, `uvch_uher`, `uvch_honi`, " +
                "`uvch_ymaa`, `uvch_busad`, `uhsen_uher`, `uhsen_honi`, `uhsen_ymaa`, `uhsen_busad`, `ustgasan_uher`, " +
                "`ustgasan_honi`, `ustgasan_ymaa`, `ustgasan_busad`, `horio_ceer_eseh`, `horio_ceer_ognoo`, " +
                "`horio_ceer_cucalsan_ognoo`, `ustgasan_eseh`, `ustgal_hiisen_ognoo`, `hform01`.`delflg`, `hform01`.`cre_at`, `hform01`.`cre_by`, " +
                "`hform01`.`mod_at`, `hform01`.`mod_by`, `hform01`.`record_date`, `hform01`.`actflg`,`h_sum`.sumname, `h_sum`.sumname_en, " +
                "`h_baghoroo`.horooname, `h_baghoroo`.horooname_en, `h_city`.cityname, `h_city`.cityname_en " +
                "FROM `hform01` INNER JOIN `h_city` " +
                "ON `h_city`.code = `hform01`.citycode " +
                "INNER JOIN `h_sum` " +
                "ON `h_sum`.sumcode = `hform01`.sumcode " +
                "INNER JOIN `h_baghoroo` ON `hform01`.bagname = `h_baghoroo`.horoocode AND `h_baghoroo`.sumcode = `hform01`.`sumcode` " +
                "WHERE ";

        if (entity.getFrmkey() != null && !entity.getFrmkey().isEmpty()){
            query += " `hform01`.frmkey = ? AND ";
        }
        if (entity.getCitycode() != null && !entity.getCitycode().isEmpty()){
            query += " `hform01`.citycode = ? AND ";
        }
        if (entity.getSumcode() != null && !entity.getSumcode().isEmpty()){
            query += " `hform01`.sumcode = ? AND ";
        }
        if (entity.getBagname() != null && !entity.getBagname().isEmpty()){
            query += " `hform01`.bagname = ? AND ";
        }
        if (entity.getGazarner() != null && !entity.getGazarner().isEmpty()){
            query += " `hform01`.gazarner = ? AND ";
        }
        if (entity.getGolomttoo() != 0){
            query += " `hform01`.golomttoo = ? AND ";
        }
        if (entity.getMalchinner() != null && !entity.getMalchinner().isEmpty()){
            query += " `hform01`.malchinner = ? AND ";
        }
        if (entity.getUvchlol_burt_urh_hari() != null && !entity.getUvchlol_burt_urh_hari().isEmpty()){
            query += " `hform01`.uvchlol_burt_urh_hari = ? AND ";
        }
        if (entity.getCoordinate_n() != null && !entity.getCoordinate_n().isEmpty()){
            query += " `hform01`.coordinate_n = ? AND ";
        }
        if (entity.getCoordinate_e() != null && !entity.getCoordinate_e().isEmpty()){
            query += " `hform01`.coordinate_e = ? AND ";
        }
        if (entity.getDuudlagaognoo() != null){
            query += " `hform01`.duudlagaognoo = ? AND ";
        }
        if (entity.getUvchlolehelsen_ognoo() != null){
            query += " `hform01`.uvchlolehelsen_ognoo = ? AND ";
        }
        if (entity.getUvchlolburt_ognoo() != null){
            query += " `hform01`.uvchlolburt_ognoo = ? AND ";
        }
        if (entity.getLab_bat_ognoo() != null){
            query += " `hform01`.lab_bat_ognoo = ? AND ";
        }

        if (entity.getLab_bat_hevshil() != null && !entity.getLab_bat_hevshil().isEmpty()){
            query += " `hform01`.lab_bat_hevshil = ? AND ";
        }
        if (entity.getSharga() != null && !entity.getSharga().isEmpty()){
            query += " `hform01`.sharga = ? AND ";
        }
        if (entity.getShurdun() != null && !entity.getShurdun().isEmpty()){
            query += " `hform01`.shurdun = ? AND ";
        }
        if (entity.getUvch_uher() != 0){
            query += " `hform01`.uvch_uher = ? AND ";
        }
        if (entity.getUvch_honi() != 0){
            query += " `hform01`.uvch_honi = ? AND ";
        }
        if (entity.getUvch_ymaa() != 0){
            query += " `hform01`.uvch_ymaa = ? AND ";
        }
        if (entity.getUvch_busad() != 0){
            query += " `hform01`.uvch_busad = ? AND ";
        }
        if (entity.getUhsen_uher() != 0){
            query += " `hform01`.uhsen_uher = ? AND ";
        }
        if (entity.getUhsen_honi() != 0){
            query += " `hform01`.uhsen_honi = ? AND ";
        }
        if (entity.getUhsen_ymaa() != 0){
            query += " `hform01`.uhsen_ymaa = ? AND ";
        }
        if (entity.getUhsen_busad() != 0){
            query += " `hform01`.uhsen_busad = ? AND ";
        }
        if (entity.getUstgasan_uher() != 0){
            query += " `hform01`.ustgasan_uher = ? AND ";
        }
        if (entity.getUstgasan_honi() != 0){
            query += " `hform01`.ustgasan_honi = ? AND ";
        }
        if (entity.getUstgasan_ymaa() != 0){
            query += " `hform01`.ustgasan_ymaa = ? AND ";
        }
        if (entity.getUstgasan_busad() != 0){
            query += " `hform01`.ustgasan_busad = ? AND ";
        }
        if (entity.getHorio_ceer_eseh() != null && !entity.getHorio_ceer_eseh().isEmpty()){
            query += " `hform01`.horio_ceer_eseh = ? AND ";
        }
        if (entity.getHorio_ceer_ognoo() != null){
            query += " `hform01`.horio_ceer_ognoo = ? AND ";
        }
        if (entity.getHorio_ceer_cucalsan_ognoo() != null){
            query += " `hform01`.horio_ceer_cucalsan_ognoo = ? AND ";
        }
        if (entity.getUstgasan_eseh() != null && !entity.getUstgasan_eseh().isEmpty()){
            query += " `hform01`.ustgasan_eseh = ? AND ";
        }
        if (entity.getUstgal_hiisen_ognoo() != null){
            query += " `hform01`.ustgal_hiisen_ognoo = ? AND ";
        }
        if (entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
            query += " `hform01`.delflg = ? AND ";
        }
        if (entity.getCre_by() != null && !entity.getCre_by().isEmpty()){
            query += " `hform01`.cre_by = ? AND ";
        }
        if (entity.getMod_at() != null){
            query += " `hform01`.mod_at = ? AND ";
        }
        if (entity.getMod_by() != null && !entity.getMod_by().isEmpty()){
            query += " `hform01`.mod_by = ? AND ";
        }
        if (entity.getRecord_date() != null && entity.getSearchDate() != null){
            query += " (`hform01`.`record_date` BETWEEN ? AND ?) AND ";
        }
        if (entity.getActflg() != null && !entity.getActflg().isEmpty()){
            query += " `hform01`.actflg = ? AND ";
        }
        if (entity != null && entity.getCre_at() != null && entity.getSearchDate() != null){
            query += " (`hform01`.`cre_at` BETWEEN ? AND ?) AND ";
        }

        query += " `hform01`.`id` > 0 ";
        query += " ORDER BY `hform01`.`cre_at` DESC ";

        try (Connection connection = boneCP.getConnection()) {

            try {
                PreparedStatement statement = connection.prepareStatement(query);

                if (entity.getFrmkey() != null && !entity.getFrmkey().isEmpty()){
                    statement.setString(stIndex++, entity.getFrmkey());
                }
                if (entity.getCitycode() != null && !entity.getCitycode().isEmpty()){
                    statement.setString(stIndex++, entity.getCitycode());
                }
                if (entity.getSumcode() != null && !entity.getSumcode().isEmpty()){
                    statement.setString(stIndex++, entity.getSumcode());
                }
                if (entity.getBagname() != null && !entity.getBagname().isEmpty()){
                    statement.setString(stIndex++, entity.getBagname());
                }
                if (entity.getGazarner() != null && !entity.getGazarner().isEmpty()){
                    statement.setString(stIndex++, entity.getGazarner());
                }
                if (entity.getGolomttoo() != 0){
                    statement.setDouble(stIndex++, entity.getGolomttoo());
                }
                if (entity.getMalchinner() != null && !entity.getMalchinner().isEmpty()){
                    statement.setString(stIndex++, entity.getMalchinner());
                }
                if (entity.getUvchlol_burt_urh_hari() != null && !entity.getUvchlol_burt_urh_hari().isEmpty()){
                    statement.setString(stIndex++, entity.getUvchlol_burt_urh_hari());
                }
                if (entity.getCoordinate_n() != null && !entity.getCoordinate_n().isEmpty()){
                    statement.setString(stIndex++, entity.getCoordinate_n());
                }
                if (entity.getCoordinate_e() != null && !entity.getCoordinate_e().isEmpty()){
                    statement.setString(stIndex++, entity.getCoordinate_e());
                }
                if (entity.getDuudlagaognoo() != null){
                    statement.setDate(stIndex++, entity.getDuudlagaognoo());
                }
                if (entity.getUvchlolehelsen_ognoo() != null){
                    statement.setDate(stIndex++, entity.getUvchlolehelsen_ognoo());
                }
                if (entity.getUvchlolburt_ognoo() != null){
                    statement.setDate(stIndex++, entity.getUvchlolburt_ognoo());
                }
                if (entity.getLab_bat_ognoo() != null){
                    statement.setDate(stIndex++, entity.getLab_bat_ognoo());
                }

                if (entity.getLab_bat_hevshil() != null && !entity.getLab_bat_hevshil().isEmpty()){
                    statement.setString(stIndex++, entity.getLab_bat_hevshil());
                }
                if (entity.getSharga() != null && !entity.getSharga().isEmpty()){
                    statement.setString(stIndex++, entity.getSharga());
                }
                if (entity.getShurdun() != null && !entity.getShurdun().isEmpty()){
                    statement.setString(stIndex++, entity.getShurdun());
                }
                if (entity.getUvch_uher() != 0){
                    statement.setDouble(stIndex++, entity.getUvch_uher());
                }
                if (entity.getUvch_honi() != 0){
                    statement.setDouble(stIndex++, entity.getUvch_honi());
                }
                if (entity.getUvch_ymaa() != 0){
                    statement.setDouble(stIndex++, entity.getUvch_ymaa());
                }
                if (entity.getUvch_busad() != 0){
                    statement.setDouble(stIndex++, entity.getUvch_busad());
                }
                if (entity.getUhsen_uher() != 0){
                    statement.setDouble(stIndex++, entity.getUhsen_uher());
                }
                if (entity.getUhsen_honi() != 0){
                    statement.setDouble(stIndex++, entity.getUhsen_honi());
                }
                if (entity.getUhsen_ymaa() != 0){
                    statement.setDouble(stIndex++, entity.getUhsen_ymaa());
                }
                if (entity.getUhsen_busad() != 0){
                    statement.setDouble(stIndex++, entity.getUhsen_busad());
                }
                if (entity.getUstgasan_uher() != 0){
                    statement.setDouble(stIndex++, entity.getUstgasan_uher());
                }
                if (entity.getUstgasan_honi() != 0){
                    statement.setDouble(stIndex++, entity.getUstgasan_honi());
                }
                if (entity.getUstgasan_ymaa() != 0){
                    statement.setDouble(stIndex++, entity.getUstgasan_ymaa());
                }
                if (entity.getUstgasan_busad() != 0){
                    statement.setDouble(stIndex++, entity.getUstgasan_busad());
                }
                if (entity.getHorio_ceer_eseh() != null && !entity.getHorio_ceer_eseh().isEmpty()){
                    statement.setString(stIndex++, entity.getHorio_ceer_eseh());
                }
                if (entity.getHorio_ceer_ognoo() != null){
                    statement.setDate(stIndex++, entity.getHorio_ceer_ognoo());
                }
                if (entity.getHorio_ceer_cucalsan_ognoo() != null){
                    statement.setDate(stIndex++, entity.getHorio_ceer_cucalsan_ognoo());
                }
                if (entity.getUstgasan_eseh() != null && !entity.getUstgasan_eseh().isEmpty()){
                    statement.setString(stIndex++, entity.getUstgasan_eseh());
                }
                if (entity.getUstgal_hiisen_ognoo() != null){
                    statement.setDate(stIndex++, entity.getUstgal_hiisen_ognoo());
                }
                if (entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
                    statement.setString(stIndex++, entity.getDelflg());
                }
                if (entity.getCre_by() != null && !entity.getCre_by().isEmpty()){
                    statement.setString(stIndex++, entity.getCre_by());
                }
                if (entity.getMod_at() != null){
                    statement.setDate(stIndex++, entity.getMod_at());
                }
                if (entity.getMod_by() != null && !entity.getMod_by().isEmpty()){
                    statement.setString(stIndex++, entity.getMod_by());
                }
                if (entity.getRecord_date() != null && entity.getSearchDate() != null){
                    statement.setDate(stIndex++, entity.getRecord_date());
                    statement.setDate(stIndex++, entity.getSearchDate());
                }
                if (entity.getActflg() != null && !entity.getActflg().isEmpty()){
                    statement.setString(stIndex++, entity.getActflg());
                }
                if (entity != null && entity.getCre_at() != null && entity.getSearchDate() != null){
                    statement.setDate(stIndex++, entity.getCre_at());
                    statement.setDate(stIndex++, entity.getSearchDate());
                }


                ResultSet resultSet = statement.executeQuery();

                entityList = new ArrayList<>();

                IComboService comboService = new ComboServiceImpl(boneCP);
                //List<BasicDBObject> cmbType = comboService.getComboVals("MAL", "MN");
                //List<BasicDBObject> shgazarType = comboService.getComboVals("SHGAZ", "MN");
                List<BasicDBObject> shargaType = comboService.getComboVals("SHARGA", "MN");
                //List<BasicDBObject> uvchinNer = comboService.getComboVals("UVCH", "MN");

                while (resultSet.next()) {
                    GalzuuFormEntity objEntity = new GalzuuFormEntity();

                    objEntity.setId(resultSet.getLong("id"));
                    objEntity.setFrmkey(resultSet.getString("frmkey"));
                    objEntity.setCitycode(resultSet.getString("citycode"));
                    objEntity.setSumcode(resultSet.getString("sumcode"));
                    objEntity.setBagname(resultSet.getString("bagname"));
                    objEntity.setGazarner(resultSet.getString("gazarner"));
                    objEntity.setGolomttoo(resultSet.getDouble("golomttoo"));
                    objEntity.setMalchinner(resultSet.getString("malchinner"));
                    objEntity.setUvchlol_burt_urh_hari(resultSet.getString("uvchlol_burt_urh_hari"));
                    objEntity.setCoordinate_n(resultSet.getString("coordinate_n"));
                    objEntity.setCoordinate_e(resultSet.getString("coordinate_e"));
                    objEntity.setDuudlagaognoo(resultSet.getDate("duudlagaognoo"));
                    objEntity.setUvchlolehelsen_ognoo(resultSet.getDate("uvchlolehelsen_ognoo"));
                    objEntity.setUvchlolburt_ognoo(resultSet.getDate("uvchlolburt_ognoo"));
                    objEntity.setLab_bat_ognoo(resultSet.getDate("lab_bat_ognoo"));
                    objEntity.setLab_bat_hevshil(resultSet.getString("lab_bat_hevshil"));
                    objEntity.setSharga(resultSet.getString("sharga"));
                    objEntity.setShurdun(resultSet.getString("shurdun"));
                    objEntity.setUvch_uher(resultSet.getDouble("uvch_uher"));
                    objEntity.setUvch_honi(resultSet.getDouble("uvch_honi"));
                    objEntity.setUvch_ymaa(resultSet.getDouble("uvch_ymaa"));
                    objEntity.setUvch_busad(resultSet.getDouble("uvch_busad"));
                    objEntity.setUhsen_uher(resultSet.getDouble("uhsen_uher"));
                    objEntity.setUhsen_honi(resultSet.getDouble("uhsen_honi"));
                    objEntity.setUhsen_ymaa(resultSet.getDouble("uhsen_ymaa"));
                    objEntity.setUhsen_busad(resultSet.getDouble("uhsen_busad"));
                    objEntity.setUstgasan_uher(resultSet.getDouble("ustgasan_uher"));
                    objEntity.setUstgasan_honi(resultSet.getDouble("ustgasan_honi"));
                    objEntity.setUstgasan_ymaa(resultSet.getDouble("ustgasan_ymaa"));
                    objEntity.setUstgasan_busad(resultSet.getDouble("ustgasan_busad"));

                    objEntity.setHorio_ceer_eseh(resultSet.getString("horio_ceer_eseh"));
                    objEntity.setHorio_ceer_ognoo(resultSet.getDate("horio_ceer_ognoo"));
                    objEntity.setHorio_ceer_cucalsan_ognoo(resultSet.getDate("horio_ceer_cucalsan_ognoo"));

                    objEntity.setUstgasan_eseh(resultSet.getString("ustgasan_eseh"));
                    objEntity.setUstgal_hiisen_ognoo(resultSet.getDate("ustgal_hiisen_ognoo"));
                    objEntity.setDelflg(resultSet.getString("delflg"));
                    objEntity.setCre_at(resultSet.getDate("cre_at"));
                    objEntity.setCre_by(resultSet.getString("cre_by"));
                    objEntity.setMod_at(resultSet.getDate("mod_at"));
                    objEntity.setMod_by(resultSet.getString("mod_by"));
                    objEntity.setRecord_date(resultSet.getDate("record_date"));
                    objEntity.setActflg(resultSet.getString("actflg"));


                    objEntity.put("shinjilsenarga", getTypeNer(shargaType, resultSet.getString("sharga")));

                    objEntity.put("cityname", resultSet.getString("cityname"));
                    objEntity.put("sumname", resultSet.getString("sumname"));
                    objEntity.put("horooname", resultSet.getString("horooname"));
                    objEntity.put("index", index++);

                    objEntity.put("strHorioCeerOgnoo", dateFormat.format(objEntity.getHorio_ceer_ognoo()));
                    objEntity.put("strDudlagaOgnoo", dateFormat.format(objEntity.getDuudlagaognoo()));
                    objEntity.put("strUvchEhelsenOgnoo", dateFormat.format(objEntity.getUvchlolehelsen_ognoo()));
                    objEntity.put("strUvchBurtOgnoo", dateFormat.format(objEntity.getUvchlolburt_ognoo()));
                    objEntity.put("strUstgalHiisenOgnoo", dateFormat.format(objEntity.getUstgal_hiisen_ognoo()));
                    objEntity.put("strRecordOgnoo", dateFormat.format(objEntity.getRecord_date()));

                    entityList.add(objEntity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return entityList;
    }

    /**
     * Get name
     *
     * @param cmbType
     * @param mal_turul_mn
     * @return
     */
    private String getTypeNer(List<BasicDBObject> cmbType, String mal_turul_mn) {
        for (BasicDBObject obj :
                cmbType) {
            if (obj.get("id") != null && mal_turul_mn != null && obj.getString("id").equalsIgnoreCase(mal_turul_mn)) {
                return obj.getString("name");
            }
        }

        return "";
    }

    @Override
    public ErrorEntity updateData(GalzuuFormEntity entity) throws SQLException {
        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (entity == null) new ErrorEntity(ErrorType.INFO, Long.valueOf(1059));
        if (entity == null) {
            errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));
            errorEntity.setErrText("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");
            errorEntity.setErrDesc("Шаардлагатай мэдээлэл хоосон байна. Дахин оролдоно уу.");

            return errorEntity;
        }

        query = "UPDATE `hospital`.`hform01` " +
                "SET     ";

        if (entity.getFrmkey() != null && !entity.getFrmkey().isEmpty()){
            query += " `hform01`.frmkey = ?, ";
        }
        if (entity.getCitycode() != null && !entity.getCitycode().isEmpty()){
            query += " `hform01`.citycode = ?, ";
        }
        if (entity.getSumcode() != null && !entity.getSumcode().isEmpty()){
            query += " `hform01`.sumcode = ?, ";
        }
        if (entity.getBagname() != null && !entity.getBagname().isEmpty()){
            query += " `hform01`.bagname = ?, ";
        }
        if (entity.getGazarner() != null && !entity.getGazarner().isEmpty()){
            query += " `hform01`.gazarner = ?, ";
        }
        if (entity.getGolomttoo() != 0){
            query += " `hform01`.golomttoo = ?, ";
        }
        if (entity.getMalchinner() != null && !entity.getMalchinner().isEmpty()){
            query += " `hform01`.malchinner = ?, ";
        }
        if (entity.getUvchlol_burt_urh_hari() != null && !entity.getUvchlol_burt_urh_hari().isEmpty()){
            query += " `hform01`.uvchlol_burt_urh_hari = ?, ";
        }
        if (entity.getCoordinate_n() != null && !entity.getCoordinate_n().isEmpty()){
            query += " `hform01`.coordinate_n = , ";
        }
        if (entity.getCoordinate_e() != null && !entity.getCoordinate_e().isEmpty()){
            query += " `hform01`.coordinate_e = ?, ";
        }
        if (entity.getDuudlagaognoo() != null){
            query += " `hform01`.duudlagaognoo = ?, ";
        }
        if (entity.getUvchlolehelsen_ognoo() != null){
            query += " `hform01`.uvchlolehelsen_ognoo = ?, ";
        }
        if (entity.getUvchlolburt_ognoo() != null){
            query += " `hform01`.uvchlolburt_ognoo = ?, ";
        }
        if (entity.getLab_bat_ognoo() != null){
            query += " `hform01`.lab_bat_ognoo = ?, ";
        }

        if (entity.getLab_bat_hevshil() != null && !entity.getLab_bat_hevshil().isEmpty()){
            query += " `hform01`.lab_bat_hevshil = ?, ";
        }
        if (entity.getSharga() != null && !entity.getSharga().isEmpty()){
            query += " `hform01`.sharga = ?, ";
        }
        if (entity.getShurdun() != null && !entity.getShurdun().isEmpty()){
            query += " `hform01`.shurdun = ?, ";
        }
        if (entity.getUvch_uher() != 0){
            query += " `hform01`.uvch_uher = ?, ";
        }
        if (entity.getUvch_honi() != 0){
            query += " `hform01`.uvch_honi = ?, ";
        }
        if (entity.getUvch_ymaa() != 0){
            query += " `hform01`.uvch_ymaa = ?, ";
        }
        if (entity.getUvch_busad() != 0){
            query += " `hform01`.uvch_busad = ?, ";
        }
        if (entity.getUhsen_uher() != 0){
            query += " `hform01`.uhsen_uher = ?, ";
        }
        if (entity.getUhsen_honi() != 0){
            query += " `hform01`.uhsen_honi = ?, ";
        }
        if (entity.getUhsen_ymaa() != 0){
            query += " `hform01`.uhsen_ymaa = ?, ";
        }
        if (entity.getUhsen_busad() != 0){
            query += " `hform01`.uhsen_busad = ?, ";
        }
        if (entity.getUstgasan_uher() != 0){
            query += " `hform01`.ustgasan_uher = ?, ";
        }
        if (entity.getUstgasan_honi() != 0){
            query += " `hform01`.ustgasan_honi = ?, ";
        }
        if (entity.getUstgasan_ymaa() != 0){
            query += " `hform01`.ustgasan_ymaa = ?, ";
        }
        if (entity.getUstgasan_busad() != 0){
            query += " `hform01`.ustgasan_busad = ?, ";
        }
        if (entity.getHorio_ceer_eseh() != null && !entity.getHorio_ceer_eseh().isEmpty()){
            query += " `hform01`.horio_ceer_eseh = ?, ";
        }
        if (entity.getHorio_ceer_ognoo() != null){
            query += " `hform01`.horio_ceer_ognoo = ?, ";
        }
        if (entity.getHorio_ceer_cucalsan_ognoo() != null){
            query += " `hform01`.horio_ceer_cucalsan_ognoo = ?, ";
        }
        if (entity.getUstgasan_eseh() != null && !entity.getUstgasan_eseh().isEmpty()){
            query += " `hform01`.ustgasan_eseh = ?, ";
        }
        if (entity.getUstgal_hiisen_ognoo() != null){
            query += " `hform01`.ustgal_hiisen_ognoo = ?, ";
        }
        if (entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
            query += " `hform01`.delflg = ?, ";
        }
        if (entity.getRecord_date() != null){
            query += " `hform01`.`record_date` = ?,";
        }
        if (entity.getActflg() != null && !entity.getActflg().isEmpty()){
            query += " `hform01`.actflg = ?, ";
        }

        query += " mod_at = ?,  ";
        query += " mod_by = ?   ";
        query += " WHERE  ID   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (entity.getFrmkey() != null && !entity.getFrmkey().isEmpty()){
                    statement.setString(stIndex++, entity.getFrmkey());
                }
                if (entity.getCitycode() != null && !entity.getCitycode().isEmpty()){
                    statement.setString(stIndex++, entity.getCitycode());
                }
                if (entity.getSumcode() != null && !entity.getSumcode().isEmpty()){
                    statement.setString(stIndex++, entity.getSumcode());
                }
                if (entity.getBagname() != null && !entity.getBagname().isEmpty()){
                    statement.setString(stIndex++, entity.getBagname());
                }
                if (entity.getGazarner() != null && !entity.getGazarner().isEmpty()){
                    statement.setString(stIndex++, entity.getGazarner());
                }
                if (entity.getGolomttoo() != 0){
                    statement.setDouble(stIndex++, entity.getGolomttoo());
                }
                if (entity.getMalchinner() != null && !entity.getMalchinner().isEmpty()){
                    statement.setString(stIndex++, entity.getMalchinner());
                }
                if (entity.getUvchlol_burt_urh_hari() != null && !entity.getUvchlol_burt_urh_hari().isEmpty()){
                    statement.setString(stIndex++, entity.getUvchlol_burt_urh_hari());
                }
                if (entity.getCoordinate_n() != null && !entity.getCoordinate_n().isEmpty()){
                    statement.setString(stIndex++, entity.getCoordinate_n());
                }
                if (entity.getCoordinate_e() != null && !entity.getCoordinate_e().isEmpty()){
                    statement.setString(stIndex++, entity.getCoordinate_e());
                }
                if (entity.getDuudlagaognoo() != null){
                    statement.setDate(stIndex++, entity.getDuudlagaognoo());
                }
                if (entity.getUvchlolehelsen_ognoo() != null){
                    statement.setDate(stIndex++, entity.getUvchlolehelsen_ognoo());
                }
                if (entity.getUvchlolburt_ognoo() != null){
                    statement.setDate(stIndex++, entity.getUvchlolburt_ognoo());
                }
                if (entity.getLab_bat_ognoo() != null){
                    statement.setDate(stIndex++, entity.getLab_bat_ognoo());
                }

                if (entity.getLab_bat_hevshil() != null && !entity.getLab_bat_hevshil().isEmpty()){
                    statement.setString(stIndex++, entity.getLab_bat_hevshil());
                }
                if (entity.getSharga() != null && !entity.getSharga().isEmpty()){
                    statement.setString(stIndex++, entity.getSharga());
                }
                if (entity.getShurdun() != null && !entity.getShurdun().isEmpty()){
                    statement.setString(stIndex++, entity.getShurdun());
                }
                if (entity.getUvch_uher() != 0){
                    statement.setDouble(stIndex++, entity.getUvch_uher());
                }
                if (entity.getUvch_honi() != 0){
                    statement.setDouble(stIndex++, entity.getUvch_honi());
                }
                if (entity.getUvch_ymaa() != 0){
                    statement.setDouble(stIndex++, entity.getUvch_ymaa());
                }
                if (entity.getUvch_busad() != 0){
                    statement.setDouble(stIndex++, entity.getUvch_busad());
                }
                if (entity.getUhsen_uher() != 0){
                    statement.setDouble(stIndex++, entity.getUhsen_uher());
                }
                if (entity.getUhsen_honi() != 0){
                    statement.setDouble(stIndex++, entity.getUhsen_honi());
                }
                if (entity.getUhsen_ymaa() != 0){
                    statement.setDouble(stIndex++, entity.getUhsen_ymaa());
                }
                if (entity.getUhsen_busad() != 0){
                    statement.setDouble(stIndex++, entity.getUhsen_busad());
                }
                if (entity.getUstgasan_uher() != 0){
                    statement.setDouble(stIndex++, entity.getUstgasan_uher());
                }
                if (entity.getUstgasan_honi() != 0){
                    statement.setDouble(stIndex++, entity.getUstgasan_honi());
                }
                if (entity.getUstgasan_ymaa() != 0){
                    statement.setDouble(stIndex++, entity.getUstgasan_ymaa());
                }
                if (entity.getUstgasan_busad() != 0){
                    statement.setDouble(stIndex++, entity.getUstgasan_busad());
                }
                if (entity.getHorio_ceer_eseh() != null && !entity.getHorio_ceer_eseh().isEmpty()){
                    statement.setString(stIndex++, entity.getHorio_ceer_eseh());
                }
                if (entity.getHorio_ceer_ognoo() != null){
                    statement.setDate(stIndex++, entity.getHorio_ceer_ognoo());
                }
                if (entity.getHorio_ceer_cucalsan_ognoo() != null){
                    statement.setDate(stIndex++, entity.getHorio_ceer_cucalsan_ognoo());
                }
                if (entity.getUstgasan_eseh() != null && !entity.getUstgasan_eseh().isEmpty()){
                    statement.setString(stIndex++, entity.getUstgasan_eseh());
                }
                if (entity.getUstgal_hiisen_ognoo() != null){
                    statement.setDate(stIndex++, entity.getUstgal_hiisen_ognoo());
                }
                if (entity.getDelflg() != null && !entity.getDelflg().isEmpty()){
                    statement.setString(stIndex++, entity.getDelflg());
                }
                if (entity.getRecord_date() != null){
                    statement.setDate(stIndex++, entity.getRecord_date());
                }
                if (entity.getActflg() != null && !entity.getActflg().isEmpty()){
                    statement.setString(stIndex++, entity.getActflg());
                }

                statement.setDate(stIndex++, entity.getMod_at());
                statement.setString(stIndex++, entity.getMod_by());
                statement.setLong(stIndex++, entity.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                    errorEntity.setErrText("Бичлэг амжилттай засагдлаа.");
                    errorEntity.setErrDesc("Бичлэг амжилттай засагдлаа.");
                    return errorEntity;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }


        errorEntity = new ErrorEntity(ErrorType.FATAL, Long.valueOf(1061));
        errorEntity.setErrText("Амжилтгүй боллоо. Дахин оролдоно уу.");
        errorEntity.setErrDesc("Амжилтгүй боллоо. Дахин оролдоно уу.");

        return errorEntity;
    }
    @Override
    public ErrorEntity exportReport(String file, GalzuuFormEntity entity) throws SQLException {
        ErrorEntity errorEntity = null;
        String  fileName = "hformOdorTutam";

        try (Connection connection = boneCP.getConnection()){
            String JRXML_PATH = Config.getJrxmlPath();
            String REPORT_PATH = Config.getReportPath();

            try {
                Map parameters = new HashMap();

                String reportTitle = "ГОЦ ХАЛДВАРТ ӨВЧНИЙ $$YEAR$$ ОНЫ МЭДЭЭ",
                        currentYear = "$$YEAR$$ он №....",
                        printDate = "$$YEAR$$ оны $$MONTH$$ сарын $$DAY$$";

                Calendar now = Calendar.getInstance();

                int year = now.get(Calendar.YEAR);
                int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
                int day = now.get(Calendar.DAY_OF_MONTH);

                printDate = printDate.replace("$$YEAR$$", String.valueOf(year));
                printDate = printDate.replace("$$MONTH$$", String.valueOf(month));
                printDate = printDate.replace("$$DAY$$", String.valueOf(day));

                currentYear = currentYear.replace("$$YEAR$$", String.valueOf(year));

                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(new Date(entity.getRecord_date().getTime()));

                reportTitle = reportTitle.replace("$$YEAR$$", String.valueOf(calendar1.get(Calendar.YEAR)));

                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(new Date(entity.getSearchDate().getTime()));

                if (calendar2.get(Calendar.MONTH) < 4){
                    reportTitle = reportTitle.replace("$$SEASON$$", "1");
                }
                else if (calendar2.get(Calendar.MONTH) < 7){
                    reportTitle = reportTitle.replace("$$SEASON$$", "2");
                }
                else if (calendar2.get(Calendar.MONTH) < 10){
                    reportTitle = reportTitle.replace("$$SEASON$$", "3");
                }
                else if (calendar2.get(Calendar.MONTH) <= 12){
                    reportTitle = reportTitle.replace("$$SEASON$$", "4");
                }

                parameters.put("start_date", entity.getRecord_date());
                parameters.put("end_date", entity.getSearchDate());
                parameters.put("citycode", (entity.getCitycode() != null && !entity.getCitycode().isEmpty()) ? entity.getCitycode(): null);
                parameters.put("sumcode", (entity.getSumcode() != null && !entity.getSumcode().isEmpty()) ? entity.getSumcode(): null);
                parameters.put("currentyear", currentYear);

                ICityService cityService = new CityServiceImpl(boneCP);
                City tmpCity = new City();
                tmpCity.setCode(entity.getCitycode());
                List<City> clist = cityService.selectAll(tmpCity);

                if (clist != null && clist.size() == 1) {
                    parameters.put("aimagner", clist.get(0).getCityname());
                } else {
                    parameters.put("aimagner", (entity.getAimagNer() == null) ? "" : entity.getAimagNer());
                }
                ISumService sumService = new SumServiceImpl(boneCP);
                Sum tmpSum = new Sum();
                tmpSum.setCitycode(entity.getCitycode());
                tmpSum.setSumcode(entity.getSumcode());
                List<Sum> sumList = sumService.selectAll(tmpSum);

                if (sumList != null && sumList.size() == 1){
                    parameters.put("sumner", sumList.get(0).getSumname());
                }
                else{
                    parameters.put("sumner", (entity.getSumNer() == null) ? "" : entity.getSumNer());
                }


                parameters.put("printdate", printDate);
                parameters.put("reporttitle", reportTitle);
                parameters.put("creby", (entity.getCre_by() != null && !entity.getCre_by().isEmpty()) ? entity.getCre_by(): null);

                PdfExcelUtil.generateReport(file, JRXML_PATH, REPORT_PATH, fileName, parameters, connection);

                System.out.println("File Generated");
            }catch (Exception ex){
                ex.printStackTrace();
            }
            finally {
                if (connection != null)connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            errorEntity = new ErrorEntity(ErrorType.INFO, Long.valueOf(1001));
            if(file.equals("pdf")){
                errorEntity.setErrText(fileName + ".pdf");
            }else if(file.equals("xls")){
                errorEntity.setErrText(fileName + ".xls");
            }
        }
        return errorEntity;
    }
}
