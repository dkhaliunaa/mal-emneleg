package com.hospital.entity;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;

/**
 * Created by 22cc3355 on 4/3/2017.
 */

@Entity
@Table(name="ROLE_PROFILE_TABLE")
public class Rpt {
    @Column(name="ROLE_ID")
    private String roleId;

    @Column(name="ROLE_DESC")
    private String roleDesc;

    @Column(name="ENTITY_CRE_FLG")
    private String entityCreFlg;

    @Column(name="DEL_FLG")
    private String delFlg;

    @Column(name="LCHG_USER_ID")
    private String lchgUserId;

    @Column(name="LCHG_TIME")
    private Date lchgTime;

    @Column(name="RCRE_USER_ID")
    private String rcheUserId;

    @Column(name="RCRE_TIME")
    private Date rcreTime;

    @Column(name="TS_CNT")
    private int tsCnt;

    public Rpt() {}

    public Rpt(String roleId,String roleDesc,String entityCreFlg,String delFlg,String lchgUserId, Date lchgTime, String rcheUserId, Date rcreTime, int tsCnt) {
        this.roleId = roleId;
        this.roleDesc = roleDesc;
        this.entityCreFlg = entityCreFlg;
        this.delFlg = delFlg;
        this.lchgUserId = lchgUserId;
        this.lchgTime = lchgTime;
        this.rcheUserId = rcheUserId;
        this.rcreTime = rcreTime;
        this.tsCnt = tsCnt;
    }
}
