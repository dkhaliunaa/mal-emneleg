package com.hospital.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Date;

/**
 * Created by 22cc3355 on 4/3/2017.
 */

@Entity
@Table(name="ROLE_PERM_INFO_TABLE")
public class Rpi {
    @Column(name="ROLE_ID")
    private String roleId;

    @Column(name="PERM_CODE")
    private String permCode;
    private String permName;

    @Column(name="BRNCH_CODE")
    private String brnchCode;
    private String brnchName;

    @Column(name="DEL_FLG")
    private String delFlg;

    @Column(name="LCHG_USER_ID")
    private String lchgUserId;

    @Column(name="LCHG_TIME")
    private Date lchgTime;

    @Column(name="RCRE_USER_ID")
    private String rcheUserId;

    @Column(name="RCRE_TIME")
    private Date rcreTime;

    public Rpi() {}

    public Rpi(String roleId,String permCode,String permName,String brnchCode,String brnchName,String delFlg,String lchgUserId, Date lchgTime, String rcheUserId, Date rcreTime) {
        this.roleId = roleId;
        this.permCode = permCode;
        this.permName = permName;
        this.brnchCode = brnchCode;
        this.brnchName = brnchName;
        this.delFlg = delFlg;
        this.lchgUserId = lchgUserId;
        this.lchgTime = lchgTime;
        this.rcheUserId = rcheUserId;
        this.rcreTime = rcreTime;
    }
}
