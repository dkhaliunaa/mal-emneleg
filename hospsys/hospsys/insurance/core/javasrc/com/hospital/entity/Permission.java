package com.hospital.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
/**
 * Created by 22cc3355 on 4/3/2017.
 */

@Entity
@Table(name="PERMISSION")
public class Permission {
    @Column(name="PERM_CODE")
    private String value;

    @Column(name="PERM_NAME")
    private String name;

    public Permission() {}

    public Permission(String value, String name) {
        this.value = value;
        this.name = name;
    }
}
