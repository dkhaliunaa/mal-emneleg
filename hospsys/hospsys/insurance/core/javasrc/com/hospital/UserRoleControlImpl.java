package com.hospital;


import com.model.*;
import com.mongodb.BasicDBObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 24be10264 on 12/17/2016.
 */
public class UserRoleControlImpl implements IUserRoleControl{
    private Connection connection;
    private ProjectErrorCode errorCode;
    private IProjectErrorCodeControl errorCodeControl;

    public UserRoleControlImpl(){}

    public UserRoleControlImpl(Connection connection){
        this.errorCode = new ProjectErrorCode();
        this.connection = connection;
        this.errorCodeControl = new ProjectErrorCodeControl(connection);
    }

    @Override
    public List<Role> roleSelect() throws SQLException, Exception{
        int index = 0;
        String strquery  = null;

        List<Role> roleList = null;
        try{
            PreparedStatement statement = null;
            ResultSet resultset = null;

            strquery = "SELECT * FROM rpt WHERE DEL_FLG='N' ";

            statement = connection.prepareStatement(strquery);

            resultset = statement.executeQuery();

            roleList = new ArrayList<>();

            while(resultset.next()){
                index ++;

                Role role = new Role();

                /*role.setId(resultset.getString("ROLE_ID"));
                role.setDescription(resultset.getString("ROLE_DESC"));*/

                roleList.add(role);
            }//end while

            if (resultset != null)resultset.close();
            if (statement!= null)statement.close();
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return roleList;
    }

    /**
     *
     * @param selObj
     * @return
     * @throws SQLException
     * @throws Exception
     */
    @Override
    public List<UserAndRole> select(UserAndRole selObj) throws SQLException, Exception {
        try{
            int index = 0;

            String strquery  = null;

            List<UserAndRole> userAndRoleList = new ArrayList<>();;
            PreparedStatement statement = null;
            ResultSet resultset = null;

            strquery = "select a.* from rpi a inner join EMPLOYEE b ON a.ROLE_ID = B.ROLE_ID WHERE ";

            /*if (selObj.get(UserAndRole.DOMAIN) != null && !selObj.getUsername().isEmpty()){
                strquery += " b.id = '"+ selObj.getUsername() +"' AND";
            }
            if (selObj.get(UserAndRole.ROLECODE) != null && !selObj.getRoleCode().isEmpty()){
                strquery += " a.PERM_CODE = '"+ selObj.getRoleCode() +"' AND";
            }*/

           /* strquery += " a.DEL_FLG = 'N'";

            statement = connection.prepareStatement(strquery);

            resultset = statement.executeQuery();

            while(resultset.next()){
                index ++;

                UserAndRole userAndRole = new UserAndRole();

                userAndRole.setUsername(selObj.getUsername());
                userAndRole.setRoleCode(resultset.getString("PERM_CODE"));
                userAndRole.setModAt(resultset.getDate("LCHG_TIME"));
                userAndRole.setModBy(resultset.getString("LCHG_USER_ID"));
                userAndRole.setCreAt(resultset.getDate("RCRE_TIME"));
                userAndRole.setCreBy(resultset.getString("RCRE_USER_ID"));

                userAndRole.put("index", index);

                userAndRoleList.add(userAndRole);
            }

            if (resultset != null)resultset.close();
            if (statement!= null)statement.close();*/

            /**
             * TODO::: FIXME eniig zasah
             */
            UserAndRole userAndRole = new UserAndRole();

            userAndRole.setUsername(selObj.getUsername());
            userAndRole.setRoleCode("001");
            userAndRole.put("index", index);

            userAndRoleList.add(userAndRole);

            return userAndRoleList;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }//end function

    @Override
    public RespList usersRoleListSelect(String domain, String sname, int pageindex) throws SQLException, Exception {
        RespList rlist = new RespList();


        return rlist;
    }

    @Override
    public BasicDBObject deleteUserRole(int id, String user) throws SQLException, Exception {
        boolean isSuccess = false;
        String strquery  = null;
        PreparedStatement statement = null;

        try{
            strquery = "DELETE FROM USERANDROLE WHERE id = ?";

            statement = connection.prepareStatement(strquery);

            statement.setInt(1, id);

            int res = statement.executeUpdate();

            if (res == 1){
                errorCode.setErrCode(1134);
                isSuccess = true;
            }else{
                errorCode.setErrCode(1135);
            }
        }catch (Exception ex){
            ex.printStackTrace();
            errorCode.setErrCode(1136);
        }
        finally {
            if (statement!= null)statement.close();
        }
        return errorCodeControl.getErrorCodeMessage(isSuccess, errorCode);
    }

    @Override
    public BasicDBObject userroleRegister(String domain, String role, String user) throws SQLException, Exception {

        boolean isSuccess = false;
        UserAndRole userAndRole = new UserAndRole();

        try {
            userAndRole.setUsername(domain);

            if (!userAndRole.getUsername().trim().equals("")){
                //update
                userAndRole.setCreBy(user);
                userAndRole.setModBy(user);
                userAndRole.setRoleCode(role);
                userAndRole.setUsername(domain);

                errorCode.setErrCode(0);
                isSuccess = this.update(userAndRole, errorCode);

                return errorCodeControl.getErrorCodeMessage(isSuccess, errorCode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            errorCode.setErrCode(1125);
        }
        return null;
    }

    private boolean update(UserAndRole userAndRole, ProjectErrorCode errorCode) throws  Exception{
        boolean isSuccess = false;
        String query = null;
        PreparedStatement statement = null;


        try{
            query = "UPDATE EMPLOYEE " +
                    "SET " +
                    "       ROLE_ID  = '"+userAndRole.getRoleCode()+"' " +
                    "WHERE  ID = '"+userAndRole.getUsername()+"'";

            statement = connection.prepareStatement(query);

            int insRows = statement.executeUpdate();

            if (insRows == 1){
                errorCode.setErrCode(1133);
                isSuccess = true;
            }else {
                errorCode.setErrCode(1132);
            }
        }catch (Exception ex){
            ex.printStackTrace();
            errorCode.setErrCode(1131);
        }finally {
            if (statement!= null)statement.close();
        }

        return isSuccess;
    }

    /**
     * Хэрэглэгчийн эрх шалгах
     *
     * @param roleCode
     * @param perm
     * @param userDomain
     * @param errorCode
     * @return
     * @throws SQLException
     */
    @Override
    public boolean checkRole(String roleCode, String perm, String userDomain, ProjectErrorCode errorCode) throws SQLException {
        boolean isSuccess = false;
        if (errorCode == null)
            return false;

        UserAndRole userAndRole = new UserAndRole();
        userAndRole.setUsername(userDomain);
        userAndRole.setRoleCode(roleCode);

        try {
            List<UserAndRole> userAndRoles = this.select(userAndRole);

            if (userAndRoles.size() == 1){
                return true;
            }
            else{
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            errorCode.setErrCode(1125);
        }

        return false;
    }

    @Override
    public boolean ContRole(String roleCode, String contId, String userDomain, ProjectErrorCode errorCode) throws SQLException {
        String query = "";
        PreparedStatement statement = null;
        ResultSet resultset = null;

        query = "select c.BRNCH,d.PARENT_ID,a.BRNCH_CODE FROM rpi a inner join permission b ON a.PERM_CODE = b.PERM_CODE inner join employee c ON a.ROLE_ID=c.ROLE_ID inner join BRANCH d ON c.BRNCH = D.ID where a.PERM_CODE='" + roleCode + "' AND a.DEL_FLG='N' AND C.ID = '"+userDomain+"'";
        // аль салбар нэгж хүртэл харах эрхийн утга байна.
        String brnch_code="";
        // салбарын дугаар байрлана.
        String branch = "";
        // дэд салбарын дугаар байрлана.
        String parent_id="";
        statement = connection.prepareStatement(query);

        resultset = statement.executeQuery();
        resultset.next();
        if (!resultset.getString("BRNCH_CODE").equals("")) {
            brnch_code = resultset.getString("BRNCH_CODE");
            branch = resultset.getString("BRNCH");
            parent_id = resultset.getString("PARENT_ID");
        }
        String where = "WHERE ISDELETE = 0 ";
        // All Branches
        if (brnch_code.equals("AL")) {
        }
        // Parent Branch
        if (brnch_code.equals("PB")) {
            where += " AND STR1 in (SELECT id FROM BRANCH WHERE PARENT_ID='" + parent_id + "' or ID='" + parent_id + "')";
        }
        // In Branch
        if (brnch_code.equals("IB")) {
            where += " AND STR1 = '" + branch + "' ";
        }
        // Only Mine
        if (brnch_code.equals("OM")) {
            if (userDomain != null && !userDomain.isEmpty()) {
                where += " AND CREATE_BY = '" + userDomain + "' ";
            }
        }

        if (contId.isEmpty()) {
            return true;
        }
        where += " AND R.CONTRACTID = '"+ contId +"'";

        query = "SELECT COUNT(*) TOTAL FROM REGCONTRACT R "+ where+" Order by CREATE_AT DESC";

        statement = connection.prepareStatement(query);

        resultset = statement.executeQuery();
        resultset.next();
        if (resultset.getString("TOTAL").equals("1")) {
            return true;
        }

        return false;
    }

    public String codeGeneration(ProjectErrorCode errorCode) throws Exception{
        //SELECT INSUDB.SEQ_PROREG.nextval CODE FROM dual;
        String strquery  = "SELECT INSUDB.SEQ_USERANDROLE.nextval CODE FROM dual";
        String seqCode = "";

        PreparedStatement statement = null;
        ResultSet resultset = null;

        try {
            statement = connection.prepareStatement(strquery);

            resultset = statement.executeQuery();

            while (resultset.next()) {
                seqCode = resultset.getString("CODE");
            }

            if (seqCode.isEmpty()){
                errorCode.setErrCode(1128);
            }
        }catch (Exception ex){
            ex.printStackTrace();
            errorCode.setErrCode(1128);
        }
        finally {
            if (resultset != null)resultset.close();
            if (statement!= null)statement.close();
        }

        return seqCode;
    }
    /**
     * Өнөөдрийн огноог авах
     * @return
     */
    private java.sql.Date getCurrentDatetime() {
        java.util.Date today = new java.util.Date();
        return new java.sql.Date(today.getTime());
    }
}
