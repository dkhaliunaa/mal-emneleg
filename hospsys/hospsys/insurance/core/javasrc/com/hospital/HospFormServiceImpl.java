package com.hospital;

import com.auth.HospService;
import com.enumclass.ErrorType;
import com.google.gson.internal.LinkedTreeMap;
import com.hospital.app.*;
import com.model.User;
import com.model.hos.*;
import com.rpc.RpcHandler;
import org.bson.types.ObjectId;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by 24be10264 on 9/27/2017.
 */
public class HospFormServiceImpl extends HospService implements IHospFormService {
    private ErrorEntity errorEntity = null;
    private  SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    public HospFormServiceImpl() {
        handler = new RpcHandler<>(this, IHospFormService.class);
    }

    @Override
    public List<GalzuuFormEntity> getForm01MainInfo(LinkedTreeMap criteria, String sessionId, String menuId) throws Exception {
        System.out.println("Hospital Service ::: get bag horoo data");

        try {
            IGalzuuFormService service = new GalzuuFormServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            GalzuuFormEntity entity = new GalzuuFormEntity();

            Iterator it = criteria.entrySet().iterator();

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                        if (pairs.getKey().equals(entity.ID)) {
                            String tmpId = "";
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                                if (pairs.getValue().toString().contains(".")) {
                                    tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                                }
                                entity.setId(Long.parseLong(String.valueOf(tmpId)));
                            }
                        }
                        else if (pairs.getKey().equals(entity.CITYCODE)) {
                            entity.setCitycode(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.SUMCODE)) {
                            entity.setSumcode(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.BAGNAME)) {
                            entity.setBagname(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.FRMKEY)) {
                            entity.setFrmkey(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.GAZARNER)) {
                            entity.setGazarner(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.GOLOMTTOO)) {
                            entity.setGolomttoo(Double.parseDouble((pairs.getValue() != null) ? pairs.getValue().toString() : "0"));
                        }
                        else if (pairs.getKey().equals(entity.MALCHINNER)) {
                            entity.setMalchinner(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.UVCHLOL_BURT_URH_HARI)) {
                            entity.setUvchlol_burt_urh_hari((String) pairs.getValue());
                        }
                        else if (pairs.getKey().equals(entity.COORDINATE_N)) {
                            entity.setCoordinate_n(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.COORDINATE_E)) {
                            entity.setCoordinate_e(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.DUUDLAGAOGNOO)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (dv != null && !dv.isEmpty()){
                                if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                                java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                                entity.setDuudlagaognoo(sqlDate);
                            }
                        }
                        else if (pairs.getKey().equals(entity.UVCHLOLEHELSEN_OGNOO)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (dv != null && !dv.isEmpty()){
                                if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                                java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                                entity.setUvchlolehelsen_ognoo(sqlDate);
                            }
                        }
                        else if (pairs.getKey().equals(entity.UVCHLOLBURT_OGNOO)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (dv != null && !dv.isEmpty()){
                                if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                                java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                                entity.setUvchlolburt_ognoo(sqlDate);
                            }
                        }
                        else if (pairs.getKey().equals(entity.LAB_BAT_OGNOO)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (dv != null && !dv.isEmpty()){
                                if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                                java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                                entity.setLab_bat_ognoo(sqlDate);
                            }
                        }
                        else if (pairs.getKey().equals(entity.LAB_BAT_HEVSHIL)) {
                            entity.setLab_bat_hevshil(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.SHARGA)) {
                            entity.setSharga(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.SHURDUN)) {
                            entity.setShurdun(String.valueOf(pairs.getValue()));
                        }
                        //Uvchilson
                        else if (pairs.getKey().equals(entity.UVCH_UHER)) {
                            entity.setUvch_uher(Double.parseDouble((pairs.getValue() != null) ? pairs.getValue().toString() : "0"));
                        }
                        else if (pairs.getKey().equals(entity.UVCH_HONI)) {
                            entity.setUvch_honi(Double.parseDouble((pairs.getValue() != null) ? pairs.getValue().toString() : "0"));
                        }
                        else if (pairs.getKey().equals(entity.UVCH_YMAA)) {
                            entity.setUvch_ymaa(Double.parseDouble((pairs.getValue() != null) ? pairs.getValue().toString() : "0"));
                        }
                        else if (pairs.getKey().equals(entity.UVCH_BUSAD)) {
                            entity.setUvch_busad(Double.parseDouble((pairs.getValue() != null) ? pairs.getValue().toString() : "0"));
                        }
                        //Uhsen
                        else if (pairs.getKey().equals(entity.UHSEN_UHER)) {
                            entity.setUhsen_uher(Double.parseDouble((pairs.getValue() != null) ? pairs.getValue().toString() : "0"));
                        }
                        else if (pairs.getKey().equals(entity.UHSEN_HONI)) {
                            entity.setUhsen_honi(Double.parseDouble((pairs.getValue() != null) ? pairs.getValue().toString() : "0"));
                        }
                        else if (pairs.getKey().equals(entity.UHSEN_YMAA)) {
                            entity.setUhsen_ymaa(Double.parseDouble((pairs.getValue() != null) ? pairs.getValue().toString() : "0"));
                        }
                        else if (pairs.getKey().equals(entity.UHSEN_BUSAD)) {
                            entity.setUhsen_busad(Double.parseDouble((pairs.getValue() != null) ? pairs.getValue().toString() : "0"));
                        }
                        //Ustgasan
                        else if (pairs.getKey().equals(entity.USTGASAN_UHER)) {
                            entity.setUstgasan_uher(Double.parseDouble((pairs.getValue() != null) ? pairs.getValue().toString() : "0"));
                        }
                        else if (pairs.getKey().equals(entity.USTGASAN_HONI)) {
                            entity.setUstgasan_honi(Double.parseDouble((pairs.getValue() != null) ? pairs.getValue().toString() : "0"));
                        }
                        else if (pairs.getKey().equals(entity.USTGASAN_YMAA)) {
                            entity.setUstgasan_ymaa(Double.parseDouble((pairs.getValue() != null) ? pairs.getValue().toString() : "0"));
                        }
                        else if (pairs.getKey().equals(entity.USTGASAN_BUSAD)) {
                            entity.setUstgasan_busad(Double.parseDouble((pairs.getValue() != null) ? pairs.getValue().toString() : "0"));
                        }

                        else if (pairs.getKey().equals(entity.HORIO_CEER_ESEH)) {
                            entity.setHorio_ceer_eseh(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.HORIO_CEER_OGNOO)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setHorio_ceer_ognoo(sqlDate);
                        }
                        else if (pairs.getKey().equals(entity.HORIO_CEER_CUCALSAN_OGNOO)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (dv != null && !dv.isEmpty()){
                                if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                                java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                                entity.setHorio_ceer_cucalsan_ognoo(sqlDate);
                            }
                        }
                        else if (pairs.getKey().equals(entity.USTGASAN_ESEH)) {
                            entity.setUstgasan_eseh(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.USTGAL_HIISEN_OGNOO)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (dv != null && !dv.isEmpty()){
                                if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                                java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                                entity.setUstgal_hiisen_ognoo(sqlDate);
                            }
                        }
                        else  if (pairs.getKey().equals(entity.RECORD_DATE)){
                            String dv = String.valueOf(pairs.getValue());
                            if (dv != null && !dv.isEmpty()){
                                if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                                java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                                entity.setRecord_date(sqlDate);
                            }
                        }
                        else  if (pairs.getKey().equals(entity.SEARCH_DATE)){
                            String dv = String.valueOf(pairs.getValue());
                            if (dv != null && !dv.isEmpty()){
                                if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                                java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                                entity.setSearchDate(sqlDate);
                            }
                        }
                        else  if (pairs.getKey().equals(entity.ACTFLG)){
                            String dv = String.valueOf(pairs.getValue());
                            entity.setActflg(dv);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            entity.setDelflg("N");
            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                entity.setCre_by(user.getUsername());
            }

            return service.getListData(entity);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public ErrorEntity setForm01MainInfo(LinkedTreeMap insdata, String btnId, String sessionId, String menuId) throws Exception {
        System.out.println("Hospital Service ::: get bag horoo data");

        try {
            String command = "EDT";

            IGalzuuFormService service = new GalzuuFormServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            GalzuuFormEntity entity = new GalzuuFormEntity();

            Iterator it = insdata.entrySet().iterator();

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                        if (pairs.getKey().equals(entity.ID)) {
                            String tmpId = "";
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                                if (pairs.getValue().toString().contains(".")) {
                                    tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                                }
                                entity.setId(Long.parseLong(String.valueOf(tmpId)));
                            }
                        }
                        else if (pairs.getKey().equals(entity.CITYCODE)) {
                            entity.setCitycode(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.SUMCODE)) {
                            entity.setSumcode(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.BAGNAME)) {
                            entity.setBagname(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.FRMKEY)) {
                            entity.setFrmkey(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.GAZARNER)) {
                            entity.setGazarner(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.GOLOMTTOO)) {
                            entity.setGolomttoo(Double.parseDouble((pairs.getValue() != null) ? pairs.getValue().toString() : "0"));
                        }
                        else if (pairs.getKey().equals(entity.MALCHINNER)) {
                            entity.setMalchinner(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.UVCHLOL_BURT_URH_HARI)) {
                            entity.setUvchlol_burt_urh_hari((String) pairs.getValue());
                        }
                        else if (pairs.getKey().equals(entity.COORDINATE_N)) {
                            entity.setCoordinate_n(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.COORDINATE_E)) {
                            entity.setCoordinate_e(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.DUUDLAGAOGNOO)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (dv != null && !dv.isEmpty()){
                                if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                                java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                                entity.setDuudlagaognoo(sqlDate);
                            }
                        }
                        else if (pairs.getKey().equals(entity.UVCHLOLEHELSEN_OGNOO)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (dv != null && !dv.isEmpty()){
                                if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                                java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                                entity.setUvchlolehelsen_ognoo(sqlDate);
                            }
                        }
                        else if (pairs.getKey().equals(entity.UVCHLOLBURT_OGNOO)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (dv != null && !dv.isEmpty()){
                                if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                                java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                                entity.setUvchlolburt_ognoo(sqlDate);
                            }
                        }
                        else if (pairs.getKey().equals(entity.LAB_BAT_OGNOO)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (dv != null && !dv.isEmpty()){
                                if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                                java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                                entity.setLab_bat_ognoo(sqlDate);
                            }
                        }
                        else if (pairs.getKey().equals(entity.LAB_BAT_HEVSHIL)) {
                            entity.setLab_bat_hevshil(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.SHARGA)) {
                            entity.setSharga(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.SHURDUN)) {
                            entity.setShurdun(String.valueOf(pairs.getValue()));
                        }
                        //Uvchilson
                        else if (pairs.getKey().equals(entity.UVCH_UHER)) {
                            entity.setUvch_uher(Double.parseDouble((pairs.getValue() != null) ? pairs.getValue().toString() : "0"));
                        }
                        else if (pairs.getKey().equals(entity.UVCH_HONI)) {
                            entity.setUvch_honi(Double.parseDouble((pairs.getValue() != null) ? pairs.getValue().toString() : "0"));
                        }
                        else if (pairs.getKey().equals(entity.UVCH_YMAA)) {
                            entity.setUvch_ymaa(Double.parseDouble((pairs.getValue() != null) ? pairs.getValue().toString() : "0"));
                        }
                        else if (pairs.getKey().equals(entity.UVCH_BUSAD)) {
                            entity.setUvch_busad(Double.parseDouble((pairs.getValue() != null) ? pairs.getValue().toString() : "0"));
                        }
                        //Uhsen
                        else if (pairs.getKey().equals(entity.UHSEN_UHER)) {
                            entity.setUhsen_uher(Double.parseDouble((pairs.getValue() != null) ? pairs.getValue().toString() : "0"));
                        }
                        else if (pairs.getKey().equals(entity.UHSEN_HONI)) {
                            entity.setUhsen_honi(Double.parseDouble((pairs.getValue() != null) ? pairs.getValue().toString() : "0"));
                        }
                        else if (pairs.getKey().equals(entity.UHSEN_YMAA)) {
                            entity.setUhsen_ymaa(Double.parseDouble((pairs.getValue() != null) ? pairs.getValue().toString() : "0"));
                        }
                        else if (pairs.getKey().equals(entity.UHSEN_BUSAD)) {
                            entity.setUhsen_busad(Double.parseDouble((pairs.getValue() != null) ? pairs.getValue().toString() : "0"));
                        }
                        //Ustgasan
                        else if (pairs.getKey().equals(entity.USTGASAN_UHER)) {
                            entity.setUstgasan_uher(Double.parseDouble((pairs.getValue() != null) ? pairs.getValue().toString() : "0"));
                        }
                        else if (pairs.getKey().equals(entity.USTGASAN_HONI)) {
                            entity.setUstgasan_honi(Double.parseDouble((pairs.getValue() != null) ? pairs.getValue().toString() : "0"));
                        }
                        else if (pairs.getKey().equals(entity.USTGASAN_YMAA)) {
                            entity.setUstgasan_ymaa(Double.parseDouble((pairs.getValue() != null) ? pairs.getValue().toString() : "0"));
                        }
                        else if (pairs.getKey().equals(entity.USTGASAN_BUSAD)) {
                            entity.setUstgasan_busad(Double.parseDouble((pairs.getValue() != null) ? pairs.getValue().toString() : "0"));
                        }

                        else if (pairs.getKey().equals(entity.HORIO_CEER_ESEH)) {
                            entity.setHorio_ceer_eseh(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.HORIO_CEER_OGNOO)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setHorio_ceer_ognoo(sqlDate);
                        }
                        else if (pairs.getKey().equals(entity.HORIO_CEER_CUCALSAN_OGNOO)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (dv != null && !dv.isEmpty()){
                                if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                                java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                                entity.setHorio_ceer_cucalsan_ognoo(sqlDate);
                            }
                        }
                        else if (pairs.getKey().equals(entity.USTGASAN_ESEH)) {
                            entity.setUstgasan_eseh(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.USTGAL_HIISEN_OGNOO)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (dv != null && !dv.isEmpty()){
                                if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                                java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                                entity.setUstgal_hiisen_ognoo(sqlDate);
                            }
                        }
                        else  if (pairs.getKey().equals(entity.RECORD_DATE)){
                            String dv = String.valueOf(pairs.getValue());
                            if (dv != null && !dv.isEmpty()){
                                if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                                java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                                entity.setRecord_date(sqlDate);
                            }
                        }
                        else  if (pairs.getKey().equals(entity.ACTFLG)){
                            String dv = String.valueOf(pairs.getValue());
                            entity.setActflg(dv);
                        }
                        else if (pairs.getKey().toString().equalsIgnoreCase("command")) {
                            command = String.valueOf(pairs.getValue());
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            java.util.Date utilDate = new java.util.Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            entity.setMod_at(sqlDate);
            entity.setMod_by(user.getUsername());

            if (command.equalsIgnoreCase("INS")) {
                String frmkey = new ObjectId().toString();
                entity.setFrmkey(frmkey);
                entity.setDelflg("N");
                entity.setActflg("Y");
                entity.setCre_by(user.getUsername());
                entity.setCre_at(sqlDate);

                errorEntity = service.insertNewData(entity);
            }
            else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL") ||
                    command.equalsIgnoreCase("ACT")) {
                if (command.equalsIgnoreCase("DEL")) {
                    entity.setDelflg("Y");
                } else if (command.equalsIgnoreCase("ACT")) {

                }
                errorEntity = service.updateData(entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return errorEntity;
    }

    //    Begin-form51
    @Override
    public List<Form51Entity> getForm51Data(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 51 data");

        try {
            IForm51Service form51Service = new Form51ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form51Entity form51Entity = new Form51Entity();

            Iterator it = criteria.entrySet().iterator();


            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form51Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            form51Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(form51Entity.AIMAG)) {
                        form51Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form51Entity.SUM)) {
                        form51Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form51Entity.SUMBAGNER)) {
                        form51Entity.setSumBagNer(String.valueOf(pairs.getValue()));
//                        try{
//                            form14Entity.setUzlegToo(Long.valueOf(pairs.getValue().toString()));
//                        }catch (NumberFormatException e){
//                            form14Entity.setUzlegToo(Long.valueOf("0"));
//                        }
                    } else if (pairs.getKey().equals(form51Entity.NIITMALTOO)) {
                        try {
                            form51Entity.setNiitMalToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form51Entity.setNiitMalToo(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form51Entity.BOGTOO)) {
                        try {
                            form51Entity.setBogToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form51Entity.setBogToo(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form51Entity.BODTOO)) {
                        try {
                            form51Entity.setBodToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form51Entity.setBodToo(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form51Entity.TOLTOO)) {
                        try {
                            form51Entity.setTolToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form51Entity.setTolToo(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form51Entity.EHNIIULDEGDEL)) {
                        try {
                            form51Entity.setEhniiUldegdel(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form51Entity.setEhniiUldegdel(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form51Entity.ORLOGO)) {
                        try {
                            form51Entity.setOrlogo(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form51Entity.setOrlogo(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form51Entity.ZARLAGA)) {
                        try {
                            form51Entity.setZarlaga(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form51Entity.setZarlaga(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form51Entity.JILEULDEGDEL)) {
                        try {
                            form51Entity.setJilEUldegdel(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form51Entity.setJilEUldegdel(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    }
                    else  if (pairs.getKey().equals(form51Entity.RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form51Entity.setRecordDate(sqlDate);
                    }
                    else  if (pairs.getKey().equals(form51Entity.SEARCH_RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form51Entity.setSearchRecordDate(sqlDate);
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                form51Entity.setCreby(user.getUsername());
            }

            return form51Service.getListData(form51Entity);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public ErrorEntity setForm51Data(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 51 data");

        try {
            String command = "EDT";
            IForm51Service form51Service = new Form51ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form51Entity form51Entity = new Form51Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form51Entity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                            if (pairs.getValue().toString().contains(".")) {
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            form51Entity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    }
                    else if (pairs.getKey().equals(form51Entity.AIMAG)) {
                        form51Entity.setAimag(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(form51Entity.SUM)) {
                        form51Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form51Entity.SUMBAGNER)) {
                        form51Entity.setSumBagNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form51Entity.NIITMALTOO)) {
                        try {
                            form51Entity.setNiitMalToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form51Entity.setNiitMalToo(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form51Entity.BOGTOO)) {
                        try {
                            form51Entity.setBogToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form51Entity.setBogToo(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form51Entity.BODTOO)) {
                        try {
                            form51Entity.setBodToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form51Entity.setBodToo(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form51Entity.TOLTOO)) {
                        try {
                            form51Entity.setTolToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form51Entity.setTolToo(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form51Entity.EHNIIULDEGDEL)) {
                        try {
                            form51Entity.setEhniiUldegdel(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form51Entity.setEhniiUldegdel(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form51Entity.ORLOGO)) {
                        try {
                            form51Entity.setOrlogo(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form51Entity.setOrlogo(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form51Entity.ZARLAGA)) {
                        try {
                            form51Entity.setZarlaga(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form51Entity.setZarlaga(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form51Entity.JILEULDEGDEL)) {
                        try {
                            form51Entity.setJilEUldegdel(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form51Entity.setJilEUldegdel(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().toString().equalsIgnoreCase("command")) {
                        command = String.valueOf(pairs.getValue());
                    }
                    else  if (pairs.getKey().equals(form51Entity.RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (!dv.isEmpty() && dv.length() >= 8){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form51Entity.setRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while
            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            form51Entity.setModAt(sqlDate);
            form51Entity.setModby(user.getUsername());

            if (command.equalsIgnoreCase("INS"))
            {
                form51Entity.setDelflg("N");
                form51Entity.setActflg("Y");
                if (user != null && (user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                        || user.getRoleCode().equalsIgnoreCase("ADMIN"))){
                    //Aimag songoh shaardlagagui
                }else{
                    form51Entity.setAimag(user.getCity());
                }
                //form51Entity.setSum(user.getSum());
                form51Entity.setCreby(user.getUsername());
                form51Entity.setCreat(sqlDate);
                if (form51Entity.getRecordDate() == null) form51Entity.setCreat(sqlDate);

                errorEntity = form51Service.insertNewData(form51Entity);
            } else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL") ||
                    command.equalsIgnoreCase("ACT")) {
                if (command.equalsIgnoreCase("DEL")) {
                    form51Entity.setDelflg("Y");
                } else if (command.equalsIgnoreCase("ACT")) {
                    form51Entity.setActflg("N");
                }
                errorEntity = form51Service.updateData(form51Entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return errorEntity;
    }
    //    End-form51

    //    Begin-form52
    @Override
    public List<Form52Entity> getForm52Data(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 52 data");

        try {
            IForm52Service form52Service = new Form52ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form52Entity form52Entity = new Form52Entity();

            Iterator it = criteria.entrySet().iterator();


            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form52Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            form52Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(form52Entity.AIMAG)) {
                        form52Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form52Entity.SUM)) {
                        form52Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form52Entity.UVCHINNER)) {
                        form52Entity.setUvchinNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form52Entity.BUGDUVCHILSEN)) {
                        try {
                            form52Entity.setBugdUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setBugdUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.BUGDUHSEN)) {
                        try {
                            form52Entity.setBugdUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setBugdUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.BUGDEDGERSEN)) {
                        try {
                            form52Entity.setBugdEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setBugdEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.TEMEEUVCHILSEN)) {
                        try {
                            form52Entity.setTemeeUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setTemeeUvchilsen(Long.valueOf("0"));
                        }
                    }
                    else if (pairs.getKey().equals(form52Entity.TEMEEUHSEN)) {
                        try {
                            form52Entity.setTemeeUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setTemeeUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.TEMEEEDGERSEN)) {
                        try {
                            form52Entity.setTemeeEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setTemeeEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.ADUUUVCHILSEN)) {
                        try {
                            form52Entity.setAduuUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setAduuUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.ADUUUHSEN)) {
                        try {
                            form52Entity.setAduuUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setAduuUhsen(Long.valueOf("0"));
                        }

                    }
                    else if (pairs.getKey().equals(form52Entity.ADUUEDGERSEN)) {
                        try {
                            form52Entity.setAduuEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setAduuEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.UHERUVCHILSEN)) {
                        try {
                            form52Entity.setUherUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setUherUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.UHERUHSEN)) {
                        try {
                            form52Entity.setUherUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setUherUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.UHEREDGERSEN)) {
                        try {
                            form52Entity.setUherEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setUherEdgersen(Long.valueOf("0"));
                        }

                    }
                    else if (pairs.getKey().equals(form52Entity.HONIUVCHILSEN)) {
                        try {
                            form52Entity.setHoniUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setHoniUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.HONIUHSEN)) {
                        try {
                            form52Entity.setHoniUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setHoniUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.HONIEDGERSEN)) {
                        try {
                            form52Entity.setHoniEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setHoniEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.YAMAAUVCHILSEN)) {
                        try {
                            form52Entity.setYamaaUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setYamaaUvchilsen(Long.valueOf("0"));
                        }

                    }
                    else if (pairs.getKey().equals(form52Entity.YAMAAUHSEN)) {
                        try {
                            form52Entity.setYamaaUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setYamaaUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.YAMAAEDGERSEN)) {
                        try {
                            form52Entity.setYamaaEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setYamaaEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.BUSADUVCHILSEN)) {
                        try {
                            form52Entity.setBusadUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setBusadUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.BUSADUHSEN)) {
                        try {
                            form52Entity.setBusadUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setBusadUhsen(Long.valueOf("0"));
                        }

                    }
                    else if (pairs.getKey().equals(form52Entity.BUSADEDGERSEN)) {
                        try {
                            form52Entity.setBusadEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setBusadEdgersen(Long.valueOf("0"));
                        }

                    }
                    else  if (pairs.getKey().equals(form52Entity.RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form52Entity.setRecordDate(sqlDate);
                    }
                    else  if (pairs.getKey().equals(form52Entity.SEARCH_RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form52Entity.setSearchRecordDate(sqlDate);
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                form52Entity.setCreby(user.getUsername());
                if(form52Entity.getAimag() == null)form52Entity.setAimag(user.getCity());
            }
            //if(form08Entity.getSum() == null)form08Entity.setSum(user.getSum());

            form52Entity.setDelflg("N");
            form52Entity.setActflg("Y");

            return form52Service.getListData(form52Entity);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public ErrorEntity setForm52Data(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 52 data");

        try {
            String command = "EDT";
            IForm52Service form52Service = new Form52ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form52Entity form52Entity = new Form52Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form52Entity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                            if (pairs.getValue().toString().contains(".")) {
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            form52Entity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    }
                    else if (pairs.getKey().equals(form52Entity.AIMAG)) {
                        form52Entity.setAimag(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(form52Entity.SUM)) {
                        form52Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form52Entity.UVCHINNER)) {
                        form52Entity.setUvchinNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form52Entity.BUGDUVCHILSEN)) {
                        try {
                            String tmpId = "";
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                                if (pairs.getValue().toString().contains(".")) {
                                    tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                                }
                                else{
                                    tmpId = pairs.getValue().toString();
                                }
                                form52Entity.setBugdUvchilsen(Long.parseLong(String.valueOf(tmpId)));
                            }
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                            form52Entity.setBugdUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.BUGDUHSEN)) {
                        try {
                            String tmpId = "";
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                                if (pairs.getValue().toString().contains(".")) {
                                    tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                                }
                                else{
                                    tmpId = pairs.getValue().toString();
                                }
                                form52Entity.setBugdUhsen(Long.parseLong(String.valueOf(tmpId)));
                            }
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                            form52Entity.setBugdUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.BUGDEDGERSEN)) {
                        try {
                            String tmpId = "";
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                                if (pairs.getValue().toString().contains(".")) {
                                    tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                                }else{
                                    tmpId = pairs.getValue().toString();
                                }
                                form52Entity.setBugdEdgersen(Long.parseLong(String.valueOf(tmpId)));
                            }
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                            form52Entity.setBugdEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.TEMEEUVCHILSEN)) {
                        try {
                            form52Entity.setTemeeUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setTemeeUvchilsen(Long.valueOf("0"));
                        }
                    }
                    else if (pairs.getKey().equals(form52Entity.TEMEEUHSEN)) {
                        try {
                            form52Entity.setTemeeUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setTemeeUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.TEMEEEDGERSEN)) {
                        try {
                            form52Entity.setTemeeEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setTemeeEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.ADUUUVCHILSEN)) {
                        try {
                            form52Entity.setAduuUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setAduuUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.ADUUUHSEN)) {
                        try {
                            form52Entity.setAduuUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setAduuUhsen(Long.valueOf("0"));
                        }

                    }
                    else if (pairs.getKey().equals(form52Entity.ADUUEDGERSEN)) {
                        try {
                            form52Entity.setAduuEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setAduuEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.UHERUVCHILSEN)) {
                        try {
                            form52Entity.setUherUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setUherUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.UHERUHSEN)) {
                        try {
                            form52Entity.setUherUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setUherUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.UHEREDGERSEN)) {
                        try {
                            form52Entity.setUherEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setUherEdgersen(Long.valueOf("0"));
                        }

                    }
                    else if (pairs.getKey().equals(form52Entity.HONIUVCHILSEN)) {
                        try {
                            form52Entity.setHoniUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setHoniUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.HONIUHSEN)) {
                        try {
                            form52Entity.setHoniUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setHoniUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.HONIEDGERSEN)) {
                        try {
                            form52Entity.setHoniEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setHoniEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.YAMAAUVCHILSEN)) {
                        try {
                            form52Entity.setYamaaUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setYamaaUvchilsen(Long.valueOf("0"));
                        }

                    }
                    else if (pairs.getKey().equals(form52Entity.YAMAAUHSEN)) {
                        try {
                            form52Entity.setYamaaUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setYamaaUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.YAMAAEDGERSEN)) {
                        try {
                            form52Entity.setYamaaEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setYamaaEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.BUSADUVCHILSEN)) {
                        try {
                            form52Entity.setBusadUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setBusadUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.BUSADUHSEN)) {
                        try {
                            form52Entity.setBusadUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setBusadUhsen(Long.valueOf("0"));
                        }

                    }
                    else if (pairs.getKey().equals(form52Entity.BUSADEDGERSEN)) {
                        try {
                            form52Entity.setBusadEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setBusadEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().toString().equalsIgnoreCase("command")) {
                        command = String.valueOf(pairs.getValue());
                    }
                    else  if (pairs.getKey().equals(form52Entity.RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (!dv.isEmpty() && dv.length() >= 8){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form52Entity.setRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while
            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            form52Entity.setModAt(sqlDate);
            form52Entity.setModby(user.getUsername());

            if (command.equalsIgnoreCase("INS"))
            {
                form52Entity.setDelflg("N");
                form52Entity.setActflg("Y");

                if (user != null && (user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                        || user.getRoleCode().equalsIgnoreCase("ADMIN"))){
                    //Aimag songoh shaardlagagui
                }else{
                    form52Entity.setAimag(user.getCity());
                }

                //form52Entity.setSum(user.getSum());

                form52Entity.setCreby(user.getUsername());
                form52Entity.setCreat(sqlDate);
                if (form52Entity.getRecordDate() == null) form52Entity.setCreat(sqlDate);

                errorEntity = form52Service.insertNewData(form52Entity);
            } else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL") ||
                    command.equalsIgnoreCase("ACT")) {
                if (command.equalsIgnoreCase("DEL")) {
                    form52Entity.setDelflg("Y");
                } else if (command.equalsIgnoreCase("ACT")) {
                    form52Entity.setActflg("N");
                }
                errorEntity = form52Service.updateData(form52Entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return errorEntity;
    }
    //    End-form52


    //    Begin-form53
    @Override
    public List<Form53Entity> getForm53Data(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 53 data");

        try {
            IForm53Service form53Service = new Form53ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form53Entity form53Entity = new Form53Entity();

            Iterator it = criteria.entrySet().iterator();


            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form53Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            form53Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(form53Entity.AIMAG)) {
                        form53Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form53Entity.SUM)) {
                        form53Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form53Entity.UVCHINNER)) {
                        form53Entity.setUvchinNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form53Entity.BUGDUVCHILSEN)) {
                        try {
                            form53Entity.setBugdUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBugdUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.BUGDUHSEN)) {
                        try {
                            form53Entity.setBugdUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBugdUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.BUGDEDGERSEN)) {
                        try {
                            form53Entity.setBugdEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBugdEdgersen(Long.valueOf("0"));
                        }

                    }else if (pairs.getKey().equals(form53Entity.BOTGOUVCHILSEN)) {
                        try {
                            form53Entity.setBotgoUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBotgoUvchilsen(Long.valueOf("0"));
                        }
                    }
                    else if (pairs.getKey().equals(form53Entity.BOTGOUHSEN)) {
                        try {
                            form53Entity.setBotgoUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBotgoUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.BOTGOEDGERSEN)) {
                        try {
                            form53Entity.setBotgoEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBotgoEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.UNAGAUVCHILSEN)) {
                        try {
                            form53Entity.setUnagaUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setUnagaUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.UNAGAUHSEN)) {
                        try {
                            form53Entity.setUnagaUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setUnagaUhsen(Long.valueOf("0"));
                        }

                    }
                    else if (pairs.getKey().equals(form53Entity.UNAGAEDGERSEN)) {
                        try {
                            form53Entity.setUnagaEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setUnagaEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.TUGALUVCHILSEN)) {
                        try {
                            form53Entity.setTugalUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setTugalUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.TUGALUHSEN)) {
                        try {
                            form53Entity.setTugalUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setTugalUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.TUGALEDGERSEN)) {
                        try {
                            form53Entity.setTugalEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setTugalEdgersen(Long.valueOf("0"));
                        }

                    }
                    else if (pairs.getKey().equals(form53Entity.HURGAUVCHILSEN)) {
                        try {
                            form53Entity.setHurgaUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setHurgaUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.HURGAUHSEN)) {
                        try {
                            form53Entity.setHurgaUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setHurgaUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.HURGAEDGERSEN)) {
                        try {
                            form53Entity.setHurgaEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setHurgaEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.ISHIGUVCHILSEN)) {
                        try {
                            form53Entity.setIshigUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setIshigUvchilsen(Long.valueOf("0"));
                        }

                    }
                    else if (pairs.getKey().equals(form53Entity.ISHIGUHSEN)) {
                        try {
                            form53Entity.setIshigUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setIshigUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.ISHIGEDGERSEN)) {
                        try {
                            form53Entity.setIshigEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setIshigEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.BUSADUVCHILSEN)) {
                        try {
                            form53Entity.setBusadUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBusadUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.BUSADUHSEN)) {
                        try {
                            form53Entity.setBusadUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBusadUhsen(Long.valueOf("0"));
                        }

                    }
                    else if (pairs.getKey().equals(form53Entity.BUSADEDGERSEN)) {
                        try {
                            form53Entity.setBusadEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBusadEdgersen(Long.valueOf("0"));
                        }

                    }
                    else  if (pairs.getKey().equals(form53Entity.RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form53Entity.setRecordDate(sqlDate);
                    }
                    else  if (pairs.getKey().equals(form53Entity.SEARCH_RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form53Entity.setSearchRecordDate(sqlDate);
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                form53Entity.setCreby(user.getUsername());
            }

            return form53Service.getListData(form53Entity);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public ErrorEntity setForm53Data(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 53 data");

        try {
            String command = "EDT";
            IForm53Service form53Service = new Form53ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form53Entity form53Entity = new Form53Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form53Entity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                            if (pairs.getValue().toString().contains(".")) {
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            form53Entity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    } else if (pairs.getKey().equals(form53Entity.SUM)) {
                        form53Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form53Entity.UVCHINNER)) {
                        form53Entity.setUvchinNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form53Entity.BUGDUVCHILSEN)) {
                        try {
                            form53Entity.setBugdUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBugdUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.BUGDUHSEN)) {
                        try {
                            form53Entity.setBugdUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBugdUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.BUGDEDGERSEN)) {
                        try {
                            form53Entity.setBugdEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBugdEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.BOTGOUVCHILSEN)) {
                        try {
                            form53Entity.setBotgoUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBotgoUvchilsen(Long.valueOf("0"));
                        }
                    }
                    else if (pairs.getKey().equals(form53Entity.BOTGOUHSEN)) {
                        try {
                            form53Entity.setBotgoUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBotgoUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.BOTGOEDGERSEN)) {
                        try {
                            form53Entity.setBotgoEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBotgoEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.UNAGAUVCHILSEN)) {
                        try {
                            form53Entity.setUnagaUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setUnagaUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.UNAGAUHSEN)) {
                        try {
                            form53Entity.setUnagaUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setUnagaUhsen(Long.valueOf("0"));
                        }

                    }
                    else if (pairs.getKey().equals(form53Entity.UNAGAEDGERSEN)) {
                        try {
                            form53Entity.setUnagaEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setUnagaEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.TUGALUVCHILSEN)) {
                        try {
                            form53Entity.setTugalUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setTugalUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.TUGALUHSEN)) {
                        try {
                            form53Entity.setTugalUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setTugalUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.TUGALEDGERSEN)) {
                        try {
                            form53Entity.setTugalEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setTugalEdgersen(Long.valueOf("0"));
                        }

                    }
                    else if (pairs.getKey().equals(form53Entity.HURGAUVCHILSEN)) {
                        try {
                            form53Entity.setHurgaUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setHurgaUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.HURGAUHSEN)) {
                        try {
                            form53Entity.setHurgaUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setHurgaUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.HURGAEDGERSEN)) {
                        try {
                            form53Entity.setHurgaEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setHurgaEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.ISHIGUVCHILSEN)) {
                        try {
                            form53Entity.setIshigUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setIshigUvchilsen(Long.valueOf("0"));
                        }

                    }
                    else if (pairs.getKey().equals(form53Entity.ISHIGUHSEN)) {
                        try {
                            form53Entity.setIshigUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setIshigUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.ISHIGEDGERSEN)) {
                        try {
                            form53Entity.setIshigEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setIshigEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.BUSADUVCHILSEN)) {
                        try {
                            form53Entity.setBusadUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBusadUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.BUSADUHSEN)) {
                        try {
                            form53Entity.setBusadUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBusadUhsen(Long.valueOf("0"));
                        }

                    }
                    else if (pairs.getKey().equals(form53Entity.BUSADEDGERSEN)) {
                        try {
                            form53Entity.setBusadEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBusadEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().toString().equalsIgnoreCase("command")) {
                        command = String.valueOf(pairs.getValue());
                    }
                    else  if (pairs.getKey().equals(form53Entity.RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (!dv.isEmpty() && dv.length() >= 8){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form53Entity.setRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while
            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            form53Entity.setModAt(sqlDate);
            form53Entity.setModby(user.getUsername());

            if (command.equalsIgnoreCase("INS"))
            {
                form53Entity.setDelflg("N");
                form53Entity.setActflg("Y");
                form53Entity.setAimag(user.getCity());
                form53Entity.setSum(user.getSum());
                form53Entity.setCreby(user.getUsername());
                form53Entity.setCreat(sqlDate);
                if (form53Entity.getRecordDate() == null) form53Entity.setCreat(sqlDate);

                errorEntity = form53Service.insertNewData(form53Entity);
            } else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL") ||
                    command.equalsIgnoreCase("ACT")) {
                if (command.equalsIgnoreCase("DEL")) {
                    form53Entity.setDelflg("Y");
                } else if (command.equalsIgnoreCase("ACT")) {
                    form53Entity.setActflg("N");
                }
                errorEntity = form53Service.updateData(form53Entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return errorEntity;
    }
    //    End-form53

    // Begin Shinj Temdeg Medeelel
    @Override
    public List<GalzuuShTFormEntity> getForm01ShTemdegInfo(LinkedTreeMap insdata, String sessionId, String menuId) throws Exception {
        System.out.println("Hospital Service ::: get shinj temdeg data");

        try {
            String command = "EDT";

            IGalzuuShTFormService service = new GalzuuShTFormServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            GalzuuShTFormEntity entity = new GalzuuShTFormEntity();

            Iterator it = insdata.entrySet().iterator();

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                        if (pairs.getKey().equals(entity.ID)) {
                            String tmpId = "";
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                                if (pairs.getValue().toString().contains(".")) {
                                    tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                                }
                                entity.setId(Long.parseLong(String.valueOf(tmpId)));
                            }
                        }
                        else if (pairs.getKey().equals(entity.CITYCODE)) {
                            entity.setCitycode(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.SUMCODE)) {
                            entity.setSumcode(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.BAGCODE)) {
                            entity.setBagcode(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.MALCHINNER)) {
                            entity.setMalchinner(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.HALUURSAN)) {
                            entity.setHaluursan(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.HELEN_D_ULHII)) {
                            entity.setHelen_d_ulhii(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.ZAVJ_D_ULHII)) {
                            entity.setZavj_d_ulhii(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.HULIIN_SAL_ULHII)) {
                            entity.setHuliin_sal_ulhii((String) pairs.getValue());
                        }
                        else if (pairs.getKey().equals(entity.UNJIIH)) {
                            entity.setUnjiih(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.TEJEEL_IDEHGU)) {
                            entity.setTejeel_idehgu(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.STR1)) {
                            String dv = String.valueOf(pairs.getValue());
                            entity.setStr1(dv);
                        }
                        else if (pairs.getKey().equals(entity.STR2)) {
                            String dv = String.valueOf(pairs.getValue());
                            entity.setStr2(dv);
                        }
                        else if (pairs.getKey().equals(entity.FORM01KEY)) {
                            entity.setForm01key(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.DELFLG)) {
                            entity.setDelflg(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.ACTFLG)) {
                            entity.setActflg(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.CRE_BY)) {
                            entity.setCre_by(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.MOD_BY)) {
                            entity.setMod_by(String.valueOf(pairs.getValue()));
                        }
                        else  if (pairs.getKey().equals(entity.RECORD_DATE)){
                            String dv = String.valueOf(pairs.getValue());
                            if (dv != null && !dv.isEmpty()){
                                if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                                java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                                entity.setRecord_date(sqlDate);
                            }
                        }
                        else  if (pairs.getKey().equals(entity.ACTFLG)){
                            String dv = String.valueOf(pairs.getValue());
                            entity.setActflg(dv);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            return service.getListData(entity);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public ErrorEntity setForm01ShTemdegInfo(LinkedTreeMap insdata, String sessionId, String menuId) throws Exception {
        System.out.println("Hospital Service ::: get shinj temdeg data");

        try {
            String command = "EDT";

            IGalzuuShTFormService service = new GalzuuShTFormServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            GalzuuShTFormEntity entity = new GalzuuShTFormEntity();

            Iterator it = insdata.entrySet().iterator();

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                        if (pairs.getKey().equals(entity.ID)) {
                            String tmpId = "";
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                                if (pairs.getValue().toString().contains(".")) {
                                    tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                                }
                                entity.setId(Long.parseLong(String.valueOf(tmpId)));
                            }
                        }
                        else if (pairs.getKey().equals(entity.CITYCODE)) {
                            entity.setCitycode(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.SUMCODE)) {
                            entity.setSumcode(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.BAGCODE)) {
                            entity.setBagcode(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.MALCHINNER)) {
                            entity.setMalchinner(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.HALUURSAN)) {
                            entity.setHaluursan(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.HELEN_D_ULHII)) {
                            entity.setHelen_d_ulhii(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.ZAVJ_D_ULHII)) {
                            entity.setZavj_d_ulhii(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.HULIIN_SAL_ULHII)) {
                            entity.setHuliin_sal_ulhii((String) pairs.getValue());
                        }
                        else if (pairs.getKey().equals(entity.UNJIIH)) {
                            entity.setUnjiih(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.TEJEEL_IDEHGU)) {
                            entity.setTejeel_idehgu(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.STR1)) {
                            String dv = String.valueOf(pairs.getValue());
                            entity.setStr1(dv);
                        }
                        else if (pairs.getKey().equals(entity.STR2)) {
                            String dv = String.valueOf(pairs.getValue());
                            entity.setStr2(dv);
                        }
                        else if (pairs.getKey().equals(entity.FORM01KEY)) {
                            entity.setForm01key(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.DELFLG)) {
                            entity.setDelflg(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.ACTFLG)) {
                            entity.setActflg(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.CRE_AT)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (dv != null && !dv.isEmpty()){
                                if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                                java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                                entity.setCre_at(sqlDate);
                            }
                        }
                        else if (pairs.getKey().equals(entity.CRE_BY)) {
                            entity.setCre_by(String.valueOf(pairs.getValue()));
                        }
                        else if (pairs.getKey().equals(entity.MOD_AT)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (dv != null && !dv.isEmpty()){
                                if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                                java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                                entity.setMod_at(sqlDate);
                            }
                        }
                        else if (pairs.getKey().equals(entity.MOD_BY)) {
                            entity.setMod_by(String.valueOf(pairs.getValue()));
                        }
                        else  if (pairs.getKey().equals(entity.RECORD_DATE)){
                            String dv = String.valueOf(pairs.getValue());
                            if (dv != null && !dv.isEmpty()){
                                if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                                java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                                entity.setRecord_date(sqlDate);
                            }
                        }
                        else  if (pairs.getKey().equals(entity.ACTFLG)){
                            String dv = String.valueOf(pairs.getValue());
                            entity.setActflg(dv);
                        }
                        else if (pairs.getKey().toString().equalsIgnoreCase("command")) {
                            command = String.valueOf(pairs.getValue());
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (entity.getForm01key().isEmpty()){
                errorEntity = new ErrorEntity(ErrorType.ERROR, new Long(1005));
                errorEntity.setErrText("Формын ерөнхий мэдээллийг бөгөлсөн байх шаардлагатай!");
                errorEntity.setErrDesc("Формын ерөнхий мэдээллийг бөгөлсөн байх шаардлагатай!");

                return errorEntity;
            }

            GalzuuShTFormEntity tmp = new GalzuuShTFormEntity();
            tmp.setForm01key(entity.getForm01key());
            tmp.setDelflg("N");
            try{
                List<GalzuuShTFormEntity> tmplist = service.getListData(tmp);

                if (tmplist != null && tmplist.size() > 0){
                    command = "EDT";
                }else {
                    command = "INS";
                }
            }catch (Exception ex){
                ex.printStackTrace();

                errorEntity = new ErrorEntity(ErrorType.ERROR, new Long(1006));
                errorEntity.setErrText("Маяг бөглөх эрх тань буруу тул сисем хариуцсан хүнтэй холбогдоно уу!");
                errorEntity.setErrDesc("Маяг бөглөх эрх тань буруу тул сисем хариуцсан хүнтэй холбогдоно уу!");

                return errorEntity;
            }

            java.util.Date utilDate = new java.util.Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            entity.setMod_at(sqlDate);
            entity.setMod_by(user.getUsername());

            if (command.equalsIgnoreCase("INS")) {
                entity.setDelflg("N");
                entity.setActflg("Y");
                entity.setCre_by(user.getUsername());
                entity.setCre_at(sqlDate);

                errorEntity = service.insertNewData(entity);
            }
            else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL") ||
                    command.equalsIgnoreCase("ACT")) {
                if (command.equalsIgnoreCase("DEL")) {
                    entity.setDelflg("Y");
                } else if (command.equalsIgnoreCase("ACT")) {

                }
                errorEntity = service.updateData(entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return errorEntity;
    }
    // End Shinj Temdeg Medeelel


    //    Begin-form61
    @Override
    public List<Form61Entity> getForm61Data(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 61 data");

        try {
            IForm61Service form61Service = new Form61ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form61Entity form61Entity = new Form61Entity();

            Iterator it = criteria.entrySet().iterator();


            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form61Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            form61Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(form61Entity.AIMAG)) {
                        form61Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form61Entity.SUM)) {
                        form61Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form61Entity.ZOORITOO)) {
                        try {
                            form61Entity.setZooriToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form61Entity.setZooriToo(Long.valueOf("0"));
                        }
                    } else if (pairs.getKey().equals(form61Entity.ZOORITALBAI)) {
                        try {
                            form61Entity.setZooriTalbai(BigDecimal.valueOf(Double.parseDouble(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form61Entity.setZooriTalbai(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form61Entity.MALHAASHAATOO)) {
                        try {
                            form61Entity.setMalHaashaaToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form61Entity.setMalHaashaaToo(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form61Entity.MALHAASHAATALBAI)) {
                        try {
                            form61Entity.setMalHaashaaTalbai(BigDecimal.valueOf(Double.parseDouble(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form61Entity.setMalHaashaaTalbai(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form61Entity.HUDAGTOO)) {
                        try {
                            form61Entity.setHudagToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form61Entity.setHudagToo(Long.parseLong("0"));
                        }

                    } else if (pairs.getKey().equals(form61Entity.HUDAGHEMJEE)) {
                        try {
                            form61Entity.setHudagHemjee(BigDecimal.valueOf(Double.parseDouble(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form61Entity.setHudagHemjee(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form61Entity.TEJAGUUTOO)) {
                        try {
                            form61Entity.setTejAguuToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form61Entity.setTejAguuToo(Long.parseLong("0"));
                        }

                    } else if (pairs.getKey().equals(form61Entity.TEJAGUUTALBAI)) {
                        try {
                            form61Entity.setTejAguuTalbai(BigDecimal.valueOf(Double.parseDouble(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form61Entity.setTejAguuTalbai(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form61Entity.TUUHIIEDHADTOO)) {
                        try {
                            form61Entity.setTuuhiiEdHadToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form61Entity.setTuuhiiEdHadToo(Long.parseLong("0"));
                        }

                    } else if (pairs.getKey().equals(form61Entity.TUUHIIEDHADHEMJEE)) {
                        try {
                            form61Entity.setTuuhiiEdHadHemjee(BigDecimal.valueOf(Double.parseDouble(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form61Entity.setTuuhiiEdHadHemjee(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form61Entity.MALTONOGHERGSEL)) {
                        try {
                            form61Entity.setMalTonogHergsel(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form61Entity.setMalTonogHergsel(Long.parseLong("0"));
                        }

                    } else if (pairs.getKey().equals(form61Entity.BELCHEERTALBAI)) {
                        try {
                            form61Entity.setBelcheerTalbai(BigDecimal.valueOf(Double.parseDouble(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form61Entity.setBelcheerTalbai(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form61Entity.NIITTALBAI)) {
                        try {
                            form61Entity.setNiitTalbai(BigDecimal.valueOf(Double.parseDouble(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form61Entity.setNiitTalbai(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    }

                    else if (pairs.getKey().equals(form61Entity.TEEVERTOO)) {
                        try {
                            form61Entity.setTeevertoo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form61Entity.setTeevertoo(Long.parseLong("0"));
                        }

                    }

                    else if (pairs.getKey().equals(form61Entity.ZORCHIGCHTOO)) {
                        try {
                            form61Entity.setZorchigchtoo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form61Entity.setZorchigchtoo(Long.parseLong("0"));
                        }

                    }

                    else  if (pairs.getKey().equals(form61Entity.RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form61Entity.setRecordDate(sqlDate);
                    }
                    else  if (pairs.getKey().equals(form61Entity.SEARCH_RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form61Entity.setSearchRecordDate(sqlDate);
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                form61Entity.setCreby(user.getUsername());
            }

            return form61Service.getListData(form61Entity);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public ErrorEntity setForm61Data(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 61 data");

        try {
            String command = "EDT";
            IForm61Service form61Service = new Form61ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form61Entity form61Entity = new Form61Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form61Entity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                            if (pairs.getValue().toString().contains(".")) {
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            form61Entity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    }
                    else if (pairs.getKey().equals(form61Entity.AIMAG)) {
                        form61Entity.setAimag(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(form61Entity.SUM)) {
                        form61Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form61Entity.ZOORITOO)) {
                        try {
                            form61Entity.setZooriToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form61Entity.setZooriToo(Long.valueOf("0"));
                        }
                    } else if (pairs.getKey().equals(form61Entity.ZOORITALBAI)) {
                        try {
                            form61Entity.setZooriTalbai(BigDecimal.valueOf(Double.parseDouble(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form61Entity.setZooriTalbai(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form61Entity.MALHAASHAATOO)) {
                        try {
                            form61Entity.setMalHaashaaToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form61Entity.setMalHaashaaToo(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form61Entity.MALHAASHAATALBAI)) {
                        try {
                            form61Entity.setMalHaashaaTalbai(BigDecimal.valueOf(Double.parseDouble(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form61Entity.setMalHaashaaTalbai(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form61Entity.HUDAGTOO)) {
                        try {
                            form61Entity.setHudagToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form61Entity.setHudagToo(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form61Entity.HUDAGHEMJEE)) {
                        try {
                            form61Entity.setHudagHemjee(BigDecimal.valueOf(Double.parseDouble(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form61Entity.setHudagHemjee(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form61Entity.TEJAGUUTOO)) {
                        try {
                            form61Entity.setTejAguuToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form61Entity.setTejAguuToo(Long.parseLong("0"));
                        }

                    } else if (pairs.getKey().equals(form61Entity.TEJAGUUTALBAI)) {
                        try {
                            form61Entity.setTejAguuTalbai(BigDecimal.valueOf(Double.parseDouble(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form61Entity.setTejAguuTalbai(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form61Entity.TUUHIIEDHADTOO)) {
                        try {
                            form61Entity.setTuuhiiEdHadToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form61Entity.setTuuhiiEdHadToo(Long.parseLong("0"));
                        }

                    } else if (pairs.getKey().equals(form61Entity.TUUHIIEDHADHEMJEE)) {
                        try {
                            form61Entity.setTuuhiiEdHadHemjee(BigDecimal.valueOf(Double.parseDouble(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form61Entity.setTuuhiiEdHadHemjee(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form61Entity.MALTONOGHERGSEL)) {
                        try {
                            form61Entity.setMalTonogHergsel(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form61Entity.setMalTonogHergsel(Long.parseLong("0"));
                        }

                    } else if (pairs.getKey().equals(form61Entity.BELCHEERTALBAI)) {
                        try {
                            form61Entity.setBelcheerTalbai(BigDecimal.valueOf(Double.parseDouble(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form61Entity.setBelcheerTalbai(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form61Entity.NIITTALBAI)) {
                        try {
                            form61Entity.setNiitTalbai(BigDecimal.valueOf(Double.parseDouble(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form61Entity.setNiitTalbai(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    }

                    else if (pairs.getKey().equals(form61Entity.TEEVERTOO)) {
                        try {
                            form61Entity.setTeevertoo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form61Entity.setTeevertoo(Long.parseLong("0"));
                        }

                    }
                    else if (pairs.getKey().equals(form61Entity.ZORCHIGCHTOO)) {
                        try {
                            form61Entity.setZorchigchtoo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form61Entity.setZorchigchtoo(Long.parseLong("0"));
                        }

                    }

                    else  if (pairs.getKey().equals(form61Entity.RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (!dv.isEmpty() && dv.length() >= 8){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form61Entity.setRecordDate(sqlDate);
                        }
                    }
                    else if (pairs.getKey().toString().equalsIgnoreCase("command")) {
                        command = String.valueOf(pairs.getValue());
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (command.equalsIgnoreCase("INS"))
            {
                form61Entity.setDelflg("N");
                form61Entity.setActflg("Y");
                if (user != null && (user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                        || user.getRoleCode().equalsIgnoreCase("ADMIN"))){
                    //Aimag songoh shaardlagagui
                }else{
                    form61Entity.setAimag(user.getCity());
                }
                //form61Entity.setSum(user.getSum());
                form61Entity.setCreby(user.getUsername());
                Date utilDate = new Date();
                java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                form61Entity.setCreat(sqlDate);
                if (form61Entity.getRecordDate() == null) form61Entity.setCreat(sqlDate);

                errorEntity = form61Service.insertNewData(form61Entity);
            } else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL") ||
                    command.equalsIgnoreCase("ACT")) {
                if (command.equalsIgnoreCase("DEL")) {
                    form61Entity.setDelflg("Y");
                } else if (command.equalsIgnoreCase("ACT")) {
                    form61Entity.setActflg("N");
                }
                errorEntity = form61Service.updateData(form61Entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return errorEntity;
    }
    //    End-form61

    //    Begin-form62
    @Override
    public List<Form62Entity> getForm62Data(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 62 data");

        try {
            IForm62Service form62Service = new Form62ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form62Entity form62Entity = new Form62Entity();

            Iterator it = criteria.entrySet().iterator();


            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form62Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            form62Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(form62Entity.AIMAG)) {
                        form62Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form62Entity.SUM)) {
                        form62Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form62Entity.AJAHUINNER)) {
                        form62Entity.setAjAhuinNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form62Entity.EZENNER)) {
                        form62Entity.setEzenNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form62Entity.MERGEJIL)) {
                        form62Entity.setMergejil(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form62Entity.IHEMCH)) {
                        form62Entity.setIhEmch(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form62Entity.BAGAEMCH)) {
                        form62Entity.setBagaEmch(String.valueOf(pairs.getValue()));

                    } else if (pairs.getKey().equals(form62Entity.MALZUICH)) {
                        form62Entity.setMalZuich(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form62Entity.ORHTOO)) {
                        try {
                            form62Entity.setOrhToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form62Entity.setOrhToo(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form62Entity.MALTOO)) {
                        try {
                            form62Entity.setMalToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form62Entity.setMalToo(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form62Entity.NEGJMALTOO)) {
                        try {
                            form62Entity.setNegjMalToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form62Entity.setNegjMalToo(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form62Entity.MERGEJILTENMALTOO)) {
                        try {
                            form62Entity.setMergejiltenMalToo(BigDecimal.valueOf(Double.parseDouble(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form62Entity.setMergejiltenMalToo(BigDecimal.valueOf(Long.parseLong("0")));
                        }
                    } else if (pairs.getKey().equals(form62Entity.BAIR)) {
                        form62Entity.setBair(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form62Entity.UNAA)) {
                        form62Entity.setUnaa(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form62Entity.ONTSSAIN)) {
                        form62Entity.setOntsSain(String.valueOf(pairs.getValue()));
                    }
                    else  if (pairs.getKey().equals(form62Entity.RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form62Entity.setRecordDate(sqlDate);
                    }
                    else  if (pairs.getKey().equals(form62Entity.SEARCH_RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form62Entity.setSearchRecordDate(sqlDate);
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                form62Entity.setCreby(user.getUsername());
            }

            return form62Service.getListData(form62Entity);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public ErrorEntity setForm62Data(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 62 data");

        try {
            String command = "EDT";
            IForm62Service form62Service = new Form62ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form62Entity form62Entity = new Form62Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form62Entity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                            if (pairs.getValue().toString().contains(".")) {
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            form62Entity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    }else if (pairs.getKey().equals(form62Entity.AIMAG)) {
                        form62Entity.setAimag(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(form62Entity.SUM)) {
                        form62Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form62Entity.AJAHUINNER)) {
                        form62Entity.setAjAhuinNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form62Entity.EZENNER)) {
                        form62Entity.setEzenNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form62Entity.MERGEJIL)) {
                        form62Entity.setMergejil(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form62Entity.IHEMCH)) {
                        form62Entity.setIhEmch(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form62Entity.BAGAEMCH)) {
                        form62Entity.setBagaEmch(String.valueOf(pairs.getValue()));

                    } else if (pairs.getKey().equals(form62Entity.MALZUICH)) {
                        form62Entity.setMalZuich(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form62Entity.ORHTOO)) {
                        try {
                            form62Entity.setOrhToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form62Entity.setOrhToo(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form62Entity.MALTOO)) {
                        try {
                            form62Entity.setMalToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form62Entity.setMalToo(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form62Entity.NEGJMALTOO)) {
                        try {
                            form62Entity.setNegjMalToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form62Entity.setNegjMalToo(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form62Entity.MERGEJILTENMALTOO)) {
                        try {
                            form62Entity.setMergejiltenMalToo(BigDecimal.valueOf(Double.parseDouble(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form62Entity.setMergejiltenMalToo(BigDecimal.valueOf(Long.parseLong("0")));
                        }
                    } else if (pairs.getKey().equals(form62Entity.BAIR)) {
                        form62Entity.setBair(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form62Entity.UNAA)) {
                        form62Entity.setUnaa(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form62Entity.ONTSSAIN)) {
                        form62Entity.setOntsSain(String.valueOf(pairs.getValue()));
                    }
                    else  if (pairs.getKey().equals(form62Entity.RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (!dv.isEmpty() && dv.length() >= 8){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form62Entity.setRecordDate(sqlDate);
                        }
                    }
                    else if (pairs.getKey().toString().equalsIgnoreCase("command")) {
                        command = String.valueOf(pairs.getValue());
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (command.equalsIgnoreCase("INS"))
            {
                form62Entity.setDelflg("N");
                form62Entity.setActflg("Y");
                if (user != null && (user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                        || user.getRoleCode().equalsIgnoreCase("ADMIN"))){
                    //Aimag songoh shaardlagagui
                }else{
                    form62Entity.setAimag(user.getCity());
                }
                form62Entity.setCreby(user.getUsername());
                Date utilDate = new Date();
                java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                form62Entity.setCreat(sqlDate);
                if (form62Entity.getRecordDate() == null) form62Entity.setCreat(sqlDate);

                errorEntity = form62Service.insertNewData(form62Entity);
            } else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL") ||
                    command.equalsIgnoreCase("ACT")) {
                if (command.equalsIgnoreCase("DEL")) {
                    form62Entity.setDelflg("Y");
                } else if (command.equalsIgnoreCase("ACT")) {
                    form62Entity.setActflg("N");
                }
                errorEntity = form62Service.updateData(form62Entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return errorEntity;
    }

    @Override
    public List<FileEntity> getUploadFileList(LinkedTreeMap insertData, String sessionId, String menuId) throws Exception {
        System.out.println("Hospital Service Implament ::: get upload file data");

        try {
            IUploadFileService service = new UploadFileServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            FileEntity entity = new FileEntity();

            Iterator it = insertData.entrySet().iterator();


            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(entity.FYEAR)) {
                        entity.setYear(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.FMONTH)) {
                        entity.setMonth(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.FDESC)) {
                        entity.setFdesc(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.FNAME)) {
                        entity.setFname(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.FSIZE)) {
                        entity.setSize(String.valueOf(pairs.getValue()));
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            return service.getListData(entity);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public ErrorEntity setUploadFile(LinkedTreeMap criteria, String sessionId, String menuId) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 62 data");

        try {
            String command = "EDT";
            IUploadFileService service = new UploadFileServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            FileEntity entity = new FileEntity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(entity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                            if (pairs.getValue().toString().contains(".")) {
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            entity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    }
                    else if (pairs.getKey().equals(entity.FYEAR)) {
                        entity.setYear(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.FMONTH)) {
                        entity.setMonth(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.FDESC)) {
                        entity.setFdesc(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.FNAME)) {
                        entity.setFname(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.FSIZE)) {
                        entity.setSize(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().toString().equalsIgnoreCase("command")) {
                        command = String.valueOf(pairs.getValue());
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (command.equalsIgnoreCase("INS"))
            {
                entity.setDelflg("N");
                entity.setActflg("Y");
                entity.setCre_by(user.getUsername());
                Date utilDate = new Date();
                java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                entity.setCre_at(sqlDate);

                errorEntity = service.insertNewData(entity);
            } else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL") ||
                    command.equalsIgnoreCase("ACT")) {
                if (command.equalsIgnoreCase("DEL")) {
                    entity.setDelflg("Y");
                } else if (command.equalsIgnoreCase("ACT")) {
                    entity.setActflg("N");
                }
                errorEntity = service.updateData(entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return errorEntity;
    }
    //    End-form62


    @Override
    public List<BagHorooEntity> getBagFormData(LinkedTreeMap criteria, String sessionId, String menuId) throws Exception {
        System.out.println("Hospital Service ::: get bag horoo data");

        try{
            IBagHorooService service = new BagHorooServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            BagHorooEntity entity = new BagHorooEntity();

            Iterator it = criteria.entrySet().iterator();


            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try{
                    if (pairs.getKey().equals(entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    }
                    else if (pairs.getKey().equals(entity.CITY_CODE)) {
                        entity.setCityCode(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.SUM_CODE)) {
                        entity.setSumcode(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.HOROO_CODE)) {
                        entity.setHoroocode(String.valueOf(pairs.getValue()));
                    }else if (pairs.getKey().equals(entity.HOROO_NAME)) {
                        entity.setHorooname(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.DELFLG)) {
                        entity.setDelflg(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.ACTFLG)) {
                        entity.setActflg(String.valueOf(pairs.getValue()));
                    }
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }//end while

            entity.setDelflg("N");
            if (entity.getCityCode().isEmpty()){
                entity.setCityCode(user.getCity());
            }
            if (entity.getSumcode().isEmpty()){
                entity.setSumcode(user.getSum());
            }

            return service.selectAll(entity);
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {

        }

        return null;
    }

    @Override
    public ErrorEntity setBagFormData(LinkedTreeMap insdata, String sessionId, String menuId) throws Exception {
        System.out.println("Hospital Service Implament ::: set city data");

        try{
            String command = "EDT";
            IBagHorooService service = new BagHorooServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            BagHorooEntity entity = new BagHorooEntity();

            Iterator it = insdata.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try{
                    if (pairs.getKey().equals(entity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()){
                            if (pairs.getValue().toString().contains(".")){
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            entity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    }
                    else if (pairs.getKey().equals(entity.CITY_CODE)) {
                        entity.setCityCode(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.SUM_CODE)) {
                        entity.setSumcode(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.HOROO_CODE)) {
                        entity.setHoroocode(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.HOROO_NAME)) {
                        entity.setHorooname(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.HOROO_DESC)) {
                        entity.setHoroodesc(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.DELFLG)) {
                        entity.setDelflg(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.ACTFLG)) {
                        entity.setActflg(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.LATITUDE)) {
                        entity.setLatitude(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.LONGITUDE)) {
                        entity.setLongitude(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().toString().equalsIgnoreCase("command")){
                        command = String.valueOf(pairs.getValue());
                    }
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }//end while

            if (command.equalsIgnoreCase("INS"))
            {
                entity.setDelflg("N");
                entity.setActflg("Y");
                entity.setCre_by(user.getUsername());
                Date utilDate = new Date();
                java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                entity.setCre_at(sqlDate);

                errorEntity = service.insertData(entity);
            }
            else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL") ||
                    command.equalsIgnoreCase("ACT"))
            {
                entity.setMod_by(user.getUsername());
                Date utilDate = new Date();
                java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                entity.setMod_at(sqlDate);

                if (command.equalsIgnoreCase("DEL")){
                    entity.setDelflg("Y");
                }
                errorEntity = service.updateData(entity);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            if (errorEntity == null) {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1000));
            }
        }

        return errorEntity;
    }

    @Override
    public List<ComboEntity> getCmbFormData(LinkedTreeMap criteria, String sessionId, String menuId) throws Exception {
        System.out.println("Hospital Service ::: get combo box data");

        try{
            IComboService service = new ComboServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            ComboEntity entity = new ComboEntity();

            Iterator it = criteria.entrySet().iterator();


            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try{
                    if (pairs.getKey().equals(entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    }
                    else if (pairs.getKey().equals(entity.CMBTYPE)) {
                        entity.setCmbtype(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.CMBNAME)) {
                        entity.setCmbname(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.CMBVAL)) {
                        entity.setCmbval(String.valueOf(pairs.getValue()));
                    }else if (pairs.getKey().equals(entity.CMBDESC)) {
                        entity.setCmbdesc(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.DELFLG)) {
                        entity.setDelflg(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.ACTFLG)) {
                        entity.setActflg(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.CREBY)) {
                        entity.setCreBy(String.valueOf(pairs.getValue()));
                    }
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }//end while

            entity.setDelflg("N");

            return service.selectAll(entity);
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {

        }

        return null;
    }

    @Override
    public ErrorEntity setCmbFormData(LinkedTreeMap insert, String sessionId, String menuId) throws Exception {
        System.out.println("Hospital Service ::: set combo box data");

        try{
            String command = "EDT";

            IComboService service = new ComboServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            ComboEntity entity = new ComboEntity();

            Iterator it = insert.entrySet().iterator();


            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try{
                    if (pairs.getKey().equals(entity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()){
                            if (pairs.getValue().toString().contains(".")){
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            entity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    }
                    else if (pairs.getKey().equals(entity.CMBTYPE)) {
                        entity.setCmbtype(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.CMBNAME)) {
                        entity.setCmbname(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.CMBVAL)) {
                        entity.setCmbval(String.valueOf(pairs.getValue()));
                    }else if (pairs.getKey().equals(entity.CMBDESC)) {
                        entity.setCmbdesc(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.DELFLG)) {
                        entity.setDelflg(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.ACTFLG)) {
                        entity.setActflg(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.CREBY)) {
                        entity.setCreBy(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().toString().equalsIgnoreCase("command")){
                        command = String.valueOf(pairs.getValue());
                    }
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }//end while

            if (command.equalsIgnoreCase("INS"))
            {
                entity.setDelflg("N");
                entity.setActflg("Y");
                entity.setCreBy(user.getUsername());
                Date utilDate = new Date();
                java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                entity.setCreAt(sqlDate);

                errorEntity = service.insertCmb(entity);
            }
            else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL") ||
                    command.equalsIgnoreCase("ACT"))
            {
                entity.setModBy(user.getUsername());
                Date utilDate = new Date();
                java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                entity.setModAt(sqlDate);

                if (command.equalsIgnoreCase("DEL")){
                    entity.setDelflg("Y");
                }
                errorEntity = service.updateCmb(entity);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            if (errorEntity == null) {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1000));
            }
        }

        return errorEntity;
    }


    @Override
    public List<SuregMedeeFormEntity> getForm01SuregMedee(LinkedTreeMap criteria, String sessionId, String menuId) throws Exception {

        System.out.println("Hospital Service ::: set combo box data");

        try{
            String command = "EDT";

            IGalzuuSuregMedeeFormService service = new GalzuuSuregMedeeFormServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);

            SuregMedeeFormEntity entity = new SuregMedeeFormEntity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try{
                    if (pairs.getKey().equals(entity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()){
                            if (pairs.getValue().toString().contains(".")){
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            entity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    }
                    else if (pairs.getKey().equals(entity.FORM01KEY)) {
                        entity.setForm01key(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.SUREG_KEY)) {
                        entity.setSureg_key(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.CITYCODE)) {
                        entity.setCitycode(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.SUMCODE)) {
                        entity.setSumcode(String.valueOf(pairs.getValue()));
                    }else if (pairs.getKey().equals(entity.BAGCODE)) {
                        entity.setBagcode(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.MALCHINNER)) {
                        entity.setMalchinner(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.DELFLG)) {
                        entity.setDelflg(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.ACTFLG)) {
                        entity.setActflg(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.STR1)) {
                        entity.setStr1(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.STR2)) {
                        entity.setStr2(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecord_date(sqlDate);
                        }
                    }
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }//end while

            return  service.selectTreeData(user, entity);
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {

        }

        return null;
    }

    @Override
    public ErrorEntity setForm01SuregMedee(LinkedTreeMap insdata, String sessionId, String menuId) throws Exception {
        System.out.println("Hospital Service ::: set combo box data");

        try{
            String command = "EDT";

            IGalzuuSuregMedeeFormService service = new GalzuuSuregMedeeFormServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            SuregMedeeFormEntity entity = new SuregMedeeFormEntity();

            List<MedremtgiiDataEntity> entityList = new ArrayList<>();

            Iterator it = insdata.entrySet().iterator();

            final String suregKey = new ObjectId().toString();
            entity.setSureg_key(suregKey);

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try{
                    if (pairs.getKey().equals(entity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()){
                            if (pairs.getValue().toString().contains(".")){
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            entity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    }
                    else if (pairs.getKey().equals(entity.FORM01KEY)) {
                        entity.setForm01key(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.SUREG_KEY)) {
                        entity.setSureg_key(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.CITYCODE)) {
                        entity.setCitycode(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.SUMCODE)) {
                        entity.setSumcode(String.valueOf(pairs.getValue()));
                    }else if (pairs.getKey().equals(entity.BAGCODE)) {
                        entity.setBagcode(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.MALCHINNER)) {
                        entity.setMalchinner(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.DELFLG)) {
                        entity.setDelflg(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.ACTFLG)) {
                        entity.setActflg(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.STR1)) {
                        entity.setStr1(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.STR2)) {
                        entity.setStr2(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecord_date(sqlDate);
                        }
                    }
                    else if (pairs.getKey().toString().equalsIgnoreCase("command")){
                        command = String.valueOf(pairs.getValue());
                    }
                    else if (pairs.getKey().toString().equalsIgnoreCase(entity.MEDREMTGII_DATA_LIST) ||
                            pairs.getKey().toString().equalsIgnoreCase(entity.UVCHILSON_MAL_LIST) ||
                            pairs.getKey().toString().equalsIgnoreCase(entity.UHSEN_LIST) ||
                            pairs.getKey().toString().equalsIgnoreCase(entity.USTGASAN_LIST)){
                        List<LinkedTreeMap> medremtgiiDataList = (List<LinkedTreeMap>) pairs.getValue();

                        // Begin for
                        for (LinkedTreeMap m :
                                medremtgiiDataList) {
                            Iterator mt = m.entrySet().iterator();
                            MedremtgiiDataEntity dataEntity = new MedremtgiiDataEntity();

                            //End while
                            while (mt.hasNext()) {
                                Map.Entry ps = (Map.Entry) mt.next();

                                try{
                                    if (ps.getValue() != null && !ps.getValue().toString().isEmpty()){
                                        if (ps.getKey() != null && ps.getKey().toString().equalsIgnoreCase(dataEntity.SUREG_KEY)){
                                            dataEntity.setSureg_key(ps.getValue().toString());
                                        }
                                        else if (ps.getKey() != null && ps.getKey().toString().equalsIgnoreCase(dataEntity.MTYPE)){
                                            dataEntity.setMtype(ps.getValue().toString());
                                        }
                                        else if (ps.getKey() != null && ps.getKey().toString().equalsIgnoreCase(dataEntity.M_AGE)){
                                            dataEntity.setM_age(ps.getValue().toString());
                                        }
                                        else if (ps.getKey() != null && ps.getKey().toString().equalsIgnoreCase(dataEntity.AGE_SEX)){
                                            dataEntity.setAge_sex(ps.getValue().toString());
                                        }
                                        else if (ps.getKey() != null && ps.getKey().toString().equalsIgnoreCase(dataEntity.UHER)){
                                            if (ps.getValue().toString().length() < 11) dataEntity.setUher(ps.getValue().toString());
                                        }
                                        else if (ps.getKey() != null && ps.getKey().toString().equalsIgnoreCase(dataEntity.HONI)){
                                            if (ps.getValue().toString().length() < 11) dataEntity.setHoni(ps.getValue().toString());
                                        }
                                        else if (ps.getKey() != null && ps.getKey().toString().equalsIgnoreCase(dataEntity.YMAA)){
                                            if (ps.getValue().toString().length() < 11) dataEntity.setYmaa(ps.getValue().toString());
                                        }
                                        else if (ps.getKey() != null && ps.getKey().toString().equalsIgnoreCase(dataEntity.TEMEE)){
                                            if (ps.getValue().toString().length() < 11) dataEntity.setTemee(ps.getValue().toString());
                                        }
                                        else if (ps.getKey() != null && ps.getKey().toString().equalsIgnoreCase(dataEntity.GAHAI)){
                                            if (ps.getValue().toString().length() < 11) dataEntity.setGahai(ps.getValue().toString());
                                        }
                                        else if (ps.getKey() != null && ps.getKey().toString().equalsIgnoreCase(dataEntity.BUSAD)){
                                            if (ps.getValue().toString().length() < 11) dataEntity.setBusad(ps.getValue().toString());
                                        }
                                        else if (ps.getKey() != null && ps.getKey().toString().equalsIgnoreCase(dataEntity.ZERLEG_AMITAD)){
                                            if (ps.getValue().toString().length() < 11) dataEntity.setZerleg_amitad(ps.getValue().toString());
                                        }
                                        else if (ps.getKey() != null && ps.getKey().toString().equalsIgnoreCase(dataEntity.DELFLG)){
                                            dataEntity.setDelflg(ps.getValue().toString());
                                        }
                                        else if (ps.getKey() != null && ps.getKey().toString().equalsIgnoreCase(dataEntity.ACTFLG)){
                                            dataEntity.setActflg(ps.getValue().toString());
                                        }
                                        else if (ps.getKey() != null && ps.getKey().toString().equalsIgnoreCase(dataEntity.REC_TYPE)){
                                            dataEntity.setRec_type(ps.getValue().toString());
                                        }
                                    }
                                }catch (Exception ex){
                                    ex.printStackTrace();
                                }
                            }// end of while

                            dataEntity.setSureg_key(suregKey);

                            if (pairs.getKey().toString().equalsIgnoreCase(entity.MEDREMTGII_DATA_LIST)){
                                dataEntity.setRec_type("M");
                            }
                            else if (pairs.getKey().toString().equalsIgnoreCase(entity.UVCHILSON_MAL_LIST)){
                                dataEntity.setRec_type("D");
                            }
                            else if (pairs.getKey().toString().equalsIgnoreCase(entity.UHSEN_LIST)){
                                dataEntity.setRec_type("A");
                            }
                            else if (pairs.getKey().toString().equalsIgnoreCase(entity.USTGASAN_LIST)){
                                dataEntity.setRec_type("R");
                            }

                            entityList.add(dataEntity);
                        }//End of for
                        System.out.println("Medremtgii Data List::: ");
                    }
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }//end while

            if (entity.getForm01key() == null || entity.getForm01key().isEmpty()){
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1000));
                errorEntity.setErrText("Дахин оролдоно уу.");
                errorEntity.setErrDesc("Дахин оролдоно уу.");

                return errorEntity;
            }

            errorEntity = service.manageData(user, entity, entityList);
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            if (errorEntity == null) {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1000));
                errorEntity.setErrText("Хадгалах үед алдаа гарлаа. Дахин оролдоно уу.");
                errorEntity.setErrDesc("Хадгалах үед алдаа гарлаа. Дахин оролдоно уу.");
            }
        }

        return errorEntity;
    }//end of function

    @Override
    public List<TarkhvarDataEntity> getForm01TarhvarMedee(LinkedTreeMap critdata, String sessionId, String menuId) throws Exception {

        try{
            ITarkhvarMedeeFormService service = new TarkhvarMedeeFormServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);

            TarkhvarDataEntity entity = new TarkhvarDataEntity();

            Iterator it = critdata.entrySet().iterator();

            // Begin while loop
            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try{
                    if (pairs.getKey().equals(entity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()){
                            if (pairs.getValue().toString().contains(".")){
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            entity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    }
                    else if (pairs.getKey().equals(entity.HFORMKEY)) {
                        entity.setHformkey(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.CITYCODE)) {
                        entity.setCitycode(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.SUMCODE)) {
                        entity.setSumcode(String.valueOf(pairs.getValue()));
                    }else if (pairs.getKey().equals(entity.BAGCODE)) {
                        entity.setBagcode(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.MALCHINNER)) {
                        entity.setMalchinner(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.DELFLG)) {
                        entity.setDelflg(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.ACTFLG)) {
                        entity.setActflg(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.BAIGAL_HALDVARIIN))
                        entity.setBaigal_haldvariin(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.BAKTSINII))
                        entity.setBaktsinii(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.DS_HIISENESEH))
                        entity.setDs_hiiseneseh(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.DS_BAG_GISHUUN_NERS))
                        entity.setDs_bag_gishuun_ners(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.DS_UR_DUN))
                        entity.setDs_bag_gishuun_ners(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.ZUVLOMJ))
                        entity.setZuvlomj(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.HUCHIN_ZUIL_OGNOO))
                        entity.setHuchin_zuil_ognoo(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.URJLIIN_MAL_HUD))
                        entity.setUrjliin_mal_hud(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.BUSAD_MAL_HUD))
                        entity.setBusad_mal_hud(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.BAKCIN_TARGU))
                        entity.setBakcin_targu(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.OTOR_NUUDEL))
                        entity.setOtor_nuudel(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.HUN_SH_HOD))
                        entity.setHun_sh_hod(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.TEEVER_SH_HOD))
                        entity.setTeever_sh_hod(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.TABIUL_MAL_AVSAN))
                        entity.setTabiul_mal_avsan(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.BELCHEER_NEG))
                        entity.setBelcheer_neg(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.HUDAG_US))
                        entity.setHudag_us(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.BUSAD_HUCH_ZUIL))
                        entity.setBusad_huch_zuil(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.SHALTGAAN_TODGU))
                        entity.setShaltgaan_todgu(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.MALCHIN_UURUU))
                        entity.setMalchin_uuruu(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.SAHALT_AIL))
                        entity.setSahalt_ail(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.MAL_EMNELEG))
                        entity.setMal_emneleg(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.UZLEG_TANDALT))
                        entity.setUzleg_tandalt(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.TANDALT_SHINJILGEE))
                        entity.setTandalt_shinjilgee(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.BUSAD))
                        entity.setBusad(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.TUSGAARLASAN_ESEH))
                        entity.setTusgaarlasan_eseh(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.TUSGAARLASAN_OGNOO)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()) {
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setTusgaarlasan_ognoo(sqlDate);
                        }
                    }
                    else if (pairs.getKey().equals(entity.TUS_HOROO_BAIRLAL))
                        entity.setTus_horoo_bairlal(String.valueOf(pairs.getValue()));

                    else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecord_date(sqlDate);
                        }
                    }
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }//end while

            if (entity.getHformkey() != null && !entity.getHformkey().isEmpty()){
                return service.getListData(entity);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            if (errorEntity == null) {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1000));
                errorEntity.setErrText("Хадгалах үед алдаа гарлаа. Дахин оролдоно уу.");
                errorEntity.setErrDesc("Хадгалах үед алдаа гарлаа. Дахин оролдоно уу.");
            }
        }

        return null;
    }

    @Override
    public ErrorEntity setForm01TarhvarMedee(LinkedTreeMap insdata, String sessionId, String menuId) throws Exception {
        System.out.println("Hospital Service ::: tarkhvar medeelel");

        try{
            ITarkhvarMedeeFormService service = new TarkhvarMedeeFormServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);

            TarkhvarDataEntity entity = new TarkhvarDataEntity();

            Iterator it = insdata.entrySet().iterator();

            // Begin while loop
            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try{
                    if (pairs.getKey().equals(entity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()){
                            if (pairs.getValue().toString().contains(".")){
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            entity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    }
                    else if (pairs.getKey().equals(entity.HFORMKEY)) {
                        entity.setHformkey(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.CITYCODE)) {
                        entity.setCitycode(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.SUMCODE)) {
                        entity.setSumcode(String.valueOf(pairs.getValue()));
                    }else if (pairs.getKey().equals(entity.BAGCODE)) {
                        entity.setBagcode(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.MALCHINNER)) {
                        entity.setMalchinner(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.DELFLG)) {
                        entity.setDelflg(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.ACTFLG)) {
                        entity.setActflg(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.BAIGAL_HALDVARIIN))
                        entity.setBaigal_haldvariin(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.BAKTSINII))
                        entity.setBaktsinii(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.DS_HIISENESEH))
                        entity.setDs_hiiseneseh(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.DS_BAG_GISHUUN_NERS))
                        entity.setDs_bag_gishuun_ners(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.DS_UR_DUN))
                        entity.setDs_bag_gishuun_ners(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.ZUVLOMJ))
                        entity.setZuvlomj(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.HUCHIN_ZUIL_OGNOO))
                        entity.setHuchin_zuil_ognoo(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.URJLIIN_MAL_HUD))
                        entity.setUrjliin_mal_hud(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.BUSAD_MAL_HUD))
                        entity.setBusad_mal_hud(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.BAKCIN_TARGU))
                        entity.setBakcin_targu(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.OTOR_NUUDEL))
                        entity.setOtor_nuudel(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.HUN_SH_HOD))
                        entity.setHun_sh_hod(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.TEEVER_SH_HOD))
                        entity.setTeever_sh_hod(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.TABIUL_MAL_AVSAN))
                        entity.setTabiul_mal_avsan(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.BELCHEER_NEG))
                        entity.setBelcheer_neg(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.HUDAG_US))
                        entity.setHudag_us(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.BUSAD_HUCH_ZUIL))
                        entity.setBusad_huch_zuil(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.SHALTGAAN_TODGU))
                        entity.setShaltgaan_todgu(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.MALCHIN_UURUU))
                        entity.setMalchin_uuruu(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.SAHALT_AIL))
                        entity.setSahalt_ail(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.MAL_EMNELEG))
                        entity.setMal_emneleg(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.UZLEG_TANDALT))
                        entity.setUzleg_tandalt(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.TANDALT_SHINJILGEE))
                        entity.setTandalt_shinjilgee(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.BUSAD))
                        entity.setBusad(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.TUSGAARLASAN_ESEH))
                        entity.setTusgaarlasan_eseh(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.TUSGAARLASAN_OGNOO)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()) {
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setTusgaarlasan_ognoo(sqlDate);
                        }
                    }
                    else if (pairs.getKey().equals(entity.TUS_HOROO_BAIRLAL))
                        entity.setTus_horoo_bairlal(String.valueOf(pairs.getValue()));

                    else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecord_date(sqlDate);
                        }
                    }
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }//end while

            if (entity.getHformkey() == null || entity.getHformkey().isEmpty()){
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1000));
                errorEntity.setErrText("Дахин оролдоно уу.");
                errorEntity.setErrDesc("Дахин оролдоно уу.");

                return errorEntity;
            }

            errorEntity = service.manageData(user, entity);
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            if (errorEntity == null) {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1000));
                errorEntity.setErrText("Хадгалах үед алдаа гарлаа. Дахин оролдоно уу.");
                errorEntity.setErrDesc("Хадгалах үед алдаа гарлаа. Дахин оролдоно уу.");
            }
        }

        return errorEntity;
    }

    @Override
    public List<TemtsehArgaHemEntity> getForm01TemcehArgaHemjeeMedee(LinkedTreeMap critdata, String sessionId, String menuId) throws Exception {
        try{
            ITemcehArgaHemjeeFormService service = new TemcehArgaHemjeeFormServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);

            TemtsehArgaHemEntity entity = new TemtsehArgaHemEntity();

            Iterator it = critdata.entrySet().iterator();

            // Begin while loop
            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try{
                    if (pairs.getKey().equals(entity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()){
                            if (pairs.getValue().toString().contains(".")){
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            entity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    }
                    else if (pairs.getKey().equals(entity.FORM01KEY)) {
                        entity.setForm01key(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.CITYCODE)) {
                        entity.setCitycode(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.SUMCODE)) {
                        entity.setSumcode(String.valueOf(pairs.getValue()));
                    }else if (pairs.getKey().equals(entity.BAGCODE)) {
                        entity.setBagcode(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.MALCHINNER)) {
                        entity.setMalchinner(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.DELFLG)) {
                        entity.setDelflg(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.ACTFLG)) {
                        entity.setActflg(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.BAKCINOGNOO)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()) {
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setBakcinOgnoo(sqlDate);
                        }
                    }
                    else if (pairs.getKey().equals(entity.BAKCIN_UUSEL))
                        entity.setBakcin_uusel(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.HEDEN_MAL_UVCHILSON))
                        entity.setHedenMalUvchilson(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.HAMRAGDSAN_MAL_TOO))
                        entity.setHamragdsan_mal_too(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.UVCH_ILERSEN_ESEH))
                        entity.setUvch_ilersen_eseh(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.HED_HO_DARAA))
                        entity.setHed_ho_daraa(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.BAKCINJUULALT_OGNOO))
                        entity.setBakcinjuulalt_ognoo(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.MAL_EMCH_NEGJ_NER))
                        entity.setMal_emch_negj_ner(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.HALD_HIISEN_ESEH))
                        entity.setHald_hiisen_eseh(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.HEDEN_UDAA))
                    {
                        entity.setHeden_udaa(stringToInteger(String.valueOf(pairs.getValue())));
                    }
                    else if (pairs.getKey().equals(entity.YMAR_BODIS))
                        entity.setYmar_bodis(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.YMAR_ARGA))
                        entity.setYmar_arga(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.TALBAI))
                        entity.setTalbai(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.ZAR_BODIS))
                        entity.setZar_bodis(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.ECSN_HALD_OGNOO)){
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()) {
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setEcsn_hald_ognoo(sqlDate);
                        }
                    }
                    else if (pairs.getKey().equals(entity.MMNDEMUZTANHESEH))
                        entity.setMmndemuztanheseh(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.MUVCHTMHESEH))
                        entity.setMuvchtmheseh(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.GAM_TARAASA_ESEH))
                        entity.setGam_taraasa_eseh(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.ZUV_SERMA_UGSUN_ESEH))
                        entity.setZuv_serma_ugsun_eseh(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.ON_MASH_HUD_ON_GAR_ESEH))
                        entity.setOn_mash_hud_on_gar_eseh(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.BUSAD_SUM_MED_ESEH))
                        entity.setBusad_sum_med_eseh(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.UVCH_BURT_TAL_MED_ESEH))
                        entity.setUvch_burt_tal_med_eseh(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.ACTFLG))
                        entity.setActflg(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecord_date(sqlDate);
                        }
                    }
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }//end while

            if (entity.getForm01key() != null && !entity.getForm01key().isEmpty()){
                return service.getListData(entity);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            if (errorEntity == null) {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1000));
                errorEntity.setErrText("Хадгалах үед алдаа гарлаа. Дахин оролдоно уу.");
                errorEntity.setErrDesc("Хадгалах үед алдаа гарлаа. Дахин оролдоно уу.");
            }
        }

        return null;
    }

    @Override
    public ErrorEntity setForm01TemcehArgaHemjeeMedee(LinkedTreeMap insdata, String sessionId, String menuId) throws Exception {
        System.out.println("Hospital Service ::: tarkhvar medeelel");

        try{
            ITemcehArgaHemjeeFormService service = new TemcehArgaHemjeeFormServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);

            TemtsehArgaHemEntity entity = new TemtsehArgaHemEntity();

            Iterator it = insdata.entrySet().iterator();

            // Begin while loop
            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try{
                    if (pairs.getKey().equals(entity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()){
                            if (pairs.getValue().toString().contains(".")){
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            entity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    }
                    else if (pairs.getKey().equals(entity.FORM01KEY)) {
                        entity.setForm01key(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.CITYCODE)) {
                        entity.setCitycode(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.SUMCODE)) {
                        entity.setSumcode(String.valueOf(pairs.getValue()));
                    }else if (pairs.getKey().equals(entity.BAGCODE)) {
                        entity.setBagcode(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.MALCHINNER)) {
                        entity.setMalchinner(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.DELFLG)) {
                        entity.setDelflg(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.ACTFLG)) {
                        entity.setActflg(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.BAKCINOGNOO)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()) {
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setBakcinOgnoo(sqlDate);
                        }
                    }
                    else if (pairs.getKey().equals(entity.BAKCIN_UUSEL))
                        entity.setBakcin_uusel(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.HEDEN_MAL_UVCHILSON))
                        entity.setHedenMalUvchilson(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.HAMRAGDSAN_MAL_TOO))
                        entity.setHamragdsan_mal_too(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.UVCH_ILERSEN_ESEH))
                        entity.setUvch_ilersen_eseh(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.HED_HO_DARAA))
                        entity.setHed_ho_daraa(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.BAKCINJUULALT_OGNOO))
                        entity.setBakcinjuulalt_ognoo(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.MAL_EMCH_NEGJ_NER))
                        entity.setMal_emch_negj_ner(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.HALD_HIISEN_ESEH))
                        entity.setHald_hiisen_eseh(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.HEDEN_UDAA))
                    {
                        entity.setHeden_udaa(stringToInteger(String.valueOf(pairs.getValue())));
                    }
                    else if (pairs.getKey().equals(entity.YMAR_BODIS))
                        entity.setYmar_bodis(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.YMAR_ARGA))
                        entity.setYmar_arga(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.TALBAI))
                        entity.setTalbai(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.ZAR_BODIS))
                        entity.setZar_bodis(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.ECSN_HALD_OGNOO)){
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()) {
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setEcsn_hald_ognoo(sqlDate);
                        }
                    }
                    else if (pairs.getKey().equals(entity.MMNDEMUZTANHESEH))
                        entity.setMmndemuztanheseh(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.MUVCHTMHESEH))
                        entity.setMuvchtmheseh(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.GAM_TARAASA_ESEH))
                        entity.setGam_taraasa_eseh(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.ZUV_SERMA_UGSUN_ESEH))
                        entity.setZuv_serma_ugsun_eseh(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.ON_MASH_HUD_ON_GAR_ESEH))
                        entity.setOn_mash_hud_on_gar_eseh(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.BUSAD_SUM_MED_ESEH))
                        entity.setBusad_sum_med_eseh(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.UVCH_BURT_TAL_MED_ESEH))
                        entity.setUvch_burt_tal_med_eseh(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.ACTFLG))
                        entity.setActflg(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecord_date(sqlDate);
                        }
                    }
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }//end while

            if (entity.getForm01key() == null || entity.getForm01key().isEmpty()){
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1000));
                errorEntity.setErrText("Ерөнхий мэдээлэл оруулсаны дараа Тэмцэх арга хэмжээний мэдээлэл оруулах юм.");
                errorEntity.setErrDesc("Ерөнхий мэдээлэл оруулсаны дараа Тэмцэх арга хэмжээний мэдээлэл оруулах юм.");

                return errorEntity;
            }

            errorEntity = service.manageData(user, entity);
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            if (errorEntity == null) {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1000));
                errorEntity.setErrText("Хадгалах үед алдаа гарлаа. Дахин оролдоно уу.");
                errorEntity.setErrDesc("Хадгалах үед алдаа гарлаа. Дахин оролдоно уу.");
            }
        }

        return errorEntity;
    }


    @Override
    public List<TemtsehAhZardalEntity> getForm01TemcehArgaHemjeeZardal(LinkedTreeMap critdata, String sessionId, String menuId) throws Exception {
        try{
            ITemcehArgaHemjeeZardalFormService service = new TemcehArgaHemjeeZardalFormServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);

            TemtsehAhZardalEntity entity = new TemtsehAhZardalEntity();

            Iterator it = critdata.entrySet().iterator();

            // Begin while loop
            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try{
                    if (pairs.getKey().equals(entity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()){
                            if (pairs.getValue().toString().contains(".")){
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            entity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    }
                    else if (pairs.getKey().equals(entity.FORM01KEY)) {
                        entity.setForm01key(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.CITYCODE)) {
                        entity.setCitycode(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.SUMCODE)) {
                        entity.setSumcode(String.valueOf(pairs.getValue()));
                    }else if (pairs.getKey().equals(entity.BAGCODE)) {
                        entity.setBagcode(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.MALCHINNER)) {
                        entity.setMalchinner(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.DELFLG)) {
                        entity.setDelflg(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.ACTFLG)) {
                        entity.setActflg(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.AJIL_HESEG))
                        entity.setAjil_heseg(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.MAL_EMCH))
                        entity.setMal_emch(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.BUSAD_HUMUUS))
                        entity.setBusad_humuus(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.TECHNIC_HEREGSEL))
                        entity.setTechnic_heregsel(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.MEUN))
                        entity.setMeun(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.MEUT))
                        entity.setMeut(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.MEA))
                        entity.setMea(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.TUR_ZB)) {
                        entity.setTur_zb(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.NUHUN_OLG_ZARDAL))
                        entity.setNuhun_olg_zardal(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.ACTFLG))
                        entity.setActflg(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecord_date(sqlDate);
                        }
                    }
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }//end while

            if (entity.getForm01key() != null && !entity.getForm01key().isEmpty()){
                return service.getListData(entity);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            if (errorEntity == null) {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1000));
                errorEntity.setErrText("Хадгалах үед алдаа гарлаа. Дахин оролдоно уу.");
                errorEntity.setErrDesc("Хадгалах үед алдаа гарлаа. Дахин оролдоно уу.");
            }
        }

        return null;
    }

    @Override
    public ErrorEntity setForm01TemcehArgaHemjeeZardal(LinkedTreeMap insdata, String sessionId, String menuId) throws Exception {
        System.out.println("Hospital Service ::: setForm01TemcehArgaHemjeeZardal");

        try{
            ITemcehArgaHemjeeZardalFormService service = new TemcehArgaHemjeeZardalFormServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);

            TemtsehAhZardalEntity entity = new TemtsehAhZardalEntity();

            Iterator it = insdata.entrySet().iterator();

            // Begin while loop
            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try{
                    if (pairs.getKey().equals(entity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()){
                            if (pairs.getValue().toString().contains(".")){
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            entity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    }
                    else if (pairs.getKey().equals(entity.FORM01KEY)) {
                        entity.setForm01key(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.CITYCODE)) {
                        entity.setCitycode(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.SUMCODE)) {
                        entity.setSumcode(String.valueOf(pairs.getValue()));
                    }else if (pairs.getKey().equals(entity.BAGCODE)) {
                        entity.setBagcode(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.MALCHINNER)) {
                        entity.setMalchinner(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.DELFLG)) {
                        entity.setDelflg(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.ACTFLG)) {
                        entity.setActflg(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.AJIL_HESEG))
                        entity.setAjil_heseg(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.MAL_EMCH))
                        entity.setMal_emch(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.BUSAD_HUMUUS))
                        entity.setBusad_humuus(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.TECHNIC_HEREGSEL))
                        entity.setTechnic_heregsel(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.MEUN))
                        entity.setMeun(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.MEUT))
                        entity.setMeut(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.MEA))
                        entity.setMea(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.TUR_ZB)) {
                        entity.setTur_zb(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.NUHUN_OLG_ZARDAL))
                        entity.setNuhun_olg_zardal(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.ACTFLG))
                        entity.setActflg(String.valueOf(pairs.getValue()));
                    else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecord_date(sqlDate);
                        }
                    }
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }//end while

            if (entity.getForm01key() == null || entity.getForm01key().isEmpty()){
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1000));
                errorEntity.setErrText("Ерөнхий мэдээлэл оруулсаны дараа Тэмцэх арга хэмжээний мэдээлэл оруулах юм.");
                errorEntity.setErrDesc("Ерөнхий мэдээлэл оруулсаны дараа Тэмцэх арга хэмжээний мэдээлэл оруулах юм.");

                return errorEntity;
            }

            errorEntity = service.manageData(user, entity);
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            if (errorEntity == null) {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1000));
                errorEntity.setErrText("Хадгалах үед алдаа гарлаа. Дахин оролдоно уу.");
                errorEntity.setErrDesc("Хадгалах үед алдаа гарлаа. Дахин оролдоно уу.");
            }
        }

        return errorEntity;
    }

    //Other used function
    private int stringToInteger(String val) throws NumberFormatException{

        if (val != null && !val.isEmpty()){
            if (val.contains(".")){
                String buhel = val.substring(0, val.indexOf("."));

                return Integer.parseInt(buhel);
            }else{
                return Integer.parseInt(val);
            }
        }

        return 0;
    }
}
