package com.hospital.log;

import org.apache.log4j.Logger;

/**
 * Created by 24be10264 on 4/10/2017.
 */
public class Log4j {
    public final static Logger logger = Logger.getLogger(Log4j.class.getName());

    public static synchronized void error(String tag, String msg) {
        //System.out.println("[" + tag + "] " + msg);
        logger.error("[" + tag + "] " + msg);
    }

    public static synchronized void warning(String tag, String msg) {
        //System.out.println("[" + tag + "] " + msg);
        logger.warn("[" + tag + "] " + msg);
    }

    public static synchronized void info(String tag, String msg) {
        //System.out.println("[" + tag + "] " + msg);
        logger.info("[" + tag + "] " + msg);
    }

    public static synchronized void debug(String tag, String msg) {
        //System.out.println("[" + tag + "] " + msg);
        logger.debug("[" + tag + "] " + msg);
    }
}
