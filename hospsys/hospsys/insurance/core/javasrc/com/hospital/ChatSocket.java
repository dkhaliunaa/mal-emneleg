package com.hospital;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

import com.auth.HospService;
import com.model.User;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WebSocketListener;
import org.json.JSONObject;

public class ChatSocket implements WebSocketListener{
    private Session outbound;


    @Override
    public void onWebSocketBinary(byte[] bytes, int i, int i1) {
        System.out.println("onWebSocketBinary");

        try {
            outbound.getRemote().sendBytes(ByteBuffer.wrap(bytes));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onWebSocketText(String s) {
        System.out.println("onWebSocketText");

        System.out.println("Text::: " + ((s == null) ? "Hereglegch 1" : s));

        JSONObject json = new JSONObject(s);

        ConcurrentHashMap<String, User> sessionUser = HospService.sessionUser;

        User user = new User();
        user.put("sessionid", json.getString("id"));

        ChatServlet.chatSessionList.put(this.outbound, user);
        User sendUser = HospService.sessionUser.get(json.getString("id"));
        String custname = sendUser.getLastName() + " " + sendUser.getFirstName();

        ChatServlet.chatSessionList.keySet().stream().filter(Session::isOpen).forEach(session -> {
            try {
                if (session == this.outbound){
                    System.out.println("HIHIHIHIHIHI");
                }

                session.getRemote().sendString(
                        String.valueOf(new JSONObject()
                                .put("userMessage", createJsonMessageFromSender(custname, json.getString("message")))
                                .put("userlist", ChatServlet.chatSessionList.values())
                        )
                );
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        /*if ((outbound != null) && (outbound.isOpen()))
        {
            // echo the message back
            outbound.getRemote().sendString(s,null);
        }*/
    }

    private static JSONObject createJsonMessageFromSender(String sender, String message) {
        JSONObject userMessage = new JSONObject();
        userMessage.put("sender", sender);
        userMessage.put("timestamp", new SimpleDateFormat("HH:mm:ss").format(new Date()));
        userMessage.put("message", message);
        return userMessage;
    }

    @Override
    public void onWebSocketClose(int i, String s) {
        System.out.println("onWebSocketClose");

        ChatServlet.chatSessionList.remove(this.outbound);

        this.outbound = null;

        System.out.println("Session removed.");
    }

    @Override
    public void onWebSocketConnect(Session session) {
        System.out.println("onWebSocketConnect");

        try {
            this.outbound = session;

            User user = new User();

            ChatServlet.chatSessionList.put(session, user);

            session.getRemote().sendString(
                    String.valueOf(new JSONObject()
                            .put("userMessage", createJsonMessageFromSender("Сервер", "New Session connected."))
                            .put("userlist", ChatServlet.chatSessionList.values())
                    )
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onWebSocketError(Throwable throwable) {
        System.out.println("onWebSocketError");

        throwable.printStackTrace(System.err);
    }
}
