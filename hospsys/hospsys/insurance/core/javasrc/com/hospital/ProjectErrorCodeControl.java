package com.hospital;

import com.model.ProjectErrorCode;
import com.mongodb.BasicDBObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by 24be10264 on 12/17/2016.
 */
public class ProjectErrorCodeControl  implements  IProjectErrorCodeControl{
    private Connection connection;

    public ProjectErrorCodeControl(){}

    public ProjectErrorCodeControl(Connection connection){
        this.connection = connection;
    }

    /**
     *
     * @param success
     * @param errorCode
     * @return
     */
    public BasicDBObject getErrorCodeMessage(boolean success, ProjectErrorCode errorCode){
        BasicDBObject basicDBObject = new BasicDBObject();

        String strquery  = "SELECT * FROM INSUDB.PROJECTERRORCODE WHERE ERRCODE=?";
        String errDesc = "";

        PreparedStatement statement = null;
        ResultSet resultset = null;

        try {
            statement = connection.prepareStatement(strquery);

            statement.setInt(1, errorCode.getErrCode());

            resultset = statement.executeQuery();

            while (resultset.next()) {
                errDesc = resultset.getString("ERRNAME");
            }

            if (resultset != null) resultset.close();
            if (statement != null) statement.close();

            basicDBObject.put("success", success);
            basicDBObject.put("message", "("+errorCode.getErrCode()+")" + errDesc);
        }catch (Exception e){
            e.printStackTrace();
            basicDBObject.put("success", false);
            basicDBObject.put("message", "(1000) Алдааны мэдээллийг харуулах боломжгүй тул админд хандана уу!!!");
        }

        return basicDBObject;
    }

    /**
     *
     * @param success
     * @param errorCode
     * @return
     * @throws Exception
     */
    @Override
    public BasicDBObject getErrorDescription(boolean success, ProjectErrorCode errorCode) throws Exception {
        BasicDBObject basicDBObject = new BasicDBObject();

        try {
            basicDBObject.put("success", success);
            basicDBObject.put("message", "(" + errorCode.getErrCode() + ")" + ((errorCode.getErrDesc() != null) ? errorCode.getErrDesc() : ""));
        }catch (Exception ex){
            ex.printStackTrace();
            basicDBObject.put("success", false);
            basicDBObject.put("message", "(1000) Алдааны мэдээллийг харуулах боломжгүй тул админд хандана уу!!!");
        }

        return basicDBObject;
    }
}
