package com.hospital;

import com.model.RecentPageLog;
import org.omg.CORBA.SystemException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 24be10264 on 8/24/2016.
 */
public class RecentPLControl {
    private Connection connection;

    public RecentPLControl(Connection connection){
        this.connection = connection;
    }


    public List<RecentPageLog> getRecentPages(String where){
        String query = "SELECT * FROM (SELECT * FROM INSUDB.RECENTPAGELOG R WHERE "+where+") WHERE ROWNUM <= 10";

        try{
            Statement statement = connection.createStatement();
            ResultSet resultSet = null;

            resultSet = statement.executeQuery(query);
            List<RecentPageLog> logList = new ArrayList<RecentPageLog>();

            while (resultSet.next()) {
                RecentPageLog recentPageLog = new RecentPageLog();

                recentPageLog.setId(resultSet.getInt("ID"));
                recentPageLog.setPage(resultSet.getString("PAGE"));

                recentPageLog.setCode1(resultSet.getString("CODE1"));
                recentPageLog.setCode2(resultSet.getString("CODE2"));
                recentPageLog.setCode3(resultSet.getString("CODE3"));

                recentPageLog.setName1(resultSet.getString("NAME1"));
                recentPageLog.setName2(resultSet.getString("NAME2"));
                recentPageLog.setName3(resultSet.getString("NAME3"));

                recentPageLog.setRefId(resultSet.getInt("REF_ID"));
                recentPageLog.setRecordType(resultSet.getString("RECORDTYPE"));

                recentPageLog.setModifyAt(resultSet.getDate("MODIFY_AT"));
                recentPageLog.setModifyBy(resultSet.getString("MODIFY_BY"));

                recentPageLog.setCreateAt(resultSet.getDate("CREATE_AT"));
                recentPageLog.setCreateBy(resultSet.getString("CREATE_BY"));

                logList.add(recentPageLog);
            }

            if (resultSet != null) resultSet.close();
            if (statement != null) statement.close();
            return logList;
        } catch (SystemException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }//end function

    /**
     * Хамгийн сүүлд хийсэн үйлдлийг хадгалах
     * */
    public void insertRecentPage(RecentPageLog recentPageLog){
        int id = 0;
        try{
            String query = "INSERT INTO INSUDB.RECENTPAGELOG ( ID, PAGE, CODE1, CODE2, CODE3, NAME1, NAME2, NAME3, REF_ID, " +
                    "RECORDTYPE, MODIFY_AT, MODIFY_BY, CREATE_AT, CREATE_BY ) " +
                    "VALUES ( INSUDB.SEQ_RECENTPAGELOG.nextval, " +
                    "'" + recentPageLog.getPage()+"'," +
                    ((recentPageLog.get(RecentPageLog.CODE1) != null) ? "'"+ recentPageLog.getCode1()+"'" : "''")+"," +
                    ((recentPageLog.get(RecentPageLog.CODE2) != null) ? "'"+ recentPageLog.getCode2()+"'" : "''")+"," +
                    ((recentPageLog.get(RecentPageLog.CODE3) != null) ? "'"+ recentPageLog.getCode3()+"'" : "''")+"," +
                    ((recentPageLog.get(RecentPageLog.NAME1) != null) ? "'"+ recentPageLog.getName1()+"'" : "''")+"," +
                    ((recentPageLog.get(RecentPageLog.NAME2) != null) ? "'"+ recentPageLog.getName2()+"'" : "''")+"," +
                    ((recentPageLog.get(RecentPageLog.NAME3) != null) ? "'"+ recentPageLog.getName3()+"'" : "''")+"," +
                    ((recentPageLog.get(RecentPageLog.REFID) != null) ? recentPageLog.getRefId() : "''")+"," +
                    ((recentPageLog.get(RecentPageLog.RECORDTYPE) != null) ? "'"+ recentPageLog.getRecordType()+"'" : "''")+"," +
                    "sysdate, " +
                    ((recentPageLog.get(RecentPageLog.MODIFYBY) != null) ? "'"+ recentPageLog.getModifyBy()+"'" : "''")+"," +
                    "sysdate, " +
                    ((recentPageLog.get(RecentPageLog.CREATEBY) != null) ? "'"+ recentPageLog.getModifyBy() +"'" : "''")+
                    ")";


            PreparedStatement ps = connection.prepareStatement(query, new String[]{"ID"});
            ps.execute();
            ResultSet keys = ps.getGeneratedKeys();


            while(keys.next()){
                id = keys.getInt(1);
            }

            if (ps != null) ps.close();
            if (keys != null) keys.close();
        }catch (Exception e){
            System.err.println("ERROR: insertRecentPage function, execute sql command");
            e.printStackTrace();
        }finally {
            recentPageLog.setId(id);
        }
    }//end function

    /**
     * Хамгийн сүүлд ашигласан үйлдлийн тохиолдол үүсгэх
     * */
    public RecentPageLog createInstance(String page, String cd1, String cd2, String cd3, String nm1, String nm2, String nm3, int refId, String recType, String user)
    {
        RecentPageLog recentPageLog = new RecentPageLog();

        recentPageLog.setPage(page);
        recentPageLog.setCode1(cd1);
        recentPageLog.setCode2(cd2);
        recentPageLog.setCode3(cd3);

        recentPageLog.setName1(nm1);
        recentPageLog.setName2(nm2);
        recentPageLog.setName3(nm3);

        recentPageLog.setRefId(refId);
        recentPageLog.setRecordType(recType);
        recentPageLog.setModifyBy(user);
        recentPageLog.setCreateBy(user);

        return recentPageLog;
    }
}
