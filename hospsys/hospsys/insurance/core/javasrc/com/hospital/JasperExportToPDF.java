package com.hospital;

import com.Config;
import com.model.PartnerDataSet;
import com.model.ProjectErrorCode;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JRDesignStyle;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * Created by 24be10264 on 2/8/2017.
 */
public class JasperExportToPDF {

    /* PDF болгосон файлын нэр хадгалах хувьсагч*/
    private String fileName = null;

    public boolean exportToPDF(int contId, String contCode, String user, ProjectErrorCode errorCode) throws JRException, FileNotFoundException, Exception{

        /* Хамтран даатгуулагчийн мэдээлэл байна */
        int pIndex= 1;
        List<PartnerDataSet> partnerDataSetList = new ArrayList<>();
        /* Файлын замуудыг оруулж ирэх */
        String JRXML_PATH = Config.getJrxmlPath();
        String CONT_PATH = Config.getContsPath();

        /* Гэрээний тайлан гаргах файлын нэр */
        String CONTFILENAME = "insurance_cont.jasper";

        fileName = "test.pdf";
        File f = new File(CONT_PATH, fileName);
        if (f.exists()) {
            // ийм нэртэй файл байгаа учраас, файлын нэрийг солих
            fileName = renameFile(fileName);
        }

        /* Jasper файл унших */
        Map<String, Object> parameters = null;
        JasperPrint jasperPrint = JasperFillManager.fillReport(JRXML_PATH + "/" + CONTFILENAME, parameters, new JREmptyDataSource());

        JRStyle jrStyle = new JRDesignStyle();
        jrStyle.setFontName("DejaVu Serif");
        jrStyle.setPdfEncoding("Identity-H");
        jrStyle.setPdfEmbedded(true);
        jasperPrint.setDefaultStyle(jrStyle);

        /* Output Stream to create PDF */
        OutputStream outputStream = new FileOutputStream(new File(CONT_PATH + "/" + fileName));

        /* Write content to PDF file */
        JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);


        /**
         * Шинээр байгуулсан гэрээ нь дээр тухайн гэрээтэй холбоотой нөхцөлүүдийг нэмэх
         */
        try {
            String FILE_PATH = Config.getFilePath();
            PDDocument doc = new PDDocument();

            String otherCondition = "";
            if (!otherCondition.isEmpty()) {
                /*PDDocument part = PDDocument.load(new File(FILE_PATH + "/" + otherCondition)); //// FIXME: 9/13/2016 тус гэрээтэй холбоотой файлын зам байна

                for (int i = 0; i < part.getNumberOfPages(); i++) {
                    PDPage tmpPage = part.getPage(i);
                    doc.addPage(tmpPage);
                }*/

                PDFMergerUtility mergePdf = new PDFMergerUtility();

                mergePdf.addSource(new File(CONT_PATH + "/" + fileName));
                mergePdf.addSource(new File(FILE_PATH + "/" + otherCondition));

                mergePdf.setDestinationFileName(CONT_PATH + "/" + fileName);
                mergePdf.mergeDocuments();
            }
        }catch (Exception ex){
            ex.printStackTrace();
            //errorCode.setErrCode();
        }

        return false;
    }//end of function


    /**
     * Файлийн нэрийг өөрчлөх
     * */
    private String renameFile(String filePath) {
        int i = filePath.length() - 1;

        // өргөтгөлийг алгасах
        String ext = "";
        while (i > 0 && filePath.charAt(i) != '.') {
            ext = filePath.charAt(i) + ext;
            i--;
        }

        // давхардсан файлуудыг dup гэж нэрлэх
        return "dup_" + String.valueOf(System.nanoTime()) + "." + ext;

    }
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
