package com.hospital;

import com.auth.HospService;
import com.enumclass.ErrorType;
import com.google.gson.internal.LinkedTreeMap;
import com.hospital.app.*;
import com.model.User;
import com.model.hos.*;
import com.rpc.RpcHandler;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by 24be10264 on 9/28/2017.
 */
public class ReportServiceImpl extends HospService implements IReportService {
    private ErrorEntity errorEntity = null;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    public ReportServiceImpl() {
        handler = new RpcHandler<>(this, IReportService.class);
    }

    @Override
    public ErrorEntity hform08Report(LinkedTreeMap criteria, String sessionId, String file) throws Exception {
        try {
            IForm08Service service = new Form08ServiceImpl(conn);

            User user = sessionUser.get(sessionId);
            Form08Entity form08Entity = new Form08Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();
                System.out.println(pairs);
                try {
                    if (pairs.getKey().equals(form08Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            form08Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(form08Entity.AIMAG)) {
                        form08Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.SUM)) {
                        form08Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.MALTURUL)) {
                        form08Entity.setMalTurul(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.ARGA_HEMJEE_NER)) {
                        form08Entity.setArgaHemjeeNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.NER)) {
                        form08Entity.setNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.ZARTSUULSAN_HEMJEE)) {
                        form08Entity.setZartsuulsanHemjee(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.TOLOVLOGOO)) {
                        form08Entity.setTolovlogoo(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.GUITSETGEL)) {
                        form08Entity.setGuitsetgel(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.HUVI)) {
                        form08Entity.setHuvi(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(form08Entity.OLGOSON_TUN)) {
                        try {
                            form08Entity.setOLGOSON_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form08Entity.setOLGOSON_TUN(Long.parseLong("0"));
                        }
                    }
                    else if (pairs.getKey().equals(form08Entity.HEREGLESEN_TUN)) {
                        try {
                            form08Entity.setHEREGLESEN_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form08Entity.setHEREGLESEN_TUN(Long.parseLong("0"));
                        }
                    }
                    else if (pairs.getKey().equals(form08Entity.USTGASAN_TUN)) {
                        try {
                            form08Entity.setUSTGASAN_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form08Entity.setUSTGASAN_TUN(Long.parseLong("0"));
                        }
                    }
                    else if (pairs.getKey().equals(form08Entity.NUUTSULSEN_TUN)) {
                        try {
                            form08Entity.setNUUTSULSEN_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form08Entity.setNUUTSULSEN_TUN(Long.parseLong("0"));
                        }
                    }
                    else if (pairs.getKey().equals(form08Entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form08Entity.setRecordDate(sqlDate);
                    } else if (pairs.getKey().equals(form08Entity.SEARCH_RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form08Entity.setSearchRecordDate(sqlDate);
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            //form08Entity.setAimag(user.getCity());
            if(form08Entity.getSum() != null && !form08Entity.getSum().isEmpty()){

            }else {
                if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                        && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                    //form08Entity.setSum(user.getSum());
                    form08Entity.setSumNer(user.getSumName());
                    form08Entity.setSum_en(user.getSumNameEn());
                }
            }


            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                form08Entity.setCreby(user.getUsername());

                form08Entity.setAimagNer(user.getCityName());
                form08Entity.setAimag_en(user.getCityNameEn());
            }

            if (form08Entity != null && form08Entity.getRecordDate() != null && form08Entity.getSearchRecordDate() != null) {

                long diff = Math.abs(form08Entity.getSearchRecordDate().getTime() - form08Entity.getRecordDate().getTime());
                long diffDays = diff / (24 * 60 * 60 * 1000);

                if (diffDays <= 91) {
                    return service.exportReport(file, form08Entity);
                } else {
                    errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                    errorEntity.setErrText("Тайланг зөвхөн улиралаар харах боломжтой! Таны сонгосон огнооны интервал 91 хонгоос их байна.");
                }
            } else {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                errorEntity.setErrText("Тайлан харах оны интервалыг сонгоно уу. Эхлэх болон дуусах огноо хоосон байна!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return errorEntity;
    }

    @Override
    public ErrorEntity hform11Report(LinkedTreeMap criteria, String sessionId, String menuId, String file) throws Exception {
        try {
            IForm11Service service = new Form11ServiceImpl(conn);

            User user = sessionUser.get(sessionId);
            Form11Entity form11Entity = new Form11Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form11Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                            form11Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        }
                    } else if (pairs.getKey().equals(form11Entity.AIMAG)) {
                        form11Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.SUM)) {
                        form11Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.BAGCODE)) {
                        form11Entity.setBagCode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.MALTURUL)) {
                        form11Entity.setMalTurul(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.DEEJIINTURUL)) {
                        form11Entity.setDeejiinTurul(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.SUMBAGNER)) {
                        form11Entity.setSumBagNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.TOOTOLGOI)) {
                        try {
                            form11Entity.setTooTolgoi(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch (Exception e) {
                            form11Entity.setTooTolgoi(Long.parseLong(String.valueOf("0")));
                        }
                    } else if (pairs.getKey().equals(form11Entity.HAANASHINJILSEN)) {
                        form11Entity.setHaanaShinjilsen(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.SHINJILSENARGA)) {
                        form11Entity.setShinjilsenArga(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.ERUUL)) {
                        try {
                            form11Entity.setEruul(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch (Exception e) {
                            form11Entity.setEruul(Long.parseLong(String.valueOf("0")));
                        }
                    } else if (pairs.getKey().equals(form11Entity.UVCHTEI)) {
                        try {
                            form11Entity.setUvchtei(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch (Exception e) {
                            form11Entity.setUvchtei(Long.parseLong(String.valueOf("0")));
                        }
                    } else if (pairs.getKey().equals(form11Entity.ONOSH)) {
                        try {
                            form11Entity.setOnosh(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch (Exception e) {
                            form11Entity.setOnosh(Long.parseLong(String.valueOf("0")));
                        }
                    } else if (pairs.getKey().equals(form11Entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form11Entity.setRecordDate(sqlDate);
                    } else if (pairs.getKey().equals(form11Entity.SEARCH_RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form11Entity.setSearchRecordDate(sqlDate);
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while


            if(form11Entity.getSum() != null && !form11Entity.getSum().isEmpty()){

            }else {
                if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                        && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                    form11Entity.setSumNer(user.getSumName());
                    form11Entity.setSum_en(user.getSumNameEn());
                }
            }

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                form11Entity.setCreby(user.getUsername());

                form11Entity.setAimagNer(user.getCityName());
                form11Entity.setAimag_en(user.getCityNameEn());
            }

            if (form11Entity != null && form11Entity.getRecordDate() != null && form11Entity.getSearchRecordDate() != null) {

                long diff = Math.abs(form11Entity.getSearchRecordDate().getTime() - form11Entity.getRecordDate().getTime());
                long diffDays = diff / (24 * 60 * 60 * 1000);

                if (diffDays <= 91) {
                    return service.exportReport(file, form11Entity);
                } else {
                    errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                    errorEntity.setErrText("Тайланг зөвхөн улиралаар харах боломжтой! Таны сонгосон огнооны интервал 91 хонгоос их байна.");
                }
            } else {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                errorEntity.setErrText("Тайлан харах оны интервалыг сонгоно уу. Эхлэх болон дуусах огноо хоосон байна!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return errorEntity;
    }

    //Begin Report-hform09
    @Override
    public ErrorEntity hform09Report(LinkedTreeMap criteria, String sessionId, String file) throws Exception {
        try {
            IForm09Service service = new Form09ServiceImpl(conn);

            User user = sessionUser.get(sessionId);
            Form09Entity form09Entity = new Form09Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form09Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            form09Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(form09Entity.AIMAG)) {
                        form09Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.SUM)) {
                        form09Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.MALTURUL)) {
                        form09Entity.setMalTurul(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.TARILGANER)) {
                        form09Entity.setTarilgaNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.NER)) {
                        form09Entity.setNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.ZARTSUULSANHEMJEE)) {
                        form09Entity.setZartsuulsanHemjee(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.TOLOVLOGOO)) {
                        form09Entity.setTolovlogoo(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.GUITSETGEL)) {
                        form09Entity.setGuitsetgel(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.HUVI)) {
                        form09Entity.setHuvi(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form09Entity.setRecordDate(sqlDate);
                    } else if (pairs.getKey().equals(form09Entity.SEARCH_RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form09Entity.setSearchRecordDate(sqlDate);
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if(form09Entity.getSum() != null && !form09Entity.getSum().isEmpty()){

            }else {
                if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                        && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                    //form08Entity.setSum(user.getSum());
                    form09Entity.setSumNer(user.getSumName());
                    form09Entity.setSum_en(user.getSumNameEn());
                }
            }

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                form09Entity.setCreby(user.getUsername());

                form09Entity.setAimagNer(user.getCityName());
                form09Entity.setAimag_en(user.getCityNameEn());
            }

            if (form09Entity != null && form09Entity.getRecordDate() != null && form09Entity.getSearchRecordDate() != null) {

                long diff = Math.abs(form09Entity.getSearchRecordDate().getTime() - form09Entity.getRecordDate().getTime());
                long diffDays = diff / (24 * 60 * 60 * 1000);

                if (diffDays <= 91) {
                    return service.exportReport(file, form09Entity);
                } else {
                    errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                    errorEntity.setErrText("Тайланг зөвхөн улиралаар харах боломжтой! Таны сонгосон огнооны интервал 91 хонгоос их байна.");
                }
            } else {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                errorEntity.setErrText("Тайлан харах оны интервалыг сонгоно уу. Эхлэх болон дуусах огноо хоосон байна!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return errorEntity;
    }
    //End Report-hform09

    //Begin Report-hform14
    @Override
    public ErrorEntity hform14Report(LinkedTreeMap criteria, String sessionId, String file) throws Exception {
        try {
            IForm14Service service = new Form14ServiceImpl(conn);

            User user = sessionUser.get(sessionId);
            Form14Entity form14Entity = new Form14Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form14Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            form14Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(form14Entity.AIMAG)) {
                        form14Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form14Entity.SUM)) {
                        form14Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if  (pairs.getKey().equals(form14Entity.SHALGAHGAZAR)){
                        form14Entity.setShalgahGazar(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(form14Entity.UZLEGTOO)){
                        try{
                            form14Entity.setUzlegToo(Long.valueOf(pairs.getValue().toString()));
                        }catch (NumberFormatException e){
                            form14Entity.setUzlegToo(Long.valueOf("0"));
                        }
                    }
                    else if  (pairs.getKey().equals(form14Entity.BAIGUULLAGATOO)){
                        try{
                            form14Entity.setBaiguullagaToo(Long.valueOf(pairs.getValue().toString()));
                        }catch (NumberFormatException e){
                            form14Entity.setBaiguullagaToo(Long.valueOf("0"));
                        }
                    }
                    else if  (pairs.getKey().equals(form14Entity.AJAHUINNEGJTOO)){
                        try{
                            form14Entity.setAjAhuinNegjToo(Long.valueOf(pairs.getValue().toString()));
                        }catch (NumberFormatException e){
                            form14Entity.setAjAhuinNegjToo(Long.valueOf("0"));
                        }
                    }
                    else if  (pairs.getKey().equals(form14Entity.IRGEDTOO)){
                        try{
                            form14Entity.setIrgedToo(Long.valueOf(pairs.getValue().toString()));
                        }catch (NumberFormatException e){
                            form14Entity.setIrgedToo(Long.valueOf("0"));
                        }
                    }
                    else if  (pairs.getKey().equals(form14Entity.HUULIINBAIGSHILJSEN)){
                        try{
                            form14Entity.setHuuliinBaigShiljsen(Long.valueOf(pairs.getValue().toString()));
                        }catch (NumberFormatException e){
                            form14Entity.setHuuliinBaigShiljsen(Long.valueOf("0"));
                        }
                    }
                    else if  (pairs.getKey().equals(form14Entity.ZAHIRGAAHARITOO)){
                        try{
                            form14Entity.setZahirgaaHariToo(Long.valueOf(pairs.getValue().toString()));
                        }catch (NumberFormatException e){
                            form14Entity.setZahirgaaHariToo(Long.valueOf("0"));
                        }
                    }
                    else if(pairs.getKey().equals(form14Entity.TORGUULIHEMJEE)){
                        try{
                            form14Entity.setTorguuliHemjee(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        }catch (NumberFormatException e){
                            form14Entity.setTorguuliHemjee(BigDecimal.valueOf(Long.parseLong("0")));
                        }
                    }
                    else if(pairs.getKey().equals(form14Entity.TOLBOR)){
                        try{
                            form14Entity.setTolbor(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        }catch (NumberFormatException e){
                            form14Entity.setTolbor(BigDecimal.valueOf(Long.parseLong("0")));
                        }
                    }
                    else if(pairs.getKey().equals(form14Entity.TORGUULIAR)){
                        try{
                            form14Entity.setTorguuliar(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        }catch (NumberFormatException e){
                            form14Entity.setTorguuliar(BigDecimal.valueOf(Long.parseLong("0")));
                        }
                    }
                    else if(pairs.getKey().equals(form14Entity.TOLBOROOR)){
                        try{
                            form14Entity.setTolboroor(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        }catch (NumberFormatException e){
                            form14Entity.setTolboroor(BigDecimal.valueOf(Long.parseLong("0")));
                        }
                    } else if (pairs.getKey().equals(form14Entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form14Entity.setRecordDate(sqlDate);
                    } else if (pairs.getKey().equals(form14Entity.SEARCH_RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form14Entity.setSearchRecordDate(sqlDate);
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if(form14Entity.getSum() != null && !form14Entity.getSum().isEmpty()){

            }else {
                if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                        && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                    //form08Entity.setSum(user.getSum());
                    form14Entity.setSumNer(user.getSumName());
                    form14Entity.setSum_en(user.getSumNameEn());
                }
            }

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                form14Entity.setCreby(user.getUsername());
                form14Entity.setAimagNer(user.getCityName());
                form14Entity.setAimag_en(user.getCityNameEn());
            }

            if (form14Entity != null && form14Entity.getRecordDate() != null && form14Entity.getSearchRecordDate() != null) {

                long diff = Math.abs(form14Entity.getSearchRecordDate().getTime() - form14Entity.getRecordDate().getTime());
                long diffDays = diff / (24 * 60 * 60 * 1000);

                if (diffDays <= 91) {
                    return service.exportReport(file, form14Entity);
                } else {
                    errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                    errorEntity.setErrText("Тайланг зөвхөн улиралаар харах боломжтой! Таны сонгосон огнооны интервал 91 хонгоос их байна.");
                }
            } else {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                errorEntity.setErrText("Тайлан харах оны интервалыг сонгоно уу. Эхлэх болон дуусах огноо хоосон байна!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return errorEntity;
    }
    //End Report-hform14


    //Begin Report-hform43
    @Override
    public ErrorEntity hform43Report(LinkedTreeMap criteria, String sessionId) throws Exception {
        try {
            IForm43Service service = new Form43ServiceImpl(conn);

            User user = sessionUser.get(sessionId);
            Form43Entity form43Entity = new Form43Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form43Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            form43Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(form43Entity.AIMAG)) {
                        form43Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form43Entity.SUM)) {
                        form43Entity.setSum(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(form43Entity.AJAHUINNEGJ)){
                        form43Entity.setAjahuinNegj(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(form43Entity.EMBELDMELNER)){
                        form43Entity.setEmBeldmelNer(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(form43Entity.HEMJIHNEGJ)){
                        form43Entity.setHemjihNegj(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(form43Entity.UILDVERLESENTOO)){
                        try{
                            form43Entity.setUildverlesenToo(Long.valueOf(pairs.getValue().toString()));
                        }catch (NumberFormatException e){
                            form43Entity.setUildverlesenToo(Long.valueOf("0"));
                        }
                    }
                    else if  (pairs.getKey().equals(form43Entity.NEGJUNE)){
                        try{
                            form43Entity.setNegjUne(String.valueOf(pairs.getValue()));
                        }catch (NumberFormatException e){
                            form43Entity.setNegjUne(String.valueOf("0"));
                        }

                    }
                    else if  (pairs.getKey().equals(form43Entity.NIITUNE)){
                        try{
                            form43Entity.setNiitUne(String.valueOf(pairs.getValue()));
                        }catch (NullPointerException e){
                            form43Entity.setNiitUne(String.valueOf("0"));
                        }
                    } else if (pairs.getKey().equals(form43Entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form43Entity.setRecordDate(sqlDate);
                    } else if (pairs.getKey().equals(form43Entity.SEARCH_RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form43Entity.setSearchRecordDate(sqlDate);
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                form43Entity.setAimagNer(user.getCityName());
                form43Entity.setAimag_en(user.getCityNameEn());
                form43Entity.setSumNer(user.getSumName());
                form43Entity.setSum_en(user.getSumNameEn());
                form43Entity.setCreby(user.getUsername());
            }


            if (form43Entity != null && form43Entity.getRecordDate() != null && form43Entity.getSearchRecordDate() != null) {

                long diff = Math.abs(form43Entity.getSearchRecordDate().getTime() - form43Entity.getRecordDate().getTime());
                long diffDays = diff / (24 * 60 * 60 * 1000);

                if (diffDays <= 91) {
                    return service.exportReport(form43Entity);
                } else {
                    errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                    errorEntity.setErrText("Тайланг зөвхөн улиралаар харах боломжтой! Таны сонгосон огнооны интервал 91 хонгоос их байна.");
                }
            } else {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                errorEntity.setErrText("Тайлан харах оны интервалыг сонгоно уу. Эхлэх болон дуусах огноо хоосон байна!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return errorEntity;
    }
    //End Report-hform43

    //Begin Report-hform46
    @Override
    public ErrorEntity hform46Report(LinkedTreeMap criteria, String sessionId, String file) throws Exception {
        try {
            IForm46Service service = new Form46ServiceImpl(conn);

            User user = sessionUser.get(sessionId);
            Form46Entity form46Entity = new Form46Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form46Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            form46Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    }
                    else if (pairs.getKey().equals(form46Entity.AIMAG)) {
                        form46Entity.setAimag(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(form46Entity.SUM)){
                        form46Entity.setSum(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(form46Entity.BAGHOROONER)){
                        form46Entity.setBagHorooNer(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(form46Entity.UHERNIITTOO)){
                        form46Entity.setUherNiitToo(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(form46Entity.TUGALTOO)){
                        try{
                            form46Entity.setTugalToo(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch(Exception e) {
                            form46Entity.setTugalToo(Long.parseLong(String.valueOf("0")));
                        }
                    }
                    else if  (pairs.getKey().equals(form46Entity.BUSADTOO)){
                        try{
                            form46Entity.setBusadToo(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch(Exception e) {
                            form46Entity.setBusadToo(Long.parseLong(String.valueOf("0")));
                        }
                    }
                    else if  (pairs.getKey().equals(form46Entity.GUURTAINIITTOO)){
                        form46Entity.setGuurtaiNiitToo(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(form46Entity.GUURTAITUGALTOO)){
                        try {
                            form46Entity.setGuurtaiBusadToo(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch (Exception e){
                            form46Entity.setGuurtaiBusadToo(Long.parseLong(String.valueOf("0")));
                        }
                    }
                    else if  (pairs.getKey().equals(form46Entity.GUURTAIBUSADTOO)){
                        try{
                            form46Entity.setGuurtaiBusadToo(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch(Exception e) {
                            form46Entity.setGuurtaiBusadToo(Long.parseLong(String.valueOf("0")));
                        }
                    }
                    else if  (pairs.getKey().equals(form46Entity.DUNDAJHUVI)){
                        form46Entity.setDundajHuvi(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(form46Entity.TUGALHUVI)){
                        form46Entity.setTugalHuvi(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(form46Entity.BUSADHUVI)){
                        form46Entity.setBusadHuvi(String.valueOf(pairs.getValue()));
                    }
                    else  if (pairs.getKey().equals(form46Entity.RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form46Entity.setRecordDate(sqlDate);
                    }
                    else  if (pairs.getKey().equals(form46Entity.SEARCH_RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form46Entity.setSearchRecordDate(sqlDate);
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if(form46Entity.getSum() != null && !form46Entity.getSum().isEmpty()){

            }else {
                if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                        && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                    //form08Entity.setSum(user.getSum());
                    form46Entity.setSumNer(user.getSumName());
                    form46Entity.setSum_en(user.getSumNameEn());
                }
            }

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                form46Entity.setCreby(user.getUsername());
                form46Entity.setAimagNer(user.getCityName());
                form46Entity.setAimag_en(user.getCityNameEn());
            }


            if (form46Entity != null && form46Entity.getRecordDate() != null && form46Entity.getSearchRecordDate() != null) {

                long diff = Math.abs(form46Entity.getSearchRecordDate().getTime() - form46Entity.getRecordDate().getTime());
                long diffDays = diff / (24 * 60 * 60 * 1000);

                if (diffDays <= 91) {
                    return service.exportReport(file, form46Entity);
                } else {
                    errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                    errorEntity.setErrText("Тайланг зөвхөн улиралаар харах боломжтой! Таны сонгосон огнооны интервал 91 хонгоос их байна.");
                }
            } else {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                errorEntity.setErrText("Тайлан харах оны интервалыг сонгоно уу. Эхлэх болон дуусах огноо хоосон байна!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return errorEntity;
    }
    //End Report-hform46

    //Begin Report-hform51
    @Override
    public ErrorEntity hform51Report(LinkedTreeMap criteria, String sessionId,String file) throws Exception {
        try {
            IForm51Service service = new Form51ServiceImpl(conn);

            User user = sessionUser.get(sessionId);
            Form51Entity form51Entity = new Form51Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form51Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            form51Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    }
                    else if (pairs.getKey().equals(form51Entity.AIMAG)) {
                        form51Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form51Entity.SUM)) {
                        form51Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form51Entity.SUMBAGNER)) {
                        form51Entity.setSumBagNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form51Entity.NIITMALTOO)) {
                        try {
                            form51Entity.setNiitMalToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form51Entity.setNiitMalToo(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form51Entity.BOGTOO)) {
                        try {
                            form51Entity.setBogToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form51Entity.setBogToo(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form51Entity.BODTOO)) {
                        try {
                            form51Entity.setBodToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form51Entity.setBodToo(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form51Entity.TOLTOO)) {
                        try {
                            form51Entity.setTolToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form51Entity.setTolToo(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form51Entity.EHNIIULDEGDEL)) {
                        try {
                            form51Entity.setEhniiUldegdel(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form51Entity.setEhniiUldegdel(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form51Entity.ORLOGO)) {
                        try {
                            form51Entity.setOrlogo(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form51Entity.setOrlogo(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form51Entity.ZARLAGA)) {
                        try {
                            form51Entity.setZarlaga(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form51Entity.setZarlaga(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form51Entity.JILEULDEGDEL)) {
                        try {
                            form51Entity.setJilEUldegdel(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form51Entity.setJilEUldegdel(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    }
                    else  if (pairs.getKey().equals(form51Entity.RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form51Entity.setRecordDate(sqlDate);
                    }
                    else  if (pairs.getKey().equals(form51Entity.SEARCH_RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form51Entity.setSearchRecordDate(sqlDate);
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if(form51Entity.getSum() != null && !form51Entity.getSum().isEmpty()){

            }else {
                if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                        && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                    form51Entity.setSumNer(user.getSumName());
                    form51Entity.setSum_en(user.getSumNameEn());
                }
            }

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                form51Entity.setCreby(user.getUsername());

                form51Entity.setAimagNer(user.getCityName());
                form51Entity.setAimag_en(user.getCityNameEn());
            }

            if (form51Entity != null && form51Entity.getRecordDate() != null && form51Entity.getSearchRecordDate() != null) {

                long diff = Math.abs(form51Entity.getSearchRecordDate().getTime() - form51Entity.getRecordDate().getTime());
                long diffDays = diff / (24 * 60 * 60 * 1000);

                if (diffDays <= 91) {
                    return service.exportReport(file,form51Entity);
                } else {
                    errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                    errorEntity.setErrText("Тайланг зөвхөн улиралаар харах боломжтой! Таны сонгосон огнооны интервал 91 хонгоос их байна.");
                }
            } else {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                errorEntity.setErrText("Тайлан харах оны интервалыг сонгоно уу. Эхлэх болон дуусах огноо хоосон байна!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return errorEntity;
    }
    //End Report-hform51


    //Begin Report-hform61
    @Override
    public ErrorEntity hform61Report(LinkedTreeMap criteria, String sessionId,String file) throws Exception {
        try {
            IForm61Service service = new Form61ServiceImpl(conn);

            User user = sessionUser.get(sessionId);
            Form61Entity form61Entity = new Form61Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form61Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            form61Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    }
                    else if (pairs.getKey().equals(form61Entity.AIMAG)) {
                        form61Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form61Entity.SUM)) {
                        form61Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form61Entity.ZOORITOO)) {
                        try {
                            form61Entity.setZooriToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form61Entity.setZooriToo(Long.valueOf("0"));
                        }
                    } else if (pairs.getKey().equals(form61Entity.ZOORITALBAI)) {
                        try {
                            form61Entity.setZooriTalbai(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form61Entity.setZooriTalbai(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form61Entity.MALHAASHAATOO)) {
                        try {
                            form61Entity.setMalHaashaaToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form61Entity.setMalHaashaaToo(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form61Entity.MALHAASHAATALBAI)) {
                        try {
                            form61Entity.setMalHaashaaTalbai(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form61Entity.setMalHaashaaTalbai(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form61Entity.HUDAGTOO)) {
                        try {
                            form61Entity.setHudagToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form61Entity.setHudagToo(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form61Entity.HUDAGHEMJEE)) {
                        try {
                            form61Entity.setHudagHemjee(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form61Entity.setHudagHemjee(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form61Entity.TEJAGUUTOO)) {
                        try {
                            form61Entity.setTejAguuToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form61Entity.setTejAguuToo(Long.parseLong("0"));
                        }

                    } else if (pairs.getKey().equals(form61Entity.TEJAGUUTALBAI)) {
                        try {
                            form61Entity.setTejAguuTalbai(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form61Entity.setTejAguuTalbai(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form61Entity.TUUHIIEDHADTOO)) {
                        try {
                            form61Entity.setTuuhiiEdHadToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form61Entity.setTuuhiiEdHadToo(Long.parseLong("0"));
                        }

                    } else if (pairs.getKey().equals(form61Entity.TUUHIIEDHADHEMJEE)) {
                        try {
                            form61Entity.setTuuhiiEdHadHemjee(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form61Entity.setTuuhiiEdHadHemjee(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form61Entity.MALTONOGHERGSEL)) {
                        try {
                            form61Entity.setMalTonogHergsel(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form61Entity.setMalTonogHergsel(Long.parseLong("0"));
                        }

                    } else if (pairs.getKey().equals(form61Entity.BELCHEERTALBAI)) {
                        try {
                            form61Entity.setBelcheerTalbai(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form61Entity.setBelcheerTalbai(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    } else if (pairs.getKey().equals(form61Entity.NIITTALBAI)) {
                        try {
                            form61Entity.setNiitTalbai(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form61Entity.setNiitTalbai(BigDecimal.valueOf(Long.parseLong("0")));
                        }

                    }

                    else if (pairs.getKey().equals(form61Entity.TEEVERTOO)) {
                        try {
                            form61Entity.setTeevertoo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form61Entity.setTeevertoo(Long.parseLong("0"));
                        }

                    }
                    else if (pairs.getKey().equals(form61Entity.ZORCHIGCHTOO)) {
                        try {
                            form61Entity.setZorchigchtoo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form61Entity.setZorchigchtoo(Long.parseLong("0"));
                        }

                    }

                    else  if (pairs.getKey().equals(form61Entity.RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form61Entity.setRecordDate(sqlDate);
                    }
                    else  if (pairs.getKey().equals(form61Entity.SEARCH_RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form61Entity.setSearchRecordDate(sqlDate);
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if(form61Entity.getSum() != null && !form61Entity.getSum().isEmpty()){

            }else {
                if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                        && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                    form61Entity.setSumNer(user.getSumName());
                    form61Entity.setSum_en(user.getSumNameEn());
                }
            }

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                form61Entity.setCreby(user.getUsername());

                form61Entity.setAimagNer(user.getCityName());
                form61Entity.setAimag_en(user.getCityNameEn());
            }

            if (form61Entity != null && form61Entity.getRecordDate() != null && form61Entity.getSearchRecordDate() != null) {

                long diff = Math.abs(form61Entity.getSearchRecordDate().getTime() - form61Entity.getRecordDate().getTime());
                long diffDays = diff / (24 * 60 * 60 * 1000);

                if (diffDays <= 91) {
                    return service.exportReport(file,form61Entity);
                } else {
                    errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                    errorEntity.setErrText("Тайланг зөвхөн улиралаар харах боломжтой! Таны сонгосон огнооны интервал 91 хонгоос их байна.");
                }
            } else {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                errorEntity.setErrText("Тайлан харах оны интервалыг сонгоно уу. Эхлэх болон дуусах огноо хоосон байна!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return errorEntity;
    }
    //End Report-hform61

    @Override
    public ErrorEntity hform08HavsraltReport(LinkedTreeMap criteria, String sessionId, String file) throws Exception {
        System.out.println(file);
        try {
            IForm08HavsraltService service = new Form08HavsraltServiceImpl(conn);

            User user = sessionUser.get(sessionId);
            Form08HavsraltEntity entity = new Form08HavsraltEntity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(entity.CITYCODE)) {
                        entity.setCitycode(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.SUMCODE)){
                        entity.setSumcode(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.BAGCODE)){
                        entity.setBagcode(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.GAZARNER)){
                        entity.setGazarner(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.URTRAG)){
                        entity.setUrtrag(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.ORGOROG)){
                        entity.setOrgorog(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.GOLOMTTOO)){
                        entity.setGolomttoo(stringToInteger(String.valueOf(pairs.getValue())));
                    }
                    else if  (pairs.getKey().equals(entity.OVCHIN_NER)){
                        entity.setOvchin_ner(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.MAL_TURUL)){
                        entity.setMal_turul(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.UVCHILSON)){
                        entity.setUvchilson(stringToInteger(String.valueOf(pairs.getValue())));
                    }
                    else if  (pairs.getKey().equals(entity.UHSEN)){
                        entity.setUhsen(stringToInteger(String.valueOf(pairs.getValue())));
                    }
                    else if  (pairs.getKey().equals(entity.EDGERSEN)){
                        entity.setEdgersen(stringToInteger(String.valueOf(pairs.getValue())));
                    }
                    else if  (pairs.getKey().equals(entity.USTGASAN)){
                        entity.setUstgasan(stringToInteger(String.valueOf(pairs.getValue())));
                    }
                    else if  (pairs.getKey().equals(entity.NYDLUULSAN)){
                        entity.setNydluulsan(stringToInteger(String.valueOf(pairs.getValue())));
                    }
                    else if  (pairs.getKey().equals(entity.LAB_NER)){
                        entity.setLab_ner(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.SHINJILGEE_ARGA)){
                        entity.setShinjilgee_arga(String.valueOf(pairs.getValue()));
                    }
                    else  if (pairs.getKey().equals(entity.RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (!dv.isEmpty() && dv.length() >= 8){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        }
                    }
                    else  if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (!dv.isEmpty() && dv.length() >= 8){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                entity.setCreby(user.getUsername());

                entity.setCityName(user.getCityName());
            }

            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {

                long diff = Math.abs(entity.getSearchRecordDate().getTime() - entity.getRecordDate().getTime());
                long diffDays = diff / (24 * 60 * 60 * 1000);

                if (diffDays <= 366) {
                    return service.exportReport(entity, file);
                } else {
                    errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                    errorEntity.setErrText("Тайланг зөвхөн жилээр харах боломжтой! Таны сонгосон огнооны интервал 365 хонгоос их байна.");
                }
            } else {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                errorEntity.setErrText("Тайлан харах оны интервалыг сонгоно уу. Эхлэх болон дуусах огноо хоосон байна!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return errorEntity;
    }

    @Override
    public ErrorEntity hform013aReport(LinkedTreeMap criteria, String sessionId, String file) throws Exception {
        System.out.println("Hospital Service Implament ::: setForm13aData havsralt data");

        try{
            IForm13aService service = new Form13aServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form13aEntity entity = new Form13aEntity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try{
                    if (String.valueOf(pairs.getValue()).isEmpty()) continue;

                    if (pairs.getKey().equals(entity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()){
                            if (pairs.getValue().toString().contains(".")){
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            entity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    }
                    else if (pairs.getKey().equals(entity.CITYCODE)) {
                        entity.setCitycode(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.SUMCODE)){
                        entity.setSumcode(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.BAG_CODE)){
                        entity.setBag_code(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.UZLEG_HIIH)){
                        entity.setUzleg_hiih(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.HEMJIH_NEGJ)){
                        entity.setHemjih_negj(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.TOO_HEMJEE)){
                        entity.setToo_hemjee(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.DEEJIIN_TOO)){
                        entity.setDeejiin_too(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.SHINJILGEE_HIISEN)){
                        entity.setShinjilgee_hiisen(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.SHINJILSEN_ARGA)){
                        entity.setShinjilsen_arga(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.HERGTSEEND_TOHIRSON)){
                        entity.setHergtseend_tohirson(Double.parseDouble(String.valueOf(pairs.getValue())));
                    }
                    else if  (pairs.getKey().equals(entity.USTGASAN)){
                        entity.setUstgasan(Double.parseDouble(String.valueOf(pairs.getValue())));
                    }
                    else if  (pairs.getKey().equals(entity.HALDBARGUIJUULELT)){
                        entity.setHaldbarguijuulelt(Double.parseDouble(String.valueOf(pairs.getValue())));
                    }
                    else if  (pairs.getKey().equals(entity.BRUTSELLYOZ)){
                        entity.setBrutsellyoz(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.MASTIT)){
                        entity.setMastit(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.TRIXINELL)){
                        entity.setTrixinell(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.FINNOZ)){
                        entity.setFinnoz(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.SURIYEE)){
                        entity.setSuriyee(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.BOOM)){
                        entity.setBoom(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.BUSAD)){
                        entity.setBusad(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.BUGD)){
                        entity.setBugd(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.DEL_FLG)){
                        entity.setDel_flg(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.CRE_BY)){
                        entity.setCre_by(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.NO_DATA)){
                        entity.setNo_data(String.valueOf(pairs.getValue()));
                    }
                    else  if (pairs.getKey().equals(entity.RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (!dv.isEmpty() && dv.length() >= 8){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        }
                    }
                    else  if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (!dv.isEmpty() && dv.length() >= 8){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }//end while


            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                entity.setCre_by(user.getUsername());

                entity.setCityName(user.getCityName());
                entity.setSumName(user.getSumName());
            }

            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {

                long diff = Math.abs(entity.getSearchRecordDate().getTime() - entity.getRecordDate().getTime());
                long diffDays = diff / (24 * 60 * 60 * 1000);

                if (diffDays <= 91) {
                    return service.exportReport(file, entity);
                } else {
                    errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                    errorEntity.setErrText("Тайланг зөвхөн улиралаар харах боломжтой! Таны сонгосон огнооны интервал 91 хонгоос их байна.");
                }
            } else {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                errorEntity.setErrText("Тайлан харах оны интервалыг сонгоно уу. Эхлэх болон дуусах огноо хоосон байна!");
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {

        }

        return errorEntity;
    }

    @Override
    public ErrorEntity hform62Report(LinkedTreeMap criteria, String sessionId,String file) throws Exception {
        try {
            IForm62Service service = new Form62ServiceImpl(conn);

            User user = sessionUser.get(sessionId);
            Form62Entity form62Entity = new Form62Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form62Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            form62Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(form62Entity.AIMAG)) {
                        form62Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form62Entity.SUM)) {
                        form62Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form62Entity.AJAHUINNER)) {
                        form62Entity.setAjAhuinNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form62Entity.EZENNER)) {
                        form62Entity.setEzenNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form62Entity.MERGEJIL)) {
                        form62Entity.setMergejil(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form62Entity.IHEMCH)) {
                        form62Entity.setIhEmch(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form62Entity.BAGAEMCH)) {
                        form62Entity.setBagaEmch(String.valueOf(pairs.getValue()));

                    } else if (pairs.getKey().equals(form62Entity.MALZUICH)) {
                        form62Entity.setMalZuich(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form62Entity.ORHTOO)) {
                        try {
                            form62Entity.setOrhToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form62Entity.setOrhToo(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form62Entity.MALTOO)) {
                        try {
                            form62Entity.setMalToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form62Entity.setMalToo(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form62Entity.NEGJMALTOO)) {
                        try {
                            form62Entity.setNegjMalToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form62Entity.setNegjMalToo(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form62Entity.MERGEJILTENMALTOO)) {
                        try {
                            form62Entity.setMergejiltenMalToo(BigDecimal.valueOf(Double.parseDouble(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form62Entity.setMergejiltenMalToo(BigDecimal.valueOf(Long.parseLong("0")));
                        }
                    } else if (pairs.getKey().equals(form62Entity.BAIR)) {
                        form62Entity.setBair(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form62Entity.UNAA)) {
                        form62Entity.setUnaa(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form62Entity.ONTSSAIN)) {
                        form62Entity.setOntsSain(String.valueOf(pairs.getValue()));
                    }
                    else  if (pairs.getKey().equals(form62Entity.RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form62Entity.setRecordDate(sqlDate);
                    }
                    else  if (pairs.getKey().equals(form62Entity.SEARCH_RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form62Entity.setSearchRecordDate(sqlDate);
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while



            if(form62Entity.getSum() != null && !form62Entity.getSum().isEmpty()){

            }else {
                if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                        && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                    //form08Entity.setSum(user.getSum());
                    form62Entity.setSumNer(user.getSumName());
                    form62Entity.setSum_en(user.getSumNameEn());
                }
            }

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                form62Entity.setCreby(user.getUsername());


                form62Entity.setAimagNer(user.getCityName());
                form62Entity.setAimag_en(user.getCityNameEn());
            }

            if (form62Entity != null && form62Entity.getRecordDate() != null && form62Entity.getSearchRecordDate() != null) {

                long diff = Math.abs(form62Entity.getSearchRecordDate().getTime() - form62Entity.getRecordDate().getTime());
                long diffDays = diff / (24 * 60 * 60 * 1000);

                if (diffDays <= 91) {
                    return service.exportReport(file,form62Entity);
                } else {
                    errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                    errorEntity.setErrText("Тайланг зөвхөн улиралаар харах боломжтой! Таны сонгосон огнооны интервал 91 хонгоос их байна.");
                }
            } else {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                errorEntity.setErrText("Тайлан харах оны интервалыг сонгоно уу. Эхлэх болон дуусах огноо хоосон байна!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return errorEntity;
    }
    //End Report-hform62

    private int stringToInteger(String val) throws NumberFormatException{

        if (val != null && !val.isEmpty()){
            if (val.contains(".")){
                String buhel = val.substring(0, val.indexOf("."));

                return Integer.parseInt(buhel);
            }
        }

        return 0;
    }

    @Override
    public ErrorEntity hformGalzuuReport(LinkedTreeMap criteria, String sessionId, String file) throws Exception {
        try {
            IGalzuuFormService service = new GalzuuFormServiceImpl(conn);

            User user = sessionUser.get(sessionId);
            GalzuuFormEntity formGalzuuEntity = new GalzuuFormEntity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(formGalzuuEntity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            formGalzuuEntity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(formGalzuuEntity.CITYCODE)) {
                        formGalzuuEntity.setCitycode(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.SUMCODE)) {
                        formGalzuuEntity.setSumcode(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.BAGNAME)) {
                        formGalzuuEntity.setBagname(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.FRMKEY)) {
                        formGalzuuEntity.setFrmkey(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.GAZARNER)) {
                        formGalzuuEntity.setGazarner(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.GOLOMTTOO)) {
                        try {
                            formGalzuuEntity.setGolomttoo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            formGalzuuEntity.setGolomttoo(Long.valueOf("0"));
                        }
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.MALCHINNER)) {
                        formGalzuuEntity.setMalchinner(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.UVCHLOL_BURT_URH_HARI)) {
                        formGalzuuEntity.setUvchlol_burt_urh_hari((String) pairs.getValue());
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.COORDINATE_N)) {
                        formGalzuuEntity.setCoordinate_n(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.COORDINATE_E)) {
                        formGalzuuEntity.setCoordinate_e(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.DUUDLAGAOGNOO)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            formGalzuuEntity.setDuudlagaognoo(sqlDate);
                        }
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.UVCHLOLEHELSEN_OGNOO)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            formGalzuuEntity.setUvchlolehelsen_ognoo(sqlDate);
                        }
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.UVCHLOLBURT_OGNOO)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            formGalzuuEntity.setUvchlolburt_ognoo(sqlDate);
                        }
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.LAB_BAT_OGNOO)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            formGalzuuEntity.setLab_bat_ognoo(sqlDate);
                        }
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.LAB_BAT_HEVSHIL)) {
                        formGalzuuEntity.setLab_bat_hevshil(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.SHARGA)) {
                        formGalzuuEntity.setSharga(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.SHURDUN)) {
                        formGalzuuEntity.setShurdun(String.valueOf(pairs.getValue()));
                    }
                    //Uvchilson
                    else if (pairs.getKey().equals(formGalzuuEntity.UVCH_UHER)) {
                        try {
                            formGalzuuEntity.setUvch_uher(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            formGalzuuEntity.setUvch_uher(Long.valueOf("0"));
                        }
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.UVCH_HONI)) {
                        try {
                            formGalzuuEntity.setUvch_honi(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            formGalzuuEntity.setUvch_honi(Long.valueOf("0"));
                        }
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.UVCH_YMAA)) {
                        try {
                            formGalzuuEntity.setUvch_ymaa(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            formGalzuuEntity.setUvch_ymaa(Long.valueOf("0"));
                        }
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.UVCH_BUSAD)) {
                        try {
                            formGalzuuEntity.setUvch_busad(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            formGalzuuEntity.setUvch_busad(Long.valueOf("0"));
                        }
                    }
                    //Uhsen
                    else if (pairs.getKey().equals(formGalzuuEntity.UHSEN_UHER)) {
                        try {
                            formGalzuuEntity.setUhsen_uher(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            formGalzuuEntity.setUhsen_uher(Long.valueOf("0"));
                        }
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.UHSEN_HONI)) {
                        try {
                            formGalzuuEntity.setUhsen_honi(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            formGalzuuEntity.setUhsen_honi(Long.valueOf("0"));
                        }
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.UHSEN_YMAA)) {
                        try {
                            formGalzuuEntity.setUhsen_ymaa(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            formGalzuuEntity.setUhsen_ymaa(Long.valueOf("0"));
                        }
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.UHSEN_BUSAD)) {
                        try {
                            formGalzuuEntity.setUhsen_busad(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            formGalzuuEntity.setUhsen_busad(Long.valueOf("0"));
                        }
                    }
                    //Ustgasan
                    else if (pairs.getKey().equals(formGalzuuEntity.USTGASAN_UHER)) {
                        try {
                            formGalzuuEntity.setUstgasan_uher(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            formGalzuuEntity.setUstgasan_uher(Long.valueOf("0"));
                        }
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.USTGASAN_HONI)) {
                        try {
                            formGalzuuEntity.setUstgasan_honi(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            formGalzuuEntity.setUstgasan_honi(Long.valueOf("0"));
                        }
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.USTGASAN_YMAA)) {
                        try {
                            formGalzuuEntity.setUstgasan_ymaa(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            formGalzuuEntity.setUstgasan_ymaa(Long.valueOf("0"));
                        }
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.USTGASAN_BUSAD)) {
                        try {
                            formGalzuuEntity.setUstgasan_busad(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            formGalzuuEntity.setUstgasan_busad(Long.valueOf("0"));
                        }
                    }

                    else if (pairs.getKey().equals(formGalzuuEntity.HORIO_CEER_ESEH)) {
                        formGalzuuEntity.setHorio_ceer_eseh(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.HORIO_CEER_OGNOO)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            formGalzuuEntity.setHorio_ceer_ognoo(sqlDate);
                        }
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.HORIO_CEER_CUCALSAN_OGNOO)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            formGalzuuEntity.setHorio_ceer_cucalsan_ognoo(sqlDate);
                        }
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.USTGASAN_ESEH)) {
                        formGalzuuEntity.setUstgasan_eseh(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(formGalzuuEntity.USTGAL_HIISEN_OGNOO)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            formGalzuuEntity.setUstgal_hiisen_ognoo(sqlDate);
                        }
                    }
                    else  if (pairs.getKey().equals(formGalzuuEntity.RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            formGalzuuEntity.setRecord_date(sqlDate);
                        }
                    }
                    else  if (pairs.getKey().equals(formGalzuuEntity.SEARCH_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            formGalzuuEntity.setSearchDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                formGalzuuEntity.setCre_by(user.getUsername());

                formGalzuuEntity.setAimagNer(user.getCityName());
                formGalzuuEntity.setSumNer(user.getSumName());
            }

            if (formGalzuuEntity != null && formGalzuuEntity.getRecord_date() != null && formGalzuuEntity.getSearchDate() != null) {

                long diff = Math.abs(formGalzuuEntity.getSearchDate().getTime() - formGalzuuEntity.getRecord_date().getTime());
                long diffDays = diff / (24 * 60 * 60 * 1000);

                if (diffDays <= 91) {
                    return service.exportReport(file, formGalzuuEntity);
                } else {
                    errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                    errorEntity.setErrText("Тайланг зөвхөн улиралаар харах боломжтой! Таны сонгосон огнооны интервал 91 хонгоос их байна.");
                }
            } else {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                errorEntity.setErrText("Тайлан харах оны интервалыг сонгоно уу. Эхлэх болон дуусах огноо хоосон байна!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return errorEntity;
    }
    //End Report-hformGalzuu

    @Override
    public ErrorEntity hform09HavsraltReport(LinkedTreeMap criteria, String sessionId,String file) throws Exception {
        try {
            IForm09HavsraltService service = new Form09HavsraltServiceImpl(conn);

            User user = sessionUser.get(sessionId);
            Form09HavsraltEntity entity = new Form09HavsraltEntity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(entity.CITYCODE)) {
                        entity.setCitycode(String.valueOf(pairs.getValue()));
                    }
                    else if (pairs.getKey().equals(entity.SUMCODE)){
                        entity.setSumcode(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.BAGCODE)){
                        entity.setBagcode(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.GAZARNER)){
                        entity.setGazarner(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.URTRAG)){
                        entity.setUrtrag(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.ORGOROG)){
                        entity.setOrgorog(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.GOLOMTTOO)){
                        entity.setGolomttoo(stringToInteger(String.valueOf(pairs.getValue())));
                    }
                    else if  (pairs.getKey().equals(entity.OVCHIN_NER)){
                        entity.setOvchin_ner(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.MAL_TURUL)){
                        entity.setMal_turul(String.valueOf(pairs.getValue()));
                    }
                    else if  (pairs.getKey().equals(entity.UVCHILSON)){
                        entity.setUvchilson(stringToInteger(String.valueOf(pairs.getValue())));
                    }
                    else if  (pairs.getKey().equals(entity.UHSEN)){
                        entity.setUhsen(stringToInteger(String.valueOf(pairs.getValue())));
                    }
                    else if  (pairs.getKey().equals(entity.EDGERSEN)){
                        entity.setEdgersen(stringToInteger(String.valueOf(pairs.getValue())));
                    }
                    else  if (pairs.getKey().equals(entity.RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (!dv.isEmpty() && dv.length() >= 8){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        }
                    }
                    else  if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (!dv.isEmpty() && dv.length() >= 8){
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if(entity.getSumcode() != null && !entity.getSumcode().isEmpty()){

            }else {
                if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                        && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                    entity.setSumName(user.getSumName());
                }
            }

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                entity.setCreby(user.getUsername());

                entity.setCityName(user.getCityName());
                entity.setSumName(user.getSumName());
            }

            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {

                long diff = Math.abs(entity.getSearchRecordDate().getTime() - entity.getRecordDate().getTime());
                long diffDays = diff / (24 * 60 * 60 * 1000);

                if (diffDays <= 91) {
                    return service.exportReport(file,entity);
                } else {
                    errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                    errorEntity.setErrText("Тайланг зөвхөн улиралаар харах боломжтой! Таны сонгосон огнооны интервал 91 хонгоос их байна.");
                }
            } else {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                errorEntity.setErrText("Тайлан харах оны интервалыг сонгоно уу. Эхлэх болон дуусах огноо хоосон байна!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return errorEntity;
    }
    @Override
    public ErrorEntity hform52Report(LinkedTreeMap criteria, String sessionId,String file) throws Exception {
        try {
            IForm52Service service = new Form52ServiceImpl(conn);

            User user = sessionUser.get(sessionId);
            Form52Entity form52Entity = new Form52Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form52Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            form52Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(form52Entity.AIMAG)) {
                        form52Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form52Entity.SUM)) {
                        form52Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form52Entity.UVCHINNER)) {
                        form52Entity.setUvchinNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form52Entity.BUGDUVCHILSEN)) {
                        try {
                            form52Entity.setBugdUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setBugdUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.BUGDUHSEN)) {
                        try {
                            form52Entity.setBugdUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setBugdUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.BUGDEDGERSEN)) {
                        try {
                            form52Entity.setBugdEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setBugdEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.TEMEEUVCHILSEN)) {
                        try {
                            form52Entity.setTemeeUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setTemeeUvchilsen(Long.valueOf("0"));
                        }
                    }
                    else if (pairs.getKey().equals(form52Entity.TEMEEUHSEN)) {
                        try {
                            form52Entity.setTemeeUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setTemeeUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.TEMEEEDGERSEN)) {
                        try {
                            form52Entity.setTemeeEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setTemeeEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.ADUUUVCHILSEN)) {
                        try {
                            form52Entity.setAduuUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setAduuUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.ADUUUHSEN)) {
                        try {
                            form52Entity.setAduuUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setAduuUhsen(Long.valueOf("0"));
                        }

                    }
                    else if (pairs.getKey().equals(form52Entity.ADUUEDGERSEN)) {
                        try {
                            form52Entity.setAduuEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setAduuEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.UHERUVCHILSEN)) {
                        try {
                            form52Entity.setUherUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setUherUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.UHERUHSEN)) {
                        try {
                            form52Entity.setUherUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setUherUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.UHEREDGERSEN)) {
                        try {
                            form52Entity.setHoniEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setHoniEdgersen(Long.valueOf("0"));
                        }

                    }
                    else if (pairs.getKey().equals(form52Entity.HONIUVCHILSEN)) {
                        try {
                            form52Entity.setHoniUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setHoniUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.HONIUHSEN)) {
                        try {
                            form52Entity.setHoniUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setHoniUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.HONIEDGERSEN)) {
                        try {
                            form52Entity.setHoniEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setHoniEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.YAMAAUVCHILSEN)) {
                        try {
                            form52Entity.setYamaaUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setYamaaUvchilsen(Long.valueOf("0"));
                        }

                    }
                    else if (pairs.getKey().equals(form52Entity.YAMAAUHSEN)) {
                        try {
                            form52Entity.setYamaaUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setYamaaUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.YAMAAEDGERSEN)) {
                        try {
                            form52Entity.setYamaaEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setYamaaEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.BUSADUVCHILSEN)) {
                        try {
                            form52Entity.setBusadUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setBusadUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form52Entity.BUSADUHSEN)) {
                        try {
                            form52Entity.setBusadUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setBusadUhsen(Long.valueOf("0"));
                        }

                    }
                    else if (pairs.getKey().equals(form52Entity.BUSADEDGERSEN)) {
                        try {
                            form52Entity.setBusadEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form52Entity.setBusadEdgersen(Long.valueOf("0"));
                        }

                    }
                    else  if (pairs.getKey().equals(form52Entity.RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form52Entity.setRecordDate(sqlDate);
                    }
                    else  if (pairs.getKey().equals(form52Entity.SEARCH_RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form52Entity.setSearchRecordDate(sqlDate);
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if(form52Entity.getSum() != null && !form52Entity.getSum().isEmpty()){

            }else {
                if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                        && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                    form52Entity.setSumNer(user.getSumName());
                    form52Entity.setSum_en(user.getSumNameEn());
                }
            }

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                form52Entity.setCreby(user.getUsername());

                form52Entity.setAimagNer(user.getCityName());
                form52Entity.setAimag_en(user.getCityNameEn());
            }

            if (form52Entity != null && form52Entity.getRecordDate() != null && form52Entity.getSearchRecordDate() != null) {

                long diff = Math.abs(form52Entity.getSearchRecordDate().getTime() - form52Entity.getRecordDate().getTime());
                long diffDays = diff / (24 * 60 * 60 * 1000);

                if (diffDays <= 91) {
                    return service.exportReport(file,form52Entity);
                } else {
                    errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                    errorEntity.setErrText("Тайланг зөвхөн улиралаар харах боломжтой! Таны сонгосон огнооны интервал 91 хонгоос их байна.");
                }
            } else {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                errorEntity.setErrText("Тайлан харах оны интервалыг сонгоно уу. Эхлэх болон дуусах огноо хоосон байна!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return errorEntity;
    }
    //End Report-hform52
//    Begin_Report-Form53
    @Override
    public ErrorEntity hform53Report(LinkedTreeMap criteria, String sessionId,String file) throws Exception {
        try {
            IForm53Service service = new Form53ServiceImpl(conn);

            User user = sessionUser.get(sessionId);
            Form53Entity form53Entity = new Form53Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form53Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            form53Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(form53Entity.AIMAG)) {
                        form53Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form53Entity.SUM)) {
                        form53Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form53Entity.UVCHINNER)) {
                        form53Entity.setUvchinNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form53Entity.BUGDUVCHILSEN)) {
                        try {
                            form53Entity.setBugdUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBugdUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.BUGDUHSEN)) {
                        try {
                            form53Entity.setBugdUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBugdUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.BUGDEDGERSEN)) {
                        try {
                            form53Entity.setBugdEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBugdEdgersen(Long.valueOf("0"));
                        }

                    }else if (pairs.getKey().equals(form53Entity.BOTGOUVCHILSEN)) {
                        try {
                            form53Entity.setBotgoUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBotgoUvchilsen(Long.valueOf("0"));
                        }
                    }
                    else if (pairs.getKey().equals(form53Entity.BOTGOUHSEN)) {
                        try {
                            form53Entity.setBotgoUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBotgoUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.BOTGOEDGERSEN)) {
                        try {
                            form53Entity.setBotgoEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBotgoEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.UNAGAUVCHILSEN)) {
                        try {
                            form53Entity.setUnagaUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setUnagaUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.UNAGAUHSEN)) {
                        try {
                            form53Entity.setUnagaUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setUnagaUhsen(Long.valueOf("0"));
                        }

                    }
                    else if (pairs.getKey().equals(form53Entity.UNAGAEDGERSEN)) {
                        try {
                            form53Entity.setUnagaEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setUnagaEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.TUGALUVCHILSEN)) {
                        try {
                            form53Entity.setTugalUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setTugalUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.TUGALUHSEN)) {
                        try {
                            form53Entity.setTugalUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setTugalUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.TUGALEDGERSEN)) {
                        try {
                            form53Entity.setTugalEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setTugalEdgersen(Long.valueOf("0"));
                        }

                    }
                    else if (pairs.getKey().equals(form53Entity.HURGAUVCHILSEN)) {
                        try {
                            form53Entity.setHurgaUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setHurgaUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.HURGAUHSEN)) {
                        try {
                            form53Entity.setHurgaUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setHurgaUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.HURGAEDGERSEN)) {
                        try {
                            form53Entity.setHurgaEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setHurgaEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.ISHIGUVCHILSEN)) {
                        try {
                            form53Entity.setIshigUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setIshigUvchilsen(Long.valueOf("0"));
                        }

                    }
                    else if (pairs.getKey().equals(form53Entity.ISHIGUHSEN)) {
                        try {
                            form53Entity.setIshigUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setIshigUhsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.ISHIGEDGERSEN)) {
                        try {
                            form53Entity.setIshigEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setIshigEdgersen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.BUSADUVCHILSEN)) {
                        try {
                            form53Entity.setBusadUvchilsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBusadUvchilsen(Long.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form53Entity.BUSADUHSEN)) {
                        try {
                            form53Entity.setBusadUhsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBusadUhsen(Long.valueOf("0"));
                        }

                    }
                    else if (pairs.getKey().equals(form53Entity.BUSADEDGERSEN)) {
                        try {
                            form53Entity.setBusadEdgersen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form53Entity.setBusadEdgersen(Long.valueOf("0"));
                        }

                    }
                    else  if (pairs.getKey().equals(form53Entity.RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form53Entity.setRecordDate(sqlDate);
                    }
                    else  if (pairs.getKey().equals(form53Entity.SEARCH_RECORD_DATE)){
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form53Entity.setSearchRecordDate(sqlDate);
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if(form53Entity.getSum() != null && !form53Entity.getSum().isEmpty()){

            }else {
                if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                        && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                    form53Entity.setSumNer(user.getSumName());
                    form53Entity.setSum_en(user.getSumNameEn());
                }
            }

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                form53Entity.setCreby(user.getUsername());
                form53Entity.setAimagNer(user.getCityName());
                form53Entity.setAimag_en(user.getCityNameEn());
            }

            if (form53Entity != null && form53Entity.getRecordDate() != null && form53Entity.getSearchRecordDate() != null) {

                long diff = Math.abs(form53Entity.getSearchRecordDate().getTime() - form53Entity.getRecordDate().getTime());
                long diffDays = diff / (24 * 60 * 60 * 1000);

                if (diffDays <= 91) {
                    return service.exportReport(file,form53Entity);
                } else {
                    errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                    errorEntity.setErrText("Тайланг зөвхөн улиралаар харах боломжтой! Таны сонгосон огнооны интервал 91 хонгоос их байна.");
                }
            } else {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1003));

                errorEntity.setErrText("Тайлан харах оны интервалыг сонгоно уу. Эхлэх болон дуусах огноо хоосон байна!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return errorEntity;
    }
//    End-Report-form53
}
