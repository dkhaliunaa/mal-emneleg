package com.hospital;

import com.mongodb.BasicDBObject;
import com.model.ProjectErrorCode;
import com.model.RespList;
import com.model.Role;
import com.model.UserAndRole;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by 24be10264 on 12/17/2016.
 */
public interface IUserRoleControl {

    List<Role> roleSelect() throws SQLException, Exception;

    List<UserAndRole> select (UserAndRole selObj) throws SQLException, Exception;

    RespList usersRoleListSelect(String domain, String sname, int pageindex) throws  SQLException, Exception;

    BasicDBObject deleteUserRole(int id, String user) throws SQLException, Exception;

    BasicDBObject userroleRegister(String domain, String role, String user) throws SQLException, Exception;

    boolean checkRole(String roleCode, String perm, String userDomain, ProjectErrorCode errorCode) throws SQLException;

    boolean ContRole(String roleCode, String contId, String userDomain, ProjectErrorCode errorCode) throws SQLException;
}
