package com.hospital;

import com.google.gson.internal.LinkedTreeMap;
import com.model.hos.*;

import java.util.List;

/**
 * Created by 24be10264 on 9/27/2017.
 */
public interface IHospFormService {

    /**
     * Fetch form number 01
     *
     * @param criteria
     * @param sessionId
     * @return
     * @throws Exception
     */
    List<GalzuuFormEntity> getForm01MainInfo(LinkedTreeMap criteria, String sessionId, String menuId) throws Exception;

    /**
     * Insert data
     *
     * @param insdata
     * @param btnId
     * @param sessionId
     * @return
     * @throws Exception
     */
    ErrorEntity setForm01MainInfo(LinkedTreeMap insdata, String btnId, String sessionId, String menuId) throws Exception;


    /**
     *
     * Form 08 Data
     *
     * @param criteria      -- criteria datas
     * @param sessionId     -- session id
     * @return
     * @throws Exception
     */
    List<Form51Entity> getForm51Data(LinkedTreeMap criteria, String sessionId) throws Exception;

    /**
     *
     * @param criteria
     * @param sessionId
     * @return
     * @throws Exception
     */
    ErrorEntity setForm51Data(LinkedTreeMap criteria, String sessionId) throws Exception;

    List<Form52Entity> getForm52Data(LinkedTreeMap criteria, String sessionId) throws Exception;

    /**
     *
     * @param criteria
     * @param sessionId
     * @return
     * @throws Exception
     */
    ErrorEntity setForm52Data(LinkedTreeMap criteria, String sessionId) throws Exception;


    /**
     *
     * Form 61 Data
     *
     * @param criteria      -- criteria datas
     * @param sessionId     -- session id
     * @return
     * @throws Exception
     */
    List<Form61Entity> getForm61Data(LinkedTreeMap criteria, String sessionId) throws Exception;

    /**
     *
     * @param criteria
     * @param sessionId
     * @return
     * @throws Exception
     */
    ErrorEntity setForm61Data(LinkedTreeMap criteria, String sessionId) throws Exception;


    /**
     *
     * Form 61 Data
     *
     * @param criteria      -- criteria datas
     * @param sessionId     -- session id
     * @return
     * @throws Exception
     */
    List<Form62Entity> getForm62Data(LinkedTreeMap criteria, String sessionId) throws Exception;

    /**
     *
     * @param criteria
     * @param sessionId
     * @return
     * @throws Exception
     */
    ErrorEntity setForm62Data(LinkedTreeMap criteria, String sessionId) throws Exception;


    // Begin File upload

    /**
     * Get Info
     *
     * @param insertData
     * @param sessionId
     * @return
     * @throws Exception
     */
    List<FileEntity> getUploadFileList(LinkedTreeMap insertData, String sessionId, String menuId) throws Exception;

    /**
     * Set Info
     *
     * @param criteria
     * @param sessionId
     * @return
     * @throws Exception
     */
    ErrorEntity setUploadFile(LinkedTreeMap criteria, String sessionId, String menuId) throws Exception;
    // End File upload

    //Begin Bag Form
    /**
     *
     * @param criteria
     * @param sessionId
     * @return
     * @throws Exception
     */
    List<BagHorooEntity> getBagFormData(LinkedTreeMap criteria, String sessionId, String menuId) throws Exception;

    ErrorEntity setBagFormData(LinkedTreeMap criteria, String sessionId, String menuId) throws Exception;
    //Ebd Bag form


    //Start Cmb Data Form
    List<ComboEntity> getCmbFormData(LinkedTreeMap criteria, String sessionId, String menuId) throws Exception;

    ErrorEntity setCmbFormData(LinkedTreeMap insert, String sessionId, String menuId) throws Exception;
    //End Cmb Data Form

    List<Form53Entity> getForm53Data(LinkedTreeMap criteria, String sessionId) throws Exception;

    /**
     *
     * @param criteria
     * @param sessionId
     * @return
     * @throws Exception
     */
    ErrorEntity setForm53Data(LinkedTreeMap criteria, String sessionId) throws Exception;


    /**
     * Get Form 01 Shinj Temdeg medeelel
     *
     * @param insdata
     * @param sessionId
     * @param menuId
     * @return
     * @throws Exception
     */
    List<GalzuuShTFormEntity> getForm01ShTemdegInfo(LinkedTreeMap insdata, String sessionId, String menuId) throws Exception;

    /**
     * Get Form 01 Shinj Temdeg Information
     *
     * @param insdata
     * @param sessionId
     * @param menuId
     * @return
     * @throws Exception
     */
    ErrorEntity setForm01ShTemdegInfo(LinkedTreeMap insdata, String sessionId, String menuId) throws Exception;

    /**
     * Get Form 01 Surgiin medeelel
     *
     * @param insdata
     * @param sessionId
     * @param menuId
     * @return
     * @throws Exception
     */
    List<SuregMedeeFormEntity> getForm01SuregMedee(LinkedTreeMap insdata, String sessionId, String menuId) throws Exception;

    /**
     * Get Form 01 Shinj Surgiin medeelel
     *
     * @param insdata
     * @param sessionId
     * @param menuId
     * @return
     * @throws Exception
     */
    ErrorEntity setForm01SuregMedee(LinkedTreeMap insdata, String sessionId, String menuId) throws Exception;


    /**
     * Get tarkhvar medeelel
     *
     * @param critdata
     * @param sessionId
     * @param menuId
     * @return
     * @throws Exception
     */
    List<TarkhvarDataEntity> getForm01TarhvarMedee(LinkedTreeMap critdata, String sessionId, String menuId) throws Exception;


    /**
     * Set tarkhvar medeelel
     *
     * @param insdata
     * @param sessionId
     * @param menuId
     * @return
     * @throws Exception
     */
    ErrorEntity setForm01TarhvarMedee(LinkedTreeMap insdata, String sessionId, String menuId) throws Exception;


    /**
     * Get tarkhvar medeelel
     *
     * @param critdata
     * @param sessionId
     * @param menuId
     * @return
     * @throws Exception
     */
    List<TemtsehArgaHemEntity> getForm01TemcehArgaHemjeeMedee(LinkedTreeMap critdata, String sessionId, String menuId) throws Exception;


    /**
     * Set tarkhvar medeelel
     *
     * @param insdata
     * @param sessionId
     * @param menuId
     * @return
     * @throws Exception
     */
    ErrorEntity setForm01TemcehArgaHemjeeMedee(LinkedTreeMap insdata, String sessionId, String menuId) throws Exception;

    /**
     * Get tarkhvar medeelel
     *
     * @param critdata
     * @param sessionId
     * @param menuId
     * @return
     * @throws Exception
     */
    List<TemtsehAhZardalEntity> getForm01TemcehArgaHemjeeZardal(LinkedTreeMap critdata, String sessionId, String menuId) throws Exception;


    /**
     * Set tarkhvar medeelel
     *
     * @param insdata
     * @param sessionId
     * @param menuId
     * @return
     * @throws Exception
     */
    ErrorEntity setForm01TemcehArgaHemjeeZardal(LinkedTreeMap insdata, String sessionId, String menuId) throws Exception;
}
