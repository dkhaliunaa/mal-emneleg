package com.hospital;

import com.google.gson.internal.LinkedTreeMap;
import com.model.FavoritePages;
import com.model.Role;
import com.model.User;
import com.model.hos.*;
import com.mongodb.BasicDBObject;

import java.util.List;

public interface HospitalService {

    /**
     * Checking session
     *
     * @param sessionId
     * @return
     */
    BasicDBObject checkSession(String sessionId);

    /**
     * Энэ нь нэвтэрсэний дараа хэрэглэгчийн хуудас дээрх эрхүүдийг татаж авна.
     *
     * @param sessionId
     * @return BasicDBObject
     * */
    List<BasicDBObject> getRoles(String sessionId);

    /**
     *
     * @param type
     * @param langid
     * @return
     * @throws Exception
     */
    List<BasicDBObject> getShalgahGazarVals(String type, String langid, String sessionId) throws Exception;

    /**
     *
     * @param type
     * @param langid
     * @return
     * @throws Exception
     */
    List<BasicDBObject> getComboVals(String type, String langid, String sessionId) throws Exception;

    /**
     *
     * News Data
     *
     * @param criteria      -- criteria datas
     * @param sessionId     -- session id
     * @return
     * @throws Exception
     */
    List<NewsEntity> getNewsData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;

    /**
     *
     * @param criteria
     * @param sessionId
     * @return
     * @throws Exception
     */
    ErrorEntity setNewsData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;

    /**
     *
     * Form 09 Data
     *
     * @param criteria      -- criteria datas
     * @param sessionId     -- session id
     * @return
     * @throws Exception
     */
    List<Form08Entity> getForm08Data(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;

    /**
     *
     * @param criteria
     * @param sessionId
     * @return
     * @throws Exception
     */
    ErrorEntity setForm08Data(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;

    /**
     *
     * Form 09 Data
     *
     * @param criteria      -- criteria datas
     * @param sessionId     -- session id
     * @return
     * @throws Exception
     */
    List<Form09Entity> getForm09Data(LinkedTreeMap criteria, String sessionId) throws Exception;

    /**
     *
     * @param criteria
     * @param sessionId
     * @return
     * @throws Exception
     */
    ErrorEntity setForm09Data(LinkedTreeMap criteria, String sessionId) throws Exception;

    /**
     *
     * Form 11 Data
     *
     * @param criteria      -- criteria datas
     * @param sessionId     -- session id
     * @return
     * @throws Exception
     */
    List<Form11Entity> getForm11Data(LinkedTreeMap criteria, String sessionId) throws Exception;

    /**
     *
     * @param criteria
     * @param sessionId
     * @return
     * @throws Exception
     */
    ErrorEntity setForm11Data(LinkedTreeMap criteria, String sessionId) throws Exception;

    /**
     *
     * @param criteria
     * @param sessionId
     * @return
     * @throws Exception
     */
    ErrorEntity setForm14Data(LinkedTreeMap criteria, String sessionId) throws Exception;




    /**
     *
     * Form 14 Data
     *
     * @param criteria      -- criteria datas
     * @param sessionId     -- session id
     * @return
     * @throws Exception
     */
    List<Form14Entity> getForm14Data(LinkedTreeMap criteria, String sessionId) throws Exception;


    /**
     *
     * Form 46 Data
     *
     * @param criteria      -- criteria datas
     * @param sessionId     -- session id
     * @return
     * @throws Exception
     */
    List<Form46Entity> getForm46Data(LinkedTreeMap criteria, String sessionId) throws Exception;

    /**
     *
     * @param criteria
     * @param sessionId
     * @return
     * @throws Exception
     */
    ErrorEntity setForm46Data(LinkedTreeMap criteria, String sessionId) throws Exception;

    /**
     *
     * @param criteria
     * @param sessionId
     * @return
     * @throws Exception
     */
    List<City> getCityData(LinkedTreeMap criteria, String sessionId) throws Exception;


    /**
     *  new city registration
     * @param insdata
     * @param sessionId
     * @return
     * @throws Exception
     */
    ErrorEntity setCityData (LinkedTreeMap insdata, String sessionId) throws Exception;

    /**
     *
     * @param criteria
     * @param sessionId
     * @return
     * @throws Exception
     */
    List<Sum> getSumData(LinkedTreeMap criteria, String sessionId) throws Exception;


    /**
     *  new city registration
     * @param insdata
     * @param sessionId
     * @return
     * @throws Exception
     */
    ErrorEntity setSumData (LinkedTreeMap insdata, String sessionId) throws Exception;

    /**
     * Fetch users
     *
     * @param credata
     * @param sessionId
     * @return
     * @throws Exception
     */
    List<User> getUserData (LinkedTreeMap credata, String sessionId) throws Exception;

    /**
     *  new user registration
     * @param insdata
     * @param sessionId
     * @return
     * @throws Exception
     */
    ErrorEntity setUserData (LinkedTreeMap insdata, String sessionId) throws Exception;

    /**
     *
     * @param criteria
     * @param sessionId
     * @return
     * @throws Exception
     */
    List<BagHorooEntity> getBagHorooData(LinkedTreeMap criteria, String sessionId) throws Exception;

    /**
     *
     * Form 43 Data
     *
     * @param criteria      -- criteria datas
     * @param sessionId     -- session id
     * @return
     * @throws Exception
     */
    List<Form43Entity> getForm43Data(LinkedTreeMap criteria, String sessionId) throws Exception;

    /**
     *
     * @param criteria
     * @param sessionId
     * @return
     * @throws Exception
     */
    ErrorEntity setForm43Data(LinkedTreeMap criteria, String sessionId) throws Exception;

    /**
     * Fetch Menu List
     *
     * @param criteria
     * @param sessionId
     * @return
     * @throws Exception
     */
    List<MenuEntity> getMenuList(LinkedTreeMap criteria, String sessionId) throws Exception;

    /**
     * Fetch Role List
     *
     * @param criteria
     * @param sessionId
     * @return
     * @throws Exception
     */
    List<Role> getRoleList(LinkedTreeMap criteria, String sessionId) throws Exception;

    /**
     * Menu Role
     *
     * @param criteria
     * @param sessionId
     * @return
     * @throws Exception
     */
    ErrorEntity setMenuRole(LinkedTreeMap criteria, String sessionId) throws Exception;

    /**
     * Fetch Menu Roles List
     *
     * @param criteria
     * @param sessionId
     * @return
     * @throws Exception
     */
    List<MenuRoles> getMenuRole(LinkedTreeMap criteria, String sessionId) throws Exception;

    /**
     * Column chart data
     *
     * @param criteria
     * @param sessionId
     * @param menuid
     * @return
     * @throws Exception
     */
    BasicDBObject getForm08ColumnChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;

    /**
     * Pie chart data
     *
     * @param criteria
     * @param sessionId
     * @param menuid
     * @return
     * @throws Exception
     */
    List<BasicDBObject> getForm08PieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;

    /**
     *
     * Form 08 Data
     *
     * @param criteria      -- criteria datas
     * @param sessionId     -- session id
     * @return
     * @throws Exception
     */
    List<Form08HavsraltEntity> getForm08HavsraltData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;

    /**
     *
     * @param criteria
     * @param sessionId
     * @return
     * @throws Exception
     */
    ErrorEntity setForm08HavsraltData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;


    List<Form09HavsraltEntity> getForm09HavsraltData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;

    /**
     *
     * @param criteria
     * @param sessionId
     * @return
     * @throws Exception
     */
    ErrorEntity setForm09HavsraltData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;


    /**
     * Write Favorite Page log
     *
     * @param sessionId
     * @param menuid
     * @return
     * @throws Exception
     */
    ErrorEntity writeFavoriteMenuLog(String sessionId, String menuid) throws Exception;

    /**
     *
     * @param sessionId
     * @return
     * @throws Exception
     */
    List<FavoritePages> getFavoriteMenuList(String sessionId) throws Exception;

    /**
     * Get Form 13a data
     *
     * @param criteria
     * @param sessionId
     * @param menuid
     * @return
     * @throws Exception
     */
    List<Form13aEntity> getForm13aData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;

    /**
     * Inser Form 13a new Data
     *
     * @param criteria
     * @param sessionId
     * @param menuid
     * @return
     * @throws Exception
     */
    ErrorEntity setForm13aData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;

    /**
     *
     * @param credata
     * @param sessionId
     * @return
     * @throws Exception
     */
    List<User> getAllUserList (LinkedTreeMap credata, String sessionId) throws Exception;


    /* CHART */
    List<BasicDBObject> getForm08HavsraltPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;

    BasicDBObject getForm08HavsraltColumnChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;

    List<BasicDBObject> getForm09PieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;

    BasicDBObject getForm09ColumnChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;


    List<BasicDBObject> getForm09HavsraltPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;

    BasicDBObject getForm09HavsraltColumnChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;

    List<BasicDBObject> getForm11PieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;

    BasicDBObject getForm11ColumnChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;


    BasicDBObject getForm08HightChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;

    /**
     * Hight chart ashiglaj Pie chart haruulah function
     *
     * @param criteria
     * @param sessionId
     * @param menuid
     * @return
     * @throws Exception
     */
    BasicDBObject getForm08HightPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;

    //Form 08 havsralt hight chart begin
    BasicDBObject getForm08HavHightChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;
    BasicDBObject getForm08HavHightPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;
    //Form 08 havsralt hight chart end

    //Form 09 havsralt hight chart begin
    BasicDBObject getForm09HavHightChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;
    BasicDBObject getForm09HavHightPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;
    //Form 09 havsralt hight chart end

    //Form 11 havsralt hight chart begin
    BasicDBObject getForm11HightChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;
    BasicDBObject getForm11HightPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;
    //Form 11 havsralt hight chart end

    //Form 09 havsralt hight chart begin
    BasicDBObject getForm09HightChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;
    BasicDBObject getForm09HightPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;
    //Form 09 havsralt hight chart end

    //Form 46 havsralt hight chart begin
    BasicDBObject getForm46HightChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;
    BasicDBObject getForm46HightPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;
    //Form 46 havsralt hight chart end

    //Form 13a havsralt hight chart begin
    BasicDBObject getForm13aHightChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;
    BasicDBObject getForm13aHightPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;
    //Form 13a havsralt hight chart end
    

    //Form 14 havsralt hight chart begin
    BasicDBObject getForm14HightChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;
    BasicDBObject getForm14HightPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;
    //Form 14 havsralt hight chart end


    //Form 51 havsralt hight chart begin
    BasicDBObject getForm51HightChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;
    BasicDBObject getForm51HightPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;
    //Form 51 havsralt hight chart end

    //Form 52 havsralt hight chart begin
    BasicDBObject getForm52HightChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;
    BasicDBObject getForm52HightPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;
    //Form 52 havsralt hight chart end

    //Form 53 havsralt hight chart begin
    BasicDBObject getForm53HightChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;
    BasicDBObject getForm53HightPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;
    //Form 53 havsralt hight chart end

    //Form 61 havsralt hight chart begin
    BasicDBObject getForm61HightChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;
    BasicDBObject getForm61HightPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;
    //Form 61 havsralt hight chart end

    //Form 62 havsralt hight chart begin
    BasicDBObject getForm62HightChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;
    BasicDBObject getForm62HightPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;
    //Form 62 havsralt hight chart end

    BasicDBObject getStatistic(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception;

}