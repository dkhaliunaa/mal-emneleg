package com.hospital;

import com.google.gson.internal.LinkedTreeMap;
import com.model.hos.ErrorEntity;

/**
 * Created by 24be10264 on 9/28/2017.
 */
public interface IReportService {

    /**
     * HFORM 08 REPORT
     *
     * @param criteria
     * @param sessionId
     * @return
     * @throws Exception
     */
    ErrorEntity hform08Report(LinkedTreeMap criteria, String sessionId, String file) throws Exception;

    /**
     * Hform 11 Report
     *
     * @param criteria
     * @param sessionId
     * @param menuId
     * @return
     * @throws Exception
     */
    ErrorEntity hform11Report(LinkedTreeMap criteria, String sessionId, String menuId, String file) throws Exception;

    ErrorEntity hform09Report(LinkedTreeMap criteria, String sessionId, String file) throws Exception;

    ErrorEntity hform14Report(LinkedTreeMap criteria, String sessionId, String file) throws Exception;

    ErrorEntity hform43Report(LinkedTreeMap criteria, String sessionId) throws Exception;

    ErrorEntity hform46Report(LinkedTreeMap criteria, String sessionId, String file) throws Exception;

    ErrorEntity hform51Report(LinkedTreeMap criteria, String sessionId,String file) throws Exception;

    ErrorEntity hform61Report(LinkedTreeMap criteria, String sessionId,String file) throws Exception;

    ErrorEntity hform62Report(LinkedTreeMap criteria, String sessionId,String file) throws Exception;

    ErrorEntity hformGalzuuReport(LinkedTreeMap criteria, String sessionId, String file) throws Exception;

    ErrorEntity hform52Report(LinkedTreeMap criteria, String sessionId,String file) throws Exception;

    ErrorEntity hform53Report(LinkedTreeMap criteria, String sessionId,String file) throws Exception;

    ErrorEntity hform08HavsraltReport(LinkedTreeMap criteria, String sessionId, String file) throws Exception;

    ErrorEntity hform09HavsraltReport(LinkedTreeMap criteria, String sessionId,String file) throws Exception;

    ErrorEntity hform013aReport(LinkedTreeMap criteria, String sessionId, String file) throws Exception;
}
