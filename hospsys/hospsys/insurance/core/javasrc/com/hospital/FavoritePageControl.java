package com.hospital;

import com.enumclass.ErrorType;
import com.jolbox.bonecp.BoneCP;
import com.model.FavoritePages;
import com.model.hos.ErrorEntity;
import com.mongodb.BasicDBObject;
import org.omg.CORBA.SystemException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Хамгийн их ашигласан меню дээр мэдээлэл удирдах класс
 *
 * Created by 24be10264 on 8/19/2016.
 */
public class FavoritePageControl {
    private BoneCP boneCP;

    public FavoritePageControl(BoneCP boneCP){
        this.boneCP = boneCP;
    }


    /**
     * Хамгийн их ашигласан цэсийг орж ирсэн нөхцлөөр татах арга
     *
     * @return
     */
    public List<FavoritePages> getFavoritePages(FavoritePages favoritePages){
        int stIndex = 1;
        List<FavoritePages> favoritePagesList = null;

        String query_sel = "SELECT favorite_page_log.`id`, favorite_page_log.`menu_id`, favorite_page_log.`cre_at`, " +
                "favorite_page_log.`cre_by`, `use_count`, h_menu.menu_name_mn,  h_menu.menu_name_en " +
                "FROM `favorite_page_log` " +
                "INNER JOIN h_menu ON `favorite_page_log`.`menu_id` = h_menu.formid WHERE ";

        if (favoritePages != null && favoritePages.getMenuid() != null && !favoritePages.getMenuid().isEmpty()){
            query_sel += " `favorite_page_log`.`menu_id` = ? AND ";
        }
        if (favoritePages.getCreate_at() != null && !favoritePages.getCreate_at().toString().isEmpty()){
            query_sel += " `favorite_page_log`.`cre_at` = ? AND ";
        }
        if (favoritePages.getCreate_by() != null && !favoritePages.getCreate_by().isEmpty()){
            query_sel += " `favorite_page_log`.`cre_by` = ? AND ";
        }
        if (favoritePages.getCount() != 0){
            query_sel += " `favorite_page_log`.`use_count` = ? AND ";
        }

        query_sel += " `favorite_page_log`.id > 0 ORDER BY use_count DESC LIMIT 10";

        try (Connection connection = boneCP.getConnection()) {
            try {
                PreparedStatement statement = connection.prepareStatement(query_sel);

                if (favoritePages != null && favoritePages.getMenuid() != null && !favoritePages.getMenuid().isEmpty()){
                    statement.setString(stIndex ++, favoritePages.getMenuid());
                }
                if (favoritePages.getCreate_at() != null && !favoritePages.getCreate_at().toString().isEmpty()){
                    statement.setDate(stIndex ++, favoritePages.getCreate_at());
                }
                if (favoritePages.getCreate_by() != null && !favoritePages.getCreate_by().isEmpty()){
                    statement.setString(stIndex ++, favoritePages.getCreate_by());
                }
                if (favoritePages.getCount() != 0){
                    statement.setInt(stIndex ++, favoritePages.getCount());
                }

                ResultSet resultSet = statement.executeQuery();

                favoritePagesList = new ArrayList<>();

                int index = 1;

                while (resultSet.next()) {
                    FavoritePages entity = new FavoritePages();

                    entity.setId(resultSet.getInt("id"));
                    entity.setMenuid(resultSet.getString("menu_id"));
                    entity.setCreate_at(resultSet.getDate("cre_at"));
                    entity.setCreate_by(resultSet.getString("cre_by"));
                    entity.setCount(resultSet.getInt("use_count"));

                    entity.put("menuname_mn", resultSet.getString("menu_name_mn"));
                    entity.put("menuname_en", resultSet.getString("menu_name_en"));

                    entity.put("index", index++);

                    favoritePagesList.add(entity);
                }

                if (statement != null) statement.close();
                if (resultSet != null) resultSet.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return favoritePagesList;
    }//end function

    /**
     * Шинээр хамгийн их ашигласан цэс бүртгэх
     * @param newFavoritePages
     */
    public ErrorEntity insertFavoritePage(FavoritePages newFavoritePages){
        PreparedStatement statement = null;

        if (newFavoritePages == null) new ErrorEntity(ErrorType.FATAL, Long.valueOf(1058));

        String query_login = "INSERT INTO `favorite_page_log`(`menu_id`, `cre_at`, `cre_by`, `use_count`) " +
                "VALUES (?, ?, ?, ?)";

        try (Connection connection = boneCP.getConnection()) {
            try{
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query_login);

                statement.setString(1, newFavoritePages.getMenuid());
                statement.setDate(2, newFavoritePages.getCreate_at());
                statement.setString(3, newFavoritePages.getCreate_by());
                statement.setInt(4, newFavoritePages.getCount());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1){
                    connection.commit();
                    return new ErrorEntity(ErrorType.INFO, Long.valueOf(1056));
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }finally {
                if (statement != null)statement.close();
                connection.close();
            }

        }catch (Exception ex){

        }

        return new ErrorEntity(ErrorType.ERROR, Long.valueOf(1057));
    }//end function

    /**
     * Орж ирсэн объектийг засаж оруулах арга
     *
     * @param favoritePages
     * @return
     */
    public ErrorEntity updateFavoritePage(FavoritePages favoritePages){

        int stIndex = 1;
        String query = null;
        PreparedStatement statement = null;

        if (favoritePages == null) new ErrorEntity(ErrorType.INFO, Long.valueOf(1059));

        query = "UPDATE `favorite_page_log` SET    ";

        if (favoritePages != null && favoritePages.getMenuid() != null && !favoritePages.getMenuid().isEmpty()){
            query += " `menu_id` = ?, ";
        }
        if (favoritePages.getCreate_by() != null && !favoritePages.getCreate_by().isEmpty()){
            query += " `cre_by` = ?, ";
        }
        if (favoritePages.getCount() != 0){
            query += " `use_count` = ?, ";
        }

        query += " cre_at = ?  ";
        query += " WHERE  id   = ? ";

        try (Connection connection = boneCP.getConnection()) {
            try {
                connection.setAutoCommit(false);
                statement = connection.prepareStatement(query);

                if (favoritePages != null && favoritePages.getMenuid() != null && !favoritePages.getMenuid().isEmpty()){
                    statement.setString(stIndex ++, favoritePages.getMenuid());
                }
                if (favoritePages.getCreate_by() != null && !favoritePages.getCreate_by().isEmpty()){
                    statement.setString(stIndex ++, favoritePages.getCreate_by());
                }
                if (favoritePages.getCount() != 0){
                    statement.setInt(stIndex ++, favoritePages.getCount());
                }

                statement.setDate(stIndex ++, favoritePages.getCreate_at());
                statement.setInt(stIndex ++, favoritePages.getId());

                int insertCount = statement.executeUpdate();

                if (insertCount == 1) {
                    connection.commit();
                    return new ErrorEntity(ErrorType.INFO, Long.valueOf(1060));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (statement != null) statement.close();
                connection.close();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }


        return new ErrorEntity(ErrorType.ERROR, Long.valueOf(1061));
    }//end function

    /**
     * Insert
     *
     * @param username
     * @param menuid
     */
    public void writeLog(String username, String menuid) {
        try{
            FavoritePages favoritePages = new FavoritePages();
            favoritePages.setMenuid(menuid);
            favoritePages.setCreate_by(username);

            List<FavoritePages> favoritePagesList = getFavoritePages(favoritePages);

            java.util.Date utilDate = new java.util.Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            favoritePages.setCreate_at(sqlDate);

            if (favoritePagesList != null && favoritePagesList.size() > 0){
                //update count
                int cc = favoritePagesList.get(0).getCount() + 1;
                favoritePages.setCount(cc);
                favoritePages.setId(favoritePagesList.get(0).getId());
                updateFavoritePage(favoritePages);
            }
            else {
                insertFavoritePage(favoritePages);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
