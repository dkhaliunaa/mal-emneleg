package com.hospital;

import com.auth.HospService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by 23949959 on 11/19/2015.
 */
public class ReportServlet extends HttpServlet {
    protected HospService service;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        service = new ReportServiceImpl();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        response.setContentType("text/html");
        response.getWriter().write(service.welcome());
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        try {
            service.dispatch(request, response);
        } catch (Throwable t) {
            System.err.println(t);
        }
    }
}