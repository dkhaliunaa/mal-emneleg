package com.hospital;

import com.Config;
import com.db.CoreDb;
import com.utils.CharsetConverter;
import com.jolbox.bonecp.BoneCP;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.log4j.Logger;

import javax.naming.Context;
import javax.naming.NameAlreadyBoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import java.io.UnsupportedEncodingException;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Random;

/**
 * Windows Domain сервертэй ажиллах туслах класс
 * 
 * @author ub
 * 
 */
public class Ldap {
	static final Logger logger = Logger.getLogger(Ldap.class.getName());
	// LDAP server
	public static String LDAP_HOST;
	public static int LDAP_PORT;
	public static int LDAP_SSL_PORT;
	public static String LDAP_DOMAIN;
	public static String LDAP_DC1;
	public static String LDAP_DC2;
	public static String LDAP_DN;
	public static String LDAP_ADMIN;
	public static String LDAP_PASS;
	static String ATTRIBUTE_FOR_USER = "sAMAccountName";

	static BoneCP db;

	static {
		db = CoreDb.get();

        Properties props = null;
        try {
            props = Config.get("insurance.properties");

            LDAP_HOST = props.getProperty("LDAP_HOST").toString();
            LDAP_PORT = Integer.parseInt(props.getProperty("LDAP_PORT").toString());
            LDAP_SSL_PORT = Integer.parseInt(props.getProperty("LDAP_SSL_PORT").toString());
            LDAP_DOMAIN = props.getProperty("LDAP_DOMAIN").toString();
            LDAP_DC1 = props.getProperty("LDAP_DC1").toString();
            LDAP_DC2 = props.getProperty("LDAP_DC2").toString();
            LDAP_DN = props.getProperty("LDAP_DN").toString();
            LDAP_ADMIN = props.getProperty("LDAP_ADMIN").toString();
            LDAP_PASS = props.getProperty("LDAP_PASS").toString();

        } catch (Exception e) {
            e.printStackTrace();
        }



	}

	/**
	 * LDAP user login
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public static Attributes authenticateUser(String username, String password) {
		// String returnedAtts[] = { "sn", "givenName", "mail", "fullName" };
		String searchFilter = "(&(objectClass=user)(" + ATTRIBUTE_FOR_USER
				+ "=" + username + "))";

		// Create the search controls
		SearchControls searchCtls = new SearchControls();
		searchCtls.setReturningAttributes(null);

		// Specify the search scope
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		String searchBase = LDAP_DN;
		Hashtable<String, String> ctx = new Hashtable<>();
		ctx.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");

		ctx.put(Context.PROVIDER_URL, "ldap://" + LDAP_HOST + ":"
				+ LDAP_PORT);
		ctx.put(Context.SECURITY_AUTHENTICATION, "simple");

		ctx.put(Context.SECURITY_PRINCIPAL, username + "@" + LDAP_DOMAIN);
		ctx.put(Context.SECURITY_CREDENTIALS, password);
		LdapContext ctxGC = null;
		try {
			ctxGC = new InitialLdapContext(ctx, null);
			// Search for objects in the GC using the filter
			NamingEnumeration<SearchResult> answer = ctxGC.search(searchBase,
					searchFilter, searchCtls);
			while (answer.hasMoreElements()) {
				SearchResult sr = answer.next();
				Attributes attrs = sr.getAttributes();
				if (attrs != null) {
					return attrs;
				}
			}

		} catch (Exception e) {
			logger.debug(e.getMessage());
		}
		return null;
	}

	/**
	 * LDAP user create
	 * 
	 * @param domain
	 * @return
	 */
	public static BasicDBObject createUser(String domain) {

		BasicDBObject obj = new BasicDBObject();
		
		//DBObject emp = db.getCollection("Ажилтан").findOne(new BasicDBObject("_id", domain));
		//DBObject dept = db.getCollection("Нэгж").findOne(new BasicDBObject("_id", emp.get("нэгж").toString()));
		
		Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, "ldaps://" + LDAP_HOST + ":" + LDAP_SSL_PORT);// /dc=golomtbank,dc=local
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL,  LDAP_ADMIN + "@" + LDAP_DOMAIN);
        env.put(Context.SECURITY_CREDENTIALS, LDAP_PASS);
        env.put(Context.SECURITY_PROTOCOL, "ssl");
		
        try {
                
            // Create the initial context
            DirContext ctx = new InitialDirContext(env);

            // Attributes to represent the user 
            Attributes attrs = new BasicAttributes(true); // case-ignore
            
//            attrs.put("uid", domain);
            
            //String cn = getCN(emp, dept);
            
            attrs.put("uid", domain);
            //attrs.put("cn", cn);
            //attrs.put("sn", mnToEn(emp.get("эцэг").toString()));
            //attrs.put("givenName", mnToEn(emp.get("нэр").toString()));
            //attrs.put("mail", firstNonNull(emp.get("имэйл"), " ").toString());
            //attrs.put("telephonenumber", firstNonNull(emp.get("утас"), " ").toString());
            //attrs.put("displayname", getDisplayName(emp, dept));
            
            attrs.put("userprincipalname", domain + "@" + LDAP_DOMAIN);
            attrs.put("samaccountname", domain);
            
    		String password = String.format("Golomt%s", new Random().nextInt(999));
            attrs.put("unicodePwd", String.format("\"%s\"", password).getBytes("UTF-16LE"));
            attrs.put("userAccountControl", "512");
            
            // objectClass
            Attribute attr = new BasicAttribute("objectClass");
            attr.add("top");
            attr.add("person");
            attr.add("organizationalPerson");
            attr.add("user");
            attrs.put(attr);
            
            // Create the user account
            //ctx.createSubcontext("CN=" + getCN(emp, dept) + ","+ dept.get("ldap_ou").toString() + ",DC="+LDAP_DC1+",DC="+LDAP_DC2+"", attrs);

            obj.put("result", password);
            
            // close 
            ctx.close();
            
        } catch (NameAlreadyBoundException e) {
            e.printStackTrace();
            obj.put("error", e.getMessage());
        } catch (NamingException e) {
            e.printStackTrace();
            obj.put("error", e.getMessage());
        } catch (UnsupportedEncodingException e) {
        	e.printStackTrace();
        	obj.put("error", e.getMessage());
        }

        return obj;
        
	}
	
	
	
	/**
	 * modify LDAP user email
	 * 
	 * @param domain
	 * @return
	 */
	public static BasicDBObject modifyUser(String domain) {

		BasicDBObject obj = new BasicDBObject();
		
		//DBObject emp = db.getCollection("Ажилтан").findOne(new BasicDBObject("_id", domain));
		//DBObject dept = db.getCollection("Нэгж").findOne(new BasicDBObject("_id", emp.get("нэгж").toString()));
		
		Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, "ldaps://" + LDAP_HOST + ":" + LDAP_SSL_PORT);// /dc=golomtbank,dc=local
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL,  LDAP_ADMIN + "@" + LDAP_DOMAIN);
        env.put(Context.SECURITY_CREDENTIALS, LDAP_PASS);
        env.put(Context.SECURITY_PROTOCOL, "ssl");
		
        try {
                
            // Create the initial context
            DirContext ctx = new InitialDirContext(env);
            
            ModificationItem[] mods = new ModificationItem[5];
            
            //Attribute mod0 = new BasicAttribute("sn", mnToEn(emp.get("эцэг").toString()));
            //Attribute mod1 = new BasicAttribute("givenName", mnToEn(emp.get("нэр").toString()));
            //Attribute mod2 = new BasicAttribute("mail", firstNonNull(emp.get("имэйл"), " ").toString());
            //Attribute mod3 = new BasicAttribute("telephonenumber", firstNonNull(emp.get("утас"), " ").toString());
            //Attribute mod4 = new BasicAttribute("displayname", getDisplayName(emp, dept));
            
            //mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, mod0);
            //mods[1] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, mod1);
            //mods[2] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, mod2);
            //mods[3] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, mod3);
            //mods[4] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, mod4);
            
            String oldCn = getOldCn(domain);
            //String newCn = getCN(emp, dept);
            
//            System.out.println("oldCn " + oldCn);
//            System.out.println("newCn " + "CN=" + newCn + "," + dept.get("ldap_ou").toString() + ",DC="+Main.LDAP_DC1+",DC="+Main.LDAP_DC2+"");
            
            // CN нэрийг өөрчлөхөөс гадна OU г өөрчлөж харгалзах нэгжийн group руу оруулна.
            //ctx.rename(oldCn, "CN=" + newCn + "," + dept.get("ldap_ou").toString() + ",DC="+LDAP_DC1+",DC="+LDAP_DC2+"");
            
            // Ажилтаны мэдээллийг шинэчлэнэ.
            //ctx.modifyAttributes("CN=" + newCn + "," + dept.get("ldap_ou").toString() + ",DC="+LDAP_DC1+",DC="+LDAP_DC2+"", mods);
            
            obj.put("result", "success");
            
            // close 
            ctx.close();

        } catch (NamingException e) {
            e.printStackTrace();
            obj.put("error", e.getMessage());
        }
        
    	return obj;
    	
	}

	public static String getOldCn(String domain) {
        
		Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, "ldap://" + LDAP_HOST + ":" + LDAP_PORT + "/DC="+LDAP_DC1+",DC="+LDAP_DC2);
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL,  LDAP_ADMIN + "@" + LDAP_DOMAIN);
        env.put(Context.SECURITY_CREDENTIALS, LDAP_PASS);
        
        String cn = "";
		NamingEnumeration results = null;
        try {
                
            // Create the initial context
            DirContext ctx = new InitialDirContext(env);

//            LdapContext user = (LdapContext)ctx.lookup("CN=MTG - PHH - Turbold Dugarsuren");
//            
//            System.out.println(user);
            
            String searchFilter = "(&(objectClass=user)(sAMAccountName="+domain+"))";
            
            SearchControls controls = new SearchControls();
            controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            controls.setCountLimit(300000);
            results = ctx.search("", searchFilter, controls);
            while (results.hasMoreElements()) {
                SearchResult searchResult = (SearchResult) results.next();
                Attributes attributes = searchResult.getAttributes();
                cn = attributes.get("DistinguishedName").toString();
                cn = cn.replace("distinguishedName: ", "");
            }

            // close 
            ctx.close();

        } catch (NamingException e) {
            e.printStackTrace();
        }
        
        return cn;
        		
	}
	
	
	private static Object firstNonNull(Object o1, Object o2){
		if (o1 != null) return o1;
	    if (o2 != null) return o2;
	    return "";
	    //throw new ArgumentError('All arguments were null');
	}
	
	
	/**
	 * 
	 * LDAP Дэлгэцэнд харагдах нэр
	 * 
	 * @param emp
	 * @return displayName
	 */
	private static String getDisplayName(DBObject emp, DBObject dept){
		
		/*
		 * Дэлгэцэнд харуулах нэр, 
		 * mail болон messenger т харагдана
		 */
		String displayName = "";
		
		/*
		 * mask. 
		 * 
		 * Доорх хэлбэрийн mask ууд байна
		 * 
		 * .  %2$s
		 * . %2$s - %1$s
		 * .%2$s - %1$s
		 * %1$s - KhAM - %2$s
		 * %1$s - %2$s
		 * 
		 * CEO эсвэ ТУЗын дарга бол '.  ', Төвийн нэгжийн захирал бол '. ', СТТ гийн захирал бол '.' хоосон зайгүй цэг авна.
		 * 
		 * Display Name ийг ямар ч байдлаар тохируулж болно. Гол нь хоёр л аргумент авдаг string байна гэдгийг тооцох ёстой.
		 * 1-р аргумент нь ажилтаны нэр, 2-р аргумент нь нэгжийн нэр. Тиймээс уг хоёр аргументын байршлыг солиж, эсвэл prefix авах зэргээр тохируулж болно.
		 * Харин Дотуур утасны ажилтаны нэрийн ард дотуур утас автоматаар бичигдэх болно.
		 *  		 * 
		 * Үүнийг "m_Албантушаал.ldap_display_mask" багананд тохируулах ёстой.
		 * 
		 *
		 */
		String ldap_display_mask = "";
		
		// Ажилтаны албан тушаалаас ldap-д хамааралтай мэдээллүүдийг авч байна.
		if(emp.containsField("ат") && emp.get("ат") != null && emp.get("ат").toString().length() > 0){
			//DBObject pos = db.getCollection("m_Албантушаал").findOne(new BasicDBObject("нэр", emp.get("ат").toString()));
			//if(pos != null){
			//	ldap_display_mask = pos.get("ldap_display_mask").toString();
			//} else {
			//	ldap_display_mask = "%1$s - %2$s";
			//}
		}
		
		// Дэлгэцэнд харагдах нэрийг оноож байна.
		displayName = String.format(ldap_display_mask, dept.get("ldap_name").toString(), mnToEn(emp.get("нэр").toString()));
		
		// Хэрэв дотуур утастай бол утасны дугаарыг ард нь залгана
        if(emp.containsField("утас") && emp.get("утас") != null && emp.get("утас").toString().length() > 0){
        	displayName = String.format("%1$s - %2$s", displayName, emp.get("утас").toString());
        }
        
		return displayName;
	}
	
	/**
	 * 
	 * LDAP CN
	 * 
	 * Захирал эсвэл ажилтан ижил байна.
	 * Нэгж - Нэр Эцэг
	 * 
	 * @param emp
	 * @return cn
	 */
	private static String getCN(DBObject emp, DBObject dept){
		
		String ldap_display_mask = "";
		
		// Ажилтаны албан тушаалаас ldap-д хамааралтай мэдээллүүдийг авч байна.
		if(emp.containsField("ат") && emp.get("ат") != null && emp.get("ат").toString().length() > 0){
			/*DBObject pos = db.getCollection("m_Албантушаал").findOne(new BasicDBObject("нэр", emp.get("ат").toString()));
			if(pos != null){
				ldap_display_mask = pos.get("ldap_display_mask").toString();
			} else {
				ldap_display_mask = "%1$s - %2$s";
			}*/
		}
		
		// Дэлгэцэнд харагдах нэрийг оноож байна.
		String cn = String.format(ldap_display_mask, dept.get("ldap_name").toString(), mnToEn(emp.get("нэр").toString()) + " " + mnToEn(emp.get("эцэг").toString()));
		return cn;
	}
	
	/**
	 * 
	 * mn to en
	 * 
	 * @param str
	 * @return cn
	 */
	private static String mnToEn(String str){
		String str2 = WordUtils.capitalize(CharsetConverter.utf8_latin(str));
		return str2;
	}

	public static void main(String[] args) throws Exception {

//		System.out.println(Ldap.createUser("20db9983"));
		System.out.println(Ldap.modifyUser("20db9983"));

	}
}