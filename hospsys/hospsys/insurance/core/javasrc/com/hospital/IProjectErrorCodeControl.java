package com.hospital;

import com.model.ProjectErrorCode;
import com.mongodb.BasicDBObject;

import java.sql.SQLException;

/**
 * Created by 24be10264 on 12/17/2016.
 */
public interface IProjectErrorCodeControl {
    BasicDBObject getErrorCodeMessage(boolean success, ProjectErrorCode errorCode) throws SQLException, Exception;

    BasicDBObject getErrorDescription(boolean success, ProjectErrorCode errorCode) throws SQLException, Exception;
}
