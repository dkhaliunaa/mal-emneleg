package com.hospital;

import com.auth.HospService;
import com.enumclass.ErrorType;
import com.google.gson.internal.LinkedTreeMap;
import com.hospital.app.*;
import com.model.FavoritePages;
import com.model.Role;
import com.model.User;
import com.model.hos.*;
import com.mongodb.BasicDBObject;
import com.rpc.RpcHandler;
import org.bson.types.ObjectId;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class HospitalServiceImpl extends HospService implements HospitalService {

    private CheckUserRoleControl roleControl = null;
    private ErrorEntity errorEntity = null;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    public HospitalServiceImpl() {
        handler = new RpcHandler<>(this, HospitalService.class);
    }

    @Override
    public BasicDBObject checkSession(String sessionId) {
        BasicDBObject basicDBObject = new BasicDBObject();

        User loginUser = getUserInfo(sessionId);

        basicDBObject.put("session", true);

        if (loginUser == null) {
            basicDBObject.put("session", false);
        }

        return basicDBObject;
    }

    @Override
    public List<BasicDBObject> getRoles(String sessionId) {
        User user = sessionUser.get(sessionId);
        if (user == null) return null;

        try {
            //Check Role
            roleControl = new CheckUserRoleControl(super.conn);

            if (roleControl == null) {
                return null;
            }

            return roleControl.getUserAccessMenuList(user);
            //End Check Role
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }


        return null;
    }

    @Override
    public List<NewsEntity> getNewsData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: get news data");

        try {
            INewsService newsService = new NewsServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            NewsEntity newsEntity = new NewsEntity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(newsEntity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            newsEntity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(newsEntity.TITLE)) {
                        newsEntity.setTitle(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(newsEntity.CONTENT)) {
                        newsEntity.setContent(String.valueOf(pairs.getValue()));
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            try {
                newsEntity.setCreby(user.getUsername());
            } catch (Exception e) {
                newsEntity.setCreby("");
            }


            if (newsEntity != null) {
                return newsService.getListData(newsEntity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public ErrorEntity setNewsData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: set news data");

        try {
            String command = "EDT";
            INewsService newsService = new NewsServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            NewsEntity newsEntity = new NewsEntity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(newsEntity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                            if (pairs.getValue().toString().contains(".")) {
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            newsEntity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    } else if (pairs.getKey().equals(newsEntity.TITLE)) {
                        newsEntity.setTitle(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(newsEntity.CONTENT)) {
                        newsEntity.setContent(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().toString().equalsIgnoreCase("command")) {
                        command = String.valueOf(pairs.getValue());
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (command.equalsIgnoreCase("INS")) {
                newsEntity.setDelflg("N");
                newsEntity.setActflg("Y");
                try {
                    newsEntity.setCreby(user.getUsername());
                    newsEntity.setModby(user.getUsername());
                } catch (NullPointerException e) {
                    newsEntity.setCreby("");
                    newsEntity.setModby("");
                }
                Date utilDate = new Date();
                java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                newsEntity.setCreat(sqlDate);
                newsEntity.setModAt(sqlDate);

                errorEntity = newsService.insertNewData(newsEntity);

            } else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL") ||
                    command.equalsIgnoreCase("ACT")) {
                int quality = 2;

                if (command.equalsIgnoreCase("DEL")) {
                    newsEntity.setDelflg("Y");
                    quality = 3;
                } else if (command.equalsIgnoreCase("ACT")) {
                    newsEntity.setActflg("N");
                }

                errorEntity = newsService.updateData(newsEntity);

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return errorEntity;
    }

    @Override
    public List<Form08Entity> getForm08Data(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 08 data");

        try {
            IForm08Service form08Service = new Form08ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form08Entity form08Entity = new Form08Entity();

            Iterator it = criteria.entrySet().iterator();

            //Check Role
            roleControl = new CheckUserRoleControl(super.conn);

            if (roleControl == null) {
                return null;
            }

            menuid = menuid.replaceAll("#", "");

            if (!roleControl.checkRole(user, menuid, 1)) {
                return null;
            }
            //End Check Role

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form08Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            form08Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(form08Entity.AIMAG)) {
                        form08Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.SUM)) {
                        form08Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.MALTURUL)) {
                        form08Entity.setMalTurul(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.ARGA_HEMJEE_NER)) {
                        form08Entity.setArgaHemjeeNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.NER)) {
                        form08Entity.setNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.ZARTSUULSAN_HEMJEE)) {
                        form08Entity.setZartsuulsanHemjee(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.TOLOVLOGOO)) {
                        form08Entity.setTolovlogoo(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.GUITSETGEL)) {
                        form08Entity.setGuitsetgel(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.HUVI)) {
                        form08Entity.setHuvi(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.OLGOSON_TUN)) {
                        try {
                            form08Entity.setOLGOSON_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form08Entity.setOLGOSON_TUN(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form08Entity.HEREGLESEN_TUN)) {
                        try {
                            form08Entity.setHEREGLESEN_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form08Entity.setHEREGLESEN_TUN(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form08Entity.USTGASAN_TUN)) {
                        try {
                            form08Entity.setUSTGASAN_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form08Entity.setUSTGASAN_TUN(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form08Entity.NUUTSULSEN_TUN)) {
                        try {
                            form08Entity.setNUUTSULSEN_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form08Entity.setNUUTSULSEN_TUN(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form08Entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form08Entity.setRecordDate(sqlDate);
                    } else if (pairs.getKey().equals(form08Entity.SEARCH_RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form08Entity.setSearchRecordDate(sqlDate);
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (form08Entity.getRecordDate() == null) form08Entity.setRecordDate(sqlDate);
            if (form08Entity.getSearchRecordDate() == null) form08Entity.setSearchRecordDate(sqlDate);

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                form08Entity.setCreby(user.getUsername());
            }


            if (form08Entity != null && form08Entity.getRecordDate() != null && form08Entity.getSearchRecordDate() != null) {
                return form08Service.getListData(form08Entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public ErrorEntity setForm08Data(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 08 data");

        try {
            String command = "EDT";
            IForm08Service form08Service = new Form08ServiceImpl(super.conn);
            //Begin Check Role Step 1
            roleControl = new CheckUserRoleControl(super.conn);

            if (roleControl == null) {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1002)); // Ерөнхий алдаа : Эрх нь хүрэлцэхгүй үед өгөх
                return errorEntity;
            }
            //End Check Role Step 1

            menuid = menuid.replaceAll("#", "");

            User user = sessionUser.get(sessionId);
            Form08Entity form08Entity = new Form08Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form08Entity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                            if (pairs.getValue().toString().contains(".")) {
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            form08Entity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    } else if (pairs.getKey().equals(form08Entity.ARGA_HEMJEE_NER)) {
                        form08Entity.setArgaHemjeeNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.AIMAG)) {
                        form08Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.SUM)) {
                        form08Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.NER)) {
                        form08Entity.setNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.ZARTSUULSAN_HEMJEE)) {
                        form08Entity.setZartsuulsanHemjee(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.MALTURUL)) {
                        form08Entity.setMalTurul(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.TOLOVLOGOO)) {
                        form08Entity.setTolovlogoo(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.GUITSETGEL)) {
                        form08Entity.setGuitsetgel(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.HUVI)) {
                        form08Entity.setHuvi(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.OLGOSON_TUN)) {
                        try {
                            form08Entity.setOLGOSON_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form08Entity.setOLGOSON_TUN(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form08Entity.HEREGLESEN_TUN)) {
                        try {
                            form08Entity.setHEREGLESEN_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form08Entity.setHEREGLESEN_TUN(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form08Entity.USTGASAN_TUN)) {
                        try {
                            form08Entity.setUSTGASAN_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form08Entity.setUSTGASAN_TUN(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form08Entity.NUUTSULSEN_TUN)) {
                        try {
                            form08Entity.setNUUTSULSEN_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form08Entity.setNUUTSULSEN_TUN(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().toString().equalsIgnoreCase("command")) {
                        command = String.valueOf(pairs.getValue());
                    } else if (pairs.getKey().equals(form08Entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (!dv.isEmpty() && dv.length() >= 8) {
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form08Entity.setRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (command.equalsIgnoreCase("INS")) {
                form08Entity.setDelflg("N");
                form08Entity.setActflg("Y");
                if (user != null && (user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                        || user.getRoleCode().equalsIgnoreCase("ADMIN"))) {
                    //Aimag songoh shaardlagagui
                } else {
                    form08Entity.setAimag(user.getCity());
                }
                //form08Entity.setSum(user.getSum());
                form08Entity.setCreby(user.getUsername());
                Date utilDate = new Date();
                java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                form08Entity.setCreat(sqlDate);
                if (form08Entity.getRecordDate() == null) form08Entity.setCreat(sqlDate);

                //Begin Check Role Step 2
                if (roleControl.checkRole(user, menuid, 2)) {
                    errorEntity = form08Service.insertNewData(form08Entity);
                } else {
                    errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1002)); // Ерөнхий алдаа : Эрх нь хүрэлцэхгүй үед өгөх
                }
                //End Check Role Step 2
            } else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL") ||
                    command.equalsIgnoreCase("ACT")) {
                int quality = 2;

                if (command.equalsIgnoreCase("DEL")) {
                    form08Entity.setDelflg("Y");
                    quality = 3;
                } else if (command.equalsIgnoreCase("ACT")) {
                    form08Entity.setActflg("N");
                }

                //Begin Check Role Step 2
                if (roleControl.checkRole(user, menuid, quality)) {
                    errorEntity = form08Service.updateData(form08Entity);
                } else {
                    errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1002)); // Ерөнхий алдаа : Эрх нь хүрэлцэхгүй үед өгөх
                }
                //End Check Role Step 2
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return errorEntity;
    }


    //    Begin-form09
    @Override
    public List<Form09Entity> getForm09Data(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 09 data");

        try {
            IForm09Service form09Service = new Form09ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form09Entity form09Entity = new Form09Entity();

            Iterator it = criteria.entrySet().iterator();


            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form09Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            form09Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(form09Entity.AIMAG)) {
                        form09Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.SUM)) {
                        form09Entity.setSum(form09Entity.SUM);
                    } else if (pairs.getKey().equals(form09Entity.MALTURUL)) {
                        form09Entity.setMalTurul(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.TARILGANER)) {
                        form09Entity.setTarilgaNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.NER)) {
                        form09Entity.setNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.ZARTSUULSANHEMJEE)) {
                        form09Entity.setZartsuulsanHemjee(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.TOLOVLOGOO)) {
                        form09Entity.setTolovlogoo(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.GUITSETGEL)) {
                        form09Entity.setGuitsetgel(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.HUVI)) {
                        form09Entity.setHuvi(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.OLGOSON_TUN)) {
                        try {
                            form09Entity.setOLGOSON_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form09Entity.setOLGOSON_TUN(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form09Entity.HEREGLESEN_TUN)) {
                        try {
                            form09Entity.setHEREGLESEN_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form09Entity.setHEREGLESEN_TUN(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form09Entity.USTGASAN_TUN)) {
                        try {
                            form09Entity.setUSTGASAN_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form09Entity.setUSTGASAN_TUN(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form09Entity.NUUTSULSEN_TUN)) {
                        try {
                            form09Entity.setNUUTSULSEN_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form09Entity.setNUUTSULSEN_TUN(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form09Entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form09Entity.setRecordDate(sqlDate);
                    } else if (pairs.getKey().equals(form09Entity.SEARCH_RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form09Entity.setSearchRecordDate(sqlDate);
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                form09Entity.setCreby(user.getUsername());
            }

            return form09Service.getListData(form09Entity);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }


    @Override
    public ErrorEntity setForm09Data(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 09 data");

        try {
            String command = "EDT";
            IForm09Service form09Service = new Form09ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form09Entity form09Entity = new Form09Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form09Entity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                            if (pairs.getValue().toString().contains(".")) {
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            form09Entity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    } else if (pairs.getKey().equals(form09Entity.TARILGANER)) {
                        form09Entity.setTarilgaNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.NER)) {
                        form09Entity.setNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.ZARTSUULSANHEMJEE)) {
                        form09Entity.setZartsuulsanHemjee(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.MALTURUL)) {
                        form09Entity.setMalTurul(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.TOLOVLOGOO)) {
                        form09Entity.setTolovlogoo(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.GUITSETGEL)) {
                        form09Entity.setGuitsetgel(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.HUVI)) {
                        form09Entity.setHuvi(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.OLGOSON_TUN)) {
                        try {
                            form09Entity.setOLGOSON_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form09Entity.setOLGOSON_TUN(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form09Entity.HEREGLESEN_TUN)) {
                        try {
                            form09Entity.setHEREGLESEN_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form09Entity.setHEREGLESEN_TUN(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form09Entity.USTGASAN_TUN)) {
                        try {
                            form09Entity.setUSTGASAN_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form09Entity.setUSTGASAN_TUN(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form09Entity.NUUTSULSEN_TUN)) {
                        try {
                            form09Entity.setNUUTSULSEN_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form09Entity.setNUUTSULSEN_TUN(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().toString().equalsIgnoreCase("command")) {
                        command = String.valueOf(pairs.getValue());
                    } else if (pairs.getKey().equals(form09Entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (!dv.isEmpty() && dv.length() >= 8) {
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form09Entity.setRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (command.equalsIgnoreCase("INS")) {
                form09Entity.setDelflg("N");
                form09Entity.setActflg("Y");
                form09Entity.setAimag(user.getCity());
                form09Entity.setSum(user.getSum());
                form09Entity.setCreby(user.getUsername());
                Date utilDate = new Date();
                java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                form09Entity.setCreat(sqlDate);
                if (form09Entity.getRecordDate() == null) form09Entity.setCreat(sqlDate);

                errorEntity = form09Service.insertNewData(form09Entity);
            } else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL") ||
                    command.equalsIgnoreCase("ACT")) {
                if (command.equalsIgnoreCase("DEL")) {
                    form09Entity.setDelflg("Y");
                } else if (command.equalsIgnoreCase("ACT")) {
                    form09Entity.setActflg("N");
                }
                errorEntity = form09Service.updateData(form09Entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return errorEntity;
    }

    //    end-form09


    @Override
    public List<Form11Entity> getForm11Data(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 11 data");

        try {
            IForm11Service form11Service = new Form11ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form11Entity form11Entity = new Form11Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form11Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                            form11Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        }
                    } else if (pairs.getKey().equals(form11Entity.AIMAG)) {
                        form11Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.SUM)) {
                        form11Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.BAGCODE)) {
                        form11Entity.setBagCode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.MALTURUL)) {
                        form11Entity.setMalTurul(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.DEEJIINTURUL)) {
                        form11Entity.setDeejiinTurul(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.SUMBAGNER)) {
                        form11Entity.setSumBagNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.TOOTOLGOI)) {
                        try {
                            form11Entity.setTooTolgoi(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch (Exception e) {
                            form11Entity.setTooTolgoi(Long.parseLong(String.valueOf("0")));
                        }
                    } else if (pairs.getKey().equals(form11Entity.HAANASHINJILSEN)) {
                        form11Entity.setHaanaShinjilsen(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.SHINJILSENARGA)) {
                        form11Entity.setShinjilsenArga(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.ERUUL)) {
                        try {
                            form11Entity.setEruul(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch (Exception e) {
                            form11Entity.setEruul(Long.parseLong(String.valueOf("0")));
                        }
                    } else if (pairs.getKey().equals(form11Entity.UVCHTEI)) {
                        try {
                            form11Entity.setUvchtei(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch (Exception e) {
                            form11Entity.setUvchtei(Long.parseLong(String.valueOf("0")));
                        }
                    } else if (pairs.getKey().equals(form11Entity.ONOSH)) {
                        try {
                            form11Entity.setOnosh(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch (Exception e) {
                            form11Entity.setOnosh(Long.parseLong(String.valueOf("0")));
                        }
                    } else if (pairs.getKey().equals(form11Entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form11Entity.setRecordDate(sqlDate);
                    } else if (pairs.getKey().equals(form11Entity.SEARCH_RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form11Entity.setSearchRecordDate(sqlDate);
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                form11Entity.setCreby(user.getUsername());
            }


            if (form11Entity != null && form11Entity.getRecordDate() != null && form11Entity.getSearchRecordDate() != null) {
                return form11Service.getListData(form11Entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public ErrorEntity setForm11Data(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 11 data");

        try {
            String command = "EDT";
            IForm11Service form11Service = new Form11ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form11Entity form11Entity = new Form11Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form11Entity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                            if (pairs.getValue().toString().contains(".")) {
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            form11Entity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    } else if (pairs.getKey().equals(form11Entity.DEEJIINTURUL)) {
                        form11Entity.setDeejiinTurul(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.AIMAG)) {
                        form11Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.SUMBAGNER)) {
                        form11Entity.setSumBagNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.SUM)) {
                        form11Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.BAGCODE)) {
                        form11Entity.setBagCode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.MALTURUL)) {
                        form11Entity.setMalTurul(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.TOOTOLGOI)) {
                        form11Entity.setTooTolgoi(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(form11Entity.HAANASHINJILSEN)) {
                        form11Entity.setHaanaShinjilsen(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.SHINJILSENARGA)) {
                        form11Entity.setShinjilsenArga(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.ERUUL)) {
                        form11Entity.setEruul(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(form11Entity.UVCHTEI)) {
                        form11Entity.setUvchtei(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(form11Entity.ONOSH)) {
                        form11Entity.setOnosh(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(form11Entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (!dv.isEmpty() && dv.length() >= 8) {
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form11Entity.setRecordDate(sqlDate);
                        }
                    } else if (pairs.getKey().toString().equalsIgnoreCase("command")) {
                        command = String.valueOf(pairs.getValue());
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            form11Entity.setModby(user.getUsername());
            form11Entity.setModAt(sqlDate);

            if (command.equalsIgnoreCase("INS")) {
                form11Entity.setDelflg("N");
                form11Entity.setActflg("Y");
                if (user != null && (user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                        || user.getRoleCode().equalsIgnoreCase("ADMIN"))) {
                    //Aimag songoh shaardlagagui
                } else {
                    form11Entity.setAimag(user.getCity());
                }
                //form11Entity.setSum(user.getSum());
                form11Entity.setCreby(user.getUsername());
                form11Entity.setCreat(sqlDate);
                if (form11Entity.getRecordDate() == null) form11Entity.setCreat(sqlDate);

                errorEntity = form11Service.insertNewData(form11Entity);
            } else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL") ||
                    command.equalsIgnoreCase("ACT")) {
                if (command.equalsIgnoreCase("DEL")) {
                    form11Entity.setDelflg("Y");
                } else if (command.equalsIgnoreCase("ACT")) {
                    form11Entity.setActflg("N");
                }
                errorEntity = form11Service.updateData(form11Entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return errorEntity;
    }


    //    Begin-form14
    @Override
    public List<Form14Entity> getForm14Data(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 14 data");

        try {
            IForm14Service form14Service = new Form14ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form14Entity form14Entity = new Form14Entity();

            Iterator it = criteria.entrySet().iterator();


            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form14Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            form14Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(form14Entity.AIMAG)) {
                        form14Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form14Entity.SUM)) {
                        form14Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form14Entity.SHALGAHGAZAR)) {
                        form14Entity.setShalgahGazar(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form14Entity.UZLEGTOO)) {
                        try {
                            form14Entity.setUzlegToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form14Entity.setUzlegToo(Long.valueOf("0"));
                        }
                    } else if (pairs.getKey().equals(form14Entity.BAIGUULLAGATOO)) {
                        try {
                            form14Entity.setBaiguullagaToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form14Entity.setBaiguullagaToo(Long.valueOf("0"));
                        }
                    } else if (pairs.getKey().equals(form14Entity.AJAHUINNEGJTOO)) {
                        try {
                            form14Entity.setAjAhuinNegjToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form14Entity.setAjAhuinNegjToo(Long.valueOf("0"));
                        }
                    } else if (pairs.getKey().equals(form14Entity.IRGEDTOO)) {
                        try {
                            form14Entity.setIrgedToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form14Entity.setIrgedToo(Long.valueOf("0"));
                        }
                    } else if (pairs.getKey().equals(form14Entity.HUULIINBAIGSHILJSEN)) {
                        try {
                            form14Entity.setHuuliinBaigShiljsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form14Entity.setHuuliinBaigShiljsen(Long.valueOf("0"));
                        }
                    } else if (pairs.getKey().equals(form14Entity.ZAHIRGAAHARITOO)) {
                        try {
                            form14Entity.setZahirgaaHariToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form14Entity.setZahirgaaHariToo(Long.valueOf("0"));
                        }
                    } else if (pairs.getKey().equals(form14Entity.TORGUULIHEMJEE)) {
                        try {
                            form14Entity.setTorguuliHemjee(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form14Entity.setTorguuliHemjee(BigDecimal.valueOf(Long.parseLong("0")));
                        }
                    } else if (pairs.getKey().equals(form14Entity.TOLBOR)) {
                        try {
                            form14Entity.setTolbor(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form14Entity.setTolbor(BigDecimal.valueOf(Long.parseLong("0")));
                        }
                    } else if (pairs.getKey().equals(form14Entity.TORGUULIAR)) {
                        try {
                            form14Entity.setTorguuliar(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form14Entity.setTorguuliar(BigDecimal.valueOf(Long.parseLong("0")));
                        }
                    } else if (pairs.getKey().equals(form14Entity.TOLBOROOR)) {
                        try {
                            form14Entity.setTolboroor(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form14Entity.setTolboroor(BigDecimal.valueOf(Long.parseLong("0")));
                        }
                    } else if (pairs.getKey().equals(form14Entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form14Entity.setRecordDate(sqlDate);
                    } else if (pairs.getKey().equals(form14Entity.SEARCH_RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form14Entity.setSearchRecordDate(sqlDate);
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            return form14Service.getListData(form14Entity);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public ErrorEntity setForm14Data(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 14 data");

        try {
            String command = "EDT";
            IForm14Service form14Service = new Form14ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form14Entity form14Entity = new Form14Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form14Entity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                            if (pairs.getValue().toString().contains(".")) {
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            form14Entity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    } else if (pairs.getKey().equals(form14Entity.SHALGAHGAZAR)) {
                        form14Entity.setShalgahGazar(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form14Entity.UZLEGTOO)) {
                        try {
                            form14Entity.setUzlegToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form14Entity.setUzlegToo(Long.valueOf("0"));
                        }
                    } else if (pairs.getKey().equals(form14Entity.BAIGUULLAGATOO)) {
                        try {
                            form14Entity.setBaiguullagaToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form14Entity.setBaiguullagaToo(Long.valueOf("0"));
                        }
                    } else if (pairs.getKey().equals(form14Entity.AJAHUINNEGJTOO)) {
                        try {
                            form14Entity.setAjAhuinNegjToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form14Entity.setAjAhuinNegjToo(Long.valueOf("0"));
                        }
                    } else if (pairs.getKey().equals(form14Entity.IRGEDTOO)) {
                        try {
                            form14Entity.setIrgedToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form14Entity.setIrgedToo(Long.valueOf("0"));
                        }
                    } else if (pairs.getKey().equals(form14Entity.HUULIINBAIGSHILJSEN)) {
                        try {
                            form14Entity.setHuuliinBaigShiljsen(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form14Entity.setHuuliinBaigShiljsen(Long.valueOf("0"));
                        }
                    } else if (pairs.getKey().equals(form14Entity.ZAHIRGAAHARITOO)) {
                        try {
                            form14Entity.setZahirgaaHariToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form14Entity.setZahirgaaHariToo(Long.valueOf("0"));
                        }
                    } else if (pairs.getKey().equals(form14Entity.TORGUULIHEMJEE)) {
                        try {
                            form14Entity.setTorguuliHemjee(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form14Entity.setTorguuliHemjee(BigDecimal.valueOf(Long.parseLong("0")));
                        }
                    } else if (pairs.getKey().equals(form14Entity.TOLBOR)) {
                        try {
                            form14Entity.setTolbor(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form14Entity.setTolbor(BigDecimal.valueOf(Long.parseLong("0")));
                        }
                    } else if (pairs.getKey().equals(form14Entity.TORGUULIAR)) {
                        try {
                            form14Entity.setTorguuliar(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form14Entity.setTorguuliar(BigDecimal.valueOf(Long.parseLong("0")));
                        }
                    } else if (pairs.getKey().equals(form14Entity.TOLBOROOR)) {
                        try {
                            form14Entity.setTolboroor(BigDecimal.valueOf(Long.parseLong(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            form14Entity.setTolboroor(BigDecimal.valueOf(Long.parseLong("0")));
                        }
                    } else if (pairs.getKey().toString().equalsIgnoreCase("command")) {
                        command = String.valueOf(pairs.getValue());
                    } else if (pairs.getKey().equals(form14Entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (!dv.isEmpty() && dv.length() >= 8) {
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form14Entity.setRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (command.equalsIgnoreCase("INS")) {
                form14Entity.setDelflg("N");
                form14Entity.setActflg("Y");
                form14Entity.setAimag(user.getCity());
                form14Entity.setSum(user.getSum());
                form14Entity.setCreby(user.getUsername());
                Date utilDate = new Date();
                java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                form14Entity.setCreat(sqlDate);
                if (form14Entity.getRecordDate() == null) form14Entity.setCreat(sqlDate);

                errorEntity = form14Service.insertNewData(form14Entity);
            } else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL") ||
                    command.equalsIgnoreCase("ACT")) {
                if (command.equalsIgnoreCase("DEL")) {
                    form14Entity.setDelflg("Y");
                } else if (command.equalsIgnoreCase("ACT")) {
                    form14Entity.setActflg("N");
                }
                errorEntity = form14Service.updateData(form14Entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return errorEntity;
    }

    //    end-form14

    @Override
    public List<Form46Entity> getForm46Data(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 46 data");

        try {
            IForm46Service form46Service = new Form46ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form46Entity entity = new Form46Entity();

            Iterator it = criteria.entrySet().iterator();


            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(entity.AIMAG)) {
                        entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.SUM)) {
                        entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.BAGHOROONER)) {
                        entity.setBagHorooNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.UHERNIITTOO)) {
                        entity.setUherNiitToo(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.TUGALTOO)) {
                        try {
                            entity.setTugalToo(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch (Exception e) {
                            entity.setTugalToo(Long.parseLong(String.valueOf("0")));
                        }
                    } else if (pairs.getKey().equals(entity.BUSADTOO)) {
                        try {
                            entity.setBusadToo(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch (Exception e) {
                            entity.setBusadToo(Long.parseLong(String.valueOf("0")));
                        }
                    } else if (pairs.getKey().equals(entity.GUURTAINIITTOO)) {
                        entity.setGuurtaiNiitToo(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.GUURTAITUGALTOO)) {
                        try {
                            entity.setGuurtaiBusadToo(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch (Exception e) {
                            entity.setGuurtaiBusadToo(Long.parseLong(String.valueOf("0")));
                        }
                    } else if (pairs.getKey().equals(entity.GUURTAIBUSADTOO)) {
                        try {
                            entity.setGuurtaiBusadToo(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch (Exception e) {
                            entity.setGuurtaiBusadToo(Long.parseLong(String.valueOf("0")));
                        }
                    } else if (pairs.getKey().equals(entity.DUNDAJHUVI)) {
                        entity.setDundajHuvi(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.TUGALHUVI)) {
                        entity.setTugalHuvi(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.BUSADHUVI)) {
                        entity.setBusadHuvi(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        entity.setRecordDate(sqlDate);
                    } else if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        entity.setSearchRecordDate(sqlDate);
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                entity.setCreby(user.getUsername());
            }

            return form46Service.getListData(entity);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public ErrorEntity setForm46Data(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 46 data");

        try {
            String command = "EDT";
            IForm46Service form46Service = new Form46ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form46Entity entity = new Form46Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(entity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                            if (pairs.getValue().toString().contains(".")) {
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            entity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    } else if (pairs.getKey().equals(entity.AIMAG)) {
                        entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.SUM)) {
                        entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.BAGHOROONER)) {
                        entity.setBagHorooNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.UHERNIITTOO)) {
                        entity.setUherNiitToo(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.TUGALTOO)) {
                        try {
                            entity.setTugalToo(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch (Exception e) {
                            entity.setTugalToo(Long.parseLong(String.valueOf("0")));
                        }
                    } else if (pairs.getKey().equals(entity.BUSADTOO)) {
                        try {
                            entity.setBusadToo(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch (Exception e) {
                            entity.setBusadToo(Long.parseLong(String.valueOf("0")));
                        }
                    } else if (pairs.getKey().equals(entity.GUURTAINIITTOO)) {
                        entity.setGuurtaiNiitToo(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.GUURTAITUGALTOO)) {
                        try {
                            entity.setGuurtaiTugalToo(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch (Exception e) {
                            entity.setGuurtaiTugalToo(Long.parseLong(String.valueOf("0")));
                        }
                    } else if (pairs.getKey().equals(entity.GUURTAIBUSADTOO)) {
                        try {
                            entity.setGuurtaiBusadToo(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch (Exception e) {
                            entity.setGuurtaiBusadToo(Long.parseLong(String.valueOf("0")));
                        }
                    } else if (pairs.getKey().equals(entity.DUNDAJHUVI)) {
                        entity.setDundajHuvi(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.TUGALHUVI)) {
                        entity.setTugalHuvi(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.BUSADHUVI)) {
                        entity.setBusadHuvi(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().toString().equalsIgnoreCase("command")) {
                        command = String.valueOf(pairs.getValue());
                    } else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (!dv.isEmpty() && dv.length() >= 8) {
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (command.equalsIgnoreCase("INS")) {
                entity.setDelflg("N");
                entity.setActflg("Y");
                if (user != null && (user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                        || user.getRoleCode().equalsIgnoreCase("ADMIN"))) {
                    //Aimag songoh shaardlagagui
                } else {
                    entity.setAimag(user.getCity());
                }
                //entity.setSum(user.getSum());
                entity.setCreby(user.getUsername());
                Date utilDate = new Date();
                java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                entity.setCreat(sqlDate);
                if (entity.getRecordDate() == null) entity.setCreat(sqlDate);

                errorEntity = form46Service.insertNewData(entity);
            } else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL") ||
                    command.equalsIgnoreCase("ACT")) {
                if (command.equalsIgnoreCase("DEL")) {
                    entity.setDelflg("Y");
                } else if (command.equalsIgnoreCase("ACT")) {
                    entity.setActflg("N");
                }
                errorEntity = form46Service.updateData(entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return errorEntity;
    }


//Begin-form43

    @Override
    public List<Form43Entity> getForm43Data(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 43 data");

        try {
            IForm43Service form43Service = new Form43ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form43Entity form43Entity = new Form43Entity();

            Iterator it = criteria.entrySet().iterator();


            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form43Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            form43Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(form43Entity.AIMAG)) {
                        form43Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form43Entity.SUM)) {
                        form43Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form43Entity.AJAHUINNEGJ)) {
                        form43Entity.setAjahuinNegj(String.valueOf(pairs.getValue()));
                    } else {
                        if (pairs.getKey().equals(form43Entity.TUSGAIZOVSHOOROL)) {
                            String dv = String.valueOf(pairs.getValue());
                            try {
                                if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                                java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                                form43Entity.setTusgaiZovshoorol(sqlDate);
                            } catch (Exception ex) {
                                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
                                Date now = new Date();
                                form43Entity.setTusgaiZovshoorol(new java.sql.Date(now.getTime()));
                            }
                        } else if (pairs.getKey().equals(form43Entity.EMBELDMELNER)) {
                            form43Entity.setEmBeldmelNer(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form43Entity.HEMJIHNEGJ)) {
                            form43Entity.setHemjihNegj(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form43Entity.PARMAKOP)) {
                            try {
                                form43Entity.setParmakop(Long.parseLong(String.valueOf(pairs.getValue())));
                            } catch (Exception e) {
                                form43Entity.setParmakop(Long.parseLong(String.valueOf("0")));
                            }
                        } else if (pairs.getKey().equals(form43Entity.UILDVERLESENTOO)) {
                            try {
                                form43Entity.setUildverlesenToo(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                form43Entity.setUildverlesenToo(Long.valueOf("0"));
                            }
                        } else if (pairs.getKey().equals(form43Entity.NEGJUNE)) {
                            try {
                                form43Entity.setNegjUne(String.valueOf(pairs.getValue()));
                            } catch (NumberFormatException e) {
                                form43Entity.setNegjUne(String.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(form43Entity.NIITUNE)) {
                            try {
                                form43Entity.setNiitUne(String.valueOf(pairs.getValue()));
                            } catch (NullPointerException e) {
                                form43Entity.setNiitUne(String.valueOf("0"));
                            }
                        } else if (pairs.getKey().equals(form43Entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form43Entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(form43Entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form43Entity.setSearchRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            return form43Service.getListData(form43Entity);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public ErrorEntity setForm43Data(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 43 data");

        try {
            String command = "EDT";
            IForm43Service form43Service = new Form43ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form43Entity form43Entity = new Form43Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form43Entity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                            if (pairs.getValue().toString().contains(".")) {
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            form43Entity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    } else if (pairs.getKey().equals(form43Entity.AJAHUINNEGJ)) {
                        form43Entity.setAjahuinNegj(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form43Entity.TUSGAIZOVSHOOROL)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (!dv.isEmpty() && dv.length() >= 8) {
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form43Entity.setTusgaiZovshoorol(sqlDate);
                        }
                    } else if (pairs.getKey().equals(form43Entity.EMBELDMELNER)) {
                        form43Entity.setEmBeldmelNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form43Entity.HEMJIHNEGJ)) {
                        form43Entity.setHemjihNegj(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form43Entity.PARMAKOP)) {
                        try {
                            form43Entity.setParmakop(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch (Exception e) {
                            form43Entity.setParmakop(Long.parseLong(String.valueOf("0")));
                        }
                    } else if (pairs.getKey().equals(form43Entity.UILDVERLESENTOO)) {
                        try {
                            form43Entity.setUildverlesenToo(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form43Entity.setUildverlesenToo(Long.valueOf("0"));
                        }
                    } else if (pairs.getKey().equals(form43Entity.NEGJUNE)) {
                        try {
                            form43Entity.setNegjUne(String.valueOf(pairs.getValue()));
                        } catch (NumberFormatException e) {
                            form43Entity.setNegjUne(String.valueOf("0"));
                        }

                    } else if (pairs.getKey().equals(form43Entity.NIITUNE)) {
                        try {
                            form43Entity.setNiitUne(String.valueOf(pairs.getValue()));
                        } catch (NullPointerException e) {
                            form43Entity.setNiitUne(String.valueOf("0"));
                        }
                    } else if (pairs.getKey().equals(form43Entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (!dv.isEmpty() && dv.length() >= 8) {
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form43Entity.setRecordDate(sqlDate);
                        }
                    } else if (pairs.getKey().toString().equalsIgnoreCase("command")) {
                        command = String.valueOf(pairs.getValue());
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            form43Entity.setModby(user.getUsername());
            form43Entity.setModAt(sqlDate);

            if (command.equalsIgnoreCase("INS")) {
                form43Entity.setDelflg("N");
                form43Entity.setActflg("Y");
                form43Entity.setAimag(user.getCity());
                form43Entity.setSum(user.getSum());
                form43Entity.setCreby(user.getUsername());
                form43Entity.setCreat(sqlDate);
                if (form43Entity.getRecordDate() == null) form43Entity.setCreat(sqlDate);

                errorEntity = form43Service.insertNewData(form43Entity);
            } else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL") ||
                    command.equalsIgnoreCase("ACT")) {
                if (command.equalsIgnoreCase("DEL")) {
                    form43Entity.setDelflg("Y");
                } else if (command.equalsIgnoreCase("ACT")) {
                    form43Entity.setActflg("N");
                }
                errorEntity = form43Service.updateData(form43Entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return errorEntity;
    }

    //end-form43


    //Begin City Form
    @Override
    public List<City> getCityData(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service ::: get city data");

        try {
            ICityService cityService = new CityServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            City city = new City();

            Iterator it = criteria.entrySet().iterator();


            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(city.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            city.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(city.CODE)) {
                        city.setCode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(city.COUNTRYCODE)) {
                        city.setCountrycode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(city.CITYNAME)) {
                        city.setCityname(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(city.CITYDESC)) {
                        city.setCitydesc(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(city.CITYNAME_EN)) {
                        city.setCitynameEn(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(city.DELFLG)) {
                        city.setDelflg(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(city.ACTFLG)) {
                        city.setActflg(String.valueOf(pairs.getValue()));
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            city.setDelflg("N");

            return cityService.selectAll(city);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public ErrorEntity setCityData(LinkedTreeMap insdata, String sessionId) throws Exception {
        System.out.println("Hospital Service Implament ::: set city data");

        try {
            String command = "EDT";
            ICityService cityService = new CityServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            City city = new City();

            Iterator it = insdata.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(city.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                            if (pairs.getValue().toString().contains(".")) {
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            city.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    } else if (pairs.getKey().equals(city.CODE)) {
                        city.setCode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(city.COUNTRYCODE)) {
                        city.setCountrycode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(city.CITYNAME)) {
                        city.setCityname(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(city.CITYDESC)) {
                        city.setCitydesc(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(city.CITYNAME_EN)) {
                        city.setCitynameEn(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(city.DELFLG)) {
                        city.setDelflg(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(city.ACTFLG)) {
                        city.setActflg(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(city.LATITUDE)) {
                        city.setLatitude(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(city.LONGITUDE)) {
                        city.setLongitude(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().toString().equalsIgnoreCase("command")) {
                        command = String.valueOf(pairs.getValue());
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (command.equalsIgnoreCase("INS")) {
                city.setDelflg("N");
                city.setActflg("Y");
                city.setCountrycode("UB");
                city.setCreby(user.getUsername());
                Date utilDate = new Date();
                java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                city.setCreat(sqlDate);

                errorEntity = cityService.insertData(city);
            } else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL") ||
                    command.equalsIgnoreCase("ACT")) {
                city.setModby(user.getUsername());
                Date utilDate = new Date();
                java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                city.setModAt(sqlDate);

                if (command.equalsIgnoreCase("DEL")) {
                    city.setDelflg("Y");
                }
                errorEntity = cityService.updateData(city);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (errorEntity == null) {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1000));
            }
        }

        return errorEntity;
    }

    //End City Form

    //Begin Sum Form

    @Override
    public List<Sum> getSumData(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service ::: get sum data");

        try {
            ISumService sumService = new SumServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Sum sum = new Sum();

            Iterator it = criteria.entrySet().iterator();


            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(sum.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            sum.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(sum.SUMCODE)) {
                        sum.setSumcode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(sum.CITYCODE)) {
                        sum.setCitycode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(sum.SUMNAME)) {
                        sum.setSumname(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(sum.SUMDESC)) {
                        sum.setSumdesc(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(sum.SUMNAME_EN)) {
                        sum.setSumname_en(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(sum.DELFLG)) {
                        sum.setDelFlg(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(sum.ACTFLG)) {
                        sum.setActFlg(String.valueOf(pairs.getValue()));
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (sum.getCitycode() != null && sum.getCitycode().equalsIgnoreCase("CURRENT")) {
                sum.setCitycode(user.getCity());
            }

            sum.setDelFlg("N");

            return sumService.selectAll(sum);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public ErrorEntity setSumData(LinkedTreeMap insdata, String sessionId) throws Exception {
        System.out.println("Hospital Service Implament ::: set sum data");

        try {
            String command = "EDT";
            ISumService sumService = new SumServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Sum sum = new Sum();

            Iterator it = insdata.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(sum.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                            if (pairs.getValue().toString().contains(".")) {
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            sum.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    } else if (pairs.getKey().equals(sum.SUMCODE)) {
                        sum.setSumcode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(sum.CITYCODE)) {
                        sum.setCitycode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(sum.SUMNAME)) {
                        sum.setSumname(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(sum.SUMDESC)) {
                        sum.setSumdesc(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(sum.SUMNAME_EN)) {
                        sum.setSumname_en(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(sum.DELFLG)) {
                        sum.setDelFlg(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(sum.ACTFLG)) {
                        sum.setActFlg(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(sum.LATITUDE)) {
                        sum.setLatitude(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(sum.LONGITUDE)) {
                        sum.setLongitude(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().toString().equalsIgnoreCase("command")) {
                        command = String.valueOf(pairs.getValue());
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (command.equalsIgnoreCase("INS")) {
                sum.setDelFlg("N");
                sum.setActFlg("Y");
                sum.setCreBy(user.getUsername());
                Date utilDate = new Date();
                java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                sum.setCreAt(sqlDate);
                sum.setModBy(user.getUsername());
                sum.setModAt(sqlDate);

                errorEntity = sumService.insertData(sum);
            } else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL") ||
                    command.equalsIgnoreCase("ACT")) {
                sum.setModBy(user.getUsername());
                Date utilDate = new Date();
                java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                sum.setModAt(sqlDate);

                if (command.equalsIgnoreCase("DEL")) {
                    sum.setDelFlg("Y");
                }
                errorEntity = sumService.updateData(sum);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (errorEntity == null) {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1000));
            }
        }

        return errorEntity;
    }

    //End Sum form

    //Begin User form

    @Override
    public List<User> getUserData(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service ::: get user data");

        try {
            IUserService userService = new UserServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            User seluser = new User();

            Iterator it = criteria.entrySet().iterator();


            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(seluser.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            seluser.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(seluser.USERNAME)) {
                        seluser.setUsername(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.ROLECODE)) {
                        seluser.setRoleCode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.FIRSTNAME)) {
                        seluser.setFirstName(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.LASTNAME)) {
                        seluser.setLastName(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.GENDER)) {
                        seluser.setGender(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.REGID)) {
                        seluser.setRegId(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.CITY)) {
                        seluser.setCity(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.SUM)) {
                        seluser.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.CREBY)) {
                        seluser.setCreBy(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.MODBY)) {
                        seluser.setModBy(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.DELFLG)) {
                        seluser.setDelFlg(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.ACTFLG)) {
                        seluser.setActFlg(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.CHAT_ID)) {
                        seluser.setChatId(String.valueOf(pairs.getValue()));
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            seluser.setDelFlg("N");

            return userService.getUserList(seluser);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public ErrorEntity setUserData(LinkedTreeMap insdata, String sessionId) throws Exception {
        System.out.println("Hospital Service ::: set user data");

        try {
            String command = "EDT";
            IUserService userService = new UserServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            User seluser = new User();

            Iterator it = insdata.entrySet().iterator();


            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(seluser.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                            if (pairs.getValue().toString().contains(".")) {
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            seluser.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    } else if (pairs.getKey().equals(seluser.USERNAME)) {
                        seluser.setUsername(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.ROLECODE)) {
                        seluser.setRoleCode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.FIRSTNAME)) {
                        seluser.setFirstName(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.LASTNAME)) {
                        seluser.setLastName(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.GENDER)) {
                        seluser.setGender(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.REGID)) {
                        seluser.setRegId(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.CITY)) {
                        seluser.setCity(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.SUM)) {
                        seluser.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.CREBY)) {
                        seluser.setCreBy(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.MODBY)) {
                        seluser.setModBy(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.DELFLG)) {
                        seluser.setDelFlg(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.ACTFLG)) {
                        seluser.setActFlg(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.PASSWORD)) {
                        seluser.setPassword(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().toString().equalsIgnoreCase("command")) {
                        command = String.valueOf(pairs.getValue());
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            seluser.setModBy(user.getUsername());
            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            seluser.setModAt(sqlDate);

            if (command.equalsIgnoreCase("INS")) {
                seluser.setDelFlg("N");
                seluser.setActFlg("Y");
                seluser.setCreBy(user.getUsername());
                seluser.setCreAt(sqlDate);
                String chatid = new ObjectId().toString();
                seluser.setChatId(chatid);

                errorEntity = userService.insertUser(seluser);
            } else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL")) {
                if (command.equalsIgnoreCase("DEL")) {
                    seluser.setDelFlg("Y");
                }
                errorEntity = userService.updateUserData(seluser);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (errorEntity == null) {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1000));
            }
        }

        return errorEntity;
    }

    //End User form


    //Begin Bag Horoo form

    @Override
    public List<BagHorooEntity> getBagHorooData(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service ::: get bag horoo data");

        try {
            IBagHorooService bagService = new BagHorooServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            BagHorooEntity bagHorooEntity = new BagHorooEntity();

            Iterator it = criteria.entrySet().iterator();


            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(bagHorooEntity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            bagHorooEntity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(bagHorooEntity.SUM_CODE)) {
                        bagHorooEntity.setSumcode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(bagHorooEntity.HOROO_CODE)) {
                        bagHorooEntity.setHoroocode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(bagHorooEntity.HOROO_NAME)) {
                        bagHorooEntity.setHorooname(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(bagHorooEntity.HOROO_DESC)) {
                        bagHorooEntity.setHoroodesc(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(bagHorooEntity.HOROO_NAME_EN)) {
                        bagHorooEntity.setHorooname_en(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(bagHorooEntity.DELFLG)) {
                        bagHorooEntity.setDelflg(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(bagHorooEntity.ACTFLG)) {
                        bagHorooEntity.setActflg(String.valueOf(pairs.getValue()));
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            bagHorooEntity.setDelflg("N");

            return bagService.selectAll(bagHorooEntity);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    //End bag horoo form
    @Override
    public List<BasicDBObject> getComboVals(String type, String langid, String sessionId) throws Exception {
        IComboService comboService = new ComboServiceImpl(conn);
        return comboService.getComboVals(type, langid);
    }

    //End bag horoo form
    @Override
    public List<BasicDBObject> getShalgahGazarVals(String type, String langid, String sessionId) throws Exception {
        IComboService comboService = new ComboServiceImpl(conn);
        return comboService.getShalgahGazarVals(type, langid);
    }


    @Override
    public List<MenuEntity> getMenuList(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service ::: get menu list data");

        try {
            IMenuService service = new MenuServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            MenuEntity entity = new MenuEntity();

            Iterator it = criteria.entrySet().iterator();


            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(entity.MENUID)) {
                        entity.setMenuid(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.MENUNAMEMN)) {
                        entity.setMenunamemn(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.MENUNAMEEN)) {
                        entity.setMenunameen(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.MENUDESC)) {
                        entity.setMenudesc(String.valueOf(pairs.getValue()));
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            entity.setDelflg("N");
            entity.setActflg("Y");
            return service.selectAll(entity);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public List<Role> getRoleList(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service ::: get role list data");

        try {
            IRoleService service = new RoleServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Role entity = new Role();

            Iterator it = criteria.entrySet().iterator();


            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(entity.CODE)) {
                        entity.setCode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.NAME)) {
                        entity.setName(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.PARENT)) {
                        entity.setParent(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.DESC)) {
                        entity.setDesc(String.valueOf(pairs.getValue()));
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            return service.selectAll(entity);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public ErrorEntity setMenuRole(LinkedTreeMap insdata, String sessionId) throws Exception {
        System.out.println("Hospital Service ::: register role menu data");

        try {
            String command = "EDT";
            IMenuRolesService service = new MenuRolesServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            MenuRoles menuRoles = new MenuRoles();

            Iterator it = insdata.entrySet().iterator();
            List<LinkedTreeMap> menuList = null;

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(menuRoles.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                            if (pairs.getValue().toString().contains(".")) {
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            menuRoles.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    } else if (pairs.getKey().equals(menuRoles.ROLECODE)) {
                        menuRoles.setRolecode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals("menulist")) {
                        menuList = (List<LinkedTreeMap>) pairs.getValue();
                    } else if (pairs.getKey().toString().equalsIgnoreCase("command")) {
                        command = String.valueOf(pairs.getValue());
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            menuRoles.setCreBy(user.getUsername());
            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            menuRoles.setCreAt(sqlDate);

            if (command.equalsIgnoreCase("INS")) {
                menuRoles.setDelflg("N");
                menuRoles.setCreBy(user.getUsername());
                menuRoles.setCreAt(sqlDate);

                for (LinkedTreeMap regmenu : menuList) {
                    Iterator itm = regmenu.entrySet().iterator();

                    String provMenuId = menuRoles.getMenuId();

                    while (itm.hasNext()) {
                        Map.Entry pairs = (Map.Entry) itm.next();

                        if (pairs != null && pairs.getKey().equals("menuId")) {
                            menuRoles.setMenuId(String.valueOf(pairs.getValue()));
                        }
                    }

                    if (provMenuId != null && menuRoles.getMenuId() != null && !provMenuId.equalsIgnoreCase(menuRoles.getMenuId())) {
                        errorEntity = service.insertData(menuRoles);
                    }
                }//end of for loop
            } else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL")) {
                if (command.equalsIgnoreCase("DEL")) {
                    menuRoles.setDelflg("Y");
                }
                errorEntity = service.updateData(menuRoles);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (errorEntity == null) {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1000));
            }
        }

        return errorEntity;
    }

    @Override
    public List<MenuRoles> getMenuRole(LinkedTreeMap criteria, String sessionId) throws Exception {
        System.out.println("Hospital Service ::: get menu role list data");

        try {
            IMenuRolesService service = new MenuRolesServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            MenuRoles entity = new MenuRoles();

            Iterator it = criteria.entrySet().iterator();


            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(entity.ROLECODE)) {
                        entity.setRolecode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.MENUID)) {
                        entity.setMenuId(String.valueOf(pairs.getValue()));
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            entity.setDelflg("N");

            return service.selectAll(entity);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }//end of function


    @Override
    public BasicDBObject getForm08ColumnChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 08 data");

        try {
            IForm08Service form08Service = new Form08ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form08Entity form08Entity = new Form08Entity();

            Iterator it = criteria.entrySet().iterator();

            //Check Role
            /*roleControl = new CheckUserRoleControl(super.conn);

            if (roleControl == null) {
                return null;
            }

            menuid = menuid.replaceAll("#", "");

            if (!roleControl.checkRole(user, menuid, 1)){
                return null;
            }*/
            //End Check Role

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form08Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            form08Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(form08Entity.AIMAG)) {
                        form08Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.SUM)) {
                        form08Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.MALTURUL)) {
                        form08Entity.setMalTurul(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.ARGA_HEMJEE_NER)) {
                        form08Entity.setArgaHemjeeNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.NER)) {
                        form08Entity.setNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.ZARTSUULSAN_HEMJEE)) {
                        form08Entity.setZartsuulsanHemjee(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.TOLOVLOGOO)) {
                        form08Entity.setTolovlogoo(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.GUITSETGEL)) {
                        form08Entity.setGuitsetgel(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.HUVI)) {
                        form08Entity.setHuvi(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()) {
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form08Entity.setRecordDate(sqlDate);
                        }
                    } else if (pairs.getKey().equals(form08Entity.SEARCH_RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()) {
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form08Entity.setSearchRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                form08Entity.setCreby(user.getUsername());
            }
            //if(form08Entity.getSum() == null)form08Entity.setSum(user.getSum());
            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                form08Entity.setCreby(user.getUsername());
            }

            form08Entity.setDelflg("N");
            form08Entity.setActflg("Y");

            if (form08Entity != null && form08Entity.getRecordDate() != null && form08Entity.getSearchRecordDate() != null) {
                return form08Service.getColumnChartData(form08Entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }//end of function

    @Override
    public List<BasicDBObject> getForm08PieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 08 data");

        try {
            IForm08Service form08Service = new Form08ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form08Entity form08Entity = new Form08Entity();

            Iterator it = criteria.entrySet().iterator();

            //Check Role
            /*roleControl = new CheckUserRoleControl(super.conn);

            if (roleControl == null) {
                return null;
            }

            menuid = menuid.replaceAll("#", "");

            if (!roleControl.checkRole(user, menuid, 1)){
                return null;
            }*/
            //End Check Role

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form08Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            form08Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(form08Entity.AIMAG)) {
                        form08Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.SUM)) {
                        form08Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.MALTURUL)) {
                        form08Entity.setMalTurul(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.ARGA_HEMJEE_NER)) {
                        form08Entity.setArgaHemjeeNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.NER)) {
                        form08Entity.setNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.ZARTSUULSAN_HEMJEE)) {
                        form08Entity.setZartsuulsanHemjee(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.TOLOVLOGOO)) {
                        form08Entity.setTolovlogoo(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.GUITSETGEL)) {
                        form08Entity.setGuitsetgel(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.HUVI)) {
                        form08Entity.setHuvi(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()) {
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form08Entity.setRecordDate(sqlDate);
                        }
                    } else if (pairs.getKey().equals(form08Entity.SEARCH_RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()) {
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form08Entity.setSearchRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (form08Entity.getAimag() == null) form08Entity.setAimag(user.getCity());
            //if(form08Entity.getSum() == null)form08Entity.setSum(user.getSum());
            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                form08Entity.setCreby(user.getUsername());
            }

            form08Entity.setDelflg("N");
            form08Entity.setActflg("Y");

            if (form08Entity != null && form08Entity.getRecordDate() != null && form08Entity.getSearchRecordDate() != null) {
                return form08Service.getPieChartData(form08Entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public List<Form08HavsraltEntity> getForm08HavsraltData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 08 havsralt data");

        try {
            IForm08HavsraltService form08Service = new Form08HavsraltServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form08HavsraltEntity entity = new Form08HavsraltEntity();

            Iterator it = criteria.entrySet().iterator();

            //Check Role
            /*roleControl = new CheckUserRoleControl(super.conn);

            if (roleControl == null) {
                return null;
            }

            menuid = menuid.replaceAll("#", "");

            if (!roleControl.checkRole(user, menuid, 1)){
                return null;
            }*/
            //End Check Role

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (!pairs.getValue().toString().isEmpty()) {
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.CITYCODE)) {
                            entity.setCitycode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SUMCODE)) {
                            entity.setSumcode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.BAGCODE)) {
                            entity.setBagcode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.GAZARNER)) {
                            entity.setGazarner(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.URTRAG)) {
                            entity.setUrtrag(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.ORGOROG)) {
                            entity.setOrgorog(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.GOLOMTTOO)) {
                            entity.setGolomttoo(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.OVCHIN_NER)) {
                            entity.setOvchin_ner(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.MAL_TURUL)) {
                            entity.setMal_turul(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.UVCHILSON)) {
                            entity.setUvchilson(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.UHSEN)) {
                            entity.setUhsen(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.EDGERSEN)) {
                            entity.setEdgersen(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.USTGASAN)) {
                            entity.setUstgasan(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.NYDLUULSAN)) {
                            entity.setNydluulsan(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.LAB_NER)) {
                            entity.setLab_ner(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SHINJILGEE_ARGA)) {
                            entity.setShinjilgee_arga(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            entity.setDelflg("N");

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                entity.setCreby(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                return form08Service.getListData(entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public ErrorEntity setForm08HavsraltData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 08 havsralt data");

        try {
            String command = "EDT";
            IForm08HavsraltService service = new Form08HavsraltServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form08HavsraltEntity entity = new Form08HavsraltEntity();
            //Begin Check Role Step 1
            roleControl = new CheckUserRoleControl(super.conn);

            if (roleControl == null) {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1002)); // Ерөнхий алдаа : Эрх нь хүрэлцэхгүй үед өгөх
                return errorEntity;
            }
            //End Check Role Step 1

            menuid = menuid.replaceAll("#", "");

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(entity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                            if (pairs.getValue().toString().contains(".")) {
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            entity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    } else if (pairs.getKey().equals(entity.CITYCODE)) {
                        entity.setCitycode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.SUMCODE)) {
                        entity.setSumcode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.BAGCODE)) {
                        entity.setBagcode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.GAZARNER)) {
                        entity.setGazarner(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.URTRAG)) {
                        entity.setUrtrag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.ORGOROG)) {
                        entity.setOrgorog(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.GOLOMTTOO)) {
                        entity.setGolomttoo(stringToInteger(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(entity.OVCHIN_NER)) {
                        entity.setOvchin_ner(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.MAL_TURUL)) {
                        entity.setMal_turul(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.UVCHILSON)) {
                        entity.setUvchilson(stringToInteger(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(entity.UHSEN)) {
                        entity.setUhsen(stringToInteger(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(entity.EDGERSEN)) {
                        entity.setEdgersen(stringToInteger(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(entity.USTGASAN)) {
                        entity.setUstgasan(stringToInteger(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(entity.NYDLUULSAN)) {
                        entity.setNydluulsan(stringToInteger(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(entity.LAB_NER)) {
                        entity.setLab_ner(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.SHINJILGEE_ARGA)) {
                        entity.setShinjilgee_arga(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (!dv.isEmpty() && dv.length() >= 8) {
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        }
                    } else if (pairs.getKey().toString().equalsIgnoreCase("command")) {
                        command = String.valueOf(pairs.getValue());
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (command.equalsIgnoreCase("INS")) {
                entity.setDelflg("N");
                if (user != null && (user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                        || user.getRoleCode().equalsIgnoreCase("ADMIN"))) {
                    //Aimag songoh shaardlagagui
                } else {
                    entity.setCitycode(user.getCity());
                }
                //entity.setSumcode(user.getSum());
                entity.setCreby(user.getUsername());
                Date utilDate = new Date();
                java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                entity.setCreat(sqlDate);
                if (entity.getRecordDate() == null) entity.setCreat(sqlDate);

                //Begin Check Role Step 2
                if (roleControl.checkRole(user, menuid, 2)) {
                    errorEntity = service.insertNewData(entity);
                } else {
                    errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1002)); // Ерөнхий алдаа : Эрх нь хүрэлцэхгүй үед өгөх
                }
                //End Check Role Step 2
            } else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL") ||
                    command.equalsIgnoreCase("ACT")) {
                int quality = 2;

                if (command.equalsIgnoreCase("DEL")) {
                    entity.setDelflg("Y");
                    quality = 3;
                }

                //Begin Check Role Step 2
                if (roleControl.checkRole(user, menuid, quality)) {
                    errorEntity = service.updateData(entity);
                } else {
                    errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1002)); // Ерөнхий алдаа : Эрх нь хүрэлцэхгүй үед өгөх
                }
                //End Check Role Step 2
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return errorEntity;
    }//


    //    begin-form09hav
    @Override
    public List<Form09HavsraltEntity> getForm09HavsraltData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 09 havsralt data");

        try {
            IForm09HavsraltService form09Service = new Form09HavsraltServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form09HavsraltEntity entity = new Form09HavsraltEntity();

            Iterator it = criteria.entrySet().iterator();

//        //Check Role
//        roleControl = new CheckUserRoleControl(super.conn);
//
//        if (roleControl == null) {
//            return null;
//        }
//
//        menuid = menuid.replaceAll("#", "");
//
//        if (!roleControl.checkRole(user, menuid, 1)){
//            return null;
//        }
//        //End Check Role

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (!pairs.getValue().toString().isEmpty()) {
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.CITYCODE)) {
                            entity.setCitycode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SUMCODE)) {
                            entity.setSumcode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.BAGCODE)) {
                            entity.setBagcode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.GAZARNER)) {
                            entity.setGazarner(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.URTRAG)) {
                            entity.setUrtrag(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.ORGOROG)) {
                            entity.setOrgorog(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.GOLOMTTOO)) {
                            entity.setGolomttoo(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.OVCHIN_NER)) {
                            entity.setOvchin_ner(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.MAL_TURUL)) {
                            entity.setMal_turul(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.UVCHILSON)) {
                            entity.setUvchilson(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.UHSEN)) {
                            entity.setUhsen(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.EDGERSEN)) {
                            entity.setEdgersen(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            entity.setDelflg("N");

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                entity.setCreby(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                return form09Service.getListData(entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public ErrorEntity setForm09HavsraltData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 09 havsralt data");

        try {
            String command = "EDT";
            IForm09HavsraltService service = new Form09HavsraltServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form09HavsraltEntity entity = new Form09HavsraltEntity();
//            //Begin Check Role Step 1
//            roleControl = new CheckUserRoleControl(super.conn);
//
//            if (roleControl == null) {
//                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1002)); // Ерөнхий алдаа : Эрх нь хүрэлцэхгүй үед өгөх
//                return errorEntity;
//            }
//            //End Check Role Step 1

//            menuid = menuid.replaceAll("#", "");

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(entity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                            if (pairs.getValue().toString().contains(".")) {
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            entity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    } else if (pairs.getKey().equals(entity.CITYCODE)) {
                        entity.setCitycode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.SUMCODE)) {
                        entity.setSumcode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.BAGCODE)) {
                        entity.setBagcode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.GAZARNER)) {
                        entity.setGazarner(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.URTRAG)) {
                        entity.setUrtrag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.ORGOROG)) {
                        entity.setOrgorog(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.GOLOMTTOO)) {
                        entity.setGolomttoo(stringToInteger(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(entity.OVCHIN_NER)) {
                        entity.setOvchin_ner(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.MAL_TURUL)) {
                        entity.setMal_turul(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.UVCHILSON)) {
                        entity.setUvchilson(stringToInteger(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(entity.UHSEN)) {
                        entity.setUhsen(stringToInteger(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(entity.EDGERSEN)) {
                        entity.setEdgersen(stringToInteger(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (!dv.isEmpty() && dv.length() >= 8) {
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        }
                    } else if (pairs.getKey().toString().equalsIgnoreCase("command")) {
                        command = String.valueOf(pairs.getValue());
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (command.equalsIgnoreCase("INS")) {
                entity.setDelflg("N");
                if (user != null && (user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                        || user.getRoleCode().equalsIgnoreCase("ADMIN"))) {
                    //Aimag songoh shaardlagagui
                } else {
                    entity.setCitycode(user.getCity());
                }
                //entity.setSumcode(user.getSum());
                entity.setCreby(user.getUsername());
                Date utilDate = new Date();
                java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                entity.setCreat(sqlDate);
                if (entity.getRecordDate() == null) entity.setCreat(sqlDate);

                errorEntity = service.insertNewData(entity);

//                //Begin Check Role Step 2
//                if (roleControl.checkRole(user, menuid, 2)){
//
//                }else {
//                    errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1002)); // Ерөнхий алдаа : Эрх нь хүрэлцэхгүй үед өгөх
//                }
//                //End Check Role Step 2
            } else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL") ||
                    command.equalsIgnoreCase("ACT")) {
                int quality = 2;

                if (command.equalsIgnoreCase("DEL")) {
                    entity.setDelflg("Y");
                    quality = 3;
                }
                errorEntity = service.updateData(entity);
//                //Begin Check Role Step 2
//                if (roleControl.checkRole(user, menuid, quality)){
//                    errorEntity = service.updateData(entity);
//                }else {
//                    errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1002)); // Ерөнхий алдаа : Эрх нь хүрэлцэхгүй үед өгөх
//                }
//                //End Check Role Step 2
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return errorEntity;
    }//
//end-form09hav

    @Override
    public ErrorEntity writeFavoriteMenuLog(String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: write favorite page");

        try {
            FavoritePageControl service = new FavoritePageControl(super.conn);

            User user = sessionUser.get(sessionId);

            menuid = menuid.replaceAll("#", "");

            service.writeLog(user.getUsername(), menuid);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return errorEntity;
    }

    @Override
    public List<FavoritePages> getFavoriteMenuList(String sessionId) throws Exception {
        System.out.println("Hospital Service Implament ::: write favorite page");

        try {
            FavoritePageControl service = new FavoritePageControl(super.conn);

            User user = sessionUser.get(sessionId);

            FavoritePages favoritePages = new FavoritePages();

            favoritePages.setCreate_by(user.getUsername());

            return service.getFavoritePages(favoritePages);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public List<Form13aEntity> getForm13aData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 08 havsralt data");

        try {
            IForm13aService service = new Form13aServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form13aEntity entity = new Form13aEntity();

            Iterator it = criteria.entrySet().iterator();

            //Check Role
            roleControl = new CheckUserRoleControl(super.conn);

            if (roleControl == null) {
                return null;
            }

            menuid = menuid.replaceAll("#", "");

            if (!roleControl.checkRole(user, menuid, 1)) {
                return null;
            }
            //End Check Role

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (!pairs.getValue().toString().isEmpty()) {
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.CITYCODE)) {
                            entity.setCitycode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SUMCODE)) {
                            entity.setSumcode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.BAG_CODE)) {
                            entity.setBag_code(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.UZLEG_HIIH)) {
                            entity.setUzleg_hiih(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.HEMJIH_NEGJ)) {
                            entity.setHemjih_negj(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.TOO_HEMJEE)) {
                            entity.setToo_hemjee(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.DEEJIIN_TOO)) {
                            entity.setDeejiin_too(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SHINJILGEE_HIISEN)) {
                            entity.setShinjilgee_hiisen(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SHINJILSEN_ARGA)) {
                            entity.setShinjilsen_arga(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.HERGTSEEND_TOHIRSON)) {
                            entity.setHergtseend_tohirson(Double.parseDouble(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.USTGASAN)) {
                            entity.setUstgasan(Double.parseDouble(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.HALDBARGUIJUULELT)) {
                            entity.setHaldbarguijuulelt(Double.parseDouble(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.BRUTSELLYOZ)) {
                            entity.setBrutsellyoz(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.MASTIT)) {
                            entity.setMastit(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.TRIXINELL)) {
                            entity.setTrixinell(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.FINNOZ)) {
                            entity.setFinnoz(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SURIYEE)) {
                            entity.setSuriyee(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.BOOM)) {
                            entity.setBoom(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.BUSAD)) {
                            entity.setBusad(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.BUGD)) {
                            entity.setBugd(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.DEL_FLG)) {
                            entity.setDel_flg(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.CRE_BY)) {
                            entity.setCre_by(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.NO_DATA)) {
                            entity.setNo_data(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                entity.setCre_by(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                return service.getListData(entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public ErrorEntity setForm13aData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: setForm13aData havsralt data");

        try {
            String command = "EDT";
            IForm13aService service = new Form13aServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form13aEntity entity = new Form13aEntity();
            //Begin Check Role Step 1
            roleControl = new CheckUserRoleControl(super.conn);

            if (roleControl == null) {
                errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1002)); // Ерөнхий алдаа : Эрх нь хүрэлцэхгүй үед өгөх
                return errorEntity;
            }
            //End Check Role Step 1

            menuid = menuid.replaceAll("#", "");

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(entity.ID)) {
                        String tmpId = "";
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                            if (pairs.getValue().toString().contains(".")) {
                                tmpId = pairs.getValue().toString().substring(0, pairs.getValue().toString().indexOf("."));
                            }
                            entity.setId(Long.parseLong(String.valueOf(tmpId)));
                        }
                    } else if (pairs.getKey().equals(entity.CITYCODE)) {
                        entity.setCitycode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.SUMCODE)) {
                        entity.setSumcode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.BAG_CODE)) {
                        entity.setBag_code(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.UZLEG_HIIH)) {
                        entity.setUzleg_hiih(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.HEMJIH_NEGJ)) {
                        entity.setHemjih_negj(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.TOO_HEMJEE)) {
                        entity.setToo_hemjee(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.DEEJIIN_TOO)) {
                        entity.setDeejiin_too(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.SHINJILGEE_HIISEN)) {
                        entity.setShinjilgee_hiisen(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.SHINJILSEN_ARGA)) {
                        entity.setShinjilsen_arga(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.HERGTSEEND_TOHIRSON)) {
                        try {
                            entity.setHergtseend_tohirson(Double.valueOf(Double.parseDouble(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            entity.setHergtseend_tohirson(Double.valueOf(Long.parseLong("0")));
                        }
                    } else if (pairs.getKey().equals(entity.USTGASAN)) {
                        try {
                            entity.setUstgasan(Double.valueOf(Double.parseDouble(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            entity.setUstgasan(Double.valueOf(Long.parseLong("0")));
                        }
                    } else if (pairs.getKey().equals(entity.HALDBARGUIJUULELT)) {
                        try {
                            entity.setHaldbarguijuulelt(Double.valueOf(Double.parseDouble(pairs.getValue().toString())));
                        } catch (NumberFormatException e) {
                            entity.setHaldbarguijuulelt(Double.valueOf(Long.parseLong("0")));
                        }
                    } else if (pairs.getKey().equals(entity.BRUTSELLYOZ)) {
                        entity.setBrutsellyoz(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.MASTIT)) {
                        entity.setMastit(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.TRIXINELL)) {
                        entity.setTrixinell(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.FINNOZ)) {
                        entity.setFinnoz(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.SURIYEE)) {
                        entity.setSuriyee(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.BOOM)) {
                        entity.setBoom(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.BUSAD)) {
                        entity.setBusad(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.BUGD)) {
                        entity.setBugd(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.DEL_FLG)) {
                        entity.setDel_flg(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.CRE_BY)) {
                        entity.setCre_by(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.NO_DATA)) {
                        entity.setNo_data(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (!dv.isEmpty() && dv.length() >= 8) {
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        }
                    } else if (pairs.getKey().toString().equalsIgnoreCase("command")) {
                        command = String.valueOf(pairs.getValue());
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            entity.setMod_at(sqlDate);
            entity.setMod_by(user.getUsername());

            if (command.equalsIgnoreCase("INS")) {
                entity.setDel_flg("N");
                entity.setCitycode(user.getCity());
                entity.setSumcode(user.getSum());
                entity.setCre_by(user.getUsername());
                entity.setCre_at(sqlDate);
                if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);

                //Begin Check Role Step 2
                if (roleControl.checkRole(user, menuid, 2)) {
                    errorEntity = service.insertNewData(entity);
                } else {
                    errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1002)); // Ерөнхий алдаа : Эрх нь хүрэлцэхгүй үед өгөх
                }
                //End Check Role Step 2
            } else if (command.equalsIgnoreCase("EDT") ||
                    command.equalsIgnoreCase("DEL") ||
                    command.equalsIgnoreCase("ACT")) {
                int quality = 2;

                if (command.equalsIgnoreCase("DEL")) {
                    entity.setDel_flg("Y");
                    quality = 3;
                }

                //Begin Check Role Step 2
                if (roleControl.checkRole(user, menuid, quality)) {
                    errorEntity = service.updateData(entity);
                } else {
                    errorEntity = new ErrorEntity(ErrorType.ERROR, Long.valueOf(1002)); // Ерөнхий алдаа : Эрх нь хүрэлцэхгүй үед өгөх
                }
                //End Check Role Step 2
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return errorEntity;
    }

    @Override
    public List<User> getAllUserList(LinkedTreeMap credata, String sessionId) throws Exception {

        System.out.println("Hospital Service ::: get all user list");

        try {
            IUserService userService = new UserServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            User seluser = new User();

            Iterator it = credata.entrySet().iterator();


            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(seluser.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            seluser.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(seluser.USERNAME)) {
                        seluser.setUsername(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.ROLECODE)) {
                        seluser.setRoleCode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.FIRSTNAME)) {
                        seluser.setFirstName(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.LASTNAME)) {
                        seluser.setLastName(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.GENDER)) {
                        seluser.setGender(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.REGID)) {
                        seluser.setRegId(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.CITY)) {
                        seluser.setCity(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.SUM)) {
                        seluser.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.CREBY)) {
                        seluser.setCreBy(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.MODBY)) {
                        seluser.setModBy(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.DELFLG)) {
                        seluser.setDelFlg(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(seluser.ACTFLG)) {
                        seluser.setActFlg(String.valueOf(pairs.getValue()));
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            seluser.setDelFlg("N");

            List<User> arrUserList = new ArrayList<>();
            List<User> listUser = userService.getUserList(seluser);

            if (listUser != null) {
                for (User us : listUser) {
                    boolean isme = false;
                    User tmp = new User();

                    tmp.put("status", "offline");
                    tmp.setChatId(us.getChatId());
                    tmp.setUsername(us.getUsername());
                    tmp.setFirstName(us.getFirstName());
                    tmp.setLastName(us.getLastName());
                    tmp.setCityName(us.getCityName());
                    tmp.setCityNameEn(us.getCityNameEn());
                    tmp.setSumName(us.getSumName());
                    tmp.setSumNameEn(us.getSumNameEn());

                    for (Map.Entry<String, User> entry : sessionUser.entrySet()) {
                        String key = entry.getKey();
                        User su = entry.getValue();

                        if (su.getUsername().equalsIgnoreCase(tmp.getUsername())) {
                            if (key.equalsIgnoreCase(sessionId)) {
                                isme = true;
                            }
                            tmp.put("status", "online");
                        }
                    }//end of for
                    if (isme) continue;

                    arrUserList.add(tmp);
                }
            }

            return arrUserList;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }


    /*
    *
    * CHART
    * */
    public List<BasicDBObject> getForm08HavsraltPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 08 havsralt data");

        try {
            IForm08HavsraltService service = new Form08HavsraltServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form08HavsraltEntity entity = new Form08HavsraltEntity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (!pairs.getValue().toString().isEmpty()) {
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.CITYCODE)) {
                            entity.setCitycode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SUMCODE)) {
                            entity.setSumcode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.BAGCODE)) {
                            entity.setBagcode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.GAZARNER)) {
                            entity.setGazarner(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.URTRAG)) {
                            entity.setUrtrag(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.ORGOROG)) {
                            entity.setOrgorog(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.GOLOMTTOO)) {
                            entity.setGolomttoo(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.OVCHIN_NER)) {
                            entity.setOvchin_ner(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.MAL_TURUL)) {
                            entity.setMal_turul(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.UVCHILSON)) {
                            entity.setUvchilson(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.UHSEN)) {
                            entity.setUhsen(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.EDGERSEN)) {
                            entity.setEdgersen(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.USTGASAN)) {
                            entity.setUstgasan(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.NYDLUULSAN)) {
                            entity.setNydluulsan(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.LAB_NER)) {
                            entity.setLab_ner(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SHINJILGEE_ARGA)) {
                            entity.setShinjilgee_arga(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            entity.setDelflg("N");
            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                entity.setCreby(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                return service.getPieChartData(entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public BasicDBObject getForm08HavsraltColumnChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 08 havsralt data");

        try {
            IForm08HavsraltService service = new Form08HavsraltServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form08HavsraltEntity entity = new Form08HavsraltEntity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (!pairs.getValue().toString().isEmpty()) {
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.CITYCODE)) {
                            entity.setCitycode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SUMCODE)) {
                            entity.setSumcode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.BAGCODE)) {
                            entity.setBagcode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.GAZARNER)) {
                            entity.setGazarner(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.URTRAG)) {
                            entity.setUrtrag(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.ORGOROG)) {
                            entity.setOrgorog(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.GOLOMTTOO)) {
                            entity.setGolomttoo(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.OVCHIN_NER)) {
                            entity.setOvchin_ner(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.MAL_TURUL)) {
                            entity.setMal_turul(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.UVCHILSON)) {
                            entity.setUvchilson(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.UHSEN)) {
                            entity.setUhsen(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.EDGERSEN)) {
                            entity.setEdgersen(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.USTGASAN)) {
                            entity.setUstgasan(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.NYDLUULSAN)) {
                            entity.setNydluulsan(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.LAB_NER)) {
                            entity.setLab_ner(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SHINJILGEE_ARGA)) {
                            entity.setShinjilgee_arga(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            entity.setDelflg("N");

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                entity.setCreby(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                return service.getColHighChartData(entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    /**
     * Pie Chart
     *
     * @param
     * @return
     * @throws NumberFormatException
     */
    @Override
    public List<BasicDBObject> getForm09PieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: getForm09PieChartData");

        try {
            IForm09Service formService = new Form09ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form09Entity form09Entity = new Form09Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form09Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            form09Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(form09Entity.AIMAG)) {
                        form09Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.SUM)) {
                        form09Entity.setSum(form09Entity.SUM);
                    } else if (pairs.getKey().equals(form09Entity.MALTURUL)) {
                        form09Entity.setMalTurul(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.TARILGANER)) {
                        form09Entity.setTarilgaNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.NER)) {
                        form09Entity.setNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.ZARTSUULSANHEMJEE)) {
                        form09Entity.setZartsuulsanHemjee(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.TOLOVLOGOO)) {
                        form09Entity.setTolovlogoo(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.GUITSETGEL)) {
                        form09Entity.setGuitsetgel(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.HUVI)) {
                        form09Entity.setHuvi(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.OLGOSON_TUN)) {
                        try {
                            form09Entity.setOLGOSON_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form09Entity.setOLGOSON_TUN(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form09Entity.HEREGLESEN_TUN)) {
                        try {
                            form09Entity.setHEREGLESEN_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form09Entity.setHEREGLESEN_TUN(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form09Entity.USTGASAN_TUN)) {
                        try {
                            form09Entity.setUSTGASAN_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form09Entity.setUSTGASAN_TUN(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form09Entity.NUUTSULSEN_TUN)) {
                        try {
                            form09Entity.setNUUTSULSEN_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form09Entity.setNUUTSULSEN_TUN(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form09Entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form09Entity.setRecordDate(sqlDate);
                    } else if (pairs.getKey().equals(form09Entity.SEARCH_RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form09Entity.setSearchRecordDate(sqlDate);
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (form09Entity.getAimag() == null) form09Entity.setAimag(user.getCity());
            //if(form08Entity.getSum() == null)form08Entity.setSum(user.getSum());
            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                form09Entity.setCreby(user.getUsername());
            }

            form09Entity.setDelflg("N");
            form09Entity.setActflg("Y");

            if (form09Entity != null && form09Entity.getRecordDate() != null && form09Entity.getSearchRecordDate() != null) {
                return formService.getPieChartData(form09Entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public BasicDBObject getForm09ColumnChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: getForm09ColumnChartData");

        try {
            IForm09Service formService = new Form09ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form09Entity form09Entity = new Form09Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form09Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            form09Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(form09Entity.AIMAG)) {
                        form09Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.SUM)) {
                        form09Entity.setSum(form09Entity.SUM);
                    } else if (pairs.getKey().equals(form09Entity.MALTURUL)) {
                        form09Entity.setMalTurul(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.TARILGANER)) {
                        form09Entity.setTarilgaNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.NER)) {
                        form09Entity.setNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.ZARTSUULSANHEMJEE)) {
                        form09Entity.setZartsuulsanHemjee(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.TOLOVLOGOO)) {
                        form09Entity.setTolovlogoo(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.GUITSETGEL)) {
                        form09Entity.setGuitsetgel(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.HUVI)) {
                        form09Entity.setHuvi(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form09Entity.OLGOSON_TUN)) {
                        try {
                            form09Entity.setOLGOSON_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form09Entity.setOLGOSON_TUN(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form09Entity.HEREGLESEN_TUN)) {
                        try {
                            form09Entity.setHEREGLESEN_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form09Entity.setHEREGLESEN_TUN(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form09Entity.USTGASAN_TUN)) {
                        try {
                            form09Entity.setUSTGASAN_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form09Entity.setUSTGASAN_TUN(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form09Entity.NUUTSULSEN_TUN)) {
                        try {
                            form09Entity.setNUUTSULSEN_TUN(Long.valueOf(pairs.getValue().toString()));
                        } catch (NumberFormatException e) {
                            form09Entity.setNUUTSULSEN_TUN(Long.parseLong("0"));
                        }
                    } else if (pairs.getKey().equals(form09Entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form09Entity.setRecordDate(sqlDate);
                    } else if (pairs.getKey().equals(form09Entity.SEARCH_RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form09Entity.setSearchRecordDate(sqlDate);
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (form09Entity.getAimag() == null) form09Entity.setAimag(user.getCity());
            //if(form08Entity.getSum() == null)form08Entity.setSum(user.getSum());
            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                form09Entity.setCreby(user.getUsername());
            }

            form09Entity.setDelflg("N");
            form09Entity.setActflg("Y");

            if (form09Entity != null && form09Entity.getRecordDate() != null && form09Entity.getSearchRecordDate() != null) {
                return formService.getColumnChartData(form09Entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public List<BasicDBObject> getForm09HavsraltPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: getForm09HavsraltPieChartData");

        try {
            IForm09HavsraltService form09Service = new Form09HavsraltServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form09HavsraltEntity entity = new Form09HavsraltEntity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (!pairs.getValue().toString().isEmpty()) {
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.CITYCODE)) {
                            entity.setCitycode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SUMCODE)) {
                            entity.setSumcode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.BAGCODE)) {
                            entity.setBagcode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.GAZARNER)) {
                            entity.setGazarner(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.URTRAG)) {
                            entity.setUrtrag(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.ORGOROG)) {
                            entity.setOrgorog(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.GOLOMTTOO)) {
                            entity.setGolomttoo(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.OVCHIN_NER)) {
                            entity.setOvchin_ner(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.MAL_TURUL)) {
                            entity.setMal_turul(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.UVCHILSON)) {
                            entity.setUvchilson(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.UHSEN)) {
                            entity.setUhsen(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.EDGERSEN)) {
                            entity.setEdgersen(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                entity.setCreby(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                return form09Service.getPieChartData(entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public BasicDBObject getForm09HavsraltColumnChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: getForm09HavsraltColumnChartData");

        try {
            IForm09HavsraltService form09Service = new Form09HavsraltServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form09HavsraltEntity entity = new Form09HavsraltEntity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (!pairs.getValue().toString().isEmpty()) {
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.CITYCODE)) {
                            entity.setCitycode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SUMCODE)) {
                            entity.setSumcode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.BAGCODE)) {
                            entity.setBagcode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.GAZARNER)) {
                            entity.setGazarner(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.URTRAG)) {
                            entity.setUrtrag(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.ORGOROG)) {
                            entity.setOrgorog(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.GOLOMTTOO)) {
                            entity.setGolomttoo(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.OVCHIN_NER)) {
                            entity.setOvchin_ner(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.MAL_TURUL)) {
                            entity.setMal_turul(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.UVCHILSON)) {
                            entity.setUvchilson(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.UHSEN)) {
                            entity.setUhsen(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.EDGERSEN)) {
                            entity.setEdgersen(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                entity.setCreby(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                return form09Service.getColumnChartData(entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public List<BasicDBObject> getForm11PieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: getForm11PieChartData");

        try {
            IForm11Service form11Service = new Form11ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form11Entity form11Entity = new Form11Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form11Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                            form11Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        }
                    } else if (pairs.getKey().equals(form11Entity.AIMAG)) {
                        form11Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.SUM)) {
                        form11Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.BAGCODE)) {
                        form11Entity.setBagCode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.MALTURUL)) {
                        form11Entity.setMalTurul(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.DEEJIINTURUL)) {
                        form11Entity.setDeejiinTurul(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.SUMBAGNER)) {
                        form11Entity.setSumBagNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.TOOTOLGOI)) {
                        try {
                            form11Entity.setTooTolgoi(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch (Exception e) {
                            form11Entity.setTooTolgoi(Long.parseLong(String.valueOf("0")));
                        }
                    } else if (pairs.getKey().equals(form11Entity.HAANASHINJILSEN)) {
                        form11Entity.setHaanaShinjilsen(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.SHINJILSENARGA)) {
                        form11Entity.setShinjilsenArga(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.ERUUL)) {
                        try {
                            form11Entity.setEruul(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch (Exception e) {
                            form11Entity.setEruul(Long.parseLong(String.valueOf("0")));
                        }
                    } else if (pairs.getKey().equals(form11Entity.UVCHTEI)) {
                        try {
                            form11Entity.setUvchtei(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch (Exception e) {
                            form11Entity.setUvchtei(Long.parseLong(String.valueOf("0")));
                        }
                    } else if (pairs.getKey().equals(form11Entity.ONOSH)) {
                        try {
                            form11Entity.setOnosh(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch (Exception e) {
                            form11Entity.setOnosh(Long.parseLong(String.valueOf("0")));
                        }
                    } else if (pairs.getKey().equals(form11Entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form11Entity.setRecordDate(sqlDate);
                    } else if (pairs.getKey().equals(form11Entity.SEARCH_RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form11Entity.setSearchRecordDate(sqlDate);
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                form11Entity.setCreby(user.getUsername());
            }


            if (form11Entity != null && form11Entity.getRecordDate() != null && form11Entity.getSearchRecordDate() != null) {
                return form11Service.getPieChartData(form11Entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public BasicDBObject getForm11ColumnChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: getForm11ColumnChartData");

        try {
            IForm11Service form11Service = new Form11ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form11Entity form11Entity = new Form11Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form11Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty()) {
                            form11Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        }
                    } else if (pairs.getKey().equals(form11Entity.AIMAG)) {
                        form11Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.SUM)) {
                        form11Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.BAGCODE)) {
                        form11Entity.setBagCode(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.MALTURUL)) {
                        form11Entity.setMalTurul(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.DEEJIINTURUL)) {
                        form11Entity.setDeejiinTurul(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.SUMBAGNER)) {
                        form11Entity.setSumBagNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.TOOTOLGOI)) {
                        try {
                            form11Entity.setTooTolgoi(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch (Exception e) {
                            form11Entity.setTooTolgoi(Long.parseLong(String.valueOf("0")));
                        }
                    } else if (pairs.getKey().equals(form11Entity.HAANASHINJILSEN)) {
                        form11Entity.setHaanaShinjilsen(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.SHINJILSENARGA)) {
                        form11Entity.setShinjilsenArga(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form11Entity.ERUUL)) {
                        try {
                            form11Entity.setEruul(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch (Exception e) {
                            form11Entity.setEruul(Long.parseLong(String.valueOf("0")));
                        }
                    } else if (pairs.getKey().equals(form11Entity.UVCHTEI)) {
                        try {
                            form11Entity.setUvchtei(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch (Exception e) {
                            form11Entity.setUvchtei(Long.parseLong(String.valueOf("0")));
                        }
                    } else if (pairs.getKey().equals(form11Entity.ONOSH)) {
                        try {
                            form11Entity.setOnosh(Long.parseLong(String.valueOf(pairs.getValue())));
                        } catch (Exception e) {
                            form11Entity.setOnosh(Long.parseLong(String.valueOf("0")));
                        }
                    } else if (pairs.getKey().equals(form11Entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form11Entity.setRecordDate(sqlDate);
                    } else if (pairs.getKey().equals(form11Entity.SEARCH_RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                        java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                        form11Entity.setSearchRecordDate(sqlDate);
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                form11Entity.setCreby(user.getUsername());
            }


            if (form11Entity != null && form11Entity.getRecordDate() != null && form11Entity.getSearchRecordDate() != null) {
                return form11Service.getColumnChartData(form11Entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public BasicDBObject getForm08HightChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 08 data");

        try {
            IForm08Service form08Service = new Form08ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form08Entity form08Entity = new Form08Entity();

            Iterator it = criteria.entrySet().iterator();

            //Check Role
            /*roleControl = new CheckUserRoleControl(super.conn);

            if (roleControl == null) {
                return null;
            }

            menuid = menuid.replaceAll("#", "");

            if (!roleControl.checkRole(user, menuid, 1)){
                return null;
            }*/
            //End Check Role

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form08Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            form08Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(form08Entity.AIMAG)) {
                        form08Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.SUM)) {
                        form08Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.MALTURUL)) {
                        form08Entity.setMalTurul(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.ARGA_HEMJEE_NER)) {
                        form08Entity.setArgaHemjeeNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.NER)) {
                        form08Entity.setNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.ZARTSUULSAN_HEMJEE)) {
                        form08Entity.setZartsuulsanHemjee(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.TOLOVLOGOO)) {
                        form08Entity.setTolovlogoo(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.GUITSETGEL)) {
                        form08Entity.setGuitsetgel(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.HUVI)) {
                        form08Entity.setHuvi(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()) {
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form08Entity.setRecordDate(sqlDate);
                        }
                    } else if (pairs.getKey().equals(form08Entity.SEARCH_RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()) {
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form08Entity.setSearchRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                form08Entity.setCreby(user.getUsername());
            }
            //if(form08Entity.getSum() == null)form08Entity.setSum(user.getSum());
            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                form08Entity.setCreby(user.getUsername());
            }

            form08Entity.setDelflg("N");
            form08Entity.setActflg("Y");

            if (form08Entity != null && form08Entity.getRecordDate() != null && form08Entity.getSearchRecordDate() != null) {
                return form08Service.getColHighChartData(form08Entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public BasicDBObject getForm08HightPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: getForm08HightPieChartData");

        try {
            IForm08Service form08Service = new Form08ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form08Entity form08Entity = new Form08Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (pairs.getKey().equals(form08Entity.ID)) {
                        if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                            form08Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    } else if (pairs.getKey().equals(form08Entity.AIMAG)) {
                        form08Entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.SUM)) {
                        form08Entity.setSum(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.MALTURUL)) {
                        form08Entity.setMalTurul(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.ARGA_HEMJEE_NER)) {
                        form08Entity.setArgaHemjeeNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.NER)) {
                        form08Entity.setNer(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.ZARTSUULSAN_HEMJEE)) {
                        form08Entity.setZartsuulsanHemjee(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.TOLOVLOGOO)) {
                        form08Entity.setTolovlogoo(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.GUITSETGEL)) {
                        form08Entity.setGuitsetgel(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.HUVI)) {
                        form08Entity.setHuvi(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(form08Entity.RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()) {
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form08Entity.setRecordDate(sqlDate);
                        }
                    } else if (pairs.getKey().equals(form08Entity.SEARCH_RECORD_DATE)) {
                        String dv = String.valueOf(pairs.getValue());
                        if (dv != null && !dv.isEmpty()) {
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form08Entity.setSearchRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                form08Entity.setCreby(user.getUsername());
                if (form08Entity.getAimag() == null) form08Entity.setAimag(user.getCity());
            }

            form08Entity.setDelflg("N");
            form08Entity.setActflg("Y");

            if (form08Entity != null && form08Entity.getRecordDate() != null && form08Entity.getSearchRecordDate() != null) {
                return form08Service.getPieHighChartData(form08Entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public BasicDBObject getForm08HavHightChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 08 havsralt hight chart data");

        try {
            IForm08HavsraltService service = new Form08HavsraltServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form08HavsraltEntity entity = new Form08HavsraltEntity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (!pairs.getValue().toString().isEmpty()) {
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.CITYCODE)) {
                            entity.setCitycode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SUMCODE)) {
                            entity.setSumcode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.BAGCODE)) {
                            entity.setBagcode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.GAZARNER)) {
                            entity.setGazarner(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.URTRAG)) {
                            entity.setUrtrag(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.ORGOROG)) {
                            entity.setOrgorog(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.GOLOMTTOO)) {
                            entity.setGolomttoo(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.OVCHIN_NER)) {
                            entity.setOvchin_ner(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.MAL_TURUL)) {
                            entity.setMal_turul(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.UVCHILSON)) {
                            entity.setUvchilson(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.UHSEN)) {
                            entity.setUhsen(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.EDGERSEN)) {
                            entity.setEdgersen(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.USTGASAN)) {
                            entity.setUstgasan(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.NYDLUULSAN)) {
                            entity.setNydluulsan(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.LAB_NER)) {
                            entity.setLab_ner(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SHINJILGEE_ARGA)) {
                            entity.setShinjilgee_arga(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            entity.setDelflg("N");

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                entity.setCreby(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                return service.getColHighChartData(entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public BasicDBObject getForm08HavHightPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: getForm08HavHightPieChartData");

        try {
            IForm08HavsraltService service = new Form08HavsraltServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form08HavsraltEntity entity = new Form08HavsraltEntity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (!pairs.getValue().toString().isEmpty()) {
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.CITYCODE)) {
                            entity.setCitycode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SUMCODE)) {
                            entity.setSumcode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.BAGCODE)) {
                            entity.setBagcode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.GAZARNER)) {
                            entity.setGazarner(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.URTRAG)) {
                            entity.setUrtrag(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.ORGOROG)) {
                            entity.setOrgorog(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.GOLOMTTOO)) {
                            entity.setGolomttoo(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.OVCHIN_NER)) {
                            entity.setOvchin_ner(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.MAL_TURUL)) {
                            entity.setMal_turul(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.UVCHILSON)) {
                            entity.setUvchilson(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.UHSEN)) {
                            entity.setUhsen(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.EDGERSEN)) {
                            entity.setEdgersen(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.USTGASAN)) {
                            entity.setUstgasan(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.NYDLUULSAN)) {
                            entity.setNydluulsan(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.LAB_NER)) {
                            entity.setLab_ner(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SHINJILGEE_ARGA)) {
                            entity.setShinjilgee_arga(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            entity.setDelflg("N");
            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                entity.setCreby(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                return service.getPieHighChartData(entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    //    form09hav-chart
    @Override
    public BasicDBObject getForm09HavHightChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 08 havsralt hight chart data");

        try {
            IForm09HavsraltService service = new Form09HavsraltServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form09HavsraltEntity entity = new Form09HavsraltEntity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (!pairs.getValue().toString().isEmpty()) {
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.CITYCODE)) {
                            entity.setCitycode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SUMCODE)) {
                            entity.setSumcode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.BAGCODE)) {
                            entity.setBagcode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.GAZARNER)) {
                            entity.setGazarner(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.URTRAG)) {
                            entity.setUrtrag(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.ORGOROG)) {
                            entity.setOrgorog(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.GOLOMTTOO)) {
                            entity.setGolomttoo(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.OVCHIN_NER)) {
                            entity.setOvchin_ner(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.MAL_TURUL)) {
                            entity.setMal_turul(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.UVCHILSON)) {
                            entity.setUvchilson(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.UHSEN)) {
                            entity.setUhsen(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.EDGERSEN)) {
                            entity.setEdgersen(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            entity.setDelflg("N");

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                entity.setCreby(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                return service.getColHighChartData(entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }

    @Override
    public BasicDBObject getForm09HavHightPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: getForm08HavHightPieChartData");

        try {
            IForm09HavsraltService service = new Form09HavsraltServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form09HavsraltEntity entity = new Form09HavsraltEntity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (!pairs.getValue().toString().isEmpty()) {
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.CITYCODE)) {
                            entity.setCitycode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SUMCODE)) {
                            entity.setSumcode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.BAGCODE)) {
                            entity.setBagcode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.GAZARNER)) {
                            entity.setGazarner(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.URTRAG)) {
                            entity.setUrtrag(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.ORGOROG)) {
                            entity.setOrgorog(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.GOLOMTTOO)) {
                            entity.setGolomttoo(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.OVCHIN_NER)) {
                            entity.setOvchin_ner(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.MAL_TURUL)) {
                            entity.setMal_turul(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.UVCHILSON)) {
                            entity.setUvchilson(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.UHSEN)) {
                            entity.setUhsen(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.EDGERSEN)) {
                            entity.setEdgersen(Integer.parseInt(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            entity.setDelflg("N");
            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                entity.setCreby(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                return service.getPieHighChartData(entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }
// form09hav-chart



//    form11-chart

    @Override
    public BasicDBObject getForm11HightChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 11 havsralt hight chart data");

        try{
            IForm11Service service = new Form11ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form11Entity form11Entity = new Form11Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try{
                    if (!pairs.getValue().toString().isEmpty()){
                        if (pairs.getKey().equals(form11Entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                form11Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        }
                        else if (pairs.getKey().equals(form11Entity.AIMAG)) {
                            form11Entity.setAimag(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form11Entity.SUM)) {
                            form11Entity.setSum(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form11Entity.BAGCODE)) {
                            form11Entity.setBagCode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form11Entity.MALTURUL)) {
                            form11Entity.setMalTurul(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form11Entity.DEEJIINTURUL)) {
                            form11Entity.setDeejiinTurul(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form11Entity.SUMBAGNER)) {
                            form11Entity.setSumBagNer(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form11Entity.TOOTOLGOI)) {
                            try {
                                form11Entity.setTooTolgoi(Long.parseLong(String.valueOf(pairs.getValue())));
                            } catch (Exception e) {
                                form11Entity.setTooTolgoi(Long.parseLong(String.valueOf("0")));
                            }
                        } else if (pairs.getKey().equals(form11Entity.HAANASHINJILSEN)) {
                            form11Entity.setHaanaShinjilsen(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form11Entity.SHINJILSENARGA)) {
                            form11Entity.setShinjilsenArga(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form11Entity.ERUUL)) {
                            try {
                                form11Entity.setEruul(Long.parseLong(String.valueOf(pairs.getValue())));
                            } catch (Exception e) {
                                form11Entity.setEruul(Long.parseLong(String.valueOf("0")));
                            }
                        } else if (pairs.getKey().equals(form11Entity.UVCHTEI)) {
                            try {
                                form11Entity.setUvchtei(Long.parseLong(String.valueOf(pairs.getValue())));
                            } catch (Exception e) {
                                form11Entity.setUvchtei(Long.parseLong(String.valueOf("0")));
                            }
                        } else if (pairs.getKey().equals(form11Entity.ONOSH)) {
                            try {
                                form11Entity.setOnosh(Long.parseLong(String.valueOf(pairs.getValue())));
                            } catch (Exception e) {
                                form11Entity.setOnosh(Long.parseLong(String.valueOf("0")));
                            }
                        } else if (pairs.getKey().equals(form11Entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form11Entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(form11Entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form11Entity.setSearchRecordDate(sqlDate);
                        }
                    }
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (form11Entity.getRecordDate() == null) form11Entity.setRecordDate(sqlDate);
            if (form11Entity.getSearchRecordDate() == null) form11Entity.setSearchRecordDate(sqlDate);

            form11Entity.setDelflg("N");

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                form11Entity.setCreby(user.getUsername());
            }


            if (form11Entity != null && form11Entity.getRecordDate() != null && form11Entity.getSearchRecordDate() != null){
                return service.getColHighChartData(form11Entity);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {

        }

        return null;
    }


    @Override
    public BasicDBObject getForm11HightPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: getForm11HightPieChartData");

        try {
            IForm11Service service = new Form11ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form11Entity form11Entity = new Form11Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (!pairs.getValue().toString().isEmpty()) {
                        if (pairs.getKey().equals(form11Entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                form11Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(form11Entity.AIMAG)) {
                            form11Entity.setAimag(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form11Entity.SUM)) {
                            form11Entity.setSum(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form11Entity.BAGCODE)) {
                            form11Entity.setBagCode(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form11Entity.MALTURUL)) {
                            form11Entity.setMalTurul(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form11Entity.DEEJIINTURUL)) {
                            form11Entity.setDeejiinTurul(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form11Entity.SUMBAGNER)) {
                            form11Entity.setSumBagNer(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form11Entity.TOOTOLGOI)) {
                            try {
                                form11Entity.setTooTolgoi(Long.parseLong(String.valueOf(pairs.getValue())));
                            } catch (Exception e) {
                                form11Entity.setTooTolgoi(Long.parseLong(String.valueOf("0")));
                            }
                        } else if (pairs.getKey().equals(form11Entity.HAANASHINJILSEN)) {
                            form11Entity.setHaanaShinjilsen(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form11Entity.SHINJILSENARGA)) {
                            form11Entity.setShinjilsenArga(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form11Entity.ERUUL)) {
                            try {
                                form11Entity.setEruul(Long.parseLong(String.valueOf(pairs.getValue())));
                            } catch (Exception e) {
                                form11Entity.setEruul(Long.parseLong(String.valueOf("0")));
                            }
                        } else if (pairs.getKey().equals(form11Entity.UVCHTEI)) {
                            try {
                                form11Entity.setUvchtei(Long.parseLong(String.valueOf(pairs.getValue())));
                            } catch (Exception e) {
                                form11Entity.setUvchtei(Long.parseLong(String.valueOf("0")));
                            }
                        } else if (pairs.getKey().equals(form11Entity.ONOSH)) {
                            try {
                                form11Entity.setOnosh(Long.parseLong(String.valueOf(pairs.getValue())));
                            } catch (Exception e) {
                                form11Entity.setOnosh(Long.parseLong(String.valueOf("0")));
                            }
                        } else if (pairs.getKey().equals(form11Entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form11Entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(form11Entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form11Entity.setSearchRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (form11Entity.getRecordDate() == null) form11Entity.setRecordDate(sqlDate);
            if (form11Entity.getSearchRecordDate() == null) form11Entity.setSearchRecordDate(sqlDate);

            form11Entity.setDelflg("N");
            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                form11Entity.setCreby(user.getUsername());
            }


            if (form11Entity != null && form11Entity.getRecordDate() != null && form11Entity.getSearchRecordDate() != null) {
                return service.getPieHighChartData(form11Entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }


//form11-chart]

//    form09-chart

    @Override
    public BasicDBObject getForm09HightChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 09 havsralt hight chart data");

        try{
            IForm09Service service = new Form09ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form09Entity form09Entity = new Form09Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try{
                    if (!pairs.getValue().toString().isEmpty()){
                        if (pairs.getKey().equals(form09Entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                form09Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        }
                        else if (pairs.getKey().equals(form09Entity.AIMAG)) {
                            form09Entity.setAimag(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form09Entity.SUM)) {
                            form09Entity.setSum(form09Entity.SUM);
                        } else if (pairs.getKey().equals(form09Entity.MALTURUL)) {
                            form09Entity.setMalTurul(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form09Entity.TARILGANER)) {
                            form09Entity.setTarilgaNer(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form09Entity.NER)) {
                            form09Entity.setNer(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form09Entity.ZARTSUULSANHEMJEE)) {
                            form09Entity.setZartsuulsanHemjee(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form09Entity.TOLOVLOGOO)) {
                            form09Entity.setTolovlogoo(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form09Entity.GUITSETGEL)) {
                            form09Entity.setGuitsetgel(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form09Entity.HUVI)) {
                            form09Entity.setHuvi(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form09Entity.OLGOSON_TUN)) {
                            try {
                                form09Entity.setOLGOSON_TUN(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                form09Entity.setOLGOSON_TUN(Long.parseLong("0"));
                            }
                        } else if (pairs.getKey().equals(form09Entity.HEREGLESEN_TUN)) {
                            try {
                                form09Entity.setHEREGLESEN_TUN(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                form09Entity.setHEREGLESEN_TUN(Long.parseLong("0"));
                            }
                        } else if (pairs.getKey().equals(form09Entity.USTGASAN_TUN)) {
                            try {
                                form09Entity.setUSTGASAN_TUN(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                form09Entity.setUSTGASAN_TUN(Long.parseLong("0"));
                            }
                        } else if (pairs.getKey().equals(form09Entity.NUUTSULSEN_TUN)) {
                            try {
                                form09Entity.setNUUTSULSEN_TUN(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                form09Entity.setNUUTSULSEN_TUN(Long.parseLong("0"));
                            }
                        } else if (pairs.getKey().equals(form09Entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form09Entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(form09Entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form09Entity.setSearchRecordDate(sqlDate);
                        }
                    }
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (form09Entity.getRecordDate() == null) form09Entity.setRecordDate(sqlDate);
            if (form09Entity.getSearchRecordDate() == null) form09Entity.setSearchRecordDate(sqlDate);

            form09Entity.setDelflg("N");

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                form09Entity.setCreby(user.getUsername());
            }


            if (form09Entity != null && form09Entity.getRecordDate() != null && form09Entity.getSearchRecordDate() != null){
                return service.getColHighChartData(form09Entity);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {

        }

        return null;
    }


    @Override
    public BasicDBObject getForm09HightPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: getForm09HightPieChartData");

        try {
            IForm09Service service = new Form09ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form09Entity form09Entity = new Form09Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (!pairs.getValue().toString().isEmpty()) {
                        if (pairs.getKey().equals(form09Entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                form09Entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(form09Entity.AIMAG)) {
                            form09Entity.setAimag(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form09Entity.SUM)) {
                            form09Entity.setSum(form09Entity.SUM);
                        } else if (pairs.getKey().equals(form09Entity.MALTURUL)) {
                            form09Entity.setMalTurul(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form09Entity.TARILGANER)) {
                            form09Entity.setTarilgaNer(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form09Entity.NER)) {
                            form09Entity.setNer(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form09Entity.ZARTSUULSANHEMJEE)) {
                            form09Entity.setZartsuulsanHemjee(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form09Entity.TOLOVLOGOO)) {
                            form09Entity.setTolovlogoo(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form09Entity.GUITSETGEL)) {
                            form09Entity.setGuitsetgel(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form09Entity.HUVI)) {
                            form09Entity.setHuvi(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(form09Entity.OLGOSON_TUN)) {
                            try {
                                form09Entity.setOLGOSON_TUN(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                form09Entity.setOLGOSON_TUN(Long.parseLong("0"));
                            }
                        } else if (pairs.getKey().equals(form09Entity.HEREGLESEN_TUN)) {
                            try {
                                form09Entity.setHEREGLESEN_TUN(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                form09Entity.setHEREGLESEN_TUN(Long.parseLong("0"));
                            }
                        } else if (pairs.getKey().equals(form09Entity.USTGASAN_TUN)) {
                            try {
                                form09Entity.setUSTGASAN_TUN(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                form09Entity.setUSTGASAN_TUN(Long.parseLong("0"));
                            }
                        } else if (pairs.getKey().equals(form09Entity.NUUTSULSEN_TUN)) {
                            try {
                                form09Entity.setNUUTSULSEN_TUN(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                form09Entity.setNUUTSULSEN_TUN(Long.parseLong("0"));
                            }
                        } else if (pairs.getKey().equals(form09Entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form09Entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(form09Entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            form09Entity.setSearchRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (form09Entity.getRecordDate() == null) form09Entity.setRecordDate(sqlDate);
            if (form09Entity.getSearchRecordDate() == null) form09Entity.setSearchRecordDate(sqlDate);

            form09Entity.setDelflg("N");
            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                form09Entity.setCreby(user.getUsername());
            }


            if (form09Entity != null && form09Entity.getRecordDate() != null && form09Entity.getSearchRecordDate() != null) {
                return service.getPieHighChartData(form09Entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }


//    form09-chart

    //    form46-chart

    @Override
    public BasicDBObject getForm46HightChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 46 havsralt hight chart data");

        try{
            IForm46Service service = new Form46ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form46Entity entity = new Form46Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try{
                    if (!pairs.getValue().toString().isEmpty()){
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        }
                        else if (pairs.getKey().equals(entity.AIMAG)) {
                            entity.setAimag(String.valueOf(pairs.getValue()));
                        }else if (pairs.getKey().equals(entity.PIE)) {
                            entity.setPie(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SUM)) {
                            entity.setSum(entity.SUM);
                        } else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            entity.setDelflg("N");

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                entity.setCreby(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null){
                return service.getColHighChartData(entity);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {

        }

        return null;
    }
    // form 46 high chart

    //    form13a-chart

    @Override
    public BasicDBObject getForm13aHightChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 13a hight chart data");

        try{
            IForm13aService service = new Form13aServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form13aEntity entity = new Form13aEntity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try{
                    if (!pairs.getValue().toString().isEmpty()){
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        }
                        else if (pairs.getKey().equals(entity.CITYCODE)) {
                            entity.setCitycode(String.valueOf(pairs.getValue()));
                        }else if (pairs.getKey().equals(entity.SHINJILSEN_ARGA)) {
                            entity.setShinjilsen_arga(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.UZLEG_HIIH)) {
                            entity.setUzleg_hiih(String.valueOf(pairs.getValue()));
                        } if (pairs.getKey().equals(entity.SUMCODE)) {
                            entity.setSumcode(entity.SUMCODE);
                        } else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            entity.setDel_flg("N");

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                entity.setCre_by(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null){
                return service.getColHighChartData(entity);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {

        }

        return null;
    }


    @Override
    public BasicDBObject getForm13aHightPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: getForm13aHightPieChartData");

        try {
            IForm13aService service = new Form13aServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form13aEntity entity = new Form13aEntity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (!pairs.getValue().toString().isEmpty()) {
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        }else if (pairs.getKey().equals(entity.CITYCODE)) {
                            entity.setCitycode(String.valueOf(pairs.getValue()));
                        }else if (pairs.getKey().equals(entity.SHINJILSEN_ARGA)) {
                            entity.setShinjilsen_arga(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.UZLEG_HIIH)) {
                            entity.setUzleg_hiih(String.valueOf(pairs.getValue()));
                        } if (pairs.getKey().equals(entity.SUMCODE)) {
                            entity.setSumcode(entity.SUMCODE);
                        }  else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            entity.setDel_flg("N");
            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                entity.setCre_by(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                return service.getPieHighChartData(entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }


//    form13-chart


    @Override
    public BasicDBObject getForm46HightPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: getForm46HightPieChartData");

        try {
            IForm46Service service = new Form46ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form46Entity entity = new Form46Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (!pairs.getValue().toString().isEmpty()) {
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.PIE)) {
                            entity.setPie(String.valueOf(pairs.getValue()));
                        }  else if (pairs.getKey().equals(entity.AIMAG)) {
                            entity.setAimag(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SUM)) {
                            entity.setSum(entity.SUM);
                        }  else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            entity.setDelflg("N");
            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                entity.setCreby(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                return service.getPieHighChartData(entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }


//    form46-chart


    //    form14-chart

    @Override
    public BasicDBObject getForm14HightChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 14 havsralt hight chart data");

        try{
            IForm14Service service = new Form14ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form14Entity entity = new Form14Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try{
                    if (!pairs.getValue().toString().isEmpty()){
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        }
                        else if (pairs.getKey().equals(entity.AIMAG)) {
                            entity.setAimag(String.valueOf(pairs.getValue()));
                        }else if (pairs.getKey().equals(entity.SHALGAHGAZAR)) {
                            entity.setShalgahGazar(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SUM)) {
                            entity.setSum(entity.SUM);
                        } else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            entity.setDelflg("N");

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                entity.setCreby(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null){
                return service.getColHighChartData(entity);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {

        }

        return null;
    }


    @Override
    public BasicDBObject getForm14HightPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: getForm14HightPieChartData");

        try {
            IForm14Service service = new Form14ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form14Entity entity = new Form14Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (!pairs.getValue().toString().isEmpty()) {
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.SHALGAHGAZAR)) {
                            entity.setShalgahGazar(String.valueOf(pairs.getValue()));
                        }  else if (pairs.getKey().equals(entity.AIMAG)) {
                            entity.setAimag(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SUM)) {
                            entity.setSum(entity.SUM);
                        }  else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            entity.setDelflg("N");
            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                entity.setCreby(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                return service.getPieHighChartData(entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }


//    form14-chart

    //    form51-chart

    @Override
    public BasicDBObject getForm51HightChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 51 hight chart data");

        try{
            IForm51Service service = new Form51ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form51Entity entity = new Form51Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try{
                    if (!pairs.getValue().toString().isEmpty()){
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                    }else if (pairs.getKey().equals(entity.AIMAG)) {
                        entity.setAimag(String.valueOf(pairs.getValue()));
                    } else if (pairs.getKey().equals(entity.SUM)) {
                        entity.setSum(entity.SUM);
                    } else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            entity.setDelflg("N");

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                entity.setCreby(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null){
                return service.getColHighChartData(entity);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {

        }

        return null;
    }


    @Override
    public BasicDBObject getForm51HightPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: getForm51aHightPieChartData");

        try {
            IForm51Service service = new Form51ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form51Entity entity = new Form51Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (!pairs.getValue().toString().isEmpty()) {
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        }else if (pairs.getKey().equals(entity.AIMAG)) {
                            entity.setAimag(String.valueOf(pairs.getValue()));
                        } if (pairs.getKey().equals(entity.SUM)) {
                            entity.setSum(entity.SUM);
                        }  else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            entity.setDelflg("N");
            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                entity.setCreby(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                return service.getPieHighChartData(entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }


//    form51-chart

    //form52-chart

    @Override
    public BasicDBObject getForm52HightChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 52 havsralt hight chart data");

        try{
            IForm52Service service = new Form52ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form52Entity entity = new Form52Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try{
                    if (!pairs.getValue().toString().isEmpty()){
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        }
                        else if (pairs.getKey().equals(entity.AIMAG)) {
                            entity.setAimag(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SUM)) {
                            entity.setSum(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.UVCHINNER)) {
                            entity.setUvchinNer(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.MALNER)) {
                                entity.setMalNer(String.valueOf(pairs.getValue()));
                         }  else if (pairs.getKey().equals(entity.BUGDUVCHILSEN)) {
                            try {
                                entity.setBugdUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBugdUvchilsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.BUGDUHSEN)) {
                            try {
                                entity.setBugdUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBugdUhsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.BUGDEDGERSEN)) {
                            try {
                                entity.setBugdEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBugdEdgersen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.TEMEEUVCHILSEN)) {
                            try {
                                entity.setTemeeUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setTemeeUvchilsen(Long.valueOf("0"));
                            }
                        }
                        else if (pairs.getKey().equals(entity.TEMEEUHSEN)) {
                            try {
                                entity.setTemeeUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setTemeeUhsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.TEMEEEDGERSEN)) {
                            try {
                                entity.setTemeeEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setTemeeEdgersen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.ADUUUVCHILSEN)) {
                            try {
                                entity.setAduuUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setAduuUvchilsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.ADUUUHSEN)) {
                            try {
                                entity.setAduuUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setAduuUhsen(Long.valueOf("0"));
                            }

                        }
                        else if (pairs.getKey().equals(entity.ADUUEDGERSEN)) {
                            try {
                                entity.setAduuEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setAduuEdgersen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.UHERUVCHILSEN)) {
                            try {
                                entity.setUherUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setUherUvchilsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.UHERUHSEN)) {
                            try {
                                entity.setUherUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setUherUhsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.UHEREDGERSEN)) {
                            try {
                                entity.setUherEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setUherEdgersen(Long.valueOf("0"));
                            }

                        }
                        else if (pairs.getKey().equals(entity.HONIUVCHILSEN)) {
                            try {
                                entity.setHoniUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setHoniUvchilsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.HONIUHSEN)) {
                            try {
                                entity.setHoniUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setHoniUhsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.HONIEDGERSEN)) {
                            try {
                                entity.setHoniEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setHoniEdgersen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.YAMAAUVCHILSEN)) {
                            try {
                                entity.setYamaaUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setYamaaUvchilsen(Long.valueOf("0"));
                            }

                        }
                        else if (pairs.getKey().equals(entity.YAMAAUHSEN)) {
                            try {
                                entity.setYamaaUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setYamaaUhsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.YAMAAEDGERSEN)) {
                            try {
                                entity.setYamaaEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setYamaaEdgersen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.BUSADUVCHILSEN)) {
                            try {
                                entity.setBusadUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBusadUvchilsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.BUSADUHSEN)) {
                            try {
                                entity.setBusadUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBusadUhsen(Long.valueOf("0"));
                            }

                        }
                        else if (pairs.getKey().equals(entity.BUSADEDGERSEN)) {
                            try {
                                entity.setBusadEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBusadEdgersen(Long.valueOf("0"));
                            }

                        }
                        else  if (pairs.getKey().equals(entity.RECORD_DATE)){
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        }
                        else  if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)){
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            entity.setDelflg("N");

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                entity.setCreby(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null){
                return service.getColHighChartData(entity);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {

        }

        return null;
    }

    @Override
    public BasicDBObject getForm52HightPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: getForm52HightPieChartData");

        try{
            IForm52Service service = new Form52ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form52Entity entity = new Form52Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try{
                    if (!pairs.getValue().toString().isEmpty()){
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.MALNER)) {
                            entity.setMalNer(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.AIMAG)) {
                            entity.setAimag(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SUM)) {
                            entity.setSum(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.UVCHINNER)) {
                            entity.setUvchinNer(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.BUGDUVCHILSEN)) {
                            try {
                                entity.setBugdUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBugdUvchilsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.BUGDUHSEN)) {
                            try {
                                entity.setBugdUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBugdUhsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.BUGDEDGERSEN)) {
                            try {
                                entity.setBugdEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBugdEdgersen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.TEMEEUVCHILSEN)) {
                            try {
                                entity.setTemeeUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setTemeeUvchilsen(Long.valueOf("0"));
                            }
                        }
                        else if (pairs.getKey().equals(entity.TEMEEUHSEN)) {
                            try {
                                entity.setTemeeUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setTemeeUhsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.TEMEEEDGERSEN)) {
                            try {
                                entity.setTemeeEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setTemeeEdgersen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.ADUUUVCHILSEN)) {
                            try {
                                entity.setAduuUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setAduuUvchilsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.ADUUUHSEN)) {
                            try {
                                entity.setAduuUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setAduuUhsen(Long.valueOf("0"));
                            }

                        }
                        else if (pairs.getKey().equals(entity.ADUUEDGERSEN)) {
                            try {
                                entity.setAduuEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setAduuEdgersen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.UHERUVCHILSEN)) {
                            try {
                                entity.setUherUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setUherUvchilsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.UHERUHSEN)) {
                            try {
                                entity.setUherUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setUherUhsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.UHEREDGERSEN)) {
                            try {
                                entity.setUherEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setUherEdgersen(Long.valueOf("0"));
                            }

                        }
                        else if (pairs.getKey().equals(entity.HONIUVCHILSEN)) {
                            try {
                                entity.setHoniUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setHoniUvchilsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.HONIUHSEN)) {
                            try {
                                entity.setHoniUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setHoniUhsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.HONIEDGERSEN)) {
                            try {
                                entity.setHoniEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setHoniEdgersen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.YAMAAUVCHILSEN)) {
                            try {
                                entity.setYamaaUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setYamaaUvchilsen(Long.valueOf("0"));
                            }

                        }
                        else if (pairs.getKey().equals(entity.YAMAAUHSEN)) {
                            try {
                                entity.setYamaaUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setYamaaUhsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.YAMAAEDGERSEN)) {
                            try {
                                entity.setYamaaEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setYamaaEdgersen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.BUSADUVCHILSEN)) {
                            try {
                                entity.setBusadUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBusadUvchilsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.BUSADUHSEN)) {
                            try {
                                entity.setBusadUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBusadUhsen(Long.valueOf("0"));
                            }

                        }
                        else if (pairs.getKey().equals(entity.BUSADEDGERSEN)) {
                            try {
                                entity.setBusadEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBusadEdgersen(Long.valueOf("0"));
                            }

                        }
                        else  if (pairs.getKey().equals(entity.RECORD_DATE)){
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        }
                        else  if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)){
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            entity.setDelflg("N");
            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                entity.setCreby(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null){
                return service.getPieHighChartData(entity);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {

        }

        return null;
    }

    //form52-chart

    //form53-chart

    @Override
    public BasicDBObject getForm53HightChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 53 havsralt hight chart data");

        try{
            IForm53Service service = new Form53ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form53Entity entity = new Form53Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try{
                    if (!pairs.getValue().toString().isEmpty()){
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        }
                        else if (pairs.getKey().equals(entity.AIMAG)) {
                            entity.setAimag(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SUM)) {
                            entity.setSum(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.UVCHINNER)) {
                            entity.setUvchinNer(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.MALNER)) {
                            entity.setMalNer(String.valueOf(pairs.getValue()));
                        }  else if (pairs.getKey().equals(entity.BUGDUVCHILSEN)) {
                            try {
                                entity.setBugdUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBugdUvchilsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.BUGDUHSEN)) {
                            try {
                                entity.setBugdUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBugdUhsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.BUGDEDGERSEN)) {
                            try {
                                entity.setBugdEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBugdEdgersen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.BOTGOUVCHILSEN)) {
                            try {
                                entity.setBotgoUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBotgoUvchilsen(Long.valueOf("0"));
                            }
                        }
                        else if (pairs.getKey().equals(entity.BOTGOUHSEN)) {
                            try {
                                entity.setBotgoUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBotgoUhsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.BOTGOEDGERSEN)) {
                            try {
                                entity.setBotgoEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBotgoEdgersen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.UNAGAUVCHILSEN)) {
                            try {
                                entity.setUnagaUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setUnagaUvchilsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.UNAGAUHSEN)) {
                            try {
                                entity.setUnagaUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setUnagaUhsen(Long.valueOf("0"));
                            }

                        }
                        else if (pairs.getKey().equals(entity.UNAGAEDGERSEN)) {
                            try {
                                entity.setUnagaEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setUnagaEdgersen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.TUGALUVCHILSEN)) {
                            try {
                                entity.setTugalUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setTugalUvchilsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.TUGALUHSEN)) {
                            try {
                                entity.setTugalUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setTugalUhsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.TUGALEDGERSEN)) {
                            try {
                                entity.setTugalEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setTugalEdgersen(Long.valueOf("0"));
                            }

                        }
                        else if (pairs.getKey().equals(entity.HURGAUVCHILSEN)) {
                            try {
                                entity.setHurgaUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setHurgaUvchilsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.HURGAUHSEN)) {
                            try {
                                entity.setHurgaUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setHurgaUhsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.HURGAEDGERSEN)) {
                            try {
                                entity.setHurgaEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setHurgaEdgersen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.ISHIGUVCHILSEN)) {
                            try {
                                entity.setIshigUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setIshigUvchilsen(Long.valueOf("0"));
                            }

                        }
                        else if (pairs.getKey().equals(entity.ISHIGUHSEN)) {
                            try {
                                entity.setIshigUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setIshigUhsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.ISHIGEDGERSEN)) {
                            try {
                                entity.setIshigEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setIshigEdgersen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.BUSADUVCHILSEN)) {
                            try {
                                entity.setBusadUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBusadUvchilsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.BUSADUHSEN)) {
                            try {
                                entity.setBusadUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBusadUhsen(Long.valueOf("0"));
                            }

                        }
                        else if (pairs.getKey().equals(entity.BUSADEDGERSEN)) {
                            try {
                                entity.setBusadEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBusadEdgersen(Long.valueOf("0"));
                            }

                        }
                        else  if (pairs.getKey().equals(entity.RECORD_DATE)){
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        }
                        else  if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)){
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            entity.setDelflg("N");

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                entity.setCreby(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null){
                return service.getColHighChartData(entity);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {

        }

        return null;
    }

    @Override
    public BasicDBObject getForm53HightPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: getForm53HightPieChartData");

        try{
            IForm53Service service = new Form53ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form53Entity entity = new Form53Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try{
                    if (!pairs.getValue().toString().isEmpty()){
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        } else if (pairs.getKey().equals(entity.MALNER)) {
                            entity.setMalNer(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.AIMAG)) {
                            entity.setAimag(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SUM)) {
                            entity.setSum(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.UVCHINNER)) {
                            entity.setUvchinNer(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.BUGDUVCHILSEN)) {
                            try {
                                entity.setBugdUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBugdUvchilsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.BUGDUHSEN)) {
                            try {
                                entity.setBugdUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBugdUhsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.BUGDEDGERSEN)) {
                            try {
                                entity.setBugdEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBugdEdgersen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.BOTGOUVCHILSEN)) {
                            try {
                                entity.setBotgoUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBotgoUvchilsen(Long.valueOf("0"));
                            }
                        }
                        else if (pairs.getKey().equals(entity.BOTGOUHSEN)) {
                            try {
                                entity.setBotgoUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBotgoUhsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.BOTGOEDGERSEN)) {
                            try {
                                entity.setBotgoEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBotgoEdgersen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.UNAGAUVCHILSEN)) {
                            try {
                                entity.setUnagaUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setUnagaUvchilsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.UNAGAUHSEN)) {
                            try {
                                entity.setUnagaUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setUnagaUhsen(Long.valueOf("0"));
                            }

                        }
                        else if (pairs.getKey().equals(entity.UNAGAEDGERSEN)) {
                            try {
                                entity.setUnagaEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setUnagaEdgersen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.TUGALUVCHILSEN)) {
                            try {
                                entity.setTugalUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setTugalUvchilsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.TUGALUHSEN)) {
                            try {
                                entity.setTugalUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setTugalUhsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.TUGALEDGERSEN)) {
                            try {
                                entity.setTugalEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setTugalEdgersen(Long.valueOf("0"));
                            }

                        }
                        else if (pairs.getKey().equals(entity.HURGAUVCHILSEN)) {
                            try {
                                entity.setHurgaUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setHurgaUvchilsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.HURGAUHSEN)) {
                            try {
                                entity.setHurgaUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setHurgaUhsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.HURGAEDGERSEN)) {
                            try {
                                entity.setHurgaEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setHurgaEdgersen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.ISHIGUVCHILSEN)) {
                            try {
                                entity.setIshigUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setIshigUvchilsen(Long.valueOf("0"));
                            }

                        }
                        else if (pairs.getKey().equals(entity.ISHIGUHSEN)) {
                            try {
                                entity.setIshigUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setIshigUhsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.ISHIGEDGERSEN)) {
                            try {
                                entity.setIshigEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setIshigEdgersen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.BUSADUVCHILSEN)) {
                            try {
                                entity.setBusadUvchilsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBusadUvchilsen(Long.valueOf("0"));
                            }

                        } else if (pairs.getKey().equals(entity.BUSADUHSEN)) {
                            try {
                                entity.setBusadUhsen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBusadUhsen(Long.valueOf("0"));
                            }

                        }
                        else if (pairs.getKey().equals(entity.BUSADEDGERSEN)) {
                            try {
                                entity.setBusadEdgersen(Long.valueOf(pairs.getValue().toString()));
                            } catch (NumberFormatException e) {
                                entity.setBusadEdgersen(Long.valueOf("0"));
                            }

                        }
                        else  if (pairs.getKey().equals(entity.RECORD_DATE)){
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        }
                        else  if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)){
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            entity.setDelflg("N");
            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                entity.setCreby(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null){
                return service.getPieHighChartData(entity);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {

        }

        return null;
    }

    //form53-chart

    //    form61-chart

    @Override
    public BasicDBObject getForm61HightChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 61 hight chart data");

        try{
            IForm61Service service = new Form61ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form61Entity entity = new Form61Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try{
                    if (!pairs.getValue().toString().isEmpty()){
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        }else if (pairs.getKey().equals(entity.AIMAG)) {
                            entity.setAimag(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SUM)) {
                            entity.setSum(entity.SUM);
                        } else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            entity.setDelflg("N");

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                entity.setCreby(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null){
                return service.getColHighChartData(entity);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {

        }

        return null;
    }


    @Override
    public BasicDBObject getForm61HightPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: getForm61aHightPieChartData");

        try {
            IForm61Service service = new Form61ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form61Entity entity = new Form61Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (!pairs.getValue().toString().isEmpty()) {
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        }else if (pairs.getKey().equals(entity.AIMAG)) {
                            entity.setAimag(String.valueOf(pairs.getValue()));
                        } if (pairs.getKey().equals(entity.SUM)) {
                            entity.setSum(entity.SUM);
                        }  else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            entity.setDelflg("N");
            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                entity.setCreby(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                return service.getPieHighChartData(entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }


//    form61-chart


    //    form62-chart

    @Override
    public BasicDBObject getForm62HightChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: get form 62 hight chart data");

        try{
            IForm62Service service = new Form62ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form62Entity entity = new Form62Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try{
                    if (!pairs.getValue().toString().isEmpty()){
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        }else if (pairs.getKey().equals(entity.AIMAG)) {
                            entity.setAimag(String.valueOf(pairs.getValue()));
                        } else if (pairs.getKey().equals(entity.SUM)) {
                            entity.setSum(entity.SUM);
                        } else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            entity.setDelflg("N");

            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")){
                entity.setCreby(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null){
                return service.getColHighChartData(entity);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {

        }

        return null;
    }


    @Override
    public BasicDBObject getForm62HightPieChartData(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception {
        System.out.println("Hospital Service Implament ::: getForm62HightPieChartData");

        try {
            IForm62Service service = new Form62ServiceImpl(super.conn);

            User user = sessionUser.get(sessionId);
            Form62Entity entity = new Form62Entity();

            Iterator it = criteria.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                try {
                    if (!pairs.getValue().toString().isEmpty()) {
                        if (pairs.getKey().equals(entity.ID)) {
                            if (pairs.getValue() != null && !pairs.getValue().toString().isEmpty())
                                entity.setId(Long.parseLong(String.valueOf(pairs.getValue())));
                        }else if (pairs.getKey().equals(entity.AIMAG)) {
                            entity.setAimag(String.valueOf(pairs.getValue()));
                        } if (pairs.getKey().equals(entity.SUM)) {
                            entity.setSum(entity.SUM);
                        }  else if (pairs.getKey().equals(entity.RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setRecordDate(sqlDate);
                        } else if (pairs.getKey().equals(entity.SEARCH_RECORD_DATE)) {
                            String dv = String.valueOf(pairs.getValue());
                            if (String.valueOf(pairs.getValue()).length() <= 10) dv += " 00:00:00";
                            java.sql.Date sqlDate = new java.sql.Date(dateFormat.parse(dv).getTime());
                            entity.setSearchRecordDate(sqlDate);
                        }
                    }
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }//end while

            Date utilDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            if (entity.getRecordDate() == null) entity.setRecordDate(sqlDate);
            if (entity.getSearchRecordDate() == null) entity.setSearchRecordDate(sqlDate);

            entity.setDelflg("N");
            if (user.getRoleCode() != null && !user.getRoleCode().equalsIgnoreCase("SUPERADMIN")
                    && !user.getRoleCode().equalsIgnoreCase("ADMIN")) {
                entity.setCreby(user.getUsername());
            }


            if (entity != null && entity.getRecordDate() != null && entity.getSearchRecordDate() != null) {
                return service.getPieHighChartData(entity);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

        return null;
    }


//    form62-chart


    @Override
    public BasicDBObject getStatistic(LinkedTreeMap criteria, String sessionId, String menuid) throws Exception{

        BasicDBObject basicDBObject = new BasicDBObject();

        try{
            IUserService userService = new UserServiceImpl(super.conn);
            IUploadFileService fileService = new UploadFileServiceImpl(super.conn);
            INewsService newsService = new NewsServiceImpl(super.conn);

            basicDBObject.put("usercount", String.valueOf(userService.getUsersCount()));
            basicDBObject.put("filecount", String.valueOf(fileService.getFileCount()));
            basicDBObject.put("newscount", String.valueOf(newsService.getNewsCount()));
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return basicDBObject;
    }


    private int stringToInteger(String val) throws NumberFormatException {

        if (val != null && !val.isEmpty()) {
            if (val.contains(".")) {
                String buhel = val.substring(0, val.indexOf("."));

                return Integer.parseInt(buhel);
            } else {
                return Integer.parseInt(val);
            }
        }

        return 0;
    }
}