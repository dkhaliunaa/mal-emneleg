package com.hospital;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JRDesignStyle;

/**
 * Created by 24be10264 on 2/6/2017.
 */
public class JasperExport {

    static class Item{
        private String name;
        private Double price;


        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Double getPrice() {
            return price;
        }

        public void setPrice(Double price) {
            this.price = price;
        }
    }

    public static void main(String args[]){
        export_barimt();
    }


    public static void export_barimt(){
        try{
            String outputFile = "D://JasperTableExample.pdf";

            Map parameters = new HashMap();

            JasperPrint jasperPrint = JasperFillManager.fillReport("D:\\Work\\All\\hospsys\\insurance\\WorkingDirectory\\data\\jrxml/hform08.jasper",
                    parameters,
                    new JREmptyDataSource());

            JRStyle jrStyle = new JRDesignStyle();
            jrStyle.setFontName("DejaVu Serif");
            jrStyle.setPdfEncoding("Identity-H");
            jrStyle.setPdfEmbedded(true);
            jasperPrint.setDefaultStyle(jrStyle);

            /* outputStream to create PDF */
            OutputStream outputStream = new FileOutputStream(new File(outputFile));
            /* Write content to PDF file */
            JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);

            System.out.println("File Generated");
        }catch (JRException ex){
            ex.printStackTrace();
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public static void export_cont(){
        try {
            /* Output file location */
            String outputFile = "D://JasperTableExample.pdf";

            /* List to hold Items */
            List<Item> listItems = new ArrayList<Item>();

            /* Create Items */
            Item iPhone = new Item();
            iPhone.setName("iPhone 6S");
            iPhone.setPrice(65000.00);

            Item iPad = new Item();
            iPad.setName("iPad Pro");
            iPad.setPrice(70000.00);

            /* Add Items to List */
            listItems.add(iPhone);
            listItems.add(iPad);

            /* Convert List to JRBeanCollectionDataSource */
            JRBeanCollectionDataSource itemsJRBean = new JRBeanCollectionDataSource(listItems);

            /* Map to hold Jasper report Parameters */
            /*Map<String, Object> parameters = new HashMap<String, Object>();
            parameters.put("ItemDataSource", itemsJRBean);
            parameters.put("Parameter1", "jkjkjkasjdfk");*/

            java.util.Date currDate = new java.util.Date();

            Map parameters = new HashMap();
            parameters.put("InsOrgName", "Мандал даатгал ХХК");
            parameters.put("InsGuarantee", "Банкны бүтээгдэхүүнтэй холбоотой даатгал");
            parameters.put("InsContId", "17210600143");
            parameters.put("BranchId", "881");
            parameters.put("UserDomain", "24be10264");
            parameters.put("PrintDate", new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS").format(currDate));


        /* Даатгуулагчтай холбоотой мэдээлэл байна*/
            parameters.put("LastName", "Мөнхбат");
            parameters.put("FirstName", "Анхцэцэг");
            parameters.put("RegId", "РЭ92092707");
            parameters.put("InsuredType", "Голомт банкны харилцагч");
            parameters.put("AccountNo", "111111111111111");
            parameters.put("Citizenship", "Монгол");
            parameters.put("PhoneNumber", "12345678");
            parameters.put("Email", "tilyeujan@golomtbank.com");
            parameters.put("Address", "УБ, БЗД, 25-р хороо, Нарны зам гудамж, 123-17 menin atim bar goi");

            /*try {
                JasperFillManager.fillReportToFile(
                        "D://Projects/pos/insurance/ins_app/src/reports/test_report.jasper", parameters, new JREmptyDataSource());
            } catch (JRException e) {
                e.printStackTrace();
            }*/

            /*D://Projects/pos/insurance/ins_app/src/reports/test_report.jasper*/

            /* Using compiled version(.jasper) of Jasper report to generate PDF */
            JasperPrint jasperPrint = JasperFillManager.fillReport("D://Tiku/insurance/jrxml/test_parameter.jasper",
                    parameters,
                    new JREmptyDataSource());

            JRStyle jrStyle = new JRDesignStyle();
            //jrStyle.setFontName("C:\\Users\\24be10264\\Downloads\\dejavu-serif\\DejaVuSerif.ttf");
            //jrStyle.setPdfFontName("C:\\Users\\24be10264\\Downloads\\dejavu-serif\\DejaVuSerif.ttf");
            jrStyle.setFontName("DejaVu Serif");
            jrStyle.setPdfEncoding("Identity-H");
            jrStyle.setPdfEmbedded(true);
            jasperPrint.setDefaultStyle(jrStyle);

            /* outputStream to create PDF */
            OutputStream outputStream = new FileOutputStream(new File(outputFile));


            //JRProperties.setProperty("net.sf.jasperreports.default.pdf.font.name", "C:\\Gujrati-Saral-1.ttf");
            /* Write content to PDF file */
            JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);

            //ns.print();

            System.out.println("File Generated");
        } catch (JRException ex) {
            ex.printStackTrace();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
    }
}