package com.db;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import java.util.List;

/**
 * Өгөгдлийн сангийн интерфэйс
 * 
 * @author ub
 * 
 */
public interface DataService {

	String saveToSql(String collection, BasicDBObject query);

	String excelExport(String collection, BasicDBObject query, List<String> projection, BasicDBObject labels, List<String> sort);

    /**
     * Серверийн одоогийн Он сар өдөр буцаана
     * @return
     */
    public String getDate();

    /**
     * Серверийн одоогийн он сар өдөр цаг минут секунд буцаана
     * @return
     */
    public String getDateTime();
	
}
