package com.db;

import com.Config;

import com.jolbox.bonecp.BoneCP;
import com.jolbox.bonecp.BoneCPConfig;

import java.util.Properties;

/**
 * Mongo өгөгдлийн сантай харьцах класс
 *
 * @author ub
 */
public class CoreDb {
    static BoneCP conn = null;

    public static BoneCP get() {
        if (conn == null) {
            try {
                Properties props = Config.get("core.properties");

                System.out.println("DB: DataBase Server = " + props.getProperty("mysql.url"));
                String url = props.getProperty("mysql.url");
                Class.forName("com.mysql.jdbc.Driver");

                BoneCPConfig config = new BoneCPConfig();
                config.setJdbcUrl(props.getProperty("mysql.url"));
                config.setUsername(props.getProperty("mysql.user"));
                config.setPassword(props.getProperty("mysql.password"));

                conn = new BoneCP(config);
            } catch (Exception e) {
                System.err.println("Bad Database host: " + e);
            }
        }

        return conn;
    }

}

