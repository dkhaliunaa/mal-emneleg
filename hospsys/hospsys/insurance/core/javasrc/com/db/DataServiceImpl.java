package com.db;

import com.Config;
import com.auth.HospService;
import com.rpc.RpcHandler;
import com.mongodb.BasicDBObject;

import java.io.*;
import com.utils.DateUtil;
import com.utils.ExcelWriter;

import java.util.*;

/**
 * Db & Collections
 *
 * @author ub
 */
public class DataServiceImpl extends HospService implements DataService {

    public DataServiceImpl() {
        // FIXME: өөр шийдэл олох
        handler = new RpcHandler<>(this, DataService.class);


    }

    public String saveToSql(String collection, BasicDBObject query) {
        String result = "";
        try{
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

    }

    @Override
    public String excelExport(String collection, BasicDBObject query, List<String> projection, BasicDBObject labels, List<String> sorting) {

        BasicDBObject proj = new BasicDBObject();
        BasicDBObject sort = new BasicDBObject();

        for (String p : projection) {
            proj.put(p, 1);
        }

        for (String s : sorting) {
            if (s.startsWith("-")) {
                sort.put(s.substring(1), -1);
            } else {
                sort.put(s, 1);
            }
        }

        String fileName = collection + ".xlsx";
        String fname = "";

        String TMP_PATH = Config.getTmpPath();

        try {
            File f = new File(TMP_PATH, fileName);
            //CSVWriter writer = new CSVWriter();
            ExcelWriter writer = new ExcelWriter();

            if (f.exists()) {
                // ийм нэртэй файл байгаа учраас, файлын нэрийг солих
                fname = renameFile(fileName);
            } else {
                fname = fileName;
            }

            //writer.writeXLSXFile(records, projection, labels, TMP_PATH + "/" + fname, collection);

        } catch (Exception ex) {
            ex.printStackTrace();
            return ex.getMessage();
        }

        return fname;
    }

    String renameFile(String filePath) {
        int i = filePath.length() - 1;

        // өргөтгөлийг алгасах
        String ext = "";
        while (i > 0 && filePath.charAt(i) != '.') {
            ext = filePath.charAt(i) + ext;
            i--;
        }

        // давхардсан файлуудыг dup гэж нэрлэх
        return "dup_" + String.valueOf(System.nanoTime()) + "." + ext;

    }

    @Override
    public String getDate() {
        return DateUtil.yyyy_MM_dd.format(new Date());
    }

    @Override
    public String getDateTime() {
        return DateUtil.yyyyMMdd_HHMMSS.format(new Date());
    }

}