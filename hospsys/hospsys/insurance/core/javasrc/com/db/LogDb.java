package com.db;

import com.Config;
import com.mongodb.*;

import java.util.Properties;

/**
 * Log мэдээлэл хадгалагдах өгөгдлийн сантай харьцах класс
 * 
 */
public class LogDb {
    static MongoClient mongo = null;
    static DB db;
    
    public static DB get(String dbName) {
        if (mongo == null) {
            
            try {
                Properties props = Config.get("core.properties");

                System.out.println("DB: LogDb Server = " + props.getProperty("LOGDB_HOST"));

                // холболт үүсгэх
                MongoClientOptions opts = new MongoClientOptions.Builder()
                        .cursorFinalizerEnabled(true).connectionsPerHost(100)
                        .threadsAllowedToBlockForConnectionMultiplier(10)
                        .socketKeepAlive(true).build();

                mongo = new MongoClient(new ServerAddress(props.getProperty("LOGDB_HOST"),
                        Integer.parseInt(props.getProperty("LOGDB_PORT"))), opts);
            } catch (Exception e) {
                System.err.println("Bad LogDb host: " + e);
            }
        }

        return mongo.getDB(dbName);
    }



    // үндсэн баазыг авах
    public static DB getDb() {
        if (db == null) {
            db = LogDb.get("logdb");

            // authenticate if needed
            try {
                Properties props = Config.get("core.properties");

                if (!db.authenticate(props.getProperty("LOGDB_USER"),
                        props.getProperty("LOGDB_PASSWORD").toCharArray())) {

                    System.out.println("Database username or password is invalid! ");
                }
            } catch (Exception ex) {
                System.err.println(ex);
            }
        }

        return db;
    }

    public static DBCollection C(String c) {
        return getDb().getCollection(c);
    }

    public static void close() {
        if (mongo != null) {
            mongo.close();
        }
    }





}
