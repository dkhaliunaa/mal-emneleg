package com.auth;

import com.db.CoreDb;
import com.Config;
import com.utils.CharsetConverter;
import com.jolbox.bonecp.BoneCP;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.log4j.Logger;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import java.util.Hashtable;
import java.util.Properties;

/**
 * Windows Domain сервертэй ажиллах туслах класс
 * 
 * @author ub
 * 
 */
public class Ldap {
	static final Logger logger = Logger.getLogger(Ldap.class.getName());
	// LDAP server
	public static String LDAP_HOST;
	public static int LDAP_PORT;
	public static int LDAP_SSL_PORT;
	public static String LDAP_DOMAIN;
	public static String LDAP_DC1;
	public static String LDAP_DC2;
	public static String LDAP_DN;
	public static String LDAP_ADMIN;
	public static String LDAP_PASS;
	static String ATTRIBUTE_FOR_USER = "sAMAccountName";

	static BoneCP conn;

	static {
		conn = CoreDb.get();

        Properties props = null;
        try {
            props = Config.get("core.properties");

            LDAP_HOST = props.getProperty("LDAP_HOST").toString();
            LDAP_PORT = Integer.parseInt(props.getProperty("LDAP_PORT").toString());
            LDAP_SSL_PORT = Integer.parseInt(props.getProperty("LDAP_SSL_PORT").toString());
            LDAP_DOMAIN = props.getProperty("LDAP_DOMAIN").toString();
            LDAP_DC1 = props.getProperty("LDAP_DC1").toString();
            LDAP_DC2 = props.getProperty("LDAP_DC2").toString();
            LDAP_DN = props.getProperty("LDAP_DN").toString();
            LDAP_ADMIN = props.getProperty("LDAP_ADMIN").toString();
            LDAP_PASS = props.getProperty("LDAP_PASS").toString();

        } catch (Exception e) {
            e.printStackTrace();
        }



	}

	/**
	 * LDAP user login
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public static Attributes authenticateUser(String username, String password) {
		// String returnedAtts[] = { "sn", "givenName", "mail", "fullName" };
		String searchFilter = "(&(objectClass=user)(" + ATTRIBUTE_FOR_USER
				+ "=" + username + "))";

		// Create the search controls
		SearchControls searchCtls = new SearchControls();
		searchCtls.setReturningAttributes(null);

		// Specify the search scope
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		String searchBase = LDAP_DN;
		Hashtable<String, String> ctx = new Hashtable<>();
		ctx.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");

		ctx.put(Context.PROVIDER_URL, "ldap://" + LDAP_HOST + ":"
				+ LDAP_PORT);
		ctx.put(Context.SECURITY_AUTHENTICATION, "simple");

		ctx.put(Context.SECURITY_PRINCIPAL, username + "@" + LDAP_DOMAIN);
		ctx.put(Context.SECURITY_CREDENTIALS, password);
		LdapContext ctxGC = null;
		try {
			ctxGC = new InitialLdapContext(ctx, null);
			// Search for objects in the GC using the filter
			NamingEnumeration<SearchResult> answer = ctxGC.search(searchBase,
					searchFilter, searchCtls);
			while (answer.hasMoreElements()) {
				SearchResult sr = answer.next();
				Attributes attrs = sr.getAttributes();
				if (attrs != null) {
					return attrs;
				}
			}

		} catch (Exception e) {
			logger.debug(e.getMessage());
		}
		return null;
	}

	public static String getOldCn(String domain) {
        
		Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, "ldap://" + LDAP_HOST + ":" + LDAP_PORT + "/DC="+LDAP_DC1+",DC="+LDAP_DC2);
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL,  LDAP_ADMIN + "@" + LDAP_DOMAIN);
        env.put(Context.SECURITY_CREDENTIALS, LDAP_PASS);
        
        String cn = "";
		NamingEnumeration results = null;
        try {
                
            // Create the initial context
            DirContext ctx = new InitialDirContext(env);

//            LdapContext user = (LdapContext)ctx.lookup("CN=MTG - PHH - Turbold Dugarsuren");
//            
//            System.out.println(user);
            
            String searchFilter = "(&(objectClass=user)(sAMAccountName="+domain+"))";
            
            SearchControls controls = new SearchControls();
            controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            controls.setCountLimit(300000);
            results = ctx.search("", searchFilter, controls);
            while (results.hasMoreElements()) {
                SearchResult searchResult = (SearchResult) results.next();
                Attributes attributes = searchResult.getAttributes();
                cn = attributes.get("DistinguishedName").toString();
                cn = cn.replace("distinguishedName: ", "");
            }

            // close 
            ctx.close();

        } catch (NamingException e) {
            e.printStackTrace();
        }
        
        return cn;
        		
	}
	
	
	private static Object firstNonNull(Object o1, Object o2){
		if (o1 != null) return o1;
	    if (o2 != null) return o2;
	    return "";
	    //throw new ArgumentError('All arguments were null');
	}
	
	/**
	 * 
	 * mn to en
	 * 
	 * @param str
	 * @return cn
	 */
	private static String mnToEn(String str){
		String str2 = WordUtils.capitalize(CharsetConverter.utf8_latin(str));
		return str2;
	}

	public static void main(String[] args) throws Exception {

	}
}