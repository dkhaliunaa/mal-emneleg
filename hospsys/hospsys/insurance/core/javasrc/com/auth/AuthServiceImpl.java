package com.auth;

import com.model.Session;
import com.model.User;
import com.rpc.RpcHandler;
import org.bson.types.ObjectId;
import org.omg.CORBA.SystemException;

import java.sql.*;
import java.util.Date;
import java.util.regex.Pattern;

public class AuthServiceImpl extends HospService implements AuthService {

    final Pattern extNum = Pattern.compile("\\d+$");

    public AuthServiceImpl() {
        // FIXME: өөр шийдэл олох
        handler = new RpcHandler<>(this, AuthService.class);
    }

    @Override
    public Session login(String username, String password, User.AuthType authType) {
        String query_login = "SELECT husers.`id`, `username`, `password`, `userrolecode`, `first_name`, `last_name`, " +
                "`full_name`, `middle_name`, `nick_name`, `gender`, `birth_day`, husers.`cre_time`, husers.`cre_by`, " +
                "husers.`mod_time`, husers.`mod_by`, `del_flg`, `registration`, husers.`citycode`, husers.`sumcode`, " +
                "h_city.cityname, h_city.cityname_en, h_sum.sumname, h_sum.sumname_en FROM `husers` INNER JOIN h_city " +
                "ON husers.citycode = h_city.code INNER JOIN h_sum ON husers.sumcode = h_sum.sumcode " +
                "WHERE username = ? AND password = ? AND del_flg = 'N'";
        User loginUser = null;

        try (Connection connection = conn.getConnection()) {
            try{
                PreparedStatement statement = connection.prepareStatement(query_login);

                statement.setString(1, username);
                statement.setString(2, password);
                System.out.println(statement);
                ResultSet resultSet = statement.executeQuery();

                while (resultSet.next()){
                    loginUser = new User();

                    loginUser.setUsername(resultSet.getString("username"));
                    loginUser.setPassword(resultSet.getString("password"));
                    loginUser.setRoleCode(resultSet.getString("userrolecode"));
                    loginUser.setFirstName(resultSet.getString("first_name"));
                    loginUser.setLastName(resultSet.getString("last_name"));
                    loginUser.setMiddleName(resultSet.getString("middle_name"));
                    loginUser.setNickName(resultSet.getString("nick_name"));
                    loginUser.setGender(resultSet.getString("gender"));
                    loginUser.setBirthDate(resultSet.getDate("birth_day"));
                    loginUser.setRegId(resultSet.getString("registration"));
                    loginUser.setCity(resultSet.getString("citycode"));
                    loginUser.setSum(resultSet.getString("sumcode"));
                    loginUser.setDelFlg(resultSet.getString("del_flg"));
                    loginUser.setCreAt(resultSet.getDate("cre_time"));
                    loginUser.setCreBy(resultSet.getString("cre_by"));
                    loginUser.setModAt(resultSet.getDate("mod_time"));

                    loginUser.setCityName(resultSet.getString("cityname"));
                    loginUser.setCityNameEn(resultSet.getString("cityname_en"));
                    loginUser.setSumName(resultSet.getString("sumname"));
                    loginUser.setSumNameEn(resultSet.getString("sumname_en"));

                    Date utilDate = new Date();
                    loginUser.setLoginDate(utilDate);

                    break;
                }

                if (statement != null)statement.close();
                if (resultSet != null) resultSet.close();

                return createSession(loginUser, remoteAddr);
            }catch (Exception ex){
                ex.printStackTrace();
            }
            finally {
                if (connection != null)connection.close();
            }
        } catch (SystemException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;

    }

    @Override
    public Session validate(String token, String user) {
        Session s = getSession(token);

        if (s != null && user.equals(s.getLogin())) {
            // remoteAddr.equals(s.getRemoteAddr())
            return s;
        }

        return null;
    }

    /**
     * Session үүсгэх
     *
     * @param user
     * @param ip
     * @return
     */
    Session createSession(User user, String ip) {
        String token = new ObjectId().toString();

        // session үүсгэх
        Session s = new Session(user.getUsername(), token, ip);
        s.setUpdated(new Date());
        s.setUsername(user.getLastName() + " " + user.getFirstName());
        s.setRoles(user.getRoles());
        s.setRoleCode(user.getRoleCode());

        saveSession(token, s);

        sessionUser.put(token, user);

        return s;
    }

    void saveSession(String token, Session ss) {
        // кээш рүү хадгалах
        sessionCache.put(token, ss);
    }

    public static Session getSession(String token) {
        Session s = null;
        if (sessionCache.containsKey(token)) {
            s = sessionCache.get(token);
            s.setUpdated(new Date());
        }
        return s;
    }

}
