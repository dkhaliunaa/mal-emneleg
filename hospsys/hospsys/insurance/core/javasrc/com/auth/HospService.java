package com.auth;

import com.db.CoreDb;
import com.model.Session;
import com.model.User;
import com.rpc.RpcService;
import com.jolbox.bonecp.BoneCP;

import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

/**
 * Basic Service
 *
 * @author ub
 */
public class HospService extends RpcService {
    public static final ConcurrentHashMap<String, Session> sessionCache = new ConcurrentHashMap<>();
    public static final ConcurrentHashMap<String, User> sessionUser = new ConcurrentHashMap<>();
    protected Logger logger;

    protected BoneCP conn;
    protected Session client;

    public HospService() {
        try {
            logger = Logger.getLogger(this.getServiceName());
            conn = CoreDb.get();
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    @Override
    protected boolean authenticate() {
        if (authType == User.AuthType.APP) {
            /*DBObject u = conn.getCollection("User").findOne(new BasicDBObject("token", token));

            if (u != null) {
                if (u.containsField("remoteAddr")) {
                    if (remoteAddr.equals(u.get("remoteAddr"))) {
                        client = new Session(u.get("_id").toString(), token, remoteAddr);
                        return true;
                    }
                } else {
                    client = new Session(u.get("_id").toString(), token, "");
                    return true;
                }
            }*/

        } else if (authType == User.AuthType.LDAP) {
            // LDAP хэрэглэгчийн хувьд login хийсэн байх ёстой
            if (sessionCache.containsKey(token)) {
                client = sessionCache.get(token);
                client.setUpdated(new Date());
                return true;
            }
        } else {
            // admin буюу local-аар ашиглаж байгаа үед
            client = new Session("admin", "admin", "localhost");
        }

        return false;
    }

    public Session getClient() {
        return client;
    }

    protected User getUserInfo(String sessionId) {
        return sessionUser.get(sessionId);
    }
}