package com.auth;

import com.model.Session;
import com.model.User;

/**
 * Authentication Service
 *
 * @author ub
 *
 */
public interface AuthService {
    /**
     * Серверт нэвтрэх
     *
     * @param user
     * @param password
     * @param authType
     * @return Connection Token
     */
    Session login(String user, String password, User.AuthType authType);

    /**
     * Token шалгах
     *
     * @param token
     * @param user
     * @return
     */
    Session validate(String token, String user);
}
