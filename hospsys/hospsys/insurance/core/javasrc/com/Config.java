package com;

import org.apache.log4j.PropertyConfigurator;
//import org.eclipse.jetty.server.Server;
//import org.eclipse.jetty.server.handler.HandlerList;

import java.io.FileReader;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Тохиргооны класс
 * 
 * @author ub
 *
 */
public class Config {
	// WokingDirectory хавтас
	public static String HOME = "WorkingDirectory";
	public static boolean DEBUG = false;

	static final ConcurrentHashMap<String, Properties> propsMap = new ConcurrentHashMap<>();

	static {
		HOME = System.getProperty("sys.home");
		DEBUG = Boolean.parseBoolean(System.getProperty("sys.debug", "false"));

		// log4j тохируулах
		PropertyConfigurator.configure(HOME + "/log4j.properties");

	}

	public static Properties get(String fileName) throws Exception {
		if (!propsMap.containsKey(fileName)) {

			try (FileReader reader = new FileReader(HOME + "/" + fileName))  {
				Properties properties = new Properties();
				properties.load(reader);

				propsMap.put(fileName, properties);
			}
		}

		return propsMap.get(fileName);
	}


	// түр файлууд үүсэх хавтас
	public static String getTmpPath() {
		return getPath("tmp");
	}

	public static String getFilePath() {
		return getPath("files");
	}
	public static String getReportPath() {
		return getPath("reports");
	}

    public static String getBillPath() {
        return getPath("bill");
    }

	// түр файлууд үүсэх хавтас
	public static String getContsPath() {
		return getPath("conts");
	}

	public static String getPath(String path) {
		return HOME + "/" + path;
	}

	/* Тайлан jasper файлууд байна */
	public static String getJrxmlPath() {
		return getPath("data/jrxml");
	}
	public static String getJasperPath() {
		return getPath("data/jasper");
	}

	static String serverName;
	public static String serverUrl() {
		try {
			Properties props = get("core.properties");

//			if (serverName == null) {
//				serverName = findLocalIP();
//			}

			// TODO: http ? https
			return "https://" + props.getProperty("SERVER_NAME"); // + ":" + props.getProperty("MAIN_PORT");
            //return "https://" + serverName;
		} catch (Exception ex) {
			System.err.println(ex);
		}

		return null;
	}

	public static String findLocalIP() {
		Enumeration nicList;
		NetworkInterface nic;
		Enumeration nicAddrList;
		InetAddress nicAddr;
		try {
			nicList = NetworkInterface.getNetworkInterfaces();
			while (nicList.hasMoreElements()) {
				nic = (NetworkInterface) nicList.nextElement();
				if (!nic.isLoopback() && nic.isUp()) {
					nicAddrList = nic.getInetAddresses();
					while (nicAddrList.hasMoreElements()) {
						nicAddr = (InetAddress) nicAddrList.nextElement();
						try {
							Inet4Address nicAddrIPv4 = (Inet4Address) nicAddr;
							return nicAddr.getHostAddress();
						} catch (Exception e) {
						}
					}
				}
			}
		} catch (SocketException e1) {
			System.err.println("SocketException handled in Networking.getIPAddress!.");
		}
		return "";
	}
}
