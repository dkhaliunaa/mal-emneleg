package com.rpc;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.junit.Test;

import java.io.StringReader;

public class JsonParseTest {

    @Test
    public void parseTest() {
        String s = "{jsonrpc:'2.0',id:'20125425011252',token:'50d9af6ae4b06528393fbd17-267371885',method:'trx.inter',params:[{'account1': '1165010603','account2':'851131001602000','currency1':'EUR','currency2':'USD','rate1':1801.98,'rate2':1433,'amount1':109.21,'amount2':150.0000,'TAG50':'','TAG59':'WESTEINDE 33 F 2512 GT DEN HAAG NETHERLANDS','TAG59A':'6391503','TAG57':'ING-BIJLMERDREEF 1091102 BW AMSTERDAM NETHERLANDS','TAG57A':'INGBNL2A   ','TAG56':'-','TAG72':'SALARY TO OWN ACCOUNT ID#00000000082','pay':'SHA','SWTXNPURP':'2114','SWCNTRCODE':'NED','SWVALUEDATE':'2012-08-29','desc':'salary to own account','sourceNo':'2','state':'4','clientId':'FD6382'}";
        JsonParser parser = new JsonParser();
        JsonElement el = parser.parse(new StringReader(s));
        System.out.println(el);
    }
}
