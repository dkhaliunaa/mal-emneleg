package com.rpc;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class StressTest {
    String token = "50d2d97ee4b00561006b9949";

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Ignore
    @Test
    public void login() {
        StringBuilder sb = new StringBuilder();
        sb.append("{ jsonrpc:'2.0'").append(',');
        sb.append("id:").append(System.nanoTime()).append(',');
        sb.append("method:'golomt.auth.login'").append(',');

        sb.append("params:['APP', '640', '588899']");
        sb.append("}");

        String data = sb.toString();

        try {
            String request = "http://localhost:8080/ghub/golomt.rpc";
            URL url = new URL(request);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("charset", "utf-8");
            connection.setRequestProperty("Content-Length",
                    "" + Integer.toString(data.getBytes().length));
            connection.setUseCaches(false);

            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream());
            wr.writeBytes(data);
            wr.flush();
            wr.close();

            String line;
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));

            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }

            connection.disconnect();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    void getAccount(String account) {
        StringBuilder sb = new StringBuilder();
        sb.append("{ jsonrpc:'2.0'").append(',');
        sb.append("id:").append(System.nanoTime()).append(',');
        sb.append("method:'account.get'").append(',');
        sb.append("token:'").append(token).append("',");
        sb.append("params:['").append(account).append("']");
        sb.append("}");

        String data = sb.toString();

        try {
            String request = "http://localhost:8080/ghub/golomt.rpc";
            URL url = new URL(request);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("charset", "utf-8");
            connection.setRequestProperty("Content-Length",
                    "" + Integer.toString(data.getBytes().length));
            connection.setUseCaches(false);

            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream());
            wr.writeBytes(data);
            wr.flush();
            wr.close();

            String line;
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));

            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }

            connection.disconnect();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void perfTest() throws Exception {
        int loops = 100;
        Thread[] tds = new Thread[loops];

        long start = System.currentTimeMillis();

        for (int i = 0; i < loops; i++) {
            tds[i] = new Thread(new Runnable() {

                @Override
                public void run() {
                    getAccount("2705003312");
                }
            });
            tds[i].start();
        }

        for (int i = 0; i < loops; i++) {
            try {
                if (tds[i] != null && tds[i].isAlive()) {
                    tds[i].join();
                }
            } catch (Exception ex) {
            }
        }

        long end = System.currentTimeMillis();
        long elapsed = end - start;

        System.out.println("loops / sec = " + (loops * 1000 / elapsed));
    }
}
