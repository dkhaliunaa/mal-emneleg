/*
 Copyright (c) 2015 The Polymer Project Authors. All rights reserved.
 This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 Code distributed by Google as part of the polymer project is also
 subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

(function (document) {
  'use strict';

  // өгөгдлийн сан
  var ds = new DataService();


  // Grab a reference to our auto-binding template
  // and give it some initial binding values
  // Learn more about auto-binding templates at http://goo.gl/Dx1u2g
  var app = document.querySelector('#app');

  //Parameters
  app.showMain      = false;
  app.showLogin     = false;
  app.swState         = '';
  app.onLine        = true;
  app.userdomain    = '';
  app.username      = '';
  app.userdept      = '';
  app.userrole      = '';
  app.userimage     = '';
  app.uid           = '';
  app.pwd           = '';
  app.statusMsg     = '';
  app.dateStr       = 'Он/Сар/Өдөр';
  app.bigrole       = false;
  app.approvalrole  = false;
  app.uaaarole      = false;
  app.selBranch     = '';
  app.selHZ         = '';
  app.selWorker     = '';
  app.selwrbranch   = '';
  app.tempBranch    = '';
  app.tempHZ        = '';
  app.tempWorker    = '';
  app.rate_tab      = 0;
  app.statistic_tab = 0;
  app.WorkersyncCount  = 0;
  app.HZsyncCount      = 0;
  app.BranchsyncCount  = 0;
  app.branches      = [];
  app.hzs           = [];
  app.workers       = [];

  app.periodsWorker = [];
  app.periodsHZ = [];
  app.periodsBranch = [];
  app.periodWorker = '';
  app.periodHZ = '';
  app.periodBranch = '';

  app.disableWorker = false;
  app.disableHZ = false;
  app.disableBranch = false;

  app.approveType = '';
  app.checklistApproval = false;
  app.approvedFlag = false;

  app.br = {
    suma : "Ажилтны гадаад төрх",
    a1 : "Дүрэмт хувцсыг иж бүрдлээр нь, цэвэр цэмцгэр өмссөн эсэх",
    a2 : "Дүрэмт хувцсыг ижилсүүлж өмссөн эсэх /зангиа, цамц гэх мэт/",
    a3 : "Нэр бүхий энгэрийн тэмдгийг харагдахуйцаар зөв зүүсэн /баруун талдаа/ эсэх",
    a4 : "Хар өнгийн гутал, хар болон биеийн өнгөтэй трико өмссөн эсэх",
    a5 : "Нүүр будалтыг стандартын дагуу будсан эсэх",
    a6 : "Үс засалт толигор байх, шууж боосон эсэх",
    a7 : "Гарын арьс арчилгаа, хумс засалт, будалт стандартын дагуу эсэх",
    a8 : "Зүүлт чимэглэл стандартын дагуу эсэх /цаг, ээмэг, бугуйвч гэх мэт/",
    sumb : "Харилцагчид үйлчилгээ үзүүлж буй байдал",
    b1 : "Харилцагчийг нүдний харцаар тосч үйлчилж байгаа эсэх",
    b2 : "Харилцагчид талархан үдэж байгаа эсэх",
    b3 : "Харилцагчтай эелдэг, найрсгаар харилцаж байгаа эсэх",
    b4 : "Харилцагчтай ярилцах, үйлчилгээ үзүүлж байх үедээ анхааралтай сонсч байгаа эсэх",

    sumc : "Ажилтны ажлын байр",
    c1 : "Ажлын байр эмх цэгцтэй эсэх",
    c2 : "Үйлчилгээний зааланд хоол, хүнсний зүйл хэрэглэхгүй, бохь зажлахгүй байх",
    c3 : "Ажилтнууд үйлчилгээ үзүүлж байх үедээ гар утсаар ярихгүй, хов, хувийн яриа ярих, инээлдэхгүй байх",

    sumd : "Салбарын тохижилт, тав тухтай байдал",
    d1 : "Ханшийн самбар (TV) хэвийн ажиллаж байгаа эсэх",
    d2 : "АТМ, АТМ-ны эргэн тойрны цэвэр байдал",
    d3 : "Дугаар олгогч төхөөрөмж хэвийн ажиллаж байгаа эсэх",
    d4 : "Үйлчилгээний танхим хог цаас, тоос шороогүй, цэвэрлэгээ бүрэн хийгдсэн эсэх",
    d5 : "Ширээ, сандал, хаалга, хана, шилэн хаалт, барьер тоосгүй, өнгө үзэмжтэй эсэх",
    d6 : "Сүлжээний болон цахилгааны утсуудын багцалж, далдалсан эсэх",
    d7 : "Гэрлийн чийдэн бүрэн бүтэн, цэвэр, гэрэлтүүлэг сайтай эсэх",
    d8 : "Тавилга, эд хогшлын бүрэн бүтэн байдал",
    d9 : "Тавилга, эд хогшил тохиромжтой газар байршсан эсэх",
    d10 : "Наалт, хуулга, хаягжилт хуучраагүй, өнгө үзэмжтэй эсэх",
    d11 : "Харилцагчийн мэдээллийн булан бүрэн мэдээлэлтэй, өнгө үзэмжтэй эсэх",
    d12 : "Брошурын тавиурт брошурууд эмх цэгцтэй, ойлгомжтой өрөгдсөн эсэх /төрөлжүүлсэн, өндөр нам нь тохиромжтой гэх мэт/",
    d13 : "Саналын хайрцгийн санал бичих хуудас, бал байгаа эсэх",

    sume : "Хамгаалагчийн хувцаслалт, биеэ авч яваа байдал",
    e1 : "Банкны хамгаалагчийн хувцаслалтын цэвэр цэмцгэр байдал",
    e2 : "Банкны хамгаалагчийн биеэ авч яваа байдал",
  };

  app.hz = {
    suma : "Ажилтны гадаад төрх, биеэ авч яваа байдал",
    a1 : "Дүрэмт хувцсыг иж бүрдлээр нь, цэвэр цэмцгэр өмссөн эсэх",
    a2 : "Нэр бүхий энгэрийн тэмдгийг харагдахуйцаар зөв зүүсэн /баруун талдаа/ эсэх",
    a3 : "Хар өнгийн гутал, хар болон биеийн өнгөтэй трико өмссөн эсэх",
    a4 : "Нүүр будалтыг стандартын дагуу будсан эсэх",
    a5 : "Үс засалт толигор байх, шууж боосон эсэх",
    a6 : "Гарын арьс арчилгаа, хумс засалт, будалт стандартын дагуу эсэх",
    a7 : "Зүүлт чимэглэл стандартын дагуу эсэх /цаг, ээмэг, бугуйвч гэх мэт/",

    sumb : "Харилцагчтай харилцаж буй байдал",
    b1 : "Харилцагчийг нүдний харцаар тосч үйлчилж байгаа эсэх",
    b2 : "Харилцагчтай харилцаж байхдаа инээмсэглэж буй эсэх",
    b3 : "Харилцагчтай эелдэг, найрсгаар харилцаж байгаа эсэх",
    b4 : "Өөрийгөө танилцуулж буй эсэх",

    sumc : "Хандлага",
    c1 : "Нөхөрсөг байдал",
    c2 : "Эерэг байдал",
    c3 : "Өөртөө итгэлтэй байдал, эрч хүчтэйгээр ажиллаж байгаа эсэх",

    sumd : "Багийн ажиллагаа",
    d1 : "Багт нөлөөлөх чадвар",
    d2 : "Үйлчилгээний ажилтнуудад ур чадвар олгож, зөвлөгөө өгч чадаж байгаа байдал",
    d3 : "Хариуцлага хүлээх чадвар",

    sume : "Бүтээгдэхүүн борлуулах, танилцуулах чадвар",
    e1 : "Бүтээгдэхүүн, үйлчилгээний мэдлэг",
    e2 : "Бүтээгдэхүүн үйлчилгээний талаар зөвлөгөө өгч байгаа байдал",

    sumf : "Тайлан явуулсан байдал",
    f1 : "Цаг хугацаандаа явуулсан",
    f2 : "Үнэлсэн байдал /бодит бус үнэлсэн эсэх/",
    f3 : "Үүрэг даалгаврын биелэлт"
  };

  app.wr = {
    suma : "Ажилтны гадаад төрх",
    a1 : "Дүрэмт хувцсыг иж бүрдлээр нь, цэвэр цэмцгэр өмссөн эсэх",
    a2 : "Дүрэмт хувцсыг ижилсүүлж өмссөн эсэх /зангиа, цамц гэх мэт/",
    a3 : "Нэр бүхий энгэрийн тэмдгийг харагдахуйцаар зөв зүүсэн /баруун талдаа/ эсэх",
    a4 : "Хар өнгийн гутал, хар болон биеийн өнгөтэй трико өмссөн эсэх",
    a5 : "Нүүр будалт нь хөнгөн, зохимжтой эсэх",
    a6 : "Үс засалт толигор байх, шууж боосон эсэх",
    a7 : "Гарын арьс арчилгаа, хумс засалт, будалт стандартын дагуу эсэх",
    a8 : "Зүүлт чимэглэл стандартын дагуу эсэх /цаг, ээмэг, бугуйвч гэх мэт/",

    sumb : "Харилцагчид үйлчилгээ үзүүлж буй байдал",
    b1 : "Харилцагчийг нүдний харцаар тосч үйлчилж байгаа эсэх",
    b2 : "Харилцагчид талархан үдэж байгаа эсэх",
    b3 : "Харилцагчтай эелдэг, найрсгаар харилцаж байгаа эсэх",
    b4 : "Харилцагчтай ярилцах, үйлчилгээ үзүүлж байх үедээ анхааралтай сонсч байгаа эсэх",

    sumc : "Ажилтны ажлын байр",
    c1 : "Ажлын байр эмх цэгцтэй эсэх",
    c2 : "Үйлчилгээний зааланд хоол, хүнсний зүйл хэрэглэхгүй, бохь зажлахгүй байх",
    c3 : "Ажилтнууд үйлчилгээ үзүүлж байх үедээ гар утсаар ярихгүй, хов, хувийн яриа ярих, инээлдэхгүй байх",

    sumd : "Банкны хамгаалагч",
    d1 : "Банкны хамгаалагчийн хувцаслалтын цэвэр цэмцгэр байдал",
    d2 : "Банкны хамгаалагчийн биеэ авч яваа байдал"
  };

  app.displayInstalledToast = function () {
    // Check to make sure caching is actually enabled—it won't be in the dev environment.
    if (!document.querySelector('platinum-sw-cache').disabled) {
      document.querySelector('#caching-complete').show();
    }
  };

  app.displayToast = function (msg) {
    document.querySelector('#main-toast').text = msg;
    document.querySelector('#main-toast').show();
  };

  // Listen for template bound event to know when bindings
  // have resolved and content has been stamped to the page
  app.addEventListener('dom-change', function () {
    console.log('Our app is ready to rock!');
    //alert("dom-change");
    app.onLine = navigator.onLine;
    if(localStorage["user"]){
      var user = JSON.parse(localStorage["user"]);
      app.username = user.username;
      app.userdomain = user.domain;
      app.userdept = user.dept;
      app.userimage = user.image;
      app.showLogin = false;
      app.showMain = true;
      app.userrole = user.role;
      if (app.userrole == "hzchecklist.uchhm") {
        app.bigrole = true;
      } else if(app.userrole == "hzchecklist.hz"){
        app.disableHZ = true;
        app.disableBranch = true;
      } else if(app.userrole == "hzchecklist.approval"){
        app.approvalrole = true;
        app.disableWorker = true;
        app.disableHZ = true;
        app.disableBranch = true;
      } else if(app.userrole == "hzchecklist.uaaa"){
        app.uaaarole = true;
        app.disableWorker = true;
        app.disableHZ = true;
        app.disableBranch = true;
      }
    } else {
      app.showLogin = true;
      app.showMain = false;
      if(!app.onLine){
        app.displayToast('Та өмнө нь сүлжээтэй газар нэвтрэн орсон байх шаардлагатай.');
      }
    }

    app.day = 0;
    app.clearChecklist("worker");
    app.clearChecklist("hz");
    app.clearChecklist("branch");
    app.dateStr = moment().format('YYYY/MM/DD');
    app.getBranches();
    app.checkIndexedDB();
  });

  // See https://github.com/Polymer/polymer/issues/1381
  window.addEventListener('WebComponentsReady', function () {
    // imports are loaded and elements have been registered
  });

  // Main area's paper-scroll-header-panel custom condensing transformation of
  // the appName in the middle-container and the bottom title in the bottom-container.
  // The appName is moved to top and shrunk on condensing. The bottom sub title
  // is shrunk to nothing on condensing.
  window.addEventListener('paper-header-transform', function (e) {
    var appName = Polymer.dom(document).querySelector('#mainToolbar .app-name');
    var middleContainer = Polymer.dom(document).querySelector('#mainToolbar .middle-container');
    var bottomContainer = Polymer.dom(document).querySelector('#mainToolbar .bottom-container');

    var detail = e.detail;
    var heightDiff = detail.height - detail.condensedHeight;
    var yRatio = Math.min(1, detail.y / heightDiff);
    // appName max size when condensed. The smaller the number the smaller the condensed size.
    var maxMiddleScale = 0.50;
    var auxHeight = heightDiff - detail.y;
    var auxScale = heightDiff / (1 - maxMiddleScale);
    var scaleMiddle = Math.max(maxMiddleScale, auxHeight / auxScale + maxMiddleScale);
    var scaleBottom = 1 - yRatio;

    // Move/translate middleContainer
    Polymer.Base.transform('translate3d(0,' + yRatio * 100 + '%,0)', middleContainer);

    // Scale bottomContainer and bottom sub title to nothing and back
    Polymer.Base.transform('scale(' + scaleBottom + ') translateZ(0)', bottomContainer);

    // Scale middleContainer appName
    Polymer.Base.transform('scale(' + scaleMiddle + ') translateZ(0)', appName);
  });

  // Close drawer after menu item is selected if drawerPanel is narrow
  app.onDataRouteClick = function () {
    var drawerPanel = document.querySelector('#paperDrawerPanel');
    if (drawerPanel.narrow) {
      drawerPanel.closeDrawer();
    }
  };

  // Scroll page to top and expand header
  app.scrollPageToTop = function () {
    if(app.showMain){
      document.getElementById('mainContainer').scrollTop = 0;
    }
  };

  app.branchSelected = function(){
    console.log(app.selBranch.нэр);
    app.getBranchList(app.selBranch.нэр, app.dateStr);

    app.periodBranch = app.dateStr;
    app.fetchAvailableDatesForBranch();
  };

  app.hzSelected = function(){
    console.log(app.selHZ.домайн);
    app.getHZList(app.selHZ.домайн, app.dateStr);
    app.periodHZ = app.dateStr;
    app.fetchAvailableDatesForHZ();
  };

  app.workerSelected = function(){
    console.log(app.selWorker.домайн);
    app.getWorkerList(app.selWorker.домайн, app.dateStr);
    app.periodWorker = app.dateStr;
    app.fetchAvailableDatesForWorker();
  };

  app.wrBranchSelected = function(){
    var tempWorkers = [];
    app.selWorker = '';
    for (var i = 0; i < app.workers.length; i++) {
      if (app.workers[i].нэгж == app.selwrbranch._id) {
        tempWorkers.push(app.workers[i]);
      }
    }
    app.cworkers = tempWorkers;
  };

  app.checkIndexedDB = function () {
    if (app.onLine) {
      ds.getConnection().then(function () {
        return ds.db_.select(lf.fn.count())
            .from(ds.db_.getSchema().table('BranchChecklists'))
            .exec();
      }).then(function (rs) {
        if (rs[0]["COUNT(*)"]) {
          app.BranchsyncCount += rs[0]["COUNT(*)"];
        }
      }).then(function () {
        return ds.db_.select(lf.fn.count())
          .from(ds.db_.getSchema().table('HZChecklists'))
          .exec();
      }).then(function (rs) {
        if (rs[0]["COUNT(*)"]) {
          app.HZsyncCount += rs[0]["COUNT(*)"];
        }
      }).then(function () {
        return ds.db_.select(lf.fn.count())
            .from(ds.db_.getSchema().table('WorkerChecklists'))
            .exec();
      }).then(function (rs) {
        if (rs[0]["COUNT(*)"]) {
          app.WorkersyncCount += rs[0]["COUNT(*)"];
        }
      });
    }
  };

  app.uploadBranchIndexedDB = function () {
    ds.getConnection().then(function () {
      return ds.db_.select()
          .from(ds.db_.getSchema().table('BranchChecklists'))
          .exec();
    }).then(function (rs) {
      if (rs.length > 0) {
        for (var i = 0; i < rs.length; i++) {
          app.saveForSync('branch', rs[i]);
        }
        app.clearIndexedDB('branch');
      }
    });
  };

  app.uploadHZIndexedDB = function () {
    ds.getConnection().then(function () {
      return ds.db_.select()
        .from(ds.db_.getSchema().table('HZChecklists'))
        .exec();
    }).then(function (rs) {
      if (rs.length > 0) {
        for (var i = 0; i < rs.length; i++) {
          app.saveForSync('hz', rs[i]);
        }
        app.clearIndexedDB('hz');
      }
    });
  };

  app.uploadWorkerIndexedDB = function () {
    ds.getConnection().then(function () {
      return ds.db_.select()
          .from(ds.db_.getSchema().table('WorkerChecklists'))
          .exec();
    }).then(function (rs) {
      if (rs.length > 0) {
        for (var i = 0; i < rs.length; i++) {
          app.saveForSync('worker', rs[i]);
        }
        app.clearIndexedDB('worker');
      }
    });
  };

  app.saveForSync = function (t, e) {
    var argument = {
      id: new Date().getTime(),
      token: 'SelfLabToken',
      method: 'saveChecklistForSync',
      params: [t, e],
      'authType': 'APP'
    };

    var type = "";
    switch (t){
      case "worker": type = "Ажилтан"; break;
      case "hz": type = "Харилцагчийн зөвлөх"; break;
      case "branch": type = "Салбар"; break;
    }
    rpc('rpc/chklist', argument, function (resp) {
      if (resp.result) {
        app.displayToast(type + ' Амжилттай хадгалагдлаа.');
      } else {
        app.displayToast(type + ' Хадгалагдаж чадсангүй.');
      }
    }, true);
  };

  app.getBranches = function () {
    //console.log('in getBranches function');
    if (app.onLine) {
      var argument = {
        id: new Date().getTime(),
        token: 'SelfLabToken',
        method: 'getBranchList',
        params: [],
        'authType': 'APP'
      };
      rpc('rpc/chklist', argument, function (resp) {
        if(resp.result){
          if (resp.result.length > 0) {
            app.branches = [];
            app.branches = resp.result.slice(0);

            if (!localStorage["branches"]) {
              localStorage["branches"] = JSON.stringify([]);
            }
            localStorage["branches"] = JSON.stringify(app.branches.slice(0));

          } else {
            app.branches = [];
            sessionStorage.removeItem('RPC_TOKEN');
          }
          app.getHZs();
        } else {
          if (localStorage["branches"]) {
            app.branches = JSON.parse(localStorage["branches"]);
          }
          app.getHZs();
        }
      }, true);
    } else {
      if (!localStorage["branches"]) {
        //alert("Please get online!");
      } else {
        app.branches = JSON.parse(localStorage["branches"]);
      }
      app.getHZs();
    }
  };

  app.getHZs = function () {
    //console.log('in getHZs function');
    if (app.onLine) {
      var argument = {
        id: new Date().getTime(),
        token: 'SelfLabToken',
        method: 'getHZList',
        params: [],
        'authType': 'APP'
      };
      rpc('rpc/chklist', argument, function (resp) {
        if(resp.result){
          if (resp.result.length > 0) {
            app.hzs = [];
            app.hzs = resp.result.slice(0);
            if (!localStorage["hzs"]) {
              localStorage["hzs"] = JSON.stringify([]);
            }
            localStorage["hzs"] = JSON.stringify(app.hzs.slice(0));
          } else {
            app.hzs = [];
          }
          app.getWorkers();
        } else {
          if(localStorage["hzs"]){
            app.hzs = JSON.parse(localStorage["hzs"]);
          }
          app.getWorkers();
        }

      }, true);
    } else {
      if (!localStorage["hzs"]) {
        app.displayToast("Please get online!");
      } else {
        app.hzs = JSON.parse(localStorage["hzs"]);
      }
      app.getWorkers();
    }
  };

  app.getWorkers = function () {
    //console.log('in getWorkers function');
    if (app.onLine) {
      var argument = {
        id: new Date().getTime(),
        token: 'SelfLabToken',
        method: 'getWorkerList',
        params: [],
        'authType': 'APP'
      };
      rpc('rpc/chklist', argument, function (resp) {
        if(resp.result) {
          if (resp.result.length > 0) {
            app.workers = [];
            app.workers = resp.result.slice(0);
            app.cworkers = app.workers;
            if (!localStorage["workers"]) {
              localStorage["workers"] = JSON.stringify([]);
            }
            localStorage["workers"] = JSON.stringify(app.workers.slice(0));
          } else {
            app.workers = [];
            app.cworkers = [];
          }
        } else {
          if(localStorage["workers"]){
            app.workers = JSON.parse(localStorage["workers"]);
            app.cworkers = app.workers;
          }
        }
      }, true);
    } else {
      if (!localStorage["workers"]) {
      } else {
        app.workers = JSON.parse(localStorage["workers"]);
        app.cworkers = app.workers;
      }
    }
  };

  app.saveBranchCheckLists = function () {
    app.tempBranch.suma = 0; app.tempBranch.suma =  app.tempBranch.a1 + app.tempBranch.a2 + app.tempBranch.a3 + app.tempBranch.a4 + app.tempBranch.a5 + app.tempBranch.a6 + app.tempBranch.a7 + app.tempBranch.a8;
    app.tempBranch.sumb = 0; app.tempBranch.sumb =  app.tempBranch.b1 + app.tempBranch.b2 + app.tempBranch.b3 + app.tempBranch.b4;
    app.tempBranch.sumc = 0; app.tempBranch.sumc =  app.tempBranch.c1 + app.tempBranch.c2 + app.tempBranch.c3;
    app.tempBranch.sumd = 0; app.tempBranch.sumd =  app.tempBranch.d1 + app.tempBranch.d2 + app.tempBranch.d3 + app.tempBranch.d4 + app.tempBranch.d5 + app.tempBranch.d6 + app.tempBranch.d7 + app.tempBranch.d8 + app.tempBranch.d9 + app.tempBranch.d10 + app.tempBranch.d11 + app.tempBranch.d12 + app.tempBranch.d13;
    app.tempBranch.sume = 0; app.tempBranch.sume =  app.tempBranch.e1 + app.tempBranch.e2;
    app.tempBranch.sum = 0; app.tempBranch.sum =  app.tempBranch.suma + app.tempBranch.sumb + app.tempBranch.sumc + app.tempBranch.sumd + app.tempBranch.sume;
    app.tempBranch.addedBy = app.userdomain;
    app.tempBranch.addedByName = app.userdept + '-' + app.username ;

    var Month = moment().format('MM');
    switch(Month){
      case '01': app.tempBranch.season = 'Өвөл'; break;
      case '02': app.tempBranch.season = 'Өвөл'; break;
      case '03': app.tempBranch.season = 'Өвөл'; break;
      case '04': app.tempBranch.season = 'Хавар'; break;
      case '05': app.tempBranch.season = 'Хавар'; break;
      case '06': app.tempBranch.season = 'Хавар'; break;
      case '07': app.tempBranch.season = 'Зун'; break;
      case '08': app.tempBranch.season = 'Зун'; break;
      case '09': app.tempBranch.season = 'Зун'; break;
      case '10': app.tempBranch.season = 'Намар'; break;
      case '11': app.tempBranch.season = 'Намар'; break;
      case '12': app.tempBranch.season = 'Намар'; break;
    }

    console.log("Online save branch " +app.onLine);

    if (app.onLine) {
      var argument = {
        id: new Date().getTime(),
        token: 'SelfLabToken',
        method: 'saveBranchChecklist',
        params: [app.selBranch, app.tempBranch, app.dateStr],
        'authType': 'APP'
      };

      rpc('rpc/chklist', argument, function (resp) {
        if (resp.result) {
          app.displayToast('Амжилттай хадгалагдлаа.');
          app.clearChecklist('branch');
        } else {
          app.displayToast('Хадгалагдаж чадсангүй.');
        }
      }, true);
    } else {
      ds.getConnection().then(function () {
        var rows = ds.db_.getSchema().table('BranchChecklists').createRow({
          "addedBy": app.userdomain,
          "addedByName": app.userdept + '-' + app.username,
          "name": app.selBranch.нэр,
          "nickname": app.selBranch._id,
          "date": app.dateStr,
          "a1": app.tempBranch.a1,
          "a2": app.tempBranch.a2,
          "a3": app.tempBranch.a3,
          "a4": app.tempBranch.a4,
          "a5": app.tempBranch.a5,
          "a6": app.tempBranch.a6,
          "a7": app.tempBranch.a7,
          "a8": app.tempBranch.a8,
          "b1": app.tempBranch.b1,
          "b2": app.tempBranch.b2,
          "b3": app.tempBranch.b3,
          "b4": app.tempBranch.b4,
          "c1": app.tempBranch.c1,
          "c2": app.tempBranch.c2,
          "c3": app.tempBranch.c3,
          "d1": app.tempBranch.d1,
          "d2": app.tempBranch.d2,
          "d3": app.tempBranch.d3,
          "d4": app.tempBranch.d4,
          "d5": app.tempBranch.d5,
          "d6": app.tempBranch.d6,
          "d7": app.tempBranch.d7,
          "d8": app.tempBranch.d8,
          "d9": app.tempBranch.d9,
          "d10": app.tempBranch.d10,
          "d11": app.tempBranch.d11,
          "d12": app.tempBranch.d12,
          "d13": app.tempBranch.d13,
          "e1": app.tempBranch.e1,
          "e2": app.tempBranch.e2,
          "comment": app.tempBranch.comment,
          "suma": app.tempBranch.suma,
          "sumb": app.tempBranch.sumb,
          "sumc": app.tempBranch.sumc,
          "sumd": app.tempBranch.sumd,
          "sume": app.tempBranch.sume,
          "sum":  app.tempBranch.sum,
          "season": app.tempBranch.season
        });

        app.selBranch = '';
        app.clearChecklist('branch');
        return ds.db_.insertOrReplace().into(ds.db_.getSchema().table('BranchChecklists')).values([rows]).exec();
      }).then(function (rs) {
        if (rs.length > 0) {
          app.displayToast('Амжилттай хадгалагдлаа.');
        } else {
          // empty
          app.displayToast('Хадгалагдаж чадсангүй.');
        }
      }.bind(this));
    }
  };

  app.saveHZCheckLists = function () {
    app.tempHZ.suma = 0; app.tempHZ.suma =  app.tempHZ.a1 + app.tempHZ.a2 + app.tempHZ.a3 + app.tempHZ.a4 + app.tempHZ.a5 + app.tempHZ.a6 + app.tempHZ.a7;
    app.tempHZ.sumb = 0; app.tempHZ.sumb =  app.tempHZ.b1 + app.tempHZ.b2 + app.tempHZ.b3 + app.tempHZ.b4;
    app.tempHZ.sumc = 0; app.tempHZ.sumc =  app.tempHZ.c1 + app.tempHZ.c2 + app.tempHZ.c3;
    app.tempHZ.sumd = 0; app.tempHZ.sumd =  app.tempHZ.d1 + app.tempHZ.d2 + app.tempHZ.d3;
    app.tempHZ.sume = 0; app.tempHZ.sume =  app.tempHZ.e1 + app.tempHZ.e2;
    app.tempHZ.sumf = 0; app.tempHZ.sumf =  app.tempHZ.f1 + app.tempHZ.f2 + app.tempHZ.f3;
    app.tempHZ.sum = 0;  app.tempHZ.sum =  app.tempHZ.suma + app.tempHZ.sumb + app.tempHZ.sumc + app.tempHZ.sumd + app.tempHZ.sume + app.tempHZ.sumf;
    app.tempHZ.addedBy = app.userdomain;
    app.tempHZ.addedByName = app.userdept + '-' + app.username;

    var Month = moment().format('MM');
    switch(Month){
      case '01': app.tempHZ.season = 'Өвөл'; break;
      case '02': app.tempHZ.season = 'Өвөл'; break;
      case '03': app.tempHZ.season = 'Өвөл'; break;
      case '04': app.tempHZ.season = 'Хавар'; break;
      case '05': app.tempHZ.season = 'Хавар'; break;
      case '06': app.tempHZ.season = 'Хавар'; break;
      case '07': app.tempHZ.season = 'Зун'; break;
      case '08': app.tempHZ.season = 'Зун'; break;
      case '09': app.tempHZ.season = 'Зун'; break;
      case '10': app.tempHZ.season = 'Намар'; break;
      case '11': app.tempHZ.season = 'Намар'; break;
      case '12': app.tempHZ.season = 'Намар'; break;
    }
    if (app.onLine) {
      var argument = {
        id: new Date().getTime(),
        token: 'SelfLabToken',
        method: 'saveHZChecklist',
        params: [app.selHZ, app.tempHZ, app.dateStr],
        'authType': 'APP'
      };

      rpc('rpc/chklist', argument, function (resp) {
        if (resp.result) {
          app.displayToast('Амжилттай хадгалагдлаа.');
          app.clearChecklist('hz');
        } else {
          app.displayToast('Хадгалагдаж чадсангүй.');
        }
      }, true);
    } else {
      ds.getConnection().then(function () {
        var rows = ds.db_.getSchema().table('HZChecklists').createRow({
          "domain": app.selHZ.домайн,
          "addedBy": app.userdomain,
          "addedByName": app.userdept + '-' + app.username,
          "name": app.selHZ.нэр,
          "surname": app.selHZ.эцэг,
          "branch": app.selHZ.нэгж,
          "date": app.dateStr,
          "a1": app.tempHZ.a1,
          "a2": app.tempHZ.a2,
          "a3": app.tempHZ.a3,
          "a4": app.tempHZ.a4,
          "a5": app.tempHZ.a5,
          "a6": app.tempHZ.a6,
          "a7": app.tempHZ.a7,
          "a8": app.tempHZ.a8,
          "b1": app.tempHZ.b1,
          "b2": app.tempHZ.b2,
          "b3": app.tempHZ.b3,
          "b4": app.tempHZ.b4,
          "c1": app.tempHZ.c1,
          "c2": app.tempHZ.c2,
          "c3": app.tempHZ.c3,
          "d1": app.tempHZ.d1,
          "d2": app.tempHZ.d2,
          "d3": app.tempHZ.d3,
          "e1": app.tempHZ.e1,
          "e2": app.tempHZ.e2,
          "f1": app.tempHZ.f1,
          "f2": app.tempHZ.f2,
          "f3": app.tempHZ.f3,
          "comment": app.tempHZ.comment,

          "suma": app.tempHZ.suma,
          "sumb": app.tempHZ.sumb,
          "sumc": app.tempHZ.sumc,
          "sumd": app.tempHZ.sumd,
          "sume": app.tempHZ.sume,
          "sumf": app.tempHZ.sumf,
          "sum": app.tempHZ.sum,
          "season": app.tempHZ.season
        });

        app.selHZ = '';
        app.clearChecklist('hz');

        return ds.db_.insertOrReplace().into(ds.db_.getSchema().table('HZChecklists')).values([rows]).exec();
      }).then(function (rs) {
        if (rs.length > 0) {
          app.displayToast('Амжилттай хадгалагдлаа.');
        } else {
          // empty
          app.displayToast('Хадгалагдаж чадсангүй.');
        }
      }.bind(this));
    }
  };

  app.saveWorkerCheckLists = function () {
    app.tempHZ.suma = 0; app.tempWorker.suma =  app.tempWorker.a1 + app.tempWorker.a2 + app.tempWorker.a3 + app.tempWorker.a4 + app.tempWorker.a5 + app.tempWorker.a6 + app.tempWorker.a7 + app.tempWorker.a8;
    app.tempHZ.sumb = 0; app.tempWorker.sumb =  app.tempWorker.b1 + app.tempWorker.b2 + app.tempWorker.b3 + app.tempWorker.b4;
    app.tempHZ.sumc = 0; app.tempWorker.sumc =  app.tempWorker.c1 + app.tempWorker.c2 + app.tempWorker.c3;
    app.tempHZ.sumd = 0; app.tempWorker.sumd =  app.tempWorker.d1 + app.tempWorker.d2;
    app.tempHZ.sum = 0; app.tempWorker.sum =  app.tempWorker.suma + app.tempWorker.sumb + app.tempWorker.sumc + app.tempWorker.sumd;
    app.tempWorker.addedBy = app.userdomain;
    app.tempWorker.addedByName = app.userdept + '-' + app.username;

    var Month = moment().format('MM');
    switch(Month){
      case '01': app.tempWorker.season = 'Өвөл'; break;
      case '02': app.tempWorker.season = 'Өвөл'; break;
      case '03': app.tempWorker.season = 'Өвөл'; break;
      case '04': app.tempWorker.season = 'Хавар'; break;
      case '05': app.tempWorker.season = 'Хавар'; break;
      case '06': app.tempWorker.season = 'Хавар'; break;
      case '07': app.tempWorker.season = 'Зун'; break;
      case '08': app.tempWorker.season = 'Зун'; break;
      case '09': app.tempWorker.season = 'Зун'; break;
      case '10': app.tempWorker.season = 'Намар'; break;
      case '11': app.tempWorker.season = 'Намар'; break;
      case '12': app.tempWorker.season = 'Намар'; break;
    }

    if (app.onLine) {
      var argument = {
        id: new Date().getTime(),
        token: 'SelfLabToken',
        method: 'saveWorkerChecklist',
        params: [app.selWorker, app.tempWorker, app.dateStr],
        'authType': 'APP'
      };

      rpc('rpc/chklist', argument, function (resp) {
        if (resp.result) {
          app.displayToast('Амжилттай хадгалагдлаа.');
          app.clearChecklist("worker");
        } else {
          app.displayToast('Хадгалагдаж чадсангүй.');
        }
      }, true);
    } else {
      ds.getConnection().then(function () {
        //console.log(app.selwrbranch);
        var rows = ds.db_.getSchema().table('WorkerChecklists').createRow({
          "addedBy": app.userdomain,
          "addedByName": app.userdept + '-' + app.username,
          "domain": app.selWorker.домайн,
          "name": app.selWorker.нэр,
          "surname": app.selWorker.эцэг,
          "date": app.dateStr,
          "branch": app.selwrbranch.нэр,
          "position": app.selWorker.нэгж,
          "a1": app.tempWorker.a1,
          "a2": app.tempWorker.a2,
          "a3": app.tempWorker.a3,
          "a4": app.tempWorker.a4,
          "a5": app.tempWorker.a5,
          "a6": app.tempWorker.a6,
          "a7": app.tempWorker.a7,
          "a8": app.tempWorker.a8,
          "b1": app.tempWorker.b1,
          "b2": app.tempWorker.b2,
          "b3": app.tempWorker.b3,
          "b4": app.tempWorker.b4,
          "c1": app.tempWorker.c1,
          "c2": app.tempWorker.c2,
          "c3": app.tempWorker.c3,
          "d1": app.tempWorker.d1,
          "d2": app.tempWorker.d2,
          "comment": app.tempWorker.comment,
          "suma": app.tempWorker.suma,
          "sumb": app.tempWorker.sumb,
          "sumc": app.tempWorker.sumc,
          "sumd": app.tempWorker.sumd,
          "sum":  app.tempWorker.sum,
          "season": app.tempWorker.season
        });

        app.selwrbranch = '';
        app.selWorker = '';
        app.clearChecklist("worker");

        return ds.db_.insertOrReplace().into(ds.db_.getSchema().table('WorkerChecklists')).values([rows]).exec();
      }).then(function (rs) {
        if (rs.length > 0) {
          //console.log(rs.length + ' workerChecklist(s) inserted.');
          app.displayToast('Амжилттай хадгалагдлаа.');
        } else {
          // empty
          app.displayToast('Хадгалагдаж чадсангүй.');
        }
      }.bind(this));
    }
  };

  app.getBranchList = function (e, d) {
    if (app.onLine) {
      var argument = {
        id: new Date().getTime(),
        token: 'SelfLabToken',
        method: 'getBranchChecklist',
        params: [app.selBranch.нэр, d],
        'authType': 'APP'
      };
      rpc('rpc/chklist', argument, function (resp) {
        if (resp.result) {
          app.tempBranch = resp.result;
          if(!app.tempBranch.approvedBy && !app.tempBranch.approvedByName && app.tempBranch.addedByName != ""){
            if(app.selBranch._id == app.userdept && app.approvalrole){
              console.log("approval");
              app.checklistApproval = true;
            } else {
              app.checklistApproval = false;
            }

          } else {
            console.log("not approval");
            app.checklistApproval = false;
          }

          if((app.tempBranch.addedBy == "" || app.tempBranch.addedBy == app.userdomain) && !(!app.bigrole || (app.tempBranch.approvedBy && app.tempBranch.approvedByName) )){
            app.disableBranch = false;
          } else {
            app.disableBranch = true;
          }


        } else {
          if(!app.bigrole || (app.tempBranch.approvedBy && app.tempBranch.approvedByName)){
            app.disableBranch = true;
          } else {
            app.disableBranch = false;
          }
          app.clearChecklist("branch");
        }
      }, false);
    } else {
      var re = new RegExp(e, "g");
      ds.getConnection().then(function () {
        return ds.db_.select()
            .from(ds.db_.getSchema().table('BranchChecklists'))
            .where(ds.db_.getSchema().table('BranchChecklists').name.match(re))
            .limit(1)
            .exec();
      }).then(function (rs) {
        if (rs.length > 0) {
          this.tempBranch = rs[0];
        } else {
          app.clearChecklist("branch");
        }
      }.bind(this));
    }
  };

  app.getHZList = function (e, d) {
    if (app.onLine) {
      var argument = {
        id: new Date().getTime(),
        token: 'SelfLabToken',
        method: 'getHZChecklist',
        params: [app.selHZ.домайн, d],
        'authType': 'APP'
      };
      rpc('rpc/chklist', argument, function (resp) {
        if (resp.result) {
          app.tempHZ = resp.result;
          if(app.selHZ.нэгж == app.userdept && !app.tempHZ.approvedBy && !app.tempHZ.approvedByName && app.tempHZ.addedByName != ""){
            console.log("approval");
            app.checklistApproval = true;
          } else {
            console.log("not approval");
            app.checklistApproval = false;
          }

          if(app.bigrole && (app.tempHZ.addedBy == "" || app.tempHZ.addedBy == app.userdomain)){
            app.disableHZ = false;
          } else {
            app.disableHZ = true;
          }
        } else {
          if(!app.bigrole || (app.tempHZ.approvedBy && app.tempHZ.approvedByName)){
            app.disableHZ = true;
          } else {
            app.disableHZ = false;
          }
          app.clearChecklist("hz");
          //console.log('No hz!');
        }
      }, true);
    } else {
      var re = new RegExp(e, "g");
      ds.getConnection().then(function () {
        return ds.db_.select()
          .from(ds.db_.getSchema().table('HZChecklists'))
          .where(ds.db_.getSchema().table('HZChecklists').domain.match(re))
          .limit(1)
          .exec();
      }).then(function (rs) {
        if (rs.length > 0) {
          //console.log(rs.length + ' hz(s) loaded.');
          //console.log(rs[0]);
          this.tempHZ = rs[0];
        } else {
          // empty
          app.clearChecklist("hz");
          //console.log('No hz!');
        }
      }.bind(this));
    }
  };

  app.getWorkerList = function (e, d) {
    if (app.onLine) {
      var argument = {
        id: new Date().getTime(),
        token: 'SelfLabToken',
        method: 'getWorkerChecklist',
        params: [app.selWorker.домайн, d],
        'authType': 'APP'
      };
      rpc('rpc/chklist', argument, function (resp) {
        if (resp.result) {
          app.tempWorker = resp.result;
          if(app.selwrbranch._id == app.userdept && !app.tempWorker.approvedBy && !app.tempWorker.approvedByName && app.tempWorker.addedByName != ""){
            console.log("approval");
            app.checklistApproval = true;
          } else {
            console.log("not approval");
            app.checklistApproval = false;
          }

          if((app.tempWorker.addedBy == "" || app.tempWorker.addedBy == app.userdomain) && !(app.uaaarole || app.approvalrole)){
            app.disableWorker = false;
          } else {
            app.disableWorker = true;
          }
        } else {
          if(app.uaaarole || app.approvalrole || (app.tempWorker.approvedBy && app.tempWorker.approvedByName)){
            app.disableWorker = true;
          } else {
            app.disableWorker = false;
          }
          app.clearChecklist("worker");
          //console.log('No worker!');
        }
      }, true);
    } else {
      var re = new RegExp(e, "g");
      ds.getConnection().then(function () {
        return ds.db_.select()
            .from(ds.db_.getSchema().table('WorkerChecklists'))
            .where(ds.db_.getSchema().table('WorkerChecklists').domain.match(re))
            .limit(1)
            .exec();
      }).then(function (rs) {
        if (rs.length > 0) {
          //console.log(rs.length + ' worker(s) loaded.');
          //console.log(rs[0]);
          this.tempWorker = rs[0];
        } else {
          // empty
          app.clearChecklist("worker");
          //console.log('No worker!');
        }
      }.bind(this));
    }
  };

  app.loginSubmit = function (e) {
    if (e.keyCode == 13) {
      if (app.uid && app.pwd) {
        app.login();
      }
    }
  };

  app.login = function () {
    if (!app.uid || !app.pwd) {
      return;
    }
    if(app.onLine) {
      app.statusMsg = "нэвтэрч байна...";
      var argument = {
        id: new Date().getTime(),
        token: 'SelfLabToken',
        method: 'login',
        params: [app.uid.trim().toLowerCase(), app.pwd, 'LDAP'],
        'authType': 'LDAP'
      };
      rpc("rpc/chklist", argument, function (resp) {
        if (resp.result) {
          app.username = resp.result.username;
          app.userdomain = app.uid.trim().toLowerCase();
          app.userdept = resp.result.dept;
          app.userrole = resp.result.role;
          app.userimage = resp.result.userimage;

          if (app.userrole == "hzchecklist.uchhm") {
            app.bigrole = true;
          } else if(app.userrole == "hzchecklist.hz"){
            app.disableBranch = true;
            app.disableHZ = true;
          } else if(app.userrole == "hzchecklist.approval"){
            app.approvalrole = true;
            app.disableWorker = true;
            app.disableHZ = true;
            app.disableBranch = true;
          } else if(app.userrole == "hzchecklist.uaaa"){
            app.uaaarole = true;
            app.disableWorker = true;
            app.disableHZ = true;
            app.disableBranch = true;
          }

          app.uid = '';
          app.pwd = '';
          app.statusMsg = '';

          if (app.userrole == "") {
            app.statusMsg = "Танд уг програмруу хандах эрх алга байна.";
          } else {
            app.showLogin = false;
            app.showMain = true;
            var user = new Object();
            user.domain = app.userdomain;
            user.username = app.username;
            user.dept = app.userdept;
            user.role = app.userrole;
            user.image = app.userimage;
            localStorage["user"] = JSON.stringify(user);
          }

        } else {
          app.statusMsg = "нэр, нууц үгээ шалгана уу";
        }
      }, true);
    } else {
      app.statusMsg = "Та зөвхөн онлайн байх үедээ нэвтрэх боломжтой!";
    }
  };

  app.fetchAvailableDatesForBranch = function() {
    console.log('in fetchAvailableDatesForBranch function');
    app.periodsBranch = [];
    if (app.onLine) {
      var argument = {
        id: new Date().getTime(),
        token: 'SelfLabToken',
        method: 'fetchAvailableDates',
        params: ['branch', app.selBranch.нэр],
        'authType': 'APP'
      };
      rpc('rpc/chklist', argument, function (resp) {
        if (resp.result.length > 0) {
          app.periodsBranch = resp.result.slice(0);
          if(app.periodsBranch[0].date == app.dateStr){
            app.periodsBranch.splice(0,1);
          }
        } else {
          app.periodsBranch = [];
          app.periodBranch = app.dateStr;
        }
      }, true);
    } else {
      app.periodsBranch = [];
      app.periodBranch = app.dateStr;
    }
  };

  app.periodBranchSelected = function(){
    app.getBranchList(app.selBranch.нэр, app.periodBranch);
  };

  app.fetchAvailableDatesForHZ = function() {
    console.log('in fetchAvailableDatesForHZ function');
    app.periodsHZ = [];
    if (app.onLine) {
      var argument = {
        id: new Date().getTime(),
        token: 'SelfLabToken',
        method: 'fetchAvailableDates',
        params: ['hz', app.selHZ.домайн],
        'authType': 'APP'
      };
      rpc('rpc/chklist', argument, function (resp) {
        if (resp.result.length > 0) {
          app.periodsHZ = resp.result.slice(0);
          if(app.periodsHZ[0].date == app.dateStr){
            app.periodsHZ.splice(0,1);
          }
        } else {
          app.periodsHZ = [];
          app.periodHZ = app.dateStr;
        }
      }, true);
    } else {
      app.periodsHZ = [];
      app.periodHZ = app.dateStr;
    }
  };

  app.periodHZSelected = function(){
    app.getHZList(app.selHZ.домайн, app.periodHZ);
  };

  app.fetchAvailableDatesForWorker = function() {
    console.log('in fetchAvailableDatesForWorker function');
    app.periodsWorker = [];
    if (app.onLine) {
      var argument = {
        id: new Date().getTime(),
        token: 'SelfLabToken',
        method: 'fetchAvailableDates',
        params: ['worker', app.selWorker.домайн],
        'authType': 'APP'
      };
      rpc('rpc/chklist', argument, function (resp) {
        if (resp.result.length > 0) {
          app.periodsWorker = resp.result.slice(0);
          if(app.periodsWorker[0].date == app.dateStr){
            app.periodsWorker.splice(0,1);
          }
        } else {
          app.periodsWorker = [];
          app.periodWorker = app.dateStr;
        }
      }, true);
    } else {
      app.periodsWorker = [];
      app.periodWorker = app.dateStr;
    }
  };

  app.periodWorkerSelected = function(){
    console.log('periodWorkerSelected');
    app.getWorkerList(app.selWorker.домайн, app.periodWorker);
  };

  app.exportXSLbr = function(){
    downloadXLS(prepareJSON('br',app.tempBranch), 'export-data-' + new Date().getTime() + '.xls', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  };

  app.exportXSLhz = function(){
    downloadXLS(prepareJSON('hz',app.tempHZ), 'export-data-' + new Date().getTime() + '.xls', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  };

  app.exportXSLwr = function(){
    downloadXLS(prepareJSON('wr',app.tempWorker), 'export-data-' + new Date().getTime() + '.xls', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  };

  app.approveBRCheckLists = function(){
    app.approveType = "br";
    var a = document.querySelector("#main-dialog");
    a.open();
  };

  app.approveHZCheckLists = function(){
    app.approveType = "hz";
    var a = document.querySelector("#main-dialog");
    a.open();
  };

  app.approveWRCheckLists = function(){
    app.approveType = "wr";
    var a = document.querySelector("#main-dialog");
    a.open();
  };

  app.approveCheckLists = function(){
    console.log(app.approveType);
    var checklist = new Object();
    switch(app.approveType){
      case 'br': app.tempBranch.approvedBy = app.userdomain; app.tempBranch.approvedByName = app.userdept + '-' + app.username;  checklist = app.tempBranch; break;
      case 'hz': app.tempHZ.approvedBy = app.userdomain; app.tempHZ.approvedByName = app.userdept + '-' + app.username;  checklist = app.tempHZ; break;
      case 'wr': app.tempWorker.approvedBy = app.userdomain; app.tempWorker.approvedByName = app.userdept + '-' + app.username;  checklist = app.tempWorker; break;
    }

    var argument = {
      id: new Date().getTime(),
      token: 'SelfLabToken',
      method: 'approveChecklist',
      params: [checklist],
      'authType': 'APP'
    };

    rpc('rpc/chklist', argument, function (resp) {
      if (resp.result) {
        app.checklistApproval = false;
        app.displayToast('Амжилттай баталгаажлаа.');
      } else {
        switch(app.approveType){
          case 'br': app.tempBranch.approvedBy = ""; app.tempBranch.approvedByName = "";  break;
          case 'hz': app.tempHZ.approvedBy = ""; app.tempHZ.approvedByName = "";  break;
          case 'wr': app.tempWorker.approvedBy = ""; app.tempWorker.approvedByName = "";  break;
        }
        app.displayToast('Баталгаажуулж чадсангүй.');
      }
    }, true);
  };

  app.confirmDialogClosed = function(e){
    console.log("closed main-dialog");
    console.log(e.target.closingReason);
    var closingReason = e.target.closingReason;
    if(closingReason.confirmed){
      console.log("confirmed");
      app.approveCheckLists();
    }
  };

  app._isCityBranch = function(item) {
    if(item.байршил == "хот"){
      return true;
    } else{
      return false;
    }
  };

  app.clearChecklist = function (e) {
    switch (e) {
      case 'worker':
        app.tempWorker = {
          "addedBy": "",
          "addedByName": "",
          "approvedBy":"",
          "approvedByName":"",
          "a1": 0,
          "a2": 0,
          "a3": 0,
          "a4": 0,
          "a5": 0,
          "a6": 0,
          "a7": 0,
          "a8": 0,
          "a9": 0,
          "a10": 0,
          "a11": 0,
          "a12": 0,
          "a13": 0,
          "a14": 0,
          "a15": 0,
          "b1": 0,
          "b2": 0,
          "b3": 0,
          "b4": 0,
          "b5": 0,
          "b6": 0,
          "b7": 0,
          "b8": 0,
          "b9": 0,
          "b10": 0,
          "b11": 0,
          "b12": 0,
          "b13": 0,
          "b14": 0,
          "b15": 0,
          "c1": 0,
          "c2": 0,
          "c3": 0,
          "c4": 0,
          "c5": 0,
          "c6": 0,
          "c7": 0,
          "c8": 0,
          "c9": 0,
          "c10": 0,
          "c11": 0,
          "c12": 0,
          "c13": 0,
          "c14": 0,
          "c15": 0,
          "d1": 0,
          "d2": 0,
          "d3": 0,
          "d4": 0,
          "d5": 0,
          "d6": 0,
          "d7": 0,
          "d8": 0,
          "d9": 0,
          "d10": 0,
          "d11": 0,
          "d12": 0,
          "d13": 0,
          "d14": 0,
          "d15": 0,
          "e1": 0,
          "e2": 0,
          "e3": 0,
          "e4": 0,
          "e5": 0,
          "e6": 0,
          "e7": 0,
          "e8": 0,
          "e9": 0,
          "e10": 0,
          "e11": 0,
          "e12": 0,
          "e13": 0,
          "e14": 0,
          "e15": 0,
          "f1": 0,
          "f2": 0,
          "f3": 0,
          "f4": 0,
          "f5": 0,
          "f6": 0,
          "f7": 0,
          "f8": 0,
          "f9": 0,
          "f10": 0,
          "f11": 0,
          "f12": 0,
          "f13": 0,
          "f14": 0,
          "f15": 0,
          "g1": 0,
          "g2": 0,
          "g3": 0,
          "g4": 0,
          "g5": 0,
          "g6": 0,
          "g7": 0,
          "g8": 0,
          "g9": 0,
          "g10": 0,
          "g11": 0,
          "g12": 0,
          "g13": 0,
          "g14": 0,
          "g15": 0,
          "comment": ''
        };
        break;
      case 'hz':
        app.tempHZ = {
          "addedBy": "",
          "addedByName": "",
          "approvedBy":"",
          "approvedByName":"",
          "a1": 0,
          "a2": 0,
          "a3": 0,
          "a4": 0,
          "a5": 0,
          "a6": 0,
          "a7": 0,
          "a8": 0,
          "a9": 0,
          "a10": 0,
          "a11": 0,
          "a12": 0,
          "a13": 0,
          "a14": 0,
          "a15": 0,
          "b1": 0,
          "b2": 0,
          "b3": 0,
          "b4": 0,
          "b5": 0,
          "b6": 0,
          "b7": 0,
          "b8": 0,
          "b9": 0,
          "b10": 0,
          "b11": 0,
          "b12": 0,
          "b13": 0,
          "b14": 0,
          "b15": 0,
          "c1": 0,
          "c2": 0,
          "c3": 0,
          "c4": 0,
          "c5": 0,
          "c6": 0,
          "c7": 0,
          "c8": 0,
          "c9": 0,
          "c10": 0,
          "c11": 0,
          "c12": 0,
          "c13": 0,
          "c14": 0,
          "c15": 0,
          "d1": 0,
          "d2": 0,
          "d3": 0,
          "d4": 0,
          "d5": 0,
          "d6": 0,
          "d7": 0,
          "d8": 0,
          "d9": 0,
          "d10": 0,
          "d11": 0,
          "d12": 0,
          "d13": 0,
          "d14": 0,
          "d15": 0,
          "e1": 0,
          "e2": 0,
          "e3": 0,
          "e4": 0,
          "e5": 0,
          "e6": 0,
          "e7": 0,
          "e8": 0,
          "e9": 0,
          "e10": 0,
          "e11": 0,
          "e12": 0,
          "e13": 0,
          "e14": 0,
          "e15": 0,
          "f1": 0,
          "f2": 0,
          "f3": 0,
          "f4": 0,
          "f5": 0,
          "f6": 0,
          "f7": 0,
          "f8": 0,
          "f9": 0,
          "f10": 0,
          "f11": 0,
          "f12": 0,
          "f13": 0,
          "f14": 0,
          "f15": 0,
          "g1": 0,
          "g2": 0,
          "g3": 0,
          "g4": 0,
          "g5": 0,
          "g6": 0,
          "g7": 0,
          "g8": 0,
          "g9": 0,
          "g10": 0,
          "g11": 0,
          "g12": 0,
          "g13": 0,
          "g14": 0,
          "g15": 0,
          "comment": ''
        };
        break;
      case 'branch':
        app.tempBranch = {
          "addedBy": "",
          "addedByName": "",
          "approvedBy":"",
          "approvedByName":"",
          "a1": 0,
          "a2": 0,
          "a3": 0,
          "a4": 0,
          "a5": 0,
          "a6": 0,
          "a7": 0,
          "a8": 0,
          "a9": 0,
          "a10": 0,
          "a11": 0,
          "a12": 0,
          "a13": 0,
          "a14": 0,
          "a15": 0,
          "b1": 0,
          "b2": 0,
          "b3": 0,
          "b4": 0,
          "b5": 0,
          "b6": 0,
          "b7": 0,
          "b8": 0,
          "b9": 0,
          "b10": 0,
          "b11": 0,
          "b12": 0,
          "b13": 0,
          "b14": 0,
          "b15": 0,
          "c1": 0,
          "c2": 0,
          "c3": 0,
          "c4": 0,
          "c5": 0,
          "c6": 0,
          "c7": 0,
          "c8": 0,
          "c9": 0,
          "c10": 0,
          "c11": 0,
          "c12": 0,
          "c13": 0,
          "c14": 0,
          "c15": 0,
          "d1": 0,
          "d2": 0,
          "d3": 0,
          "d4": 0,
          "d5": 0,
          "d6": 0,
          "d7": 0,
          "d8": 0,
          "d9": 0,
          "d10": 0,
          "d11": 0,
          "d12": 0,
          "d13": 0,
          "d14": 0,
          "d15": 0,
          "e1": 0,
          "e2": 0,
          "e3": 0,
          "e4": 0,
          "e5": 0,
          "e6": 0,
          "e7": 0,
          "e8": 0,
          "e9": 0,
          "e10": 0,
          "e11": 0,
          "e12": 0,
          "e13": 0,
          "e14": 0,
          "e15": 0,
          "f1": 0,
          "f2": 0,
          "f3": 0,
          "f4": 0,
          "f5": 0,
          "f6": 0,
          "f7": 0,
          "f8": 0,
          "f9": 0,
          "f10": 0,
          "f11": 0,
          "f12": 0,
          "f13": 0,
          "f14": 0,
          "f15": 0,
          "g1": 0,
          "g2": 0,
          "g3": 0,
          "g4": 0,
          "g5": 0,
          "g6": 0,
          "g7": 0,
          "g8": 0,
          "g9": 0,
          "g10": 0,
          "g11": 0,
          "g12": 0,
          "g13": 0,
          "g14": 0,
          "g15": 0,
          "comment": ''
        };
        break;
      default :
        break;
    }
  };

  app.clearIndexedDB = function (t) {
    var type = "";
    switch (t){
      case "worker" : type = "WorkerChecklists"; app.WorkersyncCount = 0; break;
      case "hz"     : type = "HZChecklists"; app.HZsyncCount = 0; break;
      case "branch" : type = "BranchChecklists"; app.BranchsyncCount = 0;  break;
      default : return;
    }
    ds.getConnection().then(function () {
      ds.db_.delete()
          .from(ds.db_.getSchema().table(type))
          .exec();
    });
  };

  app.logout = function () {
    app.clearIndexedDB();
    app.user = '';
    localStorage['user'] = '';
    app.showLogin = true;
    app.showMain = false;
  };

})(document);
