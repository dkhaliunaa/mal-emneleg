/**
 * Created by 23949959 on 4/7/2016.
 */

emitXmlHeader = function () {
    var headerRow;
    headerRow  = '<?xml version="1.0"?>\n';
    headerRow += '<ss:Workbook xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">\n';
    headerRow += '<ss:Styles>\n';
    headerRow += '<ss:Style ss:ID="s62">\n';
    headerRow += '<ss:Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>\n';
    headerRow += '<ss:Borders>\n';
    headerRow += '<ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>\n';
    headerRow += '<ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>\n';
    headerRow += '<ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>\n';
    headerRow += '<ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>\n';
    headerRow += '</ss:Borders>\n';
    headerRow += '<ss:Font ss:FontName="Calibri" ss:Family="Swiss" ss:Size="11" ss:Color="#000000"/>\n';
    headerRow += '</ss:Style>\n';
    headerRow += '<ss:Style ss:ID="s69">\n';
    headerRow += '<ss:Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/>\n';
    headerRow += '<ss:Borders>\n';
    headerRow += '<ss:Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>\n';
    headerRow += '<ss:Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>\n';
    headerRow += '<ss:Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>\n';
    headerRow += '<ss:Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>\n';
    headerRow += '</ss:Borders>\n';
    headerRow += '<ss:Font ss:FontName="Calibri" ss:Family="Swiss" ss:Size="11" ss:Color="#000000"/>\n';
    headerRow += '</ss:Style>\n';
    headerRow += '</ss:Styles>\n';
    headerRow += '<ss:Worksheet ss:Name="Sheet1">\n';
    headerRow += '<ss:Table>\n';
    headerRow += '<ss:Column ss:Index="5" ss:AutoFitWidth="0" ss:Width="80"/>\n';
    headerRow += '<ss:Column ss:AutoFitWidth="0" ss:Width="350"/>\n';
    headerRow += '<ss:Column ss:AutoFitWidth="0" ss:Width="66"/>\n';
    return headerRow;
};

emitXmlFooter = function (){
    var footerRow;
    footerRow = '</ss:Table>\n';
    footerRow += '</ss:Worksheet>\n';
    footerRow += '</ss:Workbook>\n';
    return footerRow;
};

emitXmlBody = function (JSONObj, first) {

    var titleRow = '';
    if(first){
        titleRow += '<ss:Row ss:Index="5" ss:AutoFitHeight="0" ss:Height="30">\n';
    } else {
        titleRow += '<ss:Row ss:AutoFitHeight="0" ss:Height="30">\n';
    }

    // Left Header
    titleRow += '<ss:Cell ss:Index="5" ss:MergeDown="' + (JSONObj.items.length - 1) + '" ss:StyleID="s62">\n';
    titleRow += '<ss:Data ss:Type="String">' + JSONObj.title + '</ss:Data></ss:Cell>\n';

    // Items
    for (var i = 0; i < JSONObj.items.length; i++) {
        if (i != 0) {
            titleRow += '<ss:Row ss:AutoFitHeight="0" ss:Height="30">';
            titleRow += '<ss:Cell ss:Index="6" ss:StyleID="s69"><ss:Data ss:Type="String">' + JSONObj.items[i].subtitle + '</ss:Data></ss:Cell>';
        } else {
            titleRow += '<ss:Cell ss:StyleID="s69"><ss:Data ss:Type="String">' + JSONObj.items[i].subtitle + '</ss:Data></ss:Cell>';
        }
        titleRow += '<ss:Cell ss:StyleID="s62"><ss:Data ss:Type="Number">' + JSONObj.items[i].value + '</ss:Data></ss:Cell>';
        titleRow += '</ss:Row>';
    }
    return titleRow;
};

emitXML2Column = function(title, text, first){
    var titleRow = '';

    if(first){
        titleRow += '<ss:Row ss:Index="5" ss:AutoFitHeight="0" ss:Height="30">\n';
    } else {
        titleRow += '<ss:Row ss:AutoFitHeight="0" ss:Height="30">\n';
    }
    if(text == null || text == 'undefined'){
        text = '';
    }

    // Title
    titleRow += '<ss:Cell ss:Index="5" ss:StyleID="s62"><ss:Data ss:Type="String">' + title + '</ss:Data></ss:Cell>\n';

    // Text
    titleRow += '<ss:Cell ss:MergeAcross="1" ss:StyleID="s62"><ss:Data ss:Type="String">'+ text + '</ss:Data></ss:Cell>\n';
    titleRow += '</ss:Row>';
    return titleRow;
};

prepareJSON = function (type, obj) {
    var JSONObja, JSONObjb, JSONObjc, JSONObjd, JSONObje, JSONObjf, JSONComm, xlsBody;

    switch (type) {
        case 'br':
            JSONObja = {
                "title": app.br.suma,
                "items": [
                    {
                        "subtitle": app.br.a1,
                        "value": obj.a1
                    },

                    {
                        "subtitle": app.br.a2,
                        "value": obj.a2
                    },

                    {
                        "subtitle": app.br.a3,
                        "value": obj.a3
                    },

                    {
                        "subtitle": app.br.a4,
                        "value": obj.a4
                    },

                    {
                        "subtitle": app.br.a5,
                        "value": obj.a5
                    },

                    {
                        "subtitle": app.br.a6,
                        "value": obj.a6
                    },

                    {
                        "subtitle": app.br.a7,
                        "value": obj.a7
                    },

                    {
                        "subtitle": app.br.a8,
                        "value": obj.a8
                    }
                ]
            };

            JSONObjb = {
                "title": app.br.sumb,
                "items": [
                    {
                        "subtitle": app.br.b1,
                        "value": obj.b1
                    },

                    {
                        "subtitle": app.br.b2,
                        "value": obj.b2
                    },

                    {
                        "subtitle": app.br.b3,
                        "value": obj.b3
                    },

                    {
                        "subtitle": app.br.b4,
                        "value": obj.b4
                    }
                ]
            };

            JSONObjc = {
                "title": app.br.sumc,
                "items": [
                    {
                        "subtitle": app.br.c1,
                        "value": obj.c1
                    },

                    {
                        "subtitle": app.br.c2,
                        "value": obj.c2
                    },

                    {
                        "subtitle": app.br.c3,
                        "value": obj.c3
                    }
                ]
            };

            JSONObjd = {
                "title": app.br.sumd,
                "items": [
                    {
                        "subtitle": app.br.d1,
                        "value": obj.d1
                    },

                    {
                        "subtitle": app.br.d2,
                        "value": obj.d2
                    },

                    {
                        "subtitle": app.br.d3,
                        "value": obj.d3
                    },

                    {
                        "subtitle": app.br.d4,
                        "value": obj.d4
                    },

                    {
                        "subtitle": app.br.d5,
                        "value": obj.d5
                    },

                    {
                        "subtitle": app.br.d6,
                        "value": obj.d6
                    },

                    {
                        "subtitle": app.br.d7,
                        "value": obj.d7
                    },

                    {
                        "subtitle": app.br.d8,
                        "value": obj.d8
                    },

                    {
                        "subtitle": app.br.d9,
                        "value": obj.d9
                    },

                    {
                        "subtitle": app.br.d10,
                        "value": obj.d10
                    },

                    {
                        "subtitle": app.br.d11,
                        "value": obj.d11
                    },

                    {
                        "subtitle": app.br.d12,
                        "value": obj.d12
                    },

                    {
                        "subtitle": app.br.d13,
                        "value": obj.d13
                    }
                ]
            };

            JSONObje = {
                "title": app.br.sume,
                "items": [
                    {
                        "subtitle": app.br.e1,
                        "value": obj.e1
                    },

                    {
                        "subtitle": app.br.e2,
                        "value": obj.e2
                    }
                ]
            };

            xlsBody = emitXmlHeader() +'\n'+
                emitXML2Column('Салбарын нэр', obj.name, true) +'\n'+
                emitXML2Column('Үнэлгээний огноо',obj.date, false) +'\n'+
                emitXML2Column('Yнэлсэн',obj.addedByName, false) +'\n'+
                emitXML2Column('Баталсан',obj.approvedByName, false) +'\n'+
                emitXmlBody(JSONObja, false) +'\n'+
                emitXmlBody(JSONObjb, false) +'\n'+
                emitXmlBody(JSONObjc, false) +'\n'+
                emitXmlBody(JSONObjd, false) +'\n'+
                emitXmlBody(JSONObje, false) +'\n'+
                emitXML2Column('Үнэлгээний талаар товч тэмдэглэл', obj.comment, false) +'\n'+
                emitXmlFooter();
            break;

        case 'hz':
            JSONObja = {
                "title": app.hz.suma,
                "items": [
                    {
                        "subtitle": app.hz.a1,
                        "value": obj.a1
                    },

                    {
                        "subtitle": app.hz.a2,
                        "value": obj.a2
                    },

                    {
                        "subtitle": app.hz.a3,
                        "value": obj.a3
                    },

                    {
                        "subtitle": app.hz.a4,
                        "value": obj.a4
                    },

                    {
                        "subtitle": app.hz.a5,
                        "value": obj.a5
                    },

                    {
                        "subtitle": app.hz.a6,
                        "value": obj.a6
                    },

                    {
                        "subtitle": app.hz.a7,
                        "value": obj.a7
                    }
                ]
            };

            JSONObjb = {
                "title": app.hz.sumb,
                "items": [
                    {
                        "subtitle": app.hz.b1,
                        "value": obj.b1
                    },

                    {
                        "subtitle": app.hz.b2,
                        "value": obj.b2
                    },

                    {
                        "subtitle": app.hz.b3,
                        "value": obj.b3
                    },

                    {
                        "subtitle": app.hz.b4,
                        "value": obj.b4
                    }
                ]
            };

            JSONObjc = {
                "title": app.hz.sumc,
                "items": [
                    {
                        "subtitle": app.hz.c1,
                        "value": obj.c1
                    },

                    {
                        "subtitle": app.hz.c2,
                        "value": obj.c2
                    },

                    {
                        "subtitle": app.hz.c3,
                        "value": obj.c3
                    }
                ]
            };

            JSONObjd = {
                "title": app.hz.sumd,
                "items": [
                    {
                        "subtitle": app.hz.d1,
                        "value": obj.d1
                    },

                    {
                        "subtitle": app.hz.d2,
                        "value": obj.d2
                    },

                    {
                        "subtitle": app.hz.d3,
                        "value": obj.d3
                    }
                ]
            };

            JSONObje = {
                "title": app.hz.sume,
                "items": [
                    {
                        "subtitle": app.hz.e1,
                        "value": obj.e1
                    },

                    {
                        "subtitle": app.hz.e2,
                        "value": obj.e2
                    }
                ]
            };

            JSONObjf = {
                "title": app.hz.sumf,
                "items": [
                    {
                        "subtitle": app.hz.f1,
                        "value": obj.f1
                    },

                    {
                        "subtitle": app.hz.f2,
                        "value": obj.f2
                    },

                    {
                        "subtitle": app.hz.f3,
                        "value": obj.f3
                    }
                ]
            };


            xlsBody = emitXmlHeader() +'\n'+
                emitXML2Column('ХЗ нэр', obj.branch +'-'+ obj.name, true) +'\n'+
                emitXML2Column('Үнэлгээний огноо',obj.date) +'\n'+
                emitXML2Column('Yнэлсэн', obj.addedByName, false) +'\n'+
                emitXML2Column('Баталсан',obj.approvedByName, false) +'\n'+
                emitXmlBody(JSONObja, false) +'\n'+
                emitXmlBody(JSONObjb, false) +'\n'+
                emitXmlBody(JSONObjc, false) +'\n'+
                emitXmlBody(JSONObjd, false) +'\n'+
                emitXmlBody(JSONObje, false)  +'\n'+
                emitXmlBody(JSONObjf, false) +'\n'+
                emitXML2Column('Үнэлгээний талаар товч тэмдэглэл', obj.comment, false) +'\n'+
                emitXmlFooter();
            break;

        case 'wr':
            JSONObja = {
                "title": app.wr.suma,
                "items": [
                    {
                        "subtitle": app.wr.a1,
                        "value": obj.a1
                    },

                    {
                        "subtitle": app.wr.a2,
                        "value": obj.a2
                    },

                    {
                        "subtitle": app.wr.a3,
                        "value": obj.a3
                    },

                    {
                        "subtitle": app.wr.a4,
                        "value": obj.a4
                    },

                    {
                        "subtitle": app.wr.a5,
                        "value": obj.a5
                    },

                    {
                        "subtitle": app.wr.a6,
                        "value": obj.a6
                    },

                    {
                        "subtitle": app.wr.a7,
                        "value": obj.a7
                    },

                    {
                        "subtitle": app.wr.a8,
                        "value": obj.a8
                    }
                ]
            };

            JSONObjb = {
                "title": app.wr.sumb,
                "items": [
                    {
                        "subtitle": app.wr.b1,
                        "value": obj.b1
                    },

                    {
                        "subtitle": app.wr.b2,
                        "value": obj.b2
                    },

                    {
                        "subtitle": app.wr.b3,
                        "value": obj.b3
                    },

                    {
                        "subtitle": app.wr.b4,
                        "value": obj.b4
                    }
                ]
            };

            JSONObjc = {
                "title": app.wr.sumc,
                "items": [
                    {
                        "subtitle": app.wr.c1,
                        "value": obj.c1
                    },

                    {
                        "subtitle": app.wr.c2,
                        "value": obj.c2
                    },

                    {
                        "subtitle": app.wr.c3,
                        "value": obj.c3
                    }
                ]
            };

            JSONObjd = {
                "title": app.wr.sumd,
                "items": [
                    {
                        "subtitle": app.wr.d1,
                        "value": obj.d1
                    },

                    {
                        "subtitle": app.wr.d2,
                        "value": obj.d2
                    }
                ]
            };

            JSONComm = {
                "title": "Үнэлгээний талаар товч тэмдэглэл",
                "items": [
                    {
                        "subtitle": obj.comment,
                        "value": ""
                    }
                ]
            };
            xlsBody = emitXmlHeader() +'\n'+
                emitXML2Column('Ажилтны нэр', obj.position +'-'+ obj.name, true) +'\n'+
                emitXML2Column('Үнэлгээний огноо',obj.date) +'\n'+
                emitXML2Column('Yнэлсэн',obj.addedByName, false) +'\n'+
                emitXML2Column('Баталсан',obj.approvedByName, false) +'\n'+
                emitXmlBody(JSONObja, false) +'\n'+
                emitXmlBody(JSONObjb, false) +'\n'+
                emitXmlBody(JSONObjc, false) +'\n'+
                emitXmlBody(JSONObjd, false) +'\n'+
                emitXML2Column('Үнэлгээний талаар товч тэмдэглэл', obj.comment, false) +'\n'+
                emitXmlFooter();
            break;


        default:
            break;
    }
    return xlsBody;
};

downloadXLS = function (content, filename, contentType) {
    if (!contentType) contentType = 'application/octet-stream';
    var a = document.createElement('a');
    a.style.display = 'none';
    var blob = new Blob([content], {
        'type': contentType
    });
    a.href = window.URL.createObjectURL(blob);
    a.download = filename;
    a.click();
    URL.revokeObjectURL(a.href);
    a.remove();
};
