/**
 * Created by ub on 6/10/14.
 */

var FILE_SERVER = 'http://localhost:8080';
var SERVER_URL = 'http://localhost:8080';


// сервер рүү хүсэлт илгээх
function rpc(uri, args, callback, asyncflag) {
    var req = new XMLHttpRequest();
    req.open('POST', SERVER_URL +  '/' + uri, asyncflag);
    req.setRequestHeader('Content-Type', 'application/json');
    req.onreadystatechange = function () {
                if (req.readyState === 4) {
                    if (req.status === 200) {
                        try {
                            var obj = JSON.parse(req.responseText);
                            //alert(obj.result[0].currency);
                            callback(obj);
                        } catch (e) {
                            callback({'Error': 'parse error!'});
                        }
                        return;
                    }
              // something went wrong
              app.onLine = false;
              callback({'Error': 'rpc: Error!'});
        }
    };

    // token тохируулах
    req.setRequestHeader('Token', sessionStorage.RPC_TOKEN);
    // хүсэлтийн дугаар
    var ts = new Date().getTime();
    req.setRequestHeader('RequestId', 'ci_' + ts);

    // илгээх
    if (args) {
        req.send(JSON.stringify(args));
    } else {
        req.send();
    }
}

// сервер рүү хүсэлт илгээх
function uploadFile(path, data, callback) {

    var req = new XMLHttpRequest();
    // FIXME: сервер хаягийг тохиргооноос унших
    req.open('POST', FILE_SERVER +  '/' + path, true);
    //req.setRequestHeader('Content-Type', 'application/json');
    req.setRequestHeader('Token', 'SelfLabToken');
    req.onreadystatechange = function () {
        if (req.readyState === 4) {
            if (req.status === 200) {
                try {
                    var obj = JSON.parse(req.responseText);
                    //alert(obj.result[0].currency);
                    callback(obj);
                } catch (e) {
                    callback({'Error': 'parse error!'});
                }

                return;
            }

            // something went wrong
            callback({'Error': 'rpc: Error!'});
        }
    };

    // илгээх
    if (data) {
        req.send(data);
    } else {
        req.send();
    }
}