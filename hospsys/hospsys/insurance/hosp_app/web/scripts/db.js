var DataService = function() {
  // Following member variables are initialized within getConnection().
  this.db_ = null;

  window.ds = this;
};

/**
 * Instantiates the DB connection (re-entrant).
 * @return {!IThenable<!lf.Database>}
 */
DataService.prototype.getConnection = function() {
  if (this.db_ != null) {
    return new Promise(function(resolve, reject) { resolve(this.db_); } );
  }

  var connectOptions = {storeType: lf.schema.DataStoreType.INDEXED_DB};
  return this.buildSchema_().connect(connectOptions).then(
    function(db) {
      this.db_ = db;
      return db;
    }.bind(this));
};


/**
 * Builds the database schema.
 * @return {!lf.schema.Builder}
 * @private
 */
DataService.prototype.buildSchema_ = function() {
  var schemaBuilder = lf.schema.create('checklist', 1);

  //Branch checklists
  schemaBuilder.createTable('BranchChecklists').
    addColumn('addedBy', lf.Type.STRING).
    addColumn('name', lf.Type.STRING).
    addColumn('nickname', lf.Type.STRING).
    addColumn('date', lf.Type.DATE).
    addColumn('a1', lf.Type.STRING).
    addColumn('a2', lf.Type.STRING).
    addColumn('a3', lf.Type.STRING).
    addColumn('a4', lf.Type.STRING).
    addColumn('a5', lf.Type.STRING).
    addColumn('a6', lf.Type.STRING).
    addColumn('a7', lf.Type.STRING).
    addColumn('a8', lf.Type.STRING).
    addColumn('b1', lf.Type.STRING).
    addColumn('b2', lf.Type.STRING).
    addColumn('b3', lf.Type.STRING).
    addColumn('b4', lf.Type.STRING).
    addColumn('c1', lf.Type.STRING).
    addColumn('c2', lf.Type.STRING).
    addColumn('c3', lf.Type.STRING).
    addColumn('d1', lf.Type.STRING).
    addColumn('d2', lf.Type.STRING).
    addColumn('d3', lf.Type.STRING).
    addColumn('d4', lf.Type.STRING).
    addColumn('d5', lf.Type.STRING).
    addColumn('d6', lf.Type.STRING).
    addColumn('d7', lf.Type.STRING).
    addColumn('d8', lf.Type.STRING).
    addColumn('d9', lf.Type.STRING).
    addColumn('d10', lf.Type.STRING).
    addColumn('d11', lf.Type.STRING).
    addColumn('d12', lf.Type.STRING).
    addColumn('d13', lf.Type.STRING).
    addColumn('e1', lf.Type.STRING).
    addColumn('e2', lf.Type.STRING).
    addColumn('suma', lf.Type.STRING).
    addColumn('sumb', lf.Type.STRING).
    addColumn('sumc', lf.Type.STRING).
    addColumn('sumd', lf.Type.STRING).
    addColumn('sume', lf.Type.STRING).
    addColumn('sum', lf.Type.STRING).
    addColumn('comment', lf.Type.STRING).
    addColumn('season', lf.Type.STRING).
    addPrimaryKey(['name']);

  //HZ checklists
  schemaBuilder.createTable('HZChecklists').
    addColumn('addedBy', lf.Type.STRING).
    addColumn('domain', lf.Type.STRING).
    addColumn('name', lf.Type.STRING).
    addColumn('surname', lf.Type.STRING).
    addColumn('date', lf.Type.DATE).
    addColumn('branch', lf.Type.STRING).
    addColumn('a1', lf.Type.STRING).
    addColumn('a2', lf.Type.STRING).
    addColumn('a3', lf.Type.STRING).
    addColumn('a4', lf.Type.STRING).
    addColumn('a5', lf.Type.STRING).
    addColumn('a6', lf.Type.STRING).
    addColumn('a7', lf.Type.STRING).
    addColumn('b1', lf.Type.STRING).
    addColumn('b2', lf.Type.STRING).
    addColumn('b3', lf.Type.STRING).
    addColumn('b4', lf.Type.STRING).
    addColumn('c1', lf.Type.STRING).
    addColumn('c2', lf.Type.STRING).
    addColumn('c3', lf.Type.STRING).
    addColumn('d1', lf.Type.STRING).
    addColumn('d2', lf.Type.STRING).
    addColumn('d3', lf.Type.STRING).
    addColumn('e1', lf.Type.STRING).
    addColumn('e2', lf.Type.STRING).
    addColumn('f1', lf.Type.STRING).
    addColumn('f2', lf.Type.STRING).
    addColumn('f3', lf.Type.STRING).
    addColumn('suma', lf.Type.STRING).
    addColumn('sumb', lf.Type.STRING).
    addColumn('sumc', lf.Type.STRING).
    addColumn('sumd', lf.Type.STRING).
    addColumn('sume', lf.Type.STRING).
    addColumn('sumf', lf.Type.STRING).
    addColumn('sum', lf.Type.STRING).
    addColumn('comment', lf.Type.STRING).
    addColumn('season', lf.Type.STRING).
    addPrimaryKey(['domain']);

  //Workers checklists
  schemaBuilder.createTable('WorkerChecklists').
    addColumn('addedBy', lf.Type.STRING).
    addColumn('domain', lf.Type.STRING).
    addColumn('name', lf.Type.STRING).
    addColumn('surname', lf.Type.STRING).
    addColumn('date', lf.Type.DATE).
    addColumn('branch', lf.Type.STRING).
    addColumn('position', lf.Type.STRING).
    addColumn('a1', lf.Type.STRING).
    addColumn('a2', lf.Type.STRING).
    addColumn('a3', lf.Type.STRING).
    addColumn('a4', lf.Type.STRING).
    addColumn('a5', lf.Type.STRING).
    addColumn('a6', lf.Type.STRING).
    addColumn('a7', lf.Type.STRING).
    addColumn('a8', lf.Type.STRING).
    addColumn('b1', lf.Type.STRING).
    addColumn('b2', lf.Type.STRING).
    addColumn('b3', lf.Type.STRING).
    addColumn('b4', lf.Type.STRING).
    addColumn('c1', lf.Type.STRING).
    addColumn('c2', lf.Type.STRING).
    addColumn('c3', lf.Type.STRING).
    addColumn('d1', lf.Type.STRING).
    addColumn('d2', lf.Type.STRING).
    addColumn('suma', lf.Type.STRING).
    addColumn('sumb', lf.Type.STRING).
    addColumn('sumc', lf.Type.STRING).
    addColumn('sumd', lf.Type.STRING).
    addColumn('sum', lf.Type.STRING).
    addColumn('comment', lf.Type.STRING).
    addColumn('season', lf.Type.STRING).
    addPrimaryKey(['domain']);
  return schemaBuilder;
};
